var Model = require('../slib/model');
var Muser = require('./muser');


class Selection extends Model {
    constructor() {
        super();
        this.addField('name',{tp: 'VARCHAR(200)',visible: true, required: true}); //имя выборки
        this.addField('sdate',{tp: 'TIMESTAMP DEFAULT current_timestamp',visible: true}); // время создания

        this.addReferenceToClass('owner_id','BIGINT',Muser,'id', 'RESTRICT'); // Потзователь обладатель выборки
        /*
            ALTER TABLE Selection ADD COLUMN owner_id BIGINT REFERENCES Muser(id) ON DELETE RESTRICT;
        */
        
        this.addOrder('byid','t0.id');
        this.addOrder('byiddesc','t0.id desc');
        this.addOrder('byname','t0.name');
        this.addOrder('bynamedesc','t0.name desc');
        this.addOrder('bysdate','t0.sdate');
        this.addOrder('bysdatedesc','t0.sdate desc');

        this.addFilter('filter',['name','sdate']);
    }            


    
    /**
     * Функция проверки прав доступа пользователя к данным. Доступ дается только администраторам.
     * @param {Object} user - пользователь, доступ которого нужно проверить
     */
    async testAccess(user) {
        this.oper_user = user;              
        if ((user.isadmin !== true)&&((user.usertype!==3) && (user.usertype!==4))) {
            return {gres: false, text: 'Доступ запрещен.'};
        }        
        return {gres: true, text: 'Ok'};
    }

    async postGuard (user) {  
        this.owner_id = user.id;                        
        return await this.testAccess(user);
    }

    async putGuard (user) {  
        this.owner_id = undefined;                       
        return await this.testAccess(user);
    }

    getUpdateOneScript(wheres, values_in) {
        if ((this.oper_user.isadmin !== true)&&((this.oper_user.usertype == 3) || (this.oper_user.usertype == 4))) {
            let in_values = values_in.concat([this.oper_user.id]);
            let in_wheres = '('+wheres+')' + ' and (owner_id=$'+in_values.length+')';
            let {script, values} = super.getUpdateOneScript(in_wheres,in_values);
            return {script: script, values: values};
        }
        else {
            let {script, values} = super.getUpdateOneScript(wheres,values_in);
            return {script: script, values: values};
        }
    }

    getDeleteOneScript() {
        let pk = this.getPk();
        let owner_rule= '';
        if ((this.oper_user.isadmin !== true)&&((this.oper_user.usertype == 3) || (this.oper_user.usertype == 4))) {
            owner_rule = ' and owner_id=$2 ';
        }
        let jsonflds = this.getFields(true).map(val=>'\''+val+'\','+val).join(',');        
        let result = 'DELETE FROM ' + this.constructor.name+' where '+pk+'=$1 '+owner_rule+' RETURNING json_build_object('+jsonflds+') as data;';                     
        return {script: result, values: [this[pk], this.oper_user.id]};        
    }

    async getAllGuard(user) {
        if ((user.isadmin !== true)&&((user.usertype == 3)||(user.usertype == 4))) {
            this.wheres_default = {script: '(t0.owner_id=$1)', values: [user.id]};
        }
        else if (user.isadmin !== true) {
            return {gres: false, text: 'Доступ запрещен.'};
        }
        return await super.getAllGuard(user);
    }

     
    
}

module.exports = Selection;
