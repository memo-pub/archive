var Model = require('../slib/model');

class NAT_T extends Model {
    constructor() {
        super();              
        this.addField('nation',{tp: 'VARCHAR(12)',visible: true, required: true});
        this.addField('nation_rmk',{tp: 'VARCHAR(60)',visible: true, required: true});

        if (this.afterCreate === undefined) {
            this.afterCreate = '';
        }
        this.afterCreate = this.afterCreate +'CREATE UNIQUE INDEX nation_nat_t ON NAT_T (UPPER(nation)); ';        
        this.addUniqueIndex(['nation_rmk']);        
        
        this.addOrder('byid','t0.id');
        this.addOrder('bynation','t0.nation');
    }            
    
    
    /**
     * Функция проверки прав доступа пользователя к данным. Доступ дается только администраторам.
     * @param {Object} user - пользователь, доступ которого нужно проверить
     */
    async testAccess(user) {
        this.oper_user = user;              
        if ((user.isadmin !== true)&&(user.usertype!==3)) {
            return {gres: false, text: 'Доступ запрещен.'};
        }        
        return {gres: true, text: 'Ok'};
    }


    translateError(err) {
        if (err.code==23505) {
            if (err.message.indexOf('nation_nat_t')>-1) {
                return {code: 409, message:'Национальность с таким кодом уже существует!'};
            }
            if (err.message.indexOf('nation_rmk_nat_t')>-1) {
                return {code: 409, message: 'Расшифровка национальность совпадает с уже существующей!'};
            }                                    
        }
    }
    
}

module.exports = NAT_T;
