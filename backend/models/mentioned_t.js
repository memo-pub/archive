var ChainedModel = require('../slib/chainedmodel');
var PERSONAL_T = require('./personal_t');
var kode_error=[{kod:23505,cod: 409,text:'Данные о человеке уже есть!'},
    {kod:23503,cod: 500,text:'Отсутствует Внешний ключ !'}];

//Название таблицы: Упоминаемые
class MENTIONED_T extends ChainedModel {
    constructor() {
        super();
        this.addReferenceToClass('personal_code','BIGINT NOT NULL',PERSONAL_T,'code', 'CASCADE', true, true); // Ссылка на personal_t (код репрессированного)
        this.addReferenceToClass('human_code','BIGINT NOT NULL',PERSONAL_T,'code', 'CASCADE', true, true); // Ссылка на personal_t(код родственника)
        this.addField('role',{tp: 'VARCHAR(30)',visible: true}); // Степень родства
        this.addField('role_rmk',{tp: 'VARCHAR(240)',visible: true}); //комментарий 
        this.addUniqueIndex(['personal_code','human_code']);
        this.addOrder('byid','t0.id');

        this.objects_chain = [PERSONAL_T];

        this.addReference('personal_t', {table:'personal_t',
            rule: '(t0.human_code={#t_self}.code)', replace:`{#t_self}.code, {#t_self}.surname, {#t_self}.fname, {#t_self}.lname,
            {#t_self}.birth, {#t_self}.birth_begin, {#t_self}.birth_end, {#t_self}.birth_rmk, {#t_self}.type_r, {#t_self}.type_a,{#t_self}.fund, {#t_self}.list_no, {#t_self}.file_no, {#t_self}.file_no_ext`});
    }            

 
    translateError(err) {
        for (var i = 0; i < kode_error.length;i++) {
            if (kode_error[i].kod==err.code) {
                return {code: kode_error[i].cod, message:kode_error[i].text};     
            }
        }
    }

    
    /**
     * Функция проверки прав доступа пользователя к данным. Доступ дается только администраторам.
     * @param {Object} user - пользователь, доступ которого нужно проверить
     */
    async testAccess(user) {
        this.oper_user = user;              
        if ((user.isadmin !== true)&&(user.usertype!==3)) {
            return {gres: false, text: 'Доступ запрещен.'};
        }        
        return {gres: true, text: 'Ok'};
    }
    
}

module.exports = MENTIONED_T;