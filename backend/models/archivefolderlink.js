const cntc = require('../db').connection;
const Model = require('../slib/model');
const ArchiveFolder = require('./archivefolder');
const ArchiveFolderLog = require('./archivefolderlog');

class ArchiveFolderLink extends Model {
    constructor() {
        super();
        this.addReferenceToClass(
            'archivefolder_id1',
            'BIGINT',
            ArchiveFolder,
            'id',
            'CASCADE',
            true,
            true,
        );
        this.addReferenceToClass(
            'archivefolder_id2',
            'BIGINT',
            ArchiveFolder,
            'id',
            'CASCADE',
            true,
            true,
        );
        // link_type - 1 дополнение, 2 родственник, 3 упоминание
        this.addField('link_type', { tp: 'SMALLINT NOT NULL', visible: true });
        //поле для входного параметра POSTMEN
        this.addInputField('id_fordelete');

        this.addReference('af1', {
            table: '(select w1.*, w2.fond, w2.opis_name from archivefolder w1, opis w2 where w2.id=w1.opis_id)',
            rule: '(t0.archivefolder_id1={#t_self}.id)',
            replace:
                '{#t_self}.name as name1, {#t_self}.delo as delo1, {#t_self}.delo_extra as delo_extra1, {#t_self}.postfix as postfix1,  {#t_self}.fond as fond1, {#t_self}.opis_name as opis_name1',
        });
        this.addReference('af2', {
            table: '(select w1.*, w2.fond, w2.opis_name from archivefolder w1, opis w2 where w2.id=w1.opis_id)',
            rule: '(t0.archivefolder_id2={#t_self}.id)',
            replace:
                '{#t_self}.name as name2, {#t_self}.delo as delo2, {#t_self}.delo_extra as delo_extra2, {#t_self}.postfix as postfix2,  {#t_self}.fond as fond2, {#t_self}.opis_name as opis_name2',
        });
        this.addOrder('byid', 't0.id');

        this.afterCreate =
            this.afterCreate +
            'create unique index uniq_archivefolderlink on archivefolderlink (least(archivefolder_id1, archivefolder_id2), greatest(archivefolder_id1, archivefolder_id2));';
    }

    /**
     * Функция проверки прав доступа пользователя к данным. Доступ дается только администраторам.
     * @param {Object} user - пользователь, доступ которого нужно проверить
     */
    async testAccess(user) {
        this.oper_user = user;
        if (
            user.isadmin !== true &&
            user.usertype !== 1 &&
            user.usertype !== 3
        ) {
            return { gres: false, text: 'Доступ запрещен.' };
        }
        return { gres: true, text: 'Ok' };
    }

    async insert(trans = cntc) {
        let selstr2 = `with list1 as (
            select archivefolder_id1 as id1,archivefolder_id2 as id2 from archivefolderlink where link_type=1
            union
            select archivefolder_id2 as id1,archivefolder_id1 as id2 from archivefolderlink where link_type=1
        ),
        list2 as (
            select distinct t1.id1 as id1,
                case when t1.id2=t2.archivefolder_id1 then t2.archivefolder_id2 
                else t2.archivefolder_id1 
                end as id2
            from list1 t1
            left outer join archivefolderlink t2
                on (t1.id2=t2.archivefolder_id1 or t1.id2=t2.archivefolder_id2) and t2.link_type=1
        ),
        to_add as (
            select t1.*, 1 as link_type from list2 t1
            left outer join list1 t2
                on t1.id1=t2.id1 and t1.id2=t2.id2
            where t1.id1<>t1.id2 and t2.id1 is null and t1.id1>t1.id2
        )
        insert into archivefolderlink (archivefolder_id1, archivefolder_id2, link_type)
        (select id1, id2, 1 from to_add) 
        ON CONFLICT DO NOTHING 
        RETURNING archivefolderlink`;

        try {
            let values = [this.archivefolder_id1, this.archivefolder_id2];
            let data = await trans.tx(async (t) => {
                // tx - это транзакция
                let data = await super.insert(t);
                let data1 = await t.any(selstr2, values);
                while (data1[0] != undefined) {
                    data1 = await t.any(selstr2, values);
                }
                if (data != null) {
                    try {
                        let archiveFolderLog = new ArchiveFolderLog();
                        await archiveFolderLog.writeLog(
                            this.oper_user.id,
                            data.archivefolder_id1,
                            27,
                            {
                                new_data: `Связанное дело №${data.archivefolder_id2}`,
                            },
                        );
                    } catch (err) {
                        console.dir({
                            code: 'AF_LOG_ERROR',
                            message: err.message,
                        });
                    }
                }
                return data;
            });
            return data;
        } catch (err) {
            console.dir(err.message);
            throw { code: err.code, message: err.message };
        }
    }

    async deleteOne(trans = cntc) {
        try {
            let values = [this.id_fordelete];
            let selstr2 =
                'delete from archivefolderlink where (((archivefolder_id1=$1) or (archivefolder_id2=$1)) and link_type=1)';
            let data = await trans.tx(async (t) => {
                let data = await super.deleteOne(t);
                if (
                    this.id_fordelete != undefined &&
                    this.id_fordelete != null
                ) {
                    await t.manyOrNone(selstr2, values);
                }
                if (data != null) {
                    try {
                        let archiveFolderLog = new ArchiveFolderLog();
                        await archiveFolderLog.writeLog(
                            this.oper_user.id,
                            data.archivefolder_id1,
                            28,
                            {
                                old_data: `Связанное дело №${data.archivefolder_id2}`,
                            },
                        );
                    } catch (err) {
                        console.dir({
                            code: 'AF_LOG_ERROR',
                            message: err.message,
                        });
                    }
                }
                return data;
            });
            return data;
        } catch (err) {
            throw { code: err.code, message: err.message };
        }
    }
}

module.exports = ArchiveFolderLink;

