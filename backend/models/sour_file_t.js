var ChainedModel = require('../slib/chainedmodel');
var SOUR_T = require('./sour_t');
var ArchiveFolderFile = require('./archivefolderfile');
var kode_error=[{kod:23505,cod: 409,text:'Данные в SOUR_FILE_T уже есть!'},
    {kod:23503,cod: 500,text:'Отсутствует Внешний ключ !'}];

//Название таблицы: Семья репресированного
class SOUR_FILE_T extends ChainedModel {
    constructor() {
        super();
        this.addReferenceToClass('sour_id','BIGINT NOT NULL',SOUR_T,'id', 'CASCADE', true, true); // Ссылка на sour_t
        this.addReferenceToClass('archivefolderfile_id','BIGINT NOT NULL',ArchiveFolderFile,'id', 'CASCADE', true, true); // Ссылка на ArchiveFolderFile 
        this.addUniqueIndex(['sour_id','archivefolderfile_id']);
        this.addOrder('byid','t0.id');

        this.objects_chain = [SOUR_T]; // sour_id ссылается-->SOUR_T ссылается-->PERSONAL_T

    } 
 
    translateError(err) {
        for (var i = 0; i < kode_error.length;i++) {
            if (kode_error[i].kod==err.code) {
                return {code: kode_error[i].cod, message:kode_error[i].text};     
            }
        }
    }
    
    /**
     * Функция проверки прав доступа пользователя к данным. Доступ дается только администраторам.
     * @param {Object} user - пользователь, доступ которого нужно проверить
     */
    async testAccess(user) {
        this.oper_user = user;              
        if ((user.isadmin !== true)&&(user.usertype!==3)) {
            return {gres: false, text: 'Доступ запрещен.'};
        }        
        return {gres: true, text: 'Ok'};
    }
    
}

module.exports = SOUR_FILE_T;
