var Model = require('../slib/model');
var REPRESS_T = require('./repress_t');
var LIST_OF_T = require('./list_of_t');
var kode_error=[{kod:23505,cod: 409,text:'Такой уникальный индекс уже есть!'},
    {kod:23503,cod: 500,text:'Отсутствует Внешний ключ !'}];

class REHABIL_REASON_T extends Model {
    constructor() {
        super();              
        this.addReferenceToClass('rep_id','BIGINT NOT NULL',REPRESS_T,'id', 'CASCADE', true, true);  //CASCADE удалять записи     
        this.addReferenceToClass('reason_id','BIGINT NOT NULL',LIST_OF_T,'id', 'RESTRICT', true, true); //RESTRICT не удалять записи
        this.addUniqueIndex(['rep_id', 'reason_id']);
    }            
    
    translateError(err) {
        if (err.code==23505) {
            if (err.message.indexOf('rep_id_reason_id_rehabil_reason_t')>-1) {
                return {code: 409, message:'Такой уникальный индекс уже есть!'};
            }
        }
        for (var i = 0; i < kode_error.length;i++) {
            if (kode_error[i].kod==err.code) {
                return {code: kode_error[i].cod, message:kode_error[i].text};     
            }
        }
        return {code: err.code, message:'Ошибка!'};
    }

    /**
     * Функция проверки прав доступа пользователя к данным. Доступ дается только администраторам.
     * @param {Object} user - пользователь, доступ которого нужно проверить
     */
    async testAccess(user) {
        this.oper_user = user;              
        if ((user.isadmin !== true)&&(user.usertype!==3)) {
            return {gres: false, text: 'Доступ запрещен.'};
        }        
        return {gres: true, text: 'Ok'};
    }
}

module.exports = REHABIL_REASON_T;
