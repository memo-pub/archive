
var ChainedModel = require('../slib/chainedmodel');
var PERSONAL_T = require('./personal_t');
var ACTIVITY_T = require('./activity_t');
var LIST_OF_T = require('./list_of_t');
var kode_error=[{kod:23505,cod: 409,text:'Такой уникальный индекс уже есть!'},
    {kod:23503,cod: 500,text:'Отсутствует Внешний ключ !'}];

class ACT_SPHERE_T extends ChainedModel {
    constructor() {
        super();              
        this.addReferenceToClass('act_id','BIGINT NOT NULL',ACTIVITY_T,'id', 'CASCADE', true, true);
        //CASCADE при удалении записи на которую указывают act_id  в ACT_SPHERE_T будут удалены все записи с таким act_id в ACT_SPHERE_T 
        this.addReferenceToClass('sphere_id','BIGINT NOT NULL',LIST_OF_T,'id', 'RESTRICT', true, true);
        //ACT_SPHERE_T - таблица где находится ссылка 
        //ACTIVITY_T и LIST_OF_T - таблицы куда указывает ссылка
        //RESTRICT запрещает удалять запись в LIST_OF_T , если на нее есть ссылка из записи  ACT_SPHERE_T
        //SET NULL и SET DEFAULT. При удалении строки LIST_OF_T в они назначают  
        //столбцам ссылок в таблице ACT_SPHERE_T значения NULL или значения по умолчанию, соответственно
        this.addUniqueIndex(['act_id', 'sphere_id']);
        
        this.objects_chain = [PERSONAL_T,ACTIVITY_T];
    }            
    //Заполнить таблицу данными на основе поля sphere из ACTIVITY_T.
    // написать какой-то скрипт, который сможет разбивать поле sphere по несимвольным разделителям, и находить для них соответствия в LIST_OF_T.
    // Разделители могут быть разными: точки, двоеточия, слэши и т.д.
    // Нужно будет написать этот скрипт и таким образом, чтобы он помечал строки, для которых не удалось найти соответствия в LIST_OF_T
    //А уже потом заполнить новую таблицу. 

    translateError(err) {
        if (err.code==23505) {
            if (err.message.indexOf('act_id_sphere_id_act_sphere_t')>-1) {
                return {code: 409, message:'Такой уникальный индекс уже есть!'};
            }
        }
        for (var i = 0; i < kode_error.length;i++) {
            if (kode_error[i].kod==err.code) {
                return {code: kode_error[i].cod, message:kode_error[i].text};     
            }
        }
        return {code: err.code, message:'Ошибка!'};
    }

    /**
     * Функция проверки прав доступа пользователя к данным. Доступ дается только администраторам.
     * @param {Object} user - пользователь, доступ которого нужно проверить
     */
    async testAccess(user) {
        this.oper_user = user;              
        if ((user.isadmin !== true)&&(user.usertype!==3)) {
            return {gres: false, text: 'Доступ запрещен.'};
        }        
        return {gres: true, text: 'Ok'};
    }
}

module.exports = ACT_SPHERE_T;
