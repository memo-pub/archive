var ChainedModel = require('../slib/chainedmodel');
var PERSONAL_T = require('./personal_t');
var COURT_T = require('./court_t');
var LIST_OF_T = require('./list_of_t');
var cntc = require('../db').connection;
var MoveOrderNom = require('../viewmodels/moveordernom');

var kode_error=[{kod:23505,cod: 409,text:'Номер заключения занят!'},
    {kod:23503,cod: 500,text:'Отсутствует Внешний ключ !'}];


//Название таблицы: Заключение 

class IMPRIS_T extends ChainedModel {
    constructor() {
        super();
        this.addReferenceToClass('crt_id','BIGINT NOT NULL',COURT_T,'id', 'CASCADE', true, true); // ссылка на COURT_T
        this.addReferenceToClass('personal_code','BIGINT NOT NULL',PERSONAL_T,'code', 'RESTRICT', true, true); // не используется
        
        //this.addField('repress_no',{tp: 'NUMERIC(2,0)',visible: false}); //
        //this.addField('trial_no',{tp: 'NUMERIC(2,0)',visible: false}); // Номер судебного процесса
        this.addField('impris_no',{tp: 'NUMERIC(4,1) NOT NULL',visible: true, required: true}); //Номер заключения
        //this.addField('impris_type',{tp: 'VARCHAR(30)',visible: true}); // Тип заключения

        this.addReferenceToClass('impris_type_id','BIGINT',LIST_OF_T,'id', 'RESTRICT', true, false); // Ссылка на list_of_t 

        this.addReferenceToClass('protest_id','BIGINT',LIST_OF_T,'id', 'RESTRICT', true, false); // Ссылка на list_of_t 

        this.addReferenceToClass('reason_id','BIGINT',LIST_OF_T,'id', 'RESTRICT', true, false); // Ссылка на list_of_t  ПРИЧИНА УБЫТИЯ
        // for JOIN
        //imp_name - произвольное имя ссылки (imp_name=true в запросе POSTMAN, если нужен join for list_of_t)
        //table:'list_of_t'  - имя таблицы на которую ссылаемся
        //t0.impris_type_id={#t_self}.id  имена полей по которым связываются таблицы поле откуда и поле куда ссылка 
        //{#t_self}.name_ent'}  - какое поле взять из таблицы на которую идет ссылка 
        //
        this.addReference('imp_name', {table:'list_of_t',
            rule: '(t0.impris_type_id={#t_self}.id)',
            replace:'{#t_self}.name_ent'});
        //  
        //this.addField('prison_place_code',{tp: 'VARCHAR(18)',visible: false}); //Судебный орган
        //  

        this.addField('prison_name',{tp: 'VARCHAR(230)',visible: true}); //Название места заключения
        /*
            ALTER TABLE IMPRIS_T ALTER prison_name TYPE varchar(230); 
        */
        this.addField('arrival_dat',{tp: 'DATE',visible: true}); //Дата прибытия
        this.addField('arrival_dat_begin',{tp: 'DATE',visible: true});
        this.addField('arrival_dat_end',{tp: 'DATE',visible: true});
        this.addField('leave_dat',{tp: 'DATE',visible: true}); //Дата убытия
        this.addField('leave_dat_begin',{tp: 'DATE',visible: true});
        this.addField('leave_dat_end',{tp: 'DATE',visible: true});
        //this.addField('reason',{tp: 'VARCHAR(50)',visible: true}); //Причина убытия
        this.addField('work',{tp: 'VARCHAR(30)',visible: true}); //Работа в заключении
        //this.addField('protest',{tp: 'VARCHAR(30)',visible: true}); //Акции протеста в заключении
        this.addField('work_rmk',{tp: 'VARCHAR(240)',visible: true}); //Комментарий к работе
        this.addField('protest_rmk',{tp: 'VARCHAR(240)',visible: true}); //Комментарий к протесту
        this.addField('prison_name_rmk',{tp: 'VARCHAR(240)',visible: true}); //Комментарий к места заключения
        this.addField('prison_place_rmk',{tp: 'VARCHAR(240)',visible: true}); //Комментарий к географическому месту заключения
        this.addField('arrival_dat_rmk',{tp: 'VARCHAR(240)',visible: true}); //Комментарий к дате прибытия
        this.addField('leave_dat_rmk',{tp: 'VARCHAR(240)',visible: true}); //Комментарий к дате убытия
        this.addField('reason_rmk',{tp: 'VARCHAR(240)',visible: true}); //Комментарий к причине убытия
        this.addField('place_num',{tp: 'NUMERIC(8,0)',visible: true}); //Номер географической единицы места заключения   

        this.addField('general_rmk',{tp: 'VARCHAR(500)',visible: true}); //Комментарий общий

        /*
        ALTER TABLE IMPRIS_T ADD COLUMN general_rmk VARCHAR(500);
        */

        this.addUniqueIndex(['crt_id','impris_no']);      
        this.addOrder('byid','t0.id');   

        this.addOrder('bypos','t0.crt_id, t0.impris_no');

        this.addInputField('newnom',1234);
        
        this.objects_chain = [PERSONAL_T, REPRESS_T, COURT_T];
    }            


    translateError(err) {
        for (var i = 0; i < kode_error.length;i++) {
            if (kode_error[i].kod==err.code) {
                return {code: kode_error[i].cod, message:kode_error[i].text};     
            }
        }
    }
    /*  UPDATE IMPRIS_T m 
            SET impris_no = sub.rn 
        FROM  
         (SELECT crt_id,impris_no, row_number() OVER (PARTITION BY crt_id order by crt_id,impris_no) AS rn 
            FROM IMPRIS_T WHERE (crt_id IS NOT NULL) and (impris_no IS NOT NULL)) sub 
         WHERE  m.crt_id = sub.crt_id and m.impris_no=sub.impris_no
*/
    
    /**
     * Функция проверки прав доступа пользователя к данным. Доступ дается только администраторам.
     * @param {Object} user - пользователь, доступ которого нужно проверить
     */
    async testAccess(user) {
        this.oper_user = user;              
        if ((user.isadmin !== true)&&(user.usertype!==3)) {
            return {gres: false, text: 'Доступ запрещен.'};
        }        
        return {gres: true, text: 'Ok'};
    }

    async insert(trans=cntc) {
        try {
            let data  = await trans.tx(async t => {
                if (this.impris_no == null)  {
                    let sql = `select COALESCE(max(impris_no)+1,1) as num from IMPRIS_T 
                                where crt_id=$1`;
                    let no = await t.one(sql,[this.crt_id]);
                    this.impris_no = no.num;
                }
                let d = await super.insert(t);
                if ((d!=null) && (this.newnom!=undefined)) {
                    let moveOrderNom = new MoveOrderNom();
                    moveOrderNom.newnom = this.newnom;
                    moveOrderNom.itemid = d.id;
                    moveOrderNom.tablename = 'IMPRIS_T';
                    let new_gr = await moveOrderNom.insert(t);
                    if (new_gr!=null) {
                        let dn = new_gr.find(val=>val.id==d.id);
                        if (dn !=null) {
                            d.impris_no = dn.impris_no;
                        }
                        else {
                            d.impris_no = this.newnom;
                        }
                    }
                    else {
                        d.impris_no = this.newnom;
                    }
                } 
                return d;
            });
            return data;
        }
        catch(err) {
            throw {code: err.code, message: err.message};
        } 
    }
    
}

module.exports = IMPRIS_T;

var REPRESS_T = require('./repress_t');