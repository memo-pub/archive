const readlineSync = require('readline-sync');
const FlagedModel = require('../slib/flagedmodel');
const { getPwdHash } = require('../getpwdhash');

class MUser extends FlagedModel {
    constructor() {
        super();
        this.addField('login', { tp: 'VARCHAR(20)', visible: true, required: true });
        this.addField('name', { tp: 'VARCHAR(100)', visible: true, required: true });
        this.addField('surname', { tp: 'VARCHAR(100)', visible: true, required: true });

        this.addField('sk', { tp: 'VARCHAR(120)', required: true });
        // usertype (-1) администратор, (1) архивист, (2) сканировщик
        this.addField('usertype', { tp: 'SMALLINT DEFAULT -1', visible: true });
        this.addField('isadmin', { tp: 'BOOLEAN DEFAULT FALSE', visible: true });

        this.addUniqueIndex(['login'], 'flagarchive=0');

        this.addOrder('byid', 'id');
        this.addOrder('bylogin', 'login');
    }

    loadFromRequest(data) {
        super.loadFromRequest(data);
        const { password } = data;
        this.sk = password !== undefined ? getPwdHash(password) : undefined;
    }

    async merge() {
        try {
            await super.merge();
            this.name = readlineSync.question('Admin user name: ');
            this.surname = readlineSync.question('Admin user surname: ');
            this.login = readlineSync.question('Admin user login: ');
            const pwd = readlineSync.question('Admin user password: ', { hideEchoBack: true });

            this.sk = pwd !== undefined ? getPwdHash(pwd) : undefined;

            this.isadmin = true;
            this.logerModel = undefined;
            this.usertype = -1;
            await this.insert();
        } catch (error) {
            throw { code: error.code, message: error.message };
        }
    }

    async getOneGuard(user) {
        if (user.id == this.id) {
            // Сделано, чтобы в случае, когда пользователь запрашивает самого себя, он был как админ
            user.isadmin = true;
        }

        return this.getAllGuard(user);
    }
}

module.exports = MUser;
