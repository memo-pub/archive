const Model = require('../slib/model');
const { connection: cntc } = require('../db');

const Opis = require('./opis');
const Muser = require('./muser');

// Таблица Дело
module.exports = class ArchiveFolder extends Model {
    constructor() {
        super();
        this.addReferenceToClass(
            'opis_id',
            'BIGINT',
            Opis,
            'id',
            'RESTRICT',
            true,
            true,
        );
        this.addField('delo', { tp: 'BIGINT', visible: true, required: true });
        this.addField('delo_extra', {
            tp: "VARCHAR(20) NOT NULL DEFAULT ''",
            visible: true,
        });

        this.addField('postfix', { tp: 'VARCHAR(3)', visible: true });
        this.addField('descrip_type', { tp: 'VARCHAR(40)', visible: true });
        this.addField('descrip_type_rmk', {
            tp: 'VARCHAR(240)',
            visible: true,
        });
        this.addField('name', {
            tp: 'VARCHAR(500)',
            visible: true,
            required: true,
        });
        this.addField('description', { tp: 'VARCHAR(5000)', visible: true });
        this.addField('page_cnt', {
            tp: 'INTEGER',
            visible: true,
            required: true,
        });
        this.addField('spec_page_cnt', {
            tp: 'INTEGER',
            visible: true,
            required: true,
        });
        this.addField('doc_cnt', {
            tp: 'INTEGER',
            visible: true,
            required: true,
        });
        this.addField('foto_cnt', {
            tp: 'INTEGER',
            visible: true,
            required: true,
        });
        this.addField('have_scanned', {
            tp: 'BOOLEAN',
            visible: true,
            required: true,
        });
        this.addField('have_memories', {
            tp: 'BOOLEAN',
            visible: true,
            required: true,
        });
        this.addField('have_arts', {
            tp: 'BOOLEAN',
            visible: true,
            required: true,
        });
        this.addField('have_act', {
            tp: 'BOOLEAN',
            visible: true,
            required: true,
        });

        this.addField('status', {
            tp: 'SMALLINT',
            visible: true,
            required: true,
        });
        this.addField('comment', { tp: 'VARCHAR(5000)', visible: true });
        this.addField('date', { tp: 'VARCHAR(50)', visible: true });
        this.addField('date_created', {
            tp: 'timestamp with time zone DEFAULT current_timestamp',
            visible: true,
        });
        this.addField('date_scan', {
            tp: 'timestamp with time zone DEFAULT NULL',
            visible: true,
        });
        this.addField('date_finish', {
            tp: 'timestamp with time zone DEFAULT NULL',
            visible: true,
        });

        /*
            ALTER TABLE ArchiveFolder ADD COLUMN date_finish timestamp with time zone DEFAULT NULL;
        */

        this.addField('url', { tp: 'VARCHAR(1000)', visible: true });
        this.addField('not_scanned', {
            tp: 'BOOLEAN DEFAULT FALSE',
            visible: true,
        });
        this.addField('not_scanned_rmk', { tp: 'VARCHAR(500)', visible: true });
        this.addField('digital', {
            tp: 'BOOLEAN DEFAULT FALSE',
            visible: true,
        });

        this.addReferenceToClass(
            'scanuser_id',
            'BIGINT',
            Muser,
            'id',
            'RESTRICT',
            true,
        );
        this.addReferenceToClass(
            'createduser_id',
            'BIGINT',
            Muser,
            'id',
            'RESTRICT',
            true,
            true,
        );

        this.addField('scan_comment', { tp: 'VARCHAR(5000)', visible: true });

        /*
            ALTER TABLE ArchiveFolder ADD COLUMN scan_comment VARCHAR(5000);
        */

        // добавлено строк 4 - 22.04.2020
        this.addField('doc_begin', { tp: 'SMALLINT', visible: true }); //
        // даты документов (крайние годы) - два числовых поля
        this.addField('doc_end', { tp: 'SMALLINT', visible: true });
        // Дата публикации дела. (поле типа дата)
        this.addField('pub_date', { tp: 'DATE', visible: true });
        // Количество пронумерованных листов (текстовое, 30 знаков)
        this.addField('page_num_cnt', { tp: 'VARCHAR(30)', visible: true });
        /*
        ALTER TABLE IF EXISTS ARCHIVEFOLDER
          ADD COLUMN IF NOT EXISTS DOC_BEGIN SMALLINT,
		  ADD COLUMN IF NOT EXISTS DOC_END SMALLINT,
		  ADD COLUMN IF NOT EXISTS PUB_DATE DATE,
          ADD COLUMN IF NOT EXISTS PAGE_NUM_CNT varchar(30)
          */

        this.addField('published', {
            tp: "VARCHAR(1) CHECK (published IN ('*',' '))",
            visible: true,
        }); //published: 'дело опубликовано'
        this.addField('mustPublish', {
            tp: "VARCHAR(1) CHECK (mustPublish IN ('*',' '))",
            visible: true,
        }); // сущность должна опубликоваться
        this.addField('mustUnpublish', {
            tp: "VARCHAR(1) CHECK (mustUnpublish IN ('*',' '))",
            visible: true,
        }); // сущность должна удалиться из публичной базы
        this.addField('pub_change_date', {
            tp: 'timestamp with time zone DEFAULT current_timestamp',
            visible: true,
        }); // дата обновления статуса публикации
        this.addReference('scanuser', {
            table: 'muser',
            rule: '(t0.scanuser_id={#t_self}.id)',
            replace: `{#t_self}.name as scanuser_name,
                        {#t_self}.surname as scanuser_surname,
                        {#t_self}.login as scanuser_login`,
            alias: 'scanusertable',
        });
        this.addReference('createduser', {
            table: 'muser',
            rule: '(t0.createduser_id={#t_self}.id)',
            replace: `{#t_self}.name as createduser_name,
                        {#t_self}.surname as createduser_surname,
                        {#t_self}.login as createduser_login`,
            alias: 'createdusertable',
        });
        this.addReference('opis_data', {
            table: 'opis',
            rule: '(t0.opis_id={#t_self}.id)',
            replace: '{#t_self}.opis_name as opis_name, {#t_self}.fond as fond',
        });

        this.scanuser = true;
        this.createduser = true;

        this.addReference('countfiles', {
            table: `(select archivefolder_id, count(*) as cnt 
                    from archivefolderfile  group by archivefolder_id)`,
            rule: '(t0.id={#t_self}.archivefolder_id)',
            replace: '{#t_self}.cnt as files_count',
        });

        this.addOrder('byid', 't0.id');
        this.addOrder('byname', 't0.name');
        this.addOrder(
            'byshifr',
            'fond, opis_name, delo, delo_extra NULLS FIRST, postfix NULLS FIRST',
        );

        this.addOrder('byiddesc', 't0.id desc');
        this.addOrder('bynamedesc', 't0.name desc');
        this.addOrder(
            'byshifrdesc',
            'fond desc, opis_name desc, delo desc, delo_extra NULLS LAST, postfix desc NULLS LAST',
        );

        this.addFilter('filter_name_delo', ['name', 'delo']);

        this.addUserFilter(
            'filter_name_delo_exact',
            `((UPPER(t0.name::varchar) like  UPPER({#f_value})) OR
            (UPPER(t0.delo::varchar) like  UPPER({#f_value})))`,
        );
        this.addInputField('searchtxt');
        this.addInputField('searchflds');
        this.addUniqueIndex(['opis_id', 'delo', 'postfix', 'delo_extra']);
        this.addUniqueIndex(
            ['opis_id', 'delo', 'postfix'],
            '(delo_extra is NULL)',
        );
        this.addUniqueIndex(
            ['opis_id', 'delo', 'delo_extra'],
            '(postfix is NULL)',
        );
        this.addUniqueIndex(
            ['opis_id', 'delo'],
            '(postfix is NULL) and (delo_extra is NULL)',
        );

        this.addUserFilter('opis_fond', 'fond = {#f_value}');
        this.addUserFilter(
            'delo_extra_empty',
            "(delo_extra is null or delo_extra='')",
        );
        this.addUserFilter('opis_opis_name', 'opis_name = {#f_value}');
        /**
          CREATE UNIQUE INDEX opis_id_delo_postfix_delo_extra_archivefolder ON archivefolder (opis_id,delo,postfix,delo_extra);
          CREATE UNIQUE INDEX opis_id_delo_postfix_archivefolder ON archivefolder (opis_id,delo,postfix)  WHERE delo_extra is NULL;
          CREATE UNIQUE INDEX opis_id_delo_delo_extra_archivefolder ON archivefolder (opis_id,delo,delo_extra)  WHERE (postfix is NULL);
          CREATE UNIQUE INDEX opis_id_delo_archivefolder ON archivefolder (opis_id,delo)  WHERE (postfix is NULL) and (delo_extra is NULL);

          ALTER TABLE archivefolder ADD COLUMN digital BOOLEAN DEFAULT FALSE;
         */

        this.addUserFilter('doc_cnt_more', 'doc_cnt >= {#f_value}');
        this.addUserFilter('foto_cnt_more', 'foto_cnt >= {#f_value}');
    }

    translateError(err) {
        if (err.code === 23_505 && err.message.includes('opis_id_delo')) {
            return { code: 409, message: 'Номер дела занят!' };
        }
    }

    // 2020.02.19
    async selectAllJSON(selecttype, trans) {
        let elem_like = '';
        let str_like = [];
        let field_mas = [
            ['t0.description'],
            ['t0.comment'],
            ['t0.url'],
            ['t0.not_scanned_rmk'],
            ['t0.scan_comment'],
            [
                'scanusertable.login',
                'scanusertable.name',
                'scanusertable.surname',
            ],
            [
                'createdusertable.login',
                'createdusertable.name',
                'createdusertable.surname',
            ],
        ];

        if (this.searchflds !== undefined && this.searchtxt != undefined) {
            if (Array.isArray(this.searchflds) !== true) {
                this.searchflds = [this.searchflds];
            }

            for (let nm of this.searchflds) {
                if (nm == 5) this.scanuser = true;
                if (nm == 6) this.createduser = true;

                // достаточно проверить, существует ли такой элемент
                if (!field_mas[nm]) {
                    // Если элемент в массиве не найден, то можно сообщить об ошибке
                    // Только я делаю не return, а вызываю исключение
                    throw {
                        code: 409,
                        message: 'Неверный индекс поля для поиска.',
                    };
                }
                for (let nm1 of field_mas[nm]) {
                    elem_like = `(UPPER(${nm1}::varchar) like UPPER({#f_value}))`;
                    str_like.push(elem_like);
                }
            }
        }

        elem_like = '(' + str_like.join(' OR ') + ')';
        this.addUserFilter('searchflds234', elem_like);
        this.searchflds234 = this.searchtxt;
        // для просмотра отработавших SQL раскоментируй строку ниже
        // this.debugSQL=true;
        // обрати внимание, что я сипользую let, а не var. Это создает локальную переменную, а не глобальную
        let u = await super.selectAllJSON(selecttype, trans);
        this.searchflds234 = undefined;
        return u;
    }

    /**
     * Функция проверки прав доступа пользователя к данным. Доступ дается только администраторам.
     * @param {Object} user - пользователь, доступ которого нужно проверить
     */
    async testAccess(user) {
        this.oper_user = user;

        if (
            user.isadmin !== true &&
            user.usertype !== 1 &&
            user.usertype !== 3
        ) {
            return { gres: false, text: 'Доступ запрещен.' };
        }

        if (this.delo_extra == null) {
            this.delo_extra = '';
        }
        return { gres: true, text: 'Ok' };
    }

    async putGuard(user) {
        this.date_created = undefined;
        this.date_scan = undefined;
        this.date_finish = undefined;

        if (user.isadmin !== true) {
            this.scanuser_id = undefined;
            this.createduser_id = undefined;
        }

        let scannerFields = [
            'status',
            'id',
            'scan_comment',
            'doc_cnt',
            'foto_cnt',
        ];
        let hasScannerFields = false;

        for (let f of scannerFields) {
            if (f !== 'id' && this[f] != undefined) {
                hasScannerFields = true;
            }
        }

        if (user.isadmin !== true && user.usertype == 2 && hasScannerFields) {
            for (let fld in this._fields) {
                if (!scannerFields.includes(fld)) {
                    this[fld] = undefined;
                }
            }

            if (this.status == 2) {
                let dt = new Date();
                this.date_scan = dt.toISOString();
                this.scanuser_id = user.id;
            }
            return { gres: true, text: 'Ok' };
        }

        let res = await this.testAccess(user);

        if (res.gres) {
            let af = new ArchiveFolder();
            af.id = this.id;
            let af_data = await af.selectOneByIdJSON();

            if (af_data != null) {
                if (af_data.status < 3 && this.status == 3) {
                    let dt = new Date();
                    this.date_finish = dt.toISOString();
                } else if (af_data.status == 3 && this.status < 3) {
                    this.date_finish = null;
                }
            }
        }
        return res;
    }

    async selectOneByIdJSON(trans = cntc) {
        this.opis_data = true;
        let data = await super.selectOneByIdJSON(trans);
        data.peronals_main = await this.getMainPersonal(
            data.fond,
            data.opis_name,
            data.delo,
            data.delo_extra,
            trans,
        );
        data.peronals_second = await this.getSecondPersonal(
            data.fond,
            data.opis_name,
            data.delo,
            data.delo_extra,
            trans,
        );
        data.archive_folder_log = await this.getLog(data.id, trans);
        return data;
    }

    async getLog(id, trans) {
        if (id) {
            let sql = `SELECT t2.login, t1.action_time, t1.action_type, t1.object_data
        FROM public.archivefolderlog t1
        left outer join muser t2
        on t1.muser_id=t2.id
        where archivefolder_id=$1
        order by t1.action_time desc;`;
            return await trans.manyOrNone(sql, [id]);
        } else return [];
    }

    async getDescripTypes(trans) {
        let sql = `select id, name_ent from list_of_t where name_f='DESCRIP_TYPE';`;
        return await trans.manyOrNone(sql);
    }

    async getUserTypes(trans) {
        let sql = `select id, name, surname from muser order by id ;`;
        return await trans.manyOrNone(sql);
    }

    async getSecondPersonal(fond, opisName, delo, deloExtra, trans) {
        if (fond && opisName && delo) {
            let sql = `select distinct t1.code, t1.surname, t1.fname, t1.lname from personal_t t1, personal_shifr_t t2
                        where t1.code=t2.personal_code and t2.fund=$1 and t2.list_no=$2 
                        and t2.file_no=$3 and COALESCE(t2.file_no_ext,'')=COALESCE($4,'')`;

            return trans.manyOrNone(sql, [fond, opisName, delo, deloExtra]);
        }
        return [];
    }

    async getMainPersonal(fond, opisName, delo, deloExtra, trans) {
        if (fond && opisName && delo) {
            let sql = `select code, surname, fname, lname from personal_t 
                        where fund=$1 and list_no=$2 and file_no=$3 and COALESCE(file_no_ext,'')=COALESCE($4,'')`;

            return trans.manyOrNone(sql, [fond, opisName, delo, deloExtra]);
        }
        return [];
    }

    async postGuard(user) {
        this.date_created = undefined;
        this.date_scan = undefined;
        this.date_finish = undefined;
        this.scanuser_id = undefined;
        this.createduser_id = user.id;
        return this.testAccess(user);
    }

    async deleteOne() {
        try {
            let data = await super.deleteOne();

            if (data) {
                try {
                    let ArchiveFolderLog = require('./archivefolderlog');
                    let archiveFolderLog = new ArchiveFolderLog();
                    await archiveFolderLog.writeLog(
                        this.oper_user.id,
                        data.id,
                        7,
                    );
                } catch (error) {
                    console.dir({
                        code: 'AF_LOG_ERROR',
                        message: error.message,
                    });
                }
            }
            let ArchiveFolderLink = require('./archivefolderlink');
            let al = new ArchiveFolderLink();
            al.archivefolder_id1 = this.id;
            al.deleteOne();
            al.archivefolder_id1 = undefined;
            al.archivefolder_id2 = this.id;
            al.deleteOne();
            return data;
        } catch (error) {
            throw { code: error.code, message: error.message };
        }
    }

    async insert(trans = cntc) {
        let data = await super.insert(trans);
        let ArchiveFolderLog = require('./archivefolderlog');
        if (data != null) {
            try {
                let archiveFolderLog = new ArchiveFolderLog();
                await archiveFolderLog.writeLog(this.oper_user.id, data.id, 1);
            } catch (error) {
                console.dir({ code: 'AF_LOG_ERROR', message: error.message });
            }
        }
        return data;
    }

    async update(trans = cntc) {
        let archiveFolder = new ArchiveFolder();
        archiveFolder.id = this.id;
        let oldData = await archiveFolder.selectOneByIdJSON();
        let data = await super.update(trans);
        let ArchiveFolderLog = require('./archivefolderlog');

        if (data != null && oldData != null) {
            try {
                if (data.status != oldData.status) {
                    let archiveFolderLog = new ArchiveFolderLog();
                    let statuses = [
                        'Создано',
                        'Заполнено',
                        'Отсканировано',
                        'Закончено',
                    ];
                    const userId = this.oper_user?.id
                        ? this.oper_user.id
                        : data.scanuser_id;
                    await archiveFolderLog.writeLog(userId, data.id, 2, {
                        old_data: statuses[oldData.status],
                        new_data: statuses[data.status],
                    });
                }
                if (data.descrip_type !== oldData.descrip_type) {
                    const archiveFolderLog = new ArchiveFolderLog();
                    const descrip_types = await this.getDescripTypes(
                        (trans = cntc),
                    );
                    const findDescrType = (id) =>
                        descrip_types?.find((el) => el.id === id)?.name_ent;
                    await archiveFolderLog.writeLog(
                        this.oper_user.id,
                        data.id,
                        11,
                        {
                            old_data: findDescrType(oldData.descrip_type),
                            new_data: findDescrType(data.descrip_type),
                        },
                    );
                }
                if (data.date !== oldData.date) {
                    const archiveFolderLog = new ArchiveFolderLog();
                    await archiveFolderLog.writeLog(
                        this.oper_user.id,
                        data.id,
                        29,
                        {
                            old_data: oldData.date,
                            new_data: data.date,
                        },
                    );
                }
                if (
                    data.createduser_id !== oldData.createduser_id ||
                    data.scanuser_id !== oldData.scanuser_id
                ) {
                    const archiveFolderLog = new ArchiveFolderLog();
                    const createduser_ids = await this.getUserTypes(
                        (trans = cntc),
                    );
                    const findUserType = (id) => {
                        const userType = createduser_ids?.find(
                            (el) => +el.id === id,
                        );
                        return userType === undefined
                            ? ''
                            : `${userType.name} ${userType.surname}`;

                    };
                    if (data.createduser_id !== oldData.createduser_id) {
                        await archiveFolderLog.writeLog(
                            this.oper_user.id,
                            data.id,
                            25,
                            {
                                old_data: findUserType(oldData.createduser_id),
                                new_data: findUserType(data.createduser_id),
                            },
                        );
                    } 
                    if (data.scanuser_id !== oldData.scanuser_id){
                        const userId = this.oper_user?.id
                            ? this.oper_user.id
                            : data.scanuser_id;
                        await archiveFolderLog.writeLog(userId, data.id, 26, {
                            old_data: findUserType(oldData.scanuser_id),
                            new_data: findUserType(data.scanuser_id),
                        });
                    }
                }

                const fieldsToCompare = [
                    'opis_id',
                    'delo',
                    'delo_extra',
                    'descrip_type',
                    'descrip_type_rmk',
                    'name',
                    'description',
                    'page_cnt',
                    'spec_page_cnt',
                    'comment',
                    'date_created',
                    'url',
                    'scan_comment',
                    'doc_begin',
                    'doc_end',
                    'pub_date',
                    'page_num_cnt',
                ];
                for (const [
                    index,
                    fieldToCompare,
                ] of fieldsToCompare.entries()) {
                    if (
                        data[fieldToCompare] !== oldData[fieldToCompare] &&
                        fieldToCompare !== 'descrip_type'
                    ) {
                        const archiveFolderLog = new ArchiveFolderLog();
                        const userId = this.oper_user.id
                            ? this.oper_user.id
                            : data.scanuser_id;
                        await archiveFolderLog.writeLog(
                            userId,
                            data.id,
                            8 + index,
                            {
                                old_data: oldData[fieldToCompare],
                                new_data: data[fieldToCompare],
                            },
                        );
                    }
                }
            } catch (error) {
                console.dir({
                    code: 'AF_LOG_ERROR_UPDATE',
                    message: error.message,
                });
            }
        }
        return data;
    }
};
