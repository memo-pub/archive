var ChainedModel = require('../slib/chainedmodel');
var PERSONAL_T = require('./personal_t');
var LIST_OF_T = require('./list_of_t');

var kode_error=[{kod:23505,cod: 409,text:'Номер помощи для человека уже занят!'},
    {kod:23503,cod: 500,text:'Отсутствует Внешний ключ !'}];

//Название таблицы: Сведения о помощи 

class HELP_T extends ChainedModel {
    constructor() {
        super();
        this.addReferenceToClass('personal_code','BIGINT NOT NULL',PERSONAL_T,'code', 'CASCADE', true, true); // Ссылка на personal_t
        this.addField('help_no',{tp:'NUMERIC(4,1) NOT NULL',visible: true, required: true}); //номер помощи 
        //this.addField('help_typ',{tp: 'VARCHAR(30)',visible: true}); // тип помощи

        this.addReferenceToClass('help_typ_id','BIGINT',LIST_OF_T,'id', 'RESTRICT', true, false); // Ссылка на list_of_t 
        this.addReference('help_typ_data', {table:'LIST_OF_T',rule: '(t0.help_typ_id={#t_self}.id)', replace:'{#t_self}.name_ent as help_typ'});

        this.addField('help_typ_rmk',{tp: 'VARCHAR(240)',visible: true}); //комментарий к типу помощи
        this.addField('help_dat',{tp: 'DATE',visible: true}); //дата помощи
        this.addField('whom',{tp: 'VARCHAR(30)',visible: true}); // кому оказывалась помощь
        this.addField('whom_rmk',{tp: 'VARCHAR(240)',visible: true}); //коментарий к кому оказывалась помощь
        this.addUniqueIndex(['personal_code','help_no']);
        this.addOrder('byid','t0.id');

        this.addOrder('bypos','t0.personal_code, t0.help_no');

        this.objects_chain = [PERSONAL_T];
    }            

    /*  UPDATE HELP_T m 
     SET help_no = sub.rn 
      FROM  
         (SELECT personal_code,help_no, row_number() OVER (PARTITION BY personal_code order by personal_code,help_no) AS rn 
            FROM HELP_T WHERE (personal_code IS NOT NULL) and (help_no IS NOT NULL)) sub 
         WHERE  m.personal_code = sub.personal_code and m.help_no=sub.help_no
*/
    translateError(err) {
        for (var i = 0; i < kode_error.length;i++) {
            if (kode_error[i].kod==err.code) {
                return {code: kode_error[i].cod, message:kode_error[i].text};     
            }
        }
    }

    
    /**
     * Функция проверки прав доступа пользователя к данным. Доступ дается только администраторам.
     * @param {Object} user - пользователь, доступ которого нужно проверить
     */
    async testAccess(user) {
        this.oper_user = user;              
        if ((user.isadmin !== true)&&(user.usertype!==3)) {
            return {gres: false, text: 'Доступ запрещен.'};
        }        
        return {gres: true, text: 'Ok'};
    }
    
}

module.exports = HELP_T;
