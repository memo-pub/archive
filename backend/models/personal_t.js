var ChainedModel = require('../slib/chainedmodel');
var NAT_T = require('./nat_t');
var cntc = require('../db').connection;
var LIST_OF_T = require('./list_of_t');

class PERSONAL_T extends ChainedModel {
    constructor() {
        super();
        this.deleteField('id');
        this.addField('code', { tp: 'BIGSERIAL', visible: true }); // Первичный ключ
        this.addUniqueIndex(['code']);
        this._pk = 'code';

        this.addField('mark', {
            tp: "VARCHAR(1) CHECK (MARK IN ('*',' '))",
            visible: true,
        });

        this.addField('type_r', {
            tp: "VARCHAR(1) CHECK (type_r IN ('*',' '))",
            visible: true,
        }); //type_r: 'репрессированный'
        this.addField('type_i', {
            tp: "VARCHAR(1) CHECK (type_i IN ('*',' '))",
            visible: true,
        }); // type_i:'информант'
        this.addField('type_a', {
            tp: "VARCHAR(1) CHECK (type_a IN ('*',' '))",
            visible: true,
        }); // type_a:'пособник'

        this.addField('published', {
            tp: "VARCHAR(1) CHECK (published IN ('*',' '))",
            visible: true,
        }); //published: 'персоналия опубликована'
        this.addField('mustPublish', {
            tp: "VARCHAR(1) CHECK (mustPublish IN ('*',' '))",
            visible: true,
        }); // сущность должна опубликоваться
        this.addField('mustUnpublish', {
            tp: "VARCHAR(1) CHECK (mustUnpublish IN ('*',' '))",
            visible: true,
        }); // сущность должна удалиться из публичной базы
        this.addField('pub_change_date', {
            tp: 'timestamp with time zone DEFAULT current_timestamp',
            visible: true,
        }); // дата обновления статуса публикации

        this.addField('surname', { tp: 'VARCHAR(50)', visible: true });
        this.addField('fname', { tp: 'VARCHAR(40)', visible: true });
        this.addField('lname', { tp: 'VARCHAR(40)', visible: true });
        this.addField('file_no_ext', {
            tp: "VARCHAR(10) NOT NULL DEFAULT ''",
            visible: true,
        }); //
        this.addField('sex', {
            tp: "VARCHAR(1) CHECK (sex IN ('М', 'Ж', '?'))",
            visible: true,
        }); // исправить проверяемые значения

        this.addField('surname_rmk', { tp: 'VARCHAR(500)', visible: true });
        this.addField('public_rmk', { tp: 'VARCHAR(500)', visible: true });
        /*
            ALTER TABLE PERSONAL_T ALTER COLUMN surname_rmk TYPE varchar(500);
        */

        this.addField('fname_rmk', { tp: 'VARCHAR(240)', visible: true });
        this.addField('lname_rmk', { tp: 'VARCHAR(240)', visible: true });

        this.addField('birth', { tp: 'DATE', visible: true });
        this.addField('birth_begin', { tp: 'DATE', visible: true });
        this.addField('birth_end', { tp: 'DATE', visible: true });

        //this.addField('birth_place',{tp: 'VARCHAR(18)',visible: true});

        //this.addField('nation',{tp: 'VARCHAR(12)',visible: false});
        this.addReferenceToClass(
            'nation_id',
            'BIGINT',
            NAT_T,
            'id',
            'RESTRICT',
            true,
            false,
        );

        this.addField('citiz', { tp: 'VARCHAR(20)', visible: true });
        //this.addField('origin',{tp: 'VARCHAR(7)',visible: true});
        this.addReferenceToClass(
            'origin_id',
            'BIGINT',
            LIST_OF_T,
            'id',
            'RESTRICT',
            true,
            false,
        ); // Ссылка на list_of_t

        ///this.addField('educ',{tp: 'VARCHAR(20)',visible: true});
        this.addReferenceToClass(
            'educ_id',
            'BIGINT',
            LIST_OF_T,
            'id',
            'RESTRICT',
            true,
            false,
        ); // Ссылка на list_of_t
        // SET NULL - поле заполняется NULL если удаляется запись на которую ссылается это ПОЛЕ
        //true,false - где false обозначает что ПОЛЕ необязательно заполнять поле при вставке ЗАПИСИ
        // а true - поле видимое - возвращает начение в запросе GET из POSTMAN
        //this.addField('spec',{tp: 'VARCHAR(30)',visible: true});
        this.addReferenceToClass(
            'spec_id',
            'BIGINT',
            LIST_OF_T,
            'id',
            'RESTRICT',
            true,
            false,
        ); // Ссылка на list_of_t

        this.addReferenceToClass(
            'mentioned_person',
            'BIGINT',
            PERSONAL_T,
            'code',
            'SET NULL',
            true,
            false,
        );
        // Ссылка на personal_t visible=true, false - не обязательно заполнять при INSERT

        /*    ALTER TABLE PERSONAL_T 
              ADD COLUMN MENTIONED_PERSON BIGINT REFERENCES PERSONAL_T(CODE) ON DELETE SET NULL*/

        this.addField('death_mark', {
            tp: "VARCHAR(1) CHECK (death_mark IN ('Д', 'Н', '?'))",
            visible: true,
        }); // исправить проверяемые значения
        this.addField('death', { tp: 'DATE', visible: true });
        this.addField('death_begin', { tp: 'DATE', visible: true });
        this.addField('death_end', { tp: 'DATE', visible: true });

        this.addField('birth_rmk', { tp: 'VARCHAR(240)', visible: true });
        this.addField('birth_place_rmk', { tp: 'VARCHAR(240)', visible: true });
        this.addField('nation_rmk', { tp: 'VARCHAR(240)', visible: true });
        this.addField('citiz_rmk', { tp: 'VARCHAR(240)', visible: true });
        this.addField('origin_rmk', { tp: 'VARCHAR(240)', visible: true });
        this.addField('educ_rmk', { tp: 'VARCHAR(240)', visible: true });
        this.addField('spec_rmk', { tp: 'VARCHAR(240)', visible: true });
        this.addField('death_date_rmk', { tp: 'VARCHAR(240)', visible: true });

        this.addField('residence_code', { tp: 'VARCHAR(18)', visible: true });
        this.addField('address', { tp: 'VARCHAR(240)', visible: true });
        this.addField('phone', { tp: 'VARCHAR(20)', visible: true });
        this.addField('url', { tp: 'VARCHAR(500)', visible: true });
        this.addField('url2', { tp: 'VARCHAR(500)', visible: true });
        this.addField('url3', { tp: 'VARCHAR(500)', visible: true });

        /*ALTER TABLE IF EXISTS PERSONAL_T 
          ADD COLUMN IF NOT EXISTS URL varchar(500)*/

        this.addField('residence_code_rmk', {
            tp: 'VARCHAR(240)',
            visible: true,
        });
        this.addField('address_rmk', { tp: 'VARCHAR(240)', visible: true });
        this.addField('phone_rmk', { tp: 'VARCHAR(30)', visible: true });
        this.addField('notice', { tp: 'VARCHAR(240)', visible: true });

        this.addField('us_name', { tp: 'VARCHAR(30)', visible: true });
        this.addField('in_date', { tp: 'DATE', visible: true });

        this.addField('place_num', { tp: 'NUMERIC(8,0)', visible: true });
        this.addField('birth_place_num', { tp: 'NUMERIC(8,0)', visible: true });

        this.addField('death_type', { tp: 'VARCHAR(4)', visible: true });
        this.addField('real_surname_rmk', {
            tp: 'VARCHAR(240)',
            visible: true,
        });

        this.addField('fund', { tp: 'INTEGER', visible: true }); // фонд
        this.addField('list_no', { tp: 'INTEGER', visible: true }); // опись
        this.addField('file_no', { tp: 'INTEGER', visible: true }); //дело

        this.addField('email', { tp: 'VARCHAR(320)', visible: true }); //email
        this.addField('death_place_num', { tp: 'NUMERIC(8,0)', visible: true }); // Ссылка на место
        this.addField('death_place_rmk', { tp: 'VARCHAR(240)', visible: true }); // Комментарий к месту

        /*
            ALTER TABLE PERSONAL_T ADD COLUMN email VARCHAR(320);
            ALTER TABLE PERSONAL_T ADD COLUMN death_place_num NUMERIC(8,0);
            ALTER TABLE PERSONAL_T ADD COLUMN death_place_rmk VARCHAR(240);
        
        */

        this.addOrder('bycode', 't0.code');
        this.addOrder('bycodedesc', 't0.code desc');

        this.addOrder(
            'bysurname',
            'UPPER(t0.surname), UPPER(t0.fname), UPPER(t0.lname)',
        );
        this.addOrder(
            'bysurnamedesc',
            'UPPER(t0.surname) desc, UPPER(t0.fname) desc, UPPER(t0.lname) desc',
        );

        this.addFilter('shortfilter', ['code', 'surname']);
        this.addUserFilter(
            'shortfilter_exact',
            '((UPPER(t0.surname::varchar) like  UPPER({#f_value})) OR (UPPER(t0.code::varchar) like  UPPER({#f_value})))',
        );

        this.addFilter('shortfilter_withrmk', [
            'code',
            'surname',
            'real_surname_rmk',
        ]);
        this.addUserFilter(
            'shortfilter_exact_withrmk',
            '((UPPER(t0.surname::varchar) like  UPPER({#f_value})) OR (UPPER(t0.code::varchar) like  UPPER({#f_value})) OR (UPPER(t0.real_surname_rmk::varchar) like  UPPER({#f_value})))',
        );

        this.addOrder('byshortfilter', 't0.code , UPPER(t0.surname)');

        this.addOrder(
            'bydelo',
            't0.fund, t0.list_no, t0.file_no, t0.file_no_ext',
        );
        this.addOrder(
            'bydelodesc',
            't0.fund desc NULLS LAST, t0.list_no desc NULLS LAST, t0.file_no desc NULLS LAST, t0.file_no_ext desc NULLS LAST',
        );
        this.addUserFilter('tree', 'true');

        this.addReference('ment_pers_data', {
            table: 'personal_t',
            rule: '(t0.mentioned_person={#t_self}.code)',
            replace: ` json_build_object ('code',{#t_self}.code,
            'surname', {#t_self}.surname, 'fname', {#t_self}.fname, 'lname', {#t_self}.lname,
            'birth_begin',{#t_self}.birth_begin, 'birth_end',  {#t_self}.birth_end,
            'birth', {#t_self}.birth, 'birth_rmk', {#t_self}.birth_rmk, 'type_r', {#t_self}.type_r, 'type_a', {#t_self}.type_a,
            'fund', {#t_self}.fund, 'list_no', {#t_self}.list_no, 'file_no',{#t_self}.file_no, 'file_no_ext',{#t_self}.file_no_ext) as ment_p`,
        });

        this.addReference('person_shifr', {
            table: `(select personal_code,json_agg(
                json_build_object(
                'id',id,
                'fund',fund,
                'list_no',list_no,
                'file_no',file_no,
                'file_no_ext',file_no_ext)
            ) as extra_shifr from personal_shifr_t
            group by personal_code)`,
            rule: '(t0.code= {#t_self}.personal_code)',
            replace: ' {#t_self}.extra_shifr',
        });

        //this.debugSQL = true;
    }

    async selectOneByIdJSON(trans = cntc) {
        try {
            this.person_shifr = true;
            let data = await super.selectOneByIdJSON(trans);
            if (data !== null && this.tree !== undefined) {
                //  задать в POSTMAN поле tree=true иначе блок не выполнится
                let inform_t = new INFORM_T();
                inform_t.personal_code = data.code;
                inform_t.personal_t = true;
                data.inform_t = await inform_t.selectAllJSON(trans);

                let family_t = new FAMILY_T();
                family_t.personal_code = data.code;
                family_t.personal_t = true;
                data.family_t = await family_t.selectAllJSON(trans);

                let mentioned_t = new MENTIONED_T();
                mentioned_t.personal_code = data.code;
                mentioned_t.personal_t = true;
                data.mentioned_t = await mentioned_t.selectAllJSON(trans);

                let activity_t = new Activity_t();
                activity_t.personal_code = data.code;
                activity_t._orderby = 'bypos';
                data.activity_t = await activity_t.selectAllJSON(trans);

                let org_t = new Org_t();
                org_t.personal_code = data.code;
                org_t.particip_data = true;
                org_t._orderby = 'bypos';
                data.org_t = await org_t.selectAllJSON(trans);

                let repress_t = new Repress_t();
                repress_t.personal_code = data.code;
                repress_t._orderby = 'bypos';
                data.repress_t = await repress_t.selectAllJSON(trans);

                let help_t = new Help_t();
                help_t.personal_code = data.code;
                help_t._orderby = 'bypos';
                help_t.help_typ_data = true;
                data.help_t = await help_t.selectAllJSON(trans);

                /* let sour_t = new Sour_t();
                sour_t.personal_code = data.code;
                sour_t._orderby='bypos';
                sour_t.filecnt=true;
                data.sour_t = await sour_t.selectAllJSON(trans);*/

                let person_sour_t = new Personal_Sour_t();
                person_sour_t.personal_code = data.code;
                person_sour_t._orderby = 'bysour_no';
                person_sour_t.sour_dan = true;
                person_sour_t.sour_type_join = true;
                person_sour_t.filecnt = true;
                person_sour_t.extra_person = true;
                //person_sour_t.debugSQL=true;
                data.sour_t = await person_sour_t.selectAllJSON(trans);

                let mentioned = new Mentioned();
                mentioned.code = data.code;
                data.mentioned = await mentioned.selectAllJSON(trans);

                let logstr = `select  t0.action_time, json_agg(t0.subobject_table) as subobject_table,
                                    t1.login as user_login, t1.name as user_name, t1.surname as user_surname from mlog t0
                                left outer join muser t1
                                    on t0.muser_id=t1.id
                                where t0.object_table='personal_t' and t0.object_id=$1
                                group by t1.login, t0.action_time, t1.name, t1.surname
                                order by t0.action_time desc`;
                data.mlog = await trans.tx(async (t) => {
                    // tx - это транзакция
                    let data_log = await t.any(logstr, [data.code]);
                    return data_log;
                });
            }
            return data;
        } catch (err) {
            throw { code: err.code, message: err.message };
        }
    }

    /**
     * Функция проверки прав доступа пользователя к данным. Доступ дается только администраторам.
     * @param {Object} user - пользователь, доступ которого нужно проверить
     */
    async testAccess(user) {
        this.oper_user = user;
        if (user.isadmin !== true && user.usertype !== 3) {
            return { gres: false, text: 'Доступ запрещен.' };
        }
        if (this.file_no_ext == null) this.file_no_ext = '';
        return { gres: true, text: 'Ok' };
    }

    isInteger(str) {
        return /^\d+$/.test(str);
    }

    async getAllGuard() {
        if (
            this.shortfilter !== undefined &&
            this._orderby === 'byshortfilter'
        ) {
            let index = 1;
            if (
                this.wheres_default !== undefined &&
                this.wheres_default.script
            ) {
                index = this.wheres_default.values.length + 1;
            }
            let wheres = this.getStaticWheres(index);
            let i = wheres.inext;
            for (var f in this._filters) {
                if (f === 'shortfilter') {
                    index = i;
                }
                if (this[f] !== undefined) {
                    i++;
                }
            }
            if (this.isInteger(this.shortfilter)) {
                this._orders[this._orderby] =
                    'position($' + index + ' in UPPER(code::varchar)), code';
            } else {
                this._orders[this._orderby] =
                    'position($' + index + ' in UPPER(surname)), surname';
            }
        }
        return { gres: true, text: 'Ok' };
    }
}

module.exports = PERSONAL_T;

//var  PERSONAL_SHIFR_T= require('./personal_shifr_t');
var INFORM_T = require('./inform_t');
var FAMILY_T = require('./family_t');
var MENTIONED_T = require('./mentioned_t');

var Activity_t = require('./activity_t');
var Org_t = require('./org_t');
var Repress_t = require('./repress_t');
var Help_t = require('./help_t');
//var Sour_t = require('./sour_t');
var Personal_Sour_t = require('./personal_sour_t');

var Mentioned = require('../viewmodels/mentioned');
