const jwt = require('jsonwebtoken');
const Model = require('../slib/model');

const { connection: cntc } = require('../db');
const config = require('../config');
const Opis = require('./opis');

const jwtOptions = {};
jwtOptions.secretOrKey = config.jwt.secretOrKey + 'file';
jwtOptions.ignoreExpiration = false;
// 2019.07.10
class OpisFile extends Model {
    constructor() {
        super();
        this.addField('opisfile', { tp: 'JSON', visible: true, required: true, isfile: true });
        this.addReferenceToClass('opis_id', 'BIGINT', Opis, 'id', 'CASCADE', true, true);
        this.addField('creation_datetime', {
            tp: 'TIMESTAMP DEFAULT current_timestamp',
            visible: true,
            required: false,
        });
        this.addOrder('byid', 'id');
        this.addOrder('byiddesc', 'id desc');
        this.addOrder('bycrtime', 'creation_datetime');
        this.addOrder('bycrtimedesc', 'creation_datetime desc');
        this.addOrder('byfilename', "opisfile->>'filename'");
        this.addOrder('byfilenamedesc', "opisfile->>'filename' desc");
        this.addFilter('filter_filename', ["opisfile->>'filename'"]);
        this.addUserFilter(
            'filter_pictures',
            `(
                (UPPER(opisfile ->> 'filename') like '%.JPG')
                OR (UPPER(opisfile ->> 'filename') like '%.JPEG')
                OR (UPPER(opisfile ->> 'filename') like '%.PNG')
                OR (UPPER(opisfile ->> 'filename') like '%.BMP')
            )`,
        );

        this.addInputField('getjwt', true);
    }

    /**
     * Функция проверки прав доступа пользователя к данным. Доступ дается только администраторам.
     * @param {Object} user - пользователь, доступ которого нужно проверить
     */
    async testAccess(user) {
        this.creation_datetime = undefined;
        this.oper_user = user;

        if (
            user.isadmin !== true &&
            user.usertype !== 2 &&
            user.usertype !== 1 &&
            user.usertype !== 3
        ) {
            return { gres: false, text: 'Доступ запрещен.' };
        }

        return { gres: true, text: 'Ok' };
    }

    async selectAllJSON(selecttype = 0, trans = cntc) {
        const data = await super.selectAllJSON(selecttype, trans);

        if (data !== null && this.getjwt == 'true') {
            for (let d of data) {
                d.jwt = jwt.sign({ id: d.id, type: 'opisfile' }, jwtOptions.secretOrKey, {
                    expiresIn: '2d',
                });
            }
        }

        return data;
    }
}

module.exports = OpisFile;
