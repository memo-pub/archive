var ChainedModel = require('../slib/chainedmodel');
var PERSONAL_T = require('./personal_t');
var LIST_OF_T = require('./list_of_t');


var kode_error=[{kod:23505,cod: 409,text:' Номер репрессии уже занят!'},
    {kod:23503,cod: 500,text:'Отсутствует Внешний ключ в таблице PERSONAL_T !'}];

//Название таблицы: Репрессия 

class REPRESS_T extends ChainedModel {
    constructor() {
        super();
        this.addReferenceToClass('personal_code','BIGINT NOT NULL',PERSONAL_T,'code', 'RESTRICT', true, true); // Ссылка на personal_t
        this.addField('repress_no',{tp: 'NUMERIC(4,1) NOT NULL',visible: true, required: true}); // номер репрессии
        //this.addField('repress_type',{tp: 'VARCHAR(30)',visible: true}); // Тип репрессии

        this.addReferenceToClass('repress_type_id','BIGINT',LIST_OF_T,'id', 'RESTRICT', true, false); // Ссылка на list_of_t 


        this.addField('court_mark',{tp: 'VARCHAR(1) CHECK (court_mark IN (\'*\'))',visible: true}); // Был ли суд
        this.addField('repress_dat',{tp: 'DATE',visible: true}); // Дата репрессии
        this.addField('repress_dat_begin',{tp: 'DATE',visible: true});
        this.addField('repress_dat_end',{tp: 'DATE',visible: true});
        //this.addField('geoplace_code',{tp: 'VARCHAR(18)',visible: true}); // Код места жит. В момент репрессии
        this.addField('reabil_mark',{tp: 'VARCHAR(1) CHECK (reabil_mark IN (\'*\'))',visible: true}); // Была ли реабилитация
        this.addField('reabil_dat',{tp: 'DATE',visible: true}); // Дата реабилитации
        this.addField('reabil_dat_begin',{tp: 'DATE',visible: true});
        this.addField('reabil_dat_end',{tp: 'DATE',visible: true});
        this.addField('repress_type_rmk',{tp: 'VARCHAR(240)',visible: true}); // Комментарий к типу репрессии
        this.addField('repress_dat_rmk',{tp: 'VARCHAR(240)',visible: true}); // Комментарий к дате репрессии
        this.addField('geoplace_code_rmk',{tp: 'VARCHAR(240)',visible: true}); // Комментарий к месту жительства
        this.addField('reabil_rmk',{tp: 'VARCHAR(240)',visible: true}); // Комментарий к реабилитации
        this.addField('repress_rmk',{tp: 'VARCHAR(240)',visible: true}); // Комментарий к репрессии
        this.addField('place_num',{tp: 'NUMERIC(8,0)',visible: true}); // place_num из place_t
        this.addUniqueIndex(['personal_code','repress_no']);      
        this.addOrder('byid','t0.id');

        this.addOrder('bypos','t0.personal_code, t0.repress_no');
  
        this.addField('rehabil_org',{tp: 'VARCHAR(240)',visible: true}); // Реабилитирующий орган
        //this.addReferenceToClass('rehabil_reason_id','BIGINT',LIST_OF_T,'id', 'RESTRICT', true, false); //  Реабилитационное основание Ссылка на list_of_t 
        //связь Реабилитационное основание идет через rehabil_reason_t
        this.objects_chain = [PERSONAL_T];

    } 
    
    /*  UPDATE REPRESS_T m 
            SET repress_no = sub.rn 
        FROM  
         (SELECT personal_code,repress_no, row_number() OVER (PARTITION BY personal_code order by personal_code,repress_no) AS rn 
            FROM REPRESS_T WHERE (personal_code IS NOT NULL) and (repress_no IS NOT NULL)) sub 
         WHERE  m.personal_code = sub.personal_code and m.repress_no=sub.repress_no
*/

    /*   translateError(err) {
        if (err.code==23505) {
            if (err.message.indexOf('personal_code_act_no_activity_t')>-1) {
                return {code: 409, message:'Номер учебы/работы занят!'};
            }
        }
    } */
    translateError(err) {
        for (var i = 0; i < kode_error.length;i++) {
            if (kode_error[i].kod==err.code) {
                return {code: kode_error[i].cod, message:kode_error[i].text};     
            }
        }
    }

    
    /**
     * Функция проверки прав доступа пользователя к данным. Доступ дается только администраторам.
     * @param {Object} user - пользователь, доступ которого нужно проверить
     */
    async testAccess(user) {
        this.oper_user = user;              
        if ((user.isadmin !== true)&&(user.usertype!==3)) {
            return {gres: false, text: 'Доступ запрещен.'};
        }        
        return {gres: true, text: 'Ok'};
    }
    
}

module.exports = REPRESS_T;
