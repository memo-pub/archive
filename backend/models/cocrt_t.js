var ChainedModel = require('../slib/chainedmodel');
var PERSONAL_T = require('./personal_t');
var COURT_T = require('./court_t');
var kode_error=[{kod:23505,cod: 409,text:'Данные о человеке уже существуют!'},
    {kod:23503,cod: 500,text:'Отсутствует Внешний ключ !'}];

//Название таблицы: Лица, упоминаемые в связи с репрессией

class COCRT_T extends ChainedModel {
    constructor() {
        super();
        this.addReferenceToClass('crt_id','BIGINT NOT NULL',COURT_T,'id', 'CASCADE', true, true); //ссылка на COURT_T
        this.addReferenceToClass('human_code','BIGINT NOT NULL',PERSONAL_T,'code', 'CASCADE', true, true); // Ссылка на personal_t 
        this.addField('role',{tp: 'VARCHAR(30)',visible: true}); // роль человека;
        this.addField('role_rmk',{tp: 'VARCHAR(240)',visible: true}); // комментарий к роли
        //this.addField('sign_mark',{tp: 'VARCHAR(1) CHECK (sign_mark IN (\'*\'))',visible: true}); // Важное упоминание
        // this.addField('personal_code',{tp: 'BIGINT',visible: false}); // код человека
        //this.addField('repress_no',{tp: 'NUMERIC(2,0)',visible: false}); // номер репрессии
        //this.addField('trial_no',{tp: 'NUMERIC(2,0)',visible: false}); // номер суда
        this.addUniqueIndex(['crt_id','human_code']);
        this.addOrder('byid','t0.id');
        
        this.objects_chain = [PERSONAL_T, REPRESS_T, COURT_T];
        this.addReference('personal_t', {table:'personal_t',
            rule: '(t0.human_code={#t_self}.code)', replace:`{#t_self}.code, {#t_self}.surname, {#t_self}.fname, {#t_self}.lname,
            {#t_self}.birth, {#t_self}.birth_begin, {#t_self}.birth_end, {#t_self}.birth_rmk, {#t_self}.type_r,{#t_self}.type_a,{#t_self}.fund, {#t_self}.list_no, {#t_self}.file_no, {#t_self}.file_no_ext`});
    }            
      
    translateError(err) {
        for (var i = 0; i < kode_error.length;i++) {
            if (kode_error[i].kod==err.code) {
                return {code: kode_error[i].cod, message:kode_error[i].text};     
            }
        }
    }

    
    /**
     * Функция проверки прав доступа пользователя к данным. Доступ дается только администраторам.
     * @param {Object} user - пользователь, доступ которого нужно проверить
     */
    async testAccess(user) {
        this.oper_user = user;              
        if ((user.isadmin !== true)&&(user.usertype!==3)) {
            return {gres: false, text: 'Доступ запрещен.'};
        }        
        return {gres: true, text: 'Ok'};
    }
    
}

module.exports = COCRT_T;

var REPRESS_T = require('./repress_t');

