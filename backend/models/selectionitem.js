var Model = require('../slib/model');
var Selection = require('./selection');
var PERSONAL_T = require('./personal_t');

class SelectionItem extends Model {
    constructor() {
        super();
        this.addReferenceToClass('selection_id','BIGINT NOT NULL',Selection,'id', 'CASCADE', true, true); // Ссылка на Selection
        this.addReferenceToClass('personal_code','BIGINT NOT NULL',PERSONAL_T,'code', 'CASCADE', true, true); // Ссылка на personal_t
        
        this.addUniqueIndex(['selection_id','personal_code']);
        this.addReference('info', {table:'personal_t',rule: '(t0.personal_code={#t_self}.code)',
            replace:`{#t_self}.surname, {#t_self}.fname, {#t_self}.lname, {#t_self}.birth,  {#t_self}.birth_begin, {#t_self}.birth_end, 
                {#t_self}.birth_rmk, {#t_self}.fund , {#t_self}.list_no, {#t_self}.file_no, {#t_self}.file_no_ext, {#t_self}.surname_rmk`,
            type: 'INNER'});


        /*this.addReference('src_cnt', {table:`(select personal_code, COUNT(id) as sour_cnt from sour_t w
                            group by personal_code)`,rule: '(t0.personal_code={#t_self}.personal_code)',
        replace:' {#t_self}.sour_cnt'});*/
        
        this.addReference('src_cnt', {table:`(select personal_code, COUNT(id) as sour_cnt from personal_sour_t w
        group by personal_code)`,rule: '(t0.personal_code={#t_self}.personal_code)',
        replace:' {#t_self}.sour_cnt'});
        

        
        this.addOrder('bycode','t0.personal_code');
        this.addOrder('bycodedesc','t0.personal_code desc');

        this.addOrder('bysour_cnt','sour_cnt');
        this.addOrder('bysour_cntdesc','sour_cnt desc');

        this.addOrder('bysurname','t1.surname, t1.fname');
        this.addOrder('bysurnamedesc','t1.surname desc, t1.fname desc');

        this.addOrder('bydelo','t1.fund, t1.list_no, t1.file_no, t1.file_no_ext');
        this.addOrder('bydelodesc','t1.fund desc NULLS LAST, t1.list_no desc NULLS LAST, t1.file_no desc NULLS LAST, t1.file_no_ext desc NULLS LAST');

        this.addFilter('shortfilter',['personal_code','t1.surname','t1.fname','t1.lname']); 
        this.addUserFilter('shortfilter_exact',
            `((UPPER(t1.surname::varchar) like  UPPER({#f_value})) OR (UPPER(t1.code::varchar) like  UPPER({#f_value}))
                OR (UPPER(t1.fname::varchar) like  UPPER({#f_value})) OR (UPPER(t1.lname::varchar) like  UPPER({#f_value})))`);


        this.addUserFilter('fund','t1.fund = {#f_value}');
        this.addUserFilter('list_no','t1.list_no = {#f_value}');
        this.addUserFilter('file_no','t1.file_no = {#f_value}');
        
    }            

    translateError(err) {
        if (err.code==23505) {
            if (err.message.indexOf('selection_id_personal_code_selectionitem')>-1) {
                return {code: 409, message:'Персоналия уже есть в данной выборке!'};
            }
        }
        else if (err.code==23503) {
            if (err.message.indexOf('selectionitem_personal_code_fkey')>-1) {
                return {code: 409, message:'Выбранная персоналия не существует в базе данных!'};
            }
            else if (err.message.indexOf('selectionitem_selection_id_fkey')>-1) {
                return {code: 409, message:'Выбранная выборка не существует в базе данных!'};
            }
        }
    }

    
    /**
     * Функция проверки прав доступа пользователя к данным. Доступ дается только администраторам.
     * @param {Object} user - пользователь, доступ которого нужно проверить
     */
    async testAccess(user) {
        this.oper_user = user;              
        if ((user.isadmin !== true)&&((user.usertype!==3) && (user.usertype!==4))) {
            return {gres: false, text: 'Доступ запрещен.'};
        }        
        return {gres: true, text: 'Ok'};
    }

    async postGuard (user) {  
        if (user.usertype===4)  {
            let selection = new Selection();
            selection.id = this.selection_id;
            let sl = await selection.selectOneByIdJSON();
            if (sl==null) {
                return {gres: false, text: 'Не найдена выборка для вставки.'};
            }
            if (sl.owner_id != user.id) {
                return {gres: false, text: 'Исследователь не может делать вставку в чужую выборку.'};
            }

        }                   
        return await this.testAccess(user);
    }

    async getAllGuard(user) {
        this.src_cnt = this.info;
        this.debugSQL=true;
        return await super.getAllGuard(user);
    }
    
}

module.exports = SelectionItem;
