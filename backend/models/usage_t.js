var Model = require('../slib/model');
var ArchiveFolder = require('./archivefolder');
var LIST_OF_T = require('./list_of_t');
var kode_error=[{kod:23505,cod: 409,text:'Номер использования уже занят!'},
    {kod:23503,cod: 500,text:'Отсутствует Внешний ключ в!'}];

//Название таблицы: Использование персонального дела

class USAGE_T extends Model {
    constructor() {
        super();
        //this.addReferenceToClass('personal_code','BIGINT NOT NULL',PERSONAL_T,'code', 'CASCADE', true, true); // Ссылка на personal_t
        this.addReferenceToClass('archivefolder_id','BIGINT NOT NULL',ArchiveFolder,'id', 'CASCADE', true, true);
        /*
        DELETE FROM USAGE_T;

        ALTER TABLE USAGE_T
           ADD COLUMN archivefolder_id BIGINT NOT NULL REFERENCES ArchiveFolder(ID) ON DELETE CASCADE;
        */


        this.addReferenceToClass('us_type_id','BIGINT',LIST_OF_T,'id', 'RESTRICT', true, false);

        /* ALTER TABLE USAGE_T
           ADD COLUMN US_TYPE_ID BIGINT REFERENCES LIST_OF_T(ID) ON DELETE RESTRICT   */ 
           
        this.addField('us_no',{tp: 'NUMERIC(4,1) NOT NULL',visible: true, required: true}); //номер использования
        //this.addField('us_type',{tp: 'VARCHAR(80)',visible: true}); // Тип использования
        this.addField('us_date',{tp: 'DATE',visible: true}); // Дата использования
        this.addField('us_date_begin',{tp: 'DATE',visible: true});
        this.addField('us_date_end',{tp: 'DATE',visible: true});
        this.addField('sour_code',{tp: 'VARCHAR(80)',visible: true}); // Шифр используемых документов
        this.addField('sour_txt',{tp: 'VARCHAR(240)',visible: true}); //Пометки о состоянии дела 
        this.addField('state_type',{tp: 'VARCHAR(4)',visible: true}); // Код состояния дела
        this.addUniqueIndex(['archivefolder_id','us_no']);      
        this.addOrder('byid','t0.id');

        this.addOrder('bypos','t0.personal_code, t0.us_no');

    }            

    translateError(err) {
        for (var i = 0; i < kode_error.length;i++) {
            if (kode_error[i].kod==err.code) {
                return {code: kode_error[i].cod, message:kode_error[i].text};     
            }
        }
    }

    /*  UPDATE USAGE_T m 
            SET us_no = sub.rn 
        FROM  
         (SELECT personal_code,us_no, row_number() OVER (PARTITION BY personal_code order by personal_code,us_no) AS rn 
            FROM USAGE_T WHERE (personal_code IS NOT NULL) and (us_no IS NOT NULL)) sub 
         WHERE  m.personal_code = sub.personal_code and m.us_no=sub.us_no
*/

    /**
     * Функция проверки прав доступа пользователя к данным. Доступ дается только администраторам.
     * @param {Object} user - пользователь, доступ которого нужно проверить
     */
    async testAccess(user) {
        this.oper_user = user;              
        if ((user.isadmin !== true)&&(user.usertype!==3)&&(user.usertype!==1)) {
            return {gres: false, text: 'Доступ запрещен.'};
        }        
        return {gres: true, text: 'Ok'};
    }
    
}

module.exports = USAGE_T;
