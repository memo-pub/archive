var ChainedModel = require('../slib/chainedmodel');
var PERSONAL_T = require('./personal_t');
var REPRESS_T = require('./repress_t');
var kode_error=[{kod:23505,cod: 409,text:'Номер судебного процесса занят!'},
    {kod:23503,cod: 500,text:'Отсутствует Внешний ключ в таблице PERSONAL_T !'}];


//Название таблицы: Следствие и суд

class COURT_T extends ChainedModel {
    constructor() {
        super();
        this.addReferenceToClass('personal_code','BIGINT NOT NULL',PERSONAL_T,'code', 'RESTRICT', true, true); // не используется
        this.addReferenceToClass('rep_id','BIGINT NOT NULL',REPRESS_T,'id', 'CASCADE', true, true); // ссылка на REPRESS_T
        //this.addField('repress_no',{tp: 'NUMERIC(2,0)',visible: false}); //
        this.addField('trial_no',{tp: 'NUMERIC(4,1) NOT NULL',visible: true}); // Номер судебного процесса
        this.addField('inq_name',{tp: 'VARCHAR(60)',visible: true}); // Следственный орган
        this.addField('inq_date',{tp: 'DATE',visible: true}); //Дата начала следствия
        this.addField('inq_date_begin',{tp: 'DATE',visible: true});
        this.addField('inq_date_end',{tp: 'DATE',visible: true});
        this.addField('court_name',{tp: 'VARCHAR(60)',visible: true}); //Судебный орган
        this.addField('sentense_date',{tp: 'DATE',visible: true}); //Дата вынесения приговора
        this.addField('sentense_date_begin',{tp: 'DATE',visible: true});
        this.addField('sentense_date_end',{tp: 'DATE',visible: true});
        this.addField('article',{tp: 'VARCHAR(100)',visible: true}); //Статья
        /*
        ALTER TABLE COURT_T ALTER article TYPE varchar(100);
        */
        this.addField('sentense',{tp: 'VARCHAR(50)',visible: true}); //Приговор
        this.addField('inq_name_rmk',{tp: 'VARCHAR(240)',visible: true}); //Комментарий к следственному органу
        this.addField('inq_date_rmk',{tp: 'VARCHAR(240)',visible: true}); //Комментарий к времени следствия
        this.addField('court_name_rmk',{tp: 'VARCHAR(240)',visible: true}); //Комментарий к судебному органу
        this.addField('trial_rmk',{tp: 'VARCHAR(240)',visible: true}); //Комментарий к судебному процессу
        this.addField('sentense_date_rmk',{tp: 'VARCHAR(240)',visible: true}); //Комментарий к дате приговора
        this.addField('article_rmk',{tp: 'VARCHAR(240)',visible: true}); //Комментарий к статье
        this.addField('sentense_rmk',{tp: 'VARCHAR(240)',visible: true}); //Комментарий к приговору
        this.addField('inq_rmk',{tp: 'VARCHAR(240)',visible: true}); //Комментарий к следствию
        this.addField('place_num_i',{tp: 'NUMERIC(8,0)',visible: true}); //Номер географической единицы места следствия
        this.addField('place_num_j',{tp: 'NUMERIC(8,0)',visible: true}); //Номер географической единицы места суда
        this.addField('place_i_rmk',{tp: 'VARCHAR(240)',visible: true}); //Комментарий к месту следствия
        this.addField('place_j_rmk',{tp: 'VARCHAR(240)',visible: true}); //Комментарий к месту суда

        
        this.addUniqueIndex(['rep_id','trial_no']);      
        this.addOrder('byid','t0.id');
        this.addOrder('bypos','t0.rep_id, t0.trial_no');

        this.objects_chain = [PERSONAL_T, REPRESS_T];
  
    }            
  
    /*  UPDATE COURT_T m 
     SET trial_no = sub.rn 
      FROM  
         (SELECT rep_id,trial_no, row_number() OVER (PARTITION BY rep_id order by rep_id,trial_no) AS rn 
            FROM COURT_T WHERE (rep_id IS NOT NULL) and (trial_no IS NOT NULL)) sub 
         WHERE  m.rep_id = sub.rep_id and m.trial_no=sub.trial_no
*/
    /*   translateError(err) {
        if (err.code==23505) {
            if (err.message.indexOf('personal_code_act_no_activity_t')>-1) {
                return {code: 409, message:'Номер учебы/работы занят!'};
            }
        }
    } */
    translateError(err) {
        for (var i = 0; i < kode_error.length;i++) {
            if (kode_error[i].kod==err.code) {
                return {code: kode_error[i].cod, message:kode_error[i].text};     
            }
        }
    }

    
    /**
     * Функция проверки прав доступа пользователя к данным. Доступ дается только администраторам.
     * @param {Object} user - пользователь, доступ которого нужно проверить
     */
    async testAccess(user) {
        this.oper_user = user;              
        if ((user.isadmin !== true)&&(user.usertype!==3)) {
            return {gres: false, text: 'Доступ запрещен.'};
        }        
        return {gres: true, text: 'Ok'};
    }
    
}

module.exports = COURT_T;
