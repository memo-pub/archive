var ChainedModel = require('../slib/chainedmodel');
var PERSONAL_T = require('./personal_t');

class PERSONAL_SHIFR_T extends ChainedModel {
    constructor() {
        super();

        this.addReferenceToClass('personal_code','BIGINT',PERSONAL_T,'code', 'CASCADE', true, true); // Ссылка на personal_t
        //CASCADE - УДАЛЯТЬ ЗАПИСЬ ,ЕСЛИ УДАЛЯЕТСЯ ЗАПИСЬ В PRESONAL_T
        this.addField('fund',{tp: 'INTEGER',visible: true}); // фонд
        this.addField('list_no',{tp: 'INTEGER',visible: true}); // опись
        this.addField('file_no',{tp: 'INTEGER',visible: true}); //дело
        this.addField('file_no_ext',{tp: 'VARCHAR(10) NOT NULL DEFAULT \'\'',visible: true}); // 
        
        this.addOrder('byid','t0.id');

        this.objects_chain = [PERSONAL_T];
    }            
    
    
    /**
     * Функция проверки прав доступа пользователя к данным. Доступ дается только администраторам.
     * @param {Object} user - пользователь, доступ которого нужно проверить
     */
    async testAccess(user) {
        this.oper_user = user;              
        if ((user.isadmin !== true)&&(user.usertype!==3)) {
            return {gres: false, text: 'Доступ запрещен.'};
        }        
        if (this.file_no_ext == null) this.file_no_ext = '';
        return {gres: true, text: 'Ok'};
    }
    
}

module.exports = PERSONAL_SHIFR_T;
