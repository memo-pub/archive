const { connection: cntc } = require('../db');

/**
 * Запускает процесс создания моделей из текущей папки. Создает таблицы для моделей, которых нет в базе данных.
 */
const merge = async () => {
    const normalizedPath = __dirname;
    let existing = [];

    try {
        // Получить список существующих таблиц в базе
        const fromdb = await cntc.manyOrNone(
            "SELECT table_name FROM information_schema.tables WHERE table_schema = 'public'",
        );

        if (fromdb) {
            existing = fromdb.map((val) => val.table_name.toUpperCase());
        }
    } catch (error) {
        console.dir(error);
    }

    const files = require('node:fs').readdirSync(normalizedPath);
    let done = false;

    while (!done) {
        done = true;

        for (let file of files) {
            if (file != 'index.js') {
                const Md = require(`./${file}`);
                const md = new Md();
                const tablename = md.constructor.name.toUpperCase();

                if (!existing.includes(tablename) && md.checkDepends(existing)) {
                    try {
                        await md.merge();
                        existing.push(tablename);
                        done = false;
                        console.log(`${file} - Ok`);
                    } catch (error) {
                        console.dir(error);
                    }
                }
            }
        }
    }
};

const makeModelsLinks = () => {
    const normalizedPath = __dirname;
    const files = require('node:fs').readdirSync(normalizedPath);

    for (let file of files) {
        if (file !== 'index.js') {
            const Md = require(`./${file}`);
            const md = new Md();
            md.addLinks();
        }
    }
};

const populate = async () => {
    try {
        const MUser = require('./muser');
        const au = new MUser();
        const dt1 = new Date();
        const dt0 = new Date('01.01.2018');
        const dt = dt1 - dt0;
        au.name = `test_arch_user${dt}`;
        au.surname = `test_arch_user${dt}`;
        au.login = `arch_${dt}`;
        au.password = 'test_arch_user';
        const { getPwdHash } = require('../getpwdhash');
        au.sk = getPwdHash(au.password);
        au.isadmin = false;
        au.logerModel = undefined;
        au.usertype = 1;
        const au_data = await au.insert();

        const Opis = require('./opis');
        const opis = new Opis();
        opis.logerModel = undefined;
        opis.fond = dt;
        opis.opis_name = 1;
        const opis1 = await opis.insert();
        opis.opis_name = 2;
        const opis2 = await opis.insert();

        let i;
        const ArchiveFolder = require('./archivefolder');
        const af = new ArchiveFolder();
        af.logerModel = undefined;
        af.opis_id = opis1.id;
        af.page_cnt = 5;
        af.spec_page_cnt = 5;
        af.doc_cnt = 5;
        af.foto_cnt = 5;
        af.have_scanned = false;
        af.have_memories = false;
        af.have_arts = false;
        af.have_act = false;
        af.status = 0;
        af.createduser_id = au_data.id;

        for (i = 0; i < 100_000; i++) {
            af.delo = i;
            af.name = `delo ${i}`;
            await af.insert();
        }

        af.opis_id = opis2.id;

        for (i = 0; i < 100_000; i++) {
            af.delo = i;
            af.name = `delo ${i}`;
            await af.insert();
        }
    } catch (error) {
        console.dir(error);
    }
};

module.exports = {
    merge,
    populate,
    makeModelsLinks,
};
