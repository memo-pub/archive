var Model = require('../slib/model');
const { connection: cntc } = require('../db');

class DELETE_PUBLIC_ENTITY extends Model {
    constructor() {
        super();
        this.deleteField('id');
        this.addField(`"legacyId"`, { tp: 'VARCHAR(50)', visible: true });
    }

    /**
     * Функция проверки прав доступа пользователя к данным. Доступ дается только администраторам.
     * @param {Object} user - пользователь, доступ которого нужно проверить
     */
    async testAccess(user) {
        this.oper_user = user;
        if (user.isadmin !== true && user.usertype !== 3) {
            return { gres: false, text: 'Доступ запрещен.' };
        }
        if (this.file_no_ext == null) this.file_no_ext = '';
        return { gres: true, text: 'Ok' };
    }

    async insert(trans = cntc) {
        return await super.insert(trans);
    }
}

module.exports = DELETE_PUBLIC_ENTITY;
