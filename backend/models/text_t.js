var ChainedModel = require('../slib/chainedmodel');
var PERSONAL_T = require('./personal_t');
var kode_error=[{kod:23505,cod: 409,text:'Номер строки текста уже занят!'},
    {kod:23503,cod: 500,text:'Отсутствует Внешний ключ в таблице PERSONAL_T !'}];

//Название таблицы: Текст 

class TEXT_T extends ChainedModel {
    constructor() {
        super();
        this.addReferenceToClass('personal_code','BIGINT NOT NULL',PERSONAL_T,'code', 'CASCADE', true, true); // Ссылка на personal_t
        this.addField('line_no',{tp: 'NUMERIC(2,0) DEFAULT 0',visible: true}); //номер строки текста
        this.addField('text_line',{tp: 'VARCHAR(1000)',visible: true}); // строка текста
        // this.addUniqueIndex(['personal_code','line_no']);      
        this.addOrder('byid','t0.id');

        this.objects_chain = [PERSONAL_T];
    }            

    translateError(err) {
        for (var i = 0; i < kode_error.length;i++) {
            if (kode_error[i].kod==err.code) {
                return {code: kode_error[i].cod, message:kode_error[i].text};     
            }
        }
    }

    
    /**
     * Функция проверки прав доступа пользователя к данным. Доступ дается только администраторам.
     * @param {Object} user - пользователь, доступ которого нужно проверить
     */
    async testAccess(user) {
        this.oper_user = user;              
        if ((user.isadmin !== true)&&(user.usertype!==3)) {
            return {gres: false, text: 'Доступ запрещен.'};
        }        
        return {gres: true, text: 'Ok'};
    }
    
}

module.exports = TEXT_T;
