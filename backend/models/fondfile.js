const jwt = require('jsonwebtoken');
const Model = require('../slib/model');

const { connection: cntc } = require('../db');
const config = require('../config');
const Fond = require('./fond');

const jwtOptions = {};
jwtOptions.secretOrKey = config.jwt.secretOrKey + 'file';
jwtOptions.ignoreExpiration = false;

// 2021.07.25
class FondFile extends Model {
    constructor() {
        super();
        this.addField('fondfile', { tp: 'JSON', visible: true, required: true, isfile: true });
        this.addReferenceToClass('fond_id', 'BIGINT', Fond, 'id', 'CASCADE', true, true);
        this.addField('creation_datetime', {
            tp: 'TIMESTAMP DEFAULT current_timestamp',
            visible: true,
            required: false,
        });
        this.addOrder('byid', 'id');
        this.addOrder('byiddesc', 'id desc');
        this.addOrder('bycrtime', 'creation_datetime');
        this.addOrder('bycrtimedesc', 'creation_datetime desc');
        this.addOrder('byfilename', "fondfile->>'filename'");
        this.addOrder('byfilenamedesc', "fondfile->>'filename' desc");
        this.addFilter('filter_filename', ["fodfile->>'filename'"]);
        this.addUserFilter(
            'filter_pictures',
            "((UPPER(fondfile->>'filename') like '%.JPG') OR (UPPER(fondfile->>'filename') like '%.JPEG') OR (UPPER(fondfile->>'filename') like '%.PNG') OR (UPPER(fondfile->>'filename') like '%.BMP'))",
        );

        this.addInputField('getjwt', true);
    }

    /**
     * Функция проверки прав доступа пользователя к данным. Доступ дается только администраторам.
     * @param {Object} user - пользователь, доступ которого нужно проверить
     */
    async testAccess(user) {
        this.creation_datetime = undefined;
        this.oper_user = user;

        if (
            user.isadmin !== true &&
            user.usertype !== 2 &&
            user.usertype !== 1 &&
            user.usertype !== 3
        ) {
            return { gres: false, text: 'Доступ запрещен.' };
        }

        return { gres: true, text: 'Ok' };
    }

    async selectAllJSON(selecttype = 0, trans = cntc) {
        let data = await super.selectAllJSON(selecttype, trans);

        if (data !== null && this.getjwt == 'true') {
            for (let d of data) {
                d.jwt = jwt.sign({ id: d.id, type: 'fondfile' }, jwtOptions.secretOrKey, {
                    expiresIn: '2d',
                });
            }
        }

        return data;
    }
}

module.exports = FondFile;
