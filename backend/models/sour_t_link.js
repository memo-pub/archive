var cntc = require('../db').connection;
var Model = require('../slib/model');
var SOUR_T = require('./sour_t');

class SOUR_T_LINK extends Model {
    constructor() {
        super();
        this.addReferenceToClass('sour_t_id1','BIGINT',SOUR_T,'id', 'CASCADE', true, true);
        this.addReferenceToClass('sour_t_id2','BIGINT',SOUR_T,'id', 'CASCADE', true, true);
        // link_type - 1 дубликат
        this.addField('link_type',{tp: 'SMALLINT NOT NULL',visible: true});
        //поле для входного параметра POSTMEN 
        this.addInputField('id_fordelete');
        
        this.addReference('st1', {table:'sour_t',rule: '(t0.sour_t_id1={#t_self}.id)',
            replace:'{#t_self}.sour_name as sour_name1, {#t_self}.fund as fund1, {#t_self}.list_no as list_no1, {#t_self}.file_no as file_no1,  {#t_self}.file_no_ext as file_no_ext1, dat2_to_text({#t_self}.sour_date_begin, {#t_self}.sour_date_end) as sour_date1'});

        this.addReference('st2', {table:'sour_t',rule: '(t0.sour_t_id2={#t_self}.id)',
            replace:'{#t_self}.sour_name as sour_name2, {#t_self}.fund as fund2, {#t_self}.list_no as list_no2, {#t_self}.file_no as file_no2,  {#t_self}.file_no_ext as file_no_ext2, dat2_to_text({#t_self}.sour_date_begin, {#t_self}.sour_date_end) as sour_date2'});
        
        this.addOrder('byid','t0.id');    
        
        this.afterCreate = 'create unique index uniq_sour_t_link on SOUR_T_LINK (least(sour_t_id1, sour_t_id2), greatest(sour_t_id1, sour_t_id2));';
    }  
    
    /**
     * Функция проверки прав доступа пользователя к данным. Доступ дается только администраторам.
     * @param {Object} user - пользователь, доступ которого нужно проверить
     */
    async testAccess(user) {
        this.oper_user = user;        
        if ((user.isadmin !== true)&&(user.usertype!==1)&&(user.usertype!==3)) {
            return {gres: false, text: 'Доступ запрещен.'};
        }        
        return {gres: true, text: 'Ok'};
    }
    
    async insert(trans = cntc) {         
        let selstr2=`with list1 as (
            select sour_t_id1 as id1,sour_t_id2 as id2 from SOUR_T_LINK where link_type=1
            union
            select sour_t_id2 as id1,sour_t_id1 as id2 from SOUR_T_LINK where link_type=1
        ),
        list2 as (
            select distinct t1.id1 as id1,
                case when t1.id2=t2.sour_t_id1 then t2.sour_t_id2 
                else t2.sour_t_id1 
                end as id2
            from list1 t1
            left outer join SOUR_T_LINK t2
                on (t1.id2=t2.sour_t_id1 or t1.id2=t2.sour_t_id2) and t2.link_type=1
        ),
        to_add as (
            select t1.*, 1 as link_type from list2 t1
            left outer join list1 t2
                on t1.id1=t2.id1 and t1.id2=t2.id2
            where t1.id1<>t1.id2 and t2.id1 is null and t1.id1>t1.id2
        )
        insert into SOUR_T_LINK (sour_t_id1, sour_t_id2, link_type)
        (select id1, id2, 1 from to_add) 
        ON CONFLICT DO NOTHING 
        RETURNING SOUR_T_LINK`;
        
        try {
            let values=[this.sour_t_id1,this.sour_t_id2];
            let data = await trans.tx(async t => {   // tx - это транзакция 
                let data = await super.insert(t);
                let data1=await t.any(selstr2,values);
                while (data1[0]!=undefined) {data1=await t.any(selstr2,values);}
                return data;
            });
            return data;  
        }
        catch(err) {
            console.dir(err.message);
            throw {code: err.code, message: err.message};
        
        }
      
    }
    
    async deleteOne(trans = cntc) {
        try { 
            let values=[this.id_fordelete];
            let selstr2='delete from SOUR_T_LINK where (((sour_t_id1=$1) or (sour_t_id2=$1)) and link_type=1)';           
            let data = await trans.tx(async t => {
                let data = await super.deleteOne(t);
                if ((this.id_fordelete!=undefined) && (this.id_fordelete!=null)) 
                {await t.manyOrNone(selstr2,values);}
                return data;                                                
            });        
            return data;
        }    
        catch(err) {                                    
            throw {code: err.code, message: err.message};
        }
    }

}

module.exports = SOUR_T_LINK;