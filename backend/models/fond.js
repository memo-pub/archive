var Model = require('../slib/model');

class Fond extends Model {
    constructor() {
        super();
        this.addField('fond', { tp: 'BIGINT', visible: true, required: true });
        this.addField('description', { tp: 'VARCHAR(500)', visible: true });
        this.addField('description_long', {
            tp: 'VARCHAR(2000)',
            visible: true,
        });
        /*
            ALTER TABLE Fond ADD COLUMN description_long VARCHAR(1000);
         */


        // добавлено строк 5 - 22.04.2020
        this.addField('doc_begin', { tp: 'SMALLINT', visible: true }); //
        this.addField('doc_end', { tp: 'SMALLINT', visible: true }); //даты документов (крайние годы) - два числовых поля
        this.addField('form_date', { tp: 'VARCHAR(100)', visible: true }); //дата формирования (Текст 100 знаков)
        this.addField('access_lim', { tp: 'VARCHAR(500)', visible: true }); //ограничения к доступу (Текст, 500 зн)
        this.addField('help', { tp: 'VARCHAR(1000)', visible: true }); //справочный аппарат (Текст, 500 зн)

        // добавлено строк 8 - 09.06.2023

        this.addField('storage_capacity', {
            tp: 'VARCHAR(200)',
            visible: true,
        }); //Объем и носитель хранения
        this.addField('creators_name', { tp: 'VARCHAR(200)', visible: true }); //Имя создателя (ей)
        this.addField('admin_biog_history', {
            tp: 'VARCHAR(5000)',
            visible: true,
        }); // Административная/Биографическая история
        this.addField('arch_history', { tp: 'VARCHAR(2000)', visible: true }); // Архивная история
        this.addField('donation_source', {
            tp: 'VARCHAR(1000)',
            visible: true,
        }); // Непосредственный источник комплектования или перевод
        this.addField('mat_organ_system', {
            tp: 'VARCHAR(1000)',
            visible: true,
        }); // Система расположения и организации материала
        this.addField('archivist_rmk', { tp: 'VARCHAR(200)', visible: true }); // Примечания архивиста
        this.addField('descrip_date', { tp: 'VARCHAR(200)', visible: true }); // Дата(ы) описания
        /*
        ALTER TABLE IF EXISTS FOND 
          ADD COLUMN IF NOT EXISTS DOC_BEGIN SMALLINT,
		  ADD COLUMN IF NOT EXISTS DOC_END SMALLINT,
		  ADD COLUMN IF NOT EXISTS FORM_DATE varchar(100),
		  ADD COLUMN IF NOT EXISTS ACCESS_LIM varchar(500),
          ADD COLUMN IF NOT EXISTS HELP varchar(500)
          */
        this.addField('published', {
            tp: "VARCHAR(1) CHECK (published IN ('*',' '))",
            visible: true,
        }); //published: 'фонд опубликован'
        this.addField('mustPublish', {
            tp: "VARCHAR(1) CHECK (mustPublish IN ('*',' '))",
            visible: true,
        }); // сущность должна опубликоваться
        this.addField('mustUnpublish', {
            tp: "VARCHAR(1) CHECK (mustUnpublish IN ('*',' '))",
            visible: true,
        }); // сущность должна удалиться из публичной базы
        this.addField('pub_change_date', {
            tp: 'timestamp with time zone DEFAULT current_timestamp',
            visible: true,
        }); // дата обновления статуса публикации
        this.addUniqueIndex(['fond']);

        this.addOrder('byid', 't0.id');
        this.addOrder('byfond', 't0.fond');
        this.addOrder('byfonddesc', 't0.fond desc');

        this.addReference('showfinished', {
            table: `((SELECT  t1.fond, count(t2.*) as cnt FROM opis t1								
                                        JOIN archivefolder t2 ON (t1.id=t2.opis_id)
                                        where (t2.status<3)
                                        group by t1.fond))`,
            rule: '(t0.fond={#t_self}.fond)',
            replace:
                'CASE WHEN {#t_self}.cnt IS NULL or {#t_self}.cnt=0' +
                ' THEN true else false END as finished,CASE WHEN {#t_self}.cnt IS NULL THEN 0 else {#t_self}.cnt END as cnt',
        });
    }

    /**
     * Функция проверки прав доступа пользователя к данным. Доступ дается только администраторам.
     * @param {Object} user - пользователь, доступ которого нужно проверить
     */
    async testAccess(user) {
        this.oper_user = user;
        if (
            user.isadmin !== true &&
            user.usertype !== 1 &&
            user.usertype !== 3
        ) {
            return { gres: false, text: 'Доступ запрещен.' };
        }
        return { gres: true, text: 'Ok' };
    }
}

module.exports = Fond;
