var ChainedModel = require('../slib/chainedmodel');
var PERSONAL_T = require('./personal_t');
var ORG_T = require('./org_t');

class COORG_T extends ChainedModel {
    constructor() {
        super();

        this.addReferenceToClass('org_id','BIGINT NOT NULL',ORG_T,'id', 'CASCADE', true, true); // Ссылка на personal_t
        this.addReferenceToClass('human_code','BIGINT',PERSONAL_T,'code', 'CASCADE', true, true); // Ссылка на personal_t
        //this.addField('personal_code',{tp: 'BIGINT',visible: false}); // код человека организации
        //this.addField('org_no',{tp: 'NUMERIC(2,0)',visible: false}); // порядковый номер организации
        this.addField('role',{tp: 'VARCHAR(30)',visible: true}); // роль человека
        this.addField('role_rmk',{tp: 'VARCHAR(240)',visible: true}); // комментарий к роли
        //this.addField('sign_mark',{tp: 'VARCHAR(1) CHECK (sign_mark IN (\'*\',\' \'))',visible: true}); // Важное упоминание

        this.addOrder('byid','t0.id');

        this.objects_chain = [PERSONAL_T, ORG_T];
        this.addReference('personal_t', {table:'personal_t',
            rule: '(t0.human_code={#t_self}.code)', replace:`{#t_self}.code, {#t_self}.surname, {#t_self}.fname, {#t_self}.lname,
            {#t_self}.birth, {#t_self}.birth_begin, {#t_self}.birth_end, {#t_self}.birth_rmk, {#t_self}.type_r,{#t_self}.type_a,{#t_self}.fund, {#t_self}.list_no, {#t_self}.file_no, {#t_self}.file_no_ext`});
    }            
    
    
    /**
     * Функция проверки прав доступа пользователя к данным. Доступ дается только администраторам.
     * @param {Object} user - пользователь, доступ которого нужно проверить
     */
    async testAccess(user) {
        this.oper_user = user;              
        if ((user.isadmin !== true)&&(user.usertype!==3)) {
            return {gres: false, text: 'Доступ запрещен.'};
        }        
        return {gres: true, text: 'Ok'};
    }
    
}

module.exports = COORG_T;
