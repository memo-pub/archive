var ChainedModel = require('../slib/chainedmodel');
var cntc = require('../db').connection;

var jwt = require('jsonwebtoken');
var config = require('../config');
var jwtOptions = {};
jwtOptions.secretOrKey = config.jwt.secretOrKey + 'file';
jwtOptions.ignoreExpiration = false;

var MoveOrderNom = require('../viewmodels/moveordernom');

//var pgp = require('../db').pgp;
var kode_error = [
    { kod: 23505, cod: 409, text: 'номер источника занят!' },
    { kod: 23503, cod: 500, text: 'Отсутствует Внешний ключ!' },
];

//Название таблицы: Источники

class SOUR_T extends ChainedModel {
    constructor() {
        super();
        //this.addReferenceToClass('personal_code','BIGINT NOT NULL',PERSONAL_T,'code', 'CASCADE', true, true); // Ссылка на personal_t
        this.addInputField('personal_code', 123321);
        this.addField('sour_no', {
            tp: 'NUMERIC(4,1) NOT NULL',
            visible: true,
            required: true,
        }); //номер источника
        this.addField('pub_type', {
            tp: 'SMALLINT CHECK ((pub_type=1) or (pub_type=2) or (pub_type=3) or (pub_type=4))',
            visible: true,
        });
        //1- Публикация, 2- Закрытая публикация, 3- Запрет публикации, 4- Не определено.
        this.addField('reprod_type_ids', { tp: 'VARCHAR(200)', visible: true }); // Тип воспроизведения (новый)
        this.addField('reprod_rmk', { tp: 'VARCHAR(240)', visible: true }); // Комментарий к типу воспроизведения
        this.addField('sour_name', { tp: 'VARCHAR(250)', visible: true }); // Название источника
        this.addField('sour_code', { tp: 'VARCHAR(40)', visible: true }); // Шифр источника
        this.addField('mark_mem', {
            tp: "VARCHAR(1) CHECK (mark_mem IN ('*'))",
            visible: true,
        }); // Источник хранится в архиве
        this.addField('place_so', { tp: 'VARCHAR(30)', visible: true }); // Местонахождения источника

        this.addField('sour_rmk', { tp: 'VARCHAR(240)', visible: true }); // Комментарий
        this.addField('acs_rmk', { tp: 'VARCHAR(240)', visible: true }); // Комментарий к правилам доступа
        //this.addField('category',{tp: 'VARCHAR(2)',visible: true}); // разряд
        this.addField('fund', { tp: 'INTEGER', visible: true }); // фонд
        this.addField('list_no', { tp: 'INTEGER', visible: true }); // опись
        this.addField('file_no', { tp: 'INTEGER', visible: true }); //дело
        this.addField('file_no_ext', {
            tp: "VARCHAR(10) NOT NULL DEFAULT ''",
            visible: true,
        }); //
        //this.addField('sour_type',{tp: 'VARCHAR(20)',visible: true}); //тип документа
        this.addField('sour_mark', {
            tp: "VARCHAR(1) CHECK  (sour_mark IN ('О','К'))",
            visible: true,
        }); //Оригинал или копия
        this.addField('sour_date', { tp: 'VARCHAR(250)', visible: true }); // Дата источника
        /*
            ALTER TABLE SOUR_T ALTER COLUMN sour_date TYPE VARCHAR(250);
        */
        this.addField('sour_date_begin', { tp: 'DATE', visible: true });
        this.addField('sour_date_end', { tp: 'DATE', visible: true });

        this.addField('published', {
            tp: "VARCHAR(1) CHECK (published IN ('*',' '))",
            visible: true,
        }); //published: 'документ опубликован'
        this.addField('mustPublish', {
            tp: "VARCHAR(1) CHECK (mustPublish IN ('*',' '))",
            visible: true,
        }); // сущность должна опубликоваться
        this.addField('mustUnpublish', {
            tp: "VARCHAR(1) CHECK (mustUnpublish IN ('*',' '))",
            visible: true,
        }); // сущность должна удалиться из публичной базы
        this.addField('pub_change_date', {
            tp: 'timestamp with time zone DEFAULT current_timestamp',
            visible: true,
        }); // дата обновления статуса публикации
        
        this.addReferenceToClass(
            'parent_id',
            'BIGINT',
            SOUR_T,
            'id',
            'SET NULL',
            true,
            false,
        );
        /*
            ALTER TABLE SOUR_T ADD COLUMN parent_id BIGINT REFERENCES SOUR_T(id) ON DELETE SET NULL;
        */

        //this.addUniqueIndex(['personal_code','sour_no']);
        this.addOrder('byid', 't0.id');
        this.addOrder('byiddesc', 't0.id desc');

        this.addOrder('bysour_date', 't0.sour_date_begin');
        this.addOrder('bysour_date_desc', 't0.sour_date_begin desc');

        this.addOrder(
            'bypos',
            't0.fund, t0.list_no, t0.file_no, t0.file_no_ext NULLS FIRST, t0.sour_no, t0.id',
        );
        this.addOrder(
            'byposdesc',
            't0.fund desc, t0.list_no desc, t0.file_no desc, t0.file_no_ext desc NULLS LAST, t0.sour_no desc, t0.id desc',
        );

        this.addFilter('sour_name_filter', ['sour_name']);
        this.addFilter('sour_date_filter', ['sour_date']);

        this.addUserFilter(
            'sour_type_filter',
            '({#f_value} = ANY(sour_type_ids))',
        );
        this.addUserFilter(
            'file_no_ext_empty',
            "(file_no_ext is null or file_no_ext='')",
        );
        this.addUserFilter(
            'file_no_ext_not_empty',
            "(file_no_ext is not null and file_no_ext<>'')",
        );
        this.addUserFilter(
            'filter_sour_date_from',
            't0.sour_date_end>={#f_value}',
        ); // TRUE если дата фильтра меньше или равна конечной дате диапазона
        this.addUserFilter(
            'filter_sour_date_to',
            't0.sour_date_begin<={#f_value}',
        ); // TRUE если дата фильтра больше или равна начальной дате диапазона
        //this.objects_chain = [PERSONAL_T];

        this.addInputField('searchtxt'); // 23.11.2020
        this.addInputField('searchflds'); // 23.11.2020

        this.addInputField('newnom', 1234);

        this.addReference('sourfolderfile', {
            table: `(select t3.sour_id ,json_agg(
                json_build_object(
                'link_id', t3.id,
                'file_id', t2.id,
                'orientation', t2.orientation,
                'archivefile', t2.archivefile) 
                ORDER BY archivefile->>'filename'
            ) as folder_file from sour_file_t t3
            left join ArchiveFolderFile t2 on t3.archivefolderfile_id=t2.id
            group by sour_id)`,
            rule: '(t0.id={#t_self}.sour_id)',
            replace: ' {#t_self}.folder_file',
        });

        this.addReference('extra_person', {
            table: `(select fund as p_fund, list_no as p_list_no, file_no as p_file_no, file_no_ext as p_file_no_ext, 
                array_agg(coalesce(surname|| ' ','') ||coalesce(fname || ' ','')||coalesce(lname,'')) as extra_person
                from personal_t
                group by fund, list_no, file_no, file_no_ext)`,
            rule: `t0.fund={#t_self}.p_fund and t0.list_no={#t_self}.p_list_no and t0.file_no={#t_self}.p_file_no
                    and t0.file_no_ext is not distinct from {#t_self}.p_file_no_ext`,
            replace: ' {#t_self}.extra_person',
        });

        this.addReference('filecnt', {
            table: '(select sour_id, count(*) as file_cnt from sour_file_t group by sour_id)',
            rule: '(t0.id={#t_self}.sour_id)',
            replace: ' {#t_self}.file_cnt',
        });

        this.addReference('sour_type_join', {
            table: `(select t1.sour_id, string_agg(t2.name_ent,';') as sour_type_join,
        array_agg(sour_type_id) as sour_type_ids
        from sour_type_t t1
        left outer join list_of_t t2
            on t2.id = t1.sour_type_id
        group by t1.sour_id)`,
            rule: '(t0.id={#t_self}.sour_id)',
            replace: ' {#t_self}.sour_type_join',
        });

        this.addReference('get_parent', {
            table: 'sour_t',
            rule: '(t0.parent_id={#t_self}.id)',
            replace: `json_build_object(
                    'sour_name',{#t_self}.sour_name, 
                    'fund',{#t_self}.fund, 
                    'list_no',{#t_self}.list_no, 
                    'file_no',{#t_self}.file_no,  
                    'file_no_ext',{#t_self}.file_no_ext) as parent_data`,
        });

        this.addReference('nochildren', {
            table: `(select parent_id from sour_t 
                where parent_id is not null
                group by parent_id)`,
            rule: '(t0.id={#t_self}.parent_id)',
            alias: 'child',
        });

        this.addUserFilter('nochildren', 'child.parent_id is null');

        //this.debugSQL = true;
    }

    translateError(err) {
        for (var i = 0; i < kode_error.length; i++) {
            if (kode_error[i].kod == err.code) {
                return { code: kode_error[i].cod, message: kode_error[i].text };
            }
        }
    }

    /* дата создания 20201025
    alter table sour_t alter column personal_code DROP NOT NULL;
    alter table sour_t alter column sour_no DROP NOT NULL;
*/
    //ДАТА создания 2020.11.11  alter table sour_t alter column sour_no SET NOT NULL;
    /**
     * Функция проверки прав доступа пользователя к данным. Доступ дается только администраторам.
     * @param {Object} user - пользователь, доступ которого нужно проверить
     */
    async testAccess(user) {
        this.oper_user = user;
        if (user.isadmin !== true && user.usertype !== 3) {
            return { gres: false, text: 'Доступ запрещен.' };
        }
        return { gres: true, text: 'Ok' };
    }

    async insert(trans = cntc) {
        try {
            if (this.file_no_ext == null) this.file_no_ext = '';
            let data = await trans.tx(async (t) => {
                if (
                    this.fund &&
                    this.list_no &&
                    this.file_no &&
                    this.sour_no == null
                ) {
                    if (this.file_no_ext == undefined) this.file_no_ext = null;
                    let sql = `select COALESCE(max(sour_no)+1,1) as num from sour_t 
                                where fund=$1 and list_no=$2 and file_no=$3 and COALESCE(file_no_ext,'')=COALESCE($4,'')`;
                    let no = await t.one(sql, [
                        this.fund,
                        this.list_no,
                        this.file_no,
                        this.file_no_ext,
                    ]);
                    this.sour_no = no.num;
                }
                let d = await super.insert(t);
                if (d != null && this.personal_code != null) {
                    let pst = new PERSONAL_SOUR_T();
                    pst.personal_code = this.personal_code;
                    pst.sour_id = d.id;
                    pst.oper_user = this.oper_user;
                    await pst.insert(t);
                    //t.none('insert into personal_sour_t (personal_code, sour_id) values ($1, $2)',[this.personal_code, d.id]);
                } else if (d != null) {
                    t.none(
                        `WITH sour as (
                                    select id, fund, list_no, file_no, file_no_ext from sour_t
                                    where fund is not null and list_no is not null and file_no is not null and id=$1
                                )
                                INSERT INTO PERSONAL_SOUR_T(personal_code,sour_id)
                                (
                                select t2.code, t1.id from sour t1, personal_t t2
                                where t1.fund=t2.fund and t1.list_no=t2.list_no and t1.file_no=t2.file_no and
                                    COALESCE(t1.file_no_ext,'')=COALESCE(t2.file_no_ext,'')
                                )`,
                        [d.id],
                    );
                }
                if (d != null && this.newnom != undefined) {
                    let moveOrderNom = new MoveOrderNom();
                    moveOrderNom.newnom = this.newnom;
                    moveOrderNom.itemid = d.id;
                    moveOrderNom.tablename = 'sour_t';
                    let new_gr = await moveOrderNom.insert(t);
                    if (new_gr != null) {
                        let dn = new_gr.find((val) => val.id == d.id);
                        if (dn != null) {
                            d.sour_no = dn.sour_no;
                        } else {
                            d.sour_no = this.newnom;
                        }
                    } else {
                        d.sour_no = this.newnom;
                    }
                }
                return d;
            });
            return data;
        } catch (err) {
            throw { code: err.code, message: err.message };
        }
    }
    //23.11.2020
    async selectAllJSON(selecttype, trans) {
        let elem_like = '';
        let str_like = [];
        if (this.file_no_ext_empty == 'false') {
            this.file_no_ext_empty = undefined;
            this.file_no_ext_not_empty = true;
        }
        var field_mas = [
            ['t0.sour_name'],
            ['t0.sour_code'],
            ['t0.place_so'],
            ['t0.sour_rmk'],
            ['t0.acs_rmk'],
            ['t0.fund'],
            ['t0.list_no'],
            ['t0.file_no'],
            ['t0.file_no_ext'],
            ['t0.sour_date'],
        ];

        // в POSTMAN задается несколко полей поиска searchflds где у каждого свое VALUE = 0...N (значение номера элемента массива field_mas)
        // в POSTMAN задается поле SEARCHTXT с VALUE типа %134eee% особенно для числовых полей которые могут содержать дробную часть

        if (this.searchflds !== undefined && this.searchtxt != undefined) {
            if (Array.isArray(this.searchflds) !== true) {
                this.searchflds = [this.searchflds];
            }

            for (let nm of this.searchflds) {
                // достаточно проверить, существует ли такой элемент
                if (field_mas[nm] !== undefined) {
                    for (let nm1 of field_mas[nm]) {
                        elem_like =
                            '(UPPER(' +
                            nm1 +
                            '::varchar) like UPPER({#f_value}))';
                        str_like.push(elem_like);
                    }
                } else {
                    // Если элемент в массиве не найден, то можно сообщить об ошибке
                    throw {
                        code: 409,
                        message: 'Неверный индекс поля для поиска.',
                    }; // Только я делаю не return, а вызываю исключение
                }
            }
        }

        elem_like = '(' + str_like.join(' OR ') + ')';
        this.addUserFilter('searchflds234', elem_like);
        this.searchflds234 = this.searchtxt;
        // для просмотра отработавших SQL раскоментируй строку ниже
        //this.debugSQL=true;
        // обрати внимание, что я сипользую let, а не var. Это создает локальную переменную, а не глобальную
        let u = await super.selectAllJSON(selecttype, trans);
        this.searchflds234 = undefined;
        return u;
    } //23.11.2020

    async selectOneByIdJSON(trans = cntc) {
        this.get_parent = true;
        let data = await super.selectOneByIdJSON();
        if (
            data !== null &&
            data.folder_file !== null &&
            this.sourfolderfile == 'true'
        ) {
            for (let d of data.folder_file) {
                d.jwt = jwt.sign(
                    { id: d.file_id, type: 'afolderfile' },
                    jwtOptions.secretOrKey,
                    { expiresIn: '2d' },
                );
            }
        }
        if (data !== null) {
            try {
                /// Персоналии
                let selstr = `select t1.id as link_id, t2.code, t2.surname, t2.fname, t2.lname, t2.birth, t2.birth_rmk, t2.fund , t2.list_no , t2.file_no, t2.file_no_ext from personal_sour_t t1
                inner join personal_t t2
                    on t2.code=t1.personal_code
                where t1.sour_id = $1;`;
                let values = [data.id];
                data.personals = await trans.tx(async (t) => {
                    return await t.manyOrNone(selstr, values);
                });
                if (data.parent_id == null) data.parent_data = null;
                /// Дочерние
                selstr =
                    'select id, sour_name, fund, list_no, file_no, file_no_ext from sour_t where parent_id=$1';
                data.children = await trans.tx(async (t) => {
                    return await t.manyOrNone(selstr, values);
                });
                return data;
            } catch (err) {
                throw { code: err.code, message: err.message };
            }
        }
        return data;
    }

    async update(trans = cntc) {
        if (this.file_no_ext == null) this.file_no_ext = '';
        return await super.update(trans);
    }

    async deleteOne() {
        try {
            let data = await super.deleteOne();
            let st = new SOUR_T_LINK();
            st.sour_t_id1 = this.id;
            st.deleteOne();
            st.sour_t_id1 = undefined;
            st.sour_t_id2 = this.id;
            st.deleteOne();
            return data;
        } catch (err) {
            throw { code: err.code, message: err.message };
        }
    }
}

module.exports = SOUR_T;

var PERSONAL_SOUR_T = require('./personal_sour_t');
var SOUR_T_LINK = require('../models/sour_t_link');
