var ChainedModel = require('../slib/chainedmodel');
var PERSONAL_T = require('./personal_t');
var REPRESS_T = require('./repress_t');
var cntc = require('../db').connection;
var MoveOrderNom = require('../viewmodels/moveordernom');

//учеба и профессиональная деятельность
class ACTIVITY_T extends ChainedModel {
    constructor() {
        super();
        this.addReferenceToClass('personal_code','BIGINT',PERSONAL_T,'code', 'RESTRICT', true, true); // Ссылка на personal_t
        this.addField('act_no',{tp: 'NUMERIC(4,1)',visible: true, required: true}); // порядковый номер учебы/работы
        //this.addField('sphere',{tp: 'VARCHAR(30)',visible: true}); // сфера деятельности
        this.addField('prof',{tp: 'VARCHAR(30)',visible: true}); // профессия
        this.addField('post',{tp: 'VARCHAR(200)',visible: true}); // должность
        this.addField('arrival_dat',{tp: 'DATE',visible: true}); // дата поступления
        this.addField('arrival_dat_begin',{tp: 'DATE',visible: true});
        this.addField('arrival_dat_end',{tp: 'DATE',visible: true});
        this.addField('depart_dat',{tp: 'DATE',visible: true}); // дата увольнения
        this.addField('depart_dat_begin',{tp: 'DATE',visible: true});
        this.addField('depart_dat_end',{tp: 'DATE',visible: true});
        this.addField('sphere_rmk',{tp: 'VARCHAR(240)',visible: true}); // комментарий к сфере деятельности
        this.addField('prof_rmk',{tp: 'VARCHAR(240)',visible: true}); // комментарий к профессии
        this.addField('post_rmk',{tp: 'VARCHAR(240)',visible: true}); // комментарий к должности
        this.addField('ent1_rmk',{tp: 'VARCHAR(240)',visible: true}); // комментарий к организации (организация)
        this.addField('arrival_dat_rmk',{tp: 'VARCHAR(240)',visible: true}); // комментарий к дате поступления
        this.addField('depart_dat_rmk',{tp: 'VARCHAR(240)',visible: true}); // комментарий к дате увольнения 
        //this.addField('geoplace_code',{tp: 'VARCHAR(18)',visible: true}); // код географического места работы
        this.addField('geoplace_code_rmk',{tp: 'VARCHAR(240)',visible: true}); // комментарий к географическому месту работы
        this.addField('place_num',{tp: 'NUMERIC(8,0)',visible: true}); // номер географической единицы места работы
        this.addField('general_rmk',{tp: 'VARCHAR(500)',visible: true}); //Комментарий общий

        /*
        ALTER TABLE ACTIVITY_T ADD COLUMN general_rmk VARCHAR(500);
        */

        this.addReferenceToClass('rep_id','BIGINT NOT NULL',REPRESS_T,'id', 'SET NULL', true, true); // ссылка на REPRESS_T

        this.addUniqueIndex(['personal_code','act_no']);      
        
        this.addOrder('byid','t0.id');
        this.addOrder('bypersonal_code','t0.personal_code');
        this.addOrder('bypos','t0.personal_code, t0.act_no');

        this.addInputField('newnom',1234);

        this.objects_chain = [PERSONAL_T];
    }            

    translateError(err) {
        if (err.code==23505) {
            if (err.message.indexOf('personal_code_act_no_activity_t')>-1) {
                return {code: 409, message:'Номер учебы/работы занят!'};
            }
        }
    }
    
    
    /**
     * Функция проверки прав доступа пользователя к данным. Доступ дается только администраторам.
     * @param {Object} user - пользователь, доступ которого нужно проверить
     */
    async testAccess(user) {
        this.oper_user = user;              
        if ((user.isadmin !== true)&&(user.usertype!==3)) {
            return {gres: false, text: 'Доступ запрещен.'};
        }        
        return {gres: true, text: 'Ok'};
    }

    async insert(trans=cntc) {
        try {
            let data  = await trans.tx(async t => {
                if (this.act_no == null)  {
                    let sql = `select COALESCE(max(act_no)+1,1) as num from ACTIVITY_T 
                                where personal_code=$1`;
                    let no = await t.one(sql,[this.personal_code]);
                    this.act_no = no.num;
                }
                let d = await super.insert(t);
                if ((d!=null) && (this.newnom!=undefined)) {
                    let moveOrderNom = new MoveOrderNom();
                    moveOrderNom.newnom = this.newnom;
                    moveOrderNom.itemid = d.id;
                    moveOrderNom.tablename = 'ACTIVITY_T';
                    let new_gr = await moveOrderNom.insert(t);
                    if (new_gr!=null) {
                        let dn = new_gr.find(val=>val.id==d.id);
                        if (dn !=null) {
                            d.act_no = dn.act_no;
                        }
                        else {
                            d.act_no = this.newnom;
                        }
                    }
                    else {
                        d.act_no = this.newnom;
                    }
                } 
                return d;
            });
            return data;
        }
        catch(err) {
            throw {code: err.code, message: err.message};
        } 
    }
    
}

module.exports = ACTIVITY_T;
