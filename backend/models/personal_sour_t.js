var ChainedModel = require('../slib/chainedmodel');
var PERSONAL_T = require('./personal_t');


class PERSONAL_SOUR_T extends ChainedModel {
    constructor() {
        super();
        this.addReferenceToClass('sour_id','BIGINT NOT NULL',SOUR_T,'id', 'CASCADE', true, true); // Ссылка на sour_t
        this.addReferenceToClass('personal_code','BIGINT NOT NULL',PERSONAL_T,'code', 'CASCADE', true, true); // Ссылка на personal_t
        this.addUniqueIndex(['personal_code','sour_id']);
        this.addOrder('byid','t0.id');
        
        this.addOrder('bysour_no',`(pers.fund=sour_t0.fund and pers.list_no=sour_t0.list_no and 
             pers.file_no=sour_t0.file_no and COALESCE(pers.file_no_ext,'')= COALESCE(sour_t0.file_no_ext,'')) desc NULLS LAST, shifr.id is null,
             sour_t0.fund, sour_t0.list_no, sour_t0.file_no, sour_t0.file_no_ext, sour_t0.sour_no, sour_t0.id_sour_t`);     

        this.objects_chain = [PERSONAL_T];
        
        this.addReference('sour_dan', {table:`(select sour_no, id as  id_sour_t,pub_type,sour_name,sour_code,mark_mem,place_so,
            sour_rmk,fund,list_no,file_no, file_no_ext ,sour_mark,sour_date, sour_date_begin, sour_date_end from sour_t )`,
        rule: '(t0.sour_id={#t_self}.id_sour_t)', replace:` {#t_self}.sour_no, {#t_self}.pub_type,
        {#t_self}.sour_name,{#t_self}.sour_code,{#t_self}.mark_mem,{#t_self}.place_so,
        {#t_self}.sour_rmk,{#t_self}.fund,{#t_self}.list_no,{#t_self}.file_no, {#t_self}.file_no_ext,{#t_self}.sour_mark,
        {#t_self}.sour_date,{#t_self}.sour_date_begin,{#t_self}.sour_date_end`, alias: 'sour_t0'}  );

        this.addReference('filecnt', {table:'(select sour_id, count(*) as file_cnt from sour_file_t group by sour_id)',
            rule: '(t0.sour_id={#t_self}.sour_id)', replace:' {#t_self}.file_cnt'});

        this.addReference('sour_type_join', {table:`(select t1.sour_id, string_agg(t2.name_ent,';') as sour_type_join from sour_type_t t1
        left outer join list_of_t t2
            on t2.id = t1.sour_type_id
        group by t1.sour_id)`,
        rule: '(t0.sour_id={#t_self}.sour_id)', replace:' {#t_self}.sour_type_join'});

        this.addReference('pers', {table:'personal_t',
            rule: '(t0.personal_code={#t_self}.code)', alias: 'pers'});

        this.addInputField('extra_person', true);

        
        this.addReference('pers_shifr', {table:'personal_shifr_t',
            rule: `(t0.personal_code={#t_self}.personal_code and {#t_self}.fund=sour_t0.fund 
            and {#t_self}.list_no=sour_t0.list_no and 
            {#t_self}.file_no=sour_t0.file_no
            and COALESCE({#t_self}.file_no_ext,'')=COALESCE(sour_t0.file_no_ext,'') ) 
            `, alias: 'shifr'}  );

    }
    
    /**
     * Функция проверки прав доступа пользователя к данным. Доступ дается только администраторам.
     * @param {Object} user - пользователь, доступ которого нужно проверить
     */
    async testAccess(user) {
        this.oper_user = user;              
        if ((user.isadmin !== true)&&(user.usertype!==3)) {
            return {gres: false, text: 'Доступ запрещен.'};
        }        
        return {gres: true, text: 'Ok'};
    }

    async selectAllJSON() {
        if ((this.personal_code !== undefined) && (this._orderby=='bysour_no')) {
            this.pers = true;
            this.sour_dan = true;
            this.debugSQL = true;
            this.pers_shifr = true;
        }
        if (this.personal_code !== undefined  && this.extra_person !== undefined) {
            this.addReference('extra_person_i', {table:`(select w1.id as sour_id, array_agg(coalesce(w2.surname|| ' ','') ||coalesce(w2.fname || ' ','')||coalesce(w2.lname,'')) as extra_person
                from sour_t w1, personal_t w2
                where w2.code<>{#self_value} and 
                    w2.fund=w1.fund and w2.list_no=w1.list_no and w2.file_no=w1.file_no
                    and w2.file_no_ext is not distinct from w1.file_no_ext
                group by w1.id)`,
            rule: '(t0.sour_id= {#t_self}.sour_id)', replace:' {#t_self}.extra_person'});
            this.extra_person_i = this.personal_code;
        }
        return await super.selectAllJSON();
    }
    
}

module.exports = PERSONAL_SOUR_T;

var SOUR_T = require('./sour_t');