var ChainedModel = require('../slib/chainedmodel');
var PERSONAL_T = require('./personal_t');

class PERSONAL_DB_LINK_T extends ChainedModel {
    constructor() {
        super();
        this.addReferenceToClass('personal_code','BIGINT',PERSONAL_T,'code', 'CASCADE', true, true);
        this.addField('link_id', { tp: 'VARCHAR(50)', visible: true });
        this.addField('link_id_rmk', { tp: 'VARCHAR(100)', visible: true });
        this.addOrder('byid','t0.id');
        this.objects_chain = [PERSONAL_T];
    }            
    
    
    /**
     * Функция проверки прав доступа пользователя к данным. Доступ дается только администраторам.
     * @param {Object} user - пользователь, доступ которого нужно проверить
     */
    async testAccess(user) {
        this.oper_user = user;              
        if ((user.isadmin !== true)&&(user.usertype!==3)) {
            return {gres: false, text: 'Доступ запрещен.'};
        }        
        if (this.file_no_ext == null) this.file_no_ext = '';
        return {gres: true, text: 'Ok'};
    }
    
}

module.exports = PERSONAL_DB_LINK_T;
