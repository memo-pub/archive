const jwt = require('jsonwebtoken');
const Model = require('../slib/model');
const { connection: cntc } = require('../db');

const config = require('../config');
const ArchiveFolderLog = require('./archivefolderlog');
const ArchiveFolder = require('./archivefolder');

const jwtOptions = {
    secretOrKey: `${config.jwt.secretOrKey}file`,
    ignoreExpiration: false,
};

class ArchiveFolderFile extends Model {
    constructor() {
        super();
        this.addField('archivefile', {
            tp: 'JSON',
            visible: true,
            required: true,
            isfile: true,
        });
        this.addReferenceToClass(
            'archivefolder_id',
            'BIGINT',
            ArchiveFolder,
            'id',
            'CASCADE',
            true,
            true,
        );

        /// 10.03.2019
        this.addField('creation_datetime', {
            tp: 'TIMESTAMP DEFAULT current_timestamp',
            visible: true,
            required: false,
        });
        /*
        ALTER TABLE ArchiveFolderFile ADD COLUMN creation_datetime TIMESTAMP DEFAULT current_timestamp;
         */

        /// 14.08.2020
        this.addField('orientation', {
            tp: 'SMALLINT DEFAULT 0',
            visible: true,
            required: false,
        });
        /*
            ALTER TABLE ArchiveFolderFile ADD COLUMN orientation SMALLINT DEFAULT 0;
        */

        this.addReference('folder', {
            table: `(select w1.id, w1.delo, w1.delo_extra, w2.fond, w2.opis_name from archivefolder w1
            left outer join opis w2
                on w2.id=w1.opis_id)`,
            rule: '(t0.archivefolder_id={#t_self}.id)',
            replace: '{#t_self}.fond, {#t_self}.opis_name, {#t_self}.delo, {#t_self}.delo_extra',
        });

        this.addReference('sour_samedelo', {
            table: `(select archivefolderfile_id from sour_file_t t1
                                                inner join sour_t t2
                                                    on t1.sour_id = t2.id
                                                inner join sour_t t3
                                                    on t3.id={#self_value} and t2.fund=t3.fund and t2.list_no=t3.list_no 
                                                    and t2.file_no=t3.file_no  and t2.file_no_ext=t3.file_no_ext
                                                group by archivefolderfile_id)`,
            rule: '(t0.id={#t_self}.archivefolderfile_id)',
            replace: '{#t_self}.archivefolderfile_id is not null as in_delo',
        });

        this.addOrder('byid', 'id');
        this.addOrder('byiddesc', 'id desc');
        this.addOrder('bycrtime', 'creation_datetime');
        this.addOrder('bycrtimedesc', 'creation_datetime desc');

        this.addOrder('byshifr', 'fond, opis_name, delo');

        this.addOrder('byshifrdesc', 'fond desc, opis_name desc, delo desc');

        this.addOrder('byfilename', "archivefile->>'filename'");
        this.addOrder('byfilenamedesc', "archivefile->>'filename' desc");

        this.addUserFilter('filter_fond', 'fond = {#f_value}');
        this.addUserFilter('filter_opis_name', 'opis_name = {#f_value}');
        this.addFilter('filter_delo', ['t1.delo']);
        this.addUserFilter('filter_delo_exact', 't1.delo = {#f_value}');
        this.addUserFilter('filter_delo_extra_exact', 't1.delo_extra = {#f_value}');
        this.addUserFilter('filter_delo_extra_empty', "t1.delo_extra = ''");
        this.addFilter('filter_filename', ["archivefile->>'filename'"]);

        this.addUserFilter(
            'filter_pictures',
            "((UPPER(archivefile->>'filename') like '%.JPG') OR (UPPER(archivefile->>'filename') like '%.JPEG') OR (UPPER(archivefile->>'filename') like '%.TIF') OR (UPPER(archivefile->>'filename') like '%.TIFF') OR (UPPER(archivefile->>'filename') like '%.PNG') OR (UPPER(archivefile->>'filename') like '%.BMP'))",
        );
        this.addInputField('getjwt', true);

        this.afterCreate =
            "create unique index uniq_archivefolderfile on archivefolderfile(archivefolder_id, (archivefile->>'filename'));";
    }

    /**
     * Функция проверки прав доступа пользователя к данным. Доступ дается только администраторам.
     * @param {Object} user - пользователь, доступ которого нужно проверить
     */
    async testAccess(user) {
        this.creation_datetime = undefined;
        this.oper_user = user;

        if (
            user.isadmin !== true &&
            user.usertype !== 2 &&
            user.usertype !== 1 &&
            user.usertype !== 3
        ) {
            return { gres: false, text: 'Доступ запрещен.' };
        }

        return { gres: true, text: 'Ok' };
    }

    async insert() {
        try {
            let data = await super.insert();

            if (data != null) {
                try {
                    let archiveFolderLog = new ArchiveFolderLog();
                    await archiveFolderLog.writeLog(this.oper_user.id, data.archivefolder_id, 3, {
                        new_data: data.archivefile.filename,
                    });
                } catch (error) {
                    console.dir({ code: 'AF_LOG_ERROR', message: error.message });
                }
            }

            let af = new ArchiveFolder();
            af.id = data.archivefolder_id;
            let af_data = await af.selectOneByIdJSON();

            if (af_data.date_scan == null) {
                let dt = new Date();
                af.date_scan = dt.toISOString();
                af.oper_user = this.oper_user;
            }

            if (af_data.scanuser_id == null) {
                af.scanuser_id = this.oper_user.id;
                af.oper_user = this.oper_user;
            }

            if (af.oper_user !== undefined) {
                af.update();
            }

            return data;
        } catch (error) {
            throw { code: error.code, message: error.message };
        }
    }

    async selectAllJSON(selecttype = 0, trans = cntc) {
        let data = await super.selectAllJSON(selecttype, trans);

        if (data !== null && this.getjwt == 'true') {
            for (let d of data) {
                d.jwt = jwt.sign({ id: d.id, type: 'afolderfile' }, jwtOptions.secretOrKey, {
                    expiresIn: '2d',
                });
            }
        }

        return data;
    }

    async deleteOne() {
        let data = await super.deleteOne();

        if (data != null) {
            try {
                let archiveFolderLog = new ArchiveFolderLog();
                await archiveFolderLog.writeLog(this.oper_user.id, data.archivefolder_id, 5, {
                    old_data: data.archivefile.filename,
                });
            } catch (error) {
                console.dir({ code: 'AF_LOG_ERROR', message: error.message });
            }
        }

        return data;
    }

    async updateFileProperty(trans = cntc, propName = 'filename') {
        const archiveFolderFile = new ArchiveFolderFile();
        archiveFolderFile.id = this.id;


        const oldData = await archiveFolderFile.selectOneByIdJSON();
        const data = await super.updateFileProperty(trans, propName);

        const renamingOriginalFileNameInDb = propName === 'filename';
        const previousName = oldData?.archivefile?.filename;
        const proposedName = data?.archivefile?.filename;
        const doLog = proposedName && proposedName !== previousName;
        if (renamingOriginalFileNameInDb && doLog) {
            try {
                const archiveFolderLog = new ArchiveFolderLog();
                await archiveFolderLog.writeLog(this.oper_user.id, data.archivefolder_id, 4, {
                    old_data: oldData.archivefile.filename,
                    new_data: data.archivefile.filename,
                });
            } catch (error) {
                console.dir({ code: 'AF_LOG_ERROR', message: error.message });
            }
        }

        return data;
    }

    async updateOne({ id }) {
        let archiveFolderFile = new ArchiveFolderFile();
        archiveFolderFile.id = { id };
        let oldData = await archiveFolderFile.selectOneByIdJSON();
        let data = await super.updateOne({ id });

        if (!(data && oldData)) return data;

        let oldName = oldData.archivefile.filename;
        let newName = data.archivefile.filename;
        let namesEqual = oldName === newName;

        if (namesEqual) return data;

        try {
            let archiveFolderLog = new ArchiveFolderLog();
            await archiveFolderLog.writeLog(this.oper_user.id, data.archivefolder_id, 4, {
                old_data: oldName,
                new_data: newName,
            });
        } catch (error) {
            console.dir({ code: 'AF_LOG_ERROR', message: error.message });
        }

        return data;
    }
}

module.exports = ArchiveFolderFile;
