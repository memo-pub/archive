var Model = require('../slib/model');
//var PERSONAL_T = require('./personal_t');
var kode_error=[{kod:23505,cod: 409,text:'Такой код уже есть!'},
    {kod:23503,cod: 500,text:'Отсутствует Внешний ключ в таблице PERSONAL_T !'}];

//Название таблицы: Причина смерти

class DEATH_REASON_T extends Model {
    constructor() {
        super();
        this.addField('death_type',{tp: 'VARCHAR(4) NOT NULL',visible: true, required: true}); //код
        this.addField('death_rmk',{tp: 'VARCHAR(60)',visible: true}); // расшифровка
        this.addUniqueIndex(['death_type']);      
        this.addOrder('byid','t0.id');
        this.addOrder('bytype','t0.death_type');
    }            

    translateError(err) {
        for (var i = 0; i < kode_error.length;i++) {
            if (kode_error[i].kod==err.code) {
                return {code: kode_error[i].cod, message:kode_error[i].text};     
            }
        }
    }

    
    /**
     * Функция проверки прав доступа пользователя к данным. Доступ дается только администраторам.
     * @param {Object} user - пользователь, доступ которого нужно проверить
     */
    async testAccess(user) {
        this.oper_user = user;              
        if ((user.isadmin !== true)&&(user.usertype!==3)) {
            return {gres: false, text: 'Доступ запрещен.'};
        }        
        return {gres: true, text: 'Ok'};
    }
    
}

module.exports = DEATH_REASON_T;
