var Model = require('../slib/model');
var Muser = require('./muser');

class MLog extends Model {
    constructor() {
        super();
        this.addReferenceToClass('muser_id','BIGINT NOT NULL',Muser,'id', '', true, true); //
        this.addField('action_time',{tp: 'DATE DEFAULT current_date',visible: true, required: false});//  
        this.addField('object_table',{tp: 'VARCHAR(30) NOT NULL',visible: true,required:true});
        this.addField('object_id',{tp: 'BIGINT NOT NULL',visible: true,required:true});//
        this.addField('subobject_table',{tp: 'VARCHAR(30) NOT NULL',visible: true,required:true});
        this.addField('subobject_id',{tp: 'BIGINT NOT NULL',visible: true,required:true});//
        this.addField('object_data',{tp: 'JSON',visible: true,required:false});
        this.addField('action_type',{tp: 'VARCHAR(10) CHECK (action_type IN (\'DELETE\', \'SET\', \'NEW\'))',visible: true, required: true});//

        this.addUniqueIndex(['muser_id', 'action_time', 'object_table', 'object_id', 'subobject_table', 'subobject_id', 'action_type']);

        this.addReference('userdata', {table:'muser',rule: '(t0.muser_id={#t_self}.id)', replace:'{#t_self}.login as user_login, {#t_self}.name as user_name, {#t_self}.surname as user_surname'});
        
    }            
   
    
    async postGuard () {
        return {gres: false, text: 'Вставка (Удаление) запрещена.'}; 
    }

    async deleteGuard () {
        return {gres: false, text: 'Вставка (Удаление) запрещена.'}; 
    }

    async putGuard () {
        return {gres: false, text: 'Вставка (Удаление) запрещена.'}; 
    }
    
    /**
     * Функция проверки прав доступа пользователя к данным. Доступ дается только администраторам.
     * @param {Object} user - пользователь, доступ которого нужно проверить
     */
    async testAccess(user) {
        this.oper_user = user;              
        if ((user.isadmin !== true)&&(user.usertype!==3)) {
            return {gres: false, text: 'Доступ запрещен.'};
        }        
        return {gres: true, text: 'Ok'};
    }
    
}
module.exports = MLog;

