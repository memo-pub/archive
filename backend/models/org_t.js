var ChainedModel = require('../slib/chainedmodel');
var PERSONAL_T = require('./personal_t');
var LIST_OF_T = require('./list_of_t');


// участие в организациях
class ORG_T extends ChainedModel {
    constructor() {
        super();
        this.addReferenceToClass('personal_code','BIGINT',PERSONAL_T,'code', 'RESTRICT', true, true); // Ссылка на personal_t
        this.addField('org_no',{tp: 'NUMERIC(4,1)',visible: true, required: true}); // порядковый номер организации
        //this.addField('org_code',{tp: 'VARCHAR(20)',visible: true}); // код организации
        this.addField('org_name',{tp: 'VARCHAR(60)',visible: true}); // название организации
        this.addField('join_dat',{tp: 'DATE',visible: true}); // дата вступления
        this.addField('join_dat_begin',{tp: 'DATE',visible: true});
        this.addField('join_dat_end',{tp: 'DATE',visible: true});
        this.addField('dismis_dat',{tp: 'DATE',visible: true}); // дата выхода
        this.addField('dismis_dat_begin',{tp: 'DATE',visible: true});
        this.addField('dismis_dat_end',{tp: 'DATE',visible: true});
        //this.addField('particip',{tp: 'VARCHAR(30)',visible: true}); // функция
        this.addField('com_rmk',{tp: 'VARCHAR(240)',visible: true}); // общий комментарий
        this.addField('ent_name_rmk',{tp: 'VARCHAR(240)',visible: true}); // комментарий к наименованию организации
        this.addField('join_dat_rmk',{tp: 'VARCHAR(240)',visible: true}); // комментарий к дате вступления
        this.addField('dismis_dat_rmk',{tp: 'VARCHAR(240)',visible: true}); // комментарий к дате выхода
        this.addField('particip_rmk',{tp: 'VARCHAR(240)',visible: true}); // комментарий к участию
        this.addReferenceToClass('org_code_id','BIGINT',LIST_OF_T,'id', 'RESTRICT', true, false); // Ссылка на list_of_t 
        
        //тип организации по полям name_ent, name_rmk


        this.addReferenceToClass('particip_id','BIGINT',LIST_OF_T,'id', 'RESTRICT', true, false); // Ссылка на list_of_t тип участия
        this.addReference('particip_data', {table:'LIST_OF_T',rule: '(t0.particip_id={#t_self}.id)', replace:'{#t_self}.name_ent as particip'});
        // тип участия по полям name_ent, name_rmk

        
        this.addUniqueIndex(['personal_code','org_no']);  
        
        this.objects_chain = [PERSONAL_T];
        
        this.addOrder('byid','t0.id');
        this.addOrder('bypersonal_code','t0.personal_code');
        this.addOrder('bypos','t0.personal_code, t0.org_no');
    }            
    
    /*  UPDATE ORG_T m 
            SET org_no = sub.rn 
        FROM  
         (SELECT personal_code,org_no, row_number() OVER (PARTITION BY personal_code order by personal_code,org_no) AS rn 
            FROM ORG_T WHERE (personal_code IS NOT NULL) and (org_no IS NOT NULL)) sub 
         WHERE  m.personal_code = sub.personal_code and m.org_no=sub.org_no
*/

    /**
     * Функция проверки прав доступа пользователя к данным. Доступ дается только администраторам.
     * @param {Object} user - пользователь, доступ которого нужно проверить
     */
    async testAccess(user) {
        this.oper_user = user;              
        if ((user.isadmin !== true)&&(user.usertype!==3)) {
            return {gres: false, text: 'Доступ запрещен.'};
        }        
        return {gres: true, text: 'Ok'};
    }
    
    translateError(err) {
        if (err.code==23505) {
            if (err.message.indexOf('personal_code_org_no_org_t')>-1) {
                return {code: 409, message:'Номер организации занят!'};
            }
        }
    }
}

module.exports = ORG_T;
