const Model = require('../slib/model');

class ArchiveFolderLog extends Model {
    constructor() {
        super();
        this.addField('muser_id', { tp: 'BIGINT NOT NULL', visible: true, required: true });
        this.addField('action_time', {
            tp: 'timestamp DEFAULT current_timestamp',
            visible: true,
            required: false,
        }); //
        this.addField('archivefolder_id', { tp: 'BIGINT NOT NULL', visible: true, required: true });
        this.addField('object_data', { tp: 'JSON', visible: true, required: false });
        this.addField('action_type', { tp: 'SMALLINT', visible: true, required: true }); //
        // 1 - создание дела, 2 - изменение статуса, 3 - добавление файла, 4 - переименование файла
        // 5 - удаление файла, 6 - изменение количества пронумерованных листов, 7 - удаление дела

        this.addUniqueIndex(['muser_id', 'action_time', 'archivefolder_id', 'action_type']);

        this.addReference('userdata', {
            table: 'muser',
            rule: '(t0.muser_id={#t_self}.id)',
            replace:
                '{#t_self}.login as user_login, {#t_self}.name as user_name, {#t_self}.surname as user_surname',
        });
    }

    async postGuard() {
        return { gres: false, text: 'Вставка (Удаление) запрещена.' };
    }

    async deleteGuard() {
        return { gres: false, text: 'Вставка (Удаление) запрещена.' };
    }

    async putGuard() {
        return { gres: false, text: 'Вставка (Удаление) запрещена.' };
    }

    /**
     * Функция проверки прав доступа пользователя к данным. Доступ дается только администраторам.
     * @param {Object} user - пользователь, доступ которого нужно проверить
     */
    async testAccess(user) {
        this.oper_user = user;

        if (user.isadmin !== true && user.usertype !== 3) {
            return { gres: false, text: 'Доступ запрещен.' };
        }

        return { gres: true, text: 'Ok' };
    }

    async writeLog(muser_id, archivefolder_id, action_type, object_data) {
        this.archivefolder_id = archivefolder_id;
        this.action_type = action_type;
        this.object_data = object_data;
        this.muser_id = muser_id;
        await this.insert();
    }
}
module.exports = ArchiveFolderLog;
