var Model = require('../slib/model');

class PLACE_T extends Model {
    constructor() {
        super();    
        //this.addField('no',{tp: 'BIGINT',visible: false});
        this.addField('place_num',{tp: 'BIGINT NOT NULL',visible: true, required: true});
        this.addField('cur_mark',{tp: 'VARCHAR(1) CHECK (cur_mark IN (\'*\',\' \'))',visible: true});//'*' это правильная запись надо брать ее
        this.addField('place_code',{tp: 'VARCHAR(18)',visible: true});
        this.addField('beg_dat',{tp: 'NUMERIC(4,0)',visible: true});
        this.addField('end_dat',{tp: 'NUMERIC(4,0)',visible: true});
        this.addField('place_name',{tp: 'VARCHAR(30) NOT NULL',visible: true, required: true});
        this.addField('place_rem',{tp: 'VARCHAR(120)',visible: true});
        //this.addField('place_level',{tp: 'NUMERIC(1,0)',visible: false});
        //this.addField('place_parent',{tp: 'BIGINT',visible: false});
        //this.addField('us_in_name',{tp: 'VARCHAR(20)',visible: false});
        //this.addField('in_date',{tp: 'DATE DEFAULT CURRENT_DATE',visible: false});
        //this.addField('us_up_name',{tp: 'VARCHAR(20)',visible: false});
        //this.addField('up_date',{tp: 'DATE',visible: false});
        this.addField('status',{tp: 'VARCHAR(3)',visible: true});
        //this.addField('valid_dat',{tp: 'VARCHAR(1)',visible: false});
        //this.addField('f_date',{tp: 'NUMERIC(4)',visible: false});

        this.addFilter('filter',['place_num','place_name']);
        this.addOrder('byfilter','place_num , place_code, place_name');
        this.addOrder('byplace_num','t0.place_num');
        this.addOrder('byplace_code','t0.place_code');
        this.addOrder('byplace_name','t0.code');
    }            
    
    /**
     * Функция проверки прав доступа пользователя к данным. Доступ дается только администраторам.
     * @param {Object} user - пользователь, доступ которого нужно проверить
     */
    async testAccess(user) {
        this.oper_user = user;              
        if ((user.isadmin !== true)&&(user.usertype!==3)) {
            return {gres: false, text: 'Доступ запрещен.'};
        }        
        return {gres: true, text: 'Ok'};
    }


    isInteger(str) {
        return /^\d+$/.test(str);
    }

    async getAllGuard () {
        if ((this.filter!==undefined)&&(this._orderby==='byfilter')) {
            let index = 1;
            if  ((this.wheres_default !== undefined) && (this.wheres_default.script))
            {
                index = this.wheres_default.values.length + 1;
            }                 
            let wheres = this.getStaticWheres(index);
            let i = wheres.inext;
            for (var f in this._filters) {
                if (f==='filter') {
                    index = i;
                }
                if (this[f]!==undefined) {
                    i++;
                }            
            }
            if (this.isInteger(this.filter)) {
                this._orders[this._orderby] = 'position($'+index+' in UPPER(place_num::varchar)), place_num';
            }
            else {
                this._orders[this._orderby] = 'position($'+index+' in UPPER(place_name)), place_name';
            }
        }
        return {gres: true, text: 'Ok'};
    }
    
}

module.exports = PLACE_T;
