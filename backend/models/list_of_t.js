var Model = require('../slib/model');

class LIST_OF_T extends Model {
    constructor() {
        super();              
        //this.addField('num_str',{tp: 'BIGINT',visible: false});
        this.addField('name_f',{tp: 'VARCHAR(20)',visible: true, required: true});
        this.addField('name_ent',{tp: 'VARCHAR(40)',visible: true, required: true});//тип орг
        this.addField('name_rmk',{tp: 'VARCHAR(60)',visible: true, required: true});//тип орг
	
        this.addUniqueIndex(['name_f', 'name_ent']);            
        
        this.addOrder('byname_f','t0.name_f');

        this.addOrder('byname_ent','t0.name_ent');
        this.addOrder('byname_ent_desc','t0.name_ent desc');

        this.addOrder('byname_rmk','t0.name_rmk');
        this.addOrder('byname_rmk_desc','t0.name_rmk desc');

        this.addOrder('byid','t0.id');
        this.addOrder('byid_desc','t0.id desc');
    }            
    
    
    /**
     * Функция проверки прав доступа пользователя к данным. Доступ дается только администраторам.
     * @param {Object} user - пользователь, доступ которого нужно проверить
     */
    async testAccess(user) {
        this.oper_user = user;              
        if ((user.isadmin !== true)&&(user.usertype!==3)) {
            return {gres: false, text: 'Доступ запрещен.'};
        }        
        return {gres: true, text: 'Ok'};
    }

    /** 
     * Функция преобразования ошибок
     * @param {Object} err - объект ошибки
     * */    
    translateError(err) {
        if (err.code==23505) {
            if (err.message.indexOf('name_f_name_ent_list_of_t')>-1) {
                return {code: 409, message:'Запись с таким кодом уже существует!'};
            }
            
        }
        if (err.code==23503) {
            if (err.message.indexOf('fkey')>-1) {
                return {code: 409, message:'Есть ссылки на эту запись!'};
            }
            
        }
    }
    
}

module.exports = LIST_OF_T;
