const Model = require('../slib/model');

const SETTINGS_LIST = [
    {
        key: 'standart_page_price',
        value: { value: 0.5 },
    },
    {
        key: 'special_page_price',
        value: { value: 10 },
    },
    {
        key: 'file_scan_price',
        value: { value: 10 },
    },
    {
        key: 'file_archive_price',
        value: { value: 50 },
    },
    {
        key: 'personal_price',
        value: { value: 8 },
    },
    {
        key: 'in_personal_price',
        value: { value: 11 },
    },
    {
        key: 'video_ext ',
        value: ['avi'],
    },
];

/*
 */

/**
 * Класс для доступа к таблице с данными настроек программы
 * @property {string} key - ключ
 * @property {Object} value - значение
 */
class MConfig extends Model {
    constructor() {
        super();
        this.addField('key', { tp: 'VARCHAR(100)', visible: true, required: true });
        this.addField('value', { tp: 'JSON', visible: true, required: true });

        this.addUniqueIndex(['key']);
        this._pk = 'key';
    }

    /**
     * Функция проверки прав доступа пользователя к данным. Доступ дается только администраторам.
     * @param {Object} user - пользователь, доступ которого нужно проверить
     */
    async testAccess(user) {
        this.oper_user = user;

        if (user.isadmin !== true) {
            return { gres: false, text: 'Доступ запрещен.' };
        }

        return { gres: true, text: 'Ok' };
    }

    async putGuard(user) {
        if (this.value !== undefined) {
            this.value = JSON.parse(this.value);
        }

        return this.testAccess(user);
    }

    async postGuard(user) {
        if (this.value !== undefined) {
            this.value = JSON.parse(this.value);
        }

        return this.testAccess(user);
    }

    async deleteGuard() {
        return { gres: false, text: 'Удаление запрещено.' };
    }

    async getAllGuard(user) {
        return this.testAccess(user);
    }

    async merge() {
        try {
            await super.merge();

            for (let st of SETTINGS_LIST) {
                this.key = st.key;
                this.value = st.value;
                await this.insert();
            }
        } catch (error) {
            throw { code: error.code, message: error.message };
        }
    }
}

module.exports = MConfig;
