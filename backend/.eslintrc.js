module.exports = {
    parserOptions: {
        ecmaVersion: 2020,
        sourceType: 'script',
    },
    env: {
        es2020: true,
        node: true,
        commonjs: true,
        es2017: true,
        es6: true,
    },
    reportUnusedDisableDirectives: true,
    extends: [
        'eslint:recommended',
        'prettier:recommended',
        'xo',
    ],
    parser: '@babel/eslint-parser',
    rules: {
        'arrow-body-style': [
            'off',
        ],
        'prefer-arrow-callback': [
            'error',
            {
                allowNamedFunctions: false,
            },
        ],
    },
};
