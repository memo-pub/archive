const crypto = require('crypto');
var config = require('./config');
var salt = config.jwt.salt;

var getPwdHash = function (sk) {
    const key = crypto.pbkdf2Sync(sk, salt, 200, 50, 'sha1');
    let password_hash=key.toString('hex').toUpperCase();
    return password_hash;
};


module.exports.getPwdHash = getPwdHash;