const jwt = require('jsonwebtoken');
const FondFile = require('../models/fondfile');
const OpisFile = require('../models/opisfile');
const ArchiveFolderFile = require('../models/archivefolderfile');
const config = require('../config');
const { getRelativeFileFolderPath } = require('./files');

const jwtOptions = {};
jwtOptions.secretOrKey = `${config.jwt.secretOrKey}file`;
jwtOptions.ignoreExpiration = false;

const mediaMiddleware = function (req, res, next) {
    let jwtData = {};

    try {
        jwtData = jwt.verify(req.query.mediaSign, jwtOptions.secretOrKey);
    } catch {
        return res.status(401).json({ message: 'Ресурс не доступен! [1]' });
    }

    if (Number(jwtData.id) !== Number(req.params.id)) {
        return res.status(401).json({ message: 'Ресурс не доступен! [2]' });
    }

    let FileClass;
    let fldname;

    switch (jwtData.type) {
        case 'afolderfile': {
            FileClass = ArchiveFolderFile;
            fldname = 'archivefile';

            break;
        }

        case 'fondfile': {
            FileClass = FondFile;
            fldname = 'fondfile';

            break;
        }

        case 'opisfile': {
            FileClass = OpisFile;
            fldname = 'opisfile';

            break;
        }

        default: {
            res.status(401).json({ message: 'Ресурс не доступен! [3]' });
        }
    }

    const aff = new FileClass();
    aff.id = Number(req.params.id);
    aff.selectOneByIdJSON()
        .then((data) => {
            if (data === null) {
                res.status(401).json({ message: 'Ресурс не доступен! [4]' });
            }

            const { id, creation_datetime: date } = data;
            const subFolder = getRelativeFileFolderPath({ id, date });
            req.url = `//${subFolder}/${data[fldname].file}`;
            res.new_filename = data[fldname].filename;

            next();
        })
        .catch(function () {
            res.status(401).json({ message: 'Ресурс не доступен! [5]' });
        });
};

function setMediaHeaders(res) {
    const mime = {
        'image/jpeg': '.jpg$|.jpeg$',
        'image/png': '.png$',
        'image/bmp': '.bmp$',
        'image/tiff': '.tiff$|.tif$',
        'image/gif': '.gif$',
        'video/mp4': '.mp4$',
        'video/ogg': '.ogg$',
        'application/x-troff-msvideo': '.avi$',
        'audio/mpeg3': '.mp3$',
        'audio/wav': '.wav$',
        'video/x-matroska': '.mkv$',
    };

    const mimeEntries = Object.entries(mime);

    for (let [mm, regexp] of mimeEntries) {
        const patt = new RegExp(regexp, 'i');

        if (patt.test(res.new_filename.toLowerCase())) {
            res.setHeader('Content-Type', mm);
            break;
        }
    }
}

module.exports = {
    mediaMiddleware,
    setMediaHeaders,
};
