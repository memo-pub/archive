var passport = require('passport');
var strategy = require('../auth').strategy;

passport.use(strategy);

var getOrAuthenticate= function(req, res, next) {
    if (req.method=='GET') {
        return next();
    }
    else {
        passport.authenticate('jwt', { session: false })(req, res, next);
    }
};



module.exports.getOrAuthenticate = getOrAuthenticate;
module.exports.passport = passport;