const cron = require('node-cron');

const checkPubDateRun = async (connection) => {
    cron.schedule('3 0 * * *', () => setPublished(connection));
};
const setOpisPublished = async (connection, curDate) => {
    const sql = `WITH published_fonds AS (SELECT fond FROM fond WHERE published = '*')
    UPDATE opis t1 SET published = '*', mustPublish = '*', mustUnpublish = null, pub_change_date = '${curDate}' FROM published_fonds t2
    WHERE t1.pub_date = '${curDate}' AND t1.fond=t2.fond RETURNING t1.id;`;
    return await connection.manyOrNone(sql);
};

const setArchiveFolderPublished = async (connection, curDate) => {
    const sql = `WITH published_fonds as (select fond from fond where published = '*'),
    published_opis_ids as (select t1.id from opis t1, published_fonds t2 where t1.fond=t2.fond and published = '*')
    UPDATE archivefolder t1 SET published = '*', mustPublish = '*', mustUnpublish = null, pub_change_date = '${curDate}' FROM published_opis_ids t2 
    WHERE t1.pub_date = '${curDate}' and t1.opis_id=t2.id RETURNING t1.id;`;
    return await connection.manyOrNone(sql);
};

const setPublished = async (connection) => {
    const curDate = new Date().toISOString().split('T')[0];
    try {
        await setOpisPublished(connection, curDate);
        await setArchiveFolderPublished(connection, curDate);
    } catch (err) {
        console.error(`Checking publication date error: ${err}`);
    }
};

module.exports = {
    checkPubDateRun,
};
