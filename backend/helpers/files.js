const process = require('node:process');
const path = require('node:path');
const fs = require('node:fs');
const config = require('../config');

const fsPromises = fs.promises;

const doesPathExist = async (filePath) => {
    try {
        await fsPromises.access(filePath);
        return true;
    } catch {
        return false;
    }
};

const ensureFolderExists = async (folderPath) => {
    try {
        await fsPromises.mkdir(folderPath, { recursive: true });
        return true;
    } catch (error) {
        console.error(error);
        return false;
    }
};

const transliterate = (text) => {
    const letters = {
        а: 'a',
        б: 'b',
        в: 'v',
        г: 'g',
        д: 'd',
        е: 'e',
        ё: 'e',
        ж: 'zh',
        з: 'z',
        и: 'i',
        й: 'y',
        к: 'k',
        л: 'l',
        м: 'm',
        н: 'n',
        о: 'o',
        п: 'p',
        р: 'r',
        с: 's',
        т: 't',
        у: 'u',
        ф: 'f',
        х: 'h',
        ц: 'c',
        ч: 'ch',
        ш: 'sh',
        щ: 'shch',
        ы: 'y',
        э: 'e',
        ю: 'yu',
        я: 'ya',
    };

    return text.replace(/[а-яё]/g, (char) => letters[char] || char);
};

const makeStorageSafeFilename = ({ originalFileName, id, fileColumn }) => {
    const prefix = config.filePrefixMap[fileColumn] || '';
    // convert to lower case
    const newFileName = `${id}${prefix}_${originalFileName}`.trim().toLowerCase();
    // convert russian letters to ascii
    const transliterated = transliterate(newFileName);
    // remove all non-ascii letters
    const asciiOnly = transliterated.replace(/[^a-z\d()-_[\]\-.\s]/gi, '');
    // replace 2 spaces or any two non-word characters with just a single that character
    const noDoubleSpaces = asciiOnly.replace(/(\s{2,})|(\W{2,})/g, '$1$2');
    // replace 2 spaces with underscores
    const noSpaces = noDoubleSpaces.trim().replace(/\s/g, '_');
    const { name, ext } = path.parse(noSpaces);
    // limit filename length to 50 characters
    const limitedLength = name.slice(0, 50);
    const result = `${limitedLength}${ext}`;
    return result;
};

const dateToYearMonthDay = (date) => {
    const d = new Date(date);
    const year = d.getFullYear();
    // no leading zeroes
    const month = d.getMonth() + 1;
    const day = d.getDate();
    return [year, month, day];
};

const getRelativeFileFolderPath = ({ id: idArg, date: dateArg }) => {
    const date = new Date(dateArg);
    const [year, month, day] = dateToYearMonthDay(date);
    const id = Number(idArg);
    const by1000 = Math.floor(id / 1000);
    const by100 = Math.floor(id / 100) % 10;
    const result = path.join(...[year, month, day, by1000, by100].map(String));
    return result;
};

const getFileFolderPath = ({ id, date }) => {
    const storageRoot = getFileStorageRoot(date);
    const subDir = getRelativeFileFolderPath({ id, date });
    const result = path.join(storageRoot, subDir);
    return result;
};

const getStorageRootPaths = () => {
    const roots = Object.entries(process.env)
        .filter(([name]) => name.startsWith(config.multiRootVariableNamePrefix))
        .map(([, value]) => value);

    return [config.storageRoot, ...roots];
};

const convertNumberToAlphaNumeric = (number) => {
    const allowedOutputChars = '0123456789abcdefghijklmnopqrstuvwxyz';
    const base = allowedOutputChars.length;
    let result = '';

    while (number > 0) {
        const remainder = number % base;
        result = allowedOutputChars[remainder] + result;
        number = Math.floor(number / base);
    }

    return result;
};

const ensureUniquePath = async (filePath, counter = 0) => {
    if (!(await doesPathExist(filePath))) return filePath;

    const { dir, name, ext } = path.parse(filePath);
    const suffix = convertNumberToAlphaNumeric(counter + 1);

    const newFilePath = path.join(dir, `${name}_${suffix}${ext}`);
    return ensureUniquePath(newFilePath, counter + 1);
};

const ensureUniquePathSync = (filePath, counter = 0) => {
    if (!fs.existsSync(filePath)) return filePath;

    const { dir, name, ext } = path.parse(filePath);
    const suffix = convertNumberToAlphaNumeric(counter + 1);

    const newFilePath = path.join(dir, `${name}_${suffix}${ext}`);
    return ensureUniquePathSync(newFilePath, counter + 1);
};

const moveFile = async (sourcePath, destinationPath) => {
    if (sourcePath === destinationPath) return destinationPath;
    
    const destinationDir = path.dirname(destinationPath);
    await ensureFolderExists(destinationDir);

    const sourceExists = await doesPathExist(sourcePath);
    if (!sourceExists) return false;

    const destinationExists = await doesPathExist(destinationPath);
    if (destinationExists) return false;

    try {
        await fsPromises.rename(sourcePath, destinationPath);
        return destinationPath;
    } catch (error) {
        console.error(error);

        try {
            await fsPromises.copyFile(sourcePath, destinationPath);
            await fsPromises.unlink(sourcePath);

            return destinationPath;
        } catch (error) {
            console.error(error);
            return false;
        }
    }
};

const checkStorageDirIsValid = async (dirPath) => {
    if (!dirPath) {
        throw new Error(`Cannot check storage dir validity. dirPath is ${dirPath}`);
    }

    const promises = [
        doesPathExist(dirPath),
        fsPromises.access(dirPath, fs.constants.R_OK || fs.constants.W_OK),
    ];

    const [isExisting, isReadable] = await Promise.all(promises);

    if (!isExisting) {
        console.error(`Directory ${dirPath} does not exist`);
    }

    if (isReadable) {
        console.error(`Directory ${dirPath} is not readable`);
    }
    return true;
};

const getFileStorageRoot = (date) => {
    if (config.usingSingleStorageRoot) return config.storageRoot;

    const pathsByYearMonthDay = Object.entries(process.env)
        .filter(([name]) => name.startsWith(config.multiRootVariableNamePrefix))
        .map(([name, value]) => {
            const yyyyMMDD = name.slice(config.multiRootVariableNamePrefix.length);
            const yyyyMD = yyyyMMDD.split('_').map(Number).join('_');
            return [yyyyMD, value];
        })
        .sort(([a], [b]) => a.localeCompare(b));

    const dateAsYearMonthDay = dateToYearMonthDay(date).join('_');

    if (pathsByYearMonthDay.length === 0) {
        return config.storageRoot;
    }

    const match = pathsByYearMonthDay.find(([envVarDate]) => envVarDate > dateAsYearMonthDay);

    return (match && match[1]) || config.storageRoot;
};

const bytesToHuman = (bytes) => {
    const units = ['B', 'KB', 'MB', 'GB'];
    const value = bytes;
    const unit = units.find((unit, index, units) => {
        if (index === units.length - 1) return true;
        return value < 1024 ** (index + 1);
    });

    const humanValue = value / 1024 ** units.indexOf(unit);
    const roundedValue = Math.round(humanValue * 100) / 100;
    return { value: roundedValue, unit };
};

const resolveFilePath = async ({ fileName = '', filePath = '', id, date }) => {
    const file = fileName || path.basename(filePath);
    const pathToParentDir = path.dirname(filePath) || getFileFolderPath({ id, date });
    const pathToFile = path.join(pathToParentDir, file);

    const altUploadRoot = getFileStorageRoot(date);
    const filePathInUploadDir = path.join(config.uploadDir, fileName);
    const filePathInAlternativeUploadDir = path.join(altUploadRoot, fileName);

    const paths = [pathToFile, filePathInUploadDir, filePathInAlternativeUploadDir];

    const result = await Promise.all(paths.map((filePath) => doesPathExist(filePath)))
        .then((results) => results.indexOf(true))
        .then((index) => paths[index]);

    return result;
};

const moveFileToStorage = async ({ req, dataModel, actionResult }) => {
    const fileColumns = Object.keys(config.filePrefixMap);
    const fileColumn = fileColumns.find((key) => actionResult[key]);

    const fileRow = actionResult[fileColumn];
    const filePath = req?.body?.fileUploaded?.[fileColumn]?.filepath;

    const { id, creation_datetime: date } = actionResult;
    const { filename: originalFileName } = fileRow;

    const newFileName = makeStorageSafeFilename({ originalFileName, id, fileColumn });
    const newFileDir = getFileFolderPath({ id, date });
    let newFilePath = path.join(newFileDir, newFileName);

    if (filePath !== newFilePath) {
        const destinationPath = await ensureUniquePath(newFilePath);
        newFilePath = await moveFile(filePath, destinationPath);
    }

    if (!newFilePath) {
        console.error(`Failed to move file ${filePath} to ${newFilePath}`);
        return false;
    }

    const dataModelInstance = new dataModel.constructor();

    const [fileColumnName] = Object.entries(dataModelInstance._fields).find(
        ([_, fieldValue]) => fieldValue.isfile,
    );

    dataModelInstance[fileColumnName] = newFileName;
    dataModelInstance.id = id;

    const { dbUpdateError } = await dataModelInstance
        .updateFileProperty(undefined, 'file')
        .then((dbUpdateResult) => ({ dbUpdateError: false, dbUpdateResult }))
        .catch((error) => ({ dbUpdateError: error }));

    if (dbUpdateError) {
        console.log(`moving ${newFilePath}  back to ${filePath}`);
        await moveFile(newFilePath, filePath);
        return filePath;
    }
};

module.exports = {
    getStorageRootPaths,
    getFileFolderPath,
    getRelativeFileFolderPath,
    checkStorageDirIsValid,
    bytesToHuman,
    makeStorageSafeFilename,
    moveFileToStorage,
    resolveFilePath,
    ensureFolderExists,
    ensureUniquePathSync,
};
