const passportJWT = require('passport-jwt');
const jwt = require('jsonwebtoken');
const MUser = require('./models/muser');
const cntc = require('./db').connection;

const config = require('./config');

const {ExtractJwt} = passportJWT;
const JwtStrategy = passportJWT.Strategy;

const jwtOptions = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme('jwt'),
    secretOrKey: config.jwt.secretOrKey,
    ignoreExpiration: false,
};

const strategy = new JwtStrategy(jwtOptions, ((jwtPayload, next) => {
    const query = 'select t1.*  from muser t1 WHERE  t1.id=$1 and flagarchive=0';
    const params = [jwtPayload.id];

    cntc.oneOrNone(query, params)
        .then((user) => next(null, user || false))
        .catch((error) => {
            console.dir(error);
            next(null, false);
        });
}));

/* Проверка данных пользователя и генерация токена */
const authLogin = async (login, password) => {
    const user = new MUser();

    if (!password) {
        return {code: 401, rslt: {message: 'Давайте придумаем пароль?'}};
    }

    user.loadFromRequest({login, password});
    user.showinvisible = true;
    let userProps;

    try {
        userProps = await user.selectAllJSON();
    } catch (error) {
        console.dir(error);
        return {code: 500, rslt: {error: error.message}};
    }

    if (userProps?.[0]?.flagarchive === 0) {
        const payload = {id: userProps[0].id};
        const token = jwt.sign(payload, jwtOptions.secretOrKey, {expiresIn: '1000d'});
        return {
            code: 201,
            rslt: {
                token,
                name: userProps[0].name,
                surname: userProps[0].surname,
                id: userProps[0].id,
                usertype: userProps[0].usertype,
                isadmin: userProps[0].isadmin,
            },
        };
    }
    return {code: 401, rslt: {message: 'Неправильный логин или пароль'}};
};

const checkuser = async (jwtPayload) => {
    const user = new MUser();

    try {
        user.id = jwtPayload.id;
        user.createCTE_asbdv = undefined;
        user.createCTE_alnk = undefined;
        user.showinvisible = true;
        const userProps = await user.selectOneByIdJSON();

        if (userProps) {
            return userProps;
        }
        return false;
    } catch (error) {
        console.dir(error);
        return false;
    }
};

const authGetPayload = async (token) => {
    const jwtPayload = jwt.verify(token, jwtOptions.secretOrKey);
    return checkuser(jwtPayload);
};

module.exports = {
    strategy,
    authLogin,
    authGetPayload,
};
