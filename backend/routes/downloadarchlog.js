var express = require('express');
var router = express.Router();

var cntc = require('../db').connection;
var xl = require('excel4node');



/* GET login. */
router.get('/', async function(req, res) {
    
    if (req.query.archivefolder_id === undefined) {
        res.status(409).json({message:'Не задан ID дела.'});
        return;
    }
    try {
        let data = await selectAllJSON(req.query.archivefolder_id);
        let wb = new xl.Workbook({dateFormat: 'dd.mm.yyyy hh:mm:ss'});
        let ws = wb.addWorksheet('Лист 1');

        let style = wb.createStyle({
            font: {
                color: '#000000',
                bold: true,
            }
        });

        ws.cell(1, 1).string('Пользователь').style(style);
        ws.cell(1, 2).string('Время').style(style);
        ws.cell(1, 3).string('Действие').style(style);
        ws.cell(1, 4).string('Было').style(style);
        ws.cell(1, 5).string('Стало').style(style);


        ws.column(1).setWidth(30);
        ws.column(2).setWidth(20);
        ws.column(3).setWidth(35);
        ws.column(4).setWidth(60);
        ws.column(5).setWidth(60);

        let i = 2;

        for (let d of data) {
            if (d.login!==null) { ws.cell(i, 1).string(d.login); }
            if (d.action_time!==null) { ws.cell(i, 2).date(d.action_time); }
            // 1 - создание дела, 2 - изменение статуса, 3 - добавление файла, 4 - переименование файла
            // 5 - удаление файла, 6 - изменение количества пронумерованных листов, 7 - удаление дела
            const acts = ['создание дела','изменение статуса','добавление файла','переименование файла','удаление файла','к-во пронумерованных листов','удаление дела'];
            if (d.action_type!==null) { ws.cell(i, 3).string(acts[d.action_type-1]); }            
            if (d.object_data !==null) {
                if (d.object_data.old_data !=null) {
                    ws.cell(i, 4).string(d.object_data.old_data);
                }
                if (d.object_data.new_data !=null) {
                    ws.cell(i, 5).string(d.object_data.new_data);
                }
            }
            
            i++;
        }
        wb.write('ExcelFile.xlsx', res);
    }
    catch (err) {
        res.status(500).json({message:'Ошибка при генерации Excel.', err: err});
    }
});

var getSelectScript = function (id) {
    let result = `SELECT t2.login, t1.action_time, t1.action_type, t1.object_data
                        FROM public.archivefolderlog t1
                    left outer join muser t2
                        on t1.muser_id=t2.id
                    where archivefolder_id=$1
                    order by t1.action_time;`;
    return {script: result, values: id};
};


var selectAllJSON = async function(id) {                   
    let selscrpt =await getSelectScript(id);                   
    try {
        let data = await cntc.tx( async t => {
            return await t.any(selscrpt.script,selscrpt.values,val=>val.data);                
        });            
        return data;
    }    
    catch(err) {                    
        throw {code: err.code, message: err.message};         
    }               
};


module.exports = router;