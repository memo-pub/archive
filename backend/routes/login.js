const express = require('express');

const router = express.Router();

const {authLogin} = require('../auth');

router.all('*', (req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    next();
});

/* POST login. */
router.post('/', async (req, res) => {
    if(req.body.login && req.body.password){
        const {login,password} = req.body;
        const obj = await authLogin(login,password);
        res.status(obj.code).json(obj.rslt);     
    }  
    else {
        res.status(401).json('Недостаточно данных');
    }  
});

module.exports = router;