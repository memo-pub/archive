var express = require('express');
var router = express.Router();

var cntc = require('../db').connection;
var xl = require('excel4node');



/* GET login. */
router.get('/', async function(req, res) {
    
    if (req.query.selection_id === undefined) {
        res.status(409).json({message:'Не задан ID выборки.'});
        return;
    }
    try {
        let data = await selectAllJSON(req.query.selection_id);
        if (data==null) {
            res.status(409).json({message:'Не задан записей.'});
            return;
        }
        let wb = new xl.Workbook();
        let ws = wb.addWorksheet('Лист 1');

        let style = wb.createStyle({
            font: {
                color: '#000000',
                bold: true,
            }
        });

        var myStyle = wb.createStyle({
            font: {
                bold: false,
                underline: false,
             
            },
            alignment: {
                wrapText: true,
                horizontal: 'center',
                vertical: 'top',
            },
        });
        ws.cell(1, 1).string('Номер по БД').style(style);
        ws.cell(1, 2).string('Фамилия').style(style);
        ws.cell(1, 3).string('Имя').style(style);
        ws.cell(1, 4).string('Отчество').style(style);
        ws.cell(1, 5).string('Дата рождения').style(style);
        ws.cell(1, 6).string('Фонд').style(style);
        ws.cell(1, 7).string('Опись').style(style);
        ws.cell(1, 8).string('Дело').style(style);
        ws.cell(1, 9).string('Источников').style(style);

        ws.column(1).setWidth(12);
        ws.column(2).setWidth(30);
        ws.column(3).setWidth(30);
        ws.column(4).setWidth(30);
        ws.column(5).setWidth(15);
        ws.column(6).setWidth(10);
        ws.column(7).setWidth(10);
        ws.column(8).setWidth(10);
        ws.column(9).setWidth(15);

        let i = 2;

        for (let d of data) {
            if (d.code!==null) { ws.cell(i, 1).number(d.code).style(myStyle); }
            if (d.surname!==null) { ws.cell(i, 2).string(d.surname); }
            if (d.fname!==null) { ws.cell(i, 3).string(d.fname); }
            if (d.lname!==null) { ws.cell(i, 4).string(d.lname); }
            if (d.birth!==null) {
                ws.cell(i, 5).string(d.birth);
            }
            else {
                if (d.birth_rmk!==null) { ws.cell(i, 5).string(d.birth_rmk); }
            }
            if (d.fund!==null) { ws.cell(i, 6).number(d.fund); }
            if (d.list_no!==null) { ws.cell(i, 7).number(d.list_no); }
            if (d.file_no!==null) {
                let no = d.file_no;
                if (d.file_no_ext!==null) {
                    no = no+d.file_no_ext;
                }
                ws.cell(i, 8).number(no); 
            }
            
            if (d.sour_cnt!==null) { ws.cell(i, 9).number(d.sour_cnt); }
            i++;
        }
        wb.write('ExcelFile.xlsx', res);
    }
    catch (err) {
        res.status(500).json({message:'Ошибка при генерации Excel.', err: err});
    }
});

var getSelectScript = function (id) {
    let result = `WITH stms as (
                    SELECT personal_code FROM selectionitem
                    WHERE selection_id=$1)
                SELECT t2.*, COALESCE(t3.sour_cnt,0) as sour_cnt FROM stms t1
                INNER JOIN personal_t t2
                    ON t1.personal_code=t2.code
                LEFT OUTER JOIN  (select personal_code, COUNT(id) as sour_cnt from personal_sour_t w
                                    group by personal_code) t3
                    ON t2.code=t3.personal_code`;
    return {script: result, values: id};
};


var selectAllJSON = async function(id) {                   
    let selscrpt =await getSelectScript(id);        
    let script = 'select json_agg(winquery) as data from ('+selscrpt.script+') winquery;';                    
    try {
        let data = await cntc.tx( async t => {
            return await t.oneOrNone(script,selscrpt.values,val=>val.data);                
        });            
        return data;
    }    
    catch(err) {                    
        throw {code: err.code, message: err.message};         
    }               
};


module.exports = router;