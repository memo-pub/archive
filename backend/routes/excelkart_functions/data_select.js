
var cntc = require('../../db').connection;

var getSelectScript = function (id) {
    let result = `WITH pers as (SELECT * FROM personal_t WHERE code=$1),
    impris as (
        select crt_id, json_agg(json_build_object('prison_name', prison_name, 'prison_name_rmk', prison_name_rmk,
        'leave_dat',dat2_to_text(leave_dat_begin, leave_dat_begin),'leave_dat_rmk',t33.leave_dat_rmk,'impris_type',t34.name_ent, 'reason',t35.name_ent, 
        'prison_place_rmk', prison_place_rmk, 'place_name', t36.place_name)
            ORDER BY impris_no) as impris 
            from impris_t t33
            left join list_of_t t34 on t33.impris_type_id=t34.id
            left join list_of_t t35 on t33.reason_id=t35.id 
            LEFT OUTER JOIN place_t t36 ON ((t36.place_num=t33.place_num) AND (t36.cur_mark='*'))
        group by crt_id
    ),
    court as (
        select t7.rep_id, json_agg (
            json_build_object(
                'court_name',t7.court_name,
                'trial_no',t7.trial_no,
                'sentense_date',dat2_to_text(t7.sentense_date_begin, t7.sentense_date_end),
                'sentense_date_rmk', t7.sentense_date_rmk,
                'article',t7.article,
                'article_rmk',t7.article_rmk,
                'sentense',t7.sentense, 
                'sentense_rmk',t7.sentense_rmk,  
                'impris',t1.impris)
                ORDER BY trial_no
            ) as courts
            from court_t t7
        left outer join impris t1
            on t7.id=t1.crt_id
        group by t7.rep_id
    ),
    org as (
        SELECT t2.personal_code, json_agg(json_build_object(
			'org_name', t2.org_name,
			'particip_id', t2.particip_id,
			'join_dat', dat2_to_text(t2.join_dat_begin, t2.join_dat_end),
			'join_dat_rmk', t2.join_dat_rmk, 
			'dismis_dat',dat2_to_text(t2.dismis_dat_begin, t2.dismis_dat_end),
			'dismis_dat_rmk', t2.dismis_dat_rmk,
            'type_particip', t3.name_ent,
			'type_org', t4.name_ent
		) ORDER BY t2.join_dat_begin) as org
        FROM org_t t2
        INNER JOIN list_of_t t4 ON (t2.org_code_id=t4.id) and (t4.name_ent='ПОЛИТИЧЕСКАЯ')
        LEFT OUTER JOIN list_of_t t3 ON (t2.particip_id=t3.id)
        where t2.personal_code=$1
        group by t2.personal_code
    ),
    activ0 as (
        select t7.*, t7_1.repress_no, t7_1.id is not null as has_rep, count(*) over (partition by t7.personal_code) as cnt
        from activity_t t7
        left outer join repress_t t7_1 
            on t7.rep_id=t7_1.id
        where t7.personal_code = $1 
    ),
    activ as (
        select t7.personal_code, json_agg (json_build_object('ent1_rmk',t7.ent1_rmk,'post',t7.post,'post_rmk',
            t7.post_rmk,'prof',t7.prof,'prof_rmk',t7.prof_rmk,'arrival_dat',dat2_to_text(t7.arrival_dat_begin,t7.arrival_dat_end))
            ORDER BY t7.repress_no) as activs 
        from activ0 t7
        where t7.has_rep or t7.cnt=1
        group by t7.personal_code
    ),
    repress as (
        select t5.id, t5.personal_code, t5.repress_no, t12.name_ent as repress_type, t5.reabil_dat_begin, t5.reabil_dat_end, t5.geoplace_code_rmk, t5.repress_dat_begin, t5.repress_dat_end,
            t5.repress_dat_rmk,t5.reabil_mark, t5.rehabil_org, t5.reabil_rmk, t6.place_code, t6.place_name, t6.place_num,
			json_agg(t11.name_ent) as name_ent
        	from repress_t t5
        LEFT OUTER JOIN place_t t6 ON ((t6.place_num=t5.place_num) AND (t6.cur_mark='*'))
        LEFT OUTER JOIN rehabil_reason_t t10 ON (t5.id=t10.rep_id)
        LEFT OUTER JOIN list_of_t t11 ON (t11.id=t10.reason_id)
        LEFT OUTER JOIN list_of_t t12 ON (t12.id=t5.repress_type_id)   
        where t5.personal_code=$1
		group by t5.id, t5.personal_code, t5.repress_no, t12.name_ent, t5.reabil_dat_begin, t5.reabil_dat_end, t5.geoplace_code_rmk, t5.repress_dat_begin, t5.repress_dat_end,
            t5.repress_dat_rmk,t5.reabil_mark, t5.rehabil_org, t5.reabil_rmk, t6.place_code, t6.place_name, t6.place_num
    ),
    all_data as (
        SELECT t2.code, t2.fname,t2.fname_rmk, t2.lname,t2.lname_rmk, t2.surname,t2.real_surname_rmk as surname_rmk, dat2_to_text(t2.birth_begin, t2.birth_end) as birth,t2.birth_rmk,t2.birth_place_rmk,
        t9.name_ent as educ,t9.name_rmk as educ_name_rmk,t2.educ_rmk,dat2_to_text(t2.death_begin, t2.death_end) as death,t2.death_date_rmk,t3.place_name as birth_place_name,t2.birth_place_num,
        t4.nation,t4.nation_rmk as nation_info, t2.nation_rmk, t2.fund, t2.list_no, t2.file_no,
		json_agg ( json_build_object (
			'repress_no', t5.repress_no,
			'repress_type',t5.repress_type,
			'reabil_dat', dat2_to_text(t5.reabil_dat_begin, t5.reabil_dat_end),
			'geoplace_code_rmk', t5.geoplace_code_rmk,
			'repress_dat', dat2_to_text(t5.repress_dat_begin, t5.repress_dat_end),
			'repress_dat_rmk', t5.repress_dat_rmk,
			'reabil_mark', t5.reabil_mark,
			'reabil_rmk', t5.reabil_rmk,
        	'rehabil_org', t5.rehabil_org,
			'place_code_on_repress', t5.place_code,
			'place_name_on_repress', t5.place_name,
			'place_num_on_repress', t5.place_num,
			'reabil_reas', t5.name_ent,
			'courts', t12.courts) order by t5.repress_no) as repress
        FROM pers t2
        LEFT OUTER JOIN place_t t3 ON ((t3.place_num=t2.birth_place_num) AND (t3.cur_mark='*'))
        LEFT OUTER JOIN nat_t t4 ON t2.nation_id=t4.id
        LEFT OUTER JOIN repress t5 ON t2.code=t5.personal_code
        LEFT OUTER JOIN list_of_t t9 ON (t2.educ_id=t9.id)
        LEFT OUTER JOIN court t12 on t5.id=t12.rep_id                                                      
        GROUP BY t2.code, t2.fname,t2.fname_rmk, t2.lname,t2.lname_rmk, t2.surname,t2.real_surname_rmk,t2.birth_begin, t2.birth_end,t2.birth_rmk,t2.birth_place_rmk,
            t9.name_ent,t9.name_rmk,t2.educ_rmk,t2.death_begin, t2.death_end,t2.death_date_rmk,t3.place_name,t2.birth_place_num,t4.nation,t4.nation_rmk, t2.nation_rmk
            , t2.fund, t2.list_no, t2.file_no
    ),

    f_relative_one as (select human_code as personal_code,role from family_t 
        where personal_code=$1),
	f_relative	as (select * from f_relative_one
	    union select personal_code,'' as role  from family_t
        where human_code=$1 and personal_code not in (select personal_code from f_relative_one)),
    f_distant_relative as (select personal_code,role from f_relative  
        union select human_code as personal_code,'' as role from family_t where human_code not in (select personal_code from f_relative)
		and personal_code in (select personal_code from f_relative) and human_code<>$1),
    family as ( select json_agg(
		json_build_object('code',code,'fname', fname, 'lname', lname, 'surname' ,surname, 'role',role,'fund', fund,'list_no', list_no, 'file_no', file_no )) 
		as family FROM
        (SELECT distinct t2.code, t2.fname,  t2.lname, t2.surname, t4.role,t2.fund, t2.list_no, t2.file_no
        FROM personal_t  t2
        INNER JOIN f_distant_relative t4 ON (t4.personal_code=t2.code)
        LEFT OUTER JOIN repress_t t2_1 ON (t2.code = t2_1.personal_code) 
        WHERE (t2_1.id is not null) or (t2.type_r='*')
        GROUP BY code,fname,lname,surname,role,fund,list_no,file_no
        )k
    ),
    inform as (
        select personal_code, json_agg(
                json_build_object('code',code,'fname', fname, 'lname', lname, 'surname' ,surname, 'role', role, 'phone', phone, 'email', email,
                'address',address,'address_rmk',address_rmk, 'address_city', address_city )) as inform FROM
            (SELECT distinct t2.code, t2.fname,  t2.lname, t2.surname, t4.personal_code, 
                t4.role, t2.phone, t2.email,t2.address,t2.address_rmk, pl.place_name as address_city
            FROM personal_t  t2
            INNER JOIN inform_t t4 
                ON (t4.personal_code=$1 and t2.code=t4.human_code)
            left outer join place_t pl
                on t2.place_num=pl.place_num and pl.cur_mark='*'
            ) w
        GROUP BY personal_code
    ), 
    sour_types as (
        select t1.sour_id, string_agg(t2.name_ent,';') as sour_type from sour_type_t t1
        left outer join list_of_t t2
            on t2.id = t1.sour_type_id
        group by t1.sour_id
    ),

    sour as (SELECT t4.personal_code, json_agg( json_build_object (
        'sour_no',t2.sour_no,
        'sour_name',t2.sour_name,
        'sour_date',dat2_to_text(sour_date_begin,sour_date_end),
        'sour_mark',t2.sour_mark,
        'sour_type',t3.sour_type,
        'sour_code', t2.sour_code,
        'fund',t2.fund,
        'list_no',t2.list_no,
        'file_no',t2.file_no,
        'file_no_ext',t2.file_no_ext,
        'is_in_extra', shifr.personal_code is not null
    ) ORDER BY 
        (t2.fund is not distinct from t0.fund and t2.list_no is not distinct from t0.list_no 
        and t2.file_no is not distinct from t0.file_no and
        COALESCE(t2.file_no_ext,'')= COALESCE(t0.file_no_ext,'')) desc NULLS LAST, 
        shifr.personal_code is null,
        t2.fund, t2.list_no, t2.file_no, t2.file_no_ext, t2.sour_no, t2.id) as sour
    FROM sour_t  t2
    inner join personal_t t0
        on t0.code=$1
    inner join personal_sour_t t4 on t2.id=t4.sour_id
    left outer join sour_types t3
        on t2.id=t3.sour_id
    left outer join (
            select distinct personal_code, fund, list_no, file_no, file_no_ext   from personal_shifr_t
            where personal_code = $1
        ) shifr
        on t4.personal_code=shifr.personal_code and shifr.fund=t2.fund 
        and shifr.list_no=t2.list_no and 
        shifr.file_no=t2.file_no
        and COALESCE(shifr.file_no_ext,'')=COALESCE(t2.file_no_ext,'')
    where t4.personal_code=$1 and t2.mark_mem='*'
    group by t4.personal_code
),
sub_shifr_count as (select count(*) as sub_shifr_cnt,json_agg( json_build_object (
    'fund',t55.fund,
    'list_no',t55.list_no,
    'file_no',t55.file_no,
    'file_no_ext',t55.file_no_ext
)) as dopdela  from personal_shifr_t t55
where personal_code =  $1)

    SELECT t1.*, t2.org, t3.activs, t4.family, t5.sour, t6.inform, 
        (t7.sub_shifr_cnt=0 and (t1.fund is null or t1.list_no is null or t1.file_no is null)) as no_shifr,
        t7.dopdela
    from all_data t1
    left outer join org t2
        on t1.code = t2.personal_code
    left outer join activ t3
        on t1.code = t3.personal_code
    left outer join family t4
        on t1.code >0
    left outer join sour t5
        on t1.code = t5.personal_code
    left outer join inform t6
        on t1.code = t6.personal_code
    left outer join sub_shifr_count t7
        on true
    `;
    return {script: result, values: id};
};


var getSelectScriptzip = function (id) {
    let result=`
        with f as (select t0.human_code from family_t t0 
        where t0.personal_code=$1
        union select t2.human_code from inform_t t2 
        where t2.personal_code=$1
        union select t4.human_code from MENTIONED_T t4 
        where t4.personal_code=$1
        union select t6.human_code from corep_t t6 
        INNER join repress_t  t7 on t7.id=t6.rep_id and t7.personal_code=$1
        union select t9.human_code from coorg_t t9 
        INNER join org_t  t10 on t10.id=t9.org_id and t10.personal_code=$1
        union select t12.human_code from coact_t t12 
        INNER join activity_t  t13 on t13.id=t12.act_id and t13.personal_code=$1
        union select t15.human_code from cocrt_t t15 
        INNER join court_t  t16 on t16.id=t15.crt_id
        INNER join repress_t  t161 on t161.id=t16.rep_id and t161.personal_code=$1 
        union select t18.human_code from coimp_t t18
        INNER join impris_t  t162 on t162.id=t18.imp_id
        INNER join court_t  t19 on t19.id=t162.crt_id
        INNER join repress_t  t20 on t20.id=t19.rep_id and t20.personal_code=$1)
        select human_code from f 
        INNER join personal_t t21 on t21.code=human_code and t21.type_r='*'
        union select code from personal_t where code=$1
        `;
    /* можно и так , где where ((t21.type_r='*') or (t21.code=2)) отнсится ко всему запросу
    select human_code from f 
    inner join personal_t t21 on (t21.code=human_code)
    where ((t21.type_r='*') or (t21.code=2)) 
    */
    
    return {script: result, values: id};
};

var selectAllJSONzip = async function(id) {                   
    let selscrpt = getSelectScriptzip(id); 
    let script = selscrpt.script;            
    try {
        let data = await cntc.tx( async t => {
            let d1 = await t.any(script,selscrpt.values); 
            return d1;                  
        });
        return data;
    }    
    catch(err) {                 
        throw {code: err.code, message: err.message};         
    }               
};


var selectAllJSON = async function(id) {                   
    let selscrpt = getSelectScript(id); 
    let script = selscrpt.script;            
    try {
        let data = await cntc.tx( async t => {
            let d1 = await t.oneOrNone(script,selscrpt.values);
            let mentioned = new Mentioned();
            mentioned.code = id;
            let d2 = await mentioned.selectAllJSON(cntc);
            d1['mention']=d2;
            return d1;
        });
        return data;
    }    
    catch(err) {                 
        throw {code: err.code, message: err.message};         
    }               
};

module.exports.selectAllJSON = selectAllJSON;
module.exports.selectAllJSONzip = selectAllJSONzip;

var  Mentioned = require('../../viewmodels/mentioned');