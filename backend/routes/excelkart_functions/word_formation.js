
var docx = require('docx');

const {AlignmentType, convertInchesToTwip, LevelFormat,
    TabStopType,TabStopPosition, PageNumber,Footer,Document, Paragraph,TextRun } = docx;

function doc_formation(mas1,mas2,mas3,mas4,mas5,mas6,mas7, data_code){
    let shsize=24;
    let word_itog=[];
    let word_mas1=[];
    word_mas1.push(new TextRun({text:'\t'+mas1[0],bold:true,size: shsize}));
    word_mas1.push(new TextRun({text:'\t'+mas1[1],break:1,bold:true,size: shsize}));
    word_itog.push(new Paragraph({
        tabStops: [
            {
                type: TabStopType.RIGHT,
                position: TabStopPosition.MAX-3,// is the distance from the left side.
            },
        ],
        children: word_mas1}));
    let word_mas2=[];

    word_mas2.push(new TextRun({text:'Род. ',break:2,underline:{},size: shsize}));
    word_mas2.push(new TextRun({text:mas2[0],size: shsize}));
    word_mas2.push(new TextRun({text:'Ум. ',break:1,underline:{},size: shsize}));
    word_mas2.push(new TextRun({text:mas2[1],size: shsize}));
    word_mas2.push(new TextRun({text:'Нац. ',break:1,underline:{},size: shsize}));
    word_mas2.push(new TextRun({text:mas2[2],size: shsize}));
    word_mas2.push(new TextRun({text:'Парт. ',break:1,underline:{},size: shsize}));
    word_mas2.push(new TextRun({text:mas2[3],size: shsize}));
    word_mas2.push(new TextRun({text:'Раб. ',break:1,underline:{},size: shsize}));
    word_mas2.push(new TextRun({text:mas2[4],size: shsize})); 
    word_mas2.push(new TextRun({text:'Жит. ',break:1,underline:{},size: shsize}));
    word_mas2.push(new TextRun({text:mas2[5],size: shsize}));
    word_mas2.push(new TextRun({text:'Репр. ',break:1,underline:{},size: shsize}));

    for (let uyi=0;uyi<mas7.length;uyi++){
        if (uyi==0){
            word_mas2.push(new TextRun({text:mas7[uyi],size: shsize}));} 
        else{
            word_mas2.push(new TextRun({text:mas7[uyi],break:1,size: shsize}));}}

    word_mas2.push(new TextRun({text:'Реаб. ',break:1,underline:{},size: shsize}));
    word_mas2.push(new TextRun({text:mas2[6],size: shsize}));
    word_mas2.push(new TextRun({text:'Р.ч.с ',break:1,underline:{},size: shsize}));
    word_mas2.push(new TextRun({text:mas2[7],size: shsize}));
    word_itog.push(new Paragraph({
        children: word_mas2,     
    }));

    if (mas4.length>0) {
        word_itog.push(new Paragraph({
            children: [new TextRun({text:'Дополнительные дела  ',break:1,underline:{},size: shsize})],     
        }));
        
        for (let i = 0; i < mas4.length;i++) {
            word_itog.push(new Paragraph({
                children: [new TextRun({text:mas4[i],size: shsize})],    
            }));   
        }
    }

    word_itog.push(new Paragraph({
        children: [new TextRun({text:'Источники ',break:1,underline:{},size: shsize})],     
    }));
    word_itog.push(new Paragraph({
        children: [new TextRun({text:' ',size: shsize})],     
    }));

    for (let i = 0; i < mas3.length;i++) {
        word_itog.push(new Paragraph({
            numbering: {
                reference: 'my-crazy-numbering',
                level: 0,
            },
            children: [new TextRun({text:mas3[i],size: shsize})],
        }));}    
    
    word_itog.push(new Paragraph({
        children: [new TextRun({text:'Персоналия упомянута в записях у ',break:1,underline:{},size: shsize})],     
    }));

    for (let i = 0; i < mas5.length;i++) {
        word_itog.push(new Paragraph({
            tabStops: [
                {
                    type: TabStopType.LEFT,
                    position: TabStopPosition.LEFT,// is the distance from the left side.
                },
            ],
            children: [new TextRun({text:'\t'+mas5[i],size: shsize})],    
        }));   
    }
        
    word_itog.push(new Paragraph({
        children: [new TextRun({text:'Информанты ',break:1,underline:{},size: shsize})],     
    }));
    
    for (let i = 0; i < mas6.length;i++) {
        word_itog.push(new Paragraph({
            tabStops: [
                {
                    type: TabStopType.LEFT,
                    position: TabStopPosition.LEFT,// is the distance from the left side.
                },
            ],
            children: [new TextRun({text:'\t'+mas6[i],size: shsize})],    
        }));   
    }

    // Create document

    const doc = new Document(
    
        {   styles: {
            paragraphStyles: [
                {
                    id: 'Heading1',
                    name: 'Heading 1',
                    basedOn: 'Normal',
                    next: 'Normal',
                    quickFormat: true,
                    run: {
                        size: shsize,
                        bold: true,
                        italics: true,
                        color: 'red',
                    },
                    paragraph: {
                        spacing: {
                            after: 120,
                        },
                    },
                }]},
        numbering: {
            config: [
                {
                    reference: 'my-crazy-numbering4',
                    levels: [
                        {
                            level: 0,
                            format: LevelFormat.DECIMAL,//.UPPER_ROMAN,
                            text: '%1.',
                            alignment: AlignmentType.START,
                            style: {
                                run:{
                                    size:shsize},
                                paragraph: {
                                    indent: { left: convertInchesToTwip(0.5), hanging: convertInchesToTwip(0.18) },   
                                },
                            },
                        }]},
                {
                    reference: 'my-crazy-numbering3',
                    levels: [
                        {
                            level: 0,
                            format: LevelFormat.DECIMAL,//.UPPER_ROMAN,
                            text: '%1.',
                            alignment: AlignmentType.START,
                            style: {
                                run:{
                                    size:shsize},
                                paragraph: {
                                    indent: { left: convertInchesToTwip(0.5), hanging: convertInchesToTwip(0.18) },
                                },
                            }
                        }]},
                
                {
                    reference: 'my-crazy-numbering2',
                    levels: [
                        {
                            level: 0,
                            format: LevelFormat.DECIMAL,//.UPPER_ROMAN,
                            text: '%1.',
                            alignment: AlignmentType.START,
                            style: {
                                run:{
                                    size:shsize},
                                paragraph: {
                                    indent: { left: convertInchesToTwip(0.5), hanging: convertInchesToTwip(0.18) },
                                },
                            }
                        }]},
                {
                    reference: 'my-crazy-numbering',
                    levels: [
                        {
                            level: 0,
                            format: LevelFormat.DECIMAL,//.UPPER_ROMAN,
                            text: '%1.',
                            alignment: AlignmentType.START,
                            style: {
                                run:{
                                    size:shsize},
                                paragraph: {
                                    indent: { left: convertInchesToTwip(0.35), hanging: convertInchesToTwip(0.28) },
                                },
                            },
                        },
                    ],
                },
            ],
        },
        }    
    );
    //
    // Documents contain sections, you can have multiple sections per document, go here to learn more about sections
    // This simple example will only contain one section
    doc.addSection({
        properties: {},
        footers: {
            default: new Footer({
                children: [
                    new Paragraph({
                        tabStops: [
                            {
                                type: TabStopType.RIGHT,
                                position: TabStopPosition.MAX-3,// is the distance from the left side.
                            },
                        ],
                        children:[
                            new TextRun({children:['ID: '+data_code]}),
                            new TextRun({children:['\tСтраница ',PageNumber.CURRENT],}),
                            new TextRun({children:[' из ',PageNumber.TOTAL_PAGES]}),]}),
                ],
            }),
        },
        children:word_itog,
        
    });

    return doc;

}

function word_formation(data){

    let fio=''; 
    let d=data;
    let mulcell='';

    if (d.fund) mulcell = mulcell + 'Ф. ' + d.fund;
    else mulcell = mulcell + 'Ф. -';
    if (d.list_no) mulcell = mulcell + ' Оп. ' + d.list_no;
    else mulcell = mulcell + ' Оп. -';
    if (d.file_no) mulcell = mulcell + ' Д. ' + d.file_no;
    else mulcell = mulcell + ' Д. -';
    let word_mas1=[];
    word_mas1.push((mulcell+' (ID: '+d.code+')').toUpperCase());
    mulcell = '';
    // Заполенение ФИО
    let fio_array=[];
    if (d.surname) {fio_array.push(d.surname);}
    if (d.fname) {fio_array.push(d.fname);}
    if (d.lname) {fio_array.push(d.lname);}
    fio = fio_array.join(' ');
    word_mas1.push(fio.toUpperCase());
    let word_mas2=[];
    // Конец заполнения ФИО
    // Заполенение ФИО комментарий
    fio_array=[];
    if (d.surname_rmk) {fio_array.push(d.surname_rmk);}
    else {fio_array.push('-');}
    if (d.fname_rmk) {fio_array.push(d.fname_rmk);}
    else {fio_array.push('-');}
    if (d.lname_rmk) {fio_array.push(d.lname_rmk);}
    else {fio_array.push('-');}
    if ((d.surname_rmk) || (d.fname_rmk) || (d.lname_rmk)) {
        fio = '(' + fio_array.join('; ') + ')';
    }
    else {
        fio = '';
    }
    // Конец заполнения ФИО комментарий
    let in_str='';
    // Дата рождения
    if ((d.birth) && (d.birth_rmk)) {
        in_str=d.birth+' ('+d.birth_rmk+')';
    }
    else if (d.birth) {
        in_str=d.birth;
    }
    else if (d.birth_rmk) {
        in_str = d.birth_rmk;
    }
    if (d.birth_place_name || d.birth_place_rmk) {
        if ((d.birth) || (d.birth_rmk)) {
            in_str = in_str + ', ';
        }
        if (d.birth_place_name) {
            in_str = in_str + d.birth_place_name;
        }
        if (d.birth_place_rmk) {
            in_str = in_str + ' / ' + d.birth_place_rmk;
        }
    }
    word_mas2.push(in_str.toUpperCase());
    // Конец даты рождения
    // Дата
    in_str = '';
    if ((d.death) && (d.death_date_rmk)) {
        in_str=d.death+' ('+d.death_date_rmk+')';
    }
    else if (d.death) {
        in_str=d.death;
    }
    else if (d.death_date_rmk) {
        in_str = d.death_date_rmk;
    }
    word_mas2.push(in_str.toUpperCase());
    // Конец даты смерти
    // Национальность
    in_str='';
    if (d.nation_info) {in_str=d.nation_info; }
    if (d.nation_rmk){in_str=in_str+' ('+d.nation_rmk+')';}
    word_mas2.push(in_str.toUpperCase()); 
    // Конец национальность
    // Политические организации
    in_str='';   
    if (d.org!==null){
        for (let iorg of d.org) {       
            let org_arr= [];         
            if (iorg.org_name) {org_arr.push(iorg.org_name);}
            if (iorg.type_particip){org_arr.push(iorg.type_particip);}
            let in_str_loc = '';
            if ((iorg.join_dat) && (iorg.join_dat_rmk)) {
                in_str_loc=iorg.join_dat+' / '+iorg.join_dat_rmk;
            }
            else if (iorg.join_dat) {
                in_str_loc=iorg.join_dat;
            }
            else if (iorg.join_dat_rmk) {
                in_str_loc = iorg.join_dat_rmk;
            }
            else {
                in_str_loc = '-';
            }
            in_str_loc = in_str_loc + ' - ';
            if ((iorg.dismis_dat) && (iorg.dismis_dat_rmk)) {
                in_str_loc=in_str_loc + iorg.dismis_dat+' / '+iorg.dismis_dat_rmk;
            }
            else if (iorg.dismis_dat) {
                in_str_loc= in_str_loc + iorg.dismis_dat;
            }
            else if (iorg.dismis_dat_rmk) {
                in_str_loc = in_str_loc + iorg.dismis_dat_rmk;
            }
            else {
                in_str_loc = in_str_loc + '-';
            }

            org_arr.push(in_str_loc);

            in_str=in_str + org_arr.join(', ')+'; ';
        }  
    }
    word_mas2.push(in_str.toUpperCase());
    // Конец политических организаций
    // Работа во время репрессий
    in_str='';
    if (d.activs!==null){
        for (let iorg1 of d.activs) { 
            if (iorg1.ent1_rmk || iorg1.post || iorg1.post_rmk || iorg1.prof || iorg1.prof_rmk) {         
                if (iorg1.ent1_rmk) {in_str=in_str+iorg1.ent1_rmk;}
                else {in_str=in_str+'-';}
                if (iorg1.post || iorg1.post_rmk) {
                    if (iorg1.post){in_str=in_str+', '+iorg1.post;}  
                    else {in_str=in_str+', -';}                
                    if (iorg1.post_rmk) {in_str=in_str + ' / ' +iorg1.post_rmk;}
                }
                else if (iorg1.prof || iorg1.prof_rmk) {
                    if (iorg1.prof){in_str=in_str+', '+iorg1.prof;}  
                    else {in_str=in_str+', -';}                
                    if (iorg1.prof_rmk) {in_str=in_str + ' / ' +iorg1.prof_rmk;}
                }
                in_str=in_str+'; '; 
            }  
        }
    }
    word_mas2.push(in_str.toUpperCase());
    // Конец работы
    // Место жительства репрессий
    in_str='';
    let word_repr=[];
    if (d.repress!==null){ 
        let in_i=1;       
        for (let iorg of d.repress) {              
            if (iorg.geoplace_code_rmk  || iorg.place_name_on_repress){ 
                in_str=in_str+'№'+in_i+' ';   
                if (iorg.place_name_on_repress) {in_str=in_str+iorg.place_name_on_repress;
                    if (iorg.geoplace_code_rmk) {in_str=in_str+' / ';}}
                if (iorg.geoplace_code_rmk) {in_str=in_str+iorg.geoplace_code_rmk;}
                in_str=in_str+'; ';
                in_i++;               
            }  
        }
    }
    word_mas2.push(in_str.toUpperCase());
    // Конец мест жительства
    mulcell='';
    let reab = [];
    for (let r of data.repress) {
        mulcell='';
        if (r.repress_no) {mulcell=mulcell+'№'+r.repress_no;}
        if (r.repress_type)   {mulcell=mulcell+' '+r.repress_type;}
        if (r.repress_dat || r.repress_dat_rmk){
            mulcell = mulcell + ' - ';
            let in_str_loc='';
            if ((r.repress_dat) && (r.repress_dat_rmk)) {
                in_str_loc=r.repress_dat+' / '+r.repress_dat_rmk;
            }
            else if (r.repress_dat) {
                in_str_loc=r.repress_dat;
            }
            else {
                in_str_loc = r.repress_dat_rmk;
            }
            mulcell=mulcell+in_str_loc; 
        }

        if (r.courts!==null) {
            let crt_arr = [];
            for (let crt of r.courts) {
                let loc_str = '';
                if (crt.article || crt.article_rmk) {
                    loc_str = loc_str + 'Ст. ';
                    if (crt.article) {
                        loc_str = loc_str + crt.article;
                    }
                    else {
                        loc_str = loc_str + '-';
                    }
                    if (crt.article_rmk) {loc_str = loc_str + ' / '+ crt.article_rmk;}
                }
                let has = false;
                if (crt.sentense || crt.sentense_rmk || crt.sentense_date || crt.sentense_date_rmk) {
                    loc_str = loc_str +', Приговор: ';
                    if ((crt.sentense_date) && (crt.sentense_date_rmk)) {
                        loc_str=loc_str + crt.sentense_date+' / '+crt.sentense_date_rmk+' ';
                    }
                    else if (crt.sentense_date) {
                        loc_str= loc_str + crt.sentense_date+' ';
                    }
                    else if (crt.sentense_date_rmk) {
                        loc_str = loc_str + crt.sentense_date_rmk+' ';
                    }
                    let loc_prig = '';
                    if (crt.sentense) {
                        loc_str = loc_str + crt.sentense;
                        loc_prig = crt.sentense.toUpperCase();
                    }
                    else {
                        loc_str = loc_str + '-';
                    }
                    if (crt.sentense_rmk) {
                        loc_str = loc_str + ' / '+ crt.sentense_rmk;
                        loc_prig = loc_prig+ ' '+ crt.sentense_rmk.toUpperCase();
                    }
                    has = loc_prig.includes('ВМН') || loc_prig.includes('РАССТРЕЛ');
                }
                if (has) {
                    loc_str = loc_str + ', ';
                    if ((d.death) && (d.death_date_rmk)) {
                        loc_str=loc_str + d.death+' / '+d.death_date_rmk;
                    }
                    else if (d.death) {
                        loc_str=loc_str + d.death;
                    }
                    else if (d.death_date_rmk) {
                        loc_str = loc_str + d.death_date_rmk;
                    }
                }
                else {
                    if (crt.impris!==null) {
                        let imp_arr = [];
                        let osv_date ='';
                        let i_reason ='';
                        for (let imp of crt.impris) {
                            let i_str = ' ';
                            if (imp.impris_type) {
                                i_str = i_str+ imp.impris_type;
                            }
                            else {
                                i_str = i_str + '-';
                            }
                            i_str = i_str+ ' - ';
                            if ((imp.prison_name) && (imp.prison_name_rmk)) {
                                i_str=i_str + imp.prison_name+' / '+imp.prison_name_rmk;
                            }
                            else if (imp.prison_name) {
                                i_str=i_str + imp.prison_name;
                            }
                            else if (imp.prison_name_rmk) {
                                i_str = i_str + imp.prison_name_rmk;
                            }
                            else {
                                i_str = i_str + '-';
                            }
                            if (imp.place_name) {
                                i_str = i_str +  ', ' + imp.place_name;
                            }
                            if (imp.prison_place_rmk) {
                                i_str = i_str + ', ' + imp.prison_place_rmk;
                            }
                            
                            imp_arr.push(i_str);
                            if (imp.leave_dat) {osv_date = imp.leave_dat;}
                            else if (imp.leave_dat_rmk) {osv_date = imp.leave_dat_rmk;}
                            else {osv_date = ' -';}
                            if (imp.reason) {i_reason = imp.reason;}
                            else {i_reason = '';}
                        }
                        loc_str = loc_str + ', Место пребывания:' + imp_arr.join(', ') + ', До: '+osv_date;
                        if (i_reason!=='') {loc_str = loc_str + ', ' + i_reason;}
                    }
                }
                if (loc_str !== '-') {
                    crt_arr.push(loc_str);
                }
            }
            mulcell=mulcell+', '+ crt_arr.join('; ')+';';    
        }
        
        // Заполнение массива реабилитаций
        if (r.reabil_mark=='*') {
            let reabstr = '';
            if (r.reabil_dat)   {
                reabstr=reabstr+r.reabil_dat;
                if (r.reabil_rmk) {reabstr=reabstr+' / ';}  
            }
            if (r.reabil_rmk)   {
                reabstr=reabstr+r.reabil_rmk;  
            }
            reab.push(reabstr);
        }

        // Конец Заполнения массива реабилитаций
        word_repr.push(mulcell.toUpperCase());   //mulcell=mulcell+'\n';  
    }
    //Вставка данных по реабилитации
    let reabil_str = '';
    if (reab.length>0) {
        reabil_str = reab.join('; ');
    } 
    word_mas2.push(reabil_str.toUpperCase());
    // Конец вставки данных по реабилитации
    if (d.family!==null){
        fio='';
        let pers_list = [];
        for (let fam of d.family) { 
            let pers_item = [];
            if (fam.surname) { pers_item.push(fam.surname.trim());}             
            if (fam.fname) {pers_item.push(fam.fname.trim());}
            if (fam.lname){pers_item.push(fam.lname.trim());}
            if ((fam.role!=null)&&(fam.role!=='')){pers_item.push('('+fam.role.trim()+')');}
            if (fam.fund || fam.list_no || fam.file_no) {
                let shift = [];
                if (fam.fund) {shift.push(fam.fund);} else {shift.push('-');}
                if (fam.list_no) {shift.push(fam.list_no);} else {shift.push('-');}
                if (fam.file_no) {shift.push(fam.file_no);} else {shift.push('-');}
                pers_item.push('- '+shift.join('-').trim());
            }
            pers_list.push(pers_item.join(' '));
        }
        fio = pers_list.join('; ');
        word_mas2.push(fio.toUpperCase());
    }
    let i=3;
    //let li = 2;
    let word_mas3=[];
    let word_mas4=[];
    if (d.sour!==null){
        let ref_del = [];
        let repl_arr = [
            '(восп)омин',
            '(реаб)',
            '(арх)ив',
            '(лаг)е',
            '(мем)ор',
            '(прокур)',
            '(учет)',
            '(призн)ан',
            '(прострад)а',
            '(репр)ес',
            '(пост)ан',
            '(избр)ан',
            '(пресеч)',
            '(сопр)ов',
            '(закл)юч',
            '(фрагме)нт'
        ];
        let sour_n = 0;
        for (let iorg of d.sour) {
            if (i>22) {
                //li++;
                i = 3;
            }
            sour_n++;
            iorg.sour_no = sour_n;
            if  (((iorg.fund==d.fund)&&(iorg.list_no==d.list_no)&&(iorg.file_no==d.file_no)) || iorg.is_in_extra || d.no_shifr ){
                fio = '';
                if (iorg.sour_name){
                    let s_name = iorg.sour_name;
                    for (let ex of repl_arr) {
                        s_name = s_name.replace(new RegExp('(^|[^а-я])'+ex+'[а-я]*([^а-я]|$)', 'gi'),'$1$2.$3');
                    }
                    fio=fio+s_name;
                }
                if (iorg.sour_type){fio=fio+' ('+iorg.sour_type+') ';}
                else {fio=fio+'(-)';}
                if (iorg.sour_code) {fio = fio + iorg.sour_code + ' л. ';}
                if (iorg.sour_date) {fio=fio+iorg.sour_date+' ';}
                if (iorg.sour_mark=='О') {fio=fio+'Оригинал';}
                if (iorg.sour_mark=='К') {fio=fio+'Копия';}
                //// Вставка архивного шифра
                let ref_str = '';
                if (iorg.fund) {
                    ref_str = iorg.fund;
                }
                else {
                    ref_str = '';
                }
                if (iorg.list_no) {
                    ref_str = ref_str+ ' - ' +iorg.list_no;
                }
                else {
                    ref_str = ref_str+ ' - ';
                }
                if (iorg.file_no) {
                    ref_str = ref_str+ ' - ' +iorg.file_no;
                }
                else {
                    ref_str = ref_str+ ' - ';
                }
                if ((ref_str!=' -  - ') && !((iorg.fund==d.fund)&&(iorg.list_no==d.list_no)&&(iorg.file_no==d.file_no)) ) {
                    fio=fio+' Хранение: ' + ref_str;
                }
                word_mas3.push(fio.trim());//.toUpperCase());  
                i++;
            }
            else {
                let ref_str = '';
                if (iorg.fund) {
                    ref_str = iorg.fund;
                }
                else {
                    ref_str = '-';
                }
                if (iorg.list_no) {
                    ref_str = ref_str+ ' ' +iorg.list_no;
                }
                else {
                    ref_str = ref_str+ ' -';
                }
                if (iorg.file_no) {
                    ref_str = ref_str+ ' ' +iorg.file_no;
                }
                else {
                    ref_str = ref_str+ ' -';
                }
                if ((ref_str!='- - -')&&(!ref_del.includes(ref_str))) {
                    ref_del.push(ref_str);
                //    word_mas4.push(ref_del.toUpperCase());
                }
            }
        } /*
        if (ref_del.length>0) {
            let ref_str = 'Также см. ' + ref_del.join('; ');
        } */
    }

    // дополнительные дела
    //let word_mas7=[];
    if (d.dopdela!==null){
        
        for (let mnt of d.dopdela) {     
            fio = '';  
            let prs_item = [];       
            if (mnt.fund) {prs_item.push('Ф.'+mnt.fund+'.');}         
            if (mnt.list_no) {prs_item.push('Оп.'+mnt.list_no+'.');}
            if (mnt.file_no) {prs_item.push('Д.'+mnt.file_no);}
            //if (mnt.file_no_ext){prs_item.push(mnt.file_no_ext);}
            fio = prs_item.join(' ') ;
            word_mas4.push(fio);
        }
    }
    //
    //console.log('11');
    let word_mas5=[];
    if (d.mention!==null){
        if (i!==3) {
            i++;
            if (i>20) {
                //++;
                i = 3;
            }
        }
        for (let mnt of d.mention) {
            if (i>22) {
                //li++;
                i = 3;
            } 
            fio = '';  
            let prs_item = [];    
            if (mnt.surname) {prs_item.push(mnt.surname.trim());}         
            if (mnt.fname) {prs_item.push(mnt.fname.trim());}
            if (mnt.lname){prs_item.push(mnt.lname.trim());}
            
            if (mnt.in_family_t){
                if (mnt.family_role!==null) 
                {prs_item.push('('+mnt.family_role.trim()+')');}
                else
                {prs_item.push('(родств.)');}
            }
            if (mnt.fund || mnt.list_no || mnt.file_no) {
                let shift = [];
                if (mnt.fund) {shift.push(mnt.fund);} else {shift.push('-');}
                if (mnt.list_no) {shift.push(mnt.list_no);} else {shift.push('-');}
                if (mnt.file_no) {shift.push(mnt.file_no);} else {shift.push('-');}
                prs_item.push('- '+shift.join('-').trim());
            }
            fio = prs_item.join(' ') + ';';
            word_mas5.push(fio.toUpperCase());
            i++;
        }
    }
    //console.log('12');
    let word_mas6=[];
    if (d.inform!==null){
        i++;
        if (i>22) {
            //li++;
            i = 3;
        }

        for (let inf of d.inform) {
            if (i>22) {
                //li++;
                i = 3;
            }
            if ((i==22) && ((inf.address) || (inf.address_rmk) || (inf.address_city) )){
                //li++;
                i = 3;   
            }
            if ((i==22) && (!inf.address) && (!inf.address_rmk) && (!inf.address_city)){
                fio = '';       
                let prs_item = [];        
                if (inf.surname) {prs_item.push(inf.surname.trim());}
                if (inf.fname) {prs_item.push(inf.fname.trim());}
                if (inf.lname){prs_item.push(inf.lname.trim());}
                if (inf.role) {prs_item.push('('+inf.role+')');}
                fio = prs_item.join(' ');
                if (inf.phone || inf.email) {
                    if (inf.phone){fio=fio+' - '+inf.phone.trim();}
                    if (inf.email) {fio=fio+' - '+inf.email.trim();}
                }
                if (fio !== '') fio = fio + ';';
                word_mas6.push(fio.toUpperCase());
                i++;
                continue;
            }
            if (i<22) {
                fio = '';       
                let prs_item = [];        
                if (inf.surname) {prs_item.push(inf.surname.trim());}
                if (inf.fname) {prs_item.push(inf.fname.trim());}
                if (inf.lname){prs_item.push(inf.lname.trim());}
                if (inf.role) {prs_item.push('('+inf.role+')');}
                fio = prs_item.join(' ');
                if (inf.phone || inf.email) {
                    if (inf.phone){fio=fio+' - '+inf.phone.trim();}
                    if (inf.email) {fio=fio+' - '+inf.email.trim();}
                }
                if (fio !== '') fio = fio + ';';
                word_mas6.push(fio.toUpperCase());
                i++;
                if ((inf.address)||(inf.address_rmk)|| (inf.address_city)){
                    fio = '';       
                    let prs_item1 = [];        
                    if (inf.address) {prs_item1.push(inf.address.trim());}
                    if (inf.address_rmk) {prs_item1.push(inf.address_rmk.trim());}
                    
                    fio = prs_item1.join(' / ');
                    if ((inf.address_city) && (fio)) {
                        fio = inf.address_city +', '+ fio;
                    }
                    else if (inf.address_city) {
                        fio = inf.address_city;
                    }
                    if (fio !== '') fio = fio + ';';
                    word_mas6.push(fio.toUpperCase());
                    i++;
                }
            }    
        }
    }

    return doc_formation(word_mas1,word_mas2,word_mas3,word_mas4,word_mas5,word_mas6,word_repr, data.code);

}

module.exports = word_formation;