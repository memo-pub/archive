
function excel_formation(data,wb){
    let opt={
        pageSetup: {
            orientation:'landscape',
            //paperHeight:'100.0mm',
            //paperWidth:'150.0mm',
            paperSize: 'JAPANESE_DOUBLE_POSTCARD',
            fitToWidth:1,
            fitToHeight:1} ,
        margins: {
            left: 0.0,
            right: 0.0,
            bottom: 0.0,
            top: 0.0,
            footer: 0,
            header: 0
        }
    };

    let ws_arr = [];
    let ws = wb.addWorksheet('Лист 1',opt);
    ws.cell(1,5).string(data.code).style({font:{size:6},
        alignment: {vertical:'bottom',horizontal: 'right'}});
    ws_arr.push(ws);
    let ws1 = wb.addWorksheet('Лист 2',opt);
    ws1.cell(1,5).string(data.code).style({font:{size:6},
        alignment: {vertical:'bottom',horizontal: 'right'}});
    ws_arr.push(ws1);
    //vertical: ['bottom', 'center', 'distributed', 'justify', 'top'],
    let style = wb.createStyle({
        font: {
            size:8,
            color: '#000000',
            bold: true,
        },
        alignment: {
            wrapText: true,
            horizontal: 'left',
            vertical: 'top'}
    });
    let style2 = wb.createStyle({
        font: {
            size:10,
            color: '#000000',
            bold: true,
        },
        alignment: {
            wrapText: true,
            horizontal: 'left',
            vertical: 'top'}
    });
    
    var myStyle = wb.createStyle({
        font: {
            size:8,
            bold: false,
            underline: false,
        },
        alignment: {
            wrapText: true,
            horizontal: 'left',
            vertical: 'top'},
    });

    setWidth(ws);

    ws.row(1).setHeight(10.8);//17
    ws.row(2).setHeight(16.8);
    ws.row(3).setHeight(13.2);
    ws.row(4).setHeight(12.6);
    ws.row(5).setHeight(36.4);//род. - ум.   было 16.2/12 cowrier 
    ws.row(6).setHeight(15);//нац. - парт.
    ws.row(7).setHeight(25.8);//работа
    ws.row(8).setHeight(22.2);//жительство
    ws.row(9).setHeight(82.0);//repress
    ws.row(10).setHeight(16.2); //reabil
    ws.row(11).setHeight(62.4);//rchs

    
    ws.cell(3, 1).string('1.ФИО.').style(style);
    ws.cell(5, 1).string('2.Род.').style(style);
    ws.cell(5, 4).string('Ум.').style(style);
    ws.cell(6, 1).string('3.Нац.').style(style);
    ws.cell(6, 3).string('4.Парт.').style(style);
    ws.cell(7, 1).string('5.Раб.').style(style);
    ws.cell(8, 1).string('6.Жит.').style(style);
    ws.cell(9, 1).string('7.Репр.').style(style);
    ws.cell(10, 1).string('8.Реаб.').style(style);
    ws.cell(11, 1).string('9.Р.ч.с.').style(style);
    ws.addPageBreak('row', 12);
    ws.addPageBreak('column', 7);
    ws.setPrintArea(1, 1, 11, 6) ;

    setWidth1(ws1);

    ws1.cell(1,1,25,6).style(style); 
    ws1.cell(1,5).string(data.code).style({font:{size:6},
        alignment: {vertical:'bottom',horizontal: 'right'}});   
    //console.log('1');

    let fio=''; 
    let d=data;
    let mulcell='';

    if (d.fund) mulcell = mulcell + 'Ф. ' + d.fund;
    else mulcell = mulcell + 'Ф. -';
    if (d.list_no) mulcell = mulcell + ' Оп. ' + d.list_no;
    else mulcell = mulcell + ' Оп. -';
    if (d.file_no) mulcell = mulcell + ' Д. ' + d.file_no;
    else mulcell = mulcell + ' Д. -';
    ws.cell(2, 2, 2, 4, true).string(mulcell.toUpperCase()).style(style);
    mulcell = '';
    
    // Заполенение ФИО
    let fio_array=[];
    if (d.surname) {fio_array.push(d.surname);}
    if (d.fname) {fio_array.push(d.fname);}
    if (d.lname) {fio_array.push(d.lname);}
    fio = fio_array.join(' ');
    ws.cell(3, 2, 3, 6, true).string(fio.toUpperCase()).style(style2);
    // Конец заполнения ФИО

    // Заполенение ФИО комментарий
    fio_array=[];
    if (d.surname_rmk) {fio_array.push(d.surname_rmk);}
    else {fio_array.push('-');}
    if (d.fname_rmk) {fio_array.push(d.fname_rmk);}
    else {fio_array.push('-');}
    if (d.lname_rmk) {fio_array.push(d.lname_rmk);}
    else {fio_array.push('-');}
    if ((d.surname_rmk) || (d.fname_rmk) || (d.lname_rmk)) {
        fio = '(' + fio_array.join('; ') + ')';
    }
    else {
        fio = '';
    }
    ws.cell(4, 2, 4, 6, true).string(fio.toUpperCase()).style(myStyle);
    // Конец заполнения ФИО комментарий
    //console.log('2');
    
    let in_str='';
    // Дата рождения
    if ((d.birth) && (d.birth_rmk)) {
        in_str=d.birth+' ('+d.birth_rmk+')';
    }
    else if (d.birth) {
        in_str=d.birth;
    }
    else if (d.birth_rmk) {
        in_str = d.birth_rmk;
    }
    if (d.birth_place_name || d.birth_place_rmk) {
        if ((d.birth) || (d.birth_rmk)) {
            in_str = in_str + ', ';
        }
        if (d.birth_place_name) {
            in_str = in_str + d.birth_place_name;
        }
        if (d.birth_place_rmk) {
            in_str = in_str + ' / ' + d.birth_place_rmk;
        }
    }
    ws.cell(5, 2, 5, 3, true).string(in_str.toUpperCase()).style(myStyle);
    // Конец даты рождения

    //console.log('3');

    // Дата
    in_str = '';
    if ((d.death) && (d.death_date_rmk)) {
        in_str=d.death+' ('+d.death_date_rmk+')';
    }
    else if (d.death) {
        in_str=d.death;
    }
    else if (d.death_date_rmk) {
        in_str = d.death_date_rmk;
    }
    ws.cell(5, 5, 5, 6, true).string(in_str.toUpperCase()).style(myStyle);
    // Конец даты
    
    // Национальность
    in_str='';
    if (d.nation_info) {in_str=d.nation_info; }
    if (d.nation_rmk){in_str=in_str+' ('+d.nation_rmk+')';}
    ws.cell(6, 2).string(in_str.toUpperCase()).style(myStyle); 
    // Конец национальность
    //console.log('4');
    // Политические организации
    in_str='';   
    if (d.org!==null){
        for (let iorg of d.org) {       
            let org_arr= [];         
            if (iorg.org_name) {org_arr.push(iorg.org_name);}
            if (iorg.type_particip){org_arr.push(iorg.type_particip);}
            let in_str_loc = '';
            if ((iorg.join_dat) && (iorg.join_dat_rmk)) {
                in_str_loc=iorg.join_dat+' / '+iorg.join_dat_rmk;
            }
            else if (iorg.join_dat) {
                in_str_loc=iorg.join_dat;
            }
            else if (iorg.join_dat_rmk) {
                in_str_loc = iorg.join_dat_rmk;
            }
            else {
                in_str_loc = '-';
            }
            in_str_loc = in_str_loc + ' - ';
            if ((iorg.dismis_dat) && (iorg.dismis_dat_rmk)) {
                in_str_loc=in_str_loc + iorg.dismis_dat+' / '+iorg.dismis_dat_rmk;
            }
            else if (iorg.dismis_dat) {
                in_str_loc= in_str_loc + iorg.dismis_dat;
            }
            else if (iorg.dismis_dat_rmk) {
                in_str_loc = in_str_loc + iorg.dismis_dat_rmk;
            }
            else {
                in_str_loc = in_str_loc + '-';
            }

            org_arr.push(in_str_loc);

            in_str=in_str + org_arr.join(', ')+'; ';
        }
       
    }
    ws.cell(6, 4, 6, 6, true).string(in_str.toUpperCase()).style(myStyle);
    // Конец политических организаций
    //console.log('5');
    // Работа во время репрессий
    in_str='';
    if (d.activs!==null){
        for (let iorg1 of d.activs) { 
            if (iorg1.ent1_rmk || iorg1.post || iorg1.post_rmk || iorg1.prof || iorg1.prof_rmk) {         
                if (iorg1.ent1_rmk) {in_str=in_str+iorg1.ent1_rmk;}
                else {in_str=in_str+'-';}
                if (iorg1.post || iorg1.post_rmk) {
                    if (iorg1.post){in_str=in_str+', '+iorg1.post;}  
                    else {in_str=in_str+', -';}                
                    if (iorg1.post_rmk) {in_str=in_str + ' / ' +iorg1.post_rmk;}
                }
                else if (iorg1.prof || iorg1.prof_rmk) {
                    if (iorg1.prof){in_str=in_str+', '+iorg1.prof;}  
                    else {in_str=in_str+', -';}                
                    if (iorg1.prof_rmk) {in_str=in_str + ' / ' +iorg1.prof_rmk;}
                }
                //if (iorg1.arrival_dat!==null) {fio=fio+iorg1.arrival_dat+';';} 
                in_str=in_str+'; '; 
            }  
        }
    }
    ws.cell(7, 2, 7, 6, true).string(in_str.toUpperCase()).style(myStyle);
    // Конец работы
    //console.log('6');
    // Место жительства репрессий
    in_str='';
    if (d.repress!==null){        
        for (let iorg of d.repress) {              
            if (iorg.geoplace_code_rmk  || iorg.place_name_on_repress){ 
                if (iorg.place_name_on_repress) {in_str=in_str+iorg.place_name_on_repress;
                    if (iorg.geoplace_code_rmk) {in_str=in_str+' / ';}}
                if (iorg.geoplace_code_rmk) {in_str=in_str+iorg.geoplace_code_rmk;}
                in_str=in_str+'; ';
                //if (iorg.place_code_on_repress) {in_str=in_str+iorg.place_code_on_repress+', ';}
                //if (iorg.place_num_on_repress){in_str=in_str+iorg.place_num_on_repress+'; ';}                
            }
            //fio=fio+'\n';  
        }
    }
    ws.cell(8, 2, 8, 6, true).string(in_str.toUpperCase()).style(myStyle);
    // Конец мест жительства
    //console.log('7');
    mulcell='';
    let reab = [];
    for (let r of data.repress) {
        if (r.repress_no) {mulcell=mulcell+'№'+r.repress_no;}
        if (r.repress_type)   {mulcell=mulcell+' '+r.repress_type;}

        if (r.repress_dat || r.repress_dat_rmk) {
            mulcell = mulcell + ' - ';
            let in_str_loc='';
            if ((r.repress_dat) && (r.repress_dat_rmk)) {
                in_str_loc=r.repress_dat+' / '+r.repress_dat_rmk;
            }
            else if (r.repress_dat) {
                in_str_loc=r.repress_dat;
            }
            else {
                in_str_loc = r.repress_dat_rmk;
            }
            mulcell=mulcell+in_str_loc; 
        }

        if (r.courts!==null) {
            let crt_arr = [];
            for (let crt of r.courts) {
                let loc_str = '';
                if (crt.article || crt.article_rmk) {
                    loc_str = loc_str + 'Ст. ';
                    if (crt.article) {
                        loc_str = loc_str + crt.article;
                    }
                    else {
                        loc_str = loc_str + '-';
                    }
                    if (crt.article_rmk) {loc_str = loc_str + ' / '+ crt.article_rmk;}
                }
                let has = false;
                if (crt.sentense || crt.sentense_rmk || crt.sentense_date || crt.sentense_date_rmk) {
                    loc_str = loc_str +', Приговор: ';
                    ///
                    if ((crt.sentense_date) && (crt.sentense_date_rmk)) {
                        loc_str=loc_str + crt.sentense_date+' / '+crt.sentense_date_rmk+' ';
                    }
                    else if (crt.sentense_date) {
                        loc_str= loc_str + crt.sentense_date+' ';
                    }
                    else if (crt.sentense_date_rmk) {
                        loc_str = loc_str + crt.sentense_date_rmk+' ';
                    }
                    ///
                    let loc_prig = '';
                    if (crt.sentense) {
                        loc_str = loc_str + crt.sentense;
                        loc_prig = crt.sentense.toUpperCase();
                    }
                    else {
                        loc_str = loc_str + '-';
                    }
                    if (crt.sentense_rmk) {
                        loc_str = loc_str + ' / '+ crt.sentense_rmk;
                        loc_prig = loc_prig+ ' '+ crt.sentense_rmk.toUpperCase();
                    }
                    has = loc_prig.includes('ВМН') || loc_prig.includes('РАССТРЕЛ');
                }
                if (has) {
                    loc_str = loc_str + ', ';
                    if ((d.death) && (d.death_date_rmk)) {
                        loc_str=loc_str + d.death+' / '+d.death_date_rmk;
                    }
                    else if (d.death) {
                        loc_str=loc_str + d.death;
                    }
                    else if (d.death_date_rmk) {
                        loc_str = loc_str + d.death_date_rmk;
                    }
                }
                else {
                    if (crt.impris!==null) {
                        let imp_arr = [];
                        let osv_date ='';
                        let i_reason ='';
                        for (let imp of crt.impris) {
                            let i_str = ' ';
                            if (imp.impris_type) {
                                i_str = i_str+ imp.impris_type;
                            }
                            else {
                                i_str = i_str + '-';
                            }
                            i_str = i_str+ ' - ';
                            if ((imp.prison_name) && (imp.prison_name_rmk)) {
                                i_str=i_str + imp.prison_name+' / '+imp.prison_name_rmk;
                            }
                            else if (imp.prison_name) {
                                i_str=i_str + imp.prison_name;
                            }
                            else if (imp.prison_name_rmk) {
                                i_str = i_str + imp.prison_name_rmk;
                            }
                            else {
                                i_str = i_str + '-';
                            }
                            if (imp.place_name) {
                                i_str = i_str +  ', ' + imp.place_name;
                            }
                            if (imp.prison_place_rmk) {
                                i_str = i_str + ', ' + imp.prison_place_rmk;
                            }
                            
                            imp_arr.push(i_str);
                            if (imp.leave_dat) {osv_date = imp.leave_dat;}
                            else if (imp.leave_dat_rmk) {osv_date = imp.leave_dat_rmk;}
                            else {osv_date = ' -';}
                            if (imp.reason) {i_reason = imp.reason;}
                            else {i_reason = '';}
                        }
                        loc_str = loc_str + ', Место пребывания:' + imp_arr.join(', ') + ', До: '+osv_date;
                        if (i_reason!=='') {loc_str = loc_str + ', ' + i_reason;}
                    }
                }
                if (loc_str !== '-') {
                    crt_arr.push(loc_str);
                }
            }
            mulcell=mulcell+', '+ crt_arr.join('; ');
        }
        // Заполнение массива реабилитаций
        if (r.reabil_mark=='*') {
            let reabstr = '';
            if (r.reabil_dat)   {
                reabstr=reabstr+r.reabil_dat;
                if (r.reabil_rmk) {reabstr=reabstr+' / ';}  
            }
            if (r.reabil_rmk)   {
                reabstr=reabstr+r.reabil_rmk;  
            }
            reab.push(reabstr);
        }
        // Конец Заполнения массива реабилитаций

        mulcell=mulcell+'\n';    
    }
    //console.log('8');
    // Заполнение ячейки репрессии
    ws.cell(9, 2, 9, 6, true).string(mulcell.toUpperCase()).style(myStyle);
    
    //Вставка данных по реабилитации
    let reabil_str = '';
    if (reab.length>0) {
        reabil_str = reab.join('; ');
    }
    ws.cell(10, 2, 10, 6, true).string(reabil_str.toUpperCase()).style(myStyle); 
    // Конец вставки данных по реабилитации
    //console.log('9'); 
    
    ws.cell(11, 2, 11, 6, true).string('');
    if (d.family!==null){
        fio='';
        let pers_list = [];
        for (let fam of d.family) { 
            let pers_item = [];
            if (fam.surname) { pers_item.push(fam.surname.trim());}             
            if (fam.fname) {pers_item.push(fam.fname.trim());}
            if (fam.lname){pers_item.push(fam.lname.trim());}
            if ((fam.role!=null)&&(fam.role!=='')){pers_item.push('('+fam.role.trim()+')');}
            if (fam.fund || fam.list_no || fam.file_no) {
                let shift = [];
                if (fam.fund) {shift.push(fam.fund);} else {shift.push('-');}
                if (fam.list_no) {shift.push(fam.list_no);} else {shift.push('-');}
                if (fam.file_no) {shift.push(fam.file_no);} else {shift.push('-');}
                pers_item.push('- '+shift.join('-').trim());
            }
            pers_list.push(pers_item.join(' '));
        }
        fio = pers_list.join('; ');
        ws.cell(11, 2).string(fio.toUpperCase()).style(myStyle);
    }
    //console.log('10');
    let kol_sour=0;
    // подсчет kol_sour
    if (d.sour!==null){
        for (let iorg of d.sour) {
            if  (((iorg.fund==d.fund)&&(iorg.list_no==d.list_no)&&(iorg.file_no==d.file_no)) || iorg.is_in_extra || d.no_shifr ){
                kol_sour++;
            }
        }
    }
    // конец подсчета kol_sour
    let i=3;
    let li = 2;
    if (kol_sour<21){
        if (d.sour!==null){
            let ref_del = [];
            let repl_arr = [
                '(восп)омин',
                '(реаб)',
                '(арх)ив',
                '(лаг)е',
                '(мем)ор',
                '(прокур)',
                '(учет)',
                '(призн)ан',
                '(прострад)а',
                '(репр)ес',
                '(пост)ан',
                '(избр)ан',
                '(пресеч)',
                '(сопр)ов',
                '(закл)юч',
                '(фрагме)нт'
            ];
            let sour_n = 0;
            for (let iorg of d.sour) {
                if (i>22) {
                    li++;
                    ws1 = wb.addWorksheet('Лист '+li,opt);
                    ws1.cell(1,5).string(data.code).style({font:{size:6},
                        alignment: {vertical:'bottom',horizontal: 'right'}});   
                    ws_arr.push(ws1);
                    setWidth1(ws1);
                    i = 3;
                }
                sour_n++;
                iorg.sour_no = sour_n;
                if  (((iorg.fund==d.fund)&&(iorg.list_no==d.list_no)&&(iorg.file_no==d.file_no)) || iorg.is_in_extra || d.no_shifr ){
                    fio = '';          
                    if (iorg.sour_no) {fio=fio+'№'+iorg.sour_no+' ';}
                    if (iorg.sour_name){
                        let s_name = iorg.sour_name;
                        for (let ex of repl_arr) {
                            s_name = s_name.replace(new RegExp('(^|[^а-я])'+ex+'[а-я]*([^а-я]|$)', 'gi'),'$1$2.$3');
                        }
                        //s_name = s_name.replace(/(^|[^а-я])воспомин[а-я]*([^а-я]|$)/ig,'восп.');
                        fio=fio+s_name;
                    }
                    if (iorg.sour_type){fio=fio+' ('+iorg.sour_type+') ';}
                    else {fio=fio+' (-) ';}
                    if (iorg.sour_code) {fio = fio + iorg.sour_code + ' л. ';}

                    if (iorg.sour_date) {fio=fio+iorg.sour_date+' ';}
                    if (iorg.sour_mark=='О') {fio=fio+'Оригинал';}
                    if (iorg.sour_mark=='К') {fio=fio+'Копия';}
                    //// Вставка архивного шифра
                    let ref_str = '';
                    if (iorg.fund) {
                        ref_str = iorg.fund;
                    }
                    else {
                        ref_str = '';
                    }
                    if (iorg.list_no) {
                        ref_str = ref_str+ ' - ' +iorg.list_no;
                    }
                    else {
                        ref_str = ref_str+ ' - ';
                    }
                    if (iorg.file_no) {
                        ref_str = ref_str+ ' - ' +iorg.file_no;
                    }
                    else {
                        ref_str = ref_str+ ' - ';
                    }
                    if ((ref_str!=' -  - ') && !((iorg.fund==d.fund)&&(iorg.list_no==d.list_no)&&(iorg.file_no==d.file_no)) ) {
                        fio=fio+' Хранение: ' + ref_str;
                    }
                    ////
                    ws1.row(i).setHeight(14.31);//источники 17
                    ws1.cell(i, 1, i, 6, true).string(fio.toUpperCase()).style(style);
                    i++;
                }
                else {
                    let ref_str = '';
                    if (iorg.fund) {
                        ref_str = iorg.fund;
                    }
                    else {
                        ref_str = '-';
                    }
                    if (iorg.list_no) {
                        ref_str = ref_str+ ' ' +iorg.list_no;
                    }
                    else {
                        ref_str = ref_str+ ' -';
                    }
                    if (iorg.file_no) {
                        ref_str = ref_str+ ' ' +iorg.file_no;
                    }
                    else {
                        ref_str = ref_str+ ' -';
                    }
                    if ((ref_str!='- - -')&&(!ref_del.includes(ref_str))) {
                        ref_del.push(ref_str);
                    }
                }
            }
            if (ref_del.length>0) {
                let ref_str = 'Также см. ' + ref_del.join('; ');
                ws_arr[1].cell(1,1,1,6,true).string(ref_str.toUpperCase()).style(style);
            }
        }
    }else{
        fio='Опись источников см. в каталоге А4';
        ws1.cell(i, 1, i, 6, true).string(fio).style(style);
        i++;
    }
    //console.log('11');
    if (d.mention!==null){
        if (i!==3) {
            i++;
            if (i>20) {
                li++;
                ws1 = wb.addWorksheet('Лист '+li,opt);
                ws1.cell(1,5).string(data.code).style({font:{size:6},
                    alignment: {vertical:'bottom',horizontal: 'right'}});   
                ws_arr.push(ws1);
                setWidth1(ws1);
                i = 3;
            }
            else {
                ws1.row(i-1).setHeight(14.31);
            }
        }
        ws1.row(i).setHeight(14.31);
        ws1.cell(i, 1).string('Упомин. ').style(style);
        // i++;
        for (let mnt of d.mention) {
            if (i>22) {
                li++;
                ws1 = wb.addWorksheet('Лист '+li,opt);
                ws1.cell(1,5).string(data.code).style({font:{size:6},
                    alignment: {vertical:'bottom',horizontal: 'right'}});   
                ws_arr.push(ws1);
                setWidth(ws1);
                i = 3;
            } 
            fio = '';  
            let prs_item = [];    
            if (mnt.surname) {prs_item.push(mnt.surname.trim());}         
            if (mnt.fname) {prs_item.push(mnt.fname.trim());}
            if (mnt.lname){prs_item.push(mnt.lname.trim());}
            
            if (mnt.in_family_t){
                if (mnt.family_role!==null) 
                {prs_item.push('('+mnt.family_role.trim()+')');}
                else
                {prs_item.push('(родств.)');}
            }
            if (mnt.fund || mnt.list_no || mnt.file_no) {
                let shift = [];
                if (mnt.fund) {shift.push(mnt.fund);} else {shift.push('-');}
                if (mnt.list_no) {shift.push(mnt.list_no);} else {shift.push('-');}
                if (mnt.file_no) {shift.push(mnt.file_no);} else {shift.push('-');}
                prs_item.push('- '+shift.join('-').trim());
            }
            fio = prs_item.join(' ') + ';';
            ws1.row(i).setHeight(14.31);//17
            ws1.cell(i, 2, i, 6, true).string(fio.toUpperCase()).style(style);
            i++;
        }
    }
    //console.log('12');

    if (d.inform!==null){
        i++;
        if (i>22) {
            li++;
            ws1 = wb.addWorksheet('Лист '+li,opt);
            ws_arr.push(ws1);
            setWidth(ws1);
            ws1.cell(1,5).string(data.code).style({font:{size:6},
                alignment: {vertical:'bottom',horizontal: 'right'}});   
            i = 3;
        }
        
        let  nach_cikl=0;
        for (let inf of d.inform) {
            nach_cikl++;
            if (i>22) {
                li++;
                ws1 = wb.addWorksheet('Лист '+li,opt);
                ws_arr.push(ws1);
                setWidth(ws1);
                ws1.cell(1,5).string(data.code).style({font:{size:6},
                    alignment: {vertical:'bottom',horizontal: 'right'}});   
                i = 3;
            }
            if (i!==3) {
                ws1.row(i-1).setHeight(14.31);
            }
            if ((i==22) && ((inf.address) || (inf.address_rmk) || (inf.address_city) )){
                li++;
                ws1 = wb.addWorksheet('Лист '+li,opt);
                ws_arr.push(ws1);
                setWidth(ws1);
                ws1.cell(1,5).string(data.code).style({font:{size:6},
                    alignment: {vertical:'bottom',horizontal: 'right'}});   
                i = 3;   
            }
            if ((i==22) && (!inf.address) && (!inf.address_rmk) && (!inf.address_city)){
                if (nach_cikl==1){
                    ws1.row(i).setHeight(14.31);
                    ws1.cell(i, 1).string('Инф. ').style(style);
                }
                fio = '';       
                let prs_item = [];        
                if (inf.surname) {prs_item.push(inf.surname.trim());}
                if (inf.fname) {prs_item.push(inf.fname.trim());}
                if (inf.lname){prs_item.push(inf.lname.trim());}
                if (inf.role) {prs_item.push('('+inf.role+')');}
                fio = prs_item.join(' ');
                if (inf.phone || inf.email) {
                    if (inf.phone){fio=fio+' - '+inf.phone.trim();}
                    if (inf.email) {fio=fio+' - '+inf.email.trim();}
                }
                if (fio !== '') fio = fio + ';';
                ws1.row(i).setHeight(14.31); //17
                ws1.cell(i, 2, i, 6, true).string(fio.toUpperCase()).style(style);
                i++;
                continue;
            }
            if (i<22) {
                if (nach_cikl==1){
                    ws1.row(i).setHeight(14.31);
                    ws1.cell(i, 1).string('Инф. ').style(style);
                }
                fio = '';       
                let prs_item = [];        
                if (inf.surname) {prs_item.push(inf.surname.trim());}
                if (inf.fname) {prs_item.push(inf.fname.trim());}
                if (inf.lname){prs_item.push(inf.lname.trim());}
                if (inf.role) {prs_item.push('('+inf.role+')');}
                fio = prs_item.join(' ');
                if (inf.phone || inf.email) {
                    if (inf.phone){fio=fio+' - '+inf.phone.trim();}
                    if (inf.email) {fio=fio+' - '+inf.email.trim();}
                }
                if (fio !== '') fio = fio + ';';
                ws1.row(i).setHeight(14.31); //17
                ws1.cell(i, 2, i, 6, true).string(fio.toUpperCase()).style(style);
                i++;
                if ((inf.address)||(inf.address_rmk)|| (inf.address_city)){
                    fio = '';       
                    let prs_item1 = [];        
                    //if (inf.address_city) {prs_item1.push(inf.address_city.trim());}
                    if (inf.address) {prs_item1.push(inf.address.trim());}
                    if (inf.address_rmk) {prs_item1.push(inf.address_rmk.trim());}
                    
                    fio = prs_item1.join(' / ');
                    if ((inf.address_city) && (fio)) {
                        fio = inf.address_city +', '+ fio;
                    }
                    else if (inf.address_city) {
                        fio = inf.address_city;
                    }
                    if (fio !== '') fio = fio + ';';
                    ws1.row(i).setHeight(14.31); //17
                    ws1.cell(i, 2, i, 6, true).string(fio.toUpperCase()).style(style);
                    i++;
                }
            }    
        }
    }
    //console.log('13');
    let ws_len = ws_arr.length;
    if (ws_len > 2) {
        for (let j=1; j<=ws_len; j+=2) {
            if (ws_arr[j-1].cell(3,2).values!==[]){
                ws_arr[j-1].cell(1,6).string(Math.ceil(j/2)+'/'+Math.ceil(ws_len/2)).style(style);
            }
        }
    }
    //console.log('14');
    return kol_sour;

}

var setWidth = function (ws) {
    ws.column(1).setWidth(6.0);
    ws.column(2).setWidth(18.92);
    ws.column(3).setWidth(5.25);
    ws.column(4).setWidth(3.75);
    ws.column(5).setWidth(11.92);
    ws.column(6).setWidth(11.80);
};
var setWidth1 = function (ws) {
    ws.column(1).setWidth(9.5);
    ws.column(2).setWidth(9.5);
    ws.column(3).setWidth(9.5);
    ws.column(4).setWidth(9.5);
    ws.column(5).setWidth(9.5);
    ws.column(6).setWidth(9.5);
};

module.exports = excel_formation;