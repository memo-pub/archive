var express = require('express');
var router = express.Router();

var cntc = require('../db').connection;
var xl = require('excel4node');



/* GET login. */
router.get('/', async function(req, res) {
    
    if (req.query.selection_id === undefined) {
        res.status(409).json({message:'Не задан ID выборки.'});
        return;
    }
    try {
        let data = await selectAllJSON(req.query.selection_id);
        if (data==null) {
            res.status(409).json({message:'Не задан записей.'});
            return;
        }   
        let wb = new xl.Workbook({
            defaultFont: {
                size: 10,
                name: 'Courier New', 
            }});
        let ws = wb.addWorksheet('Лист 1');

        let style = wb.createStyle({
            font: {
                color: '#000000',
                bold: true,
            }
        });
        var myStyle = wb.createStyle({
            font: {
                bold: false,
                underline: false,
            },
            alignment: {
                wrapText: true,
                horizontal: 'left',
                vertical: 'top',
            },
        });
          
        ws.cell(1, 1).string('Номер по БД').style(style);
        ws.cell(1, 2).string('Фонд-опись-дело').style(style);  
        ws.cell(1, 3).string('Фамилия Имя Отчество').style(style);
        ws.cell(1, 4).string('Год рождения').style(style);
        ws.cell(1, 5).string('Место рождения').style(style);
        ws.cell(1, 6).string('Национальность').style(style);
        ws.cell(1, 7).string('Образование').style(style);
        ws.cell(1, 8).string('Дата смерти').style(style);
       
        ws.column(1).setWidth(12);
        ws.column(2).setWidth(16);
        ws.column(3).setWidth(40);
        ws.column(4).setWidth(15);
        ws.column(5).setWidth(25);
        ws.column(6).setWidth(15);
        ws.column(7).setWidth(20);
        ws.column(8).setWidth(15);
       
        let i = 2;
        let fio='';
        for (let d of data) {
            fio='';
            if ((d.code!==null) && (d.code!==undefined)) {fio=d.code+' ';}
            ws.cell(i, 1).string(fio).style(myStyle);
            fio='';
            if ((d.fund!==null) && (d.fund!==undefined)) {fio=d.fund+'-';}
            if ((d.list_no!==null) && (d.list_no!==undefined)) {fio=fio+d.list_no+'-';}
            if ((d.file_no!==null) && (d.file_no!==undefined)) {fio=fio+d.file_no+'-';}
            if ((d.file_no_ext!==null) && (d.file_no_ext!==undefined)) {fio=fio+d.file_no_ext;}
            ws.cell(i, 2).string(fio.substring(0,(fio.length-1))).style(myStyle);
            fio='';
            if (d.surname!==null) {fio=d.surname+' ';}
            if (d.fname!==null) {fio=fio+d.fname+' ';}
            if (d.lname!==null) {fio=fio+d.lname;}
            ws.cell(i, 3).string(fio).style(myStyle);
            if (d.birth!==null) {
                ws.cell(i, 4).string(d.birth).style(myStyle);
            }
            else {
                if (d.birth_rmk!==null) { ws.cell(i, 4).string(d.birth_rmk).style(myStyle); }
            }
            if (d.place_name!==null) { ws.cell(i, 5).string(d.place_name).style(myStyle); }
            if (d.nation!==null) { ws.cell(i, 6).string(d.nation).style(myStyle); }
            if (d.educ!==null) { ws.cell(i, 7).string(d.educ).style(myStyle); }
            if (d.death!==null) { ws.cell(i, 8).string(d.death).style(myStyle); }
            if ((d.activity!==null) && (d.activity!==undefined)) {
                let mulcell='';
                let ikol = 9;
                for (let t of d.activity){
                    if ((t!==null) && (t!==undefined) && ((t.ent1_rmk!==null)||(t.place_name!=null)||(t.post!==null)||(t.arrival_dat!==null)||
                    (t.depart_dat!==null))){
                        mulcell='';
                        if ((t.ent1_rmk!==null) && (t.ent1_rmk!==undefined))                   {mulcell=mulcell+'Организация                    : '+t.ent1_rmk+'\n';} else 
                        {mulcell=mulcell+'Организация                    : '+'\n';} 
                        if ((t.place_name!=null) && (t.place_name!==undefined))                {mulcell=mulcell+'Место                          : '+t.place_name+'\n';} else 
                        {mulcell=mulcell+'Место                          : '+'\n';}               
                        if ((t.post!==null)  && (t.post!==undefined))                          {mulcell=mulcell+'Должность                      : '+t.post+'\n';} else 
                        {mulcell=mulcell+'Должность                      : '+'\n';}
                        if ((t.arrival_dat!==null) && (t.arrival_dat!==undefined))             {mulcell=mulcell+'Дата поступления               : '+t.arrival_dat+'\n';} else 
                        { 
                            if ((t.arrival_dat_rmk!==null) && (t.arrival_dat_rmk!==undefined)) {mulcell=mulcell+'Комментарий к дате поступления : '+t.arrival_dat_rmk+'\n';} else
                            {mulcell=mulcell+'Комментарий к дате поступления : '+'\n';}
                        }
                        if ((t.depart_dat!==null)  && (t.depart_dat!==undefined))              {mulcell=mulcell+'Дата увольнения                : '+t.depart_dat;} else 
                        { 
                            if ((t.depart_dat_rmk!==null) && (t.depart_dat_rmk!==undefined))   {mulcell=mulcell+'Комментарий к дате увольнения  : '+t.depart_dat_rmk+'\n';} else
                            {mulcell=mulcell+'Комментарий к дате увольнения  : '+'\n';}
                        }                
                    }
                    ws.cell(1, ikol).string('Профессиональная деятельность '+(ikol-8)).style(style);
                    ws.column(ikol).setWidth(90);
                    {ws.cell(i, ikol).string(mulcell).style(myStyle);} 
                    ikol++; 
                }
                i++;
            }
               
        }
        wb.write('ExcelFile.xlsx', res);
    }
    
    catch (err) {
        res.status(500).json({message:'Ошибка при генерации Excel.', err: err});
    }
});
// 
//,t2.birth_place,t2.nation,t2.educ,t2.death,
var getSelectScript = function (id) {
    let result = `WITH stms as (SELECT personal_code FROM selectionitem WHERE selection_id=$1)
    SELECT t2.code, t2.fname, t2.lname, t2.surname,t2.birth,t2.birth_rmk,
    t9.name_ent as educ,t2.death,t2.fund,t2.list_no,t2.file_no,t2.file_no_ext,t4.place_name,t2.birth_place_num,t5.nation,
    json_agg(json_build_object('act_no',t3.act_no,'ent1_rmk', t3.ent1_rmk, 'post' , t3.post , 'arrival_dat', t3.arrival_dat,
    'depart_dat', t3.depart_dat ,'arrival_dat_rmk', t3.arrival_dat_rmk,
    'depart_dat_rmk', t3.depart_dat_rmk ,'place_name',t6.place_name) ORDER BY t3.act_no) as activity FROM stms t1
    INNER JOIN personal_t t2 ON t1.personal_code=t2.code
    LEFT JOIN place_t t4 ON ((t4.place_num=t2.birth_place_num) AND (t4.cur_mark='*'))
    LEFT JOIN nat_t t5 ON t2.nation_id=t5.id
    LEFT JOIN activity_t t3 ON t1.personal_code=t3.personal_code
    LEFT JOIN place_t t6 ON ((t6.place_num=t3.place_num) AND (t6.cur_mark='*'))
    LEFT OUTER JOIN list_of_t t9 ON (t2.educ_id=t9.id)
    GROUP BY t2.code, t2.fname, t2.lname, t2.surname,t2.birth,t2.birth_rmk,
    t9.name_ent,t2.death,t2.fund,t2.list_no,t2.file_no,t2.file_no_ext,t4.place_name,t2.birth_place_num,t5.nation
    ORDER BY t2.code`;
    return {script: result, values: id};
};

var selectAllJSON = async function(id) {                   
    let selscrpt =await getSelectScript(id); 
    let script = 'select json_agg(winquery) as data from ('+selscrpt.script+') winquery;';                    
    try {
        let data = await cntc.tx( async t => {
            return await t.oneOrNone(script,selscrpt.values,val=>val.data);                
        });
        return data;
    }    
    catch(err) {                   
        throw {code: err.code, message: err.message};         
    }               
};


module.exports = router;