const path = require('node:path');
const fs = require('node:fs');
const express = require('express');
const sharp = require('sharp');
const archiver = require('archiver');
const { getFileFolderPath } = require('../helpers/files');
const ArchiveFolder = require('../models/archivefolder');
const ArchiveFolderFile = require('../models/archivefolderfile');

const router = express.Router();

const processGetRequest = async (req, res) => {
    if (!req.query.archivefolder_id) {
        res.status(409).json({ message: 'Не задан ID дела.' });
        return;
    }

    const archiveFolder = new ArchiveFolder();
    archiveFolder.id = req.query.archivefolder_id;
    const archiveNumber = req.query.archiveNumber;

    const archiveFolderData = await archiveFolder.selectOneByIdJSON();

    if (!archiveFolderData) {
        res.status(409).json({ message: 'Дело не найдено.' });
        return;
    }

    const archiveFolderFile = new ArchiveFolderFile();
    archiveFolderFile.archivefolder_id = archiveFolder.id;
    const archiveFolderFileData = await archiveFolderFile.selectAllJSON();

    if (!archiveFolderFileData) {
        res.status(409).json({ message: 'Файлы не найдены.' });
        return;
    }

    res.writeHead(200, {
        'Content-Type': 'application/zip',
        'Content-disposition': 'attachment; filename=archive_files.zip',
    });

    const zip = archiver('zip');

    zip.on('error', function (err) {
        console.dir(err);
        res.status(500).json({
            message: 'Ошибка при создании архива.',
            err,
        });
    });

    const filesToZip = archiveFolderFileData?.sort((a, b) => a.id - b.id).slice(
        archiveNumber === 1 ? 0 : (archiveNumber - 1) * 100 ,
        archiveNumber === 1  ? 100 : archiveNumber * 100
    );

    zip.pipe(res);

    for (let aff of filesToZip) {
        const { id, creation_datetime: date, archivefile } = aff;
        const { file: fileName } = archivefile;
        let { ext: originalFileExtension } = path.parse(fileName);

        const fileFolderPath = getFileFolderPath({ id, date });
        const filePath = path.join(fileFolderPath, fileName);

        const fileExists = fs.existsSync(filePath);
        if (fileExists) {
            if (originalFileExtension === '.tif') {
                const compressedFile = await sharp(filePath)
                    .resize(900)
                    .tiff({ quality: 40 });
                zip.append(compressedFile, { name: aff.archivefile.filename });
            } else if (originalFileExtension === '.jpg') {
                const compressedFile = await sharp(filePath)
                    .resize(900)
                    .jpeg({ quality: 70 });
                zip.append(compressedFile, { name: aff.archivefile.filename });
            } else if (originalFileExtension === '.pdf') {
                zip.append(filePath, { name: aff.archivefile.filename });
            }
        } else {
            zip.append(`Файл ${aff.archivefile.filename} не найден!`, {
                name: `${aff.archivefile.filename}.txt`,
            });
        }
    }

    zip.finalize();
    /// res.status(200).json(aff_data);
};

router.get('/', processGetRequest);

module.exports = router;
