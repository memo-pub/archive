var express = require('express');
var router = express.Router();

var cntc = require('../db').connection;
var xl = require('excel4node');



/* GET login. */
router.get('/', async function(req, res) {
    
    if (req.query.selection_id === undefined) {
        res.status(409).json({message:'Не задан ID выборки.'});
        return;
    }
    try {
        let data = await selectAllJSON(req.query.selection_id);
        if (data==null) {
            res.status(409).json({message:'Не задан записей.'});
            return;
        }   
        let wb = new xl.Workbook({
            defaultFont: {
                size: 10,
                name: 'Courier New', 
            }});
        let ws = wb.addWorksheet('Лист 1');

        let style = wb.createStyle({
            font: {
                color: '#000000',
                bold: true,
            }
        });
        var myStyle = wb.createStyle({
            font: {
                bold: false,
                underline: false,
            },
            alignment: {
                wrapText: true,
                horizontal: 'left',
                vertical: 'top',
            },
        });
          
        ws.cell(1, 1).string('Код').style(style);
        ws.cell(1, 2).string('Фамилия').style(style);
        ws.cell(1, 3).string('Имя').style(style);
        ws.cell(1, 4).string('Отчество').style(style);
        ws.cell(1, 5).string('Шифр').style(style);
        ws.cell(1, 6).string('Год рождения').style(style);
        ws.cell(1, 7).string('Место рождения').style(style);
        ws.cell(1, 8).string('Национальность').style(style);
        ws.cell(1, 9).string('Образование').style(style);
        ws.cell(1, 10).string('Место проживания').style(style);
        ws.cell(1, 11).string('Дата ареста').style(style);
        ws.cell(1, 12).string('Осудивший орган').style(style);
        ws.cell(1, 13).string('Дата осуждения').style(style);
        ws.cell(1, 14).string('Статья').style(style);
        ws.cell(1, 15).string('Приговор').style(style);
        ws.cell(1, 16).string('Дата смерти').style(style);
        ws.cell(1, 17).string('Место расстрела').style(style);
        ws.cell(1, 18).string('Реабилитирующий орган').style(style);
        ws.cell(1, 19).string('Основание для реабилитации').style(style);
        ws.cell(1, 20).string('Дата реабилитации').style(style);
        ws.cell(1, 21).string('Примечания для Базы').style(style);
        ws.cell(1, 22).string('Источник').style(style);

        ws.column(1).setWidth(10);
        ws.column(2).setWidth(25);
        ws.column(3).setWidth(25);
        ws.column(4).setWidth(30);
        ws.column(5).setWidth(16);
        ws.column(6).setWidth(15);
        ws.column(7).setWidth(25);
        ws.column(8).setWidth(15);
        ws.column(9).setWidth(50);
        ws.column(10).setWidth(40);
        ws.column(11).setWidth(50);
        ws.column(12).setWidth(30);
        ws.column(13).setWidth(50);

        ws.column(14).setWidth(50);
        ws.column(15).setWidth(50);
        ws.column(16).setWidth(35);
        ws.column(17).setWidth(40);
        ws.column(18).setWidth(40);
        ws.column(19).setWidth(25);
        ws.column(20).setWidth(15);
        ws.column(21).setWidth(30);
        ws.column(22).setWidth(30);
        
        let i = 2;
        let fio='';
        for (let d of data) {
            ws.cell(i, 1).number(d.code).style(myStyle);
            fio='';
            if (d.surname!==null) {fio=d.surname;}
            if (d.real_surname_rmk!==null){fio=fio+' ('+d.real_surname_rmk+')';}
            ws.cell(i, 2).string(fio).style(myStyle);
            fio='';
            if (d.fname!==null) {fio=d.fname;}
            if (d.fname_rmk!==null){fio=fio+' ('+d.fname_rmk+')';}
            ws.cell(i, 3).string(fio).style(myStyle);
            fio='';
            if (d.lname!==null) {fio=d.lname;}
            if (d.lname_rmk!==null){fio=fio+' ('+d.lname_rmk+')';}
            ws.cell(i, 4).string(fio).style(myStyle);
            let shift = [];
            if (d.fund!==null) {shift.push(d.fund);} else {shift.push('-');}
            if (d.list_no!==null) {shift.push(d.list_no);} else {shift.push('-');}
            if (d.file_no!==null) {shift.push(d.file_no);} else {shift.push('-');}
            if (d.file_no_ext!==null) {shift.push(d.file_no_ext);}
            fio = shift.join(':');
            ws.cell(i, 5).string(fio).style(myStyle);
            fio='';
            if (d.birth!==null) {fio=d.birth;}
            if (d.birth_rmk!==null){fio=fio+' '+d.birth_rmk;}
            ws.cell(i, 6).string(fio).style(myStyle);
            fio=' ';
            if (d.birth_place_name!==null) {fio=d.birth_place_name;}
            if (d.birth_place_rmk!==null) {fio=fio+' '+d.birth_place_rmk;}
            ws.cell(i, 7).string(fio).style(myStyle); 
            if (d.nation!==null) { ws.cell(i, 8).string(d.nation).style(myStyle); }
            fio=''; 
            if (d.educ!==null) {fio=d.educ;}
            if (d.educ_name_rmk!==null) {fio=fio+' '+d.educ_name_rmk;}
            if (d.educ_rmk!==null) {fio=fio+' '+d.educ_rmk;}
            ws.cell(i, 9).string(fio).style(myStyle);
            fio='';
            if (d.geoplace_code_rmk!==null) {fio=d.geoplace_code_rmk;}
            if (d.place_code_on_repress!==null) {fio=fio+' '+d.place_code_on_repress;}
            if (d.place_name_on_repress!==null) {fio=fio+' '+d.place_name_on_repress;}
            ws.cell(i,10).string(fio).style(myStyle);
            let mulcell='';
            if (d.repress_dat!==null)       {mulcell=mulcell+'Дата ареста    : '+d.repress_dat+'\n';}
            if (d.repress_dat_rmk)   {mulcell=mulcell+'Коммент к дате : '+d.repress_dat_rmk;}
            ws.cell(i, 11).string(mulcell).style(myStyle);
        
            if (d.courts!=null) {
                let mulcell1='';
                let mulcell2='';
                let mulcell3='';
                let mulcell4='';
                for (let ic=0; ic< d.courts.length;ic++ ){
                    if (d.courts[ic]!=null){
                        
                        /*      if ((d.courts[ic].trial_no==null) || (d.courts[ic].trial_no==undefined)) 
                            mulcell1=mulcell1+'Судебный процесс:'+'\n';
                        else
                            mulcell1=mulcell1+'Судебный процесс:'+d.courts[ic].trial_no+'\n';*/
                        if (ic==0) {

                            if ((d.courts[ic].court_name==null) || (d.courts[ic].court_name==undefined)) 
                                mulcell1=mulcell1+'Судебный орган:'+'\n';
                            else
                                mulcell1=mulcell1+'Судебный орган:'+d.courts[ic].court_name+'\n';

                            if (d.courts[ic].sentense_date) 
                                mulcell2=mulcell2+'Дата приговора:'+d.courts[ic].sentense_date+'\n';
                        
                            if (d.courts[ic].sentense_date_rmk) 
                                mulcell2=mulcell2+'Коммент к дате:'+d.courts[ic].sentense_date_rmk+'\n';

                            if ((d.courts[ic].sentense==null) || (d.courts[ic].sentense==undefined)) 
                                mulcell4=mulcell4+'Приговор:'+'\n';
                            else
                                mulcell4=mulcell4+'Приговор:'+d.courts[ic].sentense+'\n';

                            if (d.courts[ic].sentense_rmk) 
                                mulcell4=mulcell4+'Коммент. к приговору:'+d.courts[ic].sentense_rmk+'\n';

                        }

                        if (ic>0){
                            if ((d.courts[ic].court_name==null) || (d.courts[ic].court_name==undefined)) 
                                mulcell4=mulcell4+'\nИзменение: Судебный орган:'+'\n';
                            else
                                mulcell4=mulcell4+'\nИзменение: Судебный орган:'+d.courts[ic].court_name+'\n';
    
                            if ((d.courts[ic].sentense_date==null) || (d.courts[ic].sentense_date==undefined)) 
                                mulcell4=mulcell4+'Дата приговора:'+'\n';
                            else
                                mulcell4=mulcell4+'Дата приговора:'+d.courts[ic].sentense_date+'\n';

                            if ((d.courts[ic].sentense==null) || (d.courts[ic].sentense==undefined)) 
                                mulcell4=mulcell4+'Приговор:'+'\n';
                            else
                                mulcell4=mulcell4+'Приговор:'+d.courts[ic].sentense+'\n';
                            mulcell3=mulcell3+'\n';                       
                        }
                       
                        if ((d.courts[ic].article==null) || (d.courts[ic].article==undefined)) 
                            mulcell3=mulcell3+'Статья:'+'\n';
                        else
                            mulcell3=mulcell3+'Статья:'+d.courts[ic].article+'\n';
                            
                        if (d.courts[ic].article_rmk) 
                            mulcell3=mulcell3+'Коммент. к статье:'+d.courts[ic].article_rmk+'\n';

                        if (d.courts[ic].impris!=null) {     
                            for (let ic1=0; ic1< d.courts[ic].impris.length;ic1++ ){
                                if ((d.courts[ic].impris[ic1].prison_name==null) || (d.courts[ic].impris[ic1].prison_name==undefined)) 
                                    mulcell4=mulcell4+'Место заключения:'+'\n';
                                else
                                    mulcell4=mulcell4+'Место заключения:'+d.courts[ic].impris[ic1].prison_name+'\n';
                                mulcell3=mulcell3+'\n';
                                if (d.courts[ic].impris[ic1].prison_name_rmk)
                                    mulcell4=mulcell4+'Коммент. к названию:'+d.courts[ic].impris[ic1].prison_name_rmk+'\n';
                                mulcell3=mulcell3+'\n';
                            }
                        }
                    } //IF

                } //for
                {ws.cell(i, 12).string(mulcell1).style(myStyle);}
                {ws.cell(i, 13).string(mulcell2).style(myStyle);}
                {ws.cell(i, 14).string(mulcell3).style(myStyle);}   
                {ws.cell(i, 15).string(mulcell4).style(myStyle);}
            }
            //
            mulcell='';
            if (d.death!==null)       {mulcell=mulcell+'Дата смерти    : '+d.death+'\n';}
            if (d.death_date_rmk)   {mulcell=mulcell+'Коммент к дате : '+d.death_date_rmk;}
            ws.cell(i, 16).string(mulcell).style(myStyle);
            if (d.reabil_mark=='*'){
                if (d.rehabil_org!==null) {
                    mulcell=d.rehabil_org;
                    ws.cell(i, 18).string(mulcell).style(myStyle);
                }
                if (d.reabil_reas!==null) {
                    mulcell=d.reabil_reas;
                    ws.cell(i, 19).string(mulcell).style(myStyle);
                }

                if (d.reabil_dat!==null)   {
                    mulcell=d.reabil_dat;
                    ws.cell(i, 20).string(mulcell).style(myStyle);
                }   
            }
            i++;      
        }
        wb.write('ExcelFile.xlsx', res);
    }
    
    catch (err) {
        res.status(500).json({message:'Ошибка при генерации Excel.', err: err});
    }
});

var getSelectScript = function (id) {
    let result = `WITH stms as (SELECT personal_code FROM selectionitem WHERE selection_id=$1),
    impris as (
        select crt_id, json_agg(json_build_object('prison_name', prison_name, 'prison_name_rmk', prison_name_rmk)
                        ORDER BY impris_no) as impris 
            from impris_t 
        group by crt_id
    ),
    court as (
        select t7.rep_id, json_agg (
            json_build_object(
                'court_name',t7.court_name,
                'trial_no',t7.trial_no,
                'sentense_date',t7.sentense_date,
                'article',t7.article,
                'sentense',t7.sentense,  
                'impris',t1.impris)
                ORDER BY trial_no
            ) as courts
            from court_t t7
        left outer join impris t1
            on t7.id=t1.crt_id
        group by t7.rep_id
    )
        SELECT t2.code, t2.fund, t2.list_no, t2.file_no, t2.file_no_ext, t2.fname,t2.fname_rmk, t2.lname,t2.lname_rmk, t2.surname,t2.real_surname_rmk,t2.birth,t2.birth_rmk,t2.birth_place_rmk,
        t9.name_ent as educ,t9.name_rmk as educ_name_rmk,t2.educ_rmk,t2.death,t2.death_date_rmk,t3.place_name as birth_place_name,t2.birth_place_num,t4.nation,
        t5.repress_no,t5.reabil_dat,t5.geoplace_code_rmk,t5.repress_dat,t5.repress_dat_rmk,t5.reabil_mark,t5.rehabil_org,
        t6.place_code as place_code_on_repress,t6.place_name as place_name_on_repress,
            t11.name_ent as reabil_reas,t12.courts
        FROM stms t1
        LEFT OUTER JOIN personal_t t2 ON t1.personal_code=t2.code
        LEFT OUTER JOIN place_t t3 ON ((t3.place_num=t2.birth_place_num) AND (t3.cur_mark='*'))
        LEFT OUTER JOIN nat_t t4 ON t2.nation_id=t4.id
        JOIN repress_t t5 ON t1.personal_code=t5.personal_code
        LEFT OUTER JOIN place_t t6 ON ((t6.place_num=t5.place_num) AND (t6.cur_mark='*'))
        LEFT OUTER JOIN list_of_t t9 ON (t2.educ_id=t9.id)
        LEFT OUTER JOIN rehabil_reason_t t10 ON (t5.id=t10.rep_id)
        LEFT OUTER JOIN list_of_t t11 ON (t11.id=t10.reason_id)
        left OUTER JOIN court t12 on t5.id=t12.rep_id
                                                               
        ORDER BY t2.code,t5.repress_no`;
    return {script: result, values: id};
};

var selectAllJSON = async function(id) {                   
    let selscrpt =await getSelectScript(id); 
    let script = 'select json_agg(winquery) as data from ('+selscrpt.script+') winquery;';                    
    try {
        let data = await cntc.tx( async t => {
            return await t.oneOrNone(script,selscrpt.values,val=>val.data);                
        });
        return data;
    }    
    catch(err) {                   
        throw {code: err.code, message: err.message};         
    }               
};


module.exports = router;