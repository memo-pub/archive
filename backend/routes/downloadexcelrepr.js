var express = require('express');
var router = express.Router();

var cntc = require('../db').connection;
var xl = require('excel4node');



/* GET login. */
router.get('/', async function(req, res) {
    
    if (req.query.selection_id === undefined) {
        res.status(409).json({message:'Не задан ID выборки.'});
        return;
    }
    try {
        let data = await selectAllJSON(req.query.selection_id);
        if (data==null) {
            res.status(409).json({message:'Не задан записей.'});
            return;
        }   
        let wb = new xl.Workbook({
            defaultFont: {
                size: 10,
                name: 'Courier New', 
            }});
        let ws = wb.addWorksheet('Лист 1');

        let style = wb.createStyle({
            font: {
                color: '#000000',
                bold: true,
            }
        });
        var myStyle = wb.createStyle({
            font: {
                bold: false,
                underline: false,
            },
            alignment: {
                wrapText: true,
                horizontal: 'left',
                vertical: 'top',
            },
        });
          
        ws.cell(1, 1).string('Номер по БД').style(style);
        ws.cell(1, 2).string('Фонд-опись-дело').style(style);  
        ws.cell(1, 3).string('Фамилия Имя Отчество').style(style);
        ws.cell(1, 4).string('Год рождения').style(style);
        ws.cell(1, 5).string('Место рождения').style(style);
        ws.cell(1, 6).string('Национальность').style(style);
        ws.cell(1, 7).string('Образование').style(style);
        ws.cell(1, 8).string('Дата смерти').style(style);
        ws.cell(1, 9).string('Репрессии').style(style);
        ws.cell(1, 10).string('Следствия и суды').style(style);
        ws.cell(1, 11).string('Заключения').style(style);

        ws.column(1).setWidth(12);
        ws.column(2).setWidth(16);
        ws.column(3).setWidth(40);
        ws.column(4).setWidth(15);
        ws.column(5).setWidth(25);
        ws.column(6).setWidth(15);
        ws.column(7).setWidth(20);
        ws.column(8).setWidth(15);
        ws.column(9).setWidth(40);
        ws.column(10).setWidth(40);
        ws.column(11).setWidth(90);
        let i = 2;
        let fio='';
        for (let d of data) {
            fio='';
            if ((d.code!==null) && (d.code!==undefined)) {fio=d.code+' ';}
            ws.cell(i, 1).string(fio).style(myStyle);
            fio='';
            if ((d.fund!==null) && (d.fund!==undefined)) {fio=d.fund+'-';}
            if ((d.list_no!==null) && (d.list_no!==undefined)) {fio=fio+d.list_no+'-';}
            if ((d.file_no!==null) && (d.file_no!==undefined)) {
                fio=fio+d.file_no+'-';
                if (d.file_no_ext) fio=fio+d.file_no_ext;
            }
            ws.cell(i, 2).string(fio.substring(0,(fio.length-1))).style(myStyle);
            fio='';
            if (d.surname!==null) {fio=d.surname+' ';}
            if (d.fname!==null) {fio=fio+d.fname+' ';}
            if (d.lname!==null) {fio=fio+d.lname;}
            ws.cell(i, 3).string(fio).style(myStyle);
            if (d.birth!==null) {
                ws.cell(i, 4).string(d.birth).style(myStyle);
            }
            else {
                if (d.birth_rmk!==null) { ws.cell(i, 4).string(d.birth_rmk).style(myStyle); }
            }
            if (d.place_name!==null) { ws.cell(i, 5).string(d.place_name).style(myStyle); }
            if (d.nation!==null) { ws.cell(i, 6).string(d.nation).style(myStyle); }
            if (d.educ!==null) { ws.cell(i, 7).string(d.educ).style(myStyle); }
            if (d.death!==null) { ws.cell(i, 8).string(d.death).style(myStyle); }

            let mulcell='';
            if ((d.repress_no!==null) &&  (d.repress_no!==undefined))   {mulcell=mulcell+'Номер репрессии   : '+d.repress_no+'\n';}  else
            {mulcell=mulcell+'Номер репрессии   : '+'\n';}
            if ((d.place_name!=null) &&  (d.place_name!==undefined))     {mulcell=mulcell+'Место жительства  : '+d.place_name+'\n';} else
            {mulcell=mulcell+'Место жительства  : '+'\n';}
            if ((d.reabil_dat!==null) &&  (d.reabil_dat!==undefined))    {mulcell=mulcell+'Дата реабилитации : '+d.reabil_dat;} else
            {mulcell=mulcell+'Дата реабилитации : ';} 
            {ws.cell(i,9).string(mulcell).style(myStyle);}

            mulcell='';              
            if ((d.court_name!==null) &&  (d.court_name!==undefined))    {mulcell=mulcell+'Судебный орган       : '+d.court_name+'\n';} else
            {mulcell=mulcell+'Судебный орган       : '+'\n';}
            if ((d.trial_no!==null) &&  (d.trial_no!==undefined))      {mulcell=mulcell+'№ Судебного процесса : '+d.trial_no+'\n';} else
            {mulcell=mulcell+'№ Судебного процесса : '+'\n';}
            if ((d.sentense_date!==null) &&  (d.sentense_date!==undefined)) {mulcell=mulcell+'Дата приговора       : '+d.sentense_date+'\n';} else
            {mulcell=mulcell+'Дата приговора       : '+'\n';}
            if ((d.article!==null) &&  (d.article!==undefined))       {mulcell=mulcell+'Статья               : '+d.article+'\n';} else 
            {mulcell=mulcell+'Статья               : '+'\n';}
            if ((d.sentense!==null) &&  (d.sentense!==undefined))      {mulcell=mulcell+'Приговор             : '+d.sentense;} else
            {mulcell=mulcell+'Приговор             : ';}
            {ws.cell(i,10).string(mulcell).style(myStyle);}
            mulcell='';              
            if ((d.impris_no!==null) &&  (d.impris_no!==undefined))                  {mulcell=mulcell+'№ заключения                : '+d.impris_no+'\n';} else
            {mulcell=mulcell+'№ заключения                : '+'\n';}
            if ((d.impris_type!==null) &&  (d.impris_type!==undefined))              {mulcell=mulcell+'Тип заключения              : '+d.impris_type+'\n';} else 
            {mulcell=mulcell+'Тип заключения              : '+'\n';}
            if ((d.arrival_dat!==null) &&  (d.arrival_dat!==undefined))              {mulcell=mulcell+'Дата прибытия               : '+d.arrival_dat+'\n';} else 
            { 
                if ((d.arrival_dat_rmk!==null) && (d.arrival_dat_rmk!==undefined))   {mulcell=mulcell+'Комментарий к дате прибытия : '+d.arrival_dat_rmk+'\n';} else
                {mulcell=mulcell+'Комментарий к дате прибытия : '+'\n';}
            }

            if ((d.leave_dat!==null) &&  (d.leave_dat!==undefined))                  {mulcell=mulcell+'Дата убытия                 : '+d.leave_dat+'\n';} else  
            { 
                if ((d.leave_dat_rmk!==null) && (d.leave_dat_rmk!==undefined))       {mulcell=mulcell+'Комментарий к дате убытия   : '+d.leave_dat_rmk+'\n';} else
                {mulcell=mulcell+'Комментарий к дате убытия   : '+'\n';}
            }
            if ((d.prison_name!==null) &&  (d.prison_name!==undefined))              {mulcell=mulcell+'Название места заключения   : '+d.prison_name+'\n';} else 
            {mulcell=mulcell+'Название места заключения   : '+'\n';}
            if ((d.reason!==null) &&  (d.reason!==undefined))                        {mulcell=mulcell+'Причина убытия              : '+d.reason;} else 
            {mulcell=mulcell+'Причина убытия              : ';}
            {ws.cell(i,11).string(mulcell).style(myStyle);}
            i++;      
        }
        wb.write('ExcelFile.xlsx', res);
    }
    
    catch (err) {
        res.status(500).json({message:'Ошибка при генерации Excel.', err: err});
    }
});

var getSelectScript = function (id) {
    let result = `WITH stms as (SELECT personal_code FROM selectionitem WHERE selection_id=$1)
    SELECT t2.code, t2.fname, t2.lname, t2.surname,t2.birth,t2.birth_rmk,
    t9.name_ent as educ,t2.death,t3.place_name,t2.birth_place_num,t2.fund,t2.list_no,t2.file_no,t2.file_no_ext,t4.nation,
    t5.repress_no,t5.reabil_dat,t6.place_name,t7.court_name,t7.trial_no,t7.sentense_date,t7.article,t7.sentense,t8.prison_name,t8.impris_no,
    t8_1.name_ent as impris_type,t8.arrival_dat,t8.leave_dat,t8.arrival_dat_rmk,t8.leave_dat_rmk,t8_2.name_ent as reason
    FROM stms t1
    LEFT OUTER JOIN personal_t t2 ON t1.personal_code=t2.code
    LEFT OUTER JOIN place_t t3 ON ((t3.place_num=t2.birth_place_num) AND (t3.cur_mark='*'))
    LEFT OUTER JOIN nat_t t4 ON t2.nation_id=t4.id
    LEFT OUTER JOIN repress_t t5 ON t1.personal_code=t5.personal_code
    LEFT OUTER JOIN place_t t6 ON ((t6.place_num=t5.place_num) AND (t6.cur_mark='*'))
    LEFT OUTER JOIN court_t t7 ON (t7.personal_code=t1.personal_code) AND (t7.rep_id=t5.id)
    LEFT OUTER JOIN impris_t t8 ON (t8.crt_id=t7.id)
    LEFT OUTER JOIN list_of_t t8_1 ON (t8.impris_type_id=t8_1.id)
    LEFT OUTER JOIN list_of_t t8_2 ON (t8.reason_id=t8_2.id)
    LEFT OUTER JOIN list_of_t t9 ON (t2.educ_id=t9.id)  
    GROUP BY t2.code, t2.fname, t2.lname, t2.surname,t2.birth,t2.birth_rmk,
    t9.name_ent,t2.death,t3.place_name,t2.birth_place_num,t2.fund,t2.list_no,t2.file_no,t2.file_no_ext,t4.nation, t5.repress_no,t5.reabil_dat,t6.place_name,
    t7.court_name,t7.trial_no,t7.sentense_date,t7.article,t7.sentense,t8.prison_name,t8.impris_no,t8_1.name_ent,t8.arrival_dat,t8.leave_dat,
    t8.arrival_dat_rmk,t8.leave_dat_rmk,t8_2.name_ent														   
    ORDER BY t2.code,t5.repress_no,t7.trial_no,t8.impris_no`;

    return {script: result, values: id};
};

var selectAllJSON = async function(id) {                   
    let selscrpt =await getSelectScript(id); 
    let script = 'select json_agg(winquery) as data from ('+selscrpt.script+') winquery;';                    
    try {
        let data = await cntc.tx( async t => {
            return await t.oneOrNone(script,selscrpt.values,val=>val.data);                
        });
        return data;
    }    
    catch(err) {                   
        throw {code: err.code, message: err.message};         
    }               
};


module.exports = router;