var express = require('express');
var router = express.Router();
var xl = require('excel4node');
var Archiver = require('archiver');
var docx = require('docx');

var excel_formation = require('./excelkart_functions/excel_formation');
var word_formation = require('./excelkart_functions/word_formation');

var selectAllJSON = require('./excelkart_functions/data_select').selectAllJSON;
var selectAllJSONzip = require('./excelkart_functions/data_select').selectAllJSONzip;

const { Packer } = docx;

/* GET login. */
//{{urlm}}downloadexcelkart/zip?personal_code=2 - запрос из POSTMAN
router.get('/zip', async function(req, res) {
    if (req.query.personal_code === undefined) {
        res.status(409).json({message:'Не задан ID выборки.'});
        return;
    }
    let mascode=await selectAllJSONzip(req.query.personal_code);
    if (mascode.length!=0)   {
        var zip = Archiver('zip');
        zip.on('error', function(err) {
            res.status(500).json({message:'Ошибка при создании архива.', err: err});
        });
        zip.pipe(res);
        for (let idcode of mascode){
            try {
                let data = await selectAllJSON(idcode.human_code);   
                let wb = new xl.Workbook({
                    defaultFont: {
                        size:12,
                        name: 'Courier New', 
                    }});
                let dlinsour=excel_formation(data,wb);
                let bufferzip=await wb.writeToBuffer();
                let imo= 'Card'+idcode.human_code+'.xlsx';
                zip.append(bufferzip, { name: imo });
                // если источников больше 20
                if (dlinsour>20){
                    let doc=word_formation(data);
                    imo= 'Card'+idcode.human_code+'.docx';
                    const b64string = await Packer.toBase64String(doc);
                    zip.append(Buffer.from(b64string, 'base64'), { name: imo });
                }
            }
            catch (err) {
                console.dir(err);
                res.status(500).json({message:'Ошибка при генерации Excel.', err: err});
            }
        }
        zip.finalize();
    }    else {
        res.status(409).json({message:'Код не найден',err:409});
        return;
    }
});

/* GET login. */
router.get('/', async function(req, res) {
    if (req.query.personal_code === undefined) {
        res.status(409).json({message:'Не задан ID выборки.'});
        return;
    }
    try {
        let data = await selectAllJSON(req.query.personal_code);
        if (data==null) {
            res.status(409).json({message:'Не задан записей.'});
            return;
        }
        let wb = new xl.Workbook({
            defaultFont: {
                size:12,
                name: 'Courier New', 
            }});
        let dlinsour=excel_formation(data,wb);
        // если источников больше 20
        if (dlinsour>20){
            let doc=word_formation(data);
            let zip = Archiver('zip');
            zip.on('error', function(err) {
                res.status(500).json({message:'Ошибка при создании архива.', err: err});
            });
            zip.pipe(res);
            let bufferzip=await wb.writeToBuffer();
            let idcode=data.code;
            let imo= 'Card'+idcode+'.xlsx';
            zip.append(bufferzip, { name: imo });
            imo= 'Card'+idcode+'.docx';
            const b64string = await Packer.toBase64String(doc);
            zip.append(Buffer.from(b64string, 'base64'), { name: imo });
            zip.finalize();
        }
        else{
            wb.write('ExcelFile.xlsx', res);
        } 
    }
    
    catch (err) {
        console.dir(err);
        res.status(500).json({message:'Ошибка при генерации Excel.', err: err});
    }
});



module.exports = router;



