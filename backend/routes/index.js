const express = require('express');

const router = express.Router();

/* GET home page. */
router.get('/', ( res, ) => {
    res.send({ title: 'Express' });
});

module.exports = router;
