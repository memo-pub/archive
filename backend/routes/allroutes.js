const express = require('express');

const serveStatic = require('serve-static');
const { passport } = require('../helpers/guards');
const SRouter = require('../slib/srouter');
const { mediaMiddleware } = require('../helpers/media');
const { setMediaHeaders } = require('../helpers/media');

const { getStorageRootPaths } = require('../helpers/files');

const MUser = require('../models/muser');
const FieldSearch = require('../viewmodels/fieldsearch');
const FieldJoin = require('../viewmodels/fieldjoin');
const Diskinfo = require('../viewmodels/diskinfo');
const OpisNumbering = require('../viewmodels/opisnumbering');
const GenerateDescription = require('../viewmodels/generatedescription');
const GenerateDeloRange = require('../viewmodels/generatedelorange');
const ArchiveFolder = require('../models/archivefolder');
const ArchiveFolderFile = require('../models/archivefolderfile');
const OpisFile = require('../models/opisfile');
const FondFile = require('../models/fondfile');
const MConfig = require('../models/mconfig');
const ArchiveFolderLink = require('../models/archivefolderlink');
const Opis = require('../models/opis');
const Fond = require('../models/fond');
const NAT_T = require('../models/nat_t');
const PERSONAL_T = require('../models/personal_t');
const ORG_T = require('../models/org_t');
const COORG_T = require('../models/coorg_t');
const ACTIVITY_T = require('../models/activity_t');
const COACT_T = require('../models/coact_t');
const PLACE_T = require('../models/place_t');
const GEO_PLACE_T = require('../models/geo_place_t');
const LIST_OF_T = require('../models/list_of_t');
const PERSONAL_SHIFR_T = require('../models/personal_shifr_t');
const SOUR_T_LINK = require('../models/sour_t_link');

const REPRESS_T = require('../models/repress_t');
const COREP_T = require('../models/corep_t');
const COURT_T = require('../models/court_t');
const COCRT_T = require('../models/cocrt_t');
const IMPRIS_T = require('../models/impris_t');
const COIMP_T = require('../models/coimp_t');
const INFORM_T = require('../models/inform_t');
const HELP_T = require('../models/help_t');
const FAMILY_T = require('../models/family_t');
const MENTIONED_T = require('../models/mentioned_t');
const SOUR_T = require('../models/sour_t');
const USAGE_T = require('../models/usage_t');

// var TEXT_T = require('../models/text_t');
const DEATH_REASON_T = require('../models/death_reason_t');
const REHABIL_REASON_T = require('../models/rehabil_reason_t');
const ACT_SPHERE_T = require('../models/act_sphere_t');
const Selection = require('../models/selection');
const SelectionItem = require('../models/selectionitem');
const MLog = require('../models/mlog');
const SOUR_FILE_T = require('../models/sour_file_t');
const SOUR_TYPE_T = require('../models/sour_type_t');
const PERSONAL_SOUR_T = require('../models/personal_sour_t');

const ArchStatistic = require('../viewmodels/archstatistic');
const ScanStatistic = require('../viewmodels/scanstatistic');
const AddSelectionItem = require('../viewmodels/addselectionitem');
const MarchFileCopy = require('../viewmodels/marchfilecopy');
const DeleteSelectionItem = require('../viewmodels/deleteselectionitem');
const Mentioned = require('../viewmodels/mentioned');
const MoveOrderNom = require('../viewmodels/moveordernom');
const SeekNameListoft = require('../viewmodels/seeknamelistoft');

const DeletSour_t = require('../viewmodels/deletesour_t');

const PersonalStatistic = require('../viewmodels/personalstatistic');
const UpLoadPlaces_Date = require('../viewmodels/uploadplaces_date');
const downloadarchlog = require('./downloadarchlog');
const downloadexcelkart = require('./downloadexcelkart');
const downloadexcelreprgpr = require('./downloadexcelreprgpr');
const downloadexcelrepr = require('./downloadexcelrepr');
const downloadexcelobd = require('./downloadexcelobd');
const downloadexcelrub = require('./downloadexcelrub');
const downloadexcel = require('./downloadexcel');
const downloadfolderfiles = require('./downloadfolderfiles');
const downloadpreview = require('./downloadpreview');
const login = require('./login');
const index = require('./index');
const GetArchivefolderId = require('../viewmodels/getarchivefolderidfromfile');
const PERSONAL_DB_LINK_T = require('../models/personal_db_link_t');
const PublicSiteActions = require('../viewmodels/publicsiteactions');

const router = express.Router();

router.use(passport.initialize());
router.use('/', index);
router.use('/login', login);

router.use(
    '/downloadfolderfiles',
    passport.authenticate('jwt', { session: false }),
    downloadfolderfiles,
);
router.use(
    '/downloadpreview',
    passport.authenticate('jwt', { session: false }),
    downloadpreview,
);
router.use('/downloadexcel', passport.authenticate('jwt', { session: false }), downloadexcel);
router.use('/downloadexcelobd', passport.authenticate('jwt', { session: false }), downloadexcelobd);
router.use(
    '/downloadexcelrepr',
    passport.authenticate('jwt', { session: false }),
    downloadexcelrepr,
);
router.use(
    '/downloadexcelreprgpr',
    passport.authenticate('jwt', { session: false }),
    downloadexcelreprgpr,
);
router.use(
    '/downloadexcelkart',
    passport.authenticate('jwt', { session: false }),
    downloadexcelkart,
);
router.use('/downloadexcelrub', passport.authenticate('jwt', { session: false }), downloadexcelrub);
router.use('/downloadarchlog', passport.authenticate('jwt', { session: false }), downloadarchlog);

router.use('/nat_t', passport.authenticate('jwt', { session: false }), new SRouter(NAT_T));
router.use(
    '/personal_t',
    passport.authenticate('jwt', { session: false }),
    new SRouter(PERSONAL_T),
);
router.use(
    '/publicsiteactions',
    passport.authenticate('jwt', { session: false }),
    new SRouter(PublicSiteActions),
);
router.use('/org_t', passport.authenticate('jwt', { session: false }), new SRouter(ORG_T));
router.use('/coorg_t', passport.authenticate('jwt', { session: false }), new SRouter(COORG_T));
router.use(
    '/activity_t',
    passport.authenticate('jwt', { session: false }),
    new SRouter(ACTIVITY_T),
);
router.use('/coact_t', passport.authenticate('jwt', { session: false }), new SRouter(COACT_T));
router.use('/place_t', passport.authenticate('jwt', { session: false }), new SRouter(PLACE_T));
router.use(
    '/geo_place_t',
    passport.authenticate('jwt', { session: false }),
    new SRouter(GEO_PLACE_T),
);
router.use('/list_of_t', passport.authenticate('jwt', { session: false }), new SRouter(LIST_OF_T));
router.use(
    '/personal_shifr_t',
    passport.authenticate('jwt', { session: false }),
    new SRouter(PERSONAL_SHIFR_T),
);
router.use(
    '/personal_db_link_t',
    passport.authenticate('jwt', { session: false }),
    new SRouter(PERSONAL_DB_LINK_T),
);
router.use('/repress_t', passport.authenticate('jwt', { session: false }), new SRouter(REPRESS_T));
router.use('/selection', passport.authenticate('jwt', { session: false }), new SRouter(Selection));
router.use(
    '/selectionitem',
    passport.authenticate('jwt', { session: false }),
    new SRouter(SelectionItem),
);
router.use('/corep_t', passport.authenticate('jwt', { session: false }), new SRouter(COREP_T));
router.use('/court_t', passport.authenticate('jwt', { session: false }), new SRouter(COURT_T));
router.use('/cocrt_t', passport.authenticate('jwt', { session: false }), new SRouter(COCRT_T));
router.use('/impris_t', passport.authenticate('jwt', { session: false }), new SRouter(IMPRIS_T));
router.use('/coimp_t', passport.authenticate('jwt', { session: false }), new SRouter(COIMP_T));
router.use('/inform_t', passport.authenticate('jwt', { session: false }), new SRouter(INFORM_T));
router.use('/help_t', passport.authenticate('jwt', { session: false }), new SRouter(HELP_T));
router.use('/family_t', passport.authenticate('jwt', { session: false }), new SRouter(FAMILY_T));
router.use(
    '/mentioned_t',
    passport.authenticate('jwt', { session: false }),
    new SRouter(MENTIONED_T),
);
router.use('/sour_t', passport.authenticate('jwt', { session: false }), new SRouter(SOUR_T));
router.use('/usage_t', passport.authenticate('jwt', { session: false }), new SRouter(USAGE_T));
// router.use('/text_t', passport.authenticate('jwt', { session: false }), new SRouter(TEXT_T));
router.use(
    '/death_reason_t',
    passport.authenticate('jwt', { session: false }),
    new SRouter(DEATH_REASON_T),
);

router.use('/muser', passport.authenticate('jwt', { session: false }), new SRouter(MUser));
router.use(
    '/fieldsearch',
    passport.authenticate('jwt', { session: false }),
    new SRouter(FieldSearch),
);
router.use('/fieldjoin', passport.authenticate('jwt', { session: false }), new SRouter(FieldJoin));
router.use('/diskinfo', passport.authenticate('jwt', { session: false }), new SRouter(Diskinfo));
router.use(
    '/opisnumbering',
    passport.authenticate('jwt', { session: false }),
    new SRouter(OpisNumbering),
);
router.use(
    '/generatedescription',
    passport.authenticate('jwt', { session: false }),
    new SRouter(GenerateDescription),
);
router.use(
    '/getarchivefolderid',
    passport.authenticate('jwt', { session: false }),
    new SRouter(GetArchivefolderId),
);
router.use(
    '/generatedelorange',
    passport.authenticate('jwt', { session: false }),
    new SRouter(GenerateDeloRange),
);
router.use(
    '/archivefolder',
    passport.authenticate('jwt', { session: false }),
    new SRouter(ArchiveFolder),
);
router.use(
    '/rehabil_reason_t',
    passport.authenticate('jwt', { session: false }),
    new SRouter(REHABIL_REASON_T),
);
router.use(
    '/act_sphere_t',
    passport.authenticate('jwt', { session: false }),
    new SRouter(ACT_SPHERE_T),
);
router.use(
    '/moveordernom',
    passport.authenticate('jwt', { session: false }),
    new SRouter(MoveOrderNom),
);

router.use(
    '/archivefolderfile',
    passport.authenticate('jwt', { session: false }),
    new SRouter(ArchiveFolderFile),
);
/// /
// router.use('/archivefolderfile', getOrAuthenticate, new SRouter(ArchiveFolderFile));
// Эту строку я создавал временно, чтобы сделать доступ на Get без токена
/// /
router.use('/opisfile', passport.authenticate('jwt', { session: false }), new SRouter(OpisFile));
router.use('/fondfile', passport.authenticate('jwt', { session: false }), new SRouter(FondFile));
router.use('/mconfig', passport.authenticate('jwt', { session: false }), new SRouter(MConfig));
router.use(
    '/archivefolderlink',
    passport.authenticate('jwt', { session: false }),
    new SRouter(ArchiveFolderLink),
);
router.use(
    '/sour_t_link',
    passport.authenticate('jwt', { session: false }),
    new SRouter(SOUR_T_LINK),
);
router.use('/opis', passport.authenticate('jwt', { session: false }), new SRouter(Opis));
router.use('/fond', passport.authenticate('jwt', { session: false }), new SRouter(Fond));
router.use(
    '/archstatistic',
    passport.authenticate('jwt', { session: false }),
    new SRouter(ArchStatistic),
);
router.use(
    '/scanstatistic',
    passport.authenticate('jwt', { session: false }),
    new SRouter(ScanStatistic),
);

router.use(
    '/addselectionitem',
    passport.authenticate('jwt', { session: false }),
    new SRouter(AddSelectionItem),
);
router.use(
    '/marchfilecopy',
    passport.authenticate('jwt', { session: false }),
    new SRouter(MarchFileCopy),
);

router.use(
    '/deleteselectionitem',
    passport.authenticate('jwt', { session: false }),
    new SRouter(DeleteSelectionItem),
);
router.use('/mentioned', passport.authenticate('jwt', { session: false }), new SRouter(Mentioned));
router.use(
    '/seeknamelistoft',
    passport.authenticate('jwt', { session: false }),
    new SRouter(SeekNameListoft),
);
router.use(
    '/sour_file_t',
    passport.authenticate('jwt', { session: false }),
    new SRouter(SOUR_FILE_T),
);
router.use(
    '/deletesour_t',
    passport.authenticate('jwt', { session: false }),
    new SRouter(DeletSour_t),
);

router.use(
    '/sour_type_t',
    passport.authenticate('jwt', { session: false }),
    new SRouter(SOUR_TYPE_T),
);
router.use(
    '/personal_sour_t',
    passport.authenticate('jwt', { session: false }),
    new SRouter(PERSONAL_SOUR_T),
);
router.use('/mlog', passport.authenticate('jwt', { session: false }), new SRouter(MLog));

router.use(
    '/personalstatistic',
    passport.authenticate('jwt', { session: false }),
    new SRouter(PersonalStatistic),
);
router.use(
    '/uploadplaces_date',
    passport.authenticate('jwt', { session: false }),
    new SRouter(UpLoadPlaces_Date),
);

const storageRootPaths = getStorageRootPaths();

for (let root of storageRootPaths) {
    router.use(
        `/media/:legacyStorageToken/:id`,
        mediaMiddleware,
        serveStatic(root, { setHeaders: setMediaHeaders }),
    );
}

module.exports = router;
