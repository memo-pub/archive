const WebSocket = require('ws');

const clientsList = [];
let clientCount = 0;

const WSSList = [];

class WSServer extends WebSocket.Server {
    constructor(ServerOptions, callback) {
        super(ServerOptions, callback);

        const { authGetPayload } = require('./auth');

        this.on('connection', (ws) => {
            clientCount++;
            const index = clientCount;
            clientsList[index] = { connection: ws, subscribe: [] };
            ws.on('message', async (message) => {
                const messageArray = message.split(' ');

                if (messageArray && messageArray[0] && messageArray[1]) {
                    if (messageArray[0] === 'Token') {
                        try {
                            const u = await authGetPayload(messageArray[1]);
                            clientsList[index].user = u;
                            clearTimeout(timerId);
                            ws.send('Token - Successfully added.');
                        } catch {
                            ws.close(1000, 'invalid token');
                        }
                    } else if (clientsList[index].user) {
                        if (messageArray[0] === 'Add') {
                            clientsList[index].subscribe.push(messageArray[1]);

                            try {
                                ws.send(messageArray[1] + ' - Successfully added.');
                            } catch (error) {
                                console.dir(error);
                            }
                        } else if (messageArray[0] === 'Delete') {
                            const what = messageArray[1];
                            let ax;

                            while ((ax = clientsList[index].subscribe.indexOf(what)) !== -1) {
                                clientsList[index].subscribe.splice(ax, 1);
                            }

                            try {
                                ws.send(messageArray[1] + ' - Successfully deleted.');
                            } catch (error) {
                                console.dir(error);
                            }
                        }
                    } else {
                        try {
                            ws.send('Comand rejected. No valit token recived.');
                        } catch (error) {
                            console.dir(error);
                        }
                    }
                }
            });

            ws.on('close', () => {
                delete clientsList[index];
            });

            try {
                ws.send('Connection is established. I expect token within 20 seconds.');
            } catch (error) {
                console.dir(error);
            }

            const timerId = setTimeout(() => {
                try {
                    ws.send('No valit token recived. Closing');
                } catch (error) {
                    console.dir(error);
                }

                ws.close(1000, 'invalid token');
            }, 20_000);
        });
    }

    /**
     * Позвращает список подключений, у которых есть подписка на ресурс с указанным именем
     * @param {string} name - имя ресурса
     */
    // console.dir(objectMap(clients_list,val=>{return {id:val.id, subscribe: val.subscribe};}));
    getSubscribes(name) {
        return clientsList.filter((value) => value.subscribe.includes(name));
    }

    addToList(wss) {
        WSSList.push(wss);
    }
}

module.exports = {
    WSServer,
    WSSList,
};
