require('dotenv').config();
const { env } = require('node:process');

const GB = 1024 ** 3;

const config = {
    filePrefixMap: { fondfile: 'f', opisfile: 'o', archivefile: 'd' },
    applocation: env.APPLOCATION,
    port: env.PORT,
    staticFolder: env.STATIC_DIR,
    storageRoot: env.STORAGE_ROOT_DIR,
    uploadDir: env.UPLOAD_ROOT_DIR,
    uploadSizeLimit: env.UPLOAD_MAX_SIZE_GB * GB,
    readDir: env.UPLOAD_READ_DIR,
    connectionString: {
        host: env.DB_HOST,
        port: env.DB_PORT,
        database: env.DB_NAME,
        user: env.DB_USER,
        password: env.DB_PASSWORD,
        max: env.DB_MAX_CONN,
    },
    jwt: {
        salt: env.JWT_SALT,
        secretOrKey: env.JWT_SECRET,
    },
    usingSingleStorageRoot: env.SINGLE_STORAGE_ROOT?.toLowerCase() === 'true',
    multiRootVariableNamePrefix: env.MULTIROOT_VARIABLE_NAME_PREFIX || 'STORAGE_ROOT_BEFORE_',
    debugSQL: env.DEBUG_SQL && env.DEBUG_SQL.toLowerCase() === 'true',
};

module.exports = config;
