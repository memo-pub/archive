var cntc = require('../db').connection;
const XLSX = require('xlsx');

async function excel_check (){
    try {
        let nodePath = process.argv[2];            
        let wb=XLSX.readFile(nodePath);
        for (let list_kol=0;list_kol<1 /*wb.SheetNames.length*/;list_kol++){
            let sheet_name = wb.SheetNames[list_kol];
            console.log(sheet_name);
            let worksheet = wb.Sheets[sheet_name];
            let selstr2 = `
                    select count (*) as kol,count(*) FILTER (WHERE COALESCE(dat2_to_text(birth_begin,birth_end),'')=($4::varchar) and ($4::varchar)<>'') AS filterdate 
                    from personal_t where UPPER(TRIM(COALESCE(fname,'')))=UPPER(TRIM($1)) and 
                    UPPER(TRIM(COALESCE(lname,'')))=UPPER(TRIM($2)) and UPPER(TRIM(COALESCE(surname,'')))=UPPER(TRIM($3))
                    `;
            let i=2;
            while (worksheet['A'+i]!==undefined)  {  
                if (i % 100 == 0) {
                    console.log(i);
                }
                let valfname='';
                let valsurname='';
                let vallname='';
                let valdate='';
                let address_of_cellC='C'+i;
                let address_of_cellE='E'+i;
                let address_of_cellG='G'+i;
                let address_of_cellM='M'+i;
                let address_of_cellB='B'+i;
                if ((worksheet[address_of_cellC]!==undefined) && (worksheet[address_of_cellC]!==null)){
                    valsurname=worksheet[address_of_cellC].v;
                }
                if ((worksheet[address_of_cellE]!==undefined) && (worksheet[address_of_cellE]!==null)){
                    valfname=worksheet[address_of_cellE].v;
                }
                if ((worksheet[address_of_cellG]!==undefined) && (worksheet[address_of_cellG]!==null)){
                    vallname=worksheet[address_of_cellG].v;
                }
                if ((worksheet[address_of_cellM]!==undefined) && (worksheet[address_of_cellM]!==null)){
                    valdate=worksheet[address_of_cellM].v;
                } 
                let values=[valfname,vallname,valsurname,valdate];   
                let  data=await cntc.any(selstr2,values);
                if (data[0].kol==0){
                    XLSX.utils.sheet_add_aoa(worksheet, [[0]], {origin: address_of_cellB});} 
                if((data[0].kol!=0) && (data[0].filterdate!=0)){
                    XLSX.utils.sheet_add_aoa(worksheet, [[2]], {origin: address_of_cellB});} 
                if((data[0].kol!=0) && (data[0].filterdate==0)){
                    XLSX.utils.sheet_add_aoa(worksheet, [[1]], {origin: address_of_cellB});} 
                valfname='';
                valsurname='';
                vallname='';
                valdate='';      
                i++;
            }
        }
        XLSX.writeFile(wb,nodePath);
        return;
    }
    catch(err) {
        console.dir(err.message);
        throw {code: err.code, message: err.message};
    }
}
excel_check();