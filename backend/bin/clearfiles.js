var config = require('../config');
var fs = require('fs');
var cntc = require('../db').connection;
const path = require('path');

let populate_found = process.argv.filter((value) => value == '--delete');
let delfiles = false;
if (populate_found.length > 0) {
    // Если найден хоть один --populate_found в параметрах
    delfiles = true;
}

let checkInDB = async function (t, fname, tablename, fieldname) {
    try {
        let script = `select count(*) as cnt from ${tablename}
                         where ${fieldname}::jsonb @> '{"file": "${fname}"}'  and flagarchive=0;`;
        let data = await cntc.tx(async (t) => {
            return await t.oneOrNone(script, [], (val) => val.cnt);
        });
        return data;
    } catch (err) {
        console.dir(err);
        throw err;
    }
};

let go = async function (t) {
    let normalizedPath = process.env.UPLOAD_ROOT_DIR;
    console.log(config.uploadDir);
    let files = fs.readdirSync(normalizedPath);
    let ln = files.length;
    let i = 0;
    let deletedcnt = 0;
    for (let file of files) {
        let c = await checkInDB(t, file, 'archivefolderfile', 'archivefile');
        c = +c;
        if (c === 0) {
            if (delfiles) {
                let pth = path.normalize(normalizedPath + path.sep + file);
                fs.unlinkSync(pth);
            }
            deletedcnt++;
        }
        i++;
        if (i % 50 === 0) {
            console.log((100 * i) / ln + '%');
        }
    }
    console.log('Не найдено: ' + deletedcnt);
    process.exit(0);
};

cntc.tx((t) => {
    console.log(1);
    go(t);
});
