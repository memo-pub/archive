var fs = require('fs');
var cntc = require('../db').connection;


let getFilesList = async function (t,tablename, fieldname) {
    try {
        let script = `select ${fieldname} as fls from ${tablename}
                         where flagarchive=0;`;          
        let data = await t.any(script,[]);
        return data.map(val=>{return {filename: val.fls.filename, file: val.fls.file, storage: val.fls.storage};});
    }
    catch (err) {
        console.dir(err);
        throw err;
    }
};

let go = async function(t) {  
    let fls = await getFilesList(t,'archivefolderfile','archivefile');
    let fls_str = fls.map(val=>val.filename+':@:'+val.file+':@:'+val.storage);
    let buffer = fls_str.join('\n');
    fs.writeFileSync('fl_list.csv', buffer);
    console.dir(fls_str);
    console.dir('finished');
    process.exit(0);
};

cntc.tx(t=>{
    go(t);
});

