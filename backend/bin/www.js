#!/usr/bin/env node

const http = require('node:http');
const process = require('node:process');
const debug = require('debug')('slib-pg:server');
const app = require('../app');
const config = require('../config');
const { getStorageRootPaths, checkStorageDirIsValid } = require('../helpers/files');
const { makeModelsLinks, merge, populate } = require('../models');

const nodeProcessArgumentsList = new Set(process.argv.slice(2));

const checkUploadPaths = async () => {
    const uploadRoots = getStorageRootPaths();
    await Promise.all(uploadRoots.map((dirPath) => checkStorageDirIsValid(dirPath)));
};

const makeMerge = async () => {
    console.log('Start merge.');
    await merge();
    console.log('End merge.');
    process.exit(0);
};

const makePopulate = async () => {
    console.log('Start populate.');
    await populate();
    console.log('End populate.');
};

/**
 * Normalize a port into a number, string, or false.
 */

const normalizePortValue = (portValue) => {
    const numericValue = Number.parseInt(portValue, 10);
    return Number.isNaN(numericValue) ? portValue : numericValue >= 0 ? numericValue : false;
};

/**
 * Event listener for HTTP server "error" event.
 */

const onServerError = (error) => {
    if (error.syscall !== 'listen') {
        throw error;
    }

    const bind = typeof port === 'string' ? `Pipe ${port}` : `Port ${port}`;

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES': {
            console.error(`${bind} requires elevated privileges`);
            process.exit(1);
            break;
        }

        case 'EADDRINUSE': {
            console.error(`${bind} is already in use`);
            process.exit(1);
            break;
        }

        default: {
            throw error;
        }
    }
};

/**
 * Event listener for HTTP server "listening" event.
 */

const onSeverListening = () => {
    const addr = server.address();
    const bind = typeof addr === 'string' ? `pipe ${addr}` : `port ${addr.port}`;
    debug(`Listening on ${bind}`);
};

if (nodeProcessArgumentsList.has('--merge')) {
    makeMerge();
}

if (nodeProcessArgumentsList.has('--populate')) {
    makePopulate();
}

checkUploadPaths();
makeModelsLinks();

/**
 * Get port from environment and store in Express.
 */

const port = normalizePortValue(process.env.PORT || config.port);
app.set('port', port);

/**
 * Create HTTP server.
 */

const server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */

server.listen(port);
server.on('error', onServerError);
server.on('listening', onSeverListening);
