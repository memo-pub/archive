var cntc = require('../db').connection;
var ProtoModel = require('../slib/protomodel');
var MConfig = require('../models/mconfig');

class ScanStatistic extends ProtoModel {
    constructor() {
        super();          
        this.addField('tm1',{tp: 'TIMESTAMP', required: true});
        this.addField('tm2',{tp: 'TIMESTAMP', required: true});
    }   

    async putGuard () {   
        return {gres: false, text: 'Изменение запрещено.'};
    }
    
    async postGuard () {   
        return {gres: false, text: 'Вставка запрещена.'};
    }

    async deleteGuard () {         
        return {gres: false, text: 'Удаление запрещено.'};
    }    

    async getAllGuard () { 
        return {gres: true, text: 'Ok'};
    }

    async getSelectScript() {

        /////////
        let mc = new MConfig();
        mc.key = 'file_scan_price';
        let delo_price;
        try {
            delo_price = await mc.selectOneByIdJSON();
            delo_price = delo_price.value.value;            
        }   
        catch (err) {
            return {message: 'Не удалось получить данные настроек.'};
        }
        /////////
        mc.key = 'standart_page_price';
        let page_price;
        try {
            page_price = await mc.selectOneByIdJSON();
            page_price = page_price.value.value;            
        }   
        catch (err) {
            return {message: 'Не удалось получить данные настроек.'};
        }
        /////////
        mc.key = 'special_page_price';
        let spec_page_price;
        try {
            spec_page_price = await mc.selectOneByIdJSON();
            spec_page_price = spec_page_price.value.value;            
        }   
        catch (err) {
            return {message: 'Не удалось получить данные настроек.'};
        }
        /////////

        let values = [this.tm1, this.tm2];
        
        let result = `SELECT id, login, name, surname,json_agg(json_build_object('fond', rrr.fond, 'opis_name', rrr.opis_name,
                   'trudindex_scann', rrr.trudindex_scann, 'kolvo', rrr.kolvo, 'pgcnt', rrr.pgcnt,'spec_pgcnt', rrr.spec_pgcnt,'summa', rrr.summa)) as opis,
                    SUM(kolvo) as kolvo, SUM(pgcnt) as pgcnt,SUM(spec_pgcnt) as spec_pgcnt, SUM(summa) as summa 
                    FROM (SELECT t1.id, t1.login, t1.name, t1.surname,t3.fond,t3.opis_name,t3.trudindex_scann,
                    COUNT(t2.id) as kolvo, SUM(t2.page_cnt) as pgcnt,
                    SUM(t2.spec_page_cnt) as spec_pgcnt, 
                    SUM(${delo_price}+t3.trudindex_scann*(${page_price}*t2.page_cnt+${spec_page_price}*t2.spec_page_cnt)) as summa FROM muser t1 
                LEFT OUTER JOIN archivefolder t2
                    on (   
                        (t1.id=t2.scanuser_id) and (t2.status>=3)
                        and 
                        (
                            ((t2.date_finish is not null) and (t2.date_finish>=date($1)) and (t2.date_finish<(date($2) + interval '1 day')))
                            or
                            ((t2.date_finish is null) and (t2.date_created>=date($1)) and (t2.date_created<(date($2) + interval '1 day')))
                        )
                    )
                    
                LEFT OUTER JOIN opis t3
                    on t2.opis_id=t3.id
                WHERE t1.flagarchive=0 AND t1.usertype=2 and t2.id is not null
                GROUP BY t1.id, t1.login, t1.name, t1.surname, t3.fond,t3.opis_name,t3.trudindex_scann) as rrr
                GROUP BY id, login, name, surname`; 
                
        return {script: result, values: values};        
    }
    
    async selectAllJSON() {                   
        let selscrpt =await this.getSelectScript();        
        let script = 'select json_agg(winquery) as data from ('+selscrpt.script+') winquery;';                    
        try {
            let data = await cntc.tx( async t => {
                return await t.oneOrNone(script,selscrpt.values,val=>val.data);
            });            
            return data;
        }    
        catch(err) {                    
            throw {code: err.code, message: err.message};         
        }               
    }
}


module.exports = ScanStatistic;