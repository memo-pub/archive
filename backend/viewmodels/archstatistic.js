var cntc = require('../db').connection;
var ProtoModel = require('../slib/protomodel');
var MConfig = require('../models/mconfig');

class ArchStatistic extends ProtoModel {
    constructor() {
        super();          
        this.addField('tm1',{tp: 'TIMESTAMP', required: true});
        this.addField('tm2',{tp: 'TIMESTAMP', required: true});
    }   

    async putGuard () {   
        return {gres: false, text: 'Изменение запрещено.'};
    }
    
    async postGuard () {   
        return {gres: false, text: 'Вставка запрещена.'};
    }

    async deleteGuard () {         
        return {gres: false, text: 'Удаление запрещено.'};
    }    

    async getAllGuard () { 
        return {gres: true, text: 'Ok'};
    }

    async getSelectScript() {

        /////////
        let mc = new MConfig();
        mc.key = 'file_archive_price';
        let price;
        try {
            price = await mc.selectOneByIdJSON();
            price = price.value.value;            
        }   
        catch (err) {
            return {message: 'Не удалось получить данные настроек.'};
        }
        /////////

        let values = [this.tm1, this.tm2];
                
        let result = `SELECT id, login, name, surname,json_agg(json_build_object('fond', rrr.fond, 'opis_name', rrr.opis_name,
                       'trudindex', rrr.trudindex, 'kolvo', rrr.kolvo, 'kolvo_z', rrr.kolvo_z,'kolvo_s', rrr.kolvo_s,'summa', rrr.summa)) as opis,
                        SUM(kolvo) as kolvo,SUM(kolvo_z) as kolvo_z,SUM(kolvo_s) as kolvo_s,SUM(summa) as summa FROM (
                        SELECT t1.id, t1.login, t1.name, t1.surname, t3.fond,t3.opis_name,t3.trudindex, COUNT(t2.id) as kolvo, COUNT(t2.id) FILTER (WHERE t2.status>0) as kolvo_z,
                        COUNT(t2.id) FILTER (WHERE t2.status=0) as kolvo_s,SUM(t3.trudindex)*(${price}) as summa FROM muser t1 
                         LEFT OUTER JOIN archivefolder t2
                             on ((t1.id=t2.createduser_id) and (t2.status>= 0)
                                 and (t2.date_created>=date($1)) and (t2.date_created<(date($2) + interval '1 day')))
                         LEFT OUTER JOIN opis t3
                             on t2.opis_id=t3.id
                         WHERE t1.flagarchive=0 AND ((t1.usertype=1) or (t1.usertype=3)) and t2.id is not null
                         GROUP BY t1.id, t1.login, t1.name, t1.surname,t3.fond,t3.opis_name,t3.trudindex) as rrr
                         GROUP BY id, login, name, surname`;
        //номер фонда, номер описи, число созданных дел, число заполненных дел, трудоемкость, сумма 
        
             
        return {script: result, values: values};        
    }
    
    async selectAllJSON() {                   
        let selscrpt =await this.getSelectScript();        
        let script = 'select json_agg(winquery) as data from ('+selscrpt.script+') winquery;';                    
        try {
            let data = await cntc.tx( async t => {
                return await t.oneOrNone(script,selscrpt.values,val=>val.data);                
            });            
            return data;
        }    
        catch(err) {                    
            throw {code: err.code, message: err.message};         
        }               
    }
}


module.exports = ArchStatistic;