var cntc = require('../db').connection;
var ProtoModel = require('../slib/protomodel');

class GenerateDeloRange extends ProtoModel {
    constructor() {
        super();          
        this.addInputField('fund'); // переменная класса
        this.addInputField('list_no'); // переменная класса
        this.addInputField('file_no'); // переменная класса
        this.addInputField('file_no_ext'); // переменная класса
    }   
   
    async putGuard () {   
        return {gres: false, text: 'Изменение запрещено.'};
    }
    
    async deleteGuard () {         
        return {gres: false, text: 'Удаление запрещено.'};
    } 

    async postGuard () {   
        return {gres: false, text: 'Вставка запрещена.'};
    }
    
    async getAllGuard () { 
        return {gres: true, text: 'Ok'};
    }


    async selectAllJSON() { 
        if ((this.fund==null)|| (this.list_no==null)||(this.file_no==null)){
            throw {code: 409, message: 'Не заполнены нужные поля : фонд, опись и дело.'};
        }
        if (this.file_no_ext==null){
            this.file_no_ext='';
        }

        //    СОЗДАТЬ ЗАПРОС И ЗАПОЛНИТЬ VALUES на месте нижеследующего комментария
        
        let selstr=` select min(t0.sour_date_begin),max(t0.sour_date_end) from sour_t t0
        where t0.fund=$1 and t0.list_no=$2 and t0.file_no=$3 and t0.file_no_ext=$4`;
        try {
            let values=[+this.fund,+this.list_no,+this.file_no,this.file_no_ext];
            let data = await cntc.tx(async t => {
                return await t.any(selstr,values);
            });
            let ddd={};
            ddd.yearmin=null;
            ddd.yearmax=null;
            if ((data[0].min!==null) && (data[0].min!==undefined)){
                ddd.yearmin=(data[0].min).getFullYear();
            }
            if ((data[0].max!==null) && (data[0].max!==undefined)){
                ddd.yearmax=(data[0].max).getFullYear();
            }
            return ddd;
        }
        catch(err) {
            console.dir(err.message);
            throw {code: err.code, message: err.message};

        }       
    }
        
}
module.exports = GenerateDeloRange;