const dateformat = require('dateformat');
const fs = require('fs').promises;
const generateSafeId = require('generate-safe-id');
const path = require('path');

const { getFileStorageRoot, getSubFolderPathFomIdandDate } = require('../helpers/files');
const ArchiveFolderFile = require('../models/archivefolderfile');
const cntc = require('../db').connection;
const ProtoModel = require('../slib/protomodel');

class MarchFileCopy extends ProtoModel {
    constructor() {
        super();
        this.addInputField('files_name'); // Переменная класса
        this.addInputField('archivefolder_id'); // Переменная класса
    }

    async putGuard() {
        return { gres: false, text: 'Изменение запрещено.' };
    }

    async deleteGuard() {
        return { gres: false, text: 'Удаление запрещено.' };
    }

    async getAllGuard() {
        return { gres: true, text: 'Чтение запрещено.' };
    }

    /**
     * Проверяет параметры POST запроса на корректность, и вызывает исключение, если параметры не корректны
     */
    testInputParams() {
        const isNumber = (n) => /^-?[\d.]+(?:e-?\d+)?$/.test(n);

        if (this.files_name === undefined) {
            throw {
                code: 409,
                message: 'Не заданы имена файлов для копирования',
            };
        }

        if (!isNumber(this.archivefolder_id)) {
            throw { code: 409, message: 'Неверно задан archivefolder_id' };
        }

        if (!Array.isArray(this.files_name)) {
            this.files_name = [this.files_name];
        }
    }

    /**
     * Из файла настроек определяет текущий каталог для загрузки файлов
     */
    getUploadDir() {
        const currStor = null; // Название текущего хранилища
        const uploadDir = getFileStorageRoot();

        return { uploadDir, currStor };
    }

    /**
     * Проверяет, есть ли уже хотя бы одно из указанных имен файлов у заданного archivefolder_id
     * @param {number} archiveFolderId - id дела
     * @param {array} fileNames - массив имен файлов для тестирования
     */

    async checkFileNamesAreUniqueForArchiveFolderId(archiveFolderId, fileNames) {
        const sql = `select count(*) as cnt 
                    from archivefolderfile 
                    where archivefolder_id=$1 and archivefile->>'filename'=ANY($2)`;

        const queryResult = await cntc.one(sql, [archiveFolderId, fileNames], (val) => val.cnt);

        return !(queryResult > 0);
    }

    /**
     * Подготавливает объекты для вставки в БД для каждого файла. Проверяет их наличие на диске.
     * @param {string} sourceDir - папка для чтения файлов
     * @param {*} currentStorageName - имя текущего хранилища
     * @param {array} fileNames - список имен файлов
     */

    async generateFilesData(fileNames) {
        return Promise.all(
            fileNames.map(async (originalFilename) => {
                const currentFilePath = path.join(process.env.READ_DIR, originalFilename);
                const fileStat = await fs.stat(currentFilePath).catch((err) => {
                    throw { code: 500, message: err.message };
                });

                const prefix = 'copy';
                const safeId = generateSafeId();
                const datePart = dateformat(new Date(), 'yyyymmddHHMMssl');
                const storageFileName = [prefix, safeId, datePart].join('_');

                const objFile = {
                    file: storageFileName,
                    filename: originalFilename,
                    size: fileStat.size,
                };

                return objFile;
            })
        );
    }

    async fileCopy(sourceDir, fileObj) {
        const aff = new ArchiveFolderFile();
        aff.archivefolder_id = this.archivefolder_id;
        aff.archivefile = fileObj;
        await aff.insert();

        const sourceFilePath = path.join(sourceDir, fileObj.filename);
        const storageFileName = fileObj.file;
        const date = new Date();
        const archiveFolderFileId = aff.id;

        const destinationSubFolder = getSubFolderPathFomIdandDate({ date, id: archiveFolderFileId });
        const destinationFilePath = path.join(process.env.UPLOAD_ROOT_DIR, destinationSubFolder, storageFileName);

        await fs.copyFile(sourceFilePath, destinationFilePath);
    }

    async insert() {
        this.testInputParams(); // Проверить корректность параметров запроса

        const fileNamesAreUnique = await this.checkFileNamesAreUniqueForArchiveFolderId(
            this.archivefolder_id,
            this.files_name
        );

        if (!fileNamesAreUnique) {
            // Проверить дело на наличие таких файлов
            // Если хотя бы один файл уже есть, то ничего не копируем, а предупреждаем пользователя
            throw { code: 409, message: 'Один из файлов уже есть в деле!' };
        }

        const fileObjects = await this.generateFilesData(this.files_name).catch((err) => {
            throw err;
        }); // Сгенерировать данные для всех файлов

        // Для всех сгенерированных объектов запустить копирование
        await Promise.all(
            fileObjects.map((fileObj) => {
                const sourcePath = process.env.UPLOAD_READ_DIR;
                return this.fileCopy(sourcePath, fileObj).catch((err) => {
                    console.dir(err);
                });
            })
        );

        return 'ok';
    }

    async selectAllJSON() {
        try {
            const files = await fs.readdir(process.env.UPLOAD_READ_DIR);

            return Promise.all(
                files.map(async (filePath) => {
                    try {
                        const { size, mtime, birthtime } = await fs.stat(filePath).catch((err) => {
                            throw { code: 500, message: err.message };
                        });

                        const result = {
                            name: filePath,
                            size,
                            date: mtime,
                            birthtime,
                        };

                        return result;
                    } catch (err) {
                        console.dir(err);
                    }
                })
            );
        } catch (err) {
            throw { code: err.code, message: err.message };
        }
    }
}
module.exports = MarchFileCopy;
