var cntc = require('../db').connection;
var ProtoModel = require('../slib/protomodel');
var Selection = require('../models/selection');
var pgp = require('../db').pgp;


class AddSelectionItem extends ProtoModel {
    constructor() {
        super();          
        this.addField('selection_id',{tp: 'BIGINT', required: true});

        // Поля для personal_t
        this.addFilter('personal_t_code',['t0.code']);
        /*this.addFilter('personal_t_mark',['t0.mark']);
        this.addFilter('personal_t_type_r',['t0.type_r']);
        this.addFilter('personal_t_type_i',['t0.type_i']);
        this.addFilter('personal_t_type_a',['t0.type_a']);*/

        this.addUserFilter('personal_t_mark','t0.mark IS NOT DISTINCT FROM {#f_value}');
        this.addUserFilter('personal_t_type_r','t0.type_r IS NOT DISTINCT FROM {#f_value}');
        this.addUserFilter('personal_t_type_i','t0.type_i IS NOT DISTINCT FROM {#f_value}');
        this.addUserFilter('personal_t_type_a','t0.type_a IS NOT DISTINCT FROM {#f_value}');

        this.addFilter('personal_t_surname',['t0.surname','t0.real_surname_rmk']);
        this.addFilter('personal_t_fname',['t0.fname', 't0.fname_rmk']);
        this.addFilter('personal_t_lname',['t0.lname', 't0.lname_rmk']);

        //this.addFilter('personal_t_sex',['t0.sex']);
        this.addUserFilter('personal_t_sex','t0.sex IS NOT DISTINCT FROM {#f_value}');
        
        this.addUserFilter('personal_t_birth1','t0.birth >= {#f_value}');
        this.addUserFilter('personal_t_birth2','t0.birth <= {#f_value}');
        this.addUserFilter('personal_t_birth_from', 't0.birth_end>={#f_value}');// 
        this.addUserFilter('personal_t_birth_to', 't0.birth_begin<={#f_value}');// 
        
        this.addFilter('personal_t_birth_rmk',['t0.birth_rmk']);

        this.addUserFilter('personal_t_death1','t0.death >= {#f_value}');
        this.addUserFilter('personal_t_death2','t0.birth <= {#f_value}');
        this.addUserFilter('personal_t_death_from', 't0.death_end>={#f_value}');// 
        this.addUserFilter('personal_t_death_to', 't0.death_begin<={#f_value}');// 

        this.addFilter('personal_t_death_date_rmk',['t0.death_date_rmk']);

        this.addFilter('personal_t_birth_place',['t0.birth_place_rmk', 't0_pb.place_num','t0_pb.place_code','t0_pb.place_name']);
        this.addFilter('personal_t_nation',['t0.nation_rmk','t0_nat.nation', 't0_nat.nation_rmk']);
        this.addFilter('personal_t_citiz',['t0.citiz', 't0.citiz_rmk']);

        this.addFilter('personal_t_origin_rmk',['t0.origin_rmk']);
        this.addUserFilter('personal_t_origin','t0.origin_id = {#f_value}'); //
        this.addInputField('personal_t_origin_str', 'any text');
        this.addFilter('personal_t_educ_rmk',['t0.educ_rmk']);
        this.addUserFilter('personal_t_educ','t0.educ_id = {#f_value}'); //
        this.addInputField('personal_t_educ_str', 'any text');
        this.addFilter('personal_t_spec_rmk',['t0.spec_rmk']);
        this.addUserFilter('personal_t_spec','t0.spec_id = {#f_value}'); //
        this.addInputField('personal_t_spec_str', 'any text');
        this.addFilter('personal_t_address',['t0.address', 't0.address_rmk']);
        this.addFilter('personal_t_phone',['t0.phone', 't0.phone_rmk']);
        this.addFilter('personal_t_notice',['t0.notice']);
        this.addFilter('personal_t_commentariy',['t0.surname_rmk']);

        this.addFilter('personal_t_place',['t0.residence_code_rmk', 't0_pl.place_num','t0_pl.place_code','t0_pl.place_name']);

        this.addUserFilter('personal_t_mentioned_person','t0.mentioned_person = {#f_value}');

        this.addFilter('personal_t_url',['t0.url']);


        this.addUserFilter('personal_t_fund','t0.fund = {#f_value}');
        this.addUserFilter('personal_t_list_no','t0.list_no = {#f_value}');
        this.addUserFilter('personal_t_file_no','t0.file_no = {#f_value}');

        // t1 - activity_t
        this.addFilter('activity_t_sphere',['t1.sphere_rmk']); //Сфера деятельности
        this.addUserFilter('activity_t_sphere_id','true' /*'t3.rehabil_reason_id = {#f_value}'*/); //
        this.addInputField('activity_t_sphere_str', 'any text');

        this.addFilter('activity_t_prof',['t1.prof', 't1.prof_rmk']); //профессия
        this.addFilter('activity_t_post',['t1.post', 't1.post_rmk']); //должность

        this.addUserFilter('activity_t_arrival_dat1','t1.arrival_dat >= {#f_value}'); //дата поступления
        this.addUserFilter('activity_t_arrival_dat2','t1.arrival_dat <= {#f_value}');
        this.addUserFilter('activity_t_arrival_dat_from', 't1.arrival_dat_end>={#f_value}');// 
        this.addUserFilter('activity_t_arrival_dat_to', 't1.arrival_dat_begin<={#f_value}');// 
        this.addFilter('activity_t_arrival_dat_rmk',['t1.arrival_dat_rmk']);

        this.addUserFilter('activity_t_depart_dat1','t1.depart_dat >= {#f_value}'); //дата увольнения
        this.addUserFilter('activity_t_depart_dat2','t1.depart_dat <= {#f_value}');
        this.addUserFilter('activity_t_depart_dat_from', 't1.depart_dat_end>={#f_value}');// 
        this.addUserFilter('activity_t_depart_dat_to', 't1.depart_dat_begin<={#f_value}');// 

        this.addFilter('activity_t_depart_dat_rmk',['t1.depart_dat_rmk']);

        this.addFilter('activity_t_ent1_rmk',['t1.ent1_rmk']); // комментарий к организации
        //this.addFilter('activity_t_ent_code',['t1.ent_code']); //  код организации Фильтр больше не используется!!
        
        this.addFilter('activity_t_place',['t1.geoplace_code_rmk', 't1_pl.place_num','t1_pl.place_code','t1_pl.place_name']);

        // t2 - org_t
        //this.addFilter('org_t_org_code',['t2.org_code']); //  код организации
        this.addUserFilter('org_t_org_code','t2.org_code_id = {#f_value}'); // тип организации  
        this.addInputField('org_t_org_code_str', 'any text');
        this.addFilter('org_t_org_name',['t2.org_name', 't2.ent_name_rmk']); //  название организации
        this.addFilter('org_t_particip_rmk',['t2.particip_rmk']); //  функция
        this.addUserFilter('org_t_particip','t2.particip_id = {#f_value}'); // 
        this.addInputField('org_t_particip_str', 'any text');
        this.addFilter('org_t_com_rmk',['t2.com_rmk']); //  общий комментарий

        this.addUserFilter('org_t_join_dat1','t2.join_dat >= {#f_value}'); //дата вступления
        this.addUserFilter('org_t_join_dat2','t2.join_dat <= {#f_value}');
        this.addUserFilter('org_t_join_dat_from', 't2.join_dat_end>={#f_value}');// 
        this.addUserFilter('org_t_join_dat_to', 't2.join_dat_begin<={#f_value}');// 
        this.addFilter('org_t_join_dat_rmk',['t2.join_dat_rmk']);

        this.addUserFilter('org_t_dismis_dat1','t2.dismis_dat >= {#f_value}'); //дата вступления
        this.addUserFilter('org_t_dismis_dat2','t2.dismis_dat <= {#f_value}');
        this.addUserFilter('org_t_dismis_dat_from', 't2.dismis_dat_end>={#f_value}');// 
        this.addUserFilter('org_t_dismis_dat_to', 't2.dismis_dat_begin<={#f_value}');// 
        this.addFilter('org_t_dismis_dat_rmk',['t2.dismis_dat_rmk']);

        // t3 - repress_t
        this.addUserFilter('repress_t_repress_no','t3.repress_no = {#f_value}');
        this.addFilter('repress_t_repress_type_rmk',['t3.repress_type_rmk']); //  комментарии к типу репрессии
        this.addUserFilter('repress_t_repress_type','t3.repress_type_id = {#f_value}');// тип репрессии
        this.addInputField('repress_t_repress_type_str', 'any text');
        this.addUserFilter('repress_t_repress_dat1','t3.repress_dat >= {#f_value}'); //Дата репрессии
        this.addUserFilter('repress_t_repress_dat2','t3.repress_dat <= {#f_value}');
        this.addUserFilter('repress_t_repress_dat_from', 't3.repress_dat_end>={#f_value}');// 
        this.addUserFilter('repress_t_repress_dat_to', 't3.repress_dat_begin<={#f_value}');// 
        this.addFilter('repress_t_repress_dat_rmk',['t3.repress_dat_rmk']);
        this.addFilter('repress_t_rehabil_org',['t3.rehabil_org']);
        this.addUserFilter('repress_t_rehabil_reason_id','true' /*'t3.rehabil_reason_id = {#f_value}' */); // причина реабилитации
        this.addInputField('repress_t_rehabil_reason_str', 'any text');

        
        this.addFilter('repress_t_court_mark',['t3.court_mark']); // Был ли суд - обратить внимание на возможные значения и тип контрола
        this.addFilter('repress_t_reabil_mark',['t3.reabil_mark']); // Была ли реабилитация - обратить внимание на возможные значения и тип контрола

        this.addUserFilter('repress_t_reabil_dat1','t3.reabil_dat >= {#f_value}'); //Дата реабилитации
        this.addUserFilter('repress_t_reabil_dat2','t3.reabil_dat <= {#f_value}');
        this.addUserFilter('repress_t_reabil_dat_from', 't3.reabil_dat_end>={#f_value}');// 
        this.addUserFilter('repress_t_reabil_dat_to', 't3.reabil_dat_begin<={#f_value}');// 

        this.addFilter('repress_t_reabil_rmk',['t3.reabil_rmk']); //Комментарий к реабилитации
        this.addFilter('repress_t_repress_rmk',['t3.repress_rmk']); //Комментарий к репрессии

        this.addFilter('repress_t_place',['t3.geoplace_code_rmk', 't3_pl.place_num','t3_pl.place_code','t3_pl.place_name']);

        // t4 - court_t
        this.addUserFilter('court_t_trial_no','t4.trial_no = {#f_value}');
        this.addFilter('court_t_inq_name',['t4.inq_name','t4.inq_name_rmk']); // Следственный орган
        this.addUserFilter('court_t_inq_date1','t4.inq_date >= {#f_value}'); //Дата репрессии
        this.addUserFilter('court_t_inq_date2','t4.inq_date <= {#f_value}');
        this.addUserFilter('court_t_inq_date_from', 't4.inq_date_end>={#f_value}');// 
        this.addUserFilter('court_t_inq_date_to', 't4.inq_date_begin<={#f_value}');// 
        this.addFilter('court_t_inq_date_rmk',['t4.inq_date_rmk']);


        this.addFilter('court_t_court_name',['t4.court_name','t4.court_name_rmk']); // Судебный орган
        this.addUserFilter('court_t_sentense_date1','t4.sentense_date >= {#f_value}'); //Дата вынесения приговора
        this.addUserFilter('court_t_sentense_date2','t4.sentense_date <= {#f_value}');
        this.addUserFilter('court_t_sentense_date_from', 't4.sentense_date_end>={#f_value}');// 
        this.addUserFilter('court_t_sentense_date_to', 't4.sentense_date_begin<={#f_value}');// 
        this.addFilter('court_t_sentense_date_rmk',['t4.sentense_date_rmk']);

        this.addFilter('court_t_article',['t4.article','t4.article_rmk']); // Статья
        this.addFilter('court_t_sentense',['t4.sentense','t4.sentense_rmk']); // Приговор


        this.addFilter('court_t_trial_rmk',['t4.trial_rmk']); // Комментарий к судебному процессу
        this.addFilter('court_t_inq_rmk',['t4.inq_rmk']); // Комментарий к следствию

        this.addFilter('court_t_place_i',['t4.place_i_rmk', 't4_pl_i.place_num','t4_pl_i.place_code','t4_pl_i.place_name']); //места следствия
        this.addFilter('court_t_place_j',['t4.place_j_rmk', 't4_pl_j.place_num','t4_pl_j.place_code','t4_pl_j.place_name']); //места суда


        // t5 - impris_t
        this.addUserFilter('impris_t_impris_no','t5.impris_no = {#f_value}');
        this.addFilter('impris_t_prison_name',['t5.prison_name','t5.prison_name_rmk']); // Название места заключения

        this.addUserFilter('impris_t_arrival_dat1','t5.arrival_dat >= {#f_value}'); //Дата прибытия
        this.addUserFilter('impris_t_arrival_dat2','t5.arrival_dat <= {#f_value}');
        this.addUserFilter('impris_t_arrival_dat_from', 't5.arrival_dat_end>={#f_value}');// 
        this.addUserFilter('impris_t_arrival_dat_to', 't5.arrival_dat_begin<={#f_value}');// 
        this.addFilter('impris_t_arrival_dat_rmk',['t5.arrival_dat_rmk']);

        this.addUserFilter('impris_t_leave_dat1','t5.leave_dat >= {#f_value}'); //Дата убытия
        this.addUserFilter('impris_t_leave_dat2','t5.leave_dat <= {#f_value}');
        this.addUserFilter('impris_t_leave_dat_from', 't5.leave_dat_end>={#f_value}');// 
        this.addUserFilter('impris_t_leave_dat_to', 't5.leave_dat_begin<={#f_value}');// 
        this.addFilter('impris_t_leave_dat_rmk',['t5.leave_dat_rmk']);

        this.addFilter('impris_t_reason_rmk',['t5.reason_rmk']); // Причина убытия
        this.addUserFilter('impris_t_reason','t5.reason_id = {#f_value}'); //
        this.addInputField('impris_t_reason_str', 'any text');
        this.addFilter('impris_t_work',['t5.work','t5.work_rmk']); // Работа в заключении
        this.addFilter('impris_t_protest_rmk',['t5.protest_rmk']); // Акции протеста в заключении
        this.addUserFilter('impris_t_protest','t5.protest_id = {#f_value}');
        this.addInputField('impris_t_protest_str', 'any text');

        this.addUserFilter('impris_t_impris_type','t5.impris_type_id = {#f_value}'); // 
        this.addInputField('impris_t_impris_type_str', 'any text');

        this.addFilter('impris_t_place',['t5.prison_place_rmk', 't5_pl.place_num','t5_pl.place_code','t5_pl.place_name']); //место заключения

        // t6 - sour_t

        this.addFilter('sour_t_sour_name',['t6.sour_name']); // Название источника
        //this.addFilter('sour_t_sour_code',['t6.sour_code']); // Шифр источника
        this.addUserFilter('sour_t_sour_code','t6.sour_code = {#f_value}::text'); // номер листа
        this.addFilter('sour_t_mark_mem',['t6.mark_mem']); // Источник хранится в архиве
        this.addFilter('sour_t_sour_rmk',['t6.sour_rmk']); // Комментарий источника
        //this.addFilter('sour_t_sour_type',['t6.sour_type']); 
        //this.addUserFilter('sour_t_sour_type','t6.sour_type = {#f_value}'); // тип документа
        this.addUserFilter('sour_t_sour_type_id','true' /*'t3.rehabil_reason_id = {#f_value}'*/);
        this.addInputField('sour_t_sour_type_str', 'any text');
        this.addFilter('sour_t_sour_mark',['t6.sour_mark']); // Оригинал или копия
        this.addFilter('sour_t_place_so',['t6.place_so']); // Местонахождения источника
        this.addFilter('sour_t_sour_date',['t6.sour_date']); // Дата источника
        this.addUserFilter('sour_t_sour_date_from', 't6.sour_date_end>={#f_value}');// 
        this.addUserFilter('sour_t_sour_date_to', 't6.sour_date_begin<={#f_value}');//

        /*
        // t7 - usage_t
        this.addFilter('usage_t_sour_txt',['t7.sour_txt']); 
        this.addFilter('usage_t_sour_code',['t7.sour_code']);
        this.addUserFilter('usage_t_us_date1','t7.us_date >= {#f_value}'); //Дата источника
        this.addUserFilter('usage_t_us_date2','t7.us_date <= {#f_value}');
        this.addUserFilter('usage_t_us_type_id','t7.us_type_id = {#f_value}'); */

        // t8 - help_t
        this.addFilter('help_t_help_typ_rmk',['t8.help_typ_rmk']);  // тип помощи
        this.addUserFilter('help_t_help_typ','t8.help_typ_id = {#f_value}'); // 
        this.addInputField('help_t_help_typ_str', 'any text');
        this.addFilter('help_t_whom',['t8.whom','t8.whom_rmk']);  // кому оказывалась помощь

        this.addUserFilter('help_t_help_dat1','t8.help_dat >= {#f_value}'); //дата помощи
        this.addUserFilter('help_t_help_dat2','t8.help_dat <= {#f_value}');

        // t1_co_perso 
        this.addFilter('co_t_surname',['t1_co_perso.surname','t1_co_perso.real_surname_rmk', 't2_co_perso.surname','t2_co_perso.real_surname_rmk'
            ,'t3_co_perso.surname','t3_co_perso.real_surname_rmk','t4_co_perso.surname','t4_co_perso.real_surname_rmk'
            ,'t5_co_perso.surname','t5_co_perso.real_surname_rmk','t9_co_perso.surname','t9_co_perso.real_surname_rmk'
            ,'t10_co_perso.surname','t10_co_perso.real_surname_rmk','t11_co_perso.surname','t11_co_perso.real_surname_rmk']);
        this.addFilter('co_t_fname',['t1_co_perso.fname', 't1_co_perso.fname_rmk','t2_co_perso.fname', 't2_co_perso.fname_rmk'
            ,'t3_co_perso.fname', 't3_co_perso.fname_rmk','t4_co_perso.fname', 't4_co_perso.fname_rmk'
            ,'t5_co_perso.fname', 't5_co_perso.fname_rmk','t9_co_perso.fname', 't9_co_perso.fname_rmk'
            ,'t10_co_perso.fname', 't10_co_perso.fname_rmk','t11_co_perso.fname', 't11_co_perso.fname_rmk']);
        this.addFilter('co_t_lname',['t1_co_perso.lname', 't1_co_perso.lname_rmk','t2_co_perso.lname', 't2_co_perso.lname_rmk'
            ,'t3_co_perso.lname', 't3_co_perso.lname_rmk','t4_co_perso.lname', 't4_co_perso.lname_rmk'
            ,'t5_co_perso.lname', 't5_co_perso.lname_rmk','t9_co_perso.lname', 't9_co_perso.lname_rmk'
            ,'t10_co_perso.lname', 't10_co_perso.lname_rmk','t11_co_perso.lname', 't11_co_perso.lname_rmk']);
  

        this.addUserFilter('co_t_code'
            ,'((t1_co_perso.code = {#f_value}) OR (t2_co_perso.code = {#f_value}) OR (t3_co_perso.code = {#f_value}) OR (t4_co_perso.code = {#f_value}) OR (t5_co_perso.code = {#f_value}) OR (t9_co_perso.code = {#f_value}) OR (t10_co_perso.code = {#f_value}) OR (t11_co_perso.code = {#f_value}))');


    }   

    async putGuard () {   
        return {gres: false, text: 'Изменение запрещено.'};
    }
    
    async postGuard (user) {   
        if (this.sour_t_sour_mark==='O')
            this.sour_t_sour_mark='О';
        if ((this.selection_id===undefined)||(this.selection_id==='')) {
            return {gres: false, text: 'Не задан идентификатор выборки!'}; 
        }
        if ((!Number.isInteger(+this.selection_id))||(+this.selection_id<0)) {
            return {gres: false, text: 'Идентификатор выборки имеет неверный формат!'}; 
        }
        if (user.usertype===4)  {
            let selection = new Selection();
            selection.id = this.selection_id;
            let sl = await selection.selectOneByIdJSON();
            if (sl==null) {
                return {gres: false, text: 'Не найдена выборка для вставки.'};
            }
            if (sl.owner_id != user.id) {
                return {gres: false, text: 'Исследователь не может делать вставку в чужую выборку.'};
            }

        } 
        return {gres: true, text: 'OK'};
    }

    async deleteGuard () {         
        return {gres: false, text: 'Удаление запрещено.'};
    }    

    async getAllGuard () { 
        return {gres: false, text: 'Просмотр запрещен.'};
    }

    isSectionExists(name) {
        let keys = Object.keys(this._filters).filter(v=>(v.indexOf(name)===0));
        for (let k of keys) {
            if (this[k]!== undefined) return true;
        }
        let keysu = Object.keys(this._userfilters).filter(v=>(v.indexOf(name)===0));
        for (let k of keysu) {
            if (this[k]!== undefined) return true;
        }
        let keysi = Object.keys(this._inputfields).filter(v=>(v.indexOf(name)===0));
        for (let k of keysi) {
            if (this[k]!== undefined) return true;
        }
        return false;
    }

    async insert() {
        let has_co_t = this.isSectionExists('co_t');
        let has_activity_t = this.isSectionExists('activity_t')  || has_co_t;
        let has_org_t = this.isSectionExists('org_t')  || has_co_t;

        let has_impris_t = this.isSectionExists('impris_t')  || has_co_t;
        let has_court_t = this.isSectionExists('court_t') || has_impris_t;
        let has_repress_t = this.isSectionExists('repress_t') || has_court_t;
        let has_sour_t = this.isSectionExists('sour_t');
        let has_usage_t = this.isSectionExists('usage_t');
        let has_help_t = this.isSectionExists('help_t');
        let script = `INSERT INTO selectionitem (selection_id, personal_code)
                        (SELECT distinct ${this.selection_id}, t0.code FROM personal_t t0
                        LEFT OUTER JOIN place_t t0_pb
                            ON t0.birth_place_num=t0_pb.place_num AND t0_pb.cur_mark='*'
                        LEFT OUTER JOIN place_t t0_pl
                            ON t0.place_num=t0_pl.place_num AND t0_pl.cur_mark='*'
                        LEFT OUTER JOIN nat_t t0_nat
                            ON t0.nation_id=t0_nat.id
                        `;
        if (this.personal_t_origin_str != undefined) {
            script = script+ `INNER JOIN LIST_OF_T t0_org
                                    ON t0.origin_id=t0_org.id and (UPPER(t0_org.name_ent) like UPPER('${this.personal_t_origin_str}') 
                                        or UPPER(t0_org.name_rmk) like UPPER('${this.personal_t_origin_str}')) 
                                `;
        }
        
        if (this.personal_t_educ_str != undefined) {
            script = script+ `INNER JOIN LIST_OF_T t0_edu
                                    ON t0.educ_id=t0_edu.id and (UPPER(t0_edu.name_ent) like UPPER('${this.personal_t_educ_str}') 
                                        or UPPER(t0_edu.name_rmk) like UPPER('${this.personal_t_educ_str}')) 
                                `;
        }
        if (this.personal_t_spec_str != undefined) {
            script = script+ `INNER JOIN LIST_OF_T t0_spec
                                    ON t0.spec_id=t0_spec.id and (UPPER(t0_spec.name_ent) like UPPER('${this.personal_t_spec_str}') 
                                        or UPPER(t0_spec.name_rmk) like UPPER('${this.personal_t_spec_str}')) 
                                `;
        }

        if (has_activity_t) {
            
            
            script = script+ `LEFT OUTER JOIN activity_t t1
                                    ON t0.code=t1.personal_code 
                                LEFT OUTER JOIN place_t t1_pl
                                    ON t1.place_num=t1_pl.place_num AND t1_pl.cur_mark='*'
                                    `;
            if (has_co_t) {                        
                script = script+ `
                                LEFT OUTER JOIN coact_t t1_co
                                    ON t1.id=t1_co.act_id
                                LEFT OUTER JOIN personal_t_co t1_co_perso
                                    ON t1_co.human_code=t1_co_perso.code  
                        `;
            }
            if (this.activity_t_sphere_id!==undefined) {
                script = script+ `
                                INNER JOIN ACT_SPHERE_T t1_sphere
                                    ON t1.id=t1_sphere.act_id and t1_sphere.sphere_id=${this.activity_t_sphere_id}
                        `;
            }
            if (this.activity_t_sphere_str != undefined) {
                script = script+ `INNER JOIN ACT_SPHERE_T t1_sphere_s
                                        ON t1.id=t1_sphere_s.act_id
                                    INNER JOIN LIST_OF_T t1_sphere_l
                                        ON t1_sphere_s.sphere_id=t1_sphere_l.id and (UPPER(t1_sphere_l.name_ent) like UPPER('${this.activity_t_sphere_str}') 
                                            or UPPER(t1_sphere_l.name_rmk) like UPPER('${this.activity_t_sphere_str}') )
                                    `;
            }
        }
        if (has_org_t) {
            script = script+ `LEFT OUTER JOIN org_t t2
                                    ON t0.code=t2.personal_code
                                    `;
            if (has_co_t) {                        
                script = script+ `
                                LEFT OUTER JOIN coorg_t t2_co
                                    ON t2.id=t2_co.org_id
                                LEFT OUTER JOIN personal_t_co t2_co_perso
                                    ON t2_co.human_code=t2_co_perso.code 
                        `;
            }
            
            if (this.org_t_org_code_str != undefined) {
                script = script+ `INNER JOIN LIST_OF_T t2_org_code
                                        ON t2.org_code_id=t2_org_code.id and (UPPER(t2_org_code.name_ent) like UPPER('${this.org_t_org_code_str}') 
                                            or UPPER(t2_org_code.name_rmk) like UPPER('${this.org_t_org_code_str}') )
                                    `;
            }
            if (this.org_t_particip_str != undefined) {
                script = script+ `INNER JOIN LIST_OF_T t2_partic
                                        ON t2.particip_id=t2_partic.id and (UPPER(t2_partic.name_ent) like UPPER('${this.org_t_particip_str}') 
                                            or UPPER(t2_partic.name_rmk) like UPPER('${this.org_t_particip_str}') )
                                    `;
            }
        }
        if (has_repress_t) {
            script = script+ `LEFT OUTER JOIN repress_t t3
                                    ON t0.code=t3.personal_code 
                                LEFT OUTER JOIN place_t t3_pl
                                    ON t3.place_num=t3_pl.place_num AND t3_pl.cur_mark='*'
                                    `;
            if (has_co_t) {                        
                script = script+ `
                                LEFT OUTER JOIN corep_t t3_co
                                    ON t3.id=t3_co.rep_id
                                LEFT OUTER JOIN personal_t_co t3_co_perso
                                    ON t3_co.human_code=t3_co_perso.code 
                        `;
            }
            if (this.repress_t_rehabil_reason_id!==undefined) {
                script = script+ `
                                INNER JOIN rehabil_reason_t t3_reab
                                    ON t3.id=t3_reab.rep_id and t3_reab.reason_id=${this.repress_t_rehabil_reason_id}
                        `;
            }
            
            if (this.repress_t_rehabil_reason_str != undefined) {
                script = script+ `INNER JOIN rehabil_reason_t t3_reab_s
                                    ON t3.id=t3_reab_s.rep_id
                                    INNER JOIN LIST_OF_T t3_reab_type
                                        ON t3_reab_s.reason_id=t3_reab_type.id and (UPPER(t3_reab_type.name_ent) like UPPER('${this.repress_t_rehabil_reason_str}') 
                                            or UPPER(t3_reab_type.name_rmk) like UPPER('${this.repress_t_rehabil_reason_str}') )
                                    `;
            }
            if (this.repress_t_repress_type_str != undefined) {
                script = script+ `INNER JOIN LIST_OF_T t3_rep_type
                                        ON t3.repress_type_id=t3_rep_type.id and (UPPER(t3_rep_type.name_ent) like UPPER('${this.repress_t_repress_type_str}') 
                                            or UPPER(t3_rep_type.name_rmk) like UPPER('${this.repress_t_repress_type_str}') )
                                    `;
            }
        }
        if (has_court_t) {
            script = script+ `LEFT OUTER JOIN court_t t4
                                    ON t3.id=t4.rep_id 
                                LEFT OUTER JOIN place_t t4_pl_i
                                    ON t4.place_num_i=t4_pl_i.place_num AND t4_pl_i.cur_mark='*'
                                LEFT OUTER JOIN place_t t4_pl_j
                                    ON t4.place_num_j=t4_pl_j.place_num AND t4_pl_j.cur_mark='*'
                                    `;
            if (has_co_t) {                        
                script = script+ `
                                LEFT OUTER JOIN cocrt_t t4_co
                                    ON t4.id=t4_co.crt_id
                                LEFT OUTER JOIN personal_t_co t4_co_perso
                                    ON t4_co.human_code=t4_co_perso.code  
                        `;
            }
        }
        if (has_impris_t) {
            script = script+ `LEFT OUTER JOIN impris_t t5
                                    ON t4.id=t5.crt_id
                                LEFT OUTER JOIN place_t t5_pl
                                    ON t5.place_num=t5_pl.place_num AND t5_pl.cur_mark='*'
                                    `;
            if (has_co_t) {                        
                script = script+ `LEFT OUTER JOIN coimp_t t5_co
                                    ON t5.id=t5_co.imp_id
                                LEFT OUTER JOIN personal_t_co t5_co_perso
                                    ON t5_co.human_code=t5_co_perso.code  
                        `;
            }
            
            if (this.impris_t_reason_str != undefined) {
                script = script+ `INNER JOIN LIST_OF_T t5_reas
                                        ON t5.reason_id=t5_reas.id and (UPPER(t5_reas.name_ent) like UPPER('${this.impris_t_reason_str}') 
                                            or UPPER(t5_reas.name_rmk) like UPPER('${this.impris_t_reason_str}') )
                                    `;
            }
            
            if (this.impris_t_protest_str != undefined) {
                script = script+ `INNER JOIN LIST_OF_T t5_protest
                                        ON t5.protest_id=t5_protest.id and (UPPER(t5_protest.name_ent) like UPPER('${this.impris_t_protest_str}') 
                                            or UPPER(t5_protest.name_rmk) like UPPER('${this.impris_t_protest_str}') )
                                    `;
            }
            
            if (this.impris_t_impris_type_str != undefined) {
                script = script+ `INNER JOIN LIST_OF_T t5_imp_type
                                        ON t5.impris_type_id=t5_imp_type.id and (UPPER(t5_imp_type.name_ent) like UPPER('${this.impris_t_impris_type_str}') 
                                            or UPPER(t5_imp_type.name_rmk) like UPPER('${this.impris_t_impris_type_str}') )
                                    `;
            }
        }
        if (has_sour_t) {
            script = script+ `LEFT OUTER JOIN personal_sour_t t6_link
                                ON t0.code=t6_link.personal_code
                            LEFT OUTER JOIN sour_t t6
                                ON t6_link.sour_id=t6.id  
                        `;
            if (this.sour_t_sour_type_id!==undefined) {
                script = script+ `
                                INNER JOIN sour_type_t t6_stype
                                    ON t6.id=t6_stype.sour_id and t6_stype.sour_type_id=${this.sour_t_sour_type_id}
                        `;
            }
            
            if (this.sour_t_sour_type_str != undefined) {
                script = script+ `INNER JOIN sour_type_t t6_stype_s
                                        ON t6.id=t6_stype_s.sour_id
                                    INNER JOIN LIST_OF_T t6_sour_type
                                        ON t6_stype_s.sour_type_id=t6_sour_type.id and (UPPER(t6_sour_type.name_ent) like UPPER('${this.sour_t_sour_type_str}') 
                                            or UPPER(t6_sour_type.name_rmk) like UPPER('${this.sour_t_sour_type_str}') )
                                    `;
            }
        }
        if (has_usage_t) {
            script = script+ `LEFT OUTER JOIN usage_t t7
                                ON t0.code=t7.personal_code    
                        `;
        }
        if (has_help_t) {
            script = script+ `LEFT OUTER JOIN help_t t8
                                ON t0.code=t8.personal_code    
                        `;
            
            if (this.help_t_help_typ_str != undefined) {
                script = script+ `INNER JOIN LIST_OF_T t8_hlp_type
                                        ON t8.help_typ_id=t8_hlp_type.id and (UPPER(t8_hlp_type.name_ent) like UPPER('${this.help_t_help_typ_str}') 
                                            or UPPER(t8_hlp_type.name_rmk) like UPPER('${this.help_t_help_typ_str}') )
                                    `;
            }
        }
        if (has_co_t) {
            script = script + `LEFT OUTER JOIN inform_t t9_co
                    ON t0.code=t9_co.personal_code
                LEFT OUTER JOIN personal_t_co t9_co_perso
                    ON t9_co.human_code=t9_co_perso.code
                LEFT OUTER JOIN family_t t10_co
                    ON t0.code=t10_co.personal_code
                LEFT OUTER JOIN personal_t_co t10_co_perso
                    ON t10_co.human_code=t10_co_perso.code
                LEFT OUTER JOIN mentioned_t t11_co
                    ON t0.code=t11_co.personal_code
                LEFT OUTER JOIN personal_t_co t11_co_perso
                    ON t11_co.human_code=t11_co_perso.code
                    `;
        }
        let filters = this.getFilterWheres(1);    
        let values = filters.values;
        script = script + ' WHERE ';
        if (filters.inext>1) {       
            script = script + filters.script+ ' AND ';
        }
        script = script + `
                            t0.code not in (select personal_code from selectionitem where selection_id=${this.selection_id})
                        ) RETURNING id;`;
        if (has_co_t) {
            let cte_script = `WITH personal_t_co AS (
                SELECT * FROM PERSONAL_T`;  
            let cte_wheres = [];
            let idx = filters.inext;
            if (this.co_t_surname!==undefined) {
                cte_wheres.push('(UPPER(surname) like  \'%$'+idx+'#%\')');
                values.push(this.co_t_surname);
                idx++;
            }
            if (this.co_t_fname!==undefined) {
                cte_wheres.push('(UPPER(fname) like  \'%$'+idx+'#%\')');
                values.push(this.co_t_fname);
                idx++;
            } 
            if (this.co_t_lname!==undefined) {
                cte_wheres.push('(UPPER(lname) like  \'%$'+idx+'#%\')');
                values.push(this.co_t_lname);
                idx++;
            }
            if (this.co_t_code!==undefined) {
                cte_wheres.push('(code = $'+idx+')');
                values.push(this.co_t_code);
                idx++;
            }
            if (cte_wheres.length>0) {
                cte_script = cte_script + ' WHERE ' +cte_wheres.join(' AND ')+ ' ';
            }
            cte_script = cte_script +`)
            `;        
            script = cte_script + script;
        }
        try {
            let data = await cntc.tx( async t => {
                return await t.any(script,values);                
            });            
            return data;
        }    
        catch(err) {                    
            throw {code: err.code, message: err.message};         
        }           
    }


    getFilterWheres(istart) {
        let arr = [];
        let vl_data_array = [];
        let i = istart;
        for (var f in this._filters) {
            if (this[f]!==undefined) {
                let arr_in = [];
                for (var fld in this._filters[f])
                {
                    /*
                    if (this._filters[f][fld].indexOf('.')>0) {
                        arr_in.push('(UPPER('+this._filters[f][fld]+'::varchar) like  \'%$'+i+'#%\')');
                    }
                    else {
                        arr_in.push('(UPPER(t0.'+this._filters[f][fld]+'::varchar) like  \'%$'+i+'#%\')');
                    } */

                    if (this._filters[f][fld].indexOf('.')>0) {
                        arr_in.push('(UPPER('+this._filters[f][fld]+'::varchar) like  $'+i+')');
                    }
                    else {
                        arr_in.push('(UPPER(t0.'+this._filters[f][fld]+'::varchar) like  $'+i+')');
                    }
                }
                arr.push('('+arr_in.join(' OR ')+')');               
                vl_data_array.push(this[f]);
                i++;
            }            
        }
        for (f in this._userfilters) {
            if (this[f]!==undefined) {                                
                arr.push('('+this._userfilters[f].replace(/{#f_value}/g, '$'+i)+')');               
                vl_data_array.push(this[f]);
                i++;
            }            
        }
        let result = arr.join(' AND ');
        return {script: result, values: vl_data_array, inext: i};
    }

    translateError(err) {
        if (err.code==23505) {
            if (err.message.indexOf('selection_id_personal_code_selectionitem')>-1) {
                return {code: 409, message:'Персоналия уже есть в данной выборке!'};
            }
        }
        else if (err.code==23503) {
            if (err.message.indexOf('selectionitem_personal_code_fkey')>-1) {
                return {code: 409, message:'Выбранная персоналия не существует в базе данных!'};
            }
            else if (err.message.indexOf('selectionitem_selection_id_fkey')>-1) {
                return {code: 409, message:'Выбранная выборка не существует в базе данных!'};
            }
        }
    }
    
}


module.exports = AddSelectionItem;