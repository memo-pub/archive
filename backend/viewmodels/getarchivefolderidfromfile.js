var cntc = require('../db').connection;
var ProtoModel = require('../slib/protomodel');

class GetArchivefolderId extends ProtoModel {
    constructor() {
        super();
        this.addInputField('fondName');
        this.addInputField('opisName');
        this.addInputField('fileName');
    }

    async putGuard() {
        return { gres: false, text: 'Изменение запрещено.' };
    }

    async deleteGuard() {
        return { gres: false, text: 'Удаление запрещено.' };
    }

    async getAllGuard() {
        return { gres: false, text: 'Чтение запрещено.' };
    }

    async selectAllJSON() {
        if (!this.fondName || !this.opisName || !this.fileName) {
            throw {
                code: 409,
                message: 'Не заданы параметры для определения дела.',
            };
        }
        const sql_opis_id = `select id from opis where fond=$1 and opis_name=$2`;
        const opis_id = await cntc.manyOrNone(sql_opis_id, [
            this.fondName,
            this.opisName,
        ]);
        const sql_archivefolder_id = `select id from archivefolder where opis_id=$1 and delo=$2`;
        const archivefolderid = await cntc.manyOrNone(sql_archivefolder_id, [
            opis_id[0]?.id,
            this.fileName,
        ]);
        return archivefolderid[0];
    }
}
module.exports = GetArchivefolderId;
