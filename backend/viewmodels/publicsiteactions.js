const { connection: cntc } = require('../db');
const DELETE_PUBLIC_ENTITY = require('../models/delete_public_entity');
const ProtoModel = require('../slib/protomodel');

class PublicSiteActions extends ProtoModel {
    constructor() {
        super();
        this.addInputField('entity_id', {
            tp: 'BIGINT',
            visible: true,
            required: true,
        }); // id сущности
        this.addInputField('entity_type', {
            tp: 'INT',
            visible: true,
            required: true,
        });
        // тип сущности
        // 1 - персоналии
        // 2 - дела
        // 3 - фонд
        // 4 - опись
        // 5 - документ
        this.addInputField('action_delete', {
            tp: 'VARCHAR(1)',
            visible: true,
            required: true,
        }); // удаление записи о сущности
        this.addInputField('action_update', {
            tp: 'VARCHAR(1)',
            visible: true,
            required: true,
        }); // добавление или обновление записи о сущности

        this.addInputField('legacyId', {
            tp: 'VARCHAR(50)',
            visible: true,
            required: true,
        }); // поле для публикации
    }

    async putGuard() {
        return { gres: false, text: 'Изменение запрещено.' };
    }

    async deleteGuard() {
        return { gres: false, text: 'Удаление запрещено.' };
    }

    async getAllGuard() {
        return { gres: false, text: 'Чтение запрещено.' };
    }

    async updateChildrenPublishedFields(
        entityType,
        isActionDelete,
        entityId,
        trans = cntc,
    ) {
        const curDate = new Date().toISOString().split('T')[0];
        try {
            if (entityId) {
                const updateOpisFields = async () => {
                    if (entityType === 3) {
                        const sqlToPublish = `UPDATE OPIS SET published = '*', mustPublish = '*', mustUnpublish = null, pub_change_date = '${curDate}' WHERE fond IN 
                        (SELECT fond FROM FOND WHERE id=$1) and (pub_date <= '${curDate}' OR pub_date is null) RETURNING id;`;
                        const sqlToUnpublish = `UPDATE OPIS SET published = null, mustPublish = null, mustUnpublish = '*', pub_change_date = '${curDate}' WHERE fond IN (SELECT fond FROM FOND WHERE id=$1) RETURNING id;`;
                        const sql = isActionDelete
                            ? sqlToUnpublish
                            : sqlToPublish;
                        return await trans.manyOrNone(sql, [entityId]);
                    }
                };

                const updateArchivesFields = async () => {
                    if (entityType === 3) {
                        const sqlToPublish = `WITH fond_info as (SELECT fond FROM FOND WHERE id=$1),
                        opis_info as (SELECT t1.id FROM OPIS t1, fond_info t2 
                        WHERE t1.fond=t2.fond and (pub_date <= '${curDate}' OR pub_date is null))
                        UPDATE archivefolder t1 SET published = '*', mustPublish = '*', mustUnpublish = null, pub_change_date = '${curDate}' 
                        FROM opis_info t2 WHERE t1.opis_id=t2.id and (pub_date <= '${curDate}' OR pub_date is null) RETURNING t1.id;`;

                        const sqlToUnpublish = `UPDATE archivefolder SET published = null, mustUnpublish = '*', mustPublish = null, pub_change_date = '${curDate}' 
                        WHERE opis_id IN (SELECT id FROM opis WHERE fond IN (SELECT fond FROM FOND WHERE id=$1)) RETURNING id;`;

                        const sql = isActionDelete
                            ? sqlToUnpublish
                            : sqlToPublish;
                        return await trans.manyOrNone(sql, [entityId]);
                    }
                    if (entityType === 4) {
                        const sqlToPublish = `UPDATE archivefolder SET published = '*', mustPublish = '*', mustUnpublish = null, pub_change_date = '${curDate}' WHERE opis_id = $1 and (pub_date <= '${curDate}' OR pub_date is null) RETURNING id;`;

                        const sqlToUnpublish = `UPDATE archivefolder SET published = null, mustUnpublish = '*', mustPublish = null, pub_change_date = '${curDate}' WHERE opis_id = $1 RETURNING id;`;

                        const sql = isActionDelete
                            ? sqlToUnpublish
                            : sqlToPublish;
                        return await trans.manyOrNone(sql, [entityId]);
                    }
                };

                const updateSoursFields = async () => {
                    if (entityType === 3) {
                        const sqlToPublish = `WITH fond_info as (SELECT fond FROM FOND WHERE id=$1),
                        opis_info as (SELECT t1.id, t1.fond, t1.opis_name FROM OPIS t1, fond_info t2 
                                      WHERE t1.fond=t2.fond AND (pub_date <= '${curDate}' OR pub_date is null)),
                        arch_info as (SELECT t2.fond, t2.opis_name, t1.delo, t1.delo_extra FROM archivefolder t1, opis_info t2 
                                      WHERE t1.opis_id=t2.id AND (pub_date <= '${curDate}' OR pub_date is null))
                        UPDATE sour_t t1 SET published = '*', mustPublish = '*', mustUnpublish = null, pub_change_date = '${curDate}' FROM arch_info t2 
                        WHERE t2.fond=t1.fund AND t2.opis_name=t1.list_no AND t2.delo=t1.file_no AND t2.delo_extra=t1.file_no_ext RETURNING id;`;

                        const sqlToUnpublish = `UPDATE sour_t SET published = null, mustUnpublish = '*', mustPublish = null, pub_change_date = '${curDate}' WHERE fund in (SELECT fond FROM FOND WHERE id=$1) RETURNING id;`;

                        const sql = isActionDelete
                            ? sqlToUnpublish
                            : sqlToPublish;
                        return trans.manyOrNone(sql, [entityId]);
                    }
                    if (entityType === 4) {
                        const sqlToPublish = `WITH opis_info AS (SELECT id, fond, opis_name FROM opis WHERE id=$1),
                        arch_info AS (SELECT t2.fond, t2.opis_name, t1.delo, t1.delo_extra FROM archivefolder t1, opis_info t2 
                                     WHERE t1.opis_id=t2.id and (pub_date <= '${curDate}' OR pub_date is null))
                        UPDATE sour_t t1 SET published = '*', mustPublish = '*', mustUnpublish = null, pub_change_date = '${curDate}' FROM arch_info t2 
                        WHERE t2.fond=t1.fund AND t2.opis_name=t1.list_no AND t2.delo=t1.file_no AND t2.delo_extra=t1.file_no_ext RETURNING id;`;

                        const sqlToUnpublish = `WITH opis_info as (select fond, opis_name from opis where id=$1)
                        UPDATE sour_t t1 SET published = null, mustUnpublish = '*', mustPublish = null, pub_change_date = '${curDate}' FROM opis_info t2 WHERE t1.fund=t2.fond and t1.list_no=t2.opis_name RETURNING id;`;
                        const sql = isActionDelete
                            ? sqlToUnpublish
                            : sqlToPublish;
                        return await trans.manyOrNone(sql, [entityId]);
                    }
                    if (entityType === 2) {
                        const sqlToPublish = `WITH delo_info AS (SELECT opis_id, delo, delo_extra FROM archivefolder WHERE id=$1),
                        opis_info AS (SELECT t1.fond, t1.opis_name, t2.delo, t2.delo_extra FROM opis t1, delo_info t2 WHERE t1.id=t2.opis_id)
                        UPDATE sour_t t1 SET published = '*', mustPublish = '*', mustUnpublish = null, pub_change_date = '${curDate}' FROM opis_info t2 WHERE t1.fund=t2.fond AND t1.list_no=t2.opis_name 
                        AND t1.file_no=t2.delo AND t1.file_no_ext=t2.delo_extra RETURNING id;`;
                        const sqlToUnpublish = `WITH delo_info AS (SELECT opis_id, delo FROM archivefolder WHERE id=$1),
                        opis_info AS (SELECT t1.fond, t1.opis_name, t2.delo FROM opis t1, delo_info t2 WHERE t1.id=t2.opis_id)
                        UPDATE sour_t t1 SET published = null, mustUnpublish = '*', mustPublish = null, pub_change_date = '${curDate}' FROM opis_info t2 WHERE t1.fund=t2.fond AND t1.list_no=t2.opis_name AND t1.file_no=t2.delo RETURNING id;`;
                        const sql = isActionDelete
                            ? sqlToUnpublish
                            : sqlToPublish;
                        return await trans.manyOrNone(sql, [entityId]);
                    }
                };
                const updatedOpises = await updateOpisFields();
                const updatedArchivefolders = await updateArchivesFields();
                const updatedSours = await updateSoursFields();

                return {
                    numberOfUpdatedOpises: updatedOpises
                        ? updatedOpises.length
                        : 0,
                    numberOfUpdatedArchivefolders: updatedArchivefolders
                        ? updatedArchivefolders.length
                        : 0,
                    numberOfUpdatedSours: updatedSours
                        ? updatedSours.length
                        : 0,
                };
            }
        } catch (err) {
            console.dir(err.message);
            throw { code: err.code, message: err.message };
        }
    }

    async insert() {
        const isActionDelete = !!this.action_delete;
        const numberOfUpdateChildren = await this.updateChildrenPublishedFields(
            this.entity_type,
            isActionDelete,
            this.entity_id,
        );

        if (this.legacyId && this.action_delete) {
            const dpe = new DELETE_PUBLIC_ENTITY();
            dpe[`"legacyId"`] = this.legacyId;

            await dpe.insert();
        }
        return {
            ...numberOfUpdateChildren,
            isActionDelete: isActionDelete ? '*' : null,
            isActionUpdate: isActionDelete ? null : '*',
        };
    }
}

module.exports = PublicSiteActions;
