var cntc = require('../db').connection;
var ProtoModel = require('../slib/protomodel');
var PERSONAL_T = require('../models/personal_t');

class GenerateDescription extends ProtoModel {
    constructor() {
        super();          
        this.addInputField('personal_code'); // переменная класса
        this.addInputField('fond');
        this.addInputField('opis_name');
        this.addInputField('delo');
        this.addInputField('delo_extra');
    }   
   
    async putGuard () {   
        return {gres: false, text: 'Изменение запрещено.'};
    }
    
    async deleteGuard () {         
        return {gres: false, text: 'Удаление запрещено.'};
    }    

    async getAllGuard () { 
        return {gres: false, text: 'Чтение запрещено.'};
    }


    async insert() {
        if (!this.personal_code|| !this.delo || !this.fond || !this.opis_name) {
            throw {code: 409, message: 'Не заданы параметры для генерации.'};
        }
        try {
            let Code = new PERSONAL_T();
            Code.code = +this.personal_code;
            let Code_data= await Code.selectOneByIdJSON();
            if (Code_data === null) {
                throw {code: 409, message: 'Персоналия не найдена.'};
            }
            if ((Code_data.fund==null)|| (Code_data.list_no==null)||(Code_data.file_no==null)){
                throw {code: 409, message: 'Не заполнены нужные поля : фонд, опись и дело.'};
            }
        }
        catch(err) {
            console.dir(err.message);
            throw {code: err.code, message: err.message};

        }
        //    СОЗДАТЬ ЗАПРОС И ЗАПОЛНИТЬ VALUES на месте нижеследующего комментария
        
        let selstr=` WITH 
        sour_types as (
            select t1.sour_id, string_agg(t2.name_ent,';') as sour_type from sour_type_t t1
            left outer join list_of_t t2
                on t2.id = t1.sour_type_id
            group by t1.sour_id
        ),
        sour_lines as (
            SELECT t4.personal_code, string_agg(COALESCE(t1.sour_name,'-') 
            || '(' || COALESCE(t2.sour_type,'-') ||')', '; ' ORDER BY t1.sour_no,t1.fund, t1.list_no, t1.file_no) sour_str
            FROM public.sour_t t1
            inner join personal_sour_t t4 on t1.id=t4.sour_id
            left outer join sour_types t2
                on t1.id=t2.sour_id
            where (t4.personal_code = $1) and (t1.mark_mem='*') and (t1.fund=$2) and( t1.list_no=$3) and (t1.file_no=$4) and (t1.file_no_ext=$5)
            group by t4.personal_code
        ),

        sour_lines_shifr as (
            SELECT t1.*, t2.file_no, t2.file_no_ext, t3.id as opis_id FROM sour_lines t1
            LEFT OUTER JOIN PERSONAL_T t2
                ON t2.code=t1.personal_code
            LEFT OUTER JOIN Opis t3
                ON t3.fond = $2 and t3.opis_name = $3
        )
        UPDATE archivefolder t1
        SET description = case (length(t2.sour_str)>4985) when true then 
        concat(substring(substring(t2.sour_str from 1 for 4985) from 1 
        for (length(substring(t2.sour_str from 1 for 4985))-position(';' in reverse(substring(t2.sour_str from 1 for 4985))))),'... и другие')
        else t2.sour_str
        end
        FROM sour_lines_shifr t2
        WHERE t1.delo=$4 and t1.delo_extra=$5  and t1.opis_id=t2.opis_id
        RETURNING t1.id, t1.description`;
        try {

            let values=[+this.personal_code, +this.fond, +this.opis_name, +this.delo, this.delo_extra];
            let data = await cntc.tx(async t => {
                return await t.any(selstr,values);
            });
            return data;
        }
        catch(err) {
            console.dir(err.message);
            throw {code: err.code, message: err.message};

        }       
    }    
}
module.exports = GenerateDescription;
