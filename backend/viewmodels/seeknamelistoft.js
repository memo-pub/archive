var cntc = require('../db').connection;
var ProtoModel = require('../slib/protomodel');
var pgp = require('../db').pgp;
var FORUPDATE=
{
    'SPHERE':[{'TABL':'ACT_SPHERE_T','REF': 'sphere_id', group: 'act_id'}],
    'HELP_TYP':[{'TABL':'HELP_T','REF': 'help_typ_id'}],
    'IMPRIS_TYPE':[{'TABL':'IMPRIS_T','REF': 'impris_type_id'}],
    'PROTEST':[{'TABL':'IMPRIS_T','REF': 'protest_id'}],
    'REASON':[{'TABL':'IMPRIS_T','REF': 'reason_id'}],
    'ORG_CODE':[{'TABL':'ORG_T','REF': 'org_code_id'}],
    'PARTICIP':[{'TABL':'ORG_T','REF': 'particip_id'}],
    'ORIGIN':[{'TABL':'PERSONAL_T','REF': 'origin_id'}],
    'EDUC':[{TABL:'PERSONAL_T','REF': 'educ_id'}],
    'SPEC':[{'TABL':'PERSONAL_T','REF': 'spec_id'}], 
    'REHAB_REAS':[{'TABL':'REHABIL_REASON_T','REF': 'reason_id', group: 'rep_id'}],
    'REPRESS_TYPE':[{'TABL':'REPRESS_T','REF': 'repress_type_id'}],
    'SOUR_TYPE':[{'TABL':'SOUR_TYPE_T','REF': 'sour_type_id', group: 'sour_id'}],   
};
class SeekNameListoft extends ProtoModel {
    constructor() {
        super();          
        this.addInputField('stay_id'); // addField лучше не использовать. я его использовал раньше, пока не было addInputField
        this.addInputField('del_id');
    }   
    async putGuard () {   
        return {gres: false, text: 'Изменение запрещено.'};
    }
    
    async postGuard () { 
        if ((this.stay_id===undefined) || (this.del_id===undefined)) {
            return {gres: false, text: 'Не задан один или оба ID из LIST_OF_T', code: 409};
        }  
        return {gres: true, text: 'Ok'};
    }

    async deleteGuard () {         
        return {gres: false, text: 'Удаление запрещено.'};
    }    

    async getAllGuard () { 
        
        return {gres: false, text: 'Чтение запрещено'};
    }

    async postSelectScript() {

        let values = [this.stay_id,this.del_id];
        let result = 'select h.name_f,t1.name_f as name,t1.id FROM LIST_OF_T h join LIST_OF_T t1 on t1.id= $2 and t1.name_f=h.name_f WHERE h.ID =  $1 AND h.NAME_F=t1.name_f and t1.ID =  $2';     
        return {script: result, values: values};        
    }

    async postupdScript() {

        let values = [this.stay_id,this.del_id];
        let result='';      
        return {script: result, values: values};        
    }
    
    async postdelScript() {
        let values = [this.del_id];
        let result = 'delete FROM LIST_OF_T WHERE ID=$1';      
        return {script: result, values: values};        
    }
    
    async insert() {                   
        let selscrpt =await this.postSelectScript();        
        let script =selscrpt.script;
        let updscrpt =await this.postupdScript();        
        let updscript =updscrpt.script;
        let delscrpt =await this.postdelScript();        
        let delscript =delscrpt.script;
        //this.debugSQL = true;                   
        try {
            let data = await cntc.tx( async t => {
                //   let hh=await t.oneOrNone(script,selscrpt.values,val=>{return val===null?null:[val.name,val.name_f];});
                if (this.debugSQL === true) {
                    console.log(pgp.as.format(script,selscrpt.values));}
                let hh=await t.oneOrNone(script,selscrpt.values);
                //name,name_f - поля из запроса которые надо вернуть как результат
                //  можно по другому  await t.oneOrNone(script,selscrpt.values);
                // но в фигурных скобках можно писать выражение для обработки результата 
                if (hh==null) return false ;
                for (let d of FORUPDATE[hh.name_f]) {
                    if (d.group!==undefined) {
                        let del_sql =`
                        delete from ${d['TABL']} where id in (
                        select t1.id from ${d['TABL']} t1
                        left outer join ${d['TABL']} t2
                            on t1.${d.group}=t2.${d.group} and t2.${d['REF']}=$1	
                        where t1.${d['REF']} = $2 and t2.id is not null);
                        `;
                        await t.oneOrNone(del_sql,updscrpt.values);
                    }
                    updscript='UPDATE '+d['TABL']+' SET '+d['REF']+'=$1 WHERE '+d['REF']+'=$2;'; 
  
                    await t.oneOrNone(updscript,updscrpt.values);
                }
                await t.oneOrNone(delscript,delscrpt.values);
                return true;                
            });             
            return data;
        }    
        catch(err) {                    
            throw {code: err.code, message: err.message};         
        }               
    }
}


module.exports = SeekNameListoft;