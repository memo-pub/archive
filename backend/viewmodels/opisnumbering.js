var cntc = require('../db').connection;
var ProtoModel = require('../slib/protomodel');
//var MConfig = require('../models/mconfig');

class OpisNumbering extends ProtoModel {
    constructor() {
        super();          
        this.addInputField('fond'); // переменная класса
        this.addInputField('opis'); // переменная класса
    }   
   
    async putGuard () {   
        return {gres: false, text: 'Изменение запрещено.'};
    }
    
    async deleteGuard () {         
        return {gres: false, text: 'Удаление запрещено.'};
    }    

    async getAllGuard () { 
        return {gres: false, text: 'чтение запрещено.'};
    }
    // в postman вызываю через POST 
    async insert() {
        if ((this.fond === undefined) || (this.opis === undefined) ){
            throw {code: 409, message: 'Не заданы фонд или опись.'};
        }
        // Запрос для проверки
        let selstr1=`SELECT EXISTS (SELECT * FROM personal_t WHERE (fund=$1) and (list_no=$2) 
            and (file_no IS NOT NULL))  as da`;
            
        let selstr10=`select exists(select  from personal_t t0
            inner join personal_sour_t t1 on t0.code=t1.personal_code 
            where t0.fund=$1 and t0.list_no=$2 group by sour_id  
            having count(personal_code)>1) as da`;
                 
            
        // Запрос для изменения
        let selstr2=`UPDATE personal_t m SET file_no = sub.rn FROM  (SELECT code, row_number() OVER (ORDER BY surname,fname,lname) AS rn 
            FROM personal_t WHERE (fund=$1) and (list_no=$2)) sub
            WHERE  m.code = sub.code`;

        let selstr3=`UPDATE sour_t m 
                        SET file_no = sub.rn,fund=$1,list_no=$2 
                        FROM  (
                            SELECT t0.file_no as rn,t1.sour_id
                            FROM personal_t t0
                            INNER JOIN personal_sour_t t1 on t0.code= t1.personal_code
                            where t0.fund=$1 and t0.list_no=$2
                        ) sub
                    WHERE   (m.id = sub.sour_id) and 
                            (
                                ((m.fund=$1 ) and (m.list_no=$2)) 
                                or 
                                ((m.fund is null) and (m.list_no is null))
                            ) 
                            and (m.mark_mem='*')`;             

        try {
            let values=[+this.fond,+this.opis];
            let data = await cntc.tx(async t => {   // tx - это транзакция Поэтому я объединил оба запроса в одну транзакцию
                let d = await t.oneOrNone(selstr1,values);
                if (d.da)
                {
                    throw {code: 409, message: 'В описи присутствуют уже пронумерованные персоналии, и нумерация невозможна'};
                    // код ошибки 409 - озночает, что это не ошибка в работе сервера, а просто пользователь задал данные, для которы операция невозможна
                }
                d = await t.oneOrNone(selstr10,values);
                if (d.da)
                {
                    throw {code: 409, message: 'В personal_sour_t присутствуют несколько персоналий ссылающихся на один источник'};
                    // код ошибки 409 - озночает, что это не ошибка в работе сервера, а просто пользователь задал данные, для которы операция невозможна
                }
                let data=await t.oneOrNone(selstr2,values);
                await t.oneOrNone(selstr3,values);
                return data;
            });
            return data;
        }
        catch(err) {
        //    console.dir(err.message);
            throw {code: err.code, message: err.message};
        
        }
    }
}
module.exports = OpisNumbering;