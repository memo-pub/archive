var cntc = require('../db').connection;
var ProtoModel = require('../slib/protomodel');
var fields_list = {
    1: {
        type_get: 1,
        description: 'Общая страница',
        table_name: 'PERSONAL_T',
        fname: 'surname',
        fname_rus: 'фамилия',
        repl: true,
    },
    2: {
        type_get: 1,
        description: 'Общая страница',
        table_name: 'PERSONAL_T',
        fname: 'real_surname_rmk',
        fname_rus: ' комментарий к фамилии',
        repl: true,
    },
    3: {
        type_get: 1,
        description: 'Общая страница',
        table_name: 'PERSONAL_T',
        fname: 'fname',
        fname_rus: 'имя',
        repl: true,
    },
    4: {
        type_get: 1,
        description: 'Общая страница',
        table_name: 'PERSONAL_T',
        fname: 'fname_rmk',
        fname_rus: 'комментарий к имени',
        repl: true,
    },
    5: {
        type_get: 1,
        description: 'Общая страница',
        table_name: 'PERSONAL_T',
        fname: 'birth_rmk',
        fname_rus: 'ком. к дате рождения',
        repl: true,
    },
    660: {
        type_get: 2,
        description: 'Общая страница',
        table_name: 'PERSONAL_T',
        fname: 'birth_place_num',
        fname_rus: 'ссылка на место рождения',
        table_referred_to: 'PLACE_T',
        fname_referred: 'place_num',
        fname_rus_referred_to: 'номер места рождения',
        fname_rus_referred: 'место рождения',
        repl: false,
    },

    6: {
        type_get: 1,
        description: 'Общая страница',
        table_name: 'PERSONAL_T',
        fname: 'birth_place_rmk',
        fname_rus: 'ком. к месту рождения',
        repl: true,
    },
    7: {
        type_get: 1,
        description: 'Общая страница',
        table_name: 'PERSONAL_T',
        fname: 'death_date_rmk',
        fname_rus: 'ком. к дате смерти',
        repl: true,
    },
    //
    880: {
        type_get: 2,
        description: 'Общая страница',
        table_name: 'PERSONAL_T',
        fname: 'death_place_num',
        fname_rus: 'ссылка на место смерти',
        table_referred_to: 'PLACE_T',
        fname_referred: 'place_num',
        fname_rus_referred_to: 'номер места смерти',
        fname_rus_referred: 'место смерти',
        repl: false,
    },
    8: {
        type_get: 1,
        description: 'Общая страница',
        table_name: 'PERSONAL_T',
        fname: 'death_place_rmk',
        fname_rus: 'ком. к месту смерти',
        repl: true,
    },
    9: {
        type_get: 1,
        description: 'Общая страница',
        table_name: 'PERSONAL_T',
        fname: 'citiz',
        fname_rus: 'гражданство',
        repl: true,
    },
    10: {
        type_get: 1,
        description: 'Общая страница',
        table_name: 'PERSONAL_T',
        fname: 'citiz_rmk',
        fname_rus: 'ком. к гражданству',
        repl: true,
    },
    1100: {
        type_get: 3,
        description: 'Общая страница',
        table_name: 'PERSONAL_T',
        fname: 'spec_id',
        fname_rus: 'ссылка на специальность',
        table_referred_to: 'LIST_OF_T',
        fname_referred: 'id',
        fname_rus_referred_to: 'name_ent',
        fname_rus_referred: 'специальность',
        repl: false,
    },
    11: {
        type_get: 1,
        description: 'Общая страница',
        table_name: 'PERSONAL_T',
        fname: 'spec_rmk',
        fname_rus: 'ком. к специальности',
        repl: true,
    },
    1200: {
        type_get: 3,
        description: 'Общая страница',
        table_name: 'PERSONAL_T',
        fname: 'educ_id',
        fname_rus: 'ссылка на образование',
        table_referred_to: 'LIST_OF_T',
        fname_referred: 'id',
        fname_rus_referred_to: 'name_ent',
        fname_rus_referred: 'образование',
        repl: false,
    },
    12: {
        type_get: 1,
        description: 'Общая страница',
        table_name: 'PERSONAL_T',
        fname: 'educ_rmk',
        fname_rus: 'ком. к образованию',
        repl: true,
    },
    1300: {
        type_get: 3,
        description: 'Общая страница',
        table_name: 'PERSONAL_T',
        fname: 'origin_id',
        fname_rus: 'ссылка на происхождение',
        table_referred_to: 'LIST_OF_T',
        fname_referred: 'id',
        fname_rus_referred_to: 'name_ent',
        fname_rus_referred: 'происхождение',
        repl: false,
    },
    13: {
        type_get: 1,
        description: 'Общая страница',
        table_name: 'PERSONAL_T',
        fname: 'origin_rmk',
        fname_rus: 'ком. к происхождению',
        repl: true,
    },
    14: {
        type_get: 2,
        description: 'Общая страница',
        table_name: 'PERSONAL_T',
        fname: 'place_num',
        fname_rus: 'ссылка на место жительства',
        table_referred_to: 'PLACE_T',
        fname_referred: 'place_num',
        fname_rus_referred_to: 'номер места жительства',
        fname_rus_referred: 'место жительства',
        repl: false,
    },
    15: {
        type_get: 1,
        description: 'Общая страница',
        table_name: 'PERSONAL_T',
        fname: 'residence_code_rmk',
        fname_rus: 'ком. к месту жительства',
        repl: true,
    },
    16: {
        type_get: 1,
        description: 'Общая страница',
        table_name: 'PERSONAL_T',
        fname: 'address',
        fname_rus: 'адрес',
        repl: true,
    },
    17: {
        type_get: 1,
        description: 'Общая страница',
        table_name: 'PERSONAL_T',
        fname: 'address_rmk',
        fname_rus: 'ком. к адресу',
        repl: true,
    },

    18: {
        type_get: 1,
        description: 'Общая страница',
        table_name: 'PERSONAL_T',
        fname: 'email',
        fname_rus: 'EMAIL',
        repl: true,
    },
    19: {
        type_get: 1,
        description: 'Общая страница',
        table_name: 'PERSONAL_T',
        fname: 'url',
        fname_rus: 'URL',
        repl: true,
    },
    20: {
        type_get: 1,
        description: 'Общая страница',
        table_name: 'PERSONAL_T',
        fname: 'url2',
        fname_rus: 'URL2',
        repl: true,
    },
    71: {
        type_get: 1,
        description: 'Общая страница',
        table_name: 'PERSONAL_T',
        fname: 'url3',
        fname_rus: 'URL3',
        repl: true,
    },
    21: {
        type_get: 1,
        description: 'Общая страница',
        table_name: 'PERSONAL_T',
        fname: 'surname_rmk',
        fname_rus: 'общий комментарий',
        repl: true,
    },
    22: {
        type_get: 1,
        description: 'Общая страница',
        table_name: 'PERSONAL_T',
        fname: 'notice',
        fname_rus: 'заметки оператора',
        repl: true,
    },
    23: {
        type_get: 1,
        description: 'Учеба и профессиональная деятельность',
        table_name: 'ACTIVITY_T',
        fname: 'ent1_rmk',
        fname_rus: 'организация',
        repl: true,
    },
    /*24: {      
        description:'Учеба и профессиональная деятельность',
        table_name: 'ACTIVITY_T',
        fname: 'ent1_rmk',
        fname_rus: 'организация'
    },*/
    25: {
        type_get: 1,
        description: 'Учеба и профессиональная деятельность',
        table_name: 'ACTIVITY_T',
        fname: 'prof',
        fname_rus: 'профессия',
        repl: true,
    },
    26: {
        type_get: 1,
        description: 'Учеба и профессиональная деятельность',
        table_name: 'ACTIVITY_T',
        fname: 'prof_rmk',
        fname_rus: 'комментарий к профессии',
        repl: true,
    },
    27: {
        type_get: 1,
        description: 'Учеба и профессиональная деятельность',
        table_name: 'ACTIVITY_T',
        fname: 'post',
        fname_rus: 'должность',
        repl: true,
    },
    28: {
        type_get: 1,
        description: 'Учеба и профессиональная деятельность',
        table_name: 'ACTIVITY_T',
        fname: 'post_rmk',
        fname_rus: 'комментарий к должности',
        repl: true,
    },
    29: {
        type_get: 1,
        description: 'Учеба и профессиональная деятельность',
        table_name: 'ACTIVITY_T',
        fname: 'arrival_dat_rmk',
        fname_rus: 'комментарий к дате поступления',
        repl: true,
    },
    30: {
        type_get: 1,
        description: 'Учеба и профессиональная деятельность',
        table_name: 'ACTIVITY_T',
        fname: 'depart_dat_rmk',
        fname_rus: 'комментарий к дате увольнения',
        repl: true,
    },
    // нет места работы
    300: {
        type_get: 2,
        description: 'Учеба и профессиональная деятельность',
        table_name: 'ACTIVITY_T',
        fname: 'place_num',
        fname_rus: 'ссылка на место работы',
        table_referred_to: 'PLACE_T',
        fname_referred: 'place_num',
        fname_rus_referred_to: 'номер места работы',
        fname_rus_referred: 'географическое место работы',
        repl: false,
    },

    31: {
        type_get: 1,
        description: 'Учеба и профессиональная деятельность',
        table_name: 'ACTIVITY_T',
        fname: 'geoplace_code_rmk',
        fname_rus: 'комментарий к месту работы',
        repl: true,
    },
    32: {
        type_get: 1,
        description: 'Репрессии',
        table_name: 'REPRESS_T',
        fname: 'repress_dat_rmk',
        fname_rus: 'комментарий к дате ареста(репрессии)',
        repl: true,
    },
    // нет места жительства
    3300: {
        type_get: 2,
        description: 'Репрессии',
        table_name: 'REPRESS_T',
        fname: 'place_num',
        fname_rus: 'ссылка на место жительства',
        table_referred_to: 'PLACE_T',
        fname_referred: 'place_num',
        fname_rus_referred_to: 'номер места жительства',
        fname_rus_referred: 'место жительства',
        repl: false,
    },
    33: {
        type_get: 1,
        description: 'Репрессии',
        table_name: 'REPRESS_T',
        fname: 'geoplace_code_rmk',
        fname_rus: 'комментарий к месту жительства',
        repl: true,
    },
    34: {
        type_get: 1,
        description: 'Репрессии',
        table_name: 'REPRESS_T',
        fname: 'repress_type_rmk',
        fname_rus: 'комментарий к типу репрессии',
        repl: true,
    },
    // нет комментария к дате реабилитации
    35: {
        type_get: 1,
        description: 'Репрессии',
        table_name: 'REPRESS_T',
        fname: 'reabil_rmk',
        fname_rus: 'комментарий к реабилитации',
        repl: true,
    },
    3600: {
        type_get: 3,
        description: 'Репрессии',
        table_name: 'REHABIL_REASON_T',
        fname: 'reason_id',
        fname_rus: 'ссылка на реабилитационное основание',
        table_referred_to: 'LIST_OF_T',
        fname_referred: 'id',
        fname_rus_referred_to: 'name_ent',
        fname_rus_referred: 'реабилитационное основание',
        repl: false,
    },
    36: {
        type_get: 1,
        description: 'Репрессии',
        table_name: 'REPRESS_T',
        fname: 'rehabil_org',
        fname_rus: 'реабилитирующий орган',
        repl: true,
    },
    37: {
        type_get: 1,
        description: 'Репрессии',
        table_name: 'REPRESS_T',
        fname: 'repress_rmk',
        fname_rus: 'комментарий к репрессии',
        repl: true,
    },
    38: {
        type_get: 1,
        description: 'Следствия и суды',
        table_name: 'COURT_T',
        fname: 'inq_name',
        fname_rus: 'Следственный орган',
        repl: true,
    },
    39: {
        type_get: 1,
        description: 'Следствия и суды',
        table_name: 'COURT_T',
        fname: 'inq_name_rmk',
        fname_rus: 'комментарий к следственному органу',
        repl: true,
    },
    40: {
        type_get: 1,
        description: 'Следствия и суды',
        table_name: 'COURT_T',
        fname: 'inq_date_rmk',
        fname_rus: 'комментарий к дате начала следствия',
        repl: true,
    },
    4100: {
        type_get: 2,
        description: 'Следствия и суды',
        table_name: 'COURT_T',
        fname: 'place_num_i',
        fname_rus: 'ссылка на место следствия',
        table_referred_to: 'PLACE_T',
        fname_referred: 'place_num',
        fname_rus_referred_to: 'номер места сдедствия',
        fname_rus_referred: 'место следствия',
        repl: false,
    },
    41: {
        type_get: 1,
        description: 'Следствия и суды',
        table_name: 'COURT_T',
        fname: 'place_i_rmk',
        fname_rus: 'комментарий к месту следствия',
        repl: true,
    },
    // номер судебного процесса целое число
    /*42: {      
        description:'Следствия и суды',
        table_name: 'COURT_T',
        fname: 'trial_no',
        fname_rus: 'номер судебного процесса' 
    },*/
    43: {
        type_get: 1,
        description: 'Следствия и суды',
        table_name: 'COURT_T',
        fname: 'trial_rmk',
        fname_rus: 'Комментарий к судебному процессу',
        repl: true,
    },
    44: {
        type_get: 1,
        description: 'Следствия и суды',
        table_name: 'COURT_T',
        fname: 'court_name',
        fname_rus: 'Судебный орган',
        repl: true,
    },
    45: {
        type_get: 1,
        description: 'Следствия и суды',
        table_name: 'COURT_T',
        fname: 'court_name_rmk',
        fname_rus: 'Комментарий к судебному органу',
        repl: true,
    },
    46: {
        type_get: 2,
        description: 'Следствия и суды',
        table_name: 'COURT_T',
        fname: 'place_num_j',
        fname_rus: 'ссылка на место суда',
        table_referred_to: 'PLACE_T',
        fname_referred: 'place_num',
        fname_rus_referred_to: 'номер места суда',
        fname_rus_referred: 'место суда',
        repl: false,
    },
    47: {
        type_get: 1,
        description: 'Следствия и суды',
        table_name: 'COURT_T',
        fname: 'place_j_rmk',
        fname_rus: 'Комментарий к месту суда',
        repl: true,
    },
    48: {
        type_get: 1,
        description: 'Следствия и суды',
        table_name: 'COURT_T',
        fname: 'article',
        fname_rus: 'Статья',
        repl: true,
    },
    49: {
        type_get: 1,
        description: 'Следствия и суды',
        table_name: 'COURT_T',
        fname: 'article_rmk',
        fname_rus: 'Комментарий к статье',
        repl: true,
    },
    50: {
        type_get: 1,
        description: 'Следствия и суды',
        table_name: 'COURT_T',
        fname: 'sentense',
        fname_rus: 'Приговор',
        repl: true,
    },
    51: {
        type_get: 1,
        description: 'Следствия и суды',
        table_name: 'COURT_T',
        fname: 'sentense_rmk',
        fname_rus: 'Комментарий к приговору',
        repl: true,
    },
    52: {
        type_get: 1,
        description: 'Следствия и суды',
        table_name: 'COURT_T',
        fname: 'sentense_date_rmk',
        fname_rus: 'Комментарий к дате приговора',
        repl: true,
    },
    // не нашел Комментарий к типу заключения
    //
    5500: {
        type_get: 3,
        description: 'Заключения',
        table_name: 'IMPRIS_T',
        fname: 'impris_type_id',
        fname_rus: 'ссылка на тип заключения',
        table_referred_to: 'LIST_OF_T',
        fname_referred: 'id',
        fname_rus_referred_to: 'name_ent',
        fname_rus_referred: 'тип заключения',
        repl: false,
    },
    53: {
        type_get: 1,
        description: 'Заключения',
        table_name: 'IMPRIS_T',
        fname: 'prison_name',
        fname_rus: 'Название места заключения',
        repl: true,
    },
    54: {
        type_get: 1,
        description: 'Заключения',
        table_name: 'IMPRIS_T',
        fname: 'prison_name_rmk',
        fname_rus: 'Комментарий к названию места заключения',
        repl: true,
    },
    55: {
        type_get: 1,
        description: 'Заключения',
        table_name: 'IMPRIS_T',
        fname: 'arrival_dat_rmk',
        fname_rus: 'Комментарий к дате прибытия',
        repl: true,
    },
    56: {
        type_get: 1,
        description: 'Заключения',
        table_name: 'IMPRIS_T',
        fname: 'work',
        fname_rus: 'Работа в заключении',
        repl: true,
    },
    57: {
        type_get: 1,
        description: 'Заключения',
        table_name: 'IMPRIS_T',
        fname: 'work_rmk',
        fname_rus: 'Комментарий к работе в заключении',
        repl: true,
    },
    5510: {
        type_get: 3,
        description: 'Заключения',
        table_name: 'IMPRIS_T',
        fname: 'protest_id',
        fname_rus: 'ссылка на протест',
        table_referred_to: 'LIST_OF_T',
        fname_referred: 'id',
        fname_rus_referred_to: 'name_ent',
        fname_rus_referred: 'протест',
        repl: false,
    },
    58: {
        type_get: 1,
        description: 'Заключения',
        table_name: 'IMPRIS_T',
        fname: 'protest_rmk',
        fname_rus: 'Комментарий к протесту',
        repl: true,
    },
    5900: {
        type_get: 2,
        description: 'Заключения',
        table_name: 'IMPRIS_T',
        fname: 'place_num',
        fname_rus: 'ссылка на географическое место заключения',
        table_referred_to: 'PLACE_T',
        fname_referred: 'place_num',
        fname_rus_referred_to: 'номер географического места заключения',
        fname_rus_referred: 'географическое место заключения',
        repl: false,
    },
    59: {
        type_get: 1,
        description: 'Заключения',
        table_name: 'IMPRIS_T',
        fname: 'prison_place_rmk',
        fname_rus: 'Комментарий к географическому месту заключения',
        repl: true,
    },
    5520: {
        type_get: 3,
        description: 'Заключения',
        table_name: 'IMPRIS_T',
        fname: 'reason_id',
        fname_rus: 'ссылка на причину убытия',
        table_referred_to: 'LIST_OF_T',
        fname_referred: 'id',
        fname_rus_referred_to: 'name_ent',
        fname_rus_referred: 'причина убытия',
        repl: false,
    },
    60: {
        type_get: 1,
        description: 'Заключения',
        table_name: 'IMPRIS_T',
        fname: 'reason_rmk',
        fname_rus: 'Комментарий к причине убытия',
        repl: true,
    },
    61: {
        type_get: 1,
        description: 'Заключения',
        table_name: 'IMPRIS_T',
        fname: 'leave_dat_rmk',
        fname_rus: 'Комментарий к дате убытия',
        repl: true,
    },
    62: {
        type_get: 1,
        description: 'Участие в общественных организациях',
        table_name: 'ORG_T',
        fname: 'org_name',
        fname_rus: 'Название организации',
        repl: true,
    },
    63: {
        type_get: 1,
        description: 'Участие в общественных организациях',
        table_name: 'ORG_T',
        fname: 'ent_name_rmk',
        fname_rus: 'Комментарий к наименованию организации',
        repl: true,
    },
    64: {
        type_get: 1,
        description: 'Участие в общественных организациях',
        table_name: 'ORG_T',
        fname: 'join_dat_rmk',
        fname_rus: 'Комментарий к дате вступления',
        repl: true,
    },
    65: {
        type_get: 1,
        description: 'Участие в общественных организациях',
        table_name: 'ORG_T',
        fname: 'dismis_dat_rmk',
        fname_rus: 'Комментарий к дате убытия',
        repl: true,
    },
    66: {
        type_get: 1,
        description: 'Участие в общественных организациях',
        table_name: 'ORG_T',
        fname: 'particip_rmk',
        fname_rus: 'Комментарий к участию в деятельности организации',
        repl: true,
    },
    67: {
        type_get: 1,
        description: 'Участие в общественных организациях',
        table_name: 'ORG_T',
        fname: 'com_rmk',
        fname_rus: 'Общий комментарий',
        repl: true,
    },
    68: {
        type_get: 1,
        description: 'Источники',
        table_name: 'SOUR_T',
        fname: 'sour_name',
        fname_rus: 'Название источника',
        repl: true,
    },
    69: {
        type_get: 1,
        description: 'Источники',
        table_name: 'SOUR_T',
        fname: 'place_so',
        fname_rus: 'Местонахождение источника',
        repl: true,
    },
    70: {
        type_get: 1,
        description: 'Источники',
        table_name: 'SOUR_T',
        fname: 'sour_rmk',
        fname_rus: 'Комментарий',
        repl: true,
    },
    72: {
        type_get: 1,
        description: 'Источники',
        table_name: 'SOUR_T',
        fname: 'acs_rmk',
        fname_rus: 'Комментарий к правилам доступа',
        repl: true,
    },
    73: {
        type_get: 1,
        description: 'Источники',
        table_name: 'SOUR_T',
        fname: 'reprod_type',
        fname_rus: 'Тип воспроизведения',
        repl: true,
    },
    74: {
        type_get: 1,
        description: 'Источники',
        table_name: 'SOUR_T',
        fname: 'reprod_rmk',
        fname_rus: 'Комментарий к типу воспроизведения',
        repl: true,
    },
};
class FieldJoin extends ProtoModel {
    constructor() {
        super();
        this.addInputField('id', {
            tp: 'BIGSERIAL PRIMARY KEY',
            visible: true,
        });
        this.addInputField('exist_field', 'abcd');
        this.addInputField('repl_field', 'abcd');
        this._pk = 'id';
    }

    async putGuard() {
        return { gres: true, text: 'ok.' };
    }

    async postGuard() {
        return { gres: false, text: 'Вставка запрещена.' };
    }

    async deleteGuard() {
        return { gres: false, text: 'Удаление запрещено.' };
    }

    async getAllGuard() {
        return { gres: true, text: 'Ok' };
    }

    async selectAllJSON() {
        let res = [];
        let cnt = Object.values(fields_list).reduce(
            (a, c) => ((a[c.description] = (a[c.description] || 0) + 1), a),
            Object.create(null),
        );

        for (let nom_p in fields_list) {
            let obj = {};
            obj.id = +nom_p;
            obj.description = fields_list[nom_p].description
                .trim()
                .toUpperCase();
            obj.field_name = fields_list[nom_p].fname_rus.trim().toUpperCase();
            obj.repl = fields_list[nom_p].repl;
            obj.cnt = cnt[fields_list[nom_p].description];
            obj.table_name = fields_list[nom_p].table_name;
            obj.fname = fields_list[nom_p].fname;
            res.push(obj);
        }

        res = res.sort((a, b) => {
            if (a.cnt > b.cnt) {
                return -1;
            }
            if (a.cnt < b.cnt) {
                return 1;
            }
            if (a.description < b.description) {
                return -1;
            }
            if (a.description > b.description) {
                return 1;
            }
            if (a.field_name < b.field_name) {
                return -1;
            }
            if (a.field_name > b.field_name) {
                return 1;
            }
            return 0;
        });
        res = res.map((val) => {
            return {
                id: val.id,
                section_name: val.description,
                field_name: val.field_name,
                repl: val.repl,
            };
        });
        return res;
    }

    async update() {
        if (!Object.keys(fields_list).includes(this.id)) {
            throw { code: 409, message: 'нет такого ключа в объекте' };
        }
        if (this.exist_field == undefined || this.repl_field == undefined) {
            throw {
                code: 409,
                message: 'недостаточно данных для операции замены',
            };
        }
        if (!fields_list[this.id]['repl'] === true) {
            throw { code: 409, message: 'это поле нельзя изменить' };
        }
        let field = fields_list[this.id]['fname'];
        let table = fields_list[this.id]['table_name'];
        let selscrpt = UpdateScript(
            this.exist_field,
            this.repl_field,
            field,
            table,
        );
        let script = selscrpt.script;
        try {
            let data = await cntc.tx(async (t) => {
                let d1 = await t.any(script);
                return d1;
            });
            return data;
        } catch (err) {
            throw { code: err.code, message: err.message };
        }
    }

    async selectOneByIdJSON() {
        let selscrpt = '';
        let script = '';
        let table_name = fields_list[this.id]['table_name'];
        let fname = fields_list[this.id]['fname'];
        let description = fields_list[this.id]['description'];
        let fname_rus_referred_to =
            fields_list[this.id]['fname_rus_referred_to'];
        let fname_rus_referred = fields_list[this.id]['fname_rus_referred'];
        let fname_referred = fields_list[this.id]['fname_referred'];
        let table_referred_to = fields_list[this.id]['table_referred_to'];
        if (!Object.keys(fields_list).includes(this.id)) {
            throw { code: 409, message: 'нет такого ключа в объекте' };
        }
        description = fields_list[this.id]['description'];
        if (fields_list[this.id]['repl'] === true) {
            selscrpt = getSelectScript(fname, table_name);
            script = selscrpt.script;
        } else {
            if (fields_list[this.id]['type_get'] == 2) {
                selscrpt = getSelectScript2(
                    description,
                    fname_rus_referred,
                    table_name,
                    fname,
                );
                script = selscrpt.script;
            }
            if (fields_list[this.id]['type_get'] == 3) {
                selscrpt = getSelectScript3(
                    fname,
                    fname_rus_referred_to,
                    fname_referred,
                    table_name,
                    table_referred_to,
                );
                script = selscrpt.script;
            }
        }

        if (script == '') {
            throw { code: 409, message: 'запрос не сформирован' };
        }
        try {
            let data = await cntc.tx(async (t) => {
                let d1 = await t.any(script);
                return d1;
            });
            return data;
        } catch (err) {
            throw { code: err.code, message: err.message };
        }
    }
}
var UpdateScript = function (exist_field, repl_field, field, table) {
    let result =
        'update ' +
        table +
        ' set ' +
        field +
        "='" +
        repl_field +
        "' where " +
        field +
        "='" +
        exist_field +
        "'";
    return { script: result, values: 0 };
};
var getSelectScript = function (field, table) {
    let result =
        'select ' +
        field +
        ' as value ,count(' +
        field +
        ') as cnt' +
        ' from ' +
        table +
        ' where ' +
        field +
        ' IS NOT NULL group by ' +
        field +
        ' order by count(' +
        field +
        ') desc';
    return { script: result, values: 0 };
};
var getSelectScript2 = function (
    description,
    fname_rus_referred,
    table_name,
    fname,
) {
    let result =
        'select concat(t4.place_num' +
        ",'.',t4.place_code,'.',t4.place_name) as value ,count(t0." +
        fname +
        ') as cnt from ' +
        table_name +
        ' t0 inner join place_t t4 on t0.' +
        fname +
        '=t4.place_num and' +
        " t4.cur_mark='*'  where t0." +
        fname +
        ' IS NOT NULL group by t0.' +
        fname +
        ',t4.place_num,t4.place_code,t4.place_name order by count(t0.' +
        fname +
        ') desc';
    return { script: result, values: 0 };
};
var getSelectScript3 = function (
    fname,
    fname_rus_referred_to,
    fname_referred,
    table_name,
    table_referred_to,
) {
    let result =
        'select t4.' +
        fname_rus_referred_to +
        ' as value ,count(t0.' +
        fname +
        ') as cnt from ' +
        table_name +
        ' t0 inner join ' +
        table_referred_to +
        ' t4 on t0.' +
        fname +
        '=t4.' +
        fname_referred +
        ' where t0.' +
        fname +
        ' IS NOT NULL group by t0.' +
        fname +
        ',t4.' +
        fname_rus_referred_to +
        ' order by count(t0.' +
        fname +
        ') desc';
    return { script: result, values: 0 };
};

module.exports = FieldJoin;
