var cntc = require('../db').connection;
//var pgp = require('../db').pgp;
var ProtoModel = require('../slib/protomodel');
var masgrup={YYY1:{gruppa:'a',pole:'b'},
    ACTIVITY_T:{gruppa:'personal_code',pole:'act_no',},HELP_T:{gruppa:'personal_code',pole:'help_no', join: 'help_typ_data'},
    IMPRIS_T:{gruppa:'crt_id',pole:'impris_no'},COURT_T:{gruppa:'rep_id',pole:'trial_no'},
    ORG_T:{gruppa:'personal_code',pole:'org_no', join: 'particip_data'},REPRESS_T:{gruppa:'personal_code',pole:'repress_no'},
    SOUR_T:{gruppa:['fund', 'list_no','file_no','file_no_ext'],pole:'sour_no', join: ['filecnt','sour_dan']}, 
    USAGE_T :{gruppa:'archivefolder_id',pole:'us_no'}};

class MoveOrderNom extends ProtoModel {
    constructor() {
        super();          
        this.addInputField('tablename','abcd'); // addField лучше не использовать. я его использовал раньше, пока не было addInputField
        this.addInputField('itemid', 1234);
        this.addInputField('newnom',1234);   
    }   
   
    async putGuard () {   
        return {gres: true, text: 'Изменение запрещено.'};
    }
    
    async deleteGuard () {         
        return {gres: false, text: 'Удаление запрещено.'};
    }    

    async postGuard () {   
        return {gres: true, text: 'ok'};
    }

    async getAllGuard () { 
        
        return {gres: false, text: 'чтение запрещено'};
    }

    async insert(trans = cntc) {
       
        //    проверка входных параметров на NULL
        if ((this.tablename==null)||(this.itemid==null)||(this.newnom==null)){
            throw {code: 409, message: 'Не заполнены нужные поля : имя таблицы, id и новый порядковый номер.'};
        }
        
        let tname=this.tablename.toUpperCase();
        if  (masgrup[tname]===undefined) return {code: 500, text: 'Нет такой таблицы'};
        if  (masgrup[tname].pole===undefined) return {code: 500, text: 'Нет такого поля'};
        if  (masgrup[tname].gruppa===undefined) return {code: 500, text: 'Нет такой группы'};
        let pole=masgrup[tname].pole;
        let gruppa=masgrup[tname].gruppa;
        let join = masgrup[tname].join;

        if (Array.isArray(gruppa) !== true) {
            gruppa = [gruppa];
        }
        
        try {
            let data = await trans.tx(async t => {  // Это ТРАНЗАКЦИЯ в нее добавим все запросы
                let grp_data=await this.setNewPosition(t, this.itemid, this.newnom, tname, pole, gruppa); // Меняем номер строки и возвращаем номер группы
                await this.moveAll(t,tname, pole, grp_data); // Сместить все на 1 символ
                await this.reorderAll(t,tname, pole, grp_data); // Пересортировать группу
                return await this.getGroupData(t,tname, grp_data, join); // Получить новое состояние группы и вернуть его
            }); // Конец Транзакции
            return data;
        }
        catch(err) {
        //        console.dir(err.message);
            throw {code: err.code, message: err.message};
        } 
            
    }   

    async setNewPosition(t, itemid, newnom, tname, pole, gruppa) {
        let selstr=`UPDATE ${tname} SET ${pole} = CASE (${pole}>$1) 
                    WHEN true THEN ($1-0.3) ELSE ($1+0.3) END where id=$2 returning *`;
        let values=[newnom, itemid];
        let changed_row = await t.oneOrNone(selstr,values);  // Заменил any на oneOrNone, поскольку у нас возможно максимум одно значение в этом запросе
        if (changed_row==null) { // Проверка, если мы не нашли строку, которую хотели переместить
            throw {code: 409, message: 'Строка не найдена.'};
        }
        let res = [];
        for (let g of gruppa) {
            res.push({key:g,
                value:changed_row[g]});
        }
        console.dir(res);
        return res;
    }

    async moveAll(t, tname, pole, grp_data) {
        let wr = [];
        let values = [];
        let i = 1;
        for (let gd of grp_data) {
            wr.push (gd.key+' is not distinct from $'+i);
            values.push(gd.value);
            i++;
        }
        let selstr=`UPDATE ${tname} SET  ${pole}=( ${pole}+0.1) where ` + wr.join(' and ');
        await t.none(selstr,values); // Поставил none вместо any, поскольку запрос на возвращает строк
    }

    async reorderAll(t, tname, pole, grp_data) {
        let wr = [];
        let values = [];
        let i = 1;
        for (let gd of grp_data) {
            wr.push (gd.key+' is not distinct from $'+i);
            values.push(gd.value);
            i++;
        }
        let wr_str = wr.join(' and ');
        let gr_keys = grp_data.map(val => val.key);
        let gr_keys_str = gr_keys.join(', ');
        let gr_keys_rules = [];
        for (let k of gr_keys) {
            gr_keys_rules.push('m.'+k+' is not distinct from sub.'+k);
        }
        let gr_keys_rules_str = gr_keys_rules.join(' and ');

        let selstr1=`UPDATE ${tname} m  SET ${pole} = sub.rn FROM (SELECT id,${gr_keys_str},${pole}, row_number() 
                     OVER (PARTITION BY ${gr_keys_str} order by ${gr_keys_str},${pole}) AS rn FROM ${tname} WHERE (${wr_str}) 
                     and (${pole} IS NOT NULL)) sub 
                     WHERE  ${gr_keys_rules_str} and m.${pole}=sub.${pole}`;
        await t.none(selstr1,values); // Поставил none вместо any, поскольку запрос на возвращает строк
    }

    async getGroupData(t, tname, grp_data, join) {
        let tableClass = require('../models/'+tname.toLowerCase()); // Нахожу класс, который соответствует таблице
        let tableObject = new tableClass();
        for(let g of grp_data) {
            tableObject[g.key] = g.value; // Устанавливаю фильтр по группе
        }
        if (join !== undefined) {
            if (Array.isArray(join)!==true) {
                join = [join];
            }
            for (let j of join) {
                tableObject[j] = true;
            }
        }
        tableObject._orderby='bypos';
        return await tableObject.selectAllJSON(0,t); // Возвращаю новое состояние группы
    }
}
module.exports = MoveOrderNom;