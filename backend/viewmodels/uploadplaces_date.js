var cntc = require('../db').connection;
var pgp = require('../db').pgp; // Подключить pgp
var Model = require('../slib/model');
const XLSX = require('xlsx');
var config = require('../config');
var fsSync = require('fs');
var path = require('path');

class UpLoadPlaces_Date extends Model {
    constructor() {
        super();
        this.addField('fileexcel', { tp: 'JSON', visible: true, required: true, isfile: true });
        //   this.fileexcel содержит данные в формате 'JSON' - {file:'oooo',filename:'1.js',size:36}
        //   {file:'имя файла где хранится принятый файл',filename:'исходное имя переданного файла',size:размер файла}
    }

    fdel(oldel) {
        fsSync.unlink(oldel, (err) => {
            if (err) {
                console.error(err);
            }
        });
    }

    /**
     * Функция проверки прав доступа пользователя к данным. Доступ дается только администраторам.
     * @param {Object} user - пользователь, доступ которого нужно проверить
     */
    async testAccess(user) {
        this.oper_user = user;
        return { gres: true, text: 'Ok' };
    }

    // добвить FALSE для всех async кроме insert (работает по команде POST посланной из POSTMAN)

    async putGuard() {
        return { gres: false, text: 'Изменение запрещено.' };
    }

    async deleteGuard() {
        return { gres: false, text: 'Удаление запрещено.' };
    }

    async getAllGuard() {
        return { gres: false, text: 'Чтение запрещено.' };
    }

    async insert(trans = cntc) {
        // !! ?? отыскать id и дату файла

        // загрузить файл в EXCEL
        let { uploadDir } = config;

        const filePath = path.join(uploadDir, this.fileexcel.file);

        var wb = XLSX.readFile(filePath);

        // удалить файл с диска в папке описаной  в CONFIG.JS
        this.fdel(filePath);
        var first_sheet_name = wb.SheetNames[0];
        var worksheet = wb.Sheets[first_sheet_name];

        var i = 3;
        var address_of_cellA = 'A' + i;
        var address_of_cellB = 'B' + i;
        var address_of_cellC = 'C' + i;

        try {
            await trans.tx(async (t) => {
                // tx - это транзакция
                var update_data = [];
                let imtabel = worksheet['A1'].v;
                let comparewhere = worksheet['A2'].v;
                let columnb = worksheet['B2'].v;
                let columnc = worksheet['C2'].v;
                while (worksheet[address_of_cellA] !== undefined) {
                    let fobj = {}; //очередной объект создается при каждом проходе цикла
                    fobj[comparewhere] = worksheet[address_of_cellA].v;
                    fobj[columnb] = worksheet[address_of_cellB].v;
                    fobj[columnc] = worksheet[address_of_cellC].v;
                    update_data.push(fobj); // Добавляем в массив адрес(ссылка на) очередного FOBJ
                    ++i;
                    address_of_cellA = 'A' + i;
                    address_of_cellB = 'B' + i;
                    address_of_cellC = 'C' + i;
                }

                if (update_data.length > 0) {
                    const cs = new pgp.helpers.ColumnSet(
                        [
                            { name: comparewhere, cnd: true, cast: 'bigint' },
                            { name: columnb, cast: 'date' },
                            { name: columnc, cast: 'date' },
                        ],
                        { table: imtabel }
                    ); // Описали пол таблицу
                    const scl =
                        pgp.helpers.update(update_data, cs) + ' WHERE v.' + comparewhere + '= t.' + comparewhere;
                    await t.none(scl);
                }
            });
            return { message: 'Данные добавлены!' };
        } catch (err) {
            console.dir(err.message);
            throw { code: err.code, message: err.message };
        }
    }
}

module.exports = UpLoadPlaces_Date;
