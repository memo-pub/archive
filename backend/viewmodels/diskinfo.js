const util = require('node:util');
const dfSync = require('node-df');
const ProtoModel = require('../slib/protomodel');
const config = require('../config');

const df = util.promisify(dfSync);

const options = {
    file: 'C:/UPLOAD',
    prefixMultiplier: 'MB',
    isDisplayPrefixMultiplier: true,
    precision: 2,
};

class Diskinfo extends ProtoModel {
    constructor() {
        super();
    }

    async putGuard() {
        return { gres: false, text: 'Изменение запрещено.' };
    }

    async postGuard() {
        return { gres: false, text: 'Вставка запрещена.' };
    }

    async deleteGuard() {
        return { gres: false, text: 'Удаление запрещено.' };
    }

    async getAllGuard() {
        return { gres: true, text: 'Ok' };
    }

    async selectAllJSON() {
        let storageFolders = [];
        if (config.uploadDirList) {
            storageFolders = Object.values(config.uploadDirList);
        } else {
            storageFolders.push(config.uploadDir);
        }
        const promises = storageFolders.map(async (currentDir) => {
            options.file = currentDir;

            try {
                const yyy = await df(options);
                yyy[0].mount = currentDir;
                return yyy[0];
            } catch (error) {
                const yyy = { mount: currentDir };
                yyy.error = error;
                return yyy;
            }
        });

        return Promise.all(promises);
    }
}

module.exports = Diskinfo;
