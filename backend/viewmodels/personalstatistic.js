var cntc = require('../db').connection;
var ProtoModel = require('../slib/protomodel');
var MConfig = require('../models/mconfig');

class PersonalStatistic extends ProtoModel {
    constructor() {
        super();          
        this.addField('tm1',{tp: 'TIMESTAMP', required: true});
        this.addField('tm2',{tp: 'TIMESTAMP', required: true});
    }   

    async putGuard () {   
        return {gres: false, text: 'Изменение запрещено.'};
    }
    
    async postGuard () {   
        return {gres: false, text: 'Вставка запрещена.'};
    }

    async deleteGuard () {         
        return {gres: false, text: 'Удаление запрещено.'};
    }    

    async getAllGuard () { 
        return {gres: true, text: 'Ok'};
    }

    async getSelectScript() {

        /////////
        let mc = new MConfig();
        let personal_price, in_personal_price; 
        try {
            mc.key = 'personal_price'; 
            personal_price = await mc.selectOneByIdJSON();
            personal_price = personal_price.value.value; 
            mc.key = 'in_personal_price';   
            in_personal_price = await mc.selectOneByIdJSON();
            in_personal_price = in_personal_price.value.value;            
        }   
        catch (err) {
            return {message: 'Не удалось получить данные настроек.'};
        }

        /////////

        let values = [this.tm1, this.tm2];
        let result = `SELECT w1.*, w1.personal_cnt*${personal_price} + w1.in_personal_cnt*${in_personal_price} as summa FROM (
                        SELECT t1.id, t1.login, t1.name, t1.surname, COUNT(t2.id) FILTER (WHERE t2.subobject_table = 'personal_t') as personal_cnt, 
                            COUNT(t2.id) FILTER (WHERE t2.object_table='personal_t' and t2.subobject_table <> 'personal_t')
                            + COUNT(distinct t2.obj_date) FILTER (WHERE t2.object_table='sour_t')
                            as in_personal_cnt
                        FROM muser t1 
                        LEFT OUTER JOIN (
                            SELECT row_number() OVER(ORDER BY muser_id, action_time::date) as id, muser_id, action_time::date, object_table, object_id, subobject_table, subobject_id,
                                action_time::date::varchar||'_'||object_id as obj_date
                                FROM public.mlog 
                                where date(action_time)>=date($1) and action_time<date($2) + interval '1 day' and action_type in ('NEW', 'SET')
                                GROUP BY muser_id, action_time::date,  object_table, object_id, subobject_table, subobject_id
                        ) t2
                            on t1.id=t2.muser_id
                        WHERE /* t1.usertype=3  and */ t2.id is not null
                        GROUP BY t1.id, t1.login, t1.name, t1.surname ) w1`;

             
        return {script: result, values: values};        
    }
    
    async selectAllJSON() {    

        let selscrpt =await this.getSelectScript();        
        let script = 'select json_agg(winquery) as data from ('+selscrpt.script+') winquery;';                    
        try {
            let data = await cntc.tx( async t => {
                return await t.oneOrNone(script,selscrpt.values,val=>val.data);                
            });            
            return data;
        }    
        catch(err) {                    
            throw {code: err.code, message: err.message};         
        }         
    }
}


module.exports = PersonalStatistic;