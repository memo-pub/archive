var cntc = require('../db').connection;
var ProtoModel = require('../slib/protomodel');

class DeleteSelectionItem extends ProtoModel {
    constructor() {
        super();          
        this.addField('ids',{tp: 'BIGINT', required: true});
    }   

    async putGuard () {   
        return {gres: false, text: 'Изменение запрещено.'};
    }
    
    async postGuard (user) {   
        this.oper_user = user;
        return {gres: true, text: 'Ok'};
    }

    async deleteGuard () {         
        return {gres: false, text: 'Используйте метод POST'};
    }    

    async getAllGuard () { 
        return {gres: false, text: 'Просмотр запрещен.'};
    }

    async insert() {
        if ((this.ids===undefined)||(this.ids===null)) {
            return {code: 409, message:'Не выбраны записи для удаления!'};
        }
        let script = 'DELETE FROM selectionitem ';
        if ((this.oper_user.isadmin !== true)&&(this.oper_user.usertype !== 3)) {
            script = script + ' USING selection WHERE selectionitem.selection_id=selection.id and selection.owner_id=$2 and ';
        }
        else {
            script = script + ' WHERE ';
        }
        if (this.ids.constructor === Array) {
            script = script + 'selectionitem.id IN ($1:csv) ';
        }
        else {
            script = script + 'selectionitem.id =$1 ';
        }
        script = script + 'RETURNING json_build_object(\'id\',selectionitem.id,\'selection_id\',selection_id, \'personal_code\', personal_code)';
        try {
            let data = await cntc.tx( async t => {
                return await t.any(script,[this.ids, this.oper_user.id]);                
            });            
            return data;
        }    
        catch(err) {                    
            throw {code: err.code, message: err.message};         
        }  
    }

    
}


module.exports = DeleteSelectionItem;