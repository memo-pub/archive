var cntc = require('../db').connection;
var ProtoModel = require('../slib/protomodel');
//var MConfig = require('../models/mconfig');

class FieldSearch extends ProtoModel {
    constructor() {
        super();          
        this.addInputField('searchtxt'); // переменная класса
    }   

    async putGuard () {   
        return {gres: false, text: 'Изменение запрещено.'};
    }
    
    async postGuard () {   
        return {gres: false, text: 'Вставка запрещена.'};
    }

    async deleteGuard () {         
        return {gres: false, text: 'Удаление запрещено.'};
    }    

    async getAllGuard () { 
        return {gres: true, text: 'Ok'};
    }

    
    async selectAllJSON() {
        let tables_list = {
            personal_t: {discription: 'Персоналия',   //
                subquery: `(SELECT w1.*, w2.nation as nat_nation, w2.nation_rmk as nat_nation_rmk,
                    w3.place_num as w3_place_num,w3.place_code as w3_place_code,w3.place_name as w3_place_name,
                    w4.place_num as w4_place_num,w4.place_code as w4_place_code,w4.place_name as w4_place_name,
                    w5.name_ent as w5_name_ent,w5.name_rmk as w5_name_rmk,w6.name_ent as w6_name_ent,w6.name_rmk as w6_name_rmk,
                    w7.name_ent as w7_name_ent,w7.name_rmk as w7_name_rmk
                    FROM personal_t w1
                            LEFT OUTER JOIN nat_t w2 ON w2.id=w1.nation_id
                            LEFT OUTER JOIN place_t w3 ON w1.birth_place_num=w3.place_num AND w3.cur_mark='*'
                            LEFT OUTER JOIN place_t w4 ON w1.place_num=w4.place_num AND w4.cur_mark='*'
                            LEFT OUTER JOIN list_of_t w5 ON w1.origin_id=w5.id
                            LEFT OUTER JOIN list_of_t w6 ON w1.educ_id=w6.id
                            LEFT OUTER JOIN list_of_t w7 ON w1.spec_id=w7.id)`,
                fields: {
                    surname: {fname:'Фамилия', filter: 'personal_t_surname'},//
                    real_surname_rmk: {fname:'Фамилия', filter: 'personal_t_surname'},//
                    fname: {fname:'Имя', filter: 'personal_t_fname'},//
                    fname_rmk: {fname:'Имя', filter: 'personal_t_fname'},//
                    lname: {fname:'Отчество', filter: 'personal_t_lname'},//
                    lname_rmk: {fname:'Отчество', filter: 'personal_t_lname'},//
                    //sex:  'Пол',// Это не нужно
                    birth_rmk: {fname:'Комментарий к дате рождения', filter: 'personal_t_birth_rmk'} ,//
                    death_date_rmk: {fname:'Комментарий к дате смерти', filter: 'personal_t_death_date_rmk'},//
                    birth_place_rmk:{fname:'место рождения', filter: 'personal_t_birth_place'},//
                    w3_place_num:{fname:'место рождения', filter: 'personal_t_birth_place'},//
                    w3_place_code:{fname:'место рождения', filter: 'personal_t_birth_place'},//
                    w3_place_name:{fname:'место рождения', filter: 'personal_t_birth_place'},//
                    nation_rmk:{fname:'национальность', filter: 'personal_t_nation'},//
                    nat_nation: {fname:'национальность', filter: 'personal_t_nation'},//
                    nat_nation_rmk: {fname:'национальность', filter: 'personal_t_nation'},//
                    citiz:{fname:'гражданство', filter: 'personal_t_citiz'},//
                    citiz_rmk:{fname:'гражданство', filter: 'personal_t_citiz'},//
                    origin_rmk:{fname:'комментарий к социальному происхождению', filter: 'personal_t_origin_rmk'},//
                    w5_name_ent:{fname:'Социальное происхождение', filter: 'personal_t_origin_str'},// 
                    w5_name_rmk:{fname:'Социальное происхождение', filter: 'personal_t_origin_str'},// 
                    educ_rmk:{fname:'Комментарий к образованию', filter: 'personal_t_educ_rmk'},//
                    w6_name_ent:{fname:'Образование', filter: 'personal_t_educ_str'},// 
                    w6_name_rmk:{fname:'Образование', filter: 'personal_t_educ_str'},// 
                    spec_rmk:{fname:'Комментарий к специальности', filter: 'personal_t_spec_rmk'},//
                    w7_name_ent:{fname:'Специальность', filter: 'personal_t_spec_str'},// 
                    w7_name_rmk:{fname:'Специальность', filter: 'personal_t_spec_str'},// 
                    address:{fname:'Адрес', filter: 'personal_t_address'},//
                    address_rmk:{fname:'Адрес', filter: 'personal_t_address'},//
                    phone:{fname:'Телефон', filter: 'personal_t_phone'},//
                    phone_rmk:{fname:'Телефон', filter: 'personal_t_phone'},//
                    notice:{fname:'Заметки оператора', filter: 'notice'},//
                    surname_rmk:{fname:'Общий комментарий', filter: 'personal_t_commentariy'},//
                    residence_code_rmk:{fname:'место жительства', filter: 'personal_t_place'},//
                    w4_place_num:{fname:'место жительства', filter: 'personal_t_place'},//
                    w4_place_code:{fname:'место жительства', filter: 'personal_t_place'},//
                    w4_place_name:{fname:'место жительства', filter: 'personal_t_place'},//
                    fund:{fname:'фонд', filter: 'personal_t_fund'},//
                    list_no:{fname:'опись', filter: 'personal_t_list_no'},//
                    file_no:{fname:'дело', filter: 'personal_t_file_no'},//
                    url: {fname:'URL', filter: 'personal_t_url'}
                }},
            org_t: {discription: 'Общественная организация', //
                subquery: `(SELECT w1.*,w2.name_ent as w2_name_ent,w2.name_rmk as w2_name_rmk,w3.name_ent as w3_name_ent,w3.name_rmk as w3_name_rmk   
                FROM org_t w1 
                  LEFT OUTER JOIN list_of_t w2 ON w1.org_code_id=w2.id
                  LEFT OUTER JOIN list_of_t w3 ON w1.particip_id=w3.id)`,
                fields: {
                    w2_name_ent:{fname:'тип организации', filter: 'org_t_org_code_str'},//
                    w2_name_rmk:{fname:'тип организации', filter: 'org_t_org_code_str'},//
                    w3_name_ent:{fname:'тип участия', filter: 'org_t_particip_str'},// 
                    w3_name_rmk:{fname:'тип участия', filter: 'org_t_particip_str'},// 
                    org_name:{fname:'Название организации', filter: 'org_t_org_name'},//
                    ent_name_rmk:{fname:'Название организации', filter: 'org_t_org_name'},//
                    particip_rmk:{fname:'Комментарий к участию', filter: 'org_t_particip_rmk'},//
                    com_rmk:{fname:'Общий комментарий', filter: 'org_t_com_rmk'},//
                    join_dat_rmk:{fname:'Комментарий к дате вступления', filter: 'org_t_join_dat_rmk'},//
                    dismis_dat_rmk:{fname:'Комментарий к дате выхода', filter: 'org_t_dismis_dat_rmk'}//    
                }},
            // находим таблицу в AddSelectionItem.js
            // выбираем фильтры кроме дат. для каждого фильтра создаем столько полей в этом тексте сколько
            // их в фильтре.
            activity_t: {discription: 'Профессиональная деятельность', //
                subquery: `(SELECT w1.*,w2.place_num as w2_place_num,w2.place_code as w2_place_code,w2.place_name as w2_place_name,
                            w4.name_ent as sphere_id_ent, w4.name_rmk as sphere_id_rmk
                        FROM activity_t w1 
                        LEFT OUTER JOIN place_t w2 ON w1.place_num=w2.place_num AND w2.cur_mark='*'
                        LEFT OUTER JOIN act_sphere_t w3 ON w1.id=w3.act_id
                        LEFT OUTER JOIN list_of_t w4 ON w4.id=w3.sphere_id)`,
                fields: {
                // например для 
                //      this.addFilter('activity_t_sphere',['t1.sphere', 't1.sphere_rmk']); //Сфера деятельности
                //создаем 
                    sphere_id_ent:{fname:'сфера деятельности', filter: 'activity_t_sphere_str'},// и 
                    sphere_id_rmk:{fname:'сфера деятельности', filter: 'activity_t_sphere_str'},// и 
                    sphere_rmk:{fname:'сфера деятельности (комментарий)', filter: 'activity_t_sphere'},//поля из одного фильтра должны иметь одно русское название 
                    prof:{fname:'профессия', filter: 'activity_t_prof'},//
                    prof_rmk:{fname:'профессия', filter: 'activity_t_prof'},//
                    post:{fname:'должность', filter: 'activity_t_post'},//
                    post_rmk:{fname:'должность', filter: 'activity_t_post'},//
                    arrival_dat_rmk:{fname:'комментарий к дате поступления', filter: 'activity_t_arrival_dat_rmk'},//
                    depart_dat_rmk:{fname:'комментарий к дате увольнения', filter: 'activity_t_depart_dat_rmk'},//
                    ent1_rmk:{fname:'комментарий к организации', filter: 'activity_t_ent1_rmk'},// 
                    geoplace_code_rmk:{fname:'место организации', filter: 'activity_t_place'},//
                    w2_place_num:{fname:'место организации', filter: 'activity_t_place'},//
                    w2_place_code:{fname:'место организации', filter: 'activity_t_place'},//
                    w2_place_name:{fname:'место организации', filter: 'activity_t_place'}//  
                    // поля с датой не берем 
                }},
            repress_t: {discription: 'Репрессии', //
                subquery: `(SELECT w1.*,w2.name_ent as w2_name_ent,w2.name_rmk as w2_name_rmk,w3.name_ent as w3_name_ent,w3.name_rmk as w3_name_rmk,
                    t3.place_num as t3_place_num,t3.place_code as t3_place_code,t3.place_name as t3_place_name
                FROM repress_t w1 
                  LEFT OUTER JOIN list_of_t w2 ON w1.repress_type_id=w2.id
                  LEFT OUTER JOIN rehabil_reason_t w3_0 ON w1.id=w3_0.rep_id
                  LEFT OUTER JOIN list_of_t w3 ON w3_0.reason_id=w3.id
                  LEFT OUTER JOIN place_t t3 ON w1.place_num=t3.place_num)`,
                fields: {
                    repress_no:{fname:'номер репрессии', filter: 'repress_t_repress_no'},// !!! ?
                    repress_type_rmk:{fname:'Комментарий к типу репрессии', filter: 'repress_t_repress_type_rmk'},//
                    w2_name_ent:{fname:'Тип репрессии', filter: 'repress_t_repress_type_str'},// 
                    w2_name_rmk:{fname:'Тип репрессии', filter: 'repress_t_repress_type_str'},// 
                    repress_dat_rmk:{fname:'Комментарий к дате репрессии', filter: 'repress_t_repress_dat_rmk'},//
                    rehabil_org:{fname:'Реабилитирующий орган', filter: 'repress_t_rehabil_org'},
                    w3_name_ent:{fname:'причина реабилитации', filter: 'repress_t_rehabil_reason_str'},// 
                    w3_name_rmk:{fname:'причина реабилитации', filter: 'repress_t_rehabil_reason_str'},// 
                    reabil_rmk:{fname:'Комментарий к реабилитации', filter: 'repress_t_reabil_mark'},//
                    repress_rmk:{fname:'Комментарий к репрессии', filter: 'repress_t_repress_rmk'},//
                    geoplace_code_rmk:{fname:'место репрессии', filter: 'repress_t_place'},// место репрессии
                    t3_place_num:{fname:'место репрессии', filter: 'repress_t_place'},
                    t3_place_code:{fname:'место репрессии', filter: 'repress_t_place'},
                    t3_place_name:{fname:'место репрессии', filter: 'repress_t_place'}
                    //court_mark:'был ли суд', //Это не нужно
                    //reabil_mark:'Была ли реабилитация',// Это не нужно
                }},
            court_t: {discription: 'Следствие и суд',   //
                subquery: `(SELECT w1.*,t3.place_num as t3_place_num,t3.place_code as t3_place_code,t3.place_name as t3_place_name,
                                t4.place_num as t4_place_num,t4.place_code as t4_place_code,t4.place_name as t4_place_name
                            FROM court_t w1 
                            LEFT OUTER JOIN place_t t3 ON w1.place_num_i=t3.place_num
                            LEFT OUTER JOIN place_t t4 ON w1.place_num_j=t4.place_num)`,
                fields: {
                    //trial_no:'Номер судебного процесса',//
                    inq_name:{fname:'Следственный орган', filter: 'court_t_inq_name'},//
                    inq_name_rmk:{fname:'Следственный орган', filter: 'court_t_inq_name'},//
                    inq_date_rmk:{fname:'Комментарий к дате следствия', filter: 'court_t_inq_date_rmk'},//Комментарий к дате следствия
                    court_name:{fname:'Судебный орган', filter: 'court_t_court_name'},//
                    court_name_rmk:{fname:'Судебный орган', filter: 'court_t_court_name'},//
                    sentense_date_rmk:{fname:'Комментарий к дате приговора', filter: 'court_t_sentense_date_rmk'},//
                    article:{fname:'Статья', filter: 'court_t_article'},//
                    article_rmk:{fname:'Статья', filter: 'court_t_article'},//
                    sentense:{fname:'Приговор', filter: 'court_t_sentense'},//
                    sentense_rmk:{fname:'Приговор', filter: 'court_t_sentense'},//
                    trial_rmk:{fname:'Комментарий к судебному процессу', filter: 'court_t_trial_rmk'},//
                    inq_rmk:{fname:'Комментарий к следствию', filter: 'court_t_inq_rmk'},//
                    place_i_rmk:{fname:'место следствия', filter: 'court_t_place_i'},
                    t3_place_num:{fname:'место следствия', filter: 'court_t_place_i'},
                    t3_place_code:{fname:'место следствия', filter: 'court_t_place_i'},
                    t3_place_name:{fname:'место следствия', filter: 'court_t_place_i'},
                    place_j_rmk:{fname:'место суда', filter: 'court_t_place_j'},
                    t4_place_num:{fname:'место суда', filter: 'court_t_place_j'},
                    t4_place_code:{fname:'место суда', filter: 'court_t_place_j'},
                    t4_place_name:{fname:'место суда', filter: 'court_t_place_j'}
                }},
            impris_t: {discription: 'Заключение', //
                subquery: `(SELECT w1.*,w2.name_ent as w2_name_ent,w2.name_rmk as w2_name_rmk,w3.name_ent as w3_name_ent,w3.name_rmk as w3_name_rmk,
                        w4.name_ent as w4_name_ent,w4.name_rmk as w4_name_rmk,
                        t3.place_num as t3_place_num,t3.place_code as t3_place_code,t3.place_name as t3_place_name
                    FROM impris_t w1 
                    LEFT OUTER JOIN list_of_t w2 ON w1.reason_id=w2.id
                    LEFT OUTER JOIN list_of_t w3 ON w1.protest_id=w3.id
                    LEFT OUTER JOIN list_of_t w4 ON w1.impris_type_id=w4.id
                    LEFT OUTER JOIN place_t t3 ON w1.place_num=t3.place_num)`,
                fields: {
                    //impris_no:'Номер заключения',// Это не нужно
                    prison_name:{fname:'Название места заключения', filter: 'impris_t_prison_name'},//
                    prison_name_rmk:{fname:'Название места заключения', filter: 'impris_t_prison_name'},//
                    arrival_dat_rmk:{fname:'Комментарий к дате прибытия', filter: 'impris_t_arrival_dat_rmk'},//
                    leave_dat_rmk:{fname:'Комментарий к дате убытия', filter: 'impris_t_leave_dat_rmk'},//
                    reason_rmk:{fname:'Комментарий к причине убытия', filter: 'impris_t_reason_rmk'},//
                    w2_name_ent:{fname:'Причина убытия', filter: 'impris_t_reason_str'},// 
                    w2_name_rmk:{fname:'Причина убытия', filter: 'impris_t_reason_str'},// 
                    work:{fname:'Работа в заключении', filter: 'impris_t_work'},//
                    work_rmk:{fname:'Работа в заключении', filter: 'impris_t_work'},//
                    protest_rmk:{fname:'Комментарий к протесту', filter: 'impris_t_protest_rmk'},//
                    w3_name_ent:{fname:'Акции протеста в заключении', filter: 'impris_t_protest_str'},// 
                    w3_name_rmk:{fname:'Акции протеста в заключении', filter: 'impris_t_protest_str'},// 
                    w4_name_ent:{fname:'Тип заключения', filter: 'impris_t_impris_type_str'},// 
                    w4_name_rmk:{fname:'Тип заключения', filter: 'impris_t_impris_type_str'},// 
                    prison_place_rmk:{fname:'место заключения', filter: 'impris_t_place'},//
                    t3_place_num:{fname:'место заключения', filter: 'impris_t_place'},//
                    t3_place_code:{fname:'место заключения', filter: 'impris_t_place'},//
                    t3_place_name:{fname:'место заключения', filter: 'impris_t_place'}//
                    //reason:'Причина убытия',
                    // protest:'Акции протеста в заключении',//????
                    //impris_type:'Тип заключения',//???????
                }},
            sour_t: {discription: 'Источники', //
                subquery: `(SELECT w1.*, w2.name_ent as w2_name_ent,w2.name_rmk as w2_name_rmk FROM sour_t w1
                            INNER JOIN sour_type_t w1_stype
                                ON w1.id=w1_stype.sour_id
                            LEFT OUTER JOIN list_of_t w2
                                ON w2.id=w1_stype.sour_type_id)`,
                fields: {
                    sour_name:{fname:'Название источника', filter: 'sour_t_sour_name'},// *
                    sour_rmk:{fname:'Комментарий источника', filter: 'sour_t_sour_rmk'},// *
                    w2_name_ent:{fname:'тип документа', filter: 'sour_t_sour_type_str'},// * !!! тут вообще поле замменено
                    w2_name_rmk:{fname:'тип документа', filter: 'sour_t_sour_type_str'},// * !!! тут вообще поле замменено
                    place_so:{fname:'Местонахождения источника', filter: 'sour_t_place_so'}// *
                    //list_no:'опись'//нужно количество листов  Это не нужно 
                    //sour_mark:'Оригинал или копия', Это не нужно
                    // mark_mem:'Источник хранится в архиве',// Это не нужно
                    //sour_code:'Шифр источника',// Это ну нужно
                }},    
            /*   text_t: {discription: 'Текст', 
                fields: {
                    text_line:'строка текста'
                }}, */
            help_t: {discription: 'Сведения о помощи', //
                subquery: `(SELECT w1.*, w2.name_ent as w2_name_ent,w2.name_rmk as w2_name_rmk FROM help_t w1
                LEFT OUTER JOIN list_of_t w2
                ON w2.id=w1.help_typ_id)`,
                fields: {
                    help_typ_rmk:{fname:'комментарий к типу помощи', filter: 'help_t_help_typ_rmk'},//
                    w2_name_ent:{fname:'тип помощи', filter: 'help_t_help_typ_str'},// 
                    w2_name_rmk:{fname:'тип помощи', filter: 'help_t_help_typ_str'},// 
                    whom:{fname:'кому оказывалась помощь', filter: 'help_t_whom'},//
                    whom_rmk:{fname:'кому оказывалась помощь', filter: 'help_t_whom'}//
                }}   
        };
        let res = [];
        for (let tbl in tables_list) {
            let imp=Object.keys(tables_list[tbl].fields);
            let data=undefined;
            if (tables_list[tbl].subquery!==undefined){
                data=await show_true_false(tables_list[tbl].subquery,imp,this.searchtxt);    
            }
            if (tables_list[tbl].subquery===undefined) 
            {
                data=await show_true_false(tbl,imp,this.searchtxt); 
            }
            let impout=[]; // Объявлять переменный нужно внутри блока {} где они используются
            for (let impol in data) {
                if (data[impol])
                {
                    let fld = impout.find(element => element.fname == tables_list[tbl].fields[impol].fname);
                    if (fld == undefined){
                        impout.push(tables_list[tbl].fields[impol]);
                    }    
                }
            }
            if (impout.length!==0) {
                let element_to_add = res.find(element => element.table === tables_list[tbl].discription); // найти элемент, в котором уже есть такая таблица
                if (element_to_add === undefined) {     // Если элемент массива не был найден
                    res.push({table: tables_list[tbl].discription,fields: impout});
                }
                else {
                    element_to_add.fields=[...new Set(element_to_add.fields.concat(impout))];                
                }      
               
            }
        }
                   
        return res; 
    }
}

//
var show_true_false=async function (tabl,fld,text) {
    var selstr='';
    let str_sele=[];
    let re = /[^]*\([^]*\)[^]*/; // регулярное выражение, которое ищет круглые скобки
    if (re.test(tabl)) { // Если строка-таблица содержит в себе скобки
        selstr = `WITH src_tabl as 
                 ${tabl}
                 `;
        tabl = 'src_tabl';
    }
    if ((tabl !== undefined) && (fld !== undefined) && (text !== undefined)) {
        for (let nm of fld) {
            let sel='EXISTS (SELECT * FROM '+tabl+' WHERE (UPPER('+nm+'::varchar) like UPPER($1))) as '+nm;  
            str_sele.push(sel);           
        }
        selstr = selstr+ ' SELECT ' + str_sele.join(','); 
    } else
    {
        throw {code: 500, message: 'Ошибка. проверьте правильность ввода имени таблицы,массива полей и поисковой строки'};    
    }
    try {
        let values=[text];
        let data = await cntc.tx(async t => {
            return await t.oneOrNone(selstr,values);
        });
        return data;
    }
    catch(err) {
        throw {code: err.code, message: err.message};
    }    
};


module.exports = FieldSearch;