var cntc = require('../db').connection;
var ProtoModel = require('../slib/protomodel');
var pgp = require('../db').pgp;

class DeleteSour_t extends ProtoModel {
    constructor() {
        super();          
        this.addInputField('sour_ids'); // переменная класса
    }   
   
    async putGuard () {   
        return {gres: false, text: 'Изменение запрещено.'};
    }
    
    async deleteGuard () {         
        return {gres: false, text: 'Удаление запрещено.'};
    }    

    async getAllGuard () { 
        return {gres: false, text: 'Чтение запрещено.'};
    }


    async insert() {
        if ((this.sour_ids === undefined) || !Array.isArray(this.sour_ids)) {
            throw {code: 409, message: 'Не заданы id.'};
        }
        
        let selstr=`DELETE FROM SOUR_T
                    WHERE id = ANY($1)
                    RETURNING id`;
        try {
            let data = await cntc.tx(async t => {
                return await t.any(selstr,[this.sour_ids]);
            });
            return data;
        }
        catch(err) {
            console.dir(err.message);
            throw {code: err.code, message: err.message};

        }       
    }    
}
module.exports = DeleteSour_t;