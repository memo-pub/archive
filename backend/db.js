const pgp = require('pg-promise')();
const { connectionString } = require('./config');
const { checkPubDateRun } = require('./helpers/check_pubdate_timeout');

const connection = pgp(connectionString);

connection
    .connect()
    .then((obj) => {
        console.log('Database connected');
        obj.done(); // success, release the connection;
        checkPubDateRun(connection)
    })
    .catch((error) => {
        console.log('ERROR:', error.message || error);
    });

module.exports = {
    connection,
    pgp,
};
