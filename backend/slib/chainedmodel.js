const cntc = require('../db').connection;
const Model = require('../slib/model');
const MLog = require('../models/mlog');

class ChainedModel extends Model {
    constructor() {
        super();
        this.objects_chain = [];
        this.logerModel = MLog;
        // this.objects_chain = [Product, ProductSegment];
    }

    async getMainObjectId(objData, trans = cntc) {
        const sqlPar = this.createChainedSQL();
        const v = objData[sqlPar.lnk_fild];
        const mO = new this.objects_chain[0]();
        const moPk = mO.getPk();
        const data = await trans.tx((t) => {
            return t.oneOrNone(sqlPar.sql, [v], (val) => {
                return val === null ? null : Number(val[moPk])
            })
        });

        return { id: data, table: sqlPar.maintable };
    }

    createChainedSQL() {
        let tables = [];
        let wheres = [];
        let maintable;
        let mainttable_pk;
        let crntClass = this.constructor;

        for (let m of this.objects_chain) {
            tables.push(m.name);

            if (maintable === undefined) {
                maintable = m.name.toLowerCase();
                const m_o = new m();
                mainttable_pk = m_o.getPk();
            } else {
                const c = new crntClass();
                let lnk = c.getLinks().find((value) => {return value.depClass === m});

                if (lnk === undefined) {
                    throw { code: 500, message: 'Не найдена необходимая зависимость.' };
                }

                wheres.push(
                    `(${crntClass.name}.${lnk.refFieldName} = ${m.name}.${lnk.depFieldName})`,
                );
            }

            crntClass = m;
        }

        const cl = new crntClass();
        wheres.push(`(${crntClass.name}.${cl.getPk()} = $1)`);

        const sql =
            `SELECT ${maintable}.${mainttable_pk} FROM ${tables.join(', ')} WHERE ${wheres.join(' AND ')}`;

        const c = new crntClass();
        let lnk = c.getLinks().find((value) => {return value.depClass === this.constructor});

        if (lnk === undefined) {
            throw { code: 500, message: 'Не найдена необходимая зависимость.' };
        }
        return { sql, maintable, lnk_fild: lnk.depFieldName };
    }

    /**
     *
     * @param {Object} data  - объект для сохранения
     * @param {number} action - тип действия 1 - вставка; 2 - обновление; 3 - удаление
     * @param {Object} trans - подключение к БД
     */
    async logState(data, action, trans = cntc) {
        if (this.logerModel === undefined) {
            return;
        }

        try {
            const loger = new this.logerModel();
            loger.muser_id = this.oper_user.id;
            loger.action_type = 'SET';

            if (action === 3) {
                loger.action_type = 'DELETE';
            } else if (action === 1) {
                loger.action_type = 'NEW';
            }

            let main_object = {};

            if (this.objects_chain.length === 0) {
                const pk = this.getPk();
                main_object.pk = pk;
                main_object[pk] = data[pk];
                main_object.table = this.constructor.name.toLowerCase();
            } else {
                main_object = await this.getMainObjectId(data, trans);
                main_object.pk = 'id';
            }

            loger.object_table = main_object.table;
            loger.object_id = main_object[main_object.pk];
            loger.subobject_table = this.constructor.name.toLowerCase();
            loger.subobject_id = data[this.getPk()];

            try {
                await loger.insert(trans);
            } catch (error) {
                if (error.code != 23_505) {
                    throw error;
                }
            }
        } catch (error) {
            console.dir(error);
            throw error;
        }
    }
}

module.exports = ChainedModel;
