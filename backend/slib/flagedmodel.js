const { connection: cntc } = require('../db');
const Model = require('./model');

/**
 * Класс объектов, которые не подлежат удалению из БД
 * @extends Model
 */
class FlagedModel extends Model {
    constructor() {
        super();
        this.addField('flagarchive', { tp: 'INTEGER DEFAULT 0' });
        this.wheres_default = { script: '(t0.flagarchive=$1)', values: [0] };
        this.isFlagedModel = true;
    }

    getDeleteOneScript() {
        const pk = this.getPk();
        const jsonflds = this.getFields(true)
            .map((val) => "'" + val + "'," + val)
            .join(',');

        const result =
            `UPDATE ${this.constructor.name} SET flagarchive=current_date-date('2000-01-01') where ${pk}=$1  RETURNING json_build_object(${jsonflds}) as data;`;

        return { script: result, values: [this[pk]] };
    }

    getDeleteScript() {
        let index = 1;

        if (this.wheres_default !== undefined && this.wheres_default.script) {
            index = this.wheres_default.values.length + 1;
        }

        const wheres = this.getStaticWheres(index);
        const wr = [];
        let vl = [];

        if (this.wheres_default !== undefined && this.wheres_default.script) {
            wr.push(this.wheres_default.script);
            vl = vl.concat(this.wheres_default.values);
        }

        if (wheres.script) {
            wr.push(wheres.script);
            vl = vl.concat(wheres.values);
        }

        const jsonflds = this.getFields(true)
            .map((val) => `'${val}',${val}`)
            .join(',');

        `${this.constructor.name} t0 SET flagarchive=current_date-date('2000-01-01') where ${wr.join(' AND ')} RETURNING json_build_object(${jsonflds}) as data;`;
        return { script: result, values: vl };
    }

    async testAccess(user) {
        this.oper_user = user;

        try {
            if (user.isadmin === true) {
                return { gres: true, text: 'Ok' };
            }
            return { gres: false, text: 'Нет доступа.' };
        } catch (error) {
            return { gres: false, text: error.message };
        }
    }

    async putGuard(user) {
        return this.testAccess(user);
    }

    async postGuard(user) {
        return this.testAccess(user);
    }

    async deleteGuard(user) {
        return this.testAccess(user);
    }

    async getAllGuard(user) {
        this.oper_user = user;
        return { gres: true, text: 'Ok' };
    }

    async getOneGuard(user) {
        return this.getAllGuard(user);
    }

    /**
     * Проверяет есть ли в БД зависымые объекты других классов, зависящие от данного объекта и запрещающие удаление.
     * В случае наличия таких объектов, вызывает исключение.
     * @param {Object} trans - Подключение к БД.
     * @param {Array | string} fieldname - Имя поля, по которому проверяется зависимость, или массив полей.
     * @example
     * await this.checkRestricted(cntc,[this.getPk()]);
     */
    async checkRestricted(trans = cntc, fieldname = undefined) {
        let restarr;

        if (fieldname === undefined) {
            restarr = this.getLinks().filter((value) => value.refType == 'RESTRICT');
        } else if (fieldname.constructor === Array) {
            restarr = this.getLinks().filter(
                (value) => value.refType == 'RESTRICT' && fieldname.includes(value.refFieldName),
            );
        } else {
            restarr = this.getLinks().filter(
                (value) => value.refType == 'RESTRICT' && value.refFieldName == fieldname,
            );
        }

        for (let rs of restarr) {
            const md = new rs.depClass();
            md[rs.depFieldName] = this[rs.refFieldName];
            const data = await md.selectAllexistingJSON(1, trans);

            if (data.rowcount > 0) {
                throw {
                    message: `DELETE or UPDATE is not allowed. Dependent data is existing. In ${rs.depClass.name}.`,
                    code: 500,
                };
            }
        }
    }

    /**
     * Проверяет есть ли в БД зависымые объекты других классов, подлежащие каскадному удалению.
     * @example
     * await this.deleteCascade();
     */
    async deleteCascade(trans = cntc) {
        let restarr;
        restarr = this.getLinks().filter((value) => value.refType == 'CASCADE');

        for (let rs of restarr) {
            const md = new rs.depClass();
            md.oper_user = this.oper_user;
            md[rs.depFieldName] = this[rs.refFieldName];
            const data = await md.selectAllexistingJSON(0, trans);

            if (data !== null) {
                for (let dt of data) {
                    const pk = md.getPk();
                    md[pk] = dt[pk];
                    await md.deleteOne(trans);
                }
            }
            // throw {message: 'CASCADE::: DELETE or UPDATE is not allowed. Dependent data is existing. In '+rs.depClass.name, code: 500};
        }
    }

    /**
     * Проверяет есть ли в БД зависымые объекты других классов, и устанавливает их значения в NULL.
     * @example
     * await this.deleteSetNull();
     */
    async deleteSetNull(trans = cntc) {
        let restarr;
        restarr = this.getLinks().filter((value) => value.refType == 'SET NULL');

        for (let rs of restarr) {
            const md = new rs.depClass();
            md.oper_user = this.oper_user;
            md[rs.depFieldName] = this[rs.refFieldName];
            const data = await md.selectAllexistingJSON(0, trans);

            if (data !== null) {
                for (let dt of data) {
                    const pk = md.getPk();
                    md[pk] = dt[pk];
                    md[rs.depFieldName] = null;
                    await md.update(trans);
                }
            }
            // throw {message: 'CASCADE::: DELETE or UPDATE is not allowed. Dependent data is existing. In '+rs.depClass.name, code: 500};
        }
    }

    async deleteOne(trans = cntc) {
        const { script, values } = this.getDeleteOneScript();

        try {
            const data = await trans.tx(async (t) => {
                await this.checkRestricted(t);
                await this.deleteCascade(t);
                await this.deleteSetNull(t);
                return t.oneOrNone(script, values, (val) => val === null ? null : val.data);
            });

            if (data !== null) {
                await this.logState(data, 3, trans);

                if (this.subscribable === true) {
                    this.sendSubscription(data[this._pk], 'DELETED');
                }
            }
            return data;
        } catch (error) {
            throw { code: error.code, message: error.message };
        }
    }

    /**
     * Добавляет к классу одну ссылку на другой класс.
     * @param {Object} field - Параметры поля.
     */
    addOneLink(field) {
        const md = new field.refClass();

        if (md.isFlagedModel !== true) {
            throw {
                message:
                    'Классам, наследникам FlagedModel, запрещено ссылаться на НЕ наследников FlagedModel. (' +
                    this.constructor.name +
                    ' -> ' +
                    field.refClass.name +
                    ')',
            };
        }

        super.addOneLink(field);
    }
}

module.exports = FlagedModel;
