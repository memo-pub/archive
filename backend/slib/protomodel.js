const fsPromises = require('node:fs/promises');
const path = require('node:path');
const formidable = require('formidable');
const { connection: cntc } = require('../db');
const config = require('../config');
const {
    getFileFolderPath,
    ensureFolderExists,
    makeStorageSafeFilename,
    ensureUniquePathSync,
} = require('../helpers/files');

/**
 * Класс, который является предком и для полноценных моделей (чтение, запись,
 * создание таблиц, удаление) и для вьюшек (классов для сложых запросов)
 */
class ProtoModel {
    /**
     * Создает экземпляр класса
     */
    constructor() {
        /**
         * Список полей с их свойствами.
         * Используется при создании таблиц (только в моделях) и для фильтрации по точному значению.
         * @member {Object}
         */
        this._fields = {};
        /**
         * Список видимых полей. Используется только в полноценных моделях.
         * @member {Array}
         */
        this._visible_fields = [];
        /**
         * Фильтры. Могут использоваться как в моделях, так и во вьюшках.
         * @member {Object}
         */
        this._filters = {};
        /**
         * Фильтры. Могут использоваться как в моделях, так и во вьюшках.
         * Задают свои правила фильтравания.
         * @member {Object}
         */
        this._userfilters = {};
        /**
         * Правила. Могут использоваться как в моделях, так и во вьюшках.
         * @member {Object}
         */
        this._rules = {};
        /**
         * Порядки сортировки. Могут использоваться как в моделях, так и во вьюшках.
         * @member {Object}
         */
        this._orders = {};
        /**
         * Количество строк, возвращаемых при запросе.
         * Может использоваться как в моделях, так и во вьюшках.
         * @member {number}
         */
        this._limit = undefined;
        /**
         * Смещение, используемое при запросе. Может использоваться как в моделях, так и во вьюшках.
         * @member {number}
         */
        this._offset = undefined;
        /**
         * Поля для считывания из запроса, которым нет соответствия в БД.
         * Могут использоваться как в моделях, так и во вьюшках.
         * @member {Object}
         */
        this._inputfields = {};
    }

    /**
     * Добавляет новое поле
     * @param {string} fieldName - имя поля
     * @param {Object} params - объект с параметрами поля
     * {tp: 'SQL - необходимый для создания поля',
     *  visible: boolean (возвращать ли поле при запросе),
     *  required: boolean (обязательное ли поле),
     *  isfile: boolean (используется ли поле для хранения файла)
     * }
     * @example
     * let protoModel = new Protomodel();
     * protoModel.addField('id',{tp: 'BIGSERIAL PRIMARY KEY',visible: true});
     */
    addField(fieldName, params) {
        // Добавить поле
        this._fields[fieldName] = params;

        if (params.visible === true) {
            // Если видимое, то добавить в список видимых
            this._visible_fields.push(fieldName);
        }
    }

    /**
     * Удаляет ранее добавленное поле.
     * В случае отсутсвия поля с заданным именем ничего не делает.
     * @param {string} fname - Имя поля
     * @example
     * let protoModel = new Protomodel();
     * protoModel.deleteField('id');
     */
    deleteField(fname) {
        // this._fields[fname] = undefined;
        delete this._fields[fname];
        let inx = this._visible_fields.indexOf(fname);

        if (inx > -1) {
            this._visible_fields.splice(inx, 1);
        }
    }

    /**
     * Создает именнованный фильтр.
     * @param {string} fname - имя фильтра
     * @param {Array} fields - массив строк с именами полей,
     *                         которые будут проверяться в данном фильтре.
     * @example
     * let protoModel = new Protomodel();
     * protoModel.addFilter('filter_name',['name', 'surname']);
     */
    addFilter(fname, fields) {
        this._filters[fname] = fields;
    }

    /**
     * Создает именнованный пользовательский фильтр.
     * @param {string} fname - имя фильтра
     * @param {string} sql - строка, которая будет добавлена к условиям поиска через AND.
     *                       {#f_value} - значение переменной.
     * @example
     * let protoModel = new Protomodel();
     * protoModel.addUserFilter('filter_name','field_name > {#f_value} AND field_name2 < {#f_value}');
     */
    addUserFilter(fname, sql) {
        this._userfilters[fname] = sql;
    }

    /**
     * Создает правило.
     * @param {string} rname - имя правила
     * @param {string} sql - SQL-строка.
     * @example
     * let protoModel = new Protomodel();
     * protoModel.addFilter('rule1', 'field=555');
     */
    addRule(rname, sql) {
        this._rules[rname] = sql;
    }

    /**
     * Создает Полe для считывания из запроса, котороум нет соответствия в БД.
     * @param {string} fname - имя поля
     * @param {any} example - Пример значения
     * @example
     * let protoModel = new Protomodel();
     * protoModel.addInputField('field_name',123);
     */
    addInputField(fname, example) {
        this._inputfields[fname] = example;
    }

    /**
     * Возвращает список полей, которые являются файлами.
     * @returns {Array} Список полей, являющихся файлами
     * @example
     * let protoModel = new Protomodel();
     * let filefields = protoModel.getFileFields();
     */
    getFileFields() {
        let arr = [];

        for (let f in this._fields) {
            if (this._fields[f].isfile === true) {
                arr.push(f);
            }
        }

        return arr;
    }

    /**
     * Создает именованный порядок сортировки при запросе
     * @param {string} oname - имя порядка сортировки
     * @param {string} ovalue - строка, содержащая фрамент SQL,
     *                          который будет вставлен после 'order by'
     * @example
     * let protoModel = new Protomodel();
     * protoModel.addOrder('byid', 'id desc');
     */
    addOrder(oname, ovalue) {
        // Добавить порядок сортировки
        this._orders[oname] = ovalue;
    }

    /**
     * Возвращает список полей.
     * @param {boolean} visibleOnly - если true, возвращает только поля, которые помечены как видимые.
     * @returns {Array} Список полей
     * @example
     * let protoModel = new Protomodel();
     * let fields = protoModel.getFields(true);
     */
    getFields(visibleOnly) {
        if (visibleOnly) {
            return this._visible_fields;
        }

        return Object.keys(this._fields);
    }

    async formidablePromise(req) {
        let formidableProps = {
            uploadDir: config.uploadDir,
            maxFileSize: config.uploadSizeLimit,
        };

        if (this.selectMaxId) {
            let lastId = await this.selectMaxId();
            let id = Number(lastId) + 1;
            let uploadDir;
            let dirExists;

            let date = new Date();

            if (!Number.isNaN(lastId)) {
                uploadDir = getFileFolderPath({ id, date });
                dirExists = await ensureFolderExists(uploadDir);
            }

            if (dirExists) {
                formidableProps.uploadDir = uploadDir;
            }

            let filenameFn = (name, ext, { originalFilename, mimetype }, form) => {
                let originalFileName = originalFilename;
                let filename = makeStorageSafeFilename({
                    originalFileName,
                    id,
                    fileColumn,
                });

                let proposedFilePath = path.join(formidableProps.uploadDir, filename);
                let uniqueFilePath = ensureUniquePathSync(proposedFilePath);
                let safeFileName = path.basename(uniqueFilePath);

                return safeFileName;
            };

            let [fileColumn] = this.getFileFields && this.getFileFields();

            if (!Number.isNaN(lastId) && fileColumn) {
                formidableProps.filename = filenameFn;
            }
        }

        let form = new formidable.IncomingForm(formidableProps);

        let formidablePromiseResult = await new Promise((resolve, reject) => {
            form.parse(req, (err, fields, files) => {
                if (err) return reject(err);
                resolve({ fields, files });
            });
        });

        return formidablePromiseResult;
    }

    /**
     * Если запрос отправлен методами POST или PUT,
     * и в текущем объекте есть поля-файлы,
     * функция сохраняет эти файлы
     * @param {Object} req - объект request Express
     */

    async parseFormidable(req) {
        if (!(req.method === 'POST' || req.method === 'PUT')) return;

        let hasFileFields = Object.values(this._fields).some((val) => val.isfile);

        if (!hasFileFields) return;

        // Если есть поля-файлы
        let contentTypeHeader = req.get('Content-Type');

        if (contentTypeHeader.includes('multipart/form-data')) {
            let parsedFormidablePromise = await this.formidablePromise(req);

            req.body = Object.assign(req.body, parsedFormidablePromise.fields);
            req.body.fileUploaded = parsedFormidablePromise.files;
        }
    }

    async deleteUnusedUploded(data) {
        if (data && data.fileUploaded) {
            await Promise.all(
                Object.keys(data.fileUploaded).map(async (key) => {
                    if (data.fileUploaded[key].used !== true) {
                        let filePath = data.fileUploaded[key].path;

                        try {
                            await fsPromises.unlink(filePath);
                        } catch (error) {
                            console.log('unlink error', error);
                        }
                    }
                }),
            );
        }
    }

    checkTypes(ftype, tp_array) {
        for (let t of tp_array) {
            if (ftype.toUpperCase().includes(t.toUpperCase())) {
                return true;
            }
        }

        return false;
    }

    /**
     *
     * @param {Object} data - Объект запрос
     * @param {string} f - имя поля
     */
    processRequestField(data, f) {
        let ftype = this._fields[f].tp;

        if (this._fields[f].isfile === true) {
            let currStor = null;

            if (config.uploadDirList !== undefined) {
                currStor = config.currentUploadDir;
            }

            if (data.fileUploaded !== undefined && data.fileUploaded[f] !== undefined) {
                this[f] = {
                    file: data.fileUploaded[f].newFilename,
                    filename: data.fileUploaded[f].originalFilename,
                    size: data.fileUploaded[f].size,
                    storage: currStor,
                };
                data.fileUploaded[f].used = true;
            } else if (data.fileUploaded === undefined && data[f] !== undefined) {
                this[f] = data[f];
            } else {
                this[f] = undefined;
            }
        } else if (this.checkTypes(ftype, ['BOOLEAN'])) {
            if (typeof data[f] === 'string') {
                if (data[f].toUpperCase().trim() === 'TRUE') {
                    this[f] = true;
                } else if (data[f].toUpperCase().trim() === 'FALSE') {
                    this[f] = false;
                } else {
                    this[f] = undefined;
                }
            } else {
                this[f] = data[f];
            }
        } else if (
            this.checkTypes(ftype, [
                'smallint',
                'integer',
                'bigint',
                'decimal',
                'numeric',
                'real',
                'smallserial',
                'serial',
                'bigserial',
            ]) &&
            typeof data[f] === 'string' &&
            data[f].toUpperCase().trim() === 'NULL'
        ) {
            this[f] = null;
        } else {
            this[f] = data[f];
        }
    }

    /**
     * Загружает в модель данные из объекта:
     * 1. значения полей, определеных в модели;
     * 2. значенияименнованных фильтров;
     * 3. имя порядка сортировки.
     * 4. лимит выдачи для SELECT
     * 5. смещение выдачи для SELECT
     * @param {Object} data - объект из которого должна происходить загрузка данных.
     * @example
     * let protoModel = new Protomodel();
     * protoModel.loadFromRequest(req.body);
     */
    loadFromRequest(data) {
        this._limit = Number(data.limit);
        this._offset = Number(data.offset);
        this._orderby = data.orderby;

        for (let f in this._fields) {
            this.processRequestField(data, f);
        }

        for (let f in this._filters) {
            this[f] = data[f];

            if (this[f] !== undefined && typeof this[f] === 'string') {
                this[f] = this[f].toUpperCase();
            }
        }

        for (let f in this._userfilters) {
            this[f] = data[f];
        }

        for (let r in this._rules) {
            this[r] = data[r];
        }

        for (let r in this._inputfields) {
            this[r] = data[r];
        }
    }

    /**
     * Проверяет заполнены ли все обязательные поля.
     * @param {Array} requiredFields - список обязательных полей.
     * @returns {Array} Список незаполненных обязательных полей.
     * @example
     * dataModel.loadFromRequest(req.query);
     * let notfound = dataModel.checkRequiredFields(['login', 'password']);
     * if (notfound.length>0) {
     *     res.status(409).json({error:'Не заданы поля: '+ notfound.join(', ')});
     *     return;
     * }
     */
    checkRequiredFields(requiredFields = []) {
        return requiredFields.filter((f) => !this[f]);
    }

    /**
     * Подготавливает текст условия для вставки в SQL-запрос.
     * @param {string} fname - имя поля
     * @param {string} prefix - псевдоним таблицы
     * @param {number} index - порядковый номер переменной в масиве итогового запроса
     * @example
     * let protoModel = new Protomodel();
     * let surname_rule = protoModel.createStaticWhereText('surname','t0',1);
     */

    createStaticWhereText(fname, prefix, index) {
        if (this[fname] !== null && this[fname].constructor === Array) {
            return `(${prefix}.${fname} IN ($${index}:csv))`;
        }

        if (this[fname] === null) {
            return `(${prefix}.${fname} IS NULL)`;
        }

        return `(${prefix}.${fname} = $${index})`;
    }

    insert() {
        throw { code: 0, message: 'Действие не определено для данной модели.' };
    }

    update() {
        throw { code: 0, message: 'Действие не определено для данной модели.' };
    }

    selectOneByIdJSON() {
        throw { code: 0, message: 'Действие не определено для данной модели.' };
    }

    selectAllJSON() {
        throw { code: 0, message: 'Действие не определено для данной модели.' };
    }

    /**
     * Возвращает название поля, которое используется как ключ для модели.
     * @returns имя ключевого поля модели.
     */
    getPk() {
        return this._pk;
    }

    /**
     * Выполняет SQL-запрос, результатом которого должен быть один JSON объект или null.
     * Предполагается, что объект будет возвразен в колонке data (например: 'json_agg(t1) as data')
     * @param {string} sql - текст SQL-запроса
     * @param {Array} values - Массив параметров SQL-запроса
     * @param {Object} trans - Объект транзакции или подключения
     * @example
     * let protoModel = new Protomodel();
     * let sql = 'select json_agg(t1) from table_name t1;'
     * let data = await protoModel.selectOneAsJSONData(sql,[],cnt);
     */
    async selectOneAsJSONData(sql, values, trans = cntc) {
        try {
            let data = await trans.tx(async (t) => {
                return t.oneOrNone(sql, values, (val) => {
                    return val === null ? null : val.data;
                });
            });
            return data;
        } catch (error) {
            throw { code: error.code, message: error.message };
        }
    }

    /**
     * Выполняет SQL-запрос, результатом которого должен быть массив значений.
     * @param {string} sql - текст SQL-запроса
     * @param {Array} values - Массив параметров SQL-запроса
     * @param {Object} trans - Объект транзакции или подключения
     * @example
     * let protoModel = new Protomodel();
     * let sql = 'select json_agg(t1) from table_name t1;'
     * let data = await protoModel.selectMany(sql,[],cnt);
     */
    async selectMany(sql, values, trans = cntc) {
        try {
            let data = await trans.tx(async (t) => {
                return t.manyOrNone(sql, values);
            });
            return data;
        } catch (error) {
            throw { code: error.code, message: error.message };
        }
    }
}

module.exports = ProtoModel;
