var Model = require('./model');

class LogerModel extends Model{
    constructor () {        
        super();            
        this.addField('original_id',{tp: 'BIGINT',visible: true}); 
        this.addField('flagarchive',{tp: 'INTEGER',visible: true});
        this.addReferenceToClass('changer_airuser_id','BIGINT',AirUser,'id');        
        this.addField('change_date_time',{tp: 'timestamp with time zone DEFAULT current_timestamp',visible: true});
        this.addField('record_deleted_flag',{tp: 'BOOLEAN',visible: true});
    }  

    async putGuard () {  
        return {gres: false, text: 'Редактирование невозможно.'};
    }


    async postGuard () {  
        return {gres: false, text: 'Редактирование невозможно.'};
    }

    async deleteGuard () {
        return {gres: false, text: 'Редактирование невозможно.'};
    }

    async getAllGuard (user) {
        if (user.isadmin===true) {
            return {gres: true, text: 'Ok'};
        }            
        else {
            return {gres: false, text: 'Нет доступа.'};
        }       
    }

    async getOneGuard (user) {        
        return this.getAllGuard (user);        
    }

    addReferenceToClass(fieldname, fieldtype, refClass, reffieldname) {        
        super.addReferenceToClass(fieldname, fieldtype, refClass, reffieldname, '', true, false);                
    }
    
}

module.exports = LogerModel;

var AirUser = require('../models/airuser');