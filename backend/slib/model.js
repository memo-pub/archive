const path = require('node:path');
const fsPromises = require('node:fs/promises');
const { clonedeep } = require('lodash');
const { connection: cntc } = require('../db');
const { WSSList } = require('../wsserver');
const config = require('../config');
const { pgp } = require('../db');
const ProtoModel = require('./protomodel');

const LinkList = {};

/**
 * Класс Модели представления таблицы из базы данных.
 * @extends ProtoModel
 */
class Model extends ProtoModel {
    /**
     * Создает экземпляр класса
     */
    constructor() {
        super();
        /**
         * Список ссылок на дополнительные данные в других таблицах.
         * Используется для состовления секций JOIN в SQL.
         * Каждый объект имеет имя.
         * Например, если добавлен объект _refs['join1_name'], он будет использован
         * при составлении SQL-запроса SELECT, если this.join1_name === true.
         *
         * Каждый элемент является объектом с полями:
         *  table - имя таблицы, которая подлежит присоединению.
         *  rule - SQL правило соединения таблиц.
         *      В правиле:
         *          't0' - имя таблицы модели,
         *         '{#t_self}' - имя текущей присоединяемой таблицы.
         * replace - SQL строка, добавляемая в список возвращаемых полей.
         *          В строке '{#t_self}' - имя текущей присоединяемой таблицы.
         * @member {Object}
         * @example
         * this._refs['join1'] = {
         * table:'table1',
         * rule: '(t0.id={#t_self}.table1_id) AND (t0.IdProduitFamille={#t_self}.internal_id)',
         * replace:'nomfamille as produitfamille_name'
         * }
         */
        this._refs = {};

        /**
         * Список таблиц на которые ссылается текущая таблица.
         * Таблицы являются именами полей, значение - количеством ссылок.
         * В примере ниже указывается, что текущая таблица ссылается на таблицу table1 3 раза.
         * @member {Object}
         * @example
         * this._depends['table1'] = 3;
         */

        this._depends = {};
        this._calcfields = {};
        this.addField('id', { tp: 'BIGSERIAL PRIMARY KEY', visible: true });
        this._pk = 'id';
        this.logerModel = undefined;
        this.showinvisible = false;
    }

    /**
     * Выделяет имя таблицы, от которой зависит текущий SQL-фрагмент
     * @param {string} sqlStr - фрагмент SQL
     * @returns {string} имя таблицы
     */
    extractDepends(sqlStr) {
        let words = sqlStr.split(/\s+|\(/);
        words = words.map((val) => val.toUpperCase());
        let index = words.indexOf('REFERENCES');

        if (index > -1 && words[index + 1] !== undefined) {
            return words[index + 1];
        }
        return '';
    }

    /**
     * Выделяет имя таблицы, от которой зависит текущий SQL-фрагмент, и добавляет его в список зависимостей.
     * @param {string} sqlStr - фрагмент SQL
     * @example
     * let model = new Model();
     * model.addDepends('BIGINT REFERENCES airuser(id) ON DELETE RESTRICT');
     */
    addDepends(className) {
        let dependson = className.name.toUpperCase();

        if (dependson) {
            this._depends[dependson] = this._depends[dependson]
                ? this._depends[dependson] + 1
                : 1;
        }
    }

    /**
     * Выделяет имя таблицы, от которой зависит текущий SQL-фрагмент, и удаляет его из списока зависимостей.
     * @param {string} sqlStr - фрагмент SQL
     * @example
     * let model = new Model();
     * model.deleteDepends('BIGINT REFERENCES airuser(id) ON DELETE RESTRICT');
     */
    deleteDepends(sqlStr) {
        let dependson = this.extractDepends(sqlStr);

        if (dependson) {
            this._depends[dependson] = this._depends[dependson]
                ? this._depends[dependson] - 1
                : undefined;

            if (this._depends[dependson] < 1) {
                this._depends[dependson] = undefined;
            }
        }
    }

    /**
     * Проверяет есть ли в списке existingTables все имена таблиц,
     * на от которых зависит таблица данной модели.
     * @param {Array} existingTables - Список существующих таблиц.
     * @returns true - если existingTables содержит все необходимые таблицы
     *  false - в противном случае.
     */
    checkDepends(existingTables) {
        let ex = [...existingTables];
        ex.push(this.constructor.name.toUpperCase());

        for (let d in this._depends) {
            if (!ex.includes(d)) {
                return false;
            }
        }
        return true;
    }

    getThisTableName() {
        return this.constructor.name;
    }

    /**
     * Добавляет в SQL-скрипт CREATE TABLE дополнительное содержимое.
     * Дополнительное содержимое вставляется через запятую после списка создаваемых полей.
     * Используется только в конструкторе.
     * @param {string} sqlStr - SQL-строка.
     * @example
     * class Subdivision extends Model {
     *     constructor() {
     *         super();
     *         this.addField('name',{tp: 'VARCHAR(100)',visible: true, required: true});
     *         this.addField('code',{tp: 'VARCHAR(20)',visible: true});
     *         this.addextraCreate('UNIQUE (name)');
     *     }
     * }
     *
     * let subdivision = new Subdivision();
     * let script = subdivision.getCreateScript();
     * console.log(script);
     *
     * >>
     * CREATE TABLE Subdivision (name VARCHAR(100), code VARCHAR(20), UNIQUE (name));
     */
    addExtraCreate(sqlStr) {
        if (sqlStr.toUpperCase().includes('REFERENCES')) {
            let obj = {
                message: `ERROR: 'REFERENCES' is not allowed id Model addExtraCreate defenition. Model name: ${this.constructor.name}`,
                code: 500,
            };

            console.dir(obj);
            throw obj;
        }

        this.extraCreate = sqlStr;
    }

    addUniqueIndex(fields, where) {
        if (this.afterCreate === undefined) {
            this.afterCreate = '';
        }

        this.afterCreate = `${this.afterCreate}
            CREATE UNIQUE INDEX ${fields.join(
                '_',
            )}_${this.getThisTableName().toLocaleLowerCase()}
            ON ${this.getThisTableName().toLocaleLowerCase()} (${fields.join(
            ', ',
        )}) `;

        if (where !== undefined) {
            this.afterCreate = this.afterCreate + ' WHERE ' + where;
        }

        this.afterCreate += '; ';
    }

    async getAllGuard(user) {
        this.oper_user = user;
        return { gres: true, text: 'Ok' };
    }

    /**
     * Добавляет новое поле. Вычисляет зависимость поля от других таблиц, и добавляет имя таблицы в цепочку зависимостей.
     * @param {string} fieldName - имя поля
     * @param {Object} params - объект с параметрами поля
     * {tp: 'SQL - необходимый для создания поля',
     *  visible: boolean (возвращать ли поле при запросе),
     *  required: boolean (обязательное ли поле),
     *  isfile: boolean (используется ли поле для хранения файла)
     * }
     * @example
     * let model = new Model();
     * model.addField('id',{tp: 'BIGSERIAL PRIMARY KEY',visible: true});
     */
    addField(fname, params) {
        if (params.tp.toUpperCase().includes('REFERENCES')) {
            let obj = {
                message: `ERROR: 'REFERENCES' is not allowed id Model field defenition. Model name: ${this.constructor.name}`,
                code: 500,
            };

            console.dir(obj);
            throw obj;
        }

        super.addField(fname, params);
    }

    deleteField(fname) {
        this.deleteDepends(this._fields[fname].tp);
        super.deleteField(fname);
    }

    addCalcField(fieldName, expression, ouputName, requiredRefs) {
        if (!expression) {
            throw 'CalField expression is not defined';
        }
        if (!ouputName) {
            throw 'CalField output_name is not defined';
        }
        this._calcfields[fieldName] = {
            expression,
            output_name: ouputName,
            requiredRefs,
        };
    }

    addReference(rname, rvalue) {
        this._refs[rname] = rvalue;
    }

    addReferenceToClass(
        fieldname,
        fieldtype,
        refClass,
        reffieldname,
        ondelete = '',
        visible = true,
        required = false,
    ) {
        let ond = ondelete ? `ON DELETE ${ondelete}` : '';
        let params = {
            tp: `${fieldtype} REFERENCES ${refClass.name}(${reffieldname}) ${ond}`,
            visible,
            required,
            refClass,
            refType: ondelete,
            refFieldName: reffieldname,
            fieldName: fieldname,
        };

        super.addField(fieldname, params);
        this.addDepends(refClass);
        // ondelete: CASCADE, RESTRICT
    }

    getCreateScript() {
        let result =
            'CREATE TABLE IF NOT EXISTS ' + this.getThisTableName() + ' (';
        let fld_array = [];

        for (let f in this._fields) {
            fld_array.push(`${f} ${this._fields[f].tp}`);
        }

        result += fld_array.join(', ');

        if (this.extraCreate) {
            result += `, ${this.extraCreate}`;
        }

        result += ');';

        if (this.afterCreate) {
            result += ` ${this.afterCreate}`;
        }
        return result;
    }

    getInsertScript() {
        let queryInsertPart = `INSERT INTO ${this.getThisTableName()} `;
        let fld_array = [];
        let vl_array = [];
        let vl_data_array = [];
        let reqd = [];
        let i = 1;

        for (let f in this._fields) {
            if (this[f] !== undefined) {
                if (
                    this._fields[f].tp.toUpperCase().trim() === 'TIMESTAMP' &&
                    this[f] === 'current_timestamp'
                ) {
                    fld_array.push(f);
                    vl_array.push('current_timestamp');
                } else {
                    fld_array.push(f);
                    vl_array.push('$' + i);
                    i++;
                    vl_data_array.push(this[f]);
                }
            } else if (this._fields[f].required) {
                reqd.push(f);
            }
        }

        if (i == 1) {
            let code = 500;
            throw {
                code,
                message:
                    'Недопустимо вставлять данные не указав ни одного поля.',
            };
        }

        if (reqd.length > 0) {
            let code = 500;
            throw {
                code,
                message:
                    'Не указаны обязательные поля (' + reqd.join(', ') + ').',
            };
        }

        let jsonflds = this.getFields(true)
            .slice(0, 50)
            .map((val) => "'" + val + "'," + val)
            .join(',');

        let result = `${queryInsertPart} (${fld_array.join(', ')})
                        VALUES (${vl_array.join(', ')})
                        RETURNING json_build_object(${jsonflds}) as data;`;
        return { script: result, values: vl_data_array };
    }

    getUpdateOneScript(wheres, values) {
        let set_array = [];
        let where_arr = [];
        let i = values.length + 1;

        for (let f in this._fields) {
            if (this[f] !== undefined) {
                if (
                    this._fields[f].tp.toUpperCase().trim() === 'TIMESTAMP' &&
                    this[f] === 'current_timestamp'
                ) {
                    set_array.push(`${f}=current_timestamp`);
                    where_arr.push(`${f} IS DISTINCT FROM current_timestamp`);
                } else {
                    set_array.push(`${f}=$${i}`);

                    if (this._fields[f].tp.toUpperCase().trim() === 'JSON') {
                        where_arr.push(
                            `${f}::jsonb IS DISTINCT FROM $${i}::jsonb`,
                        );
                    } else {
                        where_arr.push(`${f} IS DISTINCT FROM $${i}`);
                    }

                    i++;
                    values.push(this[f]);
                }
            }
        }

        wheres += ` AND  (${where_arr.join(' OR ')})`;
        let result = `UPDATE ${this.getThisTableName()} SET ${set_array.join(
            ', ',
        )}`;
        result += ` WHERE ${wheres ? ' ' + wheres : ''}`;

        let jsonflds = this.getFields(true)
            .slice(0, 50)
            .map((val) => `'${val}',${val}`)
            .join(',');

        result += ` RETURNING json_build_object(${jsonflds}) as data;`;
        return { script: result, values };
    }

    getSelectScript(wheres, values, getrownum = false) {
        let fld_array = this.getFields(this.showinvisible !== true).map(
            (value) => 't0.' + value,
        );
        let ord;

        if (
            this._orderby !== undefined &&
            this._orders[this._orderby] !== undefined
        ) {
            ord = this._orders[this._orderby];
        }

        if (ord === undefined) {
            ord = 't0.' + this._pk;
        }

        if (getrownum) {
            fld_array.push(
                'ROW_NUMBER() OVER(order by ' + ord + ') as thisrownum',
            );
        }

        /// Calc Fields
        let cf_array = [];

        for (let cf_name in this._calcfields) {
            if (this[cf_name] === true || this[cf_name] == 'true') {
                let cf = this._calcfields[cf_name];
                cf_array.push(cf.expression + ' as ' + cf.output_name);

                if (cf.requiredRefs !== undefined) {
                    for (let ref of cf.requiredRefs) {
                        this[ref] = true;
                    }
                }
            }
        }

        /// ~Calc Fields
        let join_arr = [];
        let i = 1;

        for (let r in this._refs) {
            if (this[r] !== undefined) {
                let t = 't' + i;

                if (this._refs[r].alias !== undefined) {
                    t = this._refs[r].alias;
                }

                if (this._refs[r].replace !== undefined) {
                    fld_array.push(
                        this._refs[r].replace.replace(/{#t_self}/g, t),
                    );
                }

                let rule_string = this._refs[r].rule.replace(/{#t_self}/g, t);
                let tbl = this._refs[r].table;

                if (
                    rule_string.search(/{#self_value}/g) != -1 ||
                    tbl.search(/{#self_value}/g) != -1
                ) {
                    let index = '$' + (values.length + 1);
                    values.push(this[r]);
                    rule_string = rule_string.replace(/{#self_value}/g, index);
                    tbl = tbl.replace(/{#self_value}/g, index);
                }

                if (this._refs[r].type === 'INNER') {
                    join_arr.push(`INNER JOIN ${tbl} ${t} ON ${rule_string}`);
                } else {
                    join_arr.push(
                        `LEFT OUTER JOIN ${tbl} ${t} ON ${rule_string}`,
                    );
                }

                i++;
            }
        }

        /// /
        let out_flds = fld_array.join(', ');

        if (cf_array.length > 0) {
            out_flds = out_flds + ', ' + cf_array.join(', ');
        }

        let result = `SELECT ${out_flds} FROM ${this.getThisTableName()} t0 `;
        result += join_arr.join(' ');
        result += wheres ? ' ' + wheres : '';
        result = `${result} order by ${ord}`;

        if (!isNaN(this._limit)) {
            result = `${result} limit ${this._limit}`;

            if (!isNaN(this._offset)) {
                result = `${result} offset ${this._offset}`;
            }
        }
        // console.dir({script: result, values: values});
        return { script: result, values };
    }

    getStaticWheres(istart) {
        let arr = [];
        let vl_data_array = [];
        let i = istart;

        for (let f in this._fields) {
            if (this[f] !== undefined) {
                arr.push(this.createStaticWhereText(f, 't0', i));
                vl_data_array.push(this[f]);
                i++;
            }
        }

        let result = arr.join(' AND ');
        return { script: result, values: vl_data_array, inext: i };
    }

    getFilterWheres(istart) {
        let arr = [];
        let vl_data_array = [];
        let i = istart;

        for (var f in this._filters) {
            if (this[f] !== undefined) {
                let arr_in = [];

                for (let fld in this._filters[f]) {
                    if (this._filters[f][fld].indexOf('.') > 0) {
                        arr_in.push(
                            '(UPPER(' +
                                this._filters[f][fld] +
                                "::varchar) like  '%$" +
                                i +
                                "#%')",
                        );
                    } else {
                        arr_in.push(
                            '(UPPER(t0.' +
                                this._filters[f][fld] +
                                "::varchar) like  '%$" +
                                i +
                                "#%')",
                        );
                    }
                }

                arr.push('(' + arr_in.join(' OR ') + ')');
                vl_data_array.push(this[f]);
                i++;
            }
        }

        for (f in this._userfilters) {
            if (this[f] !== undefined) {
                arr.push(
                    '(' +
                        this._userfilters[f].replace(/{#f_value}/g, '$' + i) +
                        ')',
                );
                vl_data_array.push(this[f]);
                i++;
            }
        }

        let result = arr.join(' AND ');
        return { script: result, values: vl_data_array, inext: i };
    }

    getRuleWheres() {
        let arr = [];

        for (let r in this._rules) {
            if (this[r] !== undefined && this[r] == true) {
                arr.push('(' + this._rules[r] + ')');
            }
        }

        let result = arr.join(' AND ');
        return { script: result };
    }

    async update(trans = cntc) {
        let pk = this.getPk();
        let { script, values } = this.getUpdateOneScript(pk + '=$1', [
            this[pk],
        ]);
        try {
            let oldfls = {};
            let has_oldfls = false;
            if (this.logerModel === undefined) {
                for (let f in this._fields) {
                    if (
                        this._fields[f].isfile === true &&
                        this[f] !== undefined
                    ) {
                        oldfls[f] = {};
                        has_oldfls = true;
                    }
                }
                if (has_oldfls) {
                    let old_data = await this.selectOneByIdJSON(trans);
                    if (old_data !== null) {
                        for (let f in oldfls) {
                            oldfls[f] = old_data[f];
                        }
                    } else {
                        has_oldfls = false;
                    }
                    //console.dir(oldfls);
                }
            }
            let data = await trans.tx(async (t) => {
                if (this.debugSQL === true) {
                    console.log(pgp.as.format(script, values));
                }
                return await t.oneOrNone(script, values, (val) => {
                    return val === null ? null : val.data;
                });
            });
            if (data !== null) {
                await this.logState(data, 2, trans);
                if (this.subscribable === true) {
                    this.sendSubscription(data[this._pk], 'UPDATED');
                }
                if (this.logerModel === undefined && has_oldfls) {
                    for (let f in oldfls) {
                        if (oldfls[f] != null) {
                            let pth = path.normalize(config.uploadDir);
                            if (config.uploadDirList !== undefined) {
                                pth = path.normalize(
                                    config.uploadDirList[oldfls[f].storage],
                                );
                            }
                            if (oldfls[f].subFolder) {
                                pth = pth + path.sep + oldfls[f].subFolder;
                            }
                            pth = pth + path.sep + oldfls[f].file;
                            try {
                                await fsPromises.unlink(pth);
                            } catch (e) {
                                console.dir(e);
                            }
                        }
                    }
                }
            } else {
                // Если ничего не вставилось, то нужно удалить файлы, которые были загружены на сервер
                await this.deleteUnusedClassFiles();
            }
            return data;
        } catch (err) {
            await this.deleteUnusedClassFiles();
            throw { code: err.code, message: err.message };
        }
    }

    getUpdateNameScript(wheres, values, prop = 'filename') {
        let set_array = [];
        let i = values.length + 1;

        let setArrayEntry = (f, i) => {
            if (prop === 'file') {
                return `${f}=json_build_object(
                                'file', $${i},
                                'filename', ${f}->'filename',
                                'size', ${f}->'size'
                            )`;
            }
            return `${f} = json_build_object(
                                'file', ${f}->'file',
                                'filename', $${i},
                                'size', ${f}->'size'
                            )`;
        };

        for (let f in this._fields) {
            if (this[f] !== undefined && this._fields[f].isfile) {
                set_array.push(setArrayEntry(f, i));
                i++;
                values.push(this[f]);
            }
        }

        let result = `UPDATE ${this.getThisTableName()} SET ${set_array.join(
            ', ',
        )}`;
        result = `${result} WHERE ${wheres ? ' ' + wheres : ''}`;

        let jsonflds = this.getFields(true)
            .slice(0, 50)
            .map((val) => `'${val}',${val}`)
            .join(',');

        result = `${result} RETURNING json_build_object(${jsonflds}) as data;`;
        return { script: result, values };
    }

    async updateFileName(trans = cntc, propName = 'filename') {
        return this.updateFileProperty(trans, propName);
    }

    async updateFileProperty(trans = cntc, propName = 'filename') {
        let pkColumn = this.getPk();
        let pkValue = this[pkColumn];

        let queryParams = {
            wheres: `${pkColumn}=$1`,
            values: [pkValue],
            propName,
        };
        let { script, values } = this.getUpdateNameScript(
            ...Object.values(queryParams),
        );

        try {
            let data = await trans.tx((t) =>
                t.oneOrNone(script, values, (val) => val && val.data),
            );

            if (data) {
                await this.logState(data, 2, trans);

                if (this.subscribable === true) {
                    this.sendSubscription(data[this._pk], 'UPDATED');
                }
            }
            return data;
        } catch (error) {
            await this.deleteUnusedClassFiles();
            throw { code: error.code, message: error.message };
        }
    }

    /**
     *
     * @param {Object} data  - объект для сохранения
     * @param {number} action - тип действия 1 - вставка; 2 - обновление; 3 - удаление
     * @param {Object} trans - подключение к БД
     */
    async logState(data, action, trans = cntc) {
        if (this.logerModel === undefined) {
            return;
        }

        let temp_id = this.id;
        this.id = data.id;
        this.showinvisible = true;
        let new_data = await this.selectOneByIdJSON(trans);
        this.showinvisible = false;

        if (!(new_data !== null && new_data !== undefined)) {
            new_data = clonedeep(data);
        }

        try {
            new_data.original_id = new_data.id;
            new_data.id = undefined;
            let fls = this.getFileFileds();

            if (fls.length > 0) {
                new_data.fileUploaded = {};

                for (let f of fls) {
                    if (new_data[f] !== null) {
                        new_data.fileUploaded[f] = {};
                        new_data.fileUploaded[f].path = new_data[f].file;
                        new_data.fileUploaded[f].name = new_data[f].filename;
                    }
                }
            }

            let loger = new this.logerModel();
            loger.loadFromRequest(new_data);
            loger.changer_airuser_id = this.oper_user.id;
            loger.record_deleted_flag = action === 3;
            await loger.insert(trans);
        } catch (error) {
            console.dir(error);
            throw error;
        }

        this.id = temp_id;
    }

    sendSubscription(data_to_send, action) {
        let cnt = WSSList.length;
        let lowname = this.constructor.name.toLowerCase();

        if (cnt > 0) {
            let con_list = WSSList[0].getSubscribes(lowname);

            for (let cn of con_list) {
                try {
                    cn.connection.send(
                        JSON.stringify({
                            resourse: lowname,
                            pk: data_to_send,
                            action,
                        }),
                    );
                } catch (error) {
                    console.dir(error);
                }
            }
        }
    }

    async insert(trans = cntc) {
        let { script, values } = this.getInsertScript();

        try {
            let data = await trans.tx(async (t) => {
                if (config.debugSQL === true) {
                    console.log(pgp.as.format(script, values));
                }
                return t.oneOrNone(script, values, (val) =>
                    val === null ? null : val.data,
                );
            });

            if (data !== null) {
                await this.logState(data, 1, trans);

                if (this.subscribable === true) {
                    this.sendSubscription(data[this._pk], 'INSERTED');
                }
            } else {
                // Если ничего не вставилось, то нужно удалить файлы, которые были загружены на сервер
                await this.deleteUnusedClassFiles();
            }
            return data;
        } catch (error) {
            await this.deleteUnusedClassFiles();
            throw { code: error.code, message: error.message };
        }
    }

    async deleteUnusedClassFiles() {
        for (let f in this._fields) {
            if (
                this._fields[f].isfile === true &&
                this[f] !== undefined &&
                this[f].file !== undefined
            ) {
                let pth = path.normalize(
                    config.uploadDir + path.sep + this[f].file,
                );

                if (config.uploadDirList !== undefined) {
                    pth = path.normalize(
                        config.uploadDirList[this[f].storage] +
                            path.sep +
                            this[f].file,
                    );
                }

                try {
                    await fsPromises.unlink(pth);
                } catch (error) {
                    console.dir(error);
                }
            }
        }
    }

    getDeleteOneScript() {
        let pk = this.getPk();
        let jsonflds = this.getFields(true)
            .slice(0, 50)
            .map((val) => "'" + val + "'," + val)
            .join(',');

        let result =
            'DELETE FROM ' +
            this.getThisTableName() +
            ' where ' +
            pk +
            '=$1 RETURNING json_build_object(' +
            jsonflds +
            ') as data;';

        return { script: result, values: [this[pk]] };
    }

    getDeleteScript() {
        let index = 1;

        if (this.wheres_default !== undefined && this.wheres_default.script) {
            index = this.wheres_default.values.length + 1;
        }

        let wheres = this.getStaticWheres(index);
        let wr = [];
        let vl = [];

        if (this.wheres_default !== undefined && this.wheres_default.script) {
            wr.push(this.wheres_default.script);
            vl = vl.concat(this.wheres_default.values);
        }

        if (wheres.script) {
            wr.push(wheres.script);
            vl = vl.concat(wheres.values);
        }

        let jsonflds = this.getFields(true)
            .slice(0, 50)
            .map((val) => "'" + val + "'," + val)
            .join(',');

        let result =
            'DELETE FROM ' +
            this.getThisTableName() +
            ' t0 where ' +
            wr.join(' AND ') +
            ' RETURNING json_build_object(' +
            jsonflds +
            ') as data;';

        return { script: result, values: vl };
    }

    async deleteAny(trans = cntc) {
        let { script, values } = this.getDeleteScript();

        try {
            let data = await trans.tx(async (t) =>
                t.manyOrNone(script, values),
            );
            // await this.logState(data,3);
            // todo: Надо проверить как будет работать при возвращении множества записей
            data = data.map((value) => value.data);
            return data;
        } catch (error) {
            throw { code: error.code, message: error.message };
        }
    }

    /**
     * Возвращает одну запись в формате JSON по значению КЛЮЧЕВОГО поля.
     */
    async selectOneByIdJSON(trans = cntc) {
        let selscrpt = '';
        let pk = this.getPk();

        if (this.wheres_default !== undefined) {
            let l = this.wheres_default.values.length + 1;
            let wherescript = `${this.wheres_default.script} AND t0.${pk}=$${l}`;
            let wherevalues = [...this.wheres_default.values];
            wherevalues.push(this[pk]);
            selscrpt = this.getSelectScript(
                ` WHERE ${wherescript}`,
                wherevalues,
            );
        } else {
            selscrpt = this.getSelectScript(` WHERE t0.${pk}=$1 `, [this[pk]]);
        }

        let script = `select row_to_json(winquery) as data from (${selscrpt.script}) winquery;`;

        try {
            let data = await trans.tx(async (t) => {
                if (config.debugSQL === true) {
                    console.log(pgp.as.format(script, selscrpt.values));
                }
                return t.oneOrNone(
                    script,
                    selscrpt.values,
                    (val) => val && val.data,
                );
            });

            return data;
        } catch (error) {
            throw { code: error.code, message: error.message };
        }
    }

    async selectAllCountJSON(trans = cntc) {
        let data = await this.selectAllJSON(1, trans);
        return { rowcount: data };
    }

    async selectAllNextJSON() {
        let data = await this.selectAllJSON(2);
        return data;
    }

    async selectAllPreviousJSON() {
        let data = await this.selectAllJSON(3);
        return data;
    }

    /**
     * Выбирает из базы данных записи в соответствии с параметрами модели.
     * @param {number} selecttype - тип запроса (0 - выбрать данные, 1 - вернуть количество строк)
     * @param {Object} trans - объект транзакции или подключения к БД
     * @example
     * try {
     *     let u = await user.selectAllJSON();
     *     console.dir(u);
     * }
     * catch (err) {
     *     console.dir(err);
     * }
     */

    async selectAllJSON(selecttype = 0, trans = cntc) {
        let index = 1;

        if (this.wheres_default !== undefined && this.wheres_default.script) {
            index = this.wheres_default.values.length + 1;
        }

        let wheres = this.getStaticWheres(index);
        let wheres_filt = this.getFilterWheres(wheres.inext);
        let wheres_rules = this.getRuleWheres();
        let wr = [];
        let vl = [];

        if (this.wheres_default !== undefined && this.wheres_default.script) {
            wr.push(this.wheres_default.script);
            vl = vl.concat(this.wheres_default.values);
        }

        if (wheres.script) {
            wr.push(wheres.script);
            vl = vl.concat(wheres.values);
        }

        if (wheres_filt.script) {
            wr.push(wheres_filt.script);
            vl = vl.concat(wheres_filt.values);
        }

        if (wheres_rules.script) {
            wr.push(wheres_rules.script);
        }

        let wrs = '';

        if (
            wheres.script ||
            wheres_filt.script ||
            wheres_rules.script ||
            (this.wheres_default && this.wheres_default.script)
        ) {
            wrs = ' WHERE ' + wr.join(' AND ');
        }

        let selscrpt;

        if (selecttype > 1) {
            this._visible_fields = [this._pk];
            this.showinvisible = false;
            selscrpt = this.getSelectScript(wrs, vl, true);
        } else {
            selscrpt = this.getSelectScript(wrs, vl);
        }

        let script;

        if (selecttype == 1) {
            script =
                'select count(*) as data from (' +
                selscrpt.script +
                ') winquery;';
        } else if (selecttype == 2) {
            script = `select json_build_object('nextkey',w2.${this._pk}) as data from (${selscrpt.script}) w1
                        left outer join (${selscrpt.script}) w2
                        on w2.thisrownum=w1.thisrownum+1
                        where w1.${this._pk}= ${this._pkcurrent};`;
        } else if (selecttype == 3) {
            script = `select json_build_object('previouskey',w2.${this._pk}) as data from (${selscrpt.script}) w1
                        left outer join (${selscrpt.script}) w2
                        on w2.thisrownum=w1.thisrownum-1
                        where w1.${this._pk}= ${this._pkcurrent};`;
        } else {
            script =
                'select json_agg(winquery) as data from (' +
                selscrpt.script +
                ') winquery;';
        }

        try {
            let data = await trans.tx(async (t) => {
                if (config.debugSQL === true) {
                    console.log(pgp.as.format(script, selscrpt.values));
                }
                return t.oneOrNone(script, selscrpt.values, (val) =>
                    val === null ? null : val.data,
                );
            });

            return data;
        } catch (error) {
            throw { code: error.code, message: error.message };
        }
    }

    /**
     * Запускается при необходимости создания таблицы для модели.
     * Создает SQL-скрипт для создания таблицы и необходимых дополнений.
     * Запускает созданный SQL-скрипт.
     */
    async merge() {
        let script;

        try {
            script = this.getCreateScript();
            let data = await cntc.tx((t) => t.none(script));
            return data;
        } catch (error) {
            throw { code: error.code, message: error.message, sql: script };
        }
    }

    /**
     * Загружает в модель данные из объекта:
     * 1. значения полей, определеных в модели;
     * 2. значенияименнованных фильтров;
     * 3. имя порядка сортировки.
     * 4. лимит выдачи для SELECT
     * 5. смещение выдачи для SELECT
     * 6. именованные ссылки на данные из других таблиц (JOIN)
     * @param {Object} data - объект из которого должна происходить загрузка данных.
     * @example
     * let protoModel = new Protomodel();
     * protoModel.loadFromRequest(req.body);
     */
    loadFromRequest(data) {
        // Загружает список полей, фильтров, сортировок и т.д. из запроса
        super.loadFromRequest(data);

        for (let r in this._refs) {
            this[r] = data[r];
        }

        for (let c in this._calcfields) {
            this[c] = data[c];
        }
    }

    /**
     * Статическая функция класса Model
     * Добавляет к классу ссылку на другой класс.
     * @param {string} key - Имя класса к которому добавляется ссылка на другой класс.
     * @param {Object} data - объект c с описанием связи.
     * depClass - имя зависимого класса
     * refType - тип зависимости
     * refFieldName - поле класса (key) от которого зависит  связь
     * depFieldName - поле зависимого класса от которого зависит связь
     * @example
     * Model.addLink('Airuser', {depClass: AiruserDetail, refType: 'RESTRICT', refFieldName:'id', depFieldName: 'airuser_id'});
     */
    static addLink(key, value) {
        if (LinkList[key] === undefined) {
            LinkList[key] = [];
        }

        LinkList[key].push(value);
    }

    /**
     * Статическая функция класса Model
     * Возвращает массив зависимостей для заданного класса.
     * @param {string} key - Имя класса.
     * @example
     * Model.getLinks('Airuser');
     */
    static getLinks(key) {
        return LinkList[key];
    }

    /**
     * Добавляет к классу одну ссылку на другой класс.
     * @param {Object} field - Параметры поля.
     */
    addOneLink(field) {
        Model.addLink(field.refClass.name, {
            depClass: this.constructor,
            refType: field.refType,
            refFieldName: field.refFieldName,
            depFieldName: field.fieldName,
        });
    }

    /**
     * Добавляет к классу ссылки на другой класс. Добавление происходит в соответствии со списком полей.
     */
    addLinks() {
        for (let f in this._fields) {
            if (this._fields[f].refClass !== undefined) {
                this.addOneLink(this._fields[f]);
            }
        }
    }

    /**
     * Возвращает массив зависимостей для данного объекта.
     * @example
     * Airuser.getLinks();
     */
    getLinks() {
        let res = Model.getLinks(this.constructor.name);

        if (res == undefined) {
            res = [];
        }
        return res;
    }

    async getOneGuard(user) {
        return this.getAllGuard(user);
    }

    async deleteOne(trans = cntc) {
        let { script, values } = this.getDeleteOneScript();

        try {
            let data = await trans.tx((t) =>
                t.oneOrNone(script, values, (val) => val && val.data),
            );

            if (!data) {
                return null;
            }

            await this.logState(data, 3, trans);

            if (this.subscribable === true) {
                this.sendSubscription(data[this._pk], 'DELETED');
            }

            if (this.logerModel === undefined) {
                for (let f in this._fields) {
                    if (this._fields[f].isfile === true) {
                        let filePath = path.normalize(
                            path.join(config.uploadDir, data[f].file),
                        );

                        // eslint-disable-next-line no-await-in-loop
                        await fsPromises.unlink(filePath).catch((error) => {
                            console.log('unlink error', error);
                        });
                    }
                }
            }
            return data;
        } catch (error) {
            throw { code: error.code, message: error.message };
        }
    }

    async testAccess(user) {
        this.oper_user = user;

        try {
            if (user.isadmin === true) {
                return { gres: true, text: 'Ok' };
            }
            return { gres: false, text: 'Нет доступа.' };
        } catch (error) {
            return { gres: false, text: error.message };
        }
    }

    async putGuard(user) {
        return this.testAccess(user);
    }

    async postGuard(user) {
        return this.testAccess(user);
    }

    async deleteGuard(user) {
        return this.testAccess(user);
    }

    getSelectScriptExisting(wheres, values) {
        let fld_array = this.getFields(this.showinvisible !== true).map(
            (value) => 't0.' + value,
        );
        let join_arr = [];
        let i = 1;

        for (let r in this._refs) {
            if (this[r] !== undefined) {
                let t = 't' + i;

                if (this._refs[r].replace !== undefined) {
                    fld_array.push(
                        this._refs[r].replace.replace(/{#t_self}/g, t),
                    );
                }

                let rule_string = this._refs[r].rule.replace(/{#t_self}/g, t);
                let tbl = this._refs[r].table;

                if (
                    rule_string.search(/{#self_value}/g) != -1 ||
                    tbl.search(/{#self_value}/g) != -1
                ) {
                    let index = '$' + (values.length + 1);
                    values.push(this[r]);
                    rule_string = rule_string.replace(/{#self_value}/g, index);
                    tbl = tbl.replace(/{#self_value}/g, index);
                }

                if (this._refs[r].type === 'INNER') {
                    join_arr.push(`INNER JOIN ${tbl} ${t} ON ${rule_string}`);
                } else {
                    join_arr.push(
                        `LEFT OUTER JOIN ${tbl} ${t} ON ${rule_string}`,
                    );
                }

                i++;
            }
        }

        /// /
        let result = `SELECT ${fld_array.join(
            ', ',
        )} FROM ${this.getThisTableName()} t0 `;
        result += join_arr.join(' ');
        result += wheres ? ' ' + wheres : '';
        return { script: result, values };
    }

    /**
     * Выбирает из базы данных все записи данной сущности. Возвращает их в формате JSON.
     * Не применяет никаких фильтров. Выбирает только записи с flagarchive=0;
     * @param {number} selecttype - тип запроса (0 - выбрать данные, 1 - вернуть количество строк)
     * @param {Object} trans - объект транзакции или подключения к БД
     * @example
     * try {
     *     let u = await user.selectAllExistingJSON();
     *     console.dir(u);
     * }
     * catch (err) {
     *     console.dir(err);
     * }
     */
    async selectAllExistingJSON(selecttype = 0, trans = cntc) {
        let index = 1;

        if (this.wheres_default !== undefined && this.wheres_default.script) {
            index = this.wheres_default.values.length + 1;
        }

        let wheres = this.getStaticWheres(index);
        let wr = [];
        let vl = [];

        if (this.wheres_default !== undefined && this.wheres_default.script) {
            wr.push(this.wheres_default.script);
            vl = vl.concat(this.wheres_default.values);
        }

        if (wheres.script) {
            wr.push(wheres.script);
            vl = vl.concat(wheres.values);
        }

        let wrs = '';

        if (
            wheres.script ||
            (this.wheres_default && this.wheres_default.script)
        ) {
            wrs = ` WHERE ${wr.join(' AND ')}`;
        }

        let selscrpt = this.getSelectScriptExisting(wrs, vl);
        let script =
            selecttype == 1
                ? `select count(*) as data from (${selscrpt.script}) winquery;`
                : `select json_agg(winquery) as data from (${selscrpt.script}) winquery;`;

        try {
            let data = await trans.tx(async (t) =>
                t.oneOrNone(script, selscrpt.values, (val) =>
                    val === null ? null : val.data,
                ),
            );

            if (selecttype == 1) {
                return { rowcount: data };
            }
            return data;
        } catch (error) {
            throw { code: error.code, message: error.message };
        }
    }

    async selectMaxId(trans = cntc) {
        let script = `select max(id) as id from ${this.getThisTableName()};`;
        try {
            let id = await trans.tx((t) =>
                t.oneOrNone(script, [], (val) => val && val.id),
            );
            return Number(id);
        } catch (error) {
            throw { code: error.code, message: error.message };
        }
    }
}

module.exports = Model;
