const path = require('node:path');
const stream = require('node:stream');
const express = require('express');
const sharp = require('sharp');
const Jimp = require('jimp');

const { getFileFolderPath, moveFileToStorage } = require('../helpers/files.js');

const showFilePreview = async ({ res, previewWidth, originalFileName, filePath }) => {
    try {
        let width = 900;

        if (Number.isInteger(Number(previewWidth)) && Number(previewWidth) > 10) {
            width = Number(previewWidth);
        }

        let { ext: originalFileExtension } = path.parse(originalFileName);

        if (originalFileExtension.toUpperCase() === '.BMP') {
            let bmpFile = await Jimp.read(filePath);
            let thumbnail = await bmpFile
                .resize(width, Jimp.AUTO)
                .quality(60)
                .getBufferAsync(Jimp.MIME_JPEG);

            let readStream = new stream.PassThrough();
            readStream.end(thumbnail);
            readStream.pipe(res);
        }

        let thumbnail = await sharp(filePath)
            .resize(width)
            .jpeg({
                quality: 80,
                chromaSubsampling: '4:4:4',
            })
            .toBuffer();

        let readStream = new stream.PassThrough();

        readStream.end(thumbnail);
        readStream.pipe(res);
    } catch (error) {
        return res.status(409).json({
            error: 'Невозможно создать предпросмотр. message: ' + error.message,
        });
    }
};

const checkAuth = async ({ dataModel, guardName, user }) => {
    let guardingFn = dataModel[guardName].bind(dataModel);
    return guardingFn(user)
        .then((r) => ({ isAuthorized: r.gres }))
        .catch((error) => ({
            isAuthorized: false,
            text: error.message,
            code: 500,
        }));
};

const doRedirect = async ({ res, redirectToURL, redirectCookie = [] }) => {
    let cookies = Array.isArray(redirectCookie) ? redirectCookie : [redirectCookie];

    let cookieOptions = {
        secure: true,
        sameSite: 'None',
        maxAge: 1000 * 60 * 60 * 24,
    };

    for (let cookie of cookies) {
        res.cookie(cookie.name, cookie.value, cookieOptions);
    }
    return res.redirect(redirectToURL);
};

const processRequestNew = async  (props)=> {
    let {
        req,
        dataName,
        res,
        dataModel,
        guardName,
        actionName,
        requiredFields,
        requestedParamName: requestedParam,
    } = props;

    await dataModel.parseFormidable(req);
    dataModel.loadFromRequest(req[dataName]);
    await dataModel.deleteUnusedUploded(req[dataName]);

    let missingRequiredFields = dataModel.checkRequiredFields(requiredFields);
    let isMissingRequiredFields = missingRequiredFields.length > 0;

    if (isMissingRequiredFields) {
        return res
            .status(409)
            .json({ error: 'Не заданы поля: ' + missingRequiredFields.join(', ') });
    }

    if (dataModel[guardName]) {
        let isAuthorized = await checkAuth({ dataModel, guardName, user: req.user });
        if (!isAuthorized) return res.status(401).json({ error: 'Доступ запрещен' });
    }

    try {
        let actionResult = await dataModel[actionName]();
        let actionResultType = typeof actionResult;
        let isUploadedFile = req.body.fileUploaded;

        if (isUploadedFile) {
            await moveFileToStorage({ req, dataModel, actionResult });
        }

        if (actionResultType === 'string') {
            return res.status(200).send(actionResult);
        }

        let redirectToURL = actionResult?.redirectToURL;

        if (redirectToURL) {
            let { redirectCookie = [] } = actionResult || {};
            return doRedirect({ redirectToURL, redirectCookie, res });
        }

        if (!requestedParam) {
            return res.status(200).json(actionResult);
        }

        if (actionResult === null) {
            return res.status(409).json({
                error: 'Объект, который должен содержать параметр, не найден.',
            });
        }

        let dbColumnValue = actionResult[requestedParam];

        if (!dbColumnValue) {
            return res.status(409).json({ error: 'Данные параметра не найдены.' });
        }

        // todo: comprehend and make this readable
        // (at least at this point) requestedParamValue
        // is a name of a column ("field") in the database
        // dataModel._fields[requestedParamValue] defines field properties
        // so if requestedParamValue matches a column name in database
        // and datamodel._fields[] object specifies .isfile === true
        let isFileColumn = dataModel._fields[requestedParam].isfile;

        if (!isFileColumn) {
            return res.status(200).json(dbColumnValue);
        }

        let { id, creation_datetime: date } = actionResult;
        let { file: fileName } = dbColumnValue;
        let fileFolderPath = getFileFolderPath({ id, date });
        let filePath = path.join(fileFolderPath, fileName);

        let { filename: originalFileName } = dbColumnValue;

        if (req.params.width) {
            return showFilePreview({
                filePath,
                originalFileName,
                previewWidth: req.params.width,
                res,
            });
        }
        return res.download(filePath, originalFileName);
    } catch (error) {
        let code = 500;
        console.dir(error);
        let errResult = {};
        errResult.error = `Ошибка при обработке запроса: ${error.message} (Код:${error.code})`;

        if (dataModel.translateError !== undefined) {
            let errTnans = dataModel.translateError(error);

            if (errTnans !== undefined) {
                errResult.error = errTnans.message;
                code = errTnans.code;

                if (errTnans.extraData !== undefined) {
                    errResult.extraData = errTnans.extraData;
                }
            }
        }

        res.status(code).json(errResult);
    }
};

class SRouter extends express.Router {
    constructor(DataModel) {
        super();

        this.get('/', async (req, res) => {
            let dataModel = new DataModel();

            let props = {
                req,
                dataName: 'query',
                res,
                dataModel,
                guardName: 'getAllGuard',
                actionName: 'selectAllJSON',
            };

            await processRequestNew(props);
        });

        this.get('/:id/:parname', async (req, res) => {
            let dataModel = new DataModel();
            let pk = dataModel.getPk();

            if (pk === undefined) {
                return res.status(500).json({ error: 'Для модели не определен первичный ключ.' });
            }

            let props = {
                req,
                res,
                dataName: 'query',
            };

            if (req.params.parname === '_getnextkey_') {
                dataModel._pkcurrent = req.params.id;
                props.dataModel = dataModel;
                props.guardName = 'getAllGuard';
                props.actionName = 'selectAllNextJSON';
                await processRequestNew(props);
            } else if (req.params.parname === '_getpreviouskey_') {
                dataModel._pkcurrent = req.params.id;
                props.dataModel = dataModel;
                props.guardName = 'getAllGuard';
                props.actionName = 'selectAllPreviousJSON';
                await processRequestNew(props);
            } else {
                req.query[pk] = req.params.id;
                props.dataModel = dataModel;
                props.guardName = 'getOneGuard';
                props.actionName = 'selectOneByIdJSON';
                props.requestedFields = [pk];
                props.requestedParamName = req.params.parname;

                await processRequestNew(props);
            }
        });

        this.get('/:id/:parname/preview/:width', async (req, res) => {
            let dataModel = new DataModel();
            let pk = dataModel.getPk();

            if (pk === undefined) {
                return res.status(500).json({ error: 'Для модели не определен первичный ключ.' });
            }

            req.query[pk] = req.params.id;

            let props = {
                req,
                res,
                dataName: 'query',
                dataModel,
                guardName: 'getOneGuard',
                actionName: 'selectOneByIdJSON',
                requiredFields: [pk],
                requestedParamName: req.params.parname,
            };

            await processRequestNew(props);
        });

        // Добавить обработчик маршрута /:id - конкретный объект
        this.get('/:id', async (req, res) => {
            let dataModel = new DataModel();

            let props = {
                req,
                res,
                dataName: 'query',
                dataModel,
                guardName: 'getOneGuard',
                actionName: 'selectOneByIdJSON',
            };

            if (req.params.id === '_getrowcount_') {
                props.actionName = 'selectAllCountJSON';
                return processRequestNew(props);
            }

            let pk = dataModel.getPk();

            if (pk === undefined) {
                return res.status(500).json({ error: 'Для модели не определен первичный ключ.' });
            }

            req.query[pk] = req.params.id;
            props.requiredFields = [pk];
            return processRequestNew(props);
        });

        this.put('/:id', async (req, res) => {
            let dataModel = new DataModel();
            let pk = dataModel.getPk();

            if (pk === undefined) {
                return res.status(500).json({ error: 'Для модели не определен первичный ключ.' });
            }

            req.body[pk] = req.params.id;

            let props = {
                req,
                res,
                dataName: 'body',
                dataModel,
                guardName: 'putGuard',
                actionName: 'update',
                requiredFields: [pk],
            };

            return processRequestNew(props);
        });

        this.put('/:id/updatefilename', async (req, res) => {
            let dataModel = new DataModel();
            let pk = dataModel.getPk();

            if (pk === undefined) {
                return res.status(500).json({ error: 'Для модели не определен первичный ключ.' });
            }

            req.body[pk] = req.params.id;

            let props = {
                req,
                res,
                dataName: 'body',
                dataModel,
                guardName: 'putGuard',
                actionName: 'updateFileName',
                requiredFields: [pk],
            };

            return processRequestNew(props);
        });

        this.delete('/:id', async (req, res) => {
            let dataModel = new DataModel();
            let pk = dataModel.getPk();

            if (pk === undefined) {
                return res.status(500).json({ error: 'Для модели не определен первичный ключ.' });
            }

            req.body[pk] = req.params.id;

            let props = {
                req,
                res,
                dataName: 'body',
                dataModel,
                guardName: 'deleteGuard',
                actionName: 'deleteOne',
                requiredFields: [pk],
            };

            return processRequestNew(props);
        });

        this.post('/', async (req, res) => {
            if (req.body.id !== undefined) {
                return res.status(409).json({
                    error: 'Параметр id не может быть использован при вставке.'+
                    'Он генерируется автоматически.',
                });
            }

            let dataModel = new DataModel();
            let props = {
                req,
                res,
                dataName: 'body',
                dataModel,
                guardName: 'postGuard',
                actionName: 'insert',
            };

            return processRequestNew(props);
        });
    }
}

module.exports = SRouter;
