module.exports = {
    endOfLine: 'auto',
    proseWrap: 'preserve',
    tabWidth: 4,
    printWidth: 80,
    useTabs: false,
    semi: true,
    singleQuote: true,
    bracketSpacing: true,
    trailingComma: 'all',
};
