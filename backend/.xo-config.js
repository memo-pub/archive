module.exports = {
    space: 4,
    root: true,
    extends: 'prettier',
    plugins: ['prettier', 'prefer-arrow'],
    rules: {
        'arrow-parens': ['error', 'always'],
        "arrow-body-style": ["warn", "as-needed"],
        proseWrap: 0,
        'prefer-arrow/prefer-arrow-functions': [
            'error',
            {
                disallowPrototype: true,
                singleReturnOnly: false,
                classPropertiesAllowed: false,
            },
        ],
        indent: ['error', 4, { SwitchCase: 1 }],
        'unicorn/prefer-module': 'off',
        'unicorn/prevent-abbreviations': 'off',
        'import/extensions': 'off',
        'capitalized-comments': 'off',
        'no-warning-comments': 'off',
        'linebreak-style': 'off',
        'prefer-arrow-callback': 'warn',
        lineBreakStyle: 0,
        'no-throws-literal': 'off',
        'unicorn/filename-case': 'off',
        'no-throw-literal': 'off',
        camelcase: 'warn',
        'no-unsafe-optional-chaining': 'error',
        'padding-line-between-statements': [
            'error',
            {
                blankLine: 'always',
                prev: ['*'],
                next: [
                    'for',
                    'multiline-const',
                    'while',
                    'do',
                    'switch',
                    'try',
                    'class',
                    'function',
                    'export',
                    'import',
                ],
            },

            {
                blankLine: 'any',
                prev: ['*'],
                next: ['return', 'if'],
            },

            {
                blankLine: 'any',
                prev: ['singleline-const', 'singleline-let', 'if'],
                next: ['*'],
            },

            {
                blankLine: 'always',
                prev: [
                    'for',
                    'multiline-const',
                    'while',
                    'do',
                    'switch',
                    'try',
                    'class',
                    'function',
                    'export',
                    'import',
                ],
                next: ['*'],
            },
            {
                blankLine: 'never',
                prev: ['if', 'for'],
                next: ['return'],
            },
            {
                blankLine: 'never',
                prev: ['import'],
                next: ['import'],
            },
        ],
        'max-len': [
            'error',
            {
                code: 100,
                ignoreUrls: true,
            },
        ],
    },
    settings: {
        'import/resolver': 'node',
    },
};
