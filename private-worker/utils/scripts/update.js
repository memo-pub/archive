import { logError } from "../../helpers/log-error.js";

export const updateScript = async (cntc, tableName, parameters, conditions) => {
  const paramToStr = () => {
    const paramString = [];
    for (let param in parameters) {
      paramString.push(
        `"${param}" = ${
          typeof parameters[param] === "string"
            ? `'${parameters[param]}'`
            : parameters[param]
        }`
      );
    }
    return paramString.join(", ");
  };
  const conditionsToStr = () => {
    const condString = [];
    for (let cond in conditions) {
      condString.push(
        `"${cond}" = ${
          typeof conditions[cond] === "string"
            ? `'${conditions[cond]}'`
            : conditions[cond]
        }`
      );
    }
    return condString.join(", ");
  };
  try {
    if (tableName && parameters && conditions) {
      const sql = `UPDATE ${tableName} SET ${paramToStr()} WHERE ${conditionsToStr()} RETURNING ${Object.keys(
        parameters
      )
        .map((str) => `"${str}"`)
        .join(", ")};`;
      return await cntc.manyOrNone(sql);
    } else {
      return "Не введены необходимые поля";
    }
  } catch (err) {
    logError(err, updateScript.name)
  }
};
