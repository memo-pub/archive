import { logError } from "../../helpers/log-error.js";

export const insertScript = async (
  cntc,
  tableName = "",
  columnNames = [],
  values = []
) => {
  if (!tableName) {
    return "Введите название таблицы";
  }
  if (!columnNames) {
    return "Введите столбцы для заполнения";
  }
  if (!values) {
    return "Введите значения для заполнения";
  }
  try {
    const formattedColumnNames = `${columnNames
      .map((value) => `"${value}"`)
      .join(", ")}`;

    const formattedValues = `${values
      .map((value) => {
        const escapedQuotes = value.replace(/'/g, "''");
        return `'${escapedQuotes}'`;
      })
      .join(", ")}`;

    if (tableName && columnNames && values && cntc) {
      const sql = `INSERT INTO ${tableName} (${formattedColumnNames}) VALUES (${formattedValues});`;
      return await cntc.manyOrNone(sql);
    }
  } catch (err) {
    logError(err, insertScript.name)
  }
};
