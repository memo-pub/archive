import { connection } from "../../helpers/connect_db.js";
import { logError } from "../../helpers/log-error.js";

export const getInformantArchivefolders = async () => {
  try {
    const informDelosStr = `WITH opis_list AS (SELECT id, fond, opis_name FROM opis),
        inform_delos as (SELECT t2.id as "opis_id", file_no, file_no_ext FROM personal_t t1, opis_list t2
        WHERE type_i IS NOT null AND fond=fund AND opis_name=list_no)
        SELECT t1.id FROM archivefolder t1, inform_delos t2 
        WHERE t1.opis_id=t2.opis_id AND delo=file_no AND delo_extra=file_no_ext;`;
    return await connection.manyOrNone(informDelosStr);
  } catch (err) {
    logError(err, getInformantArchivefolders.name);
  }
};
