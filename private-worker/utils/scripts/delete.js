import { logError } from "../../helpers/log-error.js";

export const deleteScript = async (cntc, tableName = "", conditions) => {
  if (!tableName) {
    return "Введите название таблицы";
  }
  const conditionsToStr = () => {
    const condString = [];
    for (let cond in conditions) {
      condString.push(
        ` WHERE ${cond} = ${
          typeof conditions[cond] === "string"
            ? `'${conditions[cond]}'`
            : conditions[cond]
        }`
      );
    }
    return condString.join(", ");
  };
  try {
    if (tableName && cntc) {
      const sql = `DELETE FROM ${tableName}${
        conditions ? conditionsToStr() : ""
      };`;
      return await cntc.manyOrNone(sql);
    }
  } catch (err) {
    logError(err, deleteScript.name)
  }
};
