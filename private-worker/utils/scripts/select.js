import { logError } from "../../helpers/log-error.js";
import { logMessage } from "../../helpers/log-message.js";
import { TABLE_NAMES } from "../../static/table-names.js";
import { currentDate } from "../global-vars/current-date.js";

const conditionsToStr = (conditions, isStrictMode, isQuotesColumn) => {
  const condString = [];

  const generateConditionsFormat = (key, val) => {
    const isString = typeof val === "string";
    const isNumber = typeof val === "number";
    const isNull = val === null;
    const hasSubStr = isString ? val.startsWith("%") : false;

    const generateStringFromArray = (value = [], key = "") => {
      const formattedValues = value.map((v) =>
        typeof v === "number"
          ? `${isQuotesColumn ? `"${key}"` : key}=${v}`
          : `${isQuotesColumn ? `"${key}"` : key} like '${v}'`
      );
      return formattedValues.join(" OR ");
    };

    return isNull
      ? `${isQuotesColumn ? `"${key}"` : key} IS NULL`
      : isString && !hasSubStr
      ? `${isQuotesColumn ? `"${key}"` : key} = '${val}'`
      : isString && hasSubStr
      ? `${isQuotesColumn ? `"${key}"` : key} LIKE '${val}'`
      : isNumber
      ? `${isQuotesColumn ? `"${key}"` : key} = ${val}`
      : generateStringFromArray(val, key);
  };

  for (let cond in conditions) {
    condString.push(`${generateConditionsFormat(cond, conditions[cond])}`);
  }
  return isStrictMode ? condString.join(" AND ") : condString.join(" OR ");
};

export const selectScript = async (selectObj) => {
  const {
    cntc,
    tableName,
    parameters,
    conditions,
    isStrictMode,
    isOrderBy,
    limit,
    isQuotesColumn,
    offset,
  } = selectObj;

  try {
    if (tableName) {
      const paramToSql =
        parameters && parameters.length ? parameters.join(", ") : "*";

      const contToSql =
        conditions && Object.keys(conditions).length !== 0
          ? ` WHERE ${conditionsToStr(
              conditions,
              isStrictMode,
              isQuotesColumn
            )}`
          : "";

      const orderStr = isOrderBy ? ` ORDER BY ${isOrderBy}` : "";

      const limitStr = limit ? ` LIMIT ${limit}` : "";

      const offsetStr = ![null, undefined].includes(offset)
        ? ` OFFSET ${offset}`
        : "";

      const sql = `SELECT ${paramToSql} FROM ${tableName}${contToSql}${orderStr}${limitStr}${offsetStr};`;
      try {
        return await cntc.manyOrNone(sql);
      } catch (err) {
        logError(err, `selectScript SQL Query`);
        logError(sql, `SQL Query`);
      }
    } else {
      return "Введите название таблицы";
    }
  } catch (err) {
    logError(err, selectScript.name);
  }
};
