import { logError } from "../../helpers/log-error.js";

export const getMentionedPers = async (cntc, code) => {
  if (!code) {
    return "Введите код персоналии";
  }

  try {
    if (code && cntc) {
      const sql = `WITH personal_t_co AS (
        SELECT * FROM PERSONAL_T where code = ${code}
    ),
    coact_t_f AS (select * from coact_t where human_code = ${code}),
    coorg_t_f AS (select * from coorg_t where human_code = ${code}),
    corep_t_f AS (select * from corep_t where human_code = ${code}),
    cocrt_t_f AS (select * from cocrt_t where human_code = ${code}),
    coimp_t_f AS (select * from coimp_t where human_code = ${code}),
    inform_t_f AS (select * from inform_t where human_code = ${code}),
    family_t_f AS (select * from family_t where human_code = ${code}),
    mentioned_t_f AS (select * from mentioned_t where human_code = ${code})
    select w.code, w.surname, w.fname, w.lname, w.birth, w.birth_rmk, w.fund, w.list_no, w.file_no, w.file_no_ext, w.type_r, w.type_i, w.type_a,
    bool_or(in_activity_t) as  in_activity_t, bool_or(in_org_t) as  in_org_t, bool_or(in_repress_t) as  in_repress_t, bool_or(in_repress_t_reab) as  in_repress_t_reab,
    bool_or(in_court_t) as  in_court_t, bool_or(in_impris_t) as  in_impris_t, bool_or(in_inform_t) as  in_inform_t, bool_or(in_family_t) as in_family_t, bool_or(in_mentioned_t) as in_mentioned_t,
    string_agg(distinct role,',') as family_role
    FROM (
    select t0.code, t0.surname, t0.fname, t0.lname, t0.birth, t0.birth_rmk, t0.fund, t0.list_no, t0.file_no, t0.file_no_ext, t0.type_r, t0.type_i, t0.type_a,
    CASE WHEN t1_co_perso.code IS NULL
            THEN false
            ELSE true
    END AS in_activity_t,
    CASE WHEN t2_co_perso.code IS NULL
            THEN false
            ELSE true
    END AS in_org_t,
    CASE WHEN t3_co_perso.code IS NOT NULL AND t3_co.cotype=1
    THEN true
    ELSE false
    END AS in_repress_t,
    CASE WHEN t3_co_perso.code IS NOT NULL AND t3_co.cotype=2
    THEN true
    ELSE false
    END AS in_repress_t_reab,
    CASE WHEN t4_co_perso.code IS NULL
            THEN false
            ELSE true
    END AS in_court_t,
    CASE WHEN t5_co_perso.code IS NULL
            THEN false
            ELSE true
    END AS in_impris_t,
    CASE WHEN t9_co_perso.code IS NULL
            THEN false
            ELSE true
    END AS in_inform_t,
    CASE WHEN t10_co_perso.code IS NULL
            THEN false
            ELSE true
    END AS in_family_t,
    CASE WHEN t11_co_perso.code IS NULL
            THEN false
            ELSE true
    END AS in_mentioned_t,
    t10_co.role
        from personal_t t0
    LEFT OUTER JOIN activity_t t1
        ON t0.code=t1.personal_code
    LEFT OUTER JOIN coact_t_f t1_co
        ON t1.id=t1_co.act_id
    LEFT OUTER JOIN personal_t_co t1_co_perso
        ON t1_co.human_code=t1_co_perso.code
    LEFT OUTER JOIN org_t t2
        ON t0.code=t2.personal_code
    LEFT OUTER JOIN coorg_t_f t2_co
        ON t2.id=t2_co.org_id
    LEFT OUTER JOIN personal_t_co t2_co_perso
        ON t2_co.human_code=t2_co_perso.code
    LEFT OUTER JOIN repress_t t3
        ON t0.code=t3.personal_code
    LEFT OUTER JOIN corep_t_f t3_co
        ON t3.id=t3_co.rep_id
    LEFT OUTER JOIN personal_t_co t3_co_perso
        ON t3_co.human_code=t3_co_perso.code
    LEFT OUTER JOIN court_t t4
        ON t3.id=t4.rep_id
    LEFT OUTER JOIN cocrt_t_f t4_co
        ON t4.id=t4_co.crt_id
    LEFT OUTER JOIN personal_t_co t4_co_perso
        ON t4_co.human_code=t4_co_perso.code
    LEFT OUTER JOIN impris_t t5
        ON t4.id=t5.crt_id
    LEFT OUTER JOIN coimp_t_f t5_co
        ON t5.id=t5_co.imp_id
    LEFT OUTER JOIN personal_t_co t5_co_perso
        ON t5_co.human_code=t5_co_perso.code
    LEFT OUTER JOIN inform_t_f t9_co
        ON t0.code=t9_co.personal_code
    LEFT OUTER JOIN personal_t_co t9_co_perso
        ON t9_co.human_code=t9_co_perso.code
    LEFT OUTER JOIN family_t_f t10_co
        ON t0.code=t10_co.personal_code
    LEFT OUTER JOIN personal_t_co t10_co_perso
        ON t10_co.human_code=t10_co_perso.code
    LEFT OUTER JOIN mentioned_t_f t11_co
        ON t0.code=t11_co.personal_code
    LEFT OUTER JOIN personal_t_co t11_co_perso
        ON t11_co.human_code=t11_co_perso.code
    where (t1_co_perso.code = ${code}) or (t2_co_perso.code = ${code}) or (t3_co_perso.code = ${code}) or (t4_co_perso.code = ${code}) or (t5_co_perso.code = ${code}) or(t9_co_perso.code = ${code}) or (t10_co_perso.code = ${code}) or (t11_co_perso.code = ${code})
    ) w
    GROUP BY w.code, w.surname, w.fname, w.lname, w.birth, w.birth_rmk, w.fund, w.list_no, w.file_no, w.file_no_ext, w.type_r, w.type_i, w.type_a;`;
      return await cntc.manyOrNone(sql);
    }
  } catch (err) {
    logError(err, getMentionedPers.name);
  }
};
