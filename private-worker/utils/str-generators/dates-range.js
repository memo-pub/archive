import { logError } from "../../helpers/log-error.js";
import { getRightDateFormat } from "./right-date-format.js";

const generateFirstDate = (dateUnknownSign, dateBegin1, dateEnd1) => {
  if (!dateBegin1) {
    return dateUnknownSign;
  }
  const hasAfter =
    dateBegin1?.substring(6) !== dateEnd1?.substring(6) ? "после " : "";

  return `${hasAfter}${getRightDateFormat(dateBegin1)}`;
};

const generateSecondDate = (dateUnknownSign, dateBegin2, dateEnd2) => {
  if (!dateBegin2) {
    return dateUnknownSign;
  }
  const dateEnd1Val = getRightDateFormat(dateBegin2);
  const dateEnd2Val = getRightDateFormat(dateEnd2);

  if (!dateEnd2) {
    return dateEnd1Val;
  }

  const isDifferentYear = dateBegin2?.substring(6) !== dateEnd2?.substring(6);
  const getDateFormat =
    dateEnd2Val !== dateEnd2?.substring(6)
      ? `до ${dateEnd2Val}`
      : `до ${+dateEnd2Val + 1}`;
  return isDifferentYear ? getDateFormat : dateEnd1Val;
};

export function generateDatesRange(
  dateUnknownSign = "?",
  dateBegin1,
  dateEnd1,
  dateBegin2,
  dateEnd2
) {
  try {
    const firstDate = generateFirstDate(dateUnknownSign, dateBegin1, dateEnd1);

    const secondDate = generateSecondDate(
      dateUnknownSign,
      dateBegin2,
      dateEnd2
    );

    if (firstDate === secondDate && firstDate !== "?") {
      return firstDate ?? "";
    }

    const datesRange =
      firstDate + `${firstDate && secondDate ? ` – ` : ""}` + secondDate;

    return datesRange.trim();
  } catch (err) {
    logError(err, generateDatesRange.name);
  }
}
