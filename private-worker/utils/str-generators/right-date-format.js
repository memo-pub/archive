export const getRightDateFormat = (date) => {
  if (!date) {
    return date;
  }
  const isRange = ["28.", "29.", "30.", "31.", "01"].some((s) =>
    date.startsWith(s)
  );

  return isRange ? date.substring(6) : date;
};
