import { connection as cntc } from "../../helpers/connect_db.js";
import { logError } from "../../helpers/log-error.js";
import { logMessage } from "../../helpers/log-message.js";
import { ENTITY_FIELDS } from "../../static/common-entity-fields.js";
import { ENTITIES_NAMES } from "../../static/entity-names.js";
import { TABLE_NAMES } from "../../static/table-names.js";
import { selectScript } from "../scripts/select.js";

const entityStrGenerators = {
  fond: async (fond) => `${fond.fond}`,
  opis: async (opis) => `${opis.fond}-${opis.opis_name}`,
  arch: async (archive) => {
    const opisInfo = await selectScript({
      cntc: cntc,
      tableName: TABLE_NAMES.OPIS,
      parameters: ["fond", "opis_name"],
      conditions: {
        id: `${archive.opis_id}`,
      },
      isStrictMode: true,
      isOrderBy: null,
      limit: null,
      isQuotesColumn: null,
    });

    const { delo_extra, delo } = archive;
    const { fond, opis_name } = opisInfo[0];

    return `${fond}-${opis_name}-${delo}${delo_extra ? `/${delo_extra}` : ""}`;
  },
  pers: async (person) => `${person.code}`,
};

export async function generateLegacyId(entityName, entity) {
  try {
    const selectedFunction = entityStrGenerators[entityName];

    if (selectedFunction) {
      const legacyIdString = await selectedFunction(entity);

      const {ARCH_FOLDER, FOND, OPIS} = ENTITIES_NAMES;

      const isArch = [ARCH_FOLDER, FOND, OPIS].includes(entityName);

      return `${ENTITY_FIELDS.ARCH_ID}-${
        isArch ? ENTITIES_NAMES.ARCH_FOLDER : entityName
      }-${legacyIdString}`;
    } else {
      logMessage("Invalid function parameter");
    }
  } catch (err) {
    logError(err, generateLegacyId.name);
  }
}
