export const cleanSequence = (tableName) => `WITH seq_id AS (
    SELECT pg_get_serial_sequence('${tableName}', 'id') AS sequence_name
  )
  SELECT setval(sequence_name, 1) FROM seq_id;`;
