import dotenv from 'dotenv';
dotenv.config();

export const connectionString = {
  host: process.env.DB_HOST,
  port: process.env.DB_PORT,
  database: process.env.DB_DATABASE,
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
};

export const serverConfig = {
  host: process.env.SERVER_HOST,
  port: process.env.SERVER_PORT,
  username: process.env.SERVER_USER,
  password: process.env.SERVER_PASSWORD,
};
