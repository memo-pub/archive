import cron from "node-cron";
import { connection as cntc } from "./helpers/connect_db.js";
import { sendFilesToPublicSiteServer } from "./helpers/file-sender.js";
import { archDescCSVGenerator } from "./csv-generators/arch-desc-update.js";
import { personalsCSVGenerator } from "./csv-generators/personals-update.js";
import { personalsToDelete } from "./csv-generators/personals-delete.js";
import { logError } from "./helpers/log-error.js";
import { archDescToDelete } from "./csv-generators/arch-desc-delete.js";
import { currentDate } from "./utils/global-vars/current-date.js";
import { ENTITIES_NAMES } from "./static/entity-names.js";
import { logMessage } from "./helpers/log-message.js";
import { deleteScript } from "./utils/scripts/delete.js";
import { TABLE_NAMES } from "./static/table-names.js";
import { generateLinkedArch } from "./csv-generators/arch-desc-link.js";
import { removeOldFiles } from "./helpers/remove-old-files.js";

const personalsCSVGenerateAndSend = async (currentDate) => {
  const hasPersonalsFile = await personalsCSVGenerator();

  if (hasPersonalsFile && currentDate) {
    await sendFilesToPublicSiteServer(
      `${process.env.PATH_TO_LOCAL_FILE + ENTITIES_NAMES.AUTH_REC}/` +
        `${currentDate}.csv`,
      process.env.REMOTE_PATH_PERS
    );
    logMessage("Personality file have been transferred.");
  }
};

const archDescCSVGenerateAndSend = async (currentDate) => {
  const hasArchiveFoldersFile = await archDescCSVGenerator();
  if (hasArchiveFoldersFile && currentDate) {
    await sendFilesToPublicSiteServer(
      process.env.PATH_TO_LOCAL_FILE + `${ENTITIES_NAMES.ARCH_DESC}/`,
      process.env.REMOTE_PATH_ARCHDESC
    );
    logMessage("Archive descriptions file have been transferred.");
  }
};

const authRecDelete = async (currentDate) => {
  const hasPersonalsFile = await personalsToDelete();
  if (hasPersonalsFile && currentDate) {
    await sendFilesToPublicSiteServer(
      `${process.env.PATH_TO_LOCAL_FILE + ENTITIES_NAMES.AUTH_REC_DEL}/` +
        `${currentDate}.csv`,
      process.env.DELETE_PERS_PATH
    );
    logMessage("Deleted personality file have been transferred.");
  }
};

const archDescDelete = async (currentDate) => {
  const hasArchiveFoldersFile = await archDescToDelete();
  if (hasArchiveFoldersFile && currentDate) {
    await sendFilesToPublicSiteServer(
      `${process.env.PATH_TO_LOCAL_FILE + ENTITIES_NAMES.ARCH_DESC_DEL}/` +
        `${currentDate}.csv`,
      process.env.DELETE_ARCHDESC_PATH
    );
    logMessage("Deleted archive descriptions file have been transferred.");
  }
};

const cleanTables = async () => {
  await deleteScript(cntc, TABLE_NAMES.DEL_ENTITY);
  await deleteScript(cntc, TABLE_NAMES.ARCH_DESC);
  await deleteScript(cntc, TABLE_NAMES.AUTH_REC);
};

const runApp = async () => {
  try {
    logMessage("Started CSV generator and sender.");
    const startTime = new Date().getTime();
    await authRecDelete(currentDate);
    await archDescDelete(currentDate);
    await personalsCSVGenerateAndSend(currentDate);
    await archDescCSVGenerateAndSend(currentDate);
    await cleanTables();
    await generateLinkedArch();
    removeOldFiles();
    const endTime = new Date().getTime();
    const executionTimeInSeconds = (endTime - startTime) / 1000;
    logMessage(`Execution time: ${executionTimeInSeconds} seconds`);
  } catch (err) {
    logError(err, runApp.name);
  }
};

// runApp();

cron.schedule("24 13 * * *", runApp, {
  timezone: "Europe/Paris",
});
