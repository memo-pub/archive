import { scp } from "scp2";
import { serverConfig } from "../config.js";
import { currentDate } from "../utils/global-vars/current-date.js";
import { logError } from "./log-error.js";

export const sendFilesToPublicSiteServer = async (
  pathToLocalFile,
  remotePath
) => {
  await scp(
    pathToLocalFile,
    {
      host: serverConfig.host,
      port: serverConfig.port,
      username: serverConfig.username,
      password: serverConfig.password,
      path: remotePath,
    },
    (err) => {
      if (err) {
        logError(err, sendFilesToPublicSiteServer.name);
      }
    }
  );
};
