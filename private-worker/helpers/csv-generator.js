import fs from "fs";
import path from "path";
import { currentDate } from "../utils/global-vars/current-date.js";
import { selectScript } from "../utils/scripts/select.js";
import { connection } from "./connect_db.js";
import { createObjectCsvWriter as createCsvWriter } from "csv-writer";
import { logError } from "./log-error.js";
import { TABLE_NAMES } from "../static/table-names.js";
import { logMessage } from "./log-message.js";

export async function generateCSVFile(
  pathToSaveFile,
  fileName,
  headers,
  fields
) {
  const csvWriter = createCsvWriter({
    path: pathToSaveFile + fileName,
    header: headers,
  });

  if (!fs.existsSync(pathToSaveFile)) {
    fs.mkdirSync(pathToSaveFile, { recursive: true });
  }

  try {
    await csvWriter.writeRecords(fields);
    logMessage("CSV file generated successfully.");
  } catch (error) {
    logError(error, generateCSVFile.name);
  }
}
