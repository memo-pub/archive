import { existsSync, mkdirSync, appendFile } from "fs";
import { dirname, resolve, join } from "path";
import { fileURLToPath } from "url";
import { currentDate } from "../utils/global-vars/current-date.js";

export const logMessage = async (message) => {
  console.log(message);
  const date = new Date().toISOString();
  const __filename = fileURLToPath(import.meta.url);
  const __dirname = dirname(__filename);
  const rootDir = resolve(__dirname, "../");

  const logsFolder = join(rootDir, "logs-message");
  const logFilePath = join(logsFolder, `${currentDate}.log`);

  const logMessage = `${date} - ${message}\n`;

  if (!existsSync(logsFolder)) {
    mkdirSync(logsFolder);
  }

  appendFile(logFilePath, logMessage, (err) => {
    if (err) {
      console.error("Message recording error:", err);
    }
  });
};
