import fs from "fs";
import path from "path";
import { logMessage } from "./log-message.js";
import { ENTITIES_NAMES } from "../static/entity-names.js";

function deleteFilesExceptToday(folderPath) {
  const today = new Date();
  const yyyy = today.getFullYear();
  const mm = String(today.getMonth() + 1).padStart(2, "0");
  const dd = String(today.getDate()).padStart(2, "0");
  const formattedToday = `${yyyy}-${mm}-${dd}`;

  if (dd === "01") {
    let filesCount = 0;

    fs.readdir(folderPath, (err, files) => {
      if (err) {
        console.error("Error reading folder content:", err);
        return;
      }

      files.forEach((file) => {
        const filePath = path.join(folderPath, file);
        const fileStat = fs.statSync(filePath);

        if (fileStat.isFile()) {
          const fileName = path.parse(file).name;
          if (!fileName.includes(formattedToday)) {
            fs.unlinkSync(filePath);
            filesCount++;
          }
        } else {
          const fileName = path.parse(file).name;
          if (!fileName.includes(formattedToday)) {
            fs.rmdirSync(filePath, { recursive: true });
            filesCount++;
          }
        }
      });
      logMessage(`${filesCount} old CSV files deleted in ${folderPath}.`);
    });
  }
}

export function removeOldFiles() {
  const foldersToProcess = [
    ENTITIES_NAMES.ARCH_DESC,
    ENTITIES_NAMES.ARCH_DESC_DEL,
    ENTITIES_NAMES.AUTH_REC,
    ENTITIES_NAMES.AUTH_REC_DEL,
    ENTITIES_NAMES.LINK,
  ];

  foldersToProcess.forEach((folderPath) => {
    deleteFilesExceptToday(`${process.env.PATH_TO_LOCAL_FILE + folderPath}/`);
  });
}
