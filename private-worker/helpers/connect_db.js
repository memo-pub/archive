import pgPromise from "pg-promise";
import { connectionString } from "../config.js";
import { logError } from "./log-error.js";
import { logMessage } from "./log-message.js";
const pgp = pgPromise();

export const connection = pgp(connectionString);

connection
  .connect()
  .then((obj) => {
    logMessage("Database connected");
    obj.done();
  })
  .catch((error) => {
    logError(error, 'pg-connection')
  });
