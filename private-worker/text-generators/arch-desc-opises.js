import { connection as cntc } from "../helpers/connect_db.js";
import { ENTITY_FIELDS } from "../static/common-entity-fields.js";
import { ENTITIES_NAMES } from "../static/entity-names.js";
import { DESC_LEVEL } from "../static/level-of-description.js";
import { TABLE_NAMES } from "../static/table-names.js";
import { generateLegacyId } from "../utils/str-generators/legacy-id.js";
import { curDateWithTime } from "../utils/global-vars/current-date.js";
import { insertScript } from "../utils/scripts/insert.js";
import { selectScript } from "../utils/scripts/select.js";
import { logError } from "../helpers/log-error.js";
import { logMessage } from "../helpers/log-message.js";

async function generateParentId(opis) {
  try {
    const fondId = await selectScript({
      cntc: cntc,
      tableName: TABLE_NAMES.FOND,
      parameters: ["fond"],
      conditions: {
        fond: opis.fond,
      },
      isStrictMode: true,
      isOrderBy: null,
      limit: null,
      isQuotesColumn: null,
    });

    return `${ENTITY_FIELDS.ARCH_ID}-${ENTITIES_NAMES.ARCH_FOLDER}-${fondId[0].fond}`;
  } catch (err) {
    logError(err, generateParentId.name);
  }
}

export const generateArchDescOpisesFields = async () => {
  const yesterdayDate = new Date(
    curDateWithTime.getTime() - 24 * 60 * 60 * 1000
  );

  try {
    const archOpises = await selectScript({
      cntc: cntc,
      tableName: TABLE_NAMES.ARCH_FOLD,
      parameters: ["DISTINCT opis_id"],
      conditions: {
        mustPublish: "*",
        "DATE(pub_change_date)": `${yesterdayDate.toISOString().split("T")[0]}`,
      },
      isStrictMode: true,
      isOrderBy: null,
      limit: null,
      isQuotesColumn: null,
    });

    const archOpisIds = archOpises.map((el) => +el.opis_id);

    const allOpises = await selectScript({
      cntc: cntc,
      tableName: TABLE_NAMES.OPIS,
      parameters: ["*"],
      conditions: {
        mustPublish: "*",
        "DATE(pub_change_date)": `${yesterdayDate.toISOString().split("T")[0]}`,
      },
      isStrictMode: true,
      isOrderBy: null,
      limit: null,
      isQuotesColumn: null,
    });

    const linkedOpises = archOpisIds.length
      ? await selectScript({
          cntc: cntc,
          tableName: TABLE_NAMES.OPIS,
          parameters: ["*"],
          conditions: {
            id: archOpisIds,
          },
          isStrictMode: false,
          isOrderBy: null,
          limit: null,
          isQuotesColumn: null,
        })
      : [];

    const filteredOpises = linkedOpises.filter(
      (linkOpis) => !allOpises.some((allOpis) => allOpis.id === linkOpis.id)
    );

    const opises = archOpisIds.length
      ? [...allOpises, ...filteredOpises]
      : [...allOpises];

    logMessage(`${opises?.length} opises to publish`);

    const hasOpisesToPublish = opises?.length !== 0;

    if (hasOpisesToPublish) {
      const archOpisIds = await selectScript({
        cntc: cntc,
        tableName: TABLE_NAMES.ARCH_FOLD,
        parameters: ["DISTINCT opis_id"],
        conditions: {
          mustPublish: "*",
          "DATE(pub_change_date)": `${
            yesterdayDate.toISOString().split("T")[0]
          }`,
        },
        isStrictMode: true,
        isOrderBy: null,
        limit: null,
        isQuotesColumn: null,
      });

      const archOpisArr = archOpisIds.map((item) => +item.opis_id);

      const archiveOpises = archOpisArr.length
        ? await selectScript({
            cntc: cntc,
            tableName: TABLE_NAMES.OPIS,
            parameters: ["*"],
            conditions: {
              id: archOpisArr,
            },
            isStrictMode: true,
            isOrderBy: null,
            limit: null,
            isQuotesColumn: null,
          })
        : [];

      archiveOpises.forEach((opis) => {
        const isFondExist = opises.some((el) => el.id === opis.id);
        if (!isFondExist) {
          opises.push(opis);
        }
      });

      for (const opis of opises) {
        const legacyIdString = await generateLegacyId(
          ENTITIES_NAMES.OPIS,
          opis
        );
        const parentIdString = await generateParentId(opis);

        const {
          doc_begin,
          doc_end,
          name,
          form_date,
          storage_capacity,
          arch_history,
          donation_source,
          description,
          storage_org,
          access_lim,
          help,
          admin_biog_history,
          archivist_rmk,
          descrip_date,
          fond,
          opis_name,
        } = opis;

        const fieldsToInsert = {
          identifier: legacyIdString,
          legacyId: legacyIdString,
          parentId: parentIdString,
          title: `${name ? name : ""}`,
          culture: ENTITY_FIELDS.CULTURE,
          eventTypes: ENTITY_FIELDS.EVENT_TYPES,
          eventDates: form_date ? `NULL|${form_date}` : "",
          eventStartDates: `${doc_begin ? `${doc_begin}-00-00|NULL` : ""}`,
          eventEndDates: `${doc_end ? `${doc_end}-00-00|NULL` : ""}`,
          eventActors: ENTITY_FIELDS.EVENT_ACTORS,
          extentAndMedium: storage_capacity ? storage_capacity : "",
          levelOfDescription: DESC_LEVEL.OPIS,
          repository: ENTITY_FIELDS.REPO,
          archivalHistory: arch_history ? arch_history : "",
          acquisition: donation_source ? donation_source : "",
          scopeAndContent: description ? description : "",
          arrangement: storage_org ? storage_org : "",
          accessConditions: access_lim ? access_lim : "",
          findingAids: help ? help : "",
          generalNote: admin_biog_history ? admin_biog_history : "",
          rules: ENTITY_FIELDS.RULES,
          archivistNote: archivist_rmk ? archivist_rmk : "",
          revisionHistory: descrip_date ? descrip_date : "",
          alternativeIdentifiers: ` Ф. ${fond}. Оп. ${opis_name}.|ID-arch-${fond}-${opis_name}`,
          alternativeIdentifierLabels: "Архивный шифр|Global identifier",
        };

        const columnNames = [];
        const valuesToInsert = [];

        Object.keys(fieldsToInsert).forEach((key) => {
          columnNames.push(key);
          valuesToInsert.push(fieldsToInsert[key]);
        });

        await insertScript(
          cntc,
          TABLE_NAMES.ARCH_DESC,
          columnNames,
          valuesToInsert
        );
      }
    } else {
      logMessage("There is no suitable opis with these parameters.");
    }
    return hasOpisesToPublish;
  } catch (err) {
    logError(err, generateArchDescOpisesFields.name);
  }
};
