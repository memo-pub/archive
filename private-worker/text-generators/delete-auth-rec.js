import { connection as cntc } from "../helpers/connect_db.js";
import { ENTITIES_NAMES } from "../static/entity-names.js";
import { TABLE_NAMES } from "../static/table-names.js";
import { generateLegacyId } from "../utils/str-generators/legacy-id.js";
import { curDateWithTime } from "../utils/global-vars/current-date.js";
import { insertScript } from "../utils/scripts/insert.js";
import { selectScript } from "../utils/scripts/select.js";
import { logMessage } from "../helpers/log-message.js";

export const deleteListAuthRec = async () => {
  const yesterdayDate = new Date(
    curDateWithTime.getTime() - 24 * 60 * 60 * 1000
  );

  const personals = await selectScript({
    cntc: cntc,
    tableName: "personal_t",
    parameters: ["*"],
    conditions: {
      mustUnpublish: "*",
      'DATE(pub_change_date)': `${yesterdayDate.toISOString().split("T")[0]}`,
    },
    isStrictMode: true,
    isOrderBy: null,
    limit: null,
    isQuotesColumn: null,
  });

  logMessage(`${personals?.length} authority records to delete`);

  const hasArchDescToDelete = personals?.length !== 0;

  if (hasArchDescToDelete) {
    for (const person of personals) {
      const legacyIdString = await generateLegacyId(
        ENTITIES_NAMES.PERS,
        person
      );

      await insertScript(
        cntc,
        TABLE_NAMES.DEL_ENTITY,
        [`legacyId`],
        [legacyIdString]
      );
    }
  }
  return hasArchDescToDelete;
};
