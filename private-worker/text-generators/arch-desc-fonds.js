import { connection as cntc } from "../helpers/connect_db.js";
import { ENTITY_FIELDS } from "../static/common-entity-fields.js";
import { ENTITIES_NAMES } from "../static/entity-names.js";
import { DESC_LEVEL } from "../static/level-of-description.js";
import { TABLE_NAMES } from "../static/table-names.js";
import { generateLegacyId } from "../utils/str-generators/legacy-id.js";
import { curDateWithTime } from "../utils/global-vars/current-date.js";
import { insertScript } from "../utils/scripts/insert.js";
import { selectScript } from "../utils/scripts/select.js";
import { logError } from "../helpers/log-error.js";
import { logMessage } from "../helpers/log-message.js";

export const generateArchDescFondsFields = async () => {
  const yesterdayDate = new Date(
    curDateWithTime.getTime() - 24 * 60 * 60 * 1000
  );

  try {
    const oldPubFonds = await selectScript({
      cntc: cntc,
      tableName: TABLE_NAMES.FOND,
      parameters: ["*"],
      conditions: {
        published: "*",
      },
      isStrictMode: true,
      isOrderBy: null,
      limit: null,
      isQuotesColumn: null,
    });

    const newPubFonds = await selectScript({
      cntc: cntc,
      tableName: TABLE_NAMES.FOND,
      parameters: ["*"],
      conditions: {
        mustPublish: "*",
        "DATE(pub_change_date)": `${yesterdayDate.toISOString().split("T")[0]}`,
      },
      isStrictMode: true,
      isOrderBy: null,
      limit: null,
      isQuotesColumn: null,
    });

    newPubFonds.forEach((fond) => {
      const isFondExist = oldPubFonds.some((el) => el.id === fond.id);
      if (!isFondExist) {
        oldPubFonds.push(fond);
      }
    });

    const fonds = [...oldPubFonds];

    logMessage(`${fonds?.length} fonds to publish`);

    const hasFondsToPublish = fonds?.length !== 0;

    if (hasFondsToPublish) {
      const archOpisIds = await selectScript({
        cntc: cntc,
        tableName: TABLE_NAMES.ARCH_FOLD,
        parameters: ["DISTINCT opis_id"],
        conditions: {
          mustPublish: "*",
          "DATE(pub_change_date)": `${
            yesterdayDate.toISOString().split("T")[0]
          }`,
        },
        isStrictMode: true,
        isOrderBy: null,
        limit: null,
        isQuotesColumn: null,
      });

      const archOpisArr = archOpisIds.map((item) => +item.opis_id);

      if (archOpisArr.length) {
        const fondName = await selectScript({
          cntc: cntc,
          tableName: TABLE_NAMES.OPIS,
          parameters: ["DISTINCT fond"],
          conditions: {
            id: archOpisArr,
          },
          isStrictMode: true,
          isOrderBy: null,
          limit: null,
          isQuotesColumn: null,
        });

        const fondNameArr = fondName.map((item) => +item.fond);

        const archiveFunds = await selectScript({
          cntc: cntc,
          tableName: TABLE_NAMES.FOND,
          parameters: ["*"],
          conditions: {
            fond: fondNameArr,
          },
          isStrictMode: true,
          isOrderBy: null,
          limit: null,
          isQuotesColumn: null,
        });

        archiveFunds.forEach((fond) => {
          const isFondExist = fonds.some((el) => el.id === fond.id);
          if (!isFondExist) {
            fonds.push(fond);
          }
        });
      }
      for (const fond of fonds) {
        const legacyIdString = await generateLegacyId(
          ENTITIES_NAMES.FOND,
          fond
        );

        const {
          doc_begin,
          doc_end,
          description,
          form_date,
          storage_capacity,
          arch_history,
          donation_source,
          description_long,
          mat_organ_system,
          access_lim,
          help,
          admin_biog_history,
          archivist_rmk,
          descrip_date,
        } = fond;

        const fieldsToInsert = {
          identifier: legacyIdString,
          legacyId: legacyIdString,
          title: `${description ? description : ""}`,
          culture: ENTITY_FIELDS.CULTURE,
          eventTypes: ENTITY_FIELDS.EVENT_TYPES,
          eventDates: form_date ? `NULL|${form_date}` : "",
          eventStartDates: `${doc_begin ? `${doc_begin}-00-00|NULL` : ""}`,
          eventEndDates: `${doc_end ? `${doc_end}-00-00|NULL` : ""}`,
          eventActors: ENTITY_FIELDS.EVENT_ACTORS,
          extentAndMedium: storage_capacity ? storage_capacity : "",
          levelOfDescription: DESC_LEVEL.FOND,
          repository: ENTITY_FIELDS.REPO,
          archivalHistory: arch_history ? arch_history : "",
          acquisition: donation_source ? donation_source : "",
          scopeAndContent: description_long ? description_long : "",
          arrangement: mat_organ_system ? mat_organ_system : "",
          accessConditions: access_lim ? access_lim : "",
          findingAids: help ? help : "",
          generalNote: admin_biog_history ? admin_biog_history : "",
          rules: ENTITY_FIELDS.RULES,
          archivistNote: archivist_rmk ? archivist_rmk : "",
          revisionHistory: descrip_date ? descrip_date : "",
          alternativeIdentifiers: ` Ф. ${fond.fond}.|ID-arch-${fond.fond}`,
          alternativeIdentifierLabels: "Архивный шифр|Global identifier",
        };

        const columnNames = [];
        const valuesToInsert = [];

        Object.keys(fieldsToInsert).forEach((key) => {
          columnNames.push(key);
          valuesToInsert.push(fieldsToInsert[key]);
        });

        await insertScript(
          cntc,
          TABLE_NAMES.ARCH_DESC,
          columnNames,
          valuesToInsert
        );
      }
    } else {
      logMessage("There is no suitable fund with these parameters.");
    }
    return hasFondsToPublish;
  } catch (err) {
    logError(err, generateArchDescFondsFields.name);
  }
};
