import { connection as cntc } from "../helpers/connect_db.js";
import { ENTITIES_NAMES } from "../static/entity-names.js";
import { TABLE_NAMES } from "../static/table-names.js";
import { generateLegacyId } from "../utils/str-generators/legacy-id.js";
import { curDateWithTime } from "../utils/global-vars/current-date.js";

import { insertScript } from "../utils/scripts/insert.js";
import { selectScript } from "../utils/scripts/select.js";
import { logError } from "../helpers/log-error.js";
import { cleanSequence } from "../utils/str-generators/clean-seq.js";
import { logMessage } from "../helpers/log-message.js";
import { getMentionedPers } from "../utils/scripts/get-ment-pers.js";
import { generateDatesRange } from "../utils/str-generators/dates-range.js";
import { getRightDateFormat } from "../utils/str-generators/right-date-format.js";
import { SOUR_TYPES_EXEPT } from "../static/sour-types.js";

async function generateAuthorizedFormOfName(person) {
  const {
    surname,
    fname,
    lname,
    fname_rmk,
    real_surname_rmk,
    lname_rmk,
    birth_begin_char,
    birth_end_char,
    death_end_char,
    death_begin_char,
  } = person;

  const name = `${surname ? `${surname} ` : ""}${fname ? `${fname} ` : ""}${
    lname ? `${lname} ` : ""
  }${
    real_surname_rmk || fname_rmk || lname_rmk
      ? `(${`${real_surname_rmk ? `${real_surname_rmk} ` : ""}${
          fname_rmk ? `${fname_rmk} ` : ""
        }${lname_rmk ? `${lname_rmk}` : ""}`.trim()})`
      : ""
  }`;

  const dates = generateDatesRange(
    "?",
    birth_begin_char,
    birth_end_char,
    death_begin_char,
    death_end_char
  );

  return name + " " + `(${dates})`;
}

async function generateDatesOfExistence(person) {
  const { birth_begin_char, death_end_char, birth_end_char, death_begin_char } =
    person;

  const isSameBirthYear =
    birth_begin_char?.substring(6) === birth_end_char?.substring(6);

  const isSameDeathYear =
    death_end_char?.substring(6) === death_begin_char?.substring(6);

  return `${
    birth_begin_char
      ? `род. ${isSameBirthYear ? "" : "после "}${getRightDateFormat(
          birth_begin_char
        )} `
      : "род. ? "
  }${`–`}${
    death_end_char
      ? ` ум. ${isSameDeathYear ? "" : "до "}${getRightDateFormat(
          death_end_char
        )}`
      : " ум. ?"
  }`;
}

async function generateHistory(person) {
  try {
    const fodSet = new Set();

    fodSet.add({
      fond: person.fund,
      opis: person.list_no,
      delo: person.file_no,
      delo_extra: person.file_no_ext,
    });

    const birthPlaceInfo = await selectScript({
      cntc: cntc,
      tableName: "place_t",
      parameters: ["*"],
      conditions: {
        place_num: person.birth_place_num,
        cur_mark: "*",
      },
      isStrictMode: true,
      isOrderBy: null,
      limit: null,
      isQuotesColumn: null,
    });

    const activityInfo = await selectScript({
      cntc: cntc,
      tableName: "activity_t",
      parameters: [
        "act_no",
        "ent1_rmk",
        "prof",
        "post",
        "TO_CHAR(arrival_dat_begin, 'DD.MM.YYYY') as arrival_dat_begin",
        "TO_CHAR(depart_dat_begin, 'DD.MM.YYYY') as depart_dat_begin",
      ],
      conditions: {
        personal_code: person.code,
      },
      isStrictMode: true,
      isOrderBy: null,
      limit: null,
      isQuotesColumn: null,
    });

    const repressionInfo = await selectScript({
      cntc: cntc,
      tableName: "repress_t t1",
      parameters: [
        "t1.*",
        "TO_CHAR(repress_dat_begin, 'DD.MM.YYYY') as repress_dat_begin_char",
        "TO_CHAR(repress_dat_end, 'DD.MM.YYYY') as repress_dat_end_char",
        "TO_CHAR(reabil_dat_begin, 'DD.MM.YYYY') as reabil_dat_begin_char",
      ],
      conditions: {
        personal_code: person.code,
      },
      isStrictMode: true,
      isOrderBy: null,
      limit: null,
      isQuotesColumn: null,
    });

    const courtInfo = await selectScript({
      cntc: cntc,
      tableName: "court_t t1",
      parameters: [
        "t1.*",
        "TO_CHAR(sentense_date_begin, 'DD.MM.YYYY') as sentense_date_begin_char",
      ],
      conditions: {
        personal_code: person.code,
      },
      isStrictMode: true,
      isOrderBy: "trial_no",
      limit: null,
      isQuotesColumn: null,
    });

    const generateActivityInfoStr = async () => {
      try {
        if (!activityInfo.length) return "";

        let str = "";

        const sortedActivityInfo = activityInfo.sort(
          (a, b) => a.act_no - b.act_no
        );

        for (const activity of sortedActivityInfo) {
          const { ent1_rmk, prof, post, arrival_dat_begin, depart_dat_begin } =
            activity;

          str += `\n\u2022 ${ent1_rmk ? `${ent1_rmk} ` : ""}${
            post ? `(${post}` : ""
          }${post && prof ? " / " : ""}${!prof && post ? "). " : ""}${
            !post && prof ? "(" : ""
          }${prof ? `${prof}). ` : ""}${
            arrival_dat_begin
              ? `Дата поступления: ${getRightDateFormat(arrival_dat_begin)}. `
              : ""
          }${
            depart_dat_begin
              ? `Дата увольнения: ${getRightDateFormat(depart_dat_begin)}; `
              : ""
          }`;
        }

        return `\n\n**Учеба и профессиональная деятельность**: ${str}`;
      } catch (err) {
        logError(err, generateActivityInfoStr.name);
      }
    };

    const generateCourtInfoStr = async (repId, repress_no) => {
      try {
        let str = "";

        for (const cour of courtInfo) {
          if (cour.rep_id === repId) {
            const imprisInfo = await selectScript({
              cntc: cntc,
              tableName: "impris_t t1",
              parameters: [
                "t1.*",
                "TO_CHAR(arrival_dat_begin, 'DD.MM.YYYY') as arr_dat_begin",
                "TO_CHAR(arrival_dat_end, 'DD.MM.YYYY') as arr_dat_end",
                "TO_CHAR(leave_dat_begin, 'DD.MM.YYYY') as lv_dat_begin",
                "TO_CHAR(leave_dat_end, 'DD.MM.YYYY') as lv_dat_end",
              ],
              conditions: {
                crt_id: cour.id,
              },
              isStrictMode: true,
              isOrderBy: null,
              limit: null,
              isQuotesColumn: null,
            });

            const imprisTypes = await selectScript({
              cntc: cntc,
              tableName: "list_of_t",
              parameters: ["*"],
              conditions: {
                name_f: "IMPRIS_TYPE",
              },
              isStrictMode: true,
              isOrderBy: null,
              limit: null,
              isQuotesColumn: null,
            });

            const leaveReasonTypes = await selectScript({
              cntc: cntc,
              tableName: "list_of_t",
              parameters: ["*"],
              conditions: {
                name_f: "REASON",
              },
              isStrictMode: true,
              isOrderBy: null,
              limit: null,
              isQuotesColumn: null,
            });

            const placeTypes = await selectScript({
              cntc: cntc,
              tableName: "place_t",
              parameters: ["*"],
              conditions: {
                cur_mark: "*",
              },
              isStrictMode: true,
              isOrderBy: null,
              limit: null,
              isQuotesColumn: null,
            });

            const findImpName = (id) => {
              return imprisTypes.find((el) => el.id === id).name_ent;
            };

            const generatePlaceName = (id) => {
              const placeType = placeTypes.find((el) => el.place_num === id);

              return `${placeType ? ` ${placeType.place_name}` : ""}`;
            };

            const leaveReason = (id) => {
              const reasonType = leaveReasonTypes.find((el) => el.id === id);

              return `${
                reasonType?.name_ent
                  ? ` Причина убытия: ${reasonType.name_ent}. `
                  : ""
              }`;
            };

            const getImpris = (rep_no, cour_no) => {
              let hstr = "";

              for (const imp of imprisInfo) {
                const {
                  impris_no,
                  impris_type_id,
                  prison_name,
                  place_num,
                  prison_place_rmk,
                  arr_dat_begin,
                  arr_dat_end,
                  lv_dat_begin,
                  lv_dat_end,
                  reason_id,
                  general_rmk,
                } = imp;

                hstr += `${
                  imprisInfo.length >= 1
                    ? `\n        ${rep_no ? rep_no : ""}${
                        cour_no ? cour_no : ""
                      }${impris_no.slice(0, -1)} `
                    : ""
                }`;

                const impStr = `${
                  impris_type_id
                    ? `Заключение: ${findImpName(impris_type_id)},`
                    : ""
                }${prison_name ? ` ${prison_name}` : ""}${generatePlaceName(
                  place_num
                )}${prison_place_rmk ? ` (${prison_place_rmk})` : ""}${
                  place_num && !prison_place_rmk ? " " : ""
                }`;

                hstr += impStr.trim();

                const impDates = generateDatesRange(
                  "",
                  arr_dat_begin,
                  arr_dat_end,
                  lv_dat_begin,
                  lv_dat_end
                );

                hstr += `${impDates.trim() ? ` ${impDates}.` : ""}${leaveReason(
                  reason_id
                )}${general_rmk ? ` ${general_rmk}.` : ""}`;
              }
              return hstr;
            };

            const {
              trial_no,
              inq_name,
              court_name,
              inq_name_rmk,
              court_name_rmk,
              article,
              article_rmk,
              sentense,
              sentense_rmk,
              sentense_date_begin_char,
            } = cour;

            const generateInqAndCourt = () => {
              const inqValue = `${
                inq_name ? `Следственный орган: ${inq_name}` : ""
              }${inq_name ? ". " : ""}${inq_name_rmk ?? ""}${
                inq_name_rmk ? ". " : ""
              }`.trim();

              const courtValue = `${
                court_name ? `Судебный орган: ${court_name}` : ""
              }${court_name ? ". " : ""}${court_name_rmk ?? ""}${
                court_name_rmk ? ". " : ""
              }`.trim();

              return inqValue || courtValue
                ? inqValue + courtValue + " "
                : `Следствие и суд: информации нет. `;
            };

            const generateArticle = () => {
              const articleValue = `${article ?? ""}`;
              const articleRmkValue = `${article_rmk ?? ""}`;
              const articleFullValue = articleValue + " " + articleRmkValue;

              return articleValue || articleRmkValue
                ? `Обвинение: ` + articleFullValue.trim()
                : "";
            };

            const generateSentense = () => {
              const sentenseValue = `${
                sentense ? `Приговор: ${sentense}` : ""
              }`;
              const sentenseRmkValue = `${sentense_rmk ?? ""}`;

              const sentenseFullValue = [sentenseValue, sentenseRmkValue]
                .filter((str) => str !== "")
                .join(", ");

              return sentenseValue || sentenseRmkValue
                ? sentenseFullValue.trim()
                : "";
            };

            const generateSentenseDate = () =>
              `${
                sentense_date_begin_char
                  ? `Дата вынесения приговора: ${getRightDateFormat(
                      sentense_date_begin_char
                    )}`
                  : ""
              }`;

            const inqAndCourtStr = generateInqAndCourt();

            const articleStr = generateArticle();

            const sentenseStr = generateSentense();

            const sentenseDateDate = generateSentenseDate();

            const courInfo = [articleStr, sentenseStr, sentenseDateDate]
              .filter((str) => str !== "")
              .join(". ");

            str += `${
              courtInfo.length >= 1
                ? `\n    ${
                    repress_no ? `${repress_no.slice(0, -1)}` : ""
                  }${trial_no.slice(0, -1)} `
                : ""
            }${inqAndCourtStr}${
              courInfo.length ? courInfo + ". " : ""
            }${getImpris(repress_no.slice(0, -1), trial_no.slice(0, -1))} `;
          }
        }
        return str;
      } catch (err) {
        logError(err, generateCourtInfoStr.name);
      }
    };

    const generateRepressionInfoStr = async () => {
      try {
        if (!repressionInfo.length) return "";

        let str = "";

        const sortedRepressionInfo = repressionInfo.sort(
          (a, b) => a.repress_no - b.repress_no
        );

        for (const rep of sortedRepressionInfo) {
          const repressNum = await selectScript({
            cntc: cntc,
            tableName: "place_t",
            parameters: ["*"],
            conditions: {
              place_num: rep.place_num,
              cur_mark: "*",
            },
            isStrictMode: true,
            isOrderBy: null,
            limit: null,
            isQuotesColumn: null,
          });

          const repressionTypes = await selectScript({
            cntc: cntc,
            tableName: "list_of_t",
            parameters: ["*"],
            conditions: {
              name_f: "REPRESS_TYPE",
              id: rep.repress_type_id,
            },
            isStrictMode: true,
            isOrderBy: null,
            limit: null,
            isQuotesColumn: null,
          });

          const rehabilReason = await selectScript({
            cntc: cntc,
            tableName: "rehabil_reason_t",
            parameters: ["reason_id"],
            conditions: {
              rep_id: rep.id,
            },
            isStrictMode: true,
            isOrderBy: null,
            limit: null,
            isQuotesColumn: null,
          });

          const generateRehabilReasonStr = async () => {
            let hstr = ``;

            if (!!rehabilReason.length) {
              hstr += `основание реабилитации: `;
            }

            for (const res of rehabilReason) {
              const rehabilReasonsTypes = await selectScript({
                cntc: cntc,
                tableName: "list_of_t",
                parameters: ["*"],
                conditions: {
                  name_f: "REHAB_REAS",
                  id: res.reason_id,
                },
                isStrictMode: true,
                isOrderBy: null,
                limit: null,
                isQuotesColumn: null,
              });

              rehabilReasonsTypes.forEach(
                (r) => (hstr += `${r.name_ent.trim()}, `)
              );
            }
            return hstr.substring(0, hstr.length - 2).trim();
          };

          const repActivity = await selectScript({
            cntc: cntc,
            tableName: "activity_t",
            parameters: ["*"],
            conditions: {
              personal_code: rep.personal_code,
              rep_id: rep.id,
            },
            isStrictMode: true,
            isOrderBy: null,
            limit: null,
            isQuotesColumn: null,
          });

          const { repress_dat_begin_char, repress_dat_end_char } = rep;

          const getRepressDates = (date1, date2) => {
            if (!date1 && !date2) return "";

            const year1 = date1.substring(6);
            const year2 = date2.substring(6);

            const isMonthEnd = ["28.", "29.", "30.", "31."].some((s) =>
              date2.startsWith(s)
            );

            const isRange = date1.startsWith("01.") && isMonthEnd;

            const checkYear =
              year1 === year2
                ? year1
                : `${year1} – ${getRightDateFormat(date2)}`;
            const checkFullDate =
              date1 === date2
                ? date1
                : `${date1} – ${getRightDateFormat(date2)}`;

            return isRange ? checkYear : checkFullDate;
          };

          const repressDates = getRepressDates(
            repress_dat_begin_char,
            repress_dat_end_char
          );

          const hasRehabInfo = rep.reabil_mark && rep.reabil_dat_begin_char;

          const getRehabStr = async () => {
            const rehabDate = `${
              rep.reabil_dat_begin_char
                ? `Реабилитация: ${getRightDateFormat(
                    rep.reabil_dat_begin_char
                  )}`
                : ""
            }`;

            const rehabOrg = `${
              rep.rehabil_org ? `реабилитирующий орган: ${rep.rehabil_org}` : ""
            }`;

            const rehabReason = !!rehabilReason.length
              ? await generateRehabilReasonStr()
              : "";

            const rehabInfo = [rehabDate, rehabOrg, rehabReason]
              .filter((str) => str !== "")
              .join(", ");

            return hasRehabInfo
              ? `${rehabInfo}. `
              : `Сведений о реабилитации нет. `;
          };

          const generateRepActivity = () => {
            const firstPost = repActivity[0]?.post || "";
            const firstPostRmk = repActivity[0]?.ent1_rmk || "";

            const combinedActivity = [
              firstPost,
              firstPost
                ? firstPostRmk
                  ? `(${firstPostRmk})`
                  : ""
                : firstPostRmk ?? "",
            ]
              .filter((str) => str !== "")
              .join(" ");

            return combinedActivity
              ? ` Место работы: ${combinedActivity}.`
              : "";
          };

          const generateRepPlace = () => {
            const placeName = repressNum[0]?.place_name || "";
            const placeNameRmk = rep.geoplace_code_rmk || "";

            const combinedPlaceName = [
              placeName,
              placeName
                ? placeNameRmk
                  ? `(${placeNameRmk})`
                  : ""
                : placeNameRmk ?? "",
            ]
              .filter((str) => str !== "")
              .join(" ");

            return combinedPlaceName
              ? ` Место жительства на момент ареста: ${combinedPlaceName}.`
              : "";
          };

          const repressNo = `${
            rep.repress_no ? `\n ${rep.repress_no.slice(0, -1)} ` : ""
          }`;

          const repressPlace =
            repressNum[0]?.place_name || rep.geoplace_code_rmk
              ? generateRepPlace()
              : "";

          const repressActive = !!repActivity.length
            ? generateRepActivity()
            : "";

          const rehab = (await getRehabStr()).trim();

          const court = courtInfo
            ? `${await generateCourtInfoStr(rep.id, rep.repress_no)}`
            : "";

          const repressRmk =
            repressionTypes[0]?.name_rmk && repressDates
              ? `${repressionTypes[0]?.name_rmk}: `
              : "";

          const dates = repressDates?.length ? `${repressDates}. ` : "";

          str += `${repressNo}${repressRmk}${dates}${rehab}${repressPlace}${repressActive}${court}`;
        }

        return ` \n\n**Сведения о репрессиях**: ${str} `;
      } catch (err) {
        logError(err, generateRepressionInfoStr.name);
      }
    };

    const generateFODStr = async (code) => {
      let str = `\n\n**Упоминается в делах архива:**`;
      try {
        const pers_FOD = await selectScript({
          cntc: cntc,
          tableName: "personal_t",
          parameters: ["fund", "list_no", "file_no", "file_no_ext"],
          conditions: {
            code: code,
          },
          isStrictMode: true,
          isOrderBy: null,
          limit: null,
          isQuotesColumn: null,
        });

        const pers_extra_shifr = await selectScript({
          cntc: cntc,
          tableName: "personal_shifr_t",
          parameters: ["fund", "list_no", "file_no", "file_no_ext"],
          conditions: {
            personal_code: code,
          },
          isStrictMode: true,
          isOrderBy: "id",
          limit: null,
          isQuotesColumn: null,
        });

        const pers_sour = await selectScript({
          cntc: cntc,
          tableName: "personal_sour_t",
          parameters: ["sour_id"],
          conditions: {
            personal_code: code,
          },
          isStrictMode: true,
          isOrderBy: null,
          limit: null,
          isQuotesColumn: null,
        });

        const sourIds = pers_sour?.map((sour) => +sour.sour_id);

        const sourFOD = sourIds.length
          ? await selectScript({
              cntc: cntc,
              tableName: "sour_t",
              parameters: [
                "DISTINCT fund",
                "list_no",
                "file_no",
                "file_no_ext",
              ],
              conditions: {
                id: sourIds,
              },
              isStrictMode: false,
              isOrderBy: null,
              limit: null,
              isQuotesColumn: null,
            })
          : [];

        const mpers = await getMentionedPers(cntc, code);

        const pers_all_FODs = [
          ...pers_FOD,
          ...pers_extra_shifr,
          ...sourFOD,
          ...mpers,
        ];

        pers_all_FODs.reduce((acc, pers) => {
          const { fund, list_no, file_no, file_no_ext } = pers;
          const isEntityNotNull = fund && list_no && file_no;
          if (
            !isEntityNotNull ||
            acc.some(
              (el) =>
                el.fund === fund &&
                el.list_no === list_no &&
                el.file_no === file_no
            )
          ) {
            return acc;
          } else {
            str += `\n\u2022 Ф. ${fund}. Оп. ${list_no}. Д. ${file_no}${
              file_no_ext ? `/${file_no_ext}` : ""
            }`;
            return [...acc, pers];
          }
        }, []);

        return pers_all_FODs?.length !== 0 ? str : "";
      } catch (err) {
        logError(err, generateFODStr.name);
      }
    };

    const generateSoursInfo = async () => {
      try {
        let str = `**Упоминается в документах архива:**`;

        const personalSourTInfo = await selectScript({
          cntc: cntc,
          tableName: "personal_sour_t",
          parameters: ["sour_id"],
          conditions: {
            personal_code: person.code,
          },
          isStrictMode: true,
          isOrderBy: null,
          limit: null,
          isQuotesColumn: null,
        });

        const sourIdsList = personalSourTInfo.map((el) => +el.sour_id);

        const soursInfo = sourIdsList.length
          ? await selectScript({
              cntc: cntc,
              tableName: "sour_t t1",
              parameters: [
                "t1.*",
                "TO_CHAR(sour_date_begin, 'DD.MM.YYYY') as sour_date_begin_char",
              ],
              conditions: {
                id: sourIdsList,
              },
              isStrictMode: false,
              isOrderBy: "sour_no",
              limit: null,
              isQuotesColumn: null,
            })
          : [];

        const findSourType = async (sour_id) => {
          let hstr = ``;

          const sourInfoName =
            await cntc.manyOrNone(`with sour_id as (select sour_type_id from sour_type_t where sour_id=${sour_id})
          select name_ent, name_rmk from list_of_t t1, sour_id t2 where t1.name_f like 'SOUR_TYPE' and t1.id=t2.sour_type_id;`);

          if (sourInfoName[0]?.name_rmk) {
            hstr += `(${sourInfoName[0].name_rmk})`;
          }

          return hstr;
        };

        if (soursInfo.length) {
          for (const sour of soursInfo) {
            const {
              id,
              sour_name,
              sour_date_begin_char,
              fund,
              list_no,
              file_no,
              file_no_ext,
            } = sour;

            const sourType = await findSourType(id);

            const getSourTypeStr = () => {
              try {
                const sour_exept_uppercase = SOUR_TYPES_EXEPT.map((sour) =>
                  sour.toUpperCase()
                );
                const isExeption = sour_exept_uppercase.includes(
                  sour_name?.toUpperCase().trim()
                );

                if (sourType.includes("undefined")) {
                  logMessage(`undef sour in pers: ${person.code}`);
                }
                return isExeption ? ` ${sourType}. ` : ". ";
              } catch (err) {
                logError(err, getSourTypeStr.name);
              }
            };

            str += `\n\u2022 ${sour_name ? `${sour_name}` : ""}${
              sour_name ? getSourTypeStr() : ""
            }${
              sour_date_begin_char
                ? `Дата создания: ${getRightDateFormat(sour_date_begin_char)}. `
                : ""
            }Ф. ${fund}${list_no ? `. Оп. ${list_no}` : ""}${
              file_no ? `. Д. ${file_no}` : ""
            }${file_no_ext ? `/${file_no_ext}` : ""}. Номер документа: ${id}`;
          }
        }
        return soursInfo.length ? str : "";
      } catch (err) {
        logError(err, generateSoursInfo.name);
      }
    };

    const generateNation = async (natId) => {
      if (!natId) return "";

      const nation = await selectScript({
        cntc: cntc,
        tableName: "nat_t",
        parameters: ["*"],
        conditions: {
          id: natId,
        },
        isStrictMode: true,
        isOrderBy: null,
        limit: null,
        isQuotesColumn: null,
      });

      const nation_rmk = nation[0]?.nation_rmk;

      if (nation_rmk) {
        return `\n\n**Национальность (как указана в документах)**: ${nation_rmk}`;
      }
    };

    const { birth_place_rmk, public_rmk, code, nation_id } = person;

    const nation = await generateNation(nation_id);
    const activity = await generateActivityInfoStr();
    const repress = await generateRepressionInfoStr();
    const fod = await generateFODStr(code);
    const sours = await generateSoursInfo();

    const birthPlaceStr = `${
      birthPlaceInfo.length || birth_place_rmk ? `**Место рождения**: ` : ""
    }${
      birthPlaceInfo[0]?.place_name ? `${birthPlaceInfo[0]?.place_name} ` : ""
    }${birth_place_rmk ? `(${birth_place_rmk}) ` : ""}`;

    const remark = `${public_rmk ? `\n\n*${public_rmk}*` : ""}`;

    return `${birthPlaceStr}${remark}${nation}${activity}${repress}${fod}\n\n${sours}`;
  } catch (err) {
    logError(err, generateHistory.name);
  }
}

async function generatePlaces(person) {
  try {
    let str = ``;
    const generateRepPlaces = async () => {
      let hstr = ``;

      const rep_places = await selectScript({
        cntc: cntc,
        tableName: "repress_t",
        parameters: ["place_num", "repress_type_rmk", "geoplace_code_rmk"],
        conditions: {
          personal_code: person.code,
        },
        isStrictMode: true,
        isOrderBy: null,
        limit: null,
        isQuotesColumn: null,
      });

      for (const place of rep_places) {
        const place_type = await selectScript({
          cntc: cntc,
          tableName: "place_t",
          parameters: ["*"],
          conditions: {
            place_num: place.place_num,
          },
          isStrictMode: true,
          isOrderBy: null,
          limit: null,
          isQuotesColumn: null,
        });

        hstr += `${
          place_type[0]?.place_name || place?.geoplace_code_rmk
            ? `\n\u2022 `
            : ""
        }${place_type[0]?.place_name ? `${place_type[0]?.place_name}. ` : ""}${
          place?.geoplace_code_rmk ? `${place?.geoplace_code_rmk}. ` : ""
        }`;
      }
      return hstr;
    };

    const generateCourtPlace = async () => {
      let hstr = ``;

      const courtInfo = await selectScript({
        cntc: cntc,
        tableName: "court_t",
        parameters: ["*"],
        conditions: {
          personal_code: person.code,
        },
        isStrictMode: true,
        isOrderBy: null,
        limit: null,
        isQuotesColumn: null,
      });

      for (const cour of courtInfo) {
        const imprisInfo = await selectScript({
          cntc: cntc,
          tableName: "impris_t",
          parameters: ["*"],
          conditions: {
            crt_id: cour.id,
          },
          isStrictMode: true,
          isOrderBy: null,
          limit: null,
          isQuotesColumn: null,
        });

        const placeTypes = await selectScript({
          cntc: cntc,
          tableName: "place_t",
          parameters: ["*"],
          conditions: null,
          isStrictMode: true,
          isOrderBy: null,
          limit: null,
          isQuotesColumn: null,
        });

        const generatePlaceName = (id) => {
          const placeType = placeTypes.find((el) => el.place_num === id);
          return `${placeType ? `\n\u2022 ${placeType.place_name} ` : ""}`;
        };

        for (const imp of imprisInfo) {
          hstr += `${
            imp?.place_num ? `${generatePlaceName(imp?.place_num)}` : ""
          }${imp?.prison_place_rmk ? `(${imp?.prison_place_rmk})` : ""}`;
        }
      }
      return hstr;
    };
    const rep_place = await generateRepPlaces();
    const court_place = await generateCourtPlace();

    str += `${rep_place ? `Места репрессий: ${rep_place}` : ""}${
      court_place ? `\nМеста заключения: ${court_place}` : ""
    }`;

    return str;
  } catch (err) {
    logError(err, generatePlaces.name);
  }
}

export const generateAuthorityRecordsFields = async () => {
  const yesterdayDate = new Date(
    curDateWithTime.getTime() - 24 * 60 * 60 * 1000
  );

  const allPersonals = await selectScript({
    cntc: cntc,
    tableName: "personal_t t1",
    parameters: [
      "t1.*",
      "TO_CHAR(birth_begin, 'DD.MM.YYYY') as birth_begin_char",
      "TO_CHAR(birth_end, 'DD.MM.YYYY') as birth_end_char",
      "TO_CHAR(death_end, 'DD.MM.YYYY') as death_end_char",
      "TO_CHAR(death_begin, 'DD.MM.YYYY') as death_begin_char",
    ],
    conditions: {
      mustPublish: "*",
      "DATE(pub_change_date)": `${yesterdayDate.toISOString().split("T")[0]}`,
    },
    isStrictMode: true,
    isOrderBy: null,
    limit: null,
    isQuotesColumn: null,
  });

  const accompl = allPersonals.filter((pers) => !pers.type_i && pers.type_a);
  const repress = allPersonals.filter((pers) => !pers.type_i && pers.type_r);

  const repressCode = repress.map((el) => el.code);

  accompl.forEach((pers) => {
    if (!repressCode.includes(pers.code)) repress.push(pers);
  });
  const personals = [...repress];

  logMessage(`${personals?.length} personals to publish`);

  const hasPersonalsToPublish = personals?.length !== 0;

  if (hasPersonalsToPublish) {
    const cleanStr = cleanSequence(TABLE_NAMES.AUTH_REC);

    await cntc.manyOrNone(cleanStr);

    for (const person of personals) {
      const legacyIdString = await generateLegacyId(
        ENTITIES_NAMES.PERS,
        person
      );
      const authorizedFormOfNameString = await generateAuthorizedFormOfName(
        person
      );
      const datesOfExistenceString = await generateDatesOfExistence(person);
      const historyString = await generateHistory(person);
      const placesString = await generatePlaces(person);

      // for test
      if (
        ["undefined", "null", "NaN"].some(
          (val) => historyString.includes(val) || placesString.includes(val)
        )
      ) {
        logMessage(`undef|null in pers: ${person.code}`);
      }

      const fieldsToInsert = {
        legacyId: legacyIdString,
        authorizedFormOfName: authorizedFormOfNameString,
        datesOfExistence: datesOfExistenceString,
        history: historyString,
        places: placesString,
        descriptionIdentifier: legacyIdString,
        culture: "ru",
        typeOfEntity: "Person",
        status: "Revised",
        levelOfDetail: "Partial",
      };

      const columnNames = [];
      const valuesToInsert = [];

      Object.keys(fieldsToInsert).forEach((key) => {
        columnNames.push(key);
        valuesToInsert.push(fieldsToInsert[key]);
      });

      await insertScript(
        cntc,
        TABLE_NAMES.AUTH_REC,
        columnNames,
        valuesToInsert
      );
    }
  }
  return hasPersonalsToPublish;
};
