import { connection as cntc } from "../helpers/connect_db.js";
import { ENTITY_FIELDS } from "../static/common-entity-fields.js";
import { ENTITIES_NAMES } from "../static/entity-names.js";
import { DESC_LEVEL } from "../static/level-of-description.js";
import { TABLE_NAMES } from "../static/table-names.js";
import { generateLegacyId } from "../utils/str-generators/legacy-id.js";
import { curDateWithTime } from "../utils/global-vars/current-date.js";
import { insertScript } from "../utils/scripts/insert.js";
import { selectScript } from "../utils/scripts/select.js";
import { logError } from "../helpers/log-error.js";
import { logMessage } from "../helpers/log-message.js";
import { generateDatesRange } from "../utils/str-generators/dates-range.js";
import { getInformantArchivefolders } from "../utils/scripts/get-inform-archivefolders.js";

async function generateParentId(archive) {
  try {
    const opisInfo = await selectScript({
      cntc: cntc,
      tableName: TABLE_NAMES.OPIS,
      parameters: ["fond", "opis_name"],
      conditions: {
        id: `${archive.opis_id}`,
      },
      isStrictMode: true,
      isOrderBy: null,
      limit: null,
      isQuotesColumn: null,
    });

    const { fond, opis_name } = opisInfo[0];

    return `${ENTITY_FIELDS.ARCH_ID}-${ENTITIES_NAMES.ARCH_FOLDER}-${fond}-${opis_name}`;
  } catch (err) {
    logError(err, generateParentId.name);
  }
}

async function generateExtendAndMedium(archive) {
  try {
    const totalFilesCount = await selectScript({
      cntc: cntc,
      tableName: "archivefolderfile",
      parameters: ["*"],
      conditions: {
        archivefolder_id: `${archive.id}`,
      },
      isStrictMode: true,
      isOrderBy: null,
      limit: null,
      isQuotesColumn: null,
    });

    const str = `Цифровых единиц (файлов): ${totalFilesCount.length};`;
    `${archive?.page_num_cnt ? ` Листов: ${archive.page_num_cnt}` : ""}`;
    return str;
  } catch (err) {
    logError(err, generateExtendAndMedium.name);
  }
}

async function generateAlternativeIdentifier(archive) {
  try {
    const opisInfo = await selectScript({
      cntc: cntc,
      tableName: TABLE_NAMES.OPIS,
      parameters: ["fond", "opis_name"],
      conditions: {
        id: `${archive.opis_id}`,
      },
      isStrictMode: true,
      isOrderBy: null,
      limit: null,
      isQuotesColumn: null,
    });

    if (archive && opisInfo[0]) {
      const { delo_extra, delo } = archive;
      const { fond, opis_name } = opisInfo[0];

      const str = ` Ф. ${fond}. Оп. ${opis_name}. Д. ${delo}${
        delo_extra ? `/${delo_extra}` : ""
      }|ID-arch-${fond}-${opis_name}-${delo}${
        delo_extra ? `/${delo_extra}` : ""
      }`;

      return str;
    }
  } catch (err) {
    logError(err, generateAlternativeIdentifier.name);
  }
}

async function generateNameAccessPoints(archive) {
  try {
    const opisInfo = await selectScript({
      cntc: cntc,
      tableName: TABLE_NAMES.OPIS,
      parameters: ["fond", "opis_name"],
      conditions: {
        id: `${archive.opis_id}`,
      },
      isStrictMode: true,
      isOrderBy: null,
      limit: null,
      isQuotesColumn: null,
    });

    const { delo_extra, delo } = archive;
    const { fond, opis_name } = opisInfo[0];

    const persInfo = await selectScript({
      cntc: cntc,
      tableName: "personal_t t1",
      parameters: [
        "t1.*",
        "TO_CHAR(birth_begin, 'DD.MM.YYYY') as birth_begin_char",
        "TO_CHAR(birth_end, 'DD.MM.YYYY') as birth_end_char",
        "TO_CHAR(death_end, 'DD.MM.YYYY') as death_end_char",
        "TO_CHAR(death_begin, 'DD.MM.YYYY') as death_begin_char",
      ],
      conditions: {
        fund: `${fond}`,
        list_no: `${opis_name}`,
        file_no: `${delo}`,
        file_no_ext: `${delo_extra}`,
      },
      isStrictMode: true,
      isOrderBy: null,
      limit: null,
      isQuotesColumn: null,
    });

    const namesAccessPoints = [];
    for (const person of persInfo) {
      const {
        surname,
        fname,
        lname,
        fname_rmk,
        real_surname_rmk,
        lname_rmk,
        birth_begin_char,
        death_end_char,
        birth_end_char,
        death_begin_char,
      } = person;

      const name = `${surname ? `${surname} ` : ""}${fname ? `${fname} ` : ""}${
        lname ? `${lname} ` : ""
      }${
        real_surname_rmk || fname_rmk || lname_rmk
          ? `(${`${real_surname_rmk ? `${real_surname_rmk} ` : ""}${
              fname_rmk ? `${fname_rmk} ` : ""
            }${lname_rmk ? `${lname_rmk}` : ""}`.trim()})`
          : ""
      }`;

      const dates = generateDatesRange(
        "?",
        birth_begin_char,
        birth_end_char,
        death_begin_char,
        death_end_char
      );

      namesAccessPoints.push(name + " " + `(${dates})`);
    }
    return namesAccessPoints.join("|");
  } catch (err) {
    logError(err, generateNameAccessPoints.name);
  }
}

async function generateLinkedArchDescLegacyId(archive) {
  try {
    const linkedArchiveFolders = await selectScript({
      cntc: cntc,
      tableName: TABLE_NAMES.ARCH_FOLD_LINK,
      parameters: ["*"],
      conditions: {
        archivefolder_id1: archive.id,
      },
      isStrictMode: true,
      isOrderBy: "id",
      limit: null,
      isQuotesColumn: null,
    });

    if (!linkedArchiveFolders.length) {
      return "";
    }

    const linkedArchList = [];

    for (const arch of linkedArchiveFolders) {
      const archive = await selectScript({
        cntc: cntc,
        tableName: TABLE_NAMES.ARCH_FOLD,
        parameters: ["*"],
        conditions: {
          id: arch.archivefolder_id2,
        },
        isStrictMode: true,
        isOrderBy: null,
        limit: null,
        isQuotesColumn: null,
      });

      const legacyIdString = await generateLegacyId(
        ENTITIES_NAMES.ARCH_FOLDER,
        archive[0]
      );

      linkedArchList.push(legacyIdString);
    }
    return linkedArchList.join("|");
  } catch {
    logError(err, generateLinkedArchDescLegacyId.name);
  }
}

export const generateArchDescArchFoldFields = async () => {
  const yesterdayDate = new Date(
    curDateWithTime.getTime() - 24 * 60 * 60 * 1000
  );

  try {
    const informDeloIds = (await getInformantArchivefolders()).map(
      (el) => el.id
    );

    const allArchiveFolders = await selectScript({
      cntc: cntc,
      tableName: TABLE_NAMES.ARCH_FOLD,
      parameters: ["*"],
      conditions: {
        mustPublish: "*",
        "DATE(pub_change_date)": `${yesterdayDate.toISOString().split("T")[0]}`,
      },
      isStrictMode: true,
      isOrderBy: "delo",
      limit: null,
      isQuotesColumn: null,
    });

    const archiveFolders = allArchiveFolders.filter(
      (arch) => !informDeloIds.includes(arch.id)
    );

    logMessage(`${archiveFolders?.length} archive folders to publish`);

    const hasArchiveFoldersToPublish = archiveFolders?.length !== 0;

    if (hasArchiveFoldersToPublish) {
      for (const archive of archiveFolders) {
        const legacyIdString = await generateLegacyId(
          ENTITIES_NAMES.ARCH_FOLDER,
          archive
        );
        const parentIdString = await generateParentId(archive);
        const extentAndMediumString = await generateExtendAndMedium(archive);
        const alternativeIdentifiersString =
          await generateAlternativeIdentifier(archive);
        const nameAccessPointsString = await generateNameAccessPoints(archive);

        const { name, doc_begin, doc_end, description, delo } = archive;

        const fieldsToInsert = {
          identifier: legacyIdString,
          legacyId: legacyIdString,
          parentId: parentIdString,
          title: `Дело ${delo}. ${name}`,
          culture: ENTITY_FIELDS.CULTURE,
          eventTypes: ENTITY_FIELDS.EVENT_TYPES,
          eventStartDates: `${doc_begin ? `${doc_begin}-00-00` : ""}`,
          eventEndDates: `${doc_end ? `${doc_end}-00-00` : ""}`,
          extentAndMedium: extentAndMediumString,
          eventActors: ENTITY_FIELDS.EVENT_ACTORS,
          scopeAndContent: `${description ? description : ""}`,
          levelOfDescription: DESC_LEVEL.ARCHIVE,
          alternativeIdentifiers: alternativeIdentifiersString,
          alternativeIdentifierLabels: "Архивный шифр|Global identifier",
          nameAccessPoints: nameAccessPointsString,
        };

        const columnNames = [];
        const valuesToInsert = [];

        Object.keys(fieldsToInsert).forEach((key) => {
          columnNames.push(key);
          valuesToInsert.push(fieldsToInsert[key]);
        });

        await insertScript(
          cntc,
          TABLE_NAMES.ARCH_DESC,
          columnNames,
          valuesToInsert
        );
      }
    } else {
      logMessage("There is no suitable archive folders with these parameters.");
    }
    return hasArchiveFoldersToPublish;
  } catch (err) {
    logError(err, generateArchDescArchFoldFields.name);
  }
};
