import { connection as cntc } from "../helpers/connect_db.js";
import { ENTITIES_NAMES } from "../static/entity-names.js";
import { TABLE_NAMES } from "../static/table-names.js";
import { generateLegacyId } from "../utils/str-generators/legacy-id.js";
import { curDateWithTime } from "../utils/global-vars/current-date.js";
import { insertScript } from "../utils/scripts/insert.js";
import { selectScript } from "../utils/scripts/select.js";
import { logMessage } from "../helpers/log-message.js";

export const deleteListArchDesc = async () => {
  const yesterdayDate = new Date(
    curDateWithTime.getTime() - 24 * 60 * 60 * 1000
  );

  const fonds = await selectScript({
    cntc: cntc,
    tableName: TABLE_NAMES.FOND,
    parameters: ["*"],
    conditions: {
      mustUnpublish: "*",
      "DATE(pub_change_date)": `${yesterdayDate.toISOString().split("T")[0]}`,
    },
    isStrictMode: true,
    isOrderBy: null,
    limit: null,
    isQuotesColumn: null,
  });

  const opises = await selectScript({
    cntc: cntc,
    tableName: TABLE_NAMES.OPIS,
    parameters: ["*"],
    conditions: {
      mustUnpublish: "*",
      "DATE(pub_change_date)": `${yesterdayDate.toISOString().split("T")[0]}`,
    },
    isStrictMode: true,
    isOrderBy: null,
    limit: null,
    isQuotesColumn: null,
  });

  const archiveFolders = await selectScript({
    cntc: cntc,
    tableName: TABLE_NAMES.ARCH_FOLD,
    parameters: ["*"],
    conditions: {
      mustUnpublish: "*",
      "DATE(pub_change_date)": `${yesterdayDate.toISOString().split("T")[0]}`,
    },
    isStrictMode: true,
    isOrderBy: null,
    limit: null,
    isQuotesColumn: null,
  });

  logMessage(`${fonds?.length} fonds to delete`);

  logMessage(`${opises?.length} opises to delete`);

  logMessage(`${archiveFolders?.length} archive folders to delete`);

  const hasAuthRecToDelete =
    fonds?.length !== 0 || opises?.length !== 0 || archiveFolders?.length !== 0;

  if (hasAuthRecToDelete) {
    for (const fond of fonds) {
      const legacyIdString = await generateLegacyId(ENTITIES_NAMES.FOND, fond);

      await insertScript(
        cntc,
        TABLE_NAMES.DEL_ENTITY,
        ["legacyId"],
        [legacyIdString]
      );
    }

    for (const opis of opises) {
      const legacyIdString = await generateLegacyId(ENTITIES_NAMES.OPIS, opis);

      await insertScript(
        cntc,
        TABLE_NAMES.DEL_ENTITY,
        ["legacyId"],
        [legacyIdString]
      );
    }

    for (const archivefolder of archiveFolders) {
      const legacyIdString = await generateLegacyId(
        ENTITIES_NAMES.ARCH_FOLDER,
        archivefolder
      );

      await insertScript(
        cntc,
        TABLE_NAMES.DEL_ENTITY,
        ["legacyId"],
        [legacyIdString]
      );
    }
  }
  return hasAuthRecToDelete;
};
