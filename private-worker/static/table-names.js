export const TABLE_NAMES = {
  DEL_ENTITY: "delete_public_entity",
  ARCH_DESC: "atom_archival_descriptions",
  AUTH_REC: "atom_authority_records",
  ARCH_FOLD: "archivefolder",
  PERS: "personal_t",
  FOND: "fond",
  OPIS: "opis",
  ARCH_FOLD_LINK: "archivefolderlink",
};
