export const ENTITIES_NAMES = {
  FOND: "fond",
  OPIS: "opis",
  ARCH_FOLDER: "arch",
  SOUR: "sour",
  PERS: "pers",
  LINK: "archival-descriptions-link",
  ARCH_DESC: "archival-descriptions",
  AUTH_REC: "authority-records",
  ARCH_DESC_DEL: "archival-descriptions-delete",
  AUTH_REC_DEL: "authority-records-delete",
};
