import { selectScript } from "../utils/scripts/select.js";
import { TABLE_NAMES } from "../static/table-names.js";
import { logError } from "../helpers/log-error.js";
import { connection as cntc } from "../helpers/connect_db.js";
import { generateLegacyId } from "../utils/str-generators/legacy-id.js";
import { ENTITIES_NAMES } from "../static/entity-names.js";
import { generateCSVFile } from "../helpers/csv-generator.js";
import { currentDate } from "../utils/global-vars/current-date.js";
import { sendFilesToPublicSiteServer } from "../helpers/file-sender.js";
import { logMessage } from "../helpers/log-message.js";

export const generateLinkedArch = async () => {
  try {
    const linkedArch = await selectScript({
      cntc: cntc,
      tableName: TABLE_NAMES.ARCH_FOLD_LINK,
      parameters: ["archivefolder_id1", "archivefolder_id2"],
      conditions: null,
      isStrictMode: true,
      isOrderBy: "id",
      limit: null,
      isQuotesColumn: null,
    });

    const listOfLinkedArch = [];

    for (const link of linkedArch) {
      const { archivefolder_id1, archivefolder_id2 } = link;

      const archInfo1 = await selectScript({
        cntc: cntc,
        tableName: TABLE_NAMES.ARCH_FOLD,
        parameters: ["*"],
        conditions: {
          id: +archivefolder_id1,
        },
        isStrictMode: true,
        isOrderBy: "id",
        limit: null,
        isQuotesColumn: null,
      });

      const archInfo2 = await selectScript({
        cntc: cntc,
        tableName: TABLE_NAMES.ARCH_FOLD,
        parameters: ["*"],
        conditions: {
          id: +archivefolder_id2,
        },
        isStrictMode: true,
        isOrderBy: "id",
        limit: null,
        isQuotesColumn: null,
      });

      const legacyId1 = await generateLegacyId(
        ENTITIES_NAMES.ARCH_FOLDER,
        archInfo1[0]
      );

      const legacyId2 = await generateLegacyId(
        ENTITIES_NAMES.ARCH_FOLDER,
        archInfo2[0]
      );

      listOfLinkedArch.push({
        legacyId1: legacyId1,
        legacyId2: legacyId2,
      });
    }

    const headers = [
      { id: "legacyId1", title: "legacyId1" },
      { id: "legacyId2", title: "legacyId2" },
    ];

    await generateCSVFile(
      `${process.env.PATH_TO_LOCAL_FILE + ENTITIES_NAMES.LINK}/`,
      `${currentDate}.csv`,
      headers,
      listOfLinkedArch
    );

    await sendFilesToPublicSiteServer(
      `${process.env.PATH_TO_LOCAL_FILE + ENTITIES_NAMES.LINK}/` +
        `${currentDate}.csv`,
      process.env.LINK_ARCHDESC_PATH
    );
    logMessage("Linked archive descriptions file have been transferred.");

  } catch (err) {
    logError(err, generateLinkedArch.name);
  }
};
