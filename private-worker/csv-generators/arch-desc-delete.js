import { generateCSVFile } from "../helpers/csv-generator.js";
import { connection } from "../helpers/connect_db.js";
import { selectScript } from "../utils/scripts/select.js";
import { TABLE_NAMES } from "../static/table-names.js";
import { deleteListArchDesc } from "../text-generators/delete-arch-desc.js";
import { logError } from "../helpers/log-error.js";
import { currentDate } from "../utils/global-vars/current-date.js";
import { ENTITIES_NAMES } from "../static/entity-names.js";

export const archDescToDelete = async () => {
  const headers = [];
  try {
    const hasGeneratedFields = await deleteListArchDesc();
    if (hasGeneratedFields) {
      const columnNames = await selectScript({
        cntc: connection,
        tableName: "INFORMATION_SCHEMA.COLUMNS",
        parameters: ["COLUMN_NAME"],
        conditions: {
          TABLE_NAME: TABLE_NAMES.DEL_ENTITY,
        },
        isStrictMode: true,
        isOrderBy: null,
        limit: null,
        isQuotesColumn: null,
      });

      columnNames.forEach((names) =>
        headers.push({ id: names.column_name, title: names.column_name })
      );

      const fields = await selectScript({
        cntc: connection,
        tableName: TABLE_NAMES.DEL_ENTITY,
        parameters: ["*"],
        conditions: {
          '"legacyId" NOT': "%pers%",
        },
        isStrictMode: true,
        isOrderBy: null,
        limit: null,
        isQuotesColumn: null,
      });

      await generateCSVFile(
        `${process.env.PATH_TO_LOCAL_FILE + ENTITIES_NAMES.ARCH_DESC_DEL}/`,
        `${currentDate}.csv`,
        headers,
        fields
      );

      return hasGeneratedFields;
    } else {
      return hasGeneratedFields;
    }
  } catch (error) {
    logError(error, archDescToDelete.name);
  }
};
