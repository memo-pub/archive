import { generateCSVFile } from "../helpers/csv-generator.js";
import { connection } from "../helpers/connect_db.js";
import { selectScript } from "../utils/scripts/select.js";
import { generateAuthorityRecordsFields } from "../text-generators/auth-rec.js";
import { TABLE_NAMES } from "../static/table-names.js";
import { currentDate } from "../utils/global-vars/current-date.js";
import { logError } from "../helpers/log-error.js";
import { ENTITIES_NAMES } from "../static/entity-names.js";

export const personalsCSVGenerator = async () => {
  const headers = [];
  try {
    const hasGeneratedFields = await generateAuthorityRecordsFields();
    if (hasGeneratedFields) {
      const columnNames = await selectScript({
        cntc: connection,
        tableName: "INFORMATION_SCHEMA.COLUMNS",
        parameters: ["COLUMN_NAME"],
        conditions: {
          TABLE_NAME: TABLE_NAMES.AUTH_REC,
        },
        isStrictMode: true,
        isOrderBy: null,
        limit: null,
        isQuotesColumn: null,
      });

      columnNames.forEach((names) =>
        headers.push({ id: names.column_name, title: names.column_name })
      );
      const fields = await selectScript({
        cntc: connection,
        tableName: TABLE_NAMES.AUTH_REC,
        parameters: ["*"],
        conditions: null,
        isStrictMode: true,
        isOrderBy: "id",
        limit: null,
        isQuotesColumn: null,
      });

      await generateCSVFile(
        `${process.env.PATH_TO_LOCAL_FILE + ENTITIES_NAMES.AUTH_REC}/`,
        `${currentDate}.csv`,
        headers,
        fields
      );
      return hasGeneratedFields;
    } else {
      return hasGeneratedFields;
    }
  } catch (error) {
    logError(error, personalsCSVGenerator.name);
  }
};
