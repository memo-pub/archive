import { generateCSVFile } from "../helpers/csv-generator.js";
import { selectScript } from "../utils/scripts/select.js";
import { generateArchDescFondsFields } from "../text-generators/arch-desc-fonds.js";
import { generateArchDescOpisesFields } from "../text-generators/arch-desc-opises.js";
import { generateArchDescArchFoldFields } from "../text-generators/arch-desc-archivefolders.js";
import { connection as cntc } from "../helpers/connect_db.js";
import { TABLE_NAMES } from "../static/table-names.js";
import { currentDate } from "../utils/global-vars/current-date.js";
import { logError } from "../helpers/log-error.js";
import { cleanSequence } from "../utils/str-generators/clean-seq.js";
import { ENTITIES_NAMES } from "../static/entity-names.js";


export const archDescCSVGenerator = async () => {
  const headers = [];
  try {

    const cleanStr = cleanSequence(TABLE_NAMES.ARCH_DESC);
    await cntc.manyOrNone(cleanStr);

    const fondsFields = await generateArchDescFondsFields();
    const opisesFields = await generateArchDescOpisesFields();
    const archFoldFields = await generateArchDescArchFoldFields();

    const hasGeneratedFields = archFoldFields || fondsFields || opisesFields;

    if (hasGeneratedFields) {
      const columnNames = await selectScript({
        cntc: cntc,
        tableName: "INFORMATION_SCHEMA.COLUMNS",
        parameters: ["COLUMN_NAME"],
        conditions: {
          TABLE_NAME: TABLE_NAMES.ARCH_DESC,
        },
        isStrictMode: true,
        isOrderBy: null,
        limit: null,
        isQuotesColumn: null,
      });

      columnNames.forEach((names) =>
        headers.push({ id: names.column_name, title: names.column_name })
      );

      const fields = await selectScript({
        cntc: cntc,
        tableName: TABLE_NAMES.ARCH_DESC,
        parameters: ["*"],
        conditions: null,
        isStrictMode: false,
        isOrderBy: "id",
        limit: null,
        isQuotesColumn: null,
      });
  
      await generateCSVFile(
        `${process.env.PATH_TO_LOCAL_FILE}${ENTITIES_NAMES.ARCH_DESC}/${currentDate}/`,
        `${currentDate}.csv`,
        headers,
        fields
      );
    }

    return hasGeneratedFields;
  } catch (error) {
    logError(error, archDescCSVGenerator.name);
  }
};
