import { promisify } from "util";
import { exec } from "child_process";
import { logError } from "../helpers/log-error.js";
import { ATOM_COMMANDS } from "../static/commands/atom.js";

export const elasticPopulateAndCC = async () => {
  const toAtomFolder = ATOM_COMMANDS.TO_ATOM_FOLDER;
  const cacheClean = ATOM_COMMANDS.CACHE_CLEAN;
  const elasticSearchPopulate = ATOM_COMMANDS.ELASTICSEARCH_POPULATE;

  try {
    await promisify(exec)(
      `${toAtomFolder} && ${cacheClean} && ${elasticSearchPopulate}`,
      {
        maxBuffer: 10 * 1024 * 1024,
      }
    );
  } catch (err) {
    logError(err, elasticPopulateAndCC.name);
  }
};
