import util from "util";
import { promisify } from "util";
import { exec } from "child_process";
import fs from "fs";
import { ATOM_COMMANDS } from "../static/commands/atom.js";
import { logError } from "../helpers/log-error.js";
import { elasticPopulateAndCC } from "./elastic-populate.js";
import { removeDuplicates } from "./remove-duplicates.js";
import { ENTITIES_NAMES } from "../static/names/entities.js";
import { logMessage } from "../helpers/log-message.js";

export const updateFields = async (entityName, currentDate, filePath) => {
  const readdir = util.promisify(fs.readdir);
  if (entityName && currentDate) {
    try {
      const {
        TO_ATOM_FOLDER,
        IMPORT_AUTH_RECORDS,
        IMPORT_ARCHIVAL_DESCRIPTION,
      } = ATOM_COMMANDS;

      const isAuthRec = entityName === ENTITIES_NAMES.AUTH_REC;

      if (isAuthRec) {
        const importCommand = IMPORT_AUTH_RECORDS + filePath;
        await promisify(exec)(`${TO_ATOM_FOLDER} && ${importCommand}`, {
          maxBuffer: 10 * 1024 * 1024,
        });
        await removeDuplicates(currentDate);
        await elasticPopulateAndCC(currentDate);
      } else {
        const files = await readdir(filePath);
        if (files) {
          for (const file of files) {
            logMessage(`Loading ${file} started`);
            const importCommand = IMPORT_ARCHIVAL_DESCRIPTION + filePath + file;
            await promisify(exec)(`${TO_ATOM_FOLDER} && ${importCommand}`, {
              maxBuffer: 10 * 1024 * 1024,
            });
            logMessage(`Loading ${file} finish`);
            await elasticPopulateAndCC(currentDate);
          }
          logMessage("finish all!");
        }
      }
    } catch (err) {
      logError(err, updateFields.name);
    }
  }
};
