import util from "util";
import { exec } from "child_process";
import { ATOM_COMMANDS } from "../static/commands/atom.js";
import { DELETE_ARCH_DESC, DELETE_AUTH_REC } from "../static/commands/sql.js";
import { logError } from "../helpers/log-error.js";
import { getItemsFromCSV } from "../helpers/csv-parser.js";
import { executeQuery } from "../utils/execute-query.js";
import { ENTITIES_NAMES } from "../static/names/entities.js";
import { elasticPopulateAndCC } from "./elastic-populate.js";

export const deleteArchDesc = async (currentDate, entityName, filePath) => {
  const execAsync = util.promisify(exec);
  try {
    const data = await getItemsFromCSV(filePath);
    const legacyIds = data.map((item) => `"${item.legacyId}"`);
    if (legacyIds.length) {
      const getInfObjId = DELETE_ARCH_DESC["INF_OBJ_ID_FUNC"];
      const infObjIdStr = getInfObjId(legacyIds);

      const idToDelete = await executeQuery(infObjIdStr);

      const getSlug = DELETE_ARCH_DESC["SLUG_FUNC"];
      const slugIds = idToDelete.map((item) => item.id);
      const slugIdStr = getSlug(slugIds);

      const slugToDelete = await executeQuery(slugIdStr);

      const slugArray = slugToDelete.map((obj) => `'${obj.slug}'`);

      for (const slug of slugArray) {
        const slugExistCheck = DELETE_ARCH_DESC["FIND_SLUG_FUNC"];
        const slugCheckStr = slugExistCheck(slug);
        const isSlugExist = await executeQuery(slugCheckStr);

        if (isSlugExist.length) {
          await execAsync(
            `${ATOM_COMMANDS.TO_ATOM_FOLDER} && ${
              ATOM_COMMANDS.DELETE_DESC_BY_SLUG + slug
            }`
          );
        }
      }

      if (idToDelete.length) {
        const getActorId = DELETE_AUTH_REC["ACTOR_ID_FUNC"];
        const idsArray = idToDelete.map((item) => item.id);
        const actorIdStr = getActorId(idsArray, "parent_id");

        const resultToDelete = await executeQuery(actorIdStr);

        if (resultToDelete.length) {
          const delIds = resultToDelete.map((item) => item.id);

          const delFromTable = DELETE_AUTH_REC["DELETE"];

          const actorStr = delFromTable("actor", "id", delIds);
          const slugStr = delFromTable("slug", "object_id", delIds);
          const actor_i18nStr = delFromTable("actor_i18n", "id", delIds);
          const objectStr = delFromTable("object", "id", delIds);

          await executeQuery(actorStr);
          await executeQuery(slugStr);
          await executeQuery(actor_i18nStr);
          await executeQuery(objectStr);
        }
      }

      await elasticPopulateAndCC(currentDate);
    }
  } catch (err) {
    logError(err, deleteArchDesc.name);
  }
};
