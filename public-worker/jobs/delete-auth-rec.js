import { DELETE_AUTH_REC } from "../static/commands/sql.js";
import { logError } from "../helpers/log-error.js";
import { getItemsFromCSV } from "../helpers/csv-parser.js";
import { executeQuery } from "../utils/execute-query.js";
import { elasticPopulateAndCC } from "./elastic-populate.js";

export const deleteAuthRec = async (currentDate, entityName, filePath) => {
  try {
    const data = await getItemsFromCSV(filePath);
    const legacyIds = data.map((item) => `"${item.legacyId}"`);

    if (legacyIds.length) {

      const getActorId = DELETE_AUTH_REC["ACTOR_ID_FUNC"];
      const actorIdStr = getActorId(legacyIds, "description_identifier");

      const resultToDelete = await executeQuery(actorIdStr);

      if (resultToDelete.length) {
        const idToDelete = resultToDelete.map((item) => item.id);

        const delFromTable = DELETE_AUTH_REC["DELETE"];

        const actorStr = delFromTable("actor", "id", idToDelete);
        const slugStr = delFromTable("slug", "object_id", idToDelete);
        const actor_i18nStr = delFromTable("actor_i18n", "id", idToDelete);
        const objectStr = delFromTable("object", "id", idToDelete);

        await executeQuery(actorStr);
        await executeQuery(slugStr);
        await executeQuery(actor_i18nStr);
        await executeQuery(objectStr);
      }
      await elasticPopulateAndCC(currentDate);
    }
  } catch (err) {
    logError(err, deleteAuthRec.name);
  }
};
