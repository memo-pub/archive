import { DELETE_MATCHES } from "../static/commands/sql.js";
import { logError } from "../helpers/log-error.js";
import { executeQuery } from "../utils/execute-query.js";
import { elasticPopulateAndCC } from "./elastic-populate.js";

export const removeDuplicates = async (currentDate) => {
  try {
    const delFromTable = DELETE_MATCHES["DELETE"];

    const slugStr = delFromTable("slug", "object_id");
    const actor_i18nStr = delFromTable("actor_i18n", "id");
    const objectStr = delFromTable("object", "id");

    await executeQuery(DELETE_MATCHES.CREATE_TEMPORARY_TABLE);
    await executeQuery(slugStr);
    await executeQuery(actor_i18nStr);
    await executeQuery(objectStr);
    await executeQuery(DELETE_MATCHES.DELETE_TEMPORARY_TABLE);
    await elasticPopulateAndCC(currentDate);
  } catch (err) {
    logError(err, removeDuplicates.name);
  }
};
