export const ENTITIES_NAMES = {
  AUTH_REC: "authority-records",
  ARCH_DESC: "archival-descriptions",
  ARCH_DESC_FILE: "arch-desc-delete",
  AUTH_REC_FILE: "auth-rec-delete",
};
