export const ATOM_COMMANDS = {
  TO_ATOM_FOLDER: "cd /usr/share/nginx/atom",
  IMPORT_AUTH_RECORDS:
    'php symfony csv:authority-import --update="match-and-update" ',
  IMPORT_ARCHIVAL_DESCRIPTION:
    'php symfony csv:import --update="match-and-update" ',
  DELETE_DESC_BY_SLUG: "php symfony tools:delete-description -B ",
  CACHE_CLEAN: "php symfony cc",
  ELASTICSEARCH_POPULATE: "php symfony search:populate",
};
