export const DELETE_MATCHES = {
  CREATE_TEMPORARY_TABLE:
    "CREATE TABLE t_temp as (SELECT max(id) as id FROM actor GROUP BY description_identifier);",
  DELETE: (TABLE_NAME, FIELD) =>
    `DELETE from ${TABLE_NAME} where ${FIELD} in (SELECT id from actor WHERE actor.id not in (SELECT id FROM t_temp) and description_identifier is not null);`,
  DELETE_TEMPORARY_TABLE: "DROP TABLE t_temp;",
};

export const DELETE_AUTH_REC = {
  ACTOR_ID_FUNC: (param, cond) =>
    `SELECT id FROM actor WHERE ${cond} IN (${param.join(
      ","
    )});`,
  DELETE: (TABLE_NAME, FIELD, idToDelete) =>
    `DELETE FROM ${TABLE_NAME} WHERE ${FIELD} IN (${idToDelete.join(",")})`,
};

export const DELETE_ARCH_DESC = {
  INF_OBJ_ID_FUNC: (identifiers) =>
    `SELECT id FROM information_object WHERE identifier IN (${identifiers.join(
      ","
    )});`,
  SLUG_FUNC: (obj_id) =>
    `SELECT slug FROM slug WHERE object_id IN (${obj_id.join(",")});`,
  FIND_SLUG_FUNC: (slug) => `SELECT slug FROM slug WHERE slug=${slug};`,
};
