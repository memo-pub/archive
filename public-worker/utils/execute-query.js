import { connection } from "../helpers/connect_db.js";
import { logError } from "../helpers/log-error.js";
import { currentDate } from "./current-date.js";

export const executeQuery = async (sqlQuery) => {
  try {
    return new Promise((resolve, reject) => {
      connection.query(sqlQuery, (err, result) => {
        if (err) {
          reject(err);
        } else {
          resolve(result);
        }
      });
    });
  } catch (err) {
    logError(err, executeQuery.name);
  }
};
