import fs from "fs";
import csvParser from "csv-parser";

export const getItemsFromCSV = async (csvFilePath) => {
  const dataArray = [];

  return new Promise((resolve, reject) => {
    fs.createReadStream(csvFilePath)
      .pipe(csvParser())
      .on("data", (data) => {
        dataArray.push(data);
      })
      .on("end", () => {
        resolve(dataArray);
      })
      .on("error", (error) => {
        reject(error);
      });
  });
};
