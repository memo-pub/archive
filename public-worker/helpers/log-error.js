import { existsSync, mkdirSync, appendFile } from "fs";
import { dirname, resolve, join } from "path";
import { fileURLToPath } from "url";
import { connection } from "./connect_db.js";
import { currentDate } from "../utils/current-date.js";

export const logError = async (error, errorPath) => {
  console.error(`Error in ${errorPath}: ${error}`);
  const date = new Date().toISOString();
  const __filename = fileURLToPath(import.meta.url);
  const __dirname = dirname(__filename);
  const rootDir = resolve(__dirname, "../");

  const logsFolder = join(rootDir, "logs-error");
  const logFilePath = join(logsFolder, `${currentDate}.log`);

  const logMessage = `${date} - Error: ${error} in ${errorPath}\n`;

  if (!existsSync(logsFolder)) {
    mkdirSync(logsFolder);
  }

  appendFile(logFilePath, logMessage, (err) => {
    if (err) {
      console.error("Error recording error:", err);
    }
  });
};
