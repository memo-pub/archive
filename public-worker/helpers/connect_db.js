import mysql from "mysql";
import { connectionObject } from "../config.js";

export const connection = mysql.createConnection(connectionObject);
