import nodemailer from "nodemailer";
import { emailConfig } from "../config.js";
import { currentDate } from "../utils/current-date.js";
import { logError } from "./log-error.js";
import { logMessage } from "./log-message.js";

export const sendErrorEmail = async (err) => {
  const transporter = nodemailer.createTransport(emailConfig);

  const mailOptions = {
    from: process.env.EMAIL_USER_FROM,
    to: process.env.EMAIL_USER_TO,
    subject: "Atom error",
    text: `Error message: ${err.message}`,
  };

  transporter.sendMail(mailOptions, (error, info) =>
    error
      ? logError(error, sendErrorEmail.name)
      : logMessage("Email sent: " + info.response)
  );
};
