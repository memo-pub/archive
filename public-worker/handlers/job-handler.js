import util from "util";
import fs from "fs";
import { updateFields } from "../jobs/update-fields.js";
import { ENTITIES_NAMES } from "../static/names/entities.js";
import { deleteAuthRec } from "../jobs/delete-auth-rec.js";
import { deleteArchDesc } from "../jobs/delete-arch-desc.js";
import { logError } from "../helpers/log-error.js";

const jobs = {
  AUTH_REC: updateFields,
  ARCH_DESC: updateFields,
  ARCH_DESC_FILE: deleteArchDesc,
  AUTH_REC_FILE: deleteAuthRec,
};

export const jobHandler = async (entityName, currentDate) => {
  try {
    const readdir = util.promisify(fs.readdir);
    if (entityName && currentDate) {
      const isArchDesc = entityName === 'ARCH_DESC';
      const filesDirName = `csv-files-${ENTITIES_NAMES[entityName]}/`;
      const dirPath = process.env.PATH_TO_APP + filesDirName;

      const fileToCSVImport = `${currentDate}.csv`;
      const folderArchDesc = `${currentDate}/`;

      const filePath =
        dirPath + (isArchDesc ? folderArchDesc : fileToCSVImport);

      const files = await readdir(dirPath);

      if (files) {
        const hasTodayFile = files.includes(
          isArchDesc ? currentDate : fileToCSVImport
        );
        if (hasTodayFile) {
          const selectedFunction = jobs[entityName];
          if (selectedFunction) {
            await selectedFunction(
              ENTITIES_NAMES[entityName],
              currentDate,
              filePath
            );
          }
        }
      }
    }
  } catch (err) {
    logError(err, jobHandler.name);
  }
};
