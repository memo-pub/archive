import { jobHandler } from "./handlers/job-handler.js";
import { connection as cntc } from "./helpers/connect_db.js";
import { sendErrorEmail } from "./helpers/send-email.js";
import { elasticPopulateAndCC } from "./jobs/elastic-populate.js";
import { currentDate } from "./utils/current-date.js";
import { logError } from "./helpers/log-error.js";
import { logMessage } from "./helpers/log-message.js";

const runApp = async () => {
  try {
    logMessage("Publishing started");

    cntc.connect();

    await jobHandler("AUTH_REC_FILE", currentDate);

    await jobHandler("ARCH_DESC_FILE", currentDate);

    await jobHandler("AUTH_REC", currentDate);

    await jobHandler("ARCH_DESC", currentDate);

    await elasticPopulateAndCC(currentDate);

    cntc.end();
    logMessage("Publishing finished");
  } catch (err) {
    logError(err, runApp.name);
    sendErrorEmail(err);
  }
};

runApp();


