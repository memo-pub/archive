import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthService } from '../../shared/services/auth.service';
import { UsersService } from '../../shared/services/users.service';
import { isNullOrUndefined } from 'util';

// import { UsersService } from '../../shared/services/users.service';
// import { User } from '../../shared/models/user.model';
// import { AuthService } from '../../shared/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  form: FormGroup;

  constructor(
    private usersService: UsersService,
    private authService: AuthService,
    public snackBar: MatSnackBar,
    private router: Router
  ) {
    let user = JSON.parse(window.localStorage.getItem('reportuser'));
    if (user) {
      this.authService.login(user);
    }
  }

  async ngOnInit() {
    this.form = new FormGroup({
      login: new FormControl(null, [Validators.required]),
      password: new FormControl(null, [Validators.required]),
      rmbMe: new FormControl(true)
    });
  }

  async onSubmit() {
    if (this.form.invalid) {
      this.openSnackBar(`Введите логин и пароль.`, 'Ok');
      return;
    }
    let user = {
      login: this.form.value.login,
      password: this.form.value.password
    };
    try {
      let _user = await this.usersService.getUserByLogin(user);
      this.authService.login(_user);
      if (this.form.value.rmbMe) {
        window.localStorage.setItem('reportuser', JSON.stringify(_user));
      }
      // if (isNullOrUndefined(_user.usertype)) {
      //   this.router.navigate(['/sys/arch/list']);
      // } else if (_user.usertype === 3) {
      //   this.router.navigate(['/sys/person/personaltlist']);
      // } else if (_user.usertype === 4) {
      //   this.router.navigate(['/sys/person/selectionlist']);
      // } else {
      //   this.router.navigate(['/sys/arch/list']);
      // }
    } catch (error) {
      if (error.status === 401) {
        this.openSnackBar(`Логин или пароль не верны.`, 'Ok');
      } else {
        this.openSnackBar(
          `Ошибка соединения, попробуйте позже или обратитесь к системному администратору.`,
          'Ok'
        );
      }
    }
  }

  navRegister() {
    this.router.navigate(['/register']);
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 5000
    });
  }
}
