import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UsersService } from '../../shared/services/users.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

    form: FormGroup;

    constructor(
        private usersService: UsersService,
        public snackBar: MatSnackBar,
        private router: Router
    ) { }

    ngOnInit() {
        this.form = new FormGroup({
            'login': new FormControl(null, [Validators.required]),
            'name': new FormControl(null, [Validators.required]),
            'email': new FormControl(null),
            'password': new FormControl(null, [Validators.required])
        });
    }

    async onSubmit() {
        if (this.form.invalid) {
            this.openSnackBar(`Введите имя, логин и пароль.`, 'Ok');
            return;
        }
        let user = {
            login: this.form.value.login,
            name: this.form.value.name,
            email: this.form.value.email,
            password: this.form.value.password
        };
        try {

            this.navLogin();
        } catch (error) {
            if (error.status === 401) {
                this.openSnackBar(`Логин или пароль не верны.`, 'Ok');
            } else {
                this.openSnackBar(`Ошибка соединения, попробуйте позже или обратитесь к системному администратору.`, 'Ok');
            }
        }
    }

    navLogin() {
        this.router.navigate(['/login']);
    }

    openSnackBar(message: string, action: string) {
        this.snackBar.open(message, action, {
            duration: 10000,
        });
    }



}
