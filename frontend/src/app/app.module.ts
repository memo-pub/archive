import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  NgModule,
  LOCALE_ID,
  forwardRef,
  APP_INITIALIZER,
  ErrorHandler,
} from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { registerLocaleData } from '@angular/common';
import { MatPaginatorIntl } from '@angular/material/paginator';
import localeRu from '@angular/common/locales/ru';
registerLocaleData(localeRu);

import { SystemModule } from './system/system.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { AuthModule } from './auth/auth.module';
import { MatPaginatorIntlRus } from './shared/services/global.service';
import { ConfigService } from './shared/services/config.service';
import { environment } from 'src/environments/environment';
import { ParamInterceptor } from './shared/core/api.interceptor';
import { BaseApi } from './shared/core/base-api';
import { GlobalErrorHandler } from './system/shared/services/globalErrorHandler';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
// import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    // Ng4LoadingSpinnerModule.forRoot(),
    // NgxSpinnerModule,
    NgbModule,
    FormsModule,
    HttpClientModule,
    AuthModule,
    SystemModule,
    SharedModule,
  ],
  providers: [
    BaseApi,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ParamInterceptor,
      multi: true,
    },
    ConfigService,
    {
      provide: APP_INITIALIZER,
      useFactory: ConfigLoader,
      deps: [ConfigService],
      multi: true,
    },
    { provide: LOCALE_ID, useValue: 'ru' },
    { provide: 'pathStr', useValue: '' },
    { provide: 'fileStr', useValue: '' },
    { provide: 'parentStr', useValue: '' },
    {
      provide: ErrorHandler,
      useClass: GlobalErrorHandler,
    },
    GlobalErrorHandler,
    // { provide: MatPaginatorIntl, useClass: forwardRef(() => MatPaginatorIntlRus) }
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
export function ConfigLoader(configService: ConfigService) {
  return () => configService.load(environment.configFile);
}
