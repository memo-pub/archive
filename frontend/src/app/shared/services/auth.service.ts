import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { Muser } from '../models/muser.model';
import { isNullOrUndefined } from 'util';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  redirectUrl: string;
  redirectParams: any;
  private isAuthenticated = false;
  private loggedInUser: Muser = null;

  constructor(private router: Router) {}

  login(user: Muser) {
    this.loggedInUser = user;
    this.isAuthenticated = true;
    if (!isNullOrUndefined(this.redirectUrl) && this.redirectUrl !== '') {
      this.router.navigate([this.redirectUrl], {
        replaceUrl: false,
        queryParams: this.redirectParams
      });
    } else {
      this.onLinkMainPage();
    }
  }

  onLinkMainPage() {
    if (isNullOrUndefined(this.loggedInUser)) {
      this.router.navigate(['/login']);
    } else if (isNullOrUndefined(this.loggedInUser.usertype)) {
      this.router.navigate(['/sys/arch/list']);
    } else if (this.loggedInUser.usertype === 3) {
      this.router.navigate(['/sys/person/personaltlist']);
    } else if (this.loggedInUser.usertype === 4) {
      this.router.navigate(['/sys/person/selectionlist']);
    } else {
      this.router.navigate(['/sys/arch/list']);
    }
  }

  logout() {
    this.loggedInUser = null;
    this.redirectUrl = null;
    this.redirectParams = null;
    window.localStorage.clear();
    this.isAuthenticated = false;
    this.router.navigate(['/login']);
    window.location.reload();
  }

  isLoggedIn(): boolean {
    return this.isAuthenticated;
  }

  isBrl(): boolean {
    return this.loggedInUser.usertype === 3;
  }

  getCurUser(): Muser {
    return this.loggedInUser;
  }
}
