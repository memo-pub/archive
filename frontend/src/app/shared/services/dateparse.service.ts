import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { GlobalService } from './global.service';

@Injectable({
  providedIn: 'root',
})
export class DateparseService {
  seek_month = {
    ЯНВ: '01',
    ФЕВ: '02',
    МАР: '03',
    АПР: '04',
    МАЙ: '05',
    ИЮН: '06',
    ИЮЛ: '07',
    АВГ: '08',
    СЕН: '09',
    ОКТ: '10',
    НОЯ: '11',
    ДЕК: '12',
    МАЯ: '05',
    МАЕ: '05',
  };

  months = [
    'Январь',
    'Февраль',
    'Март',
    'Апрель',
    'Май',
    'Июнь',
    'Июль',
    'Август',
    'Сентябрь',
    'Октябрь',
    'Ноябрь',
    'Декабрь',
  ];

  constructor(private globalService: GlobalService) {}

  private parse_date(date_str) {
    let re = /[.,\s]+/;
    let date_elements = date_str.trim().split(re).reverse();
    const yr = date_elements[0];
    if (yr.length !== 4) {
      throw new Error('Неверный формат года');
    }
    if (!isNumeric(yr)) {
      throw new Error('Год не определен');
    }
    let month1;
    let month2;
    if (date_elements[1]) {
      month1 = date_elements[1];
      if (!isNumeric(month1)) {
        if (month1.length < 3) {
          throw new Error('Название месяца меньше 3х знаков');
        }
        month1 = this.seek_month[month1.substr(0, 3).toUpperCase()];
        if (month1 === undefined) {
          throw new Error('Месяц не определен');
        }
      }
      if (!isNumeric(month1)) {
        throw new Error('Неверно указан  месяц');
      }
      if (month1 < 1 || month1 > 12) {
        throw new Error('Неверно указан месяц');
      }
      month1 = month1.padStart(2, '0');
      month2 = month1;
    } else {
      month1 = '01';
      month2 = '12';
    }
    let day1;
    let day2;
    if (date_elements[2]) {
      day1 = date_elements[2];
      if (day1 < 1 || day1 > 31) {
        throw new Error('Неверно указан день');
      }
      day1 = day1.padStart(2, '0');
      day2 = day1;
    } else {
      day1 = '01';
      day2 = daysInMonth(month1 - 1, yr).toString();
      day2 = day2.padStart(2, '0');
    }
    try {
      let obj = {
        date1: new Date(yr + '-' + month1 + '-' + day1),
        date2: new Date(yr + '-' + month2 + '-' + day2),
      };
      return obj;
    } catch (e) {
      // throw new UserException('Ошибка формирования даты');
      throw new Error('Ошибка формирования даты');
    }
  }

  private parseDate(input_string) {
    let rg = /^([0-9]{3}0)\s*-\s*е$/;
    if (rg.test(input_string.trim())) {
      let res = input_string.trim().match(rg);
      let yrs = +res[1];
      return {
        date1: new Date(yrs + '-01-01'),
        date2: new Date(yrs + 9 + '-12-31'),
      };
    }
    const str_dates = input_string.split('-');
    let obj = this.parse_date(str_dates[0]);
    if (str_dates[1]) {
      let d2 = this.parse_date(str_dates[1]);
      obj.date2 = d2.date2;
    }
    return obj;
  }

  getSour_date(
    form: FormGroup,
    date: string,
    date_begin: string,
    date_end: string
  ) {
    if (!form.value[date] || form.value[date] === '') {
      form.patchValue({ [date_begin]: null });
      form.patchValue({ [date_end]: null });
    } else {
      try {
        let res = this.parseDate(form.value[date]);
        let res_date = '';
        if (res.date1 && res.date2) {
          if (+res.date1 === +res.date2) {
            res_date = this.toDateStr(res.date1);
            // res_date = res.date1.toLocaleDateString();
          } else {
            res_date = `${this.toDateStr(res.date1)} - ${this.toDateStr(
              res.date2
            )}`;
            // res_date = `${res.date1.toLocaleDateString()} - ${res.date2.toLocaleDateString()} `;
          }
        } else if (res.date1) {
          res_date = this.toDateStr(res.date1);
          // res_date = res.date1.toLocaleDateString();
        } else {
          res_date = null;
        }
        form.patchValue({ [date]: res_date });
        form.patchValue({ [date_begin]: res.date1 });
        form.patchValue({ [date_end]: res.date2 });
        form.controls[date].setErrors(null);
      } catch (err) {
        form.controls[date].setErrors({ incorrect: true });
        this.globalService.openSnackBar(err.message, 'Ok');
      }
    }
  }

  setSourDate(form: FormGroup, date: string, date1: Date, date2: Date) {
    let res_date = '';
    let d1;
    let d2;
    if (date1) {
      d1 = new Date(date1);
      res_date = this.toDateStr(d1);
      // res_date = d1.toLocaleDateString();
    }
    if (date2) {
      d2 = new Date(date2);
      if (+d1 !== +d2) {
        res_date += ` - ${this.toDateStr(d2)}`;
        // res_date += ` - ${d2.toLocaleDateString()}`;
      }
    }
    form.patchValue({ [date]: res_date });
  }

  toDateStr(date: Date) {
    let strDate = `${date.getDate()}`.padStart(2, '0');
    let strMonth = `${date.getMonth() + 1}`.padStart(2, '0');
    let strYear = `${date.getFullYear()}`;
    return `${strDate}.${strMonth}.${strYear}`;
  }

  toMultiDateStr(str_begin: string, str_end: string, html = true): string {
    if (!str_begin) {
      return '';
    }
    let date_begin = new Date(str_begin);
    let date_end = str_end ? new Date(str_end) : new Date(str_begin);
    if (+date_begin === +date_end) {
      let strDate_begin = `${date_begin.getDate()}`.padStart(2, '0');
      let strMonth_begin = `${date_begin.getMonth() + 1}`.padStart(2, '0');
      let strYear_begin = `${date_begin.getFullYear()}`;
      return `${strDate_begin}.${strMonth_begin}.${strYear_begin}`;
    } else if (
      date_begin.getDate() === 1 &&
      date_begin.getMonth() === 0 &&
      date_end.getDate() ==
        getLastDayOfMonth(date_end.getFullYear(), date_end.getMonth()) &&
      date_end.getMonth() === 11
    ) {
      if (date_end.getFullYear() === date_begin.getFullYear()) {
        return `${date_begin.getFullYear()}`;
      } else {
        return `${date_begin.getFullYear()} - ${date_end.getFullYear()}`;
      }
    } else if (
      date_end.getFullYear() === date_begin.getFullYear() &&
      date_begin.getDate() === 1 &&
      date_end.getDate() ==
        getLastDayOfMonth(date_end.getFullYear(), date_end.getMonth())
    ) {
      return `${
        this.months[date_begin.getMonth()]
      } ${date_begin.getFullYear()}`;
    } else {
      let strDate_begin = `${date_begin.getDate()}`.padStart(2, '0');
      let strMonth_begin = `${date_begin.getMonth() + 1}`.padStart(2, '0');
      let strYear_begin = `${date_begin.getFullYear()}`;
      let strDate_end = `${date_end.getDate()}`.padStart(2, '0');
      let strMonth_end = `${date_end.getMonth() + 1}`.padStart(2, '0');
      let strYear_end = `${date_end.getFullYear()}`;
      if (html) {
        return `<small>${strDate_begin}.${strMonth_begin}.${strYear_begin} -<br>${strDate_end}.${strMonth_end}.${strYear_end}</small>`;
      } else {
        return `${strDate_begin}.${strMonth_begin}.${strYear_begin} - ${strDate_end}.${strMonth_end}.${strYear_end}`;
      }
    }
  }
}

// function UserException(message) {
//   this.message = message;
//   this.name = 'Исключение, определенное пользователем';
// }

function daysInMonth(iMonth, iYear) {
  return 32 - new Date(iYear, iMonth, 32).getDate();
}

function isNumeric(value) {
  return /^-{0,1}\d+$/.test(value);
}

function getLastDayOfMonth(year, month) {
  let date = new Date(year, month + 1, 0);
  return date.getDate();
}
