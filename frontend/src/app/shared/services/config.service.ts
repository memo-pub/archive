import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  private config: Configuration;

  constructor(private http: HttpClient) {}

  async load(url: string) {
    this.config = await this.http.get(url).toPromise();
  }

  getConfiguration(): Configuration {
    return this.config;
  }
}

export class Configuration {
  baseUrl?: string[];
}
