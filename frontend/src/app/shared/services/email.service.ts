// import { Injectable } from '@angular/core';
// import { HttpClient, HttpHeaders } from '@angular/common/http';

// import { BaseApi } from '../core/base-api';
// import { AuthService } from './auth.service';
// import { Muser } from '../models/muser.model';


// @Injectable({
//   providedIn: 'root'
// })
// export class EmailService extends BaseApi {

//   loggedInUser: Muser;
//   options: HttpHeaders;
//   optionsFile: HttpHeaders;

//   constructor(
//     public http: HttpClient,
//     private authService: AuthService,
//   ) {
//     super(http);
//     this.loggedInUser = this.authService.getCurUser();
//     this.options = new HttpHeaders();
//     this.options = this.options.set('Authorization', 'JWT ' + this.loggedInUser.token);
//     this.options = this.options.append('Content-Type', 'application/json');
//     this.optionsFile = new HttpHeaders();
//     this.optionsFile = this.optionsFile.set('Authorization', 'JWT ' + this.loggedInUser.token);
//   }

//   async sendemail(data) {
//     return this.post('sendemail', data, this.options).toPromise();
//   }

//   sendEmail(data) {
//     return this.post('sendemail', data, this.options);
//   }

// }
