import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseApi } from '../core/base-api';
import { ConfigService } from './config.service';

@Injectable({
  providedIn: 'root'
})
export class UsersService extends BaseApi {
  constructor(public http: HttpClient, public configService: ConfigService) {
    super(http, configService);
  }

  async getUserByLogin(user) {
    return this.login('login', user).toPromise();
  }
}
