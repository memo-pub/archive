import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router
} from '@angular/router';
import { Observable } from 'rxjs';

import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class ExtraGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    if (
      this.authService.isLoggedIn() &&
      (this.authService.getCurUser().isadmin ||
        this.authService.getCurUser().usertype === 3 ||
        this.authService.getCurUser().usertype === 4)
    ) {
      return true;
    } else {
      this.authService.redirectParams = next.queryParams;
      this.authService.redirectUrl = state.url.substring(
        0,
        state.url.indexOf('?') === -1
          ? state.url.length
          : state.url.indexOf('?')
      );
      // this.authService.onLinkMainPage();
      this.router.navigate(['/login']);
      return false;
    }
  }

  canActivateChild(
    childRoute: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean | Observable<boolean> | Promise<boolean> {
    return this.canActivate(childRoute, state);
  }
}
