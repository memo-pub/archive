import { isNullOrUndefined } from 'util';
import { Injectable } from '@angular/core';
import { MatPaginatorIntl } from '@angular/material/paginator';
import { PersonalTFilter } from '../models/personalt.model';
import { BehaviorSubject, Subscription } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root',
})
export class GlobalService {
  hasError: BehaviorSubject<any>;
  routerObserver: Subscription;

  muserTypes = [
    {
      key: -1,
      value: 'не задано',
    },
    {
      key: 1,
      value: 'архивист',
    },
    {
      key: 2,
      value: 'сканировщик',
    },
    {
      key: 3,
      value: 'архивист БРЛ',
    },
    {
      key: 4,
      value: 'исследователь',
    },
  ];

  postfixTypes = [
    {
      key: 'ОЦ',
      value: 'ОЦ',
    },
    {
      key: 'Доп',
      value: 'Доп',
    },
    {
      key: '',
      value: '(без значения)',
    },
  ];

  yesno = [
    {
      key: '*',
      value: 'Да',
    },
    {
      key: ' ',
      value: 'Нет',
    },
  ];

  yesnoq = [
    {
      key: 'Д',
      value: 'Да',
    },
    {
      key: 'Н',
      value: 'Нет',
    },
    {
      key: '?',
      value: 'Неизвестно',
    },
  ];

  sex = [
    {
      key: 'М',
      value: 'Муж',
    },
    {
      key: 'Ж',
      value: 'Жен',
    },
  ];

  statusTypes = [
    {
      key: 0,
      value: 'Создано',
    },
    {
      key: 1,
      value: 'Заполнено',
    },
    {
      key: 2,
      value: 'Отсканировано',
    },
    {
      key: 3,
      value: 'Закончено',
    },
  ];

  actionTypes = [
    {
      key: 'SET',
      value: 'Изменение',
    },
    {
      key: 'DELETE',
      value: 'Удаление',
    },
    {
      key: 'NEW',
      value: 'Создание',
    },
  ];
  actionLogTypes = [
    {
      key: 1,
      value: 'создание дела',
    },
    {
      key: 2,
      value: 'изменение статуса',
    },
    {
      key: 3,
      value: 'добавление файла',
    },
    {
      key: 4,
      value: 'переименование файла',
    },
    {
      key: 5,
      value: 'удаление файла',
    },
    {
      key: 6,
      value: 'к-во пронумерованных листов',
    },
    {
      key: 7,
      value: 'удаление дела',
    },
    { key: 8, value: 'Опись' },
    { key: 9, value: 'Дело' },
    { key: 10, value: 'Дополнительный шифр' },
    { key: 11, value: 'Тип описания' },
    { key: 12, value: 'Комментарий к типу описания' },
    { key: 13, value: 'Название' },
    { key: 14, value: 'Описание' },
    { key: 15, value: 'Общее количество изображений' },
    { key: 16, value: 'Изображений Tiff' },
    { key: 17, value: 'Комментарий' },
    { key: 18, value: 'Дата создания документа' },
    { key: 19, value: 'URL публикации' },
    { key: 20, value: 'Комментарий для сканировщика' },
    { key: 21, value: 'Даты документов, с' },
    { key: 22, value: 'Даты документов, по' },
    { key: 23, value: 'Дата публикации' },
    { key: 24, value: 'Номер последнего пронумерованного листа' },
    { key: 25, value: 'Архивист' },
    { key: 26, value: 'Сканировщик' },
    { key: 27, value: 'Добавлено связанное дело' },
    { key: 28, value: 'Связанное дело удалено' },
    { key: 29, value: 'Дата (текст)' },
  ];
  tableTypes = [
    {
      key: 'personal_t',
      value: 'Персоналия',
    },
    {
      key: 'activity_t',
      value: 'Учеба и профессиональная деятельность',
    },
    {
      key: 'org_t',
      value: 'Участие в общественных организациях',
    },
    {
      key: 'sour_t',
      value: 'Источники',
    },
    {
      key: 'personal_sour_t',
      value: 'Источники',
    },
    {
      key: 'repress_t',
      value: 'Репрессии',
    },
    {
      key: 'usage_t',
      value: 'Использование',
    },
    {
      key: 'help_t',
      value: 'Помощь',
    },
    {
      key: 'court_t',
      value: 'Суд',
    },
    {
      key: 'impris_t',
      value: 'Заключение',
    },
    {
      key: 'cocrt_t',
      value: 'Упоминаемые в связи с судом',
    },
    {
      key: 'familyt_t',
      value: 'Члены семьи',
    },
    {
      key: 'family_t',
      value: 'Члены семьи',
    },
    {
      key: 'corep_t',
      value: 'Упоминаемые в связи с репрессией',
    },
    {
      key: 'inform_t',
      value: 'Информанты',
    },
    {
      key: 'mentioned_t',
      value: 'Упомянут в личном деле',
    },
    {
      key: 'sour_type_t',
      value: 'Тип источника',
    },
    {
      key: 'act_sphere_t',
      value: 'Сфера деятельности',
    },
    {
      key: 'coact_t',
      value: 'Лица, упоминаемые в связи в c учебой или работой',
    },
    {
      key: 'sour_file_t',
      value: 'Файлы источника',
    },
    {
      key: 'coimp_t',
      value: 'Лица, упоминаемые в связи с заключением',
    },
    {
      key: 'coorg_t',
      value: 'Лица, упоминаемые в связи в организацией',
    },
    {
      key: 'personal_shifr_t',
      value: 'Архивные шифры',
    },
  ];

  name_f = [
    {
      key: 'EDUC',
      value: 'Образование',
    },
    {
      key: 'SPEC',
      value: 'Специальность',
    },
    {
      key: 'SOUR_TYPE',
      value: 'Тип источника',
    },
    {
      key: 'REPRESS_TYPE',
      value: 'Тип репрессии',
    },
    {
      key: 'IMPRIS_TYPE',
      value: 'Тип наказания',
    },
    {
      key: 'SPHERE',
      value: 'Сфера деятельности',
    },
    {
      key: 'PROTEST',
      value: 'Вид протеста',
    },
    {
      key: 'ORG_CODE',
      value: 'Тип организации',
    },
    {
      key: 'ORIGIN',
      value: 'Происхождение',
    },
    {
      key: 'ORG_NAME',
      value: 'Название полит.организации',
    },
    {
      key: 'PARTICIP',
      value: 'Тип участия',
    },
    {
      key: 'SENTENSE',
      value: 'Приговор',
    },
    {
      key: 'HELP_TYP',
      value: 'Тип помощи',
    },
    {
      key: 'SOUR_NAME',
      value: 'Название источника',
    },
    {
      key: 'REASON',
      value: 'Причина освобождения',
    },
    {
      key: 'US_TYPE',
      value: 'Тип использования',
    },
    {
      key: 'REPROD_TYPE',
      value: 'Тип воспроизведения',
    },
    {
      key: 'DESCRIP_TYPE',
      value: 'Тип описания',
    },
    // {
    //   key: 'DEATH_REASON_T',
    //   value: 'Причина смерти'
    // }
  ];

  sour_mark = [
    {
      key: 'О',
      value: 'Оригинал',
    },
    {
      key: 'К',
      value: 'Копия',
    },
  ];

  link_types = [
    {
      key: 1,
      value: 'Дополнение',
    },
    {
      key: 2,
      value: 'Родственник',
    },
    {
      key: 3,
      value: 'Упоминание',
    },
  ];

  sourtSearchflds = [
    {
      key: 0,
      value: 'Название',
    },
    {
      key: 1,
      value: 'Кол-во листов',
    },
    {
      key: 2,
      value: 'Место нахождения',
    },
    {
      key: 3,
      value: 'Комментарий',
    },
    {
      key: 4,
      value: 'Фонд',
    },
    {
      key: 5,
      value: 'Опись',
    },
    {
      key: 6,
      value: 'Дело',
    },
    {
      key: 7,
      value: 'Дело доп.',
    },
    {
      key: 8,
      value: 'Дата',
    },
  ];
  searchflds = [
    {
      key: 0,
      // value: 'description'
      value: 'Описание',
    },
    {
      key: 1,
      // value: 'comment'
      value: 'Комментарий',
    },
    {
      key: 2,
      // value: 'url'
      value: 'URL',
    },
    {
      key: 3,
      // value: 'not_scanned_rmk'
      value: 'Комментарий к «Не сканируется»',
    },
    {
      key: 4,
      // value: 'scan_comment'
      value: 'Комментарий для сканировщика',
    },
    {
      key: 5,
      value: 'Сканировщик',
    },
    {
      key: 6,
      value: 'Архивист',
    },
  ];

  tables = [
    { key: 'personal_t', value: 'Персоналии' },
    { key: 'activity_t', value: 'Учеба и профессиональная деятельность' },
    { key: 'org_t', value: 'Участие в общественных организациях' },
    { key: 'repress_t', value: 'Репрессии' },
    { key: 'help_t', value: 'Сведения о помощи' },
    { key: 'sour_t', value: 'Источники' },
    { key: 'usage_t', value: 'Использования персонального дела' },
    { key: 'court_t', value: 'Следствия и суды' },
    { key: 'impris_t', value: 'Заключения' },
    { key: 'text_t', value: 'Тексты' },
    { key: 'inform_t', value: 'Информанты' },
    { key: 'family_t', value: 'Члены семьи' },
  ];

  citizes = [
    'АВСТРИЯ',
    'БОЛГАРИЯ',
    'ВЕНГ',
    'ГЕРМАНИЯ',
    'ГРЕЦИЯ',
    'ИРАН',
    'ИСПАНИЯ',
    'КАНАДА',
    'КИТАЙ',
    'КОРЕЯ',
    'ЛАТВИЯ',
    'ПОЛЬША',
    'РУМЫНИЯ',
    'РУС',
    'СССР',
    'США',
    'УКР',
    'ФИНЛЯНДИЯ',
    'ФРАНЦИЯ',
    'ЧЕХОСЛОВАКИЯ',
    'ЮГОСЛАВИЯ',
    'ЯПОНИЯ',
  ];

  pubTypes = [
    { id: 1, value: 'Публикация' },
    { id: 2, value: 'Закрытая публикация' },
    { id: 3, value: 'Запрет публикации' },
    { id: 4, value: 'Не определено' },
  ];
  reprodTypes = [
    { id: 1, value: 'Aвтограф' },
    { id: 2, value: 'Aвтограф, копия под копирку' },
    { id: 3, value: 'Aвтограф с правкой' },
    { id: 4, value: 'Газетные и журнальные вырезки' },
    { id: 5, value: 'Дубликат' },
    { id: 6, value: 'Компьютерная распечатка' },
    { id: 7, value: 'Копия' },
    { id: 8, value: 'Ксерокопия' },
    { id: 9, value: 'Ксерокопия автографа' },
    { id: 10, value: 'Ксерокопия машинописи' },
    { id: 11, value: 'Ксерокопия машинописной копии' },
    { id: 12, value: 'Машинописная копия' },
    { id: 13, value: 'Оттиск' },
    { id: 14, value: 'Офсетная печать' },
    { id: 15, value: 'Печать' },
    { id: 16, value: 'подлинник' },
    { id: 17, value: 'Ротапринт' },
    { id: 18, value: 'Факс' },
    { id: 19, value: 'Фотокопия' },
    { id: 20, value: 'Фотокопия машинописи' },
  ];

  breadcrumbs = [];

  public loadedStr: BehaviorSubject<string>;

  public fileslistFilter = {
    searchOrder: 'byid',
    filter_fond: '',
    filter_opis_nam: '',
    filter_delo: '',
    filter_filename: '',
  };
  public fileslistsetImgIdx = -1;
  public fileslistsetImgIdxCount = 0;
  public fileslistsetImgId = -1;

  public personal_edit = {
    scroll: 0,
    selectedOrgt: false,
    selectedActivityt: false,
    selectedRepresst: false,
    selectedHelp: false,
    selectedSourt: false,
    // selectedUsaget: false,
    selectedMentioned: false,
  };

  public represst_edit = {
    scroll: 0,
    selectedCourtt: false,
  };

  public courtt_edit = {
    scroll: 0,
    selectedImprist: false,
  };

  public marchivelistPage = 1;
  public personaltlistPage = 1;
  private _marchivelistFilter;
  // private _personaltlistFilter;
  // private _personaltlistFilterExact = false;
  // private _selectionlistFilter;
  // private _selectionitemFilter;
  // private _selectioneditPage = 0;
  // private _selectioneditPageSize = 100;

  get marchivelistFilter() {
    return this._marchivelistFilter;
  }

  set marchivelistFilter(marchivelistFilter) {
    if (!isNullOrUndefined(marchivelistFilter)) {
      this._marchivelistFilter = marchivelistFilter;
    }
  }

  // get selectioneditPage() {
  //   return this._selectioneditPage;
  // }

  // set selectioneditPage(selectioneditPage) {
  //   if (!isNullOrUndefined(selectioneditPage)) {
  //     this._selectioneditPage = selectioneditPage;
  //   }
  // }

  // get selectioneditPageSize() {
  //   return this._selectioneditPageSize;
  // }

  // set selectioneditPageSize(selectioneditPageSize) {
  //   if (!isNullOrUndefined(selectioneditPageSize)) {
  //     this._selectioneditPageSize = selectioneditPageSize;
  //   }
  // }

  // get personaltlistFilter() {
  //   return this._personaltlistFilter;
  // }

  // set personaltlistFilter(personaltlistFilter) {
  //   if (!isNullOrUndefined(personaltlistFilter)) {
  //     this._personaltlistFilter = personaltlistFilter;
  //   }
  // }

  // get personaltlistFilterExact() {
  //   return this._personaltlistFilterExact;
  // }

  // set personaltlistFilterExact(personaltlistFilterExact) {
  //   if (!isNullOrUndefined(personaltlistFilterExact)) {
  //     this._personaltlistFilterExact = personaltlistFilterExact;
  //   }
  // }

  // get selectionitemFilter() {
  //   return this._selectionitemFilter;
  // }

  // set selectionitemFilter(selectionitemFilter) {
  //   if (!isNullOrUndefined(selectionitemFilter)) {
  //     this._selectionitemFilter = selectionitemFilter;
  //   }
  // }

  // get selectionlistFilter() {
  //   return this._selectionlistFilter;
  // }

  // set selectionlistFilter(selectionlistFilter) {
  //   if (!isNullOrUndefined(selectionlistFilter)) {
  //     this._selectionlistFilter = selectionlistFilter;
  //   }
  // }

  private _listoftIndex;
  get listoftIndex() {
    return this._listoftIndex;
  }

  set listoftIndex(listoftIndex) {
    if (!isNullOrUndefined(listoftIndex)) {
      this._listoftIndex = listoftIndex;
    }
  }

  constructor(private snackBar: MatSnackBar, private authService: AuthService) {
    this.loadedStr = new BehaviorSubject('');
    this.hasError = new BehaviorSubject(false);
  }

  dateFromIsoString(str: string | Date) {
    // 2018-09-14T23:00:00
    if (typeof str === 'string') {
      let _arr = str.split('.')[0].split('T');
      let arr = _arr[0].split('-').concat(_arr[1].split(':')); // yyyy mm dd hh mm ss
      return new Date(+arr[0], +arr[1] - 1, +arr[2], +arr[3], +arr[4]);
    } else {
      return new Date(str);
    }
  }
  dateFromDateString(str: string | Date) {
    if (typeof str === 'string') {
      let arr = str.split('-');
      return new Date(+arr[0], +arr[1] - 1, +arr[2]);
    } else {
      return str;
    }
  }
  isImgtype(filename: string): boolean {
    let res = false;
    switch (filename.toLowerCase().split('.').reverse()[0]) {
      case 'png':
      case 'gif':
      case 'jpg':
      case 'jpeg':
      case 'bmp':
      case 'tif':
      case 'tiff':
        res = true;
        break;
      default:
        res = false;
        break;
    }
    return res;
  }
  isVideotype(filename: string): boolean {
    let res = false;
    switch (filename.toLowerCase().split('.').reverse()[0]) {
      // case 'avi':
      case 'mp4':
      case 'mkv':
      case 'ogg':
      case 'ogv':
        res = true;
        break;
      default:
        res = false;
        break;
    }
    return res;
  }
  isAudiotype(filename: string): boolean {
    let res = false;
    switch (filename.toLowerCase().split('.').reverse()[0]) {
      case 'mp3':
      case 'wav':
        // case 'ogg':
        res = true;
        break;
      default:
        res = false;
        break;
    }
    return res;
  }
  isPdftype(filename: string): boolean {
    let res = false;
    switch (filename.toLowerCase().split('.').reverse()[0]) {
      case 'pdf':
        // case 'ogg':
        res = true;
        break;
      default:
        res = false;
        break;
    }
    return res;
  }

  exceptionHandling(err) {
    if (err.status === 401) {
      this.authService.logout();
      this.openSnackBar('Ошибка авторизации', 'Ok');
      console.error(err);
    } else if (err.status === 409) {
      this.openSnackBar(`${err.error['error']}`, 'Ok');
    } else {
      console.error(err);
    }
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 5000,
    });
  }
}

@Injectable() // перевод пагинатора
export class MatPaginatorIntlRus extends MatPaginatorIntl {
  itemsPerPageLabel = 'Строк на странице: ';
  nextPageLabel = 'Далее';
  previousPageLabel = 'Назад';

  getRangeLabel = (page: number, pageSize: number, length: number) => {
    return (
      page * pageSize +
      1 +
      ' - ' +
      (page * pageSize + pageSize) +
      ' из ' +
      length
    );
  }
}
