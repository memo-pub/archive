import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { TextMaskModule } from 'angular2-text-mask';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatChipsModule } from '@angular/material/chips';
import { MatNativeDateModule, MAT_DATE_LOCALE, DateAdapter, MAT_DATE_FORMATS } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatTabsModule } from '@angular/material/tabs';
import { MatTooltipModule } from '@angular/material/tooltip';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DisablecontrolDirective } from '../system/shared/directives/disablecontrol.directive';
import { FiledropDirective } from '../system/shared/directives/filedrop.directive';
import { TrimtextDirective } from '../system/shared/directives/trimtext.directive';
import { ImageCropperModule } from 'ngx-image-cropper';
import { PaginationComponent } from '../system/shared/components/pagination/pagination.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { LightboxComponent } from '../system/shared/components/lightbox/lightbox.component';

export const MY_FORMATS = {
  parse: {
    dateInput: 'DD.MM.YYYY'
  },
  display: {
    dateInput: 'DD.MM.YYYY',
    monthYearLabel: 'MM YYYY',
    fullPickerInput: 'DD.MM.YYYY',
    datePickerInput: 'DD.MM.YYYY'
  }
};

@NgModule({
  declarations: [
    DisablecontrolDirective,
    TrimtextDirective,
    FiledropDirective,
    PaginationComponent,
    LightboxComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatInputModule,
    MatButtonModule,
    MatCheckboxModule,
    MatTabsModule,
    MatSelectModule,
    MatCardModule,
    MatTooltipModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatButtonToggleModule,
    MatSnackBarModule,
    MatExpansionModule,
    MatSortModule,
    MatAutocompleteModule,
    MatDialogModule,
    MatListModule,
    MatPaginatorModule,
    MatSlideToggleModule,
    MatProgressBarModule,
    MatChipsModule,
    TextMaskModule,
    NgbModule,
    MatRadioModule,
    ImageCropperModule,
    NgxSpinnerModule,
    DragDropModule
  ],
  exports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatInputModule,
    MatButtonModule,
    MatCheckboxModule,
    MatTabsModule,
    MatSelectModule,
    MatCardModule,
    MatTooltipModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatButtonToggleModule,
    MatSnackBarModule,
    MatExpansionModule,
    MatSortModule,
    MatAutocompleteModule,
    MatDialogModule,
    MatListModule,
    MatPaginatorModule,
    MatSlideToggleModule,
    MatProgressBarModule,
    MatChipsModule,
    NgbModule,
    DisablecontrolDirective,
    TrimtextDirective,
    TextMaskModule,
    FiledropDirective,
    MatRadioModule,
    ImageCropperModule,
    PaginationComponent,
    NgxSpinnerModule,
    DragDropModule,
    LightboxComponent
  ],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'ru' },
    // { provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: { useUtc: true } },
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE]
    },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS }
    // { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },

    // { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ]
})
export class SharedModule {}
