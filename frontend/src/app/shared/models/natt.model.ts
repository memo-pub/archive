export class NatT {
  constructor(
    public id?: number,
    public nation?: string,
    public nation_rmk?: string
  ) {}
}

export const natT_width = {
  nation_rmk: 60,
  nation: 12
};
