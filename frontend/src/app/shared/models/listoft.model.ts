export interface ListOfT {
  id?: number;
  death_type?: string;
  death_rmk?: string;
  name_ent?: string;
  name_rmk?: string;
  name_f?: string;
}

export const listOfT_width = {
  death_type: 4,
  death_rmk: 60,
  name_ent: 40,
  name_rmk: 60,
  name_f: 20
};

// СОвмещен с death_reason_t
// export class ListOfT {
//   constructor(
//     public id?: number,
//     public name_f?: string,
//     public name_ent?: string,
//     public name_rmk?: string,
//     public death_type?: string,
//     public death_rmk?: string
//   ) { }

// }
