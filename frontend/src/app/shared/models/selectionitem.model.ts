export interface SelectionItem {
  id?: number;
  selection_id?: number;
  personal_code?: number;
  fund?: number;
  list_no?: number;
  file_no?: number;
  file_no_ext?: number;
  sour_cnt?: number;
  surname?: string;
  surname_rmk?: string;
  fname?: string;
  lname?: string;
  birth?: Date;
  birth_rmk?: string;
}

export const selectionItem_width = {
  personal_t_surname: 5000,
  personal_t_fname: 5000,
  personal_t_lname: 5000,
  personal_t_birth_rmk: 5000,
  personal_t_death_date_rmk: 5000,
  personal_t_birth_place: 5000,
  personal_t_citiz: 5000,
  personal_t_origin_rmk: 5000,
  personal_t_educ_rmk: 5000,
  personal_t_spec_rmk: 5000,
  personal_t_place: 5000,
  personal_t_address: 5000,
  personal_t_phone: 5000,
  personal_t_notice: 5000,
  personal_t_commentariy: 5000,
  activity_t_prof: 5000,
  activity_t_post: 5000,
  activity_t_arrival_dat_rmk: 5000,
  activity_t_depart_dat_rmk: 5000,
  activity_t_ent1_rmk: 5000,
  activity_t_place: 5000,
  org_t_org_name: 5000,
  org_t_particip_rmk: 5000,
  org_t_com_rmk: 5000,
  org_t_join_dat_rmk: 5000,
  org_t_dismis_dat_rmk: 5000,
  repress_t_repress_type_rmk: 5000,
  repress_t_repress_rmk: 5000,
  repress_t_place: 5000,
  repress_t_repress_dat_rmk: 5000,
  repress_t_reabil_rmk: 5000,
  repress_t_rehabil_org: 5000,
  court_t_inq_name: 5000,
  court_t_court_name: 5000,
  court_t_article: 5000,
  court_t_place_i: 5000,
  court_t_place_j: 5000,
  court_t_trial_rmk: 5000,
  court_t_inq_rmk: 5000,
  court_t_inq_date_rmk: 5000,
  court_t_sentense_date_rmk: 5000,
  impris_t_prison_name: 5000,
  impris_t_place: 5000,
  impris_t_arrival_dat_rmk: 5000,
  impris_t_leave_dat_rmk: 5000,
  impris_t_reason_rmk: 5000,
  impris_t_work: 5000,
  impris_t_protest_rmk: 5000,
  sour_t_sour_name: 5000,
  sour_t_sour_rmk: 5000,
  sour_t_sour_date: 5000,
  sour_t_place_so: 5000,
  text_t_text_line: 5000,
  help_t_help_typ_rmk: 5000,
  help_t_whom: 5000,
  co_t_surname: 5000,
  co_t_fname: 5000,
  co_t_lname: 5000,
  activity_t_sphere: 5000,
};
// export class SelectionItem {
//   constructor(
//     public id?: number,
//     public selection_id?: number,
//     public personal_code?: number,
//     public fund?: number,
//     public list_no?: number,
//     public file_no?: number,
//     public sour_cnt?: number,
//     public surname?: string,
//     public fname?: string,
//     public lname?: string,
//     public birth?: Date,
//     public birth_rmk?: string
//   ) {}
// }
