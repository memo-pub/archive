export interface InformT {
  id?: number;
  crt_id?: number;
  human_code?: number;
  role?: string;
  role_rmk?: string;
  inform_dat?: Date;
}

export const informT_width = {
  role_rmk: 240,
  role: 30
};
// export class InformT {
// 	constructor(
// 		public id?: number,
// 		public crt_id?: number,
// 		public human_code?: number,
// 		public role?: string,
// 		public role_rmk?: string,
// 		public inform_dat?: Date
// 	) { }
// }
