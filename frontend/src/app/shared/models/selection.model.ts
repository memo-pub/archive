export class Selection {
  constructor(public id?: number, public name?: string, public sdate?: Date) {}
}

export class SelectionFilter {
  constructor(
    public searchStr?: string,
    public page?: number,
    public searchOrder?: string
  ) {
    this.searchStr = '';
    this.searchOrder = 'bysdatedesc';
    this.page = 1;
  }
}
