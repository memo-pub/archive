export interface Muser {
  token?: string;
  id?: number;
  password?: string;
  name?: string;
  login?: string;
  surname?: string;
  usertype?: number;
  isadmin?: boolean;
}

export const muser_width = {
  name: 100,
  login: 20,
  surname: 100,
};

export class MuserFilter {
  constructor(public searchStr?: string, public page?: number) {
    this.searchStr = '';
    this.page = 1;
  }
}

export class FieldjoinFilter {
  constructor(public searchStr?: string, public page?: number) {
    this.searchStr = '';
    this.page = 1;
  }
}

// export class Muser {
//   constructor(
//     public token?: string,
//     public id?: number,
//     public login?: string,
//     public password?: string,
//     public name?: string,
//     public surname?: string,
//     public usertype?: number,
//     public isadmin?: boolean
//   ) {}
// }
