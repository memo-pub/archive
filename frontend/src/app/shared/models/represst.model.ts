export interface RepressT {
  id?: number;
  personal_code?: number;
  repress_no?: number;
  repress_type_id?: number;
  repress_dat?: Date;
  repress_dat_begin?: Date;
  repress_dat_end?: Date;
  reabil_dat?: Date;
  reabil_dat_begin?: Date;
  reabil_dat_end?: Date;
  geoplace_code_rmk?: string;
  repress_type_rmk?: string;
  repress_dat_rmk?: string;
  geoplace_code?: string;
  // repress_type?: string;
  repress_rmk?: string;
  reabil_mark?: string;
  court_mark?: string;
  reabil_rmk?: string;
  place_num?: number;
}

export const repressT_width = {
  geoplace_code_rmk: 240,
  repress_type_rmk: 240,
  repress_dat_rmk: 240,
  //   geoplace_code: 255,
  //   repress_type: 255,
  repress_rmk: 240,
  //   court_mark: 255,
  reabil_rmk: 240,
};

// export class RepressT {
// 	constructor(
// 		public id?: number,
// 		public personal_code?: number,
// 		public repress_no?: number,
// 		public repress_type_id?: number,
// 		public repress_type?: string,
// 		public court_mark?: string,
// 		public repress_dat?: Date,
// 		public geoplace_code?: string,
// 		public reabil_mark?: string,
// 		public reabil_dat?: Date,
// 		public repress_type_rmk?: string,
// 		public repress_dat_rmk?: string,
// 		public geoplace_code_rmk?: string,
// 		public reabil_rmk?: string,
// 		public repress_rmk?: string,
// 		public place_num?: number
// 	) { }

// }
