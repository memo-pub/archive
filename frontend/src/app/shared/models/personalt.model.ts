export interface PersonalT {
  code?: number;
  mentioned_person?: PersonalT;
  birth?: Date;
  birth_begin?: Date;
  birth_end?: Date;
  nation_id?: number;
  origin_id?: number; // id из LIST_OF_T-ORIGIN
  educ_id?: number; // id из LIST_OF_T-educ
  spec_id?: number; // id из LIST_OF_T-spec
  death?: Date;
  death_begin?: Date;
  death_end?: Date;
  in_date?: Date;
  place_num?: number; // PLACE_T-PLACE_NUM
  birth_place_num?: number; // PLACE_T-PLACE_NUM
  death_place_num?: number; // PLACE_T-PLACE_NUM
  in_activity_t?: boolean;
  in_org_t?: boolean;
  in_repress_t?: boolean;
  in_court_t?: boolean;
  in_impris_t?: boolean;
  in_inform_t?: boolean;
  in_family_t?: boolean;
  in_mentioned_t?: boolean;
  in_repress_t_reab?: boolean;
  mlog?: any[];
  sex?: string;
  url?: string;
  url2?: string;
  url3?: string;
  mark?: string;
  educ?: string; // сокращенное название из LIST_OF_T-educ
  spec?: string; // сокращенное название из LIST_OF_T-spec
  fund?: string;
  fname?: string;
  lname?: string;
  citiz?: string;
  phone?: string;
  email?: string;
  type_r?: string;
  type_i?: string;
  type_a?: string;
  published?: string;
  pub_change_date?: string;
  mustPublish?: string;
  mustUnpublish?: string;
  origin?: string; // сокращенное название из LIST_OF_T-ORIGIN
  notice?: string;
  public_rmk?: string;
  surname?: string;
  address?: string;
  list_no?: string;
  file_no?: string;
  file_no_ext?: string;
  educ_rmk?: string;
  spec_rmk?: string;
  fname_rmk?: string;
  lname_rmk?: string;
  phone_rmk?: string;
  birth_rmk?: string;
  citiz_rmk?: string;
  death_mark?: string;
  nation_rmk?: string;
  death_type?: string;
  origin_rmk?: string;
  address_rmk?: string;
  surname_rmk?: string;
  birth_place?: string; // PLACE_T-PLACE_CODE
  death_date_rmk?: string;
  residence_code?: string; // PLACE_T-PLACE_CODE
  birth_place_rmk?: string;
  death_place_rmk?: string;
  real_surname_rmk?: string;
  residence_code_rmk?: string;
  general_rmk?: string;
}

export const personalT_width = {
  address: 240,
  address_rmk: 240,
  birth_place_rmk: 240,
  birth_rmk: 240,
  citiz: 20,
  citiz_rmk: 240,
  death_date_rmk: 240,
  death_mark: 1,
  death_place_rmk: 240,
  death_type: 4,
  educ_rmk: 240,
  email: 320,
  fname: 40,
  fname_rmk: 240,
  lname: 40,
  lname_rmk: 240,
  mark: 1,
  published: 1,
  nation_rmk: 240,
  notice: 240,
  origin_rmk: 240,
  phone: 20,
  phone_rmk: 30,
  real_surname_rmk: 240,
  residence_code: 18,
  residence_code_rmk: 240,
  sex: 1,
  spec_rmk: 240,
  surname: 50,
  surname_rmk: 500,
  public_rmk: 500,
  type_a: 1,
  type_i: 1,
  type_r: 1,
  us_name: 30,
  url: 500,
  general_rmk: 500,
  link_id: 50,
  link_id_rmk: 100,
  fund: 4,
  list_no: 4,
  file_no: 5,
  file_no_ext: 10,
};

export class PersonalTFilter {
  constructor(
    public searchStr?: string,
    public searchStr_fond?: string,
    public searchStr_opis?: string,
    public searchStr_delo?: string,
    public searchStr_delo_ext?: string,
    public searchOrder?: string,
    public filterExactExt?: boolean,
    public filterExact?: boolean,
    public page?: number,
    public newFond?: number,
    public newOpis?: number
  ) {
    this.searchStr = '';
    this.searchStr_fond = '';
    this.searchStr_opis = '';
    this.searchStr_delo = '';
    this.searchStr_delo_ext = '';
    this.searchOrder = 'bycode';
    this.filterExactExt = false;
    this.filterExact = false;
    this.page = 1;
    this.newFond = null;
    this.newOpis = null;
  }
}

// export class PersonalT {
//   constructor(
//     public code?: number,
//     public mark?: string,
//     public type_r?: string,
//     public type_i?: string,
//     public type_a?: string,
//     public surname?: string,
//     public surname_rmk?: string,
//     public fname?: string,
//     public fname_rmk?: string,
//     public lname?: string,
//     public lname_rmk?: string,
//     public sex?: string,
//     public birth?: Date,
//     public birth_place?: string, // PLACE_T-PLACE_CODE
//     public nation_id?: number,
//     public citiz?: string,
//     public origin?: string, // сокращенное название из LIST_OF_T-ORIGIN
//     public educ?: string, // сокращенное название из LIST_OF_T-educ
//     public spec?: string, // сокращенное название из LIST_OF_T-spec
//     public origin_id?: number, // id из LIST_OF_T-ORIGIN
//     public educ_id?: number, // id из LIST_OF_T-educ
//     public spec_id?: number, // id из LIST_OF_T-spec
//     public death_mark?: string,
//     public death?: Date,
//     public birth_rmk?: string,
//     public birth_place_rmk?: string,
//     public death_place_rmk?: string,
//     public nation_rmk?: string,
//     public citiz_rmk?: string,
//     public origin_rmk?: string,
//     public educ_rmk?: string,
//     public spec_rmk?: string,
//     public death_date_rmk?: string,
//     public residence_code?: string, // PLACE_T-PLACE_CODE
//     public address?: string,
//     public phone?: string,
//     public email?: string,
//     public residence_code_rmk?: string,
//     public address_rmk?: string,
//     public phone_rmk?: string,
//     public notice?: string,
//     public in_date?: Date,
//     public place_num?: number, // PLACE_T-PLACE_NUM
//     public birth_place_num?: number, // PLACE_T-PLACE_NUM
//     public death_place_num?: number, // PLACE_T-PLACE_NUM
//     public death_type?: string,
//     public real_surname_rmk?: string,
//     public fund?: string,
//     public list_no?: string,
//     public file_no?: string,
//     public in_activity_t?: boolean,
//     public in_org_t?: boolean,
//     public in_repress_t?: boolean,
//     public in_court_t?: boolean,
//     public in_impris_t?: boolean,
//     public in_inform_t?: boolean,
//     public in_family_t?: boolean,
//     public mlog?: any[]
//   ) {}
// }
