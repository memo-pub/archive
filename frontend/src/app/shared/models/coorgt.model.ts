export interface CoorgT {
  id?: number;
  org_id?: number;
  human_code?: number;
  role?: string;
  role_rmk?: string;
  // sign_mark?: string;
}
export const coorgT_width = {
  role: 30,
  role_rmk: 240
  // sign_mark: 255
};
// export class CoorgT {
// 	constructor(
// 		public id?: number,
// 		public org_id?: number,
// 		public human_code?: number,
// 		public role?: string,
// 		public role_rmk?: string,
// 		public sign_mark?: string
// 	) {	}

// }
