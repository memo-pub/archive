export interface FamilyT {
  id?: number;
  personal_code?: number;
  human_code?: number;
  role?: string;
  role_rmk?: string;
}

export const familyT_width = {
  role: 30,
  role_rmk: 240
  // sign_mark: 255
};

// export class FamilyT {
// 	constructor(
// 		public id?: number,
// 		public personal_code?: number,
// 		public human_code?: number,
// 		public role?: string ,
// 		public role_rmk?: string,
// 	) { }

// }
