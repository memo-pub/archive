export interface HelpT {
  id?: number;
  personal_code?: number;
  help_no?: number;
  help_dat?: Date;
  help_typ_rmk?: string;
  help_typ?: string;
  whom_rmk?: string;
  whom?: string;
}

export const helpT_width = {
  help_typ_rmk: 240,
//   help_typ: 255,
  whom_rmk: 240,
  whom: 30
};
// export class HelpT {
//   constructor(
//     public id?: number,
//     public personal_code?: number,
//     public help_no?: number,
//     public help_dat?: Date,
//     public help_typ_rmk?: string,
//     public help_typ?: string,
//     public whom_rmk?: string,
//     public whom?: string
//   ) {}
// }
