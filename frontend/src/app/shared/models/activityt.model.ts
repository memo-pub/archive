export interface ActivityT {
  id?: number;
  personal_code?: number;
  act_no?: number;
  arrival_dat?: Date;
  arrival_dat_begin?: Date;
  arrival_dat_end?: Date;
  depart_dat?: Date;
  depart_dat_begin?: Date;
  depart_dat_end?: Date;
  // rep_mark?: number;
  rep_id?: number;
  // range_mark?: number;
  sphere_rmk?: string;
  prof_rmk?: string;
  post_rmk?: string;
  ent1_rmk?: string;
  geoplace_code_rmk?: string;
  arrival_dat_rmk?: string;
  depart_dat_rmk?: string;
  // geoplace_code?: string;
  // ent_code?: string;
  sphere?: string;
  prof?: string;
  post?: string;
}

export const activityT_width = {
  sphere_rmk: 240,
  prof_rmk: 240,
  ent1_rmk: 240,
  geoplace_code_rmk: 240,
  arrival_dat_rmk: 240,
  depart_dat_rmk: 240,
  prof: 30,
  post: 200,
  general_rmk: 200,
};
// export class ActivityT {
//   constructor(
//     public id?: number,
//     public personal_code?: number,
//     public act_no?: number,
//     public sphere?: string,
//     public prof?: string,
//     public post?: string,
//     public arrival_dat?: Date,
//     public depart_dat?: Date,
//     public sphere_rmk?: string,
//     public prof_rmk?: string,
//     public post_rmk?: string,
//     public ent1_rmk?: string,
//     public arrival_dat_rmk?: string,
//     public depart_dat_rmk?: string,
//     public ent_code?: string,
//     public geoplace_code?: string,
//     public geoplace_code_rmk?: string,
//     public range_mark?: number,
//     public rep_mark?: number,
//     public rep_id?: number,
//     public maxWidth?: any
//   ) {
//     this.maxWidth = { test: 1000 };
//   }
// }
