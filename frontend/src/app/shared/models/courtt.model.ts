export interface CourtT {
  id?: number;
  personal_code?: number;
  rep_id?: number;
  trial_no?: number;
  inq_date?: Date;
  inq_date_begin?: Date;
  inq_date_end?: Date;
  court_name?: string;
  sentense_date?: Date;
  sentense_date_begin?: Date;
  sentense_date_end?: Date;
  place_num_i?: number;
  place_num_j?: number;
  trial_rmk?: string;
  article?: string;
  sentense?: string;
  inq_name_rmk?: string;
  inq_date_rmk?: string;
  court_name_rmk?: string;
  sentense_date_rmk?: string;
  article_rmk?: string;
  sentense_rmk?: string;
  inq_rmk?: string;
  inq_name?: string;
  place_i_rmk?: string;
  place_j_rmk?: string;
}

export const courtT_width = {
  sentense_date_rmk: 240,
  court_name_rmk: 240,
  inq_name_rmk: 240,
  inq_date_rmk: 240,
  sentense_rmk: 240,
  article_rmk: 240,
  place_i_rmk: 240,
  place_j_rmk: 240,
  trial_rmk: 240,
  // sign_mark: 255
  inq_name: 60,
  article: 100,
  inq_rmk: 240,
};
// export class CourtT {
// 	constructor(
// 		public id?: number,
// 		public personal_code?: number,
// 		public rep_id?: number,
// 		public trial_no?: number,
// 		public inq_name?: string,
// 		public inq_date?: Date,
// 		public court_name?: string,
// 		public sentense_date?: Date,
// 		public article?: string,
// 		public sentense?: string,
// 		public inq_name_rmk?: string,
// 		public inq_date_rmk?: string,
// 		public court_name_rmk?: string,
// 		public trial_rmk?: string,
// 		public sentense_date_rmk?: string,
// 		public article_rmk?: string,
// 		public sentense_rmk?: string,
// 		public inq_rmk?: string,
// 		public place_num_i?: number,
// 		public place_num_j?: number,
// 		public place_i_rmk?: string,
// 		public place_j_rmk?: string
// 	) { }
// }
