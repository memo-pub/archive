export interface OrgT {
  id?: number;
  personal_code?: number;
  org_no?: number;
  join_dat?: Date;
  join_dat_begin?: Date;
  join_dat_end?: Date;
  dismis_dat?: Date;
  dismis_dat_begin?: Date;
  dismis_dat_end?: Date;
  com_rmk?: string;
  particip?: string;
  // org_code?: string;
  org_name?: string;
  ent_name_rmk?: string;
  join_dat_rmk?: string;
  particip_rmk?: string;
  dismis_dat_rmk?: string;
}

export const orgT_width = {
  com_rmk: 240,
  // particip: 255,
  // org_code: 255,
  // org_name: 255,
  ent_name_rmk: 240,
  join_dat_rmk: 240,
  particip_rmk: 240,
  dismis_dat_rmk: 240,
};

// export class OrgT {
//   constructor(
//     public id?: number,
//     public personal_code?: number,
//     public org_no?: number,
//     public join_dat?: Date,
//     public dismis_dat?: Date,
//     public com_rmk?: string,
//     public particip?: string,
//     public org_code?: string,
//     public org_name?: string,
//     public ent_name_rmk?: string,
//     public join_dat_rmk?: string,
//     public particip_rmk?: string,
//     public dismis_dat_rmk?: string
//   ) { }
// }
