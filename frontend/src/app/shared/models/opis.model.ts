export interface Opis {
  id?: number;
  opis_name?: number;
  fond?: number;
  trudindex?: number;
  trudindex_scann?: number;
  description?: string;
  fond_description?: string;
  finished?: boolean;
  help?: string;
  name?: string;
  access_lim?: string;
  form_date?: string;
  storage_org?: string;
  doc_begin?: number;
  doc_end?: number;
  storage_capacity: string;
  creators_name: string;
  admin_biog_history: string;
  arch_history: string;
  donation_source: string;
  mat_organ_system: string;
  archivist_rmk: string;
  descrip_date: string;
  published?: string;
  pub_date?: Date;
  pub_change_date?: Date;
  mustPublish?: string;
  mustUnpublish?: string;
}

export const opis_width = {
  fond_description: 255,
  description: 2000,
  help: 1000,
  access_lim: 200,
  storage_org: 1000,
  name: 500,
  form_date: 100,
  doc_begin: 4,
  doc_end: 4,
  storage_capacity: 200,
  creators_name: 200,
  admin_biog_history: 5000,
  arch_history: 2000,
  donation_source: 1000,
  mat_organ_system: 1000,
  archivist_rmk: 200,
  descrip_date: 200,
  published: 1,
};

export class OpisFilter {
  constructor(public searchStr?: string, public page?: number) {
    this.searchStr = '';
    this.page = 1;
  }
}

// export class Opis {
//   constructor(
//     public id?: number,
//     public opis_name?: number,
//     public fond?: number,
//     public trudindex?: number,
//     public trudindex_scann?: number,
//     public description?: string,
//     public fond_description?: string
//   ) {}
// }
