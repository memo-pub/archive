export interface UsageT {
  id?: number;
  archivefolder_id?: number;
  us_no?: number;
  us_date?: Date;
  us_date_begin?: Date;
  us_date_end?: Date;
  state_type?: string;
  sour_code?: string;
  sour_txt?: string;
  // us_type?: string;
  us_type_id?: string;
}

export const usageT_width = {
  state_type: 4,
  sour_code: 80,
  sour_txt: 240,
  us_type: 80,
};
// export class UsageT {
//   constructor(
//     public id?: number,
//     public personal_code?: number,
//     public us_no?: number,
//     public us_type?: string,
//     public us_date?: Date,
//     public sour_code?: string,
//     public sour_txt?: string,
//     public state_type?: string
//   ) { }

// }
