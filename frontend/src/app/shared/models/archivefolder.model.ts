export interface Archivefolder {
  id?: number;
  fond?: number;
  opis?: number;
  opis_id?: number;
  scanuser_id?: number;
  delo?: number;
  not_scanned_rmk?: string;
  scan_comment?: string;
  description?: string;
  delo_extra?: string;
  postfix?: string;
  comment?: string;
  date?: string;
  name?: string;
  url?: string;
  page_cnt?: number;
  spec_page_cnt?: number;
  doc_cnt?: number;
  foto_cnt?: number;
  have_scanned?: boolean;
  have_memories?: boolean;
  have_arts?: boolean;
  digital?: boolean;
  have_act?: boolean;
  status?: number;
  not_scanned?: boolean;
  createduser_id?: number;
  doc_begin?: number;
  doc_end?: number;
  pub_date?: Date;
  pub_change_date?: Date;
  mustPublish?: string;
  mustUnpublish?: string;
  page_num_cnt?: string;
  descrip_type?: string;
  descrip_type_rmk?: string;
}

export const archivefolder_width = {
  not_scanned_rmk: 500,
  descrip_type_rmk: 240,
  scan_comment: 5000,
  description: 5000,
  delo_extra: 20,
  comment: 5000,
  date: 50,
  name: 500,
  url: 1000,
  doc_begin: 4,
  doc_end: 4,
  page_num_cnt: 30,
  published: 1,
};

export class ArchivefolderFilter {
  constructor(
    public searchStr?: string,
    public searchFond?: any[],
    public searchOpis?: any[],
    public searchflds?: any[],
    public searchOrder?: string,
    public searchtxt?: string,
    public searchStatus?: number | null,
    public searchAct?: boolean | null,
    public searchMem?: boolean | null,
    public searchArt?: boolean | null,
    public searchDig?: boolean | null,
    public searchUnPhoto?: number | null,
    public searchUnDoc?: number | null,
    public page?: number,
    public descrip_type?: string,
    public filterExact?: boolean
  ) {
    this.searchStr = '';
    this.searchtxt = '';
    this.searchFond = [];
    this.searchOpis = [];
    this.searchflds = [];
    this.searchOrder = 'byshifr';
    this.searchStatus = null;
    this.searchAct = null;
    this.searchMem = null;
    this.searchArt = null;
    this.searchDig = null;
    this.searchUnPhoto = null;
    this.searchUnDoc = null;
    this.page = 1;
    this.descrip_type = null;
    this.filterExact = false;
  }
}

export class Archivefolder {
  constructor(
    public id?: number,
    public opis_name?: number,
    public fond?: number,
    public opis?: number,
    public opis_id?: number,
    public scanuser_id?: number,
    public delo?: number,
    public postfix?: string,
    public descrip_type?: string,
    public descrip_type_rmk?: string,
    public name?: string,
    public description?: string,
    public delo_extra?: string,
    public page_cnt?: number,
    public spec_page_cnt?: number,
    public doc_cnt?: number,
    public foto_cnt?: number,
    public have_scanned?: boolean,
    public have_memories?: boolean,
    public have_arts?: boolean,
    public digital?: boolean,
    public have_act?: boolean,
    public comment?: string,
    public date?: string,
    public status?: number,
    public url?: string,
    public not_scanned?: boolean,
    public not_scanned_rmk?: string,
    public scan_comment?: string,
    public createduser_id?: number,
    public peronals_main?: any[],
    public peronals_second?: any[],
    public published?: string,
    public archive_folder_log?: any[]
  ) {}
}
