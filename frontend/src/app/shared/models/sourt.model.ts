import { ListOfT } from './listoft.model';

export interface SourT {
  id?: number;
  sour_id?: number;
  pub_type?: number;
  reprod_type_ids?: string;
  reprod_rmk?: string;
  personal_code?: number;
  sour_no?: number;
  file_cnt?: number;
  parent_id?: number;
  parent_data?: SourT;
  children?: SourT[];
  sour_name?: string;
  sour_code?: string;
  mark_mem?: string;
  place_so?: string;
  sour_rmk?: string;
  acs_rmk?: string;
  category?: string;
  sour_type_join?: string;
  fund?: string;
  list_no?: string;
  file_no?: string;
  extra_person?: string[];
  file_no_ext?: string;
  // sour_type?: string;
  sour_mark?: string;
  sour_date?: string;
  sour_date_begin?: Date;
  sour_date_end?: Date;
  folder_file?: any[];
  personals?: any[];
  checkToDel?: boolean;
  published?: string;
  pub_change_date?: Date;
  mustPublish?: string;
  mustUnpublish?: string;
}

export const sourT_width = {
  sour_name: 250,
  sour_code: 40,
  place_so: 30,
  sour_rmk: 240,
  acs_rmk: 240,
  reprod_rmk: 240,
  // category: 255,
  // sour_type: 20,
  sour_date: 250,
  list_no: 4,
  file_no: 5,
  file_no_ext: 10,
  fund: 4,
  published: 1,
};

export class SourtFilter {
  constructor(
    public searchOrder?: string,
    public fund?: string,
    public list_no?: string,
    public file_no?: string,
    public file_no_ext?: string,
    public file_no_ext_noneed?: boolean,
    public sour_name_filter?: string,
    public sour_type_filter?: number,
    public searchflds?: any[],
    public searchtxt?: string,
    public pub_type?: number,
    public filter_sour_date_from?: string,
    public filter_sour_date_to?: string,
    public filter_sour_date_from_date?: Date,
    public filter_sour_date_to_date?: Date,
    public page?: number
  ) {
    this.searchOrder = 'bypos';
    this.fund = null;
    this.list_no = null;
    this.file_no = null;
    this.file_no_ext = null;
    this.file_no_ext_noneed = false;
    this.sour_name_filter = null;
    this.sour_type_filter = null;
    this.searchflds = [];
    this.searchtxt = null;
    this.pub_type = null;
    this.filter_sour_date_from = null;
    this.filter_sour_date_to = null;
    this.page = 1;
  }
}
// export class SourT {
//   constructor(
//     public id?: number,
//     public personal_code?: number,
//     public sour_no?: number,
//     public sour_name?: string,
//     public sour_code?: string,
//     public mark_mem?: string,
//     public place_so?: string,
//     public sour_rmk?: string,
//     public category?: string,
//     public fund?: string,
//     public list_no?: string,
//     public file_no?: string,
//     public sour_type?: string,
//     public sour_mark?: string,
//     public sour_date?: string
//   ) { }

// }
