export interface ImprisT {
  id?: number;
  crt_id?: number;
  personal_code?: number;
  impris_no?: number;
  // impris_type?: string;
  impris_type_id?: number;
  arrival_dat?: Date;
  arrival_dat_begin: Date;
  arrival_dat_end?: Date;
  leave_dat?: Date;
  leave_dat_begin?: Date;
  leave_dat_end?: Date;
  prison_place_rmk?: string;
  prison_name_rmk?: string;
  arrival_dat_rmk?: string;
  leave_dat_rmk?: string;
  protest_rmk?: string;
  prison_name?: string;
  reason_rmk?: string;
  work_rmk?: string;
  protest?: string;
  reason?: string;
  work?: string;
  place_num?: number;
}

export const imprisT_width = {
  general_rmk: 240,
  prison_place_rmk: 240,
  prison_name_rmk: 240,
  arrival_dat_rmk: 240,
  leave_dat_rmk: 240,
  protest_rmk: 240,
  prison_name: 230,
  reason_rmk: 240,
  work_rmk: 240,
  //   protest: 255,
  //   reason: 255,
  work: 30,
};
// export class ImprisT {
// 	constructor(
// 		public id?: number,
// 		public crt_id?: number,
// 		public personal_code?: number,
// 		public impris_no?: number,
// 		public impris_type?: string,
// 		public impris_type_id?: number,
// 		public arrival_dat?: Date,
// 		public leave_dat?: Date,
// 		public prison_place_rmk?: string,
// 		public prison_name_rmk?: string,
// 		public arrival_dat_rmk?: string,
// 		public leave_dat_rmk?: string,
// 		public protest_rmk?: string,
// 		public prison_name?: string,
// 		public reason_rmk?: string,
// 		public work_rmk?: string,
// 		public protest?: string,
// 		public reason?: string,
// 		public work?: string,
// 		public place_num?: number
// 	) { }
// }
