export interface CoactT {
  id?: number;
  act_id?: number;
  human_code?: number;
  role?: string;
  role_rmk?: string;
  // sign_mark?: string;
}

export const coactT_width = {
  role: 30,
  role_rmk: 240
  // sign_mark: 255
};

// export class CoactT {
//   constructor(
//     public id?: number,
//     public act_id?: number,
//     public human_code?: number,
//     public role?: string,
//     public role_rmk?: string,
//     public sign_mark?: string
//   ) {}
// }
