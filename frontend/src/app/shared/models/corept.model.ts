export interface CorepT {
  id?: number;
  rep_id?: number;
  human_code?: number;
  role?: string;
  role_rmk?: string;
  // sign_mark?: string;
  cotype?: number;
}
export const corepT_width = {
  role: 30,
  role_rmk: 240
  // sign_mark: 255
};
// export class CorepT {
// 	constructor(
// 		public id?: number,
// 		public rep_id?: number,
// 		public human_code?: number,
// 		public role?: string,
// 		public role_rmk?: string,
// 		public sign_mark?: string
// 	) {	}

// }
