export interface CoimpT {
  id?: number;
  imp_id?: number;
  human_code?: number;
  role?: string;
  role_rmk?: string;
  // sign_mark?: string;
}
export const coimpT_width = {
  role: 30,
  role_rmk: 240
  // sign_mark: 255
};

// export class CoimpT {
//   constructor(
//     public id?: number,
//     public imp_id?: number,
//     public human_code?: number,
//     public role?: string,
//     public role_rmk?: string,
//     public sign_mark?: string
//   ) {}
// }
