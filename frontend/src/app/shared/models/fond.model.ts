export interface Fond {
  storage_capacity: string;
  creators_name: string;
  admin_biog_history: string;
  arch_history: string;
  donation_source: string;
  mat_organ_system: string;
  archivist_rmk: string;
  descrip_date: string;
  id?: number;
  fond?: number;
  description?: string;
  description_long?: string;
  help?: string;
  access_lim?: string;
  form_date?: string;
  doc_begin?: number;
  doc_end?: number;
  published?: string;
  mustPublish?: string;
  mustUnpublish?: string;
  pub_change_date?: Date;
}

export const fond_width = {
  description: 500,
  description_long: 2000,
  help: 1000,
  access_lim: 200,
  form_date: 100,
  doc_begin: 4,
  doc_end: 4,
  storage_capacity: 200,
  creators_name: 200,
  admin_biog_history: 5000,
  arch_history: 2000,
  donation_source: 1000,
  mat_organ_system: 1000,
  archivist_rmk: 200,
  descrip_date: 200,
  published: 1,
  mustPublish: 1,
  mustUnpublish: 1,

};
// export class Fond {
//   constructor(
//     public id?: number,
//     public fond?: number,
//     public description?: string
//   ) {}
// }
