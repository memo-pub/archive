export interface PlaceT {
  id?: number;
  place_num?: number;
  beg_dat?: number;
  end_dat?: number;
  place_code?: string;
  place_name?: string;
  place_rem?: string;
  cur_mark?: string; // избыточно?
  status?: string;
}

export const placeT_width = {
  place_code: 18,
  place_name: 30,
  place_rem: 120,
  status: 3
};

export class PlaceTFilter {
  constructor(public searchStr?: string, public page?: number) {
    this.searchStr = '';
    this.page = 1;
  }
}

// export class PlaceT {
//   constructor(
//     public id?: number,
//     public place_num?: number,
//     public place_code?: string,
//     public cur_mark?: string, // избыточно?
//     public beg_dat?: number,
//     public end_dat?: number,
//     public place_name?: string,
//     public place_rem?: string,
//     public status?: string
//   ) {}
// }
