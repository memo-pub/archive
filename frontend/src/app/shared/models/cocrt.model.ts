export class CocrT {
  constructor(
    public id?: number,
    public crt_id?: number,
    public human_code?: number,
    public role?: string,
    public role_rmk?: string,
    // public sign_mark?: string
  ) {}
}

export const cocrT_width = {
  role: 30,
  role_rmk: 240
  // sign_mark: 255
};
// export class CocrT {
//   constructor(
//     public id?: number,
//     public crt_id?: number,
//     public human_code?: number,
//     public role?: string,
//     public role_rmk?: string,
//     public sign_mark?: string
//   ) {}
// }
