import { NgModule } from '@angular/core';
import {
  Routes,
  RouterModule,
  PreloadAllModules,
  NoPreloading
} from '@angular/router';

const routes: Routes = [];

@NgModule({
  // imports: [
  //   RouterModule.forRoot(routes, {
  //     preloadingStrategy: PreloadAllModules
  //   })
  // ],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
