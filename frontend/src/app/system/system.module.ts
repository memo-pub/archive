import { NgModule } from '@angular/core';

import { SystemRoutingModule } from './system-routing.module';
import { SystemComponent } from './system.component';
import { SharedModule } from '../shared/shared.module';
import { SidebarComponent } from './shared/components/sidebar/sidebar.component';
import { HeaderComponent } from './shared/components/header/header.component';
import { LogoComponent } from './shared/components/logo/logo.component';
import { DialogYesNoComponent } from './shared/components/dialog-yes-no/dialog-yes-no.component';
import { DialogSaveNoComponent } from './shared/components/dialog-save-no/dialog-save-no.component';
import { DialogSetstringComponent } from './shared/components/dialog-setstring/dialog-setstring.component';
import { DialogPersonalEditComponent } from './personal/personal-edit/dialog-personal-edit/dialog-personal-edit.component';
import { DialogMarchiveEditComponent } from './personal/personal-edit/dialog-marchive-edit/dialog-marchive-edit.component';
import { DialogTwoSelectComponent } from './shared/components/dialog-two-select/dialog-two-select.component';
import { DialogSelectFilesComponent } from './shared/components/dialog-select-files/dialog-select-files.component';
import { DialogNameslistComponent } from './shared/components/dialog-nameslist/dialog-nameslist.component';
import { DialogSaveToSelectionitemComponent } from './personal/search-page/dialog-save-to-selectionitem/dialog-save-to-selectionitem.component';
import { DialogJoinSourtComponent } from './shared/components/dialog-join-sourt/dialog-join-sourt.component';
import { DialogJoinPersonalComponent } from './shared/components/dialog-join-personal/dialog-join-personal.component';
import { DialogSetpositionComponent } from './shared/components/dialog-setposition/dialog-setposition.component';
import { DialogFileListComponent } from './shared/components/dialog-file-list/dialog-file-list.component';
import { DialogOkComponent } from './shared/components/dialog-ok/dialog-ok.component';
import { FilterFilePipe } from './shared/components/filter-file.pipe';
import { DialogPdfViewerComponent } from './shared/components/dialog-pdf-viewer/dialog-pdf-viewer.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { DialogSetDocumentParentComponent } from './shared/components/dialog-set-document-parent/dialog-set-document-parent.component';
import { DialogFileLoaderComponent } from './shared/components/dialog-file-loader/dialog-file-loader.component';

@NgModule({
  imports: [SystemRoutingModule, SharedModule, PdfViewerModule],
  entryComponents: [
    DialogYesNoComponent,
    DialogSaveNoComponent,
    DialogSetstringComponent,
    DialogPersonalEditComponent,
    DialogMarchiveEditComponent,
    DialogTwoSelectComponent,
    DialogSelectFilesComponent,
    DialogNameslistComponent,
    DialogSaveToSelectionitemComponent,
    DialogJoinSourtComponent,
    DialogJoinPersonalComponent,
    DialogSetpositionComponent,
    DialogFileListComponent,
    DialogOkComponent,
    DialogPdfViewerComponent,
    DialogSetDocumentParentComponent,
    DialogFileLoaderComponent,
  ],
  declarations: [
    SystemComponent,
    SidebarComponent,
    HeaderComponent,
    LogoComponent,
    DialogYesNoComponent,
    DialogSaveNoComponent,
    DialogSetstringComponent,
    DialogPersonalEditComponent,
    DialogMarchiveEditComponent,
    DialogTwoSelectComponent,
    DialogSelectFilesComponent,
    DialogNameslistComponent,
    DialogSaveToSelectionitemComponent,
    DialogJoinSourtComponent,
    DialogJoinPersonalComponent,
    DialogSetpositionComponent,
    DialogFileListComponent,
    DialogOkComponent,
    FilterFilePipe,
    DialogPdfViewerComponent,
    DialogSetDocumentParentComponent,
    DialogFileLoaderComponent,
  ],
})
export class SystemModule {}
