import { Directive, ElementRef, Renderer2, OnInit, HostListener, ViewContainerRef } from '@angular/core';


@Directive({
    selector: '[appTrimtext]'
})
export class TrimtextDirective implements OnInit {

    content: string;

    @HostListener('window:resize', ['$event.target'])
    onResize() {
        this.trim();
    }
    constructor(public element: ViewContainerRef, private renderer: Renderer2) {
    }

    ngOnInit() {
        this.content = this.element.element.nativeElement.innerHTML;
        // console.log(this.content);
        this.trim();
    }

    trim() {
        // this.element.insert()
        // let end: number;
        // let str: string;
        // if (document.documentElement.offsetWidth > 650) {
        // 	end = Math.floor((document.documentElement.offsetWidth - 300) / 50);
        // } else {
        // 	end = Math.floor((document.documentElement.offsetWidth) / 50);
        // }
        // if (this.content.length > end) {
        // 	str = this.content.substring(0, end);
        // 	str +=  '<span matTooltip="543">...</span>';
        // 	// console.log(str);
        // } else {
        // 	str = this.content;
        // }
        // this.renderer.setProperty(this.element.nativeElement, 'innerHTML', str);
    }

}
