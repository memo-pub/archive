import { Directive, Input } from '@angular/core';
import { NgControl } from '@angular/forms';

@Directive({
  selector: '[appDisablecontrol]'
})
export class DisablecontrolDirective {

  @Input() set disableControl(condition: boolean) {
    let action = condition ? 'disable' : 'enable';
    this.ngControl.control[action]();
    // action = condition ? 'disabled' : 'enabled';
    // this.ngControl.control[action]();
  }

  constructor(private ngControl: NgControl) {
  }

}
