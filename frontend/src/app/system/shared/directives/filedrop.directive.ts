import {
  Directive,
  EventEmitter,
  HostListener,
  Input,
  Output,
  HostBinding
} from '@angular/core';

@Directive({
  selector: '[appFiledrop]'
})
export class FiledropDirective {
  @Input() private allowed_extensions: Array<string> = [];
  @Output() private filesChangeEmiter: EventEmitter<
    File[]
  > = new EventEmitter();
  @Output() private filesInvalidEmiter: EventEmitter<
    File[]
  > = new EventEmitter();
  @HostBinding('style.background') private background = '#fff';
  @HostBinding('style.border-color') private borderColor = '#d2d2d2';
  @HostBinding('style.color') private color = '#d2d2d2';

  constructor() {}

  @HostListener('mouseover') public onMouseOver() {
    this.borderColor = '#3a4651';
    this.color = '#777';
  }

  @HostListener('mouseout') public onMouseOut() {
    this.borderColor = '#d2d2d2';
    this.color = '#d2d2d2';
  }

  @HostListener('dragover', ['$event']) public onDragOver(evt) {
    evt.preventDefault();
    evt.stopPropagation();
    this.background = '#fff';
    this.borderColor = '#3a4651';
    this.color = '#777';
  }

  @HostListener('dragleave', ['$event']) public onDragLeave(evt) {
    evt.preventDefault();
    evt.stopPropagation();
    this.background = '#fff';
    this.borderColor = '#d2d2d2';
    this.color = '#d2d2d2';
  }

  @HostListener('drop', ['$event']) public onDrop(evt) {
    evt.preventDefault();
    evt.stopPropagation();
    this.background = '#fff';
    let files = evt.dataTransfer.files;
    let valid_files: Array<File> = [];
    let invalid_files: Array<File> = [];
    if (files.length > 0) {
      for (let i = 0; i < files.length; i++) {
        let ext = files[i].name.split('.')[files[i].name.split('.').length - 1];
        if (this.allowed_extensions.lastIndexOf(ext.toLowerCase()) !== -1) {
          valid_files.push(files[i]);
        } else {
          invalid_files.push(files[i]);
        }
      }
      this.filesChangeEmiter.emit(valid_files);
      this.filesInvalidEmiter.emit(invalid_files);
    }
  }
}
