import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { BaseService } from './base.service';
import { AuthService } from '../../../shared/services/auth.service';
import { ConfigService } from 'src/app/shared/services/config.service';

@Injectable({
  providedIn: 'root'
})
export class OpisService extends BaseService {
  constructor(
    public http: HttpClient,
    public configService: ConfigService,
    public authService: AuthService
  ) {
    super(http, configService, authService, 'opis');
  }

  getByFond(fond: number) {
    return this.get(
      `${this.path}/?fond=${fond}&showfinished=true`,
      this.getOptions()
    ).toPromise();
  }

  getAll() {
    return this.get(
      `${this.path}/?orderby=byfondopis_name&fonddata=true&showfinished=true`,
      this.getOptions()
    ).toPromise();
  }
}
