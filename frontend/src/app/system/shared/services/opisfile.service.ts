import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { AuthService } from '../../../shared/services/auth.service';
import { BasefileService } from './base-file.service';
import { isNullOrUndefined } from 'util';
import { ConfigService } from 'src/app/shared/services/config.service';

@Injectable({
  providedIn: 'root'
})
export class OpisfileService extends BasefileService {
  constructor(
    public http: HttpClient,
    public configService: ConfigService,
    public authService: AuthService
  ) {
    super(http, configService, authService, 'opisfile', 'opisfile', 'opis_id');
  }

  putFilename(id: number, filename: string) {
    return this.put(
      `opisfile/${id}/updatefilename`,
      { opisfile: filename },
      this.getOptions()
    ).toPromise();
  }
}
