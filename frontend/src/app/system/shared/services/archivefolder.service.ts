import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../../../shared/services/auth.service';
import { isNullOrUndefined } from 'util';
import { ConfigService } from 'src/app/shared/services/config.service';

@Injectable({
  providedIn: 'root',
})
export class ArchivefolderService extends BaseService {
  constructor(
    public http: HttpClient,
    public configService: ConfigService,
    public authService: AuthService
  ) {
    super(http, configService, authService, 'archivefolder');
  }

  postDescription(personal_code, delo, opis_name, fond, delo_extra) {
    return this.post(
      `generatedescription`,
      { personal_code, delo, opis_name, fond, delo_extra },
      this.getOptions()
    ).toPromise();
  }

  generatedelorange(fund, list_no, file_no, file_no_ext) {
    return this.get(
      `generatedelorange?fund=${fund}&list_no=${list_no}&file_no=${file_no}&file_no_ext=${file_no_ext}`,
      this.getOptions()
    ).toPromise();
  }

  async getAll() {
    return this.get(
      `${this.path}/?scanuser=true`,
      this.getOptions()
    ).toPromise();
  }

  async getAll_count() {
    return this.get(
      `${this.path}/_getrowcount_`,
      this.getOptions()
    ).toPromise();
  }

  async getWithScanuserOffsetLimit(offset, limit) {
    return this.get(
      `${this.path}/?scanuser=true&order=byid&offset=${offset}&limit=${limit}`,
      this.getOptions()
    ).toPromise();
  }

  async getWithScanuserOffsetLimitFilterOrder(
    offset: number,
    limit: number,
    searchStr: string,
    opisId: number[],
    order: string,
    searchStatus,
    searchUnPhoto,
    searchUnDoc,
    searchAct,
    searchMem,
    searchArt,
    searchDig,
    searchtxt: string,
    searchflds: number[],
    descrip_type: string,
    filterExact: boolean
  ) {
    let str = `${this.path}/?scanuser=true&opis_data=true&orderby=${order}&offset=${offset}&limit=${limit}`;
     // &&filter_name_delo_exact=${encodeURI(searchStr)}`;
    if (!isNullOrUndefined(searchStr) && searchStr.length > 0) {
      // str += `&&filter_name_delo_exact=${searchStr}`;
      if (filterExact === true) {
        str += `&filter_name_delo_exact=${encodeURI(searchStr)}`;
      } else {
        str += `&filter_name_delo=${encodeURI(searchStr)}`;
      }
    }
    for (let i = 0; i < opisId.length; i++) {
      str += `&opis_id=${opisId[i]}`;
    }
    if (!isNullOrUndefined(searchStatus)) {
      str += `&status=${searchStatus}`;
    }
    if (!isNullOrUndefined(searchUnPhoto)) {
      str += `&foto_cnt_more=${searchUnPhoto}`;
    }
    if (!isNullOrUndefined(searchUnDoc)) {
      str += `&doc_cnt_more=${searchUnDoc}`;
    }
    if (!isNullOrUndefined(searchAct)) {
      str += `&have_act=${searchAct}`;
    }
    if (!isNullOrUndefined(searchMem)) {
      str += `&have_memories=${searchMem}`;
    }
    if (!isNullOrUndefined(searchArt)) {
      str += `&have_arts=${searchArt}`;
    }
    if (!isNullOrUndefined(searchDig)) {
      str += `&digital=${searchDig}`;
    }
    if (descrip_type) {
      str += `&descrip_type=${descrip_type}`;
    }
    if (
      !isNullOrUndefined(searchtxt) &&
      !isNullOrUndefined(searchflds) &&
      searchtxt !== '' &&
      searchflds.length > 0
    ) {
      str += `&searchtxt=${encodeURI(searchtxt)}`;
      for (let i = 0; i < searchflds.length; i++) {
        str += `&searchflds=${searchflds[i]}`;
      }
    }

    return this.get(str, this.getOptions()).toPromise();
  }

  async getWithFilter(limit: number, searchStr) {
    let resarr = [];
    let temparr1 = [];
    let temparr2 = [];
    let str = `${this.path}?limit=${limit}`;
    if (!isNullOrUndefined(searchStr) && searchStr.length > 0) {
      /**если поиск по номеру, то по номеру дела и вхождению номера в имя */
      if (Math.pow(searchStr, 1) === +searchStr) {
        let str1 = str + `&delo=${searchStr}`;
        let _temparr1 = this.get(str1, this.getOptions()).toPromise();
        temparr1 = isNullOrUndefined(await _temparr1) ? [] : await _temparr1;
      }
      /**если не число, то просто фильтр по имени */
      let str2 = str + `&filter_name_delo=${encodeURI(searchStr)}`;
      let _temparr2 = this.get(str2, this.getOptions()).toPromise();
      temparr2 = isNullOrUndefined(await _temparr2) ? [] : await _temparr2;
      resarr = [...temparr1, ...temparr2];
    } else {
      // при пустой строке запрос с лимитом, без фильтра
      let _resarr = this.get(str, this.getOptions()).toPromise();
      resarr = isNullOrUndefined(await _resarr) ? [] : await _resarr;
    }
    let used = {};
    let _res = resarr.filter((obj) => {
      return obj.id in used ? false : (used[obj.id] = 1);
    });
    return _res;

    // let arr = isNullOrUndefined(await this.get(str, this.options).toPromise())
    //   ? []
    //   : await this.get(str, this.options).toPromise();

    // let str = `${this.path}?name=${searchStr}&limit=${limit}`;
    // let arr = isNullOrUndefined(await this.get(str, this.options).toPromise())
    //   ? []
    //   : await this.get(str, this.options).toPromise();
    // let arr1 = [];
    // if (Math.pow(searchStr, 1) === +searchStr) {
    //   let str1 = `${this.path}?delo=${searchStr}&limit=${limit}`;
    //   arr1 = isNullOrUndefined(await this.get(str1, this.options).toPromise())
    //     ? []
    //     : await this.get(str1, this.options).toPromise();
    // }
    // let arr2 = arr.concat(arr1);

    // let str2 = `${this.path}/?orderby=byid&limit=${limit}`;
    // if (!isNullOrUndefined(searchStr) && searchStr.length > 0) {
    //   str += `&filter_name_delo_exact=${encodeURI(searchStr)}`;
    // }
    // let arr3 = isNullOrUndefined(await this.get(str2, this.options).toPromise())
    //   ? []
    //   : await this.get(str2, this.options).toPromise();
    // let res = arr2.concat(arr3);
    // let used = {};
    // let _res = res.filter(obj => {
    //   return obj.id in used ? 0 : (used[obj.id] = 1);
    // });
    // return _res;
  }

  async getFilter_count(
    searchStr: string,
    opisId: number[],
    searchStatus,
    searchUnPhoto,
    searchUnDoc,
    searchAct,
    searchMem,
    searchArt,
    searchDig,
    searchtxt: string,
    searchflds: number[],
    descrip_type: string,
    filterExact: boolean
  ) {
    let str = `${this.path}/_getrowcount_?scanuser=true&order=byid`; // &&filter_name_delo_exact=${encodeURI(searchStr)}`;
    if (!isNullOrUndefined(searchStr) && searchStr.length > 0) {
      if (filterExact === true) {
        str += `&filter_name_delo_exact=${encodeURI(searchStr)}`;
      } else {
        str += `&filter_name_delo=${encodeURI(searchStr)}`;
      }
    }
    for (let i = 0; i < opisId.length; i++) {
      str += `&opis_id=${opisId[i]}`;
    }
    if (!isNullOrUndefined(searchStatus)) {
      str += `&status=${searchStatus}`;
    }
    if (!isNullOrUndefined(searchUnPhoto)) {
      str += `&foto_cnt_more=${searchUnPhoto}`;
    }
    if (descrip_type) {
      str += `&descrip_type=${descrip_type}`;
    }
    if (!isNullOrUndefined(searchUnDoc)) {
      str += `&doc_cnt_more=${searchUnDoc}`;
    }
    if (!isNullOrUndefined(searchAct)) {
      str += `&have_act=${searchAct}`;
    }
    if (!isNullOrUndefined(searchMem)) {
      str += `&have_memories=${searchMem}`;
    }
    if (!isNullOrUndefined(searchArt)) {
      str += `&have_arts=${searchArt}`;
    }
    if (!isNullOrUndefined(searchDig)) {
      str += `&digital=${searchDig}`;
    }
    if (
      !isNullOrUndefined(searchtxt) &&
      !isNullOrUndefined(searchflds) &&
      searchtxt !== '' &&
      searchflds.length > 0
    ) {
      str += `&searchtxt=${encodeURI(searchtxt)}`;
      for (let i = 0; i < searchflds.length; i++) {
        str += `&searchflds=${searchflds[i]}`;
      }
    }
    return this.get(str, this.getOptions()).toPromise();
  }

  async getnextkey(
    id: number,
    searchStr: string,
    opisId: number[],
    order: string,
    searchStatus,
    searchUnPhoto,
    searchUnDoc,
    searchAct,
    searchMem,
    searchArt,
    searchDig,
    searchtxt: string,
    searchflds: number[],
    descrip_type: string,
    filterExact: boolean
  ) {
    let str = `${this.path}/${id}/_getnextkey_?scanuser=true&opis_data=true&orderby=${order}`; // &&filter_name_delo_exact=${encodeURI(searchStr)}`;
    if (!isNullOrUndefined(searchStr) && searchStr.length > 0) {
      if (filterExact === true) {
        str += `&filter_name_delo_exact=${encodeURI(searchStr)}`;
      } else {
        str += `&filter_name_delo=${encodeURI(searchStr)}`;
      }
    }
    for (let i = 0; i < opisId.length; i++) {
      str += `&opis_id=${opisId[i]}`;
    }
    if (!isNullOrUndefined(searchStatus)) {
      str += `&status=${searchStatus}`;
    }
    if (!isNullOrUndefined(searchUnPhoto)) {
      str += `&foto_cnt_more=${searchUnPhoto}`;
    }
    if (descrip_type) {
      str += `&descrip_type=${descrip_type}`;
    }
    if (!isNullOrUndefined(searchUnDoc)) {
      str += `&doc_cnt_more=${searchUnDoc}`;
    }
    if (!isNullOrUndefined(searchAct)) {
      str += `&have_act=${searchAct}`;
    }
    if (!isNullOrUndefined(searchMem)) {
      str += `&have_memories=${searchMem}`;
    }
    if (!isNullOrUndefined(searchArt)) {
      str += `&have_arts=${searchArt}`;
    }
    if (!isNullOrUndefined(searchDig)) {
      str += `&digital=${searchDig}`;
    }
    if (
      !isNullOrUndefined(searchtxt) &&
      !isNullOrUndefined(searchflds) &&
      searchtxt !== '' &&
      searchflds.length > 0
    ) {
      str += `&searchtxt=${encodeURI(searchtxt)}`;
      for (let i = 0; i < searchflds.length; i++) {
        str += `&searchflds=${searchflds[i]}`;
      }
    }
    return this.get(str, this.getOptions()).toPromise();
  }

  async getpreviouskey(
    id: number,
    searchStr: string,
    opisId: number[],
    order: string,
    searchStatus,
    searchUnPhoto,
    searchUnDoc,
    searchAct,
    searchMem,
    searchArt,
    searchDig,
    searchtxt: string,
    searchflds: number[],
    descrip_type: string,
    filterExact: boolean
  ) {
    let str = `${this.path}/${id}/_getpreviouskey_?scanuser=true&opis_data=true&orderby=${order}`;
    if (!isNullOrUndefined(searchStr) && searchStr.length > 0) {
      if (filterExact === true) {
        str += `&filter_name_delo_exact=${encodeURI(searchStr)}`;
      } else {
        str += `&filter_name_delo=${encodeURI(searchStr)}`;
      }
    }
    for (let i = 0; i < opisId.length; i++) {
      str += `&opis_id=${opisId[i]}`;
    }
    if (!isNullOrUndefined(searchStatus)) {
      str += `&status=${searchStatus}`;
    }
    if (!isNullOrUndefined(searchUnPhoto)) {
      str += `&foto_cnt_more=${searchUnPhoto}`;
    }
    if (descrip_type) {
      str += `&descrip_type=${descrip_type}`;
    }
    if (!isNullOrUndefined(searchUnDoc)) {
      str += `&doc_cnt_more=${searchUnDoc}`;
    }
    if (!isNullOrUndefined(searchAct)) {
      str += `&have_act=${searchAct}`;
    }
    if (!isNullOrUndefined(searchMem)) {
      str += `&have_memories=${searchMem}`;
    }
    if (!isNullOrUndefined(searchArt)) {
      str += `&have_arts=${searchArt}`;
    }
    if (!isNullOrUndefined(searchDig)) {
      str += `&digital=${searchDig}`;
    }
    if (
      !isNullOrUndefined(searchtxt) &&
      !isNullOrUndefined(searchflds) &&
      searchtxt !== '' &&
      searchflds.length > 0
    ) {
      str += `&searchtxt=${encodeURI(searchtxt)}`;
      for (let i = 0; i < searchflds.length; i++) {
        str += `&searchflds=${searchflds[i]}`;
      }
    }
    return this.get(str, this.getOptions()).toPromise();
  }

  async getWithOpis_data(delo, fond, opis, delo_extra?: string) {
    let str = `${this.path}/?opis_data=true&delo=${delo}&opis_fond=${fond}&opis_opis_name=${opis}`;
    if (delo_extra && delo_extra.length > 0) {
      str += `&delo_extra=${encodeURI(delo_extra)}`;
    }
    return this.get(str, this.getOptions()).toPromise();
  }

  // async getAllWithCreateduser() {
  // 	return this.get(`${this.path}/?createduser=true`, this.options).toPromise();
  // }

  // async getAllWithUsers() {
  // 	return this.get(`${this.path}/?createduser=true&scanuser=true`, this.options).toPromise();
  // }

  // async getAllWithCountfiles() {
  // 	return this.get(`${this.path}/?countfiles=true`, this.options).toPromise();
  // }
}
