import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { AuthService } from 'src/app/shared/services/auth.service';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from 'src/app/shared/services/config.service';

@Injectable({
  providedIn: 'root'
})
export class SelectionService extends BaseService {
  constructor(
    public http: HttpClient,
    public configService: ConfigService,
    public authService: AuthService
  ) {
    super(http, configService, authService, 'selection');
  }

  async getAll_count() {
    return this.get(`${this.path}/_getrowcount_`, this.getOptions()).toPromise();
  }

  async getWithOffsetLimit(offset, limit) {
    return this.get(
      `${this.path}/?orderby=byid&offset=${offset}&limit=${limit}`,
      this.getOptions()
    ).toPromise();
  }

  async getWithOffsetLimitOrder(offset: number, limit: number, order: string) {
    return this.get(
      `${this.path}/?orderby=${order}&offset=${offset}&limit=${limit}`,
      this.getOptions()
    ).toPromise();
  }

  async getFilter_count(searchStr: string) {
    let str = `${this.path}/_getrowcount_?filter=${encodeURI(searchStr)}`;
    return this.get(str, this.getOptions()).toPromise();
  }

  async getWithFilterOffsetLimitOrder(
    searchStr: string,
    offset: number,
    limit: number,
    order: string
  ) {
    return this.get(
      `${this.path}/?filter=${encodeURI(
        searchStr
      )}&orderby=${order}&offset=${offset}&limit=${limit}`,
      this.getOptions()
    ).toPromise();
  }
}
