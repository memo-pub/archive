import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { BaseService } from './base.service';
import { AuthService } from '../../../shared/services/auth.service';
import { ConfigService } from 'src/app/shared/services/config.service';

@Injectable({
  providedIn: 'root',
})
export class SourtypeService extends BaseService {
  constructor(
    public http: HttpClient,
    public configService: ConfigService,
    public authService: AuthService
  ) {
    super(http, configService, authService, 'sour_type_t');
  }

  getBySourid(sour_id: number) {
    return this.get(
      `${this.path}/?sour_id=${sour_id}`,
      this.getOptions()
    ).toPromise();
  }
}
