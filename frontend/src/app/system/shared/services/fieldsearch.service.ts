import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../../../shared/services/auth.service';
import { ConfigService } from 'src/app/shared/services/config.service';

@Injectable({
  providedIn: 'root'
})
export class FieldsearchService extends BaseService {
  constructor(
    public http: HttpClient,
    public configService: ConfigService,
    public authService: AuthService
  ) {
    super(http, configService, authService, 'fieldsearch');
  }

  getByStr(searchStr) {
    return this.get(
      `${this.path}/?searchtxt=${encodeURI(searchStr)}`,
      this.getOptions()
    ).toPromise();
  }
}
