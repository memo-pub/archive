import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { BaseService } from './base.service';
import { AuthService } from '../../../shared/services/auth.service';
import { isNullOrUndefined } from 'util';
import { ConfigService } from 'src/app/shared/services/config.service';

@Injectable({
  providedIn: 'root',
})
export class PersonaltService extends BaseService {
  constructor(
    public http: HttpClient,
    public configService: ConfigService,
    public authService: AuthService
  ) {
    super(http, configService, authService, 'personal_t');
  }

  postNumbering(fond, opis) {
    return this.post(
      `opisnumbering`,
      { opis, fond },
      this.getOptions()
    ).toPromise();
  }

  async getWithShortfilterLimit(limit: number, searchStr) {
    let str = `${
      this.path
    }/?limit=${limit}&orderby=bysurname&shortfilter_exact=${encodeURI(
      searchStr
    )}`;
    return this.get(str, this.getOptions()).toPromise();
  }

  async getBycode(code) {
    return this.get(
      `${this.path}/?code=${code}`,
      this.getOptions()
    ).toPromise();
  }

  // async getAll() {
  // 	return this.get(`${this.path}/?scanuser=true`, this.getOptions()).toPromise();
  // }

  async getAll_count() {
    return this.get(
      `${this.path}/_getrowcount_`,
      this.getOptions()
    ).toPromise();
  }

  async getWithOffsetLimit(offset, limit) {
    return this.get(
      `${this.path}/?orderby=byid&offset=${offset}&limit=${limit}`,
      this.getOptions()
    ).toPromise();
  }

  async getWithOffsetLimitOrder(offset: number, limit: number, order: string) {
    return this.get(
      `${this.path}/?orderby=${order}&offset=${offset}&limit=${limit}`,
      this.getOptions()
    ).toPromise();
  }

  async getShortfilter_count(searchStr: string) {
    let str = `${this.path}/_getrowcount_?shortfilter=${encodeURI(searchStr)}`;
    return this.get(str, this.getOptions()).toPromise();
  }

  async getShortfilterOpisFondDelo_count(
    searchStr: string,
    fund: string,
    list_no: string,
    file_no: string,
    file_no_ext: string,
    exact: boolean,
    exactExt: boolean
  ) {
    let str = `${this.path}/_getrowcount_`;
    let prefix = '?';
    let filterstr = exact ? 'shortfilter_exact' : 'shortfilter';
    if (!isNullOrUndefined(searchStr) && searchStr.length > 0) {
      str += `${prefix}${filterstr}=${encodeURI(searchStr)}`;
      prefix = '&';
    }
    if (!isNullOrUndefined(fund) && fund.length > 0) {
      str += `${prefix}fund=${fund}`;
      prefix = '&';
    }
    if (!isNullOrUndefined(list_no) && list_no.length > 0) {
      str += `${prefix}list_no=${list_no}`;
      prefix = '&';
    }
    if (!isNullOrUndefined(file_no) && file_no.length > 0) {
      str += `${prefix}file_no=${file_no}`;
      prefix = '&';
    }
    if (!isNullOrUndefined(file_no_ext) && file_no_ext.length > 0) {
      str += `${prefix}file_no_ext=${file_no_ext}`;
      prefix = '&';
    }
    return this.get(str, this.getOptions()).toPromise();
  }

  async getWithShortfilterOffsetLimitOrder(
    searchStr: string,
    offset: number,
    limit: number,
    order: string
  ) {
    return this.get(
      `${this.path}/?shortfilter=${encodeURI(
        searchStr
      )}&orderby=${order}&offset=${offset}&limit=${limit}`,
      this.getOptions()
    ).toPromise();
  }

  async getWithShortfilterOpisFondDeloOffsetLimitOrder(
    searchStr: string,
    offset: number,
    limit: number,
    order: string,
    fund: string,
    list_no: string,
    file_no: string,
    file_no_ext: string,
    exact: boolean,
    exactExt: boolean
  ) {
    // let filterstr = exact ? 'shortfilter_exact' : 'shortfilter';
    let filterstr = '';
    if (exactExt) {
      filterstr = exact ? 'shortfilter_exact_withrmk' : 'shortfilter_withrmk';
    } else {
      filterstr = exact ? 'shortfilter_exact' : 'shortfilter';
    }
    let str = `${this.path}/?orderby=${order}&offset=${offset}&limit=${limit}`;
    if (!isNullOrUndefined(searchStr) && searchStr.length > 0) {
      str += `&${filterstr}=${encodeURI(searchStr)}`;
    }
    if (!isNullOrUndefined(fund) && fund.length > 0) {
      str += `&fund=${fund}`;
    }
    if (!isNullOrUndefined(list_no) && list_no.length > 0) {
      str += `&list_no=${list_no}`;
    }
    if (!isNullOrUndefined(file_no) && file_no.length > 0) {
      str += `&file_no=${file_no}`;
    }
    if (!isNullOrUndefined(file_no_ext) && file_no_ext.length > 0) {
      str += `&file_no_ext=${file_no_ext}`;
    }
    return this.get(str, this.getOptions()).toPromise();
    // return this.get(
    //   `${this.path}/?shortfilter=${encodeURI(searchStr)}&orderby=${order}&offset=${offset}&limit=${limit}`, this.getOptions()).toPromise();
  }

  getExcelkart(personal_code) {
    return this.get(
      `downloadexcelkart?personal_code=${personal_code}`,
      this.getOptions()
    ).toPromise();
  }
}
