import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { AuthService } from '../../../shared/services/auth.service';
import { BasefileService } from './base-file.service';
import { isNullOrUndefined } from 'util';
import { ConfigService } from 'src/app/shared/services/config.service';

@Injectable({
  providedIn: 'root',
})
export class FondfileService extends BasefileService {
  constructor(
    public http: HttpClient,
    public configService: ConfigService,
    public authService: AuthService
  ) {
    super(http, configService, authService, 'fondfile', 'fondfile', 'fond_id');
  }

  putFilename(id: number, filename: string) {
    return this.put(
      `fondfile/${id}/updatefilename`,
      { fondfile: filename },
      this.getOptions()
    ).toPromise();
  }
}
