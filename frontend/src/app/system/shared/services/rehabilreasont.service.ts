import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { BaseService } from './base.service';
import { AuthService } from '../../../shared/services/auth.service';
import { ConfigService } from 'src/app/shared/services/config.service';

@Injectable({
  providedIn: 'root'
})
export class RehabilreasontService extends BaseService {
  constructor(
    public http: HttpClient,
    public configService: ConfigService,
    public authService: AuthService
  ) {
    super(http, configService, authService, 'rehabil_reason_t');
  }

  getByRepid(rep_id) {
    return this.get(
      `${this.path}/?rep_id=${rep_id}`,
      this.getOptions()
    ).toPromise();
  }
}
