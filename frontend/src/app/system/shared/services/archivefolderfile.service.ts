import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { AuthService } from '../../../shared/services/auth.service';
import { BasefileService } from './base-file.service';
import { isNullOrUndefined } from 'util';
import { ConfigService } from 'src/app/shared/services/config.service';

@Injectable({
  providedIn: 'root',
})
export class ArchivefolderfileService extends BasefileService {
  constructor(
    public http: HttpClient,
    public configService: ConfigService,
    public authService: AuthService
  ) {
    super(
      http,
      configService,
      authService,
      'archivefolderfile',
      'archivefile',
      'archivefolder_id'
    );
  }

  getZip(id) {
    return this.getFile(
      `downloadfolderfiles?archivefolder_id=${id}`,
      this.getOptions()
    );
  }
  getPreviewZip(id, archiveNumber) {
    return this.getFile(
      `downloadpreview?archivefolder_id=${id}&archiveNumber=${archiveNumber}`,
      this.getOptions()
    );
  }

  getZipBrowser(id) {
    return this.get(
      `downloadfolderfiles?archivefolder_id=${id}`,
      this.getOptions()
    );
  }

  async getAll_count() {
    return this.get(
      `${this.path}/_getrowcount_`,
      this.getOptions()
    ).toPromise();
  }

  async getWithOffsetLimitOrder(offset, limit, order) {
    return this.get(
      `${this.path}/?order=${order}&offset=${offset}&limit=${limit}`,
      this.getOptions()
    ).toPromise();
  }

  async getWithFiltersOffsetLimitOrder(
    filter_fond: string,
    filter_opis_name: string,
    filter_delo: string,
    filter_filename: string,
    offset: number,
    limit: number,
    order: string,
    imgOnly = false
  ) {
    let str = `${this.path}/?orderby=${order}&offset=${offset}&limit=${limit}&folder=true`;
    if (!isNullOrUndefined(filter_fond) && filter_fond !== '') {
      str += `&filter_fond=${encodeURI(filter_fond)}`;
    }
    if (!isNullOrUndefined(filter_opis_name) && filter_opis_name !== '') {
      str += `&filter_opis_name=${encodeURI(filter_opis_name)}`;
    }
    if (!isNullOrUndefined(filter_delo) && filter_delo !== '') {
      str += `&filter_delo=${encodeURI(filter_delo)}`;
    }
    if (!isNullOrUndefined(filter_filename) && filter_filename !== '') {
      str += `&filter_filename=${encodeURI(filter_filename)}`;
    }
    if (imgOnly === true) {
      str += '&filter_pictures=true';
    }
    return this.get(str, this.getOptions()).toPromise();
  }

  async getWithFilters_count(
    filter_fond: string,
    filter_opis_name: string,
    filter_delo: string,
    filter_filename: string
  ) {
    let str = `${this.path}/_getrowcount_?orderby=byid&folder=true`;
    if (!isNullOrUndefined(filter_fond) && filter_fond !== '') {
      str += `&filter_fond=${encodeURI(filter_fond)}`;
    }
    if (!isNullOrUndefined(filter_opis_name) && filter_opis_name !== '') {
      str += `&filter_opis_name=${encodeURI(filter_opis_name)}`;
    }
    if (!isNullOrUndefined(filter_delo) && filter_delo !== '') {
      str += `&filter_delo=${encodeURI(filter_delo)}`;
    }
    if (!isNullOrUndefined(filter_filename) && filter_filename !== '') {
      str += `&filter_filename=${encodeURI(filter_filename)}`;
    }
    return this.get(str, this.getOptions()).toPromise();
  }

  putFilename(id: number, filename: string) {
    return this.put(
      `archivefolderfile/${id}/updatefilename`,
      { archivefile: filename },
      this.getOptions()
    ).toPromise();
  }

  async getAllByParentId(id, getjwt = false) {
    let str = `${this.path}/?${this.parent}=${id}`;
    if (getjwt === true) {
      str += '&getjwt=true';
    }
    return this.get(str, this.getOptions()).toPromise();
  }

  async getByFilter(
    fund,
    list_no,
    file_no,
    file_no_ext,
    file_no_ext_noneed,
    sour_samedelo?: number,
    filter_filename?: string
  ) {
    let str = `${this.path}/?folder=true&orderby=byfilename`;
    if (fund) {
      str += `&filter_fond=${fund}`;
    }
    if (sour_samedelo) {
      str += `&sour_samedelo=${sour_samedelo}`;
    }
    if (filter_filename && filter_filename !== '') {
      str += `&filter_filename=${filter_filename}`;
    }
    if (list_no) {
      str += `&filter_opis_name=${list_no}`;
    }
    if (file_no) {
      str += `&filter_delo_exact=${file_no}`;
    }
    if (file_no_ext_noneed) {
      str += `&filter_delo_extra_empty=${true}`;
    } else {
      if (file_no_ext && file_no_ext !== '') {
        str += `&filter_delo_extra_exact=${file_no_ext}`;
      }
    }
    str += '&getjwt=true';
    return this.get(str, this.getOptions()).toPromise();
  }
  async getArchiveFolderId(fondName, opisName, fileName) {
    return this.get(
      `getarchivefolderid?fondName=${fondName}&opisName=${opisName}&fileName=${fileName}`,
      this.getOptions()
    ).toPromise();
  }
}
