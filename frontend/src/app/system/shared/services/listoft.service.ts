import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { BaseService } from './base.service';
import { AuthService } from '../../../shared/services/auth.service';
import { ConfigService } from 'src/app/shared/services/config.service';

@Injectable({
  providedIn: 'root'
})
export class ListoftService extends BaseService {

  constructor(
    public http: HttpClient,
    public configService: ConfigService,
    public authService: AuthService,
  ) {
    super(http, configService, authService, 'list_of_t');
  }

  async getWithFilter(searchStr) {
    let str = `${this.path}/?name_f=${encodeURI(searchStr)}`;
    return this.get(str, this.getOptions()).toPromise();
  }

  async getWithFilter_count(searchStr) {
    let str = `${this.path}/_getrowcount_?name_f=${encodeURI(searchStr)}`;
    return this.get(str, this.getOptions()).toPromise();
  }
}
