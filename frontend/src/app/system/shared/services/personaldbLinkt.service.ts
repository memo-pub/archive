import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { HttpClient } from '@angular/common/http';
import { AuthService } from 'src/app/shared/services/auth.service';
import { ConfigService } from 'src/app/shared/services/config.service';

@Injectable({
  providedIn: 'root',
})
export class PersonalDbLinkService extends BaseService {
  constructor(
    public http: HttpClient,
    public configService: ConfigService,
    public authService: AuthService
  ) {
    super(http, configService, authService, 'personal_db_link_t');
  }

  getByPersonalcode(personal_code: number) {
    let str = `${this.path}/?personal_code=${personal_code}`;
    return this.get(str, this.getOptions()).toPromise();
  }
}
