import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { HttpClient } from '@angular/common/http';
import { AuthService } from 'src/app/shared/services/auth.service';
import { ConfigService } from 'src/app/shared/services/config.service';

@Injectable({
  providedIn: 'root'
})
export class HelptService extends BaseService {

  constructor(
    public http: HttpClient,
    public configService: ConfigService,
    public authService: AuthService,
  ) {
    super(http, configService, authService, 'help_t');
  }

  getByPersonalcode(personal_code) {
    return this.get(`${this.path}/?personal_code=${personal_code}`, this.getOptions()).toPromise();
  }
}
