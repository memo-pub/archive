import { HttpClient, HttpHeaders } from '@angular/common/http';

import { BaseApi } from '../../../shared/core/base-api';
import { AuthService } from '../../../shared/services/auth.service';
import { Inject, Optional } from '@angular/core';
import { Muser } from '../../../shared/models/muser.model';
import { ConfigService } from 'src/app/shared/services/config.service';

export class BasefileService extends BaseApi {
  // loggedInUser: Muser;
  // options: HttpHeaders;

  constructor(
    public http: HttpClient,
    public configService: ConfigService,
    public authService: AuthService,
    @Inject('pathStr') @Optional() public path: string,
    @Inject('fileStr') @Optional() public file: string,
    @Inject('parentStr') @Optional() public parent: string
  ) {
    super(http, configService);
    this.path = path;
    // this.loggedInUser = this.authService.getCurUser();
    // this.options = new HttpHeaders();
    // this.options = this.options.set('Authorization', 'JWT ' + this.loggedInUser.token);
  }

  getOptions(): HttpHeaders {
    let options = new HttpHeaders();
    if (this.authService.isLoggedIn()) {
      options = options.set(
        'Authorization',
        'JWT ' + this.authService.getCurUser().token
      );
    }
    return options;
  }

  async getAll() {
    return this.get(`${this.path}`, this.getOptions()).toPromise();
  }

  async getOneById(id) {
    let str = `${this.path}/${id}`;
    return this.get(str, this.getOptions()).toPromise();
  }

  async getAllByParentId(id, getjwt = false) {
    let str = `${this.path}/?${this.parent}=${id}`;
    if (getjwt === true) {
      str += '&getjwt=true';
    }
    return this.get(str, this.getOptions()).toPromise();
  }

  downloadOneById(id) {
    return this.getFile(
      `${this.path}/` + id + `/${this.file}`,
      this.getOptions()
    );
    // return await this.getFile(`${this.path}/` + id + `/${this.file}`, this.options).toPromise();
  }

  async getFileById(id) {
    return await this.getFileBlob(
      `${this.path}/` + id + `/${this.file}`,
      this.getOptions()
    ).toPromise();
  }

  async postOne(data) {
    return this.post(`${this.path}`, data, this.getOptions()).toPromise();
  }

  uploadOne(data) {
    return this.postFile(`${this.path}`, data, this.getOptions());
  }

  async putOneById(id, data) {
    return this.put(`${this.path}/` + id, data, this.getOptions()).toPromise();
  }

  async deleteOneById(id) {
    return this.delete(`${this.path}/` + id, this.getOptions()).toPromise();
  }

  getFilePreviewById(id) {
    return this.getFileBlob(
      `${this.path}/` + id + `/${this.file}/preview/1000`,
      this.getOptions()
    ).toPromise();
  }
}
