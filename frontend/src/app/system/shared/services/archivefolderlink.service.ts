import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../../../shared/services/auth.service';
import { isNullOrUndefined } from 'util';
import { ConfigService } from 'src/app/shared/services/config.service';

@Injectable({
  providedIn: 'root'
})
export class ArchivefolderlinkService extends BaseService {
  constructor(
    public http: HttpClient,
    public configService: ConfigService,
    public authService: AuthService
  ) {
    super(http, configService, authService, 'archivefolderlink');
  }

  async getAllWithNamesById(id) {
    let _res1 = this.get(
      `${this.path}/?af1=true&af2=true&archivefolder_id1=${id}`,
      this.getOptions()
    ).toPromise();
    let _res2 = this.get(
      `${this.path}/?af1=true&af2=true&archivefolder_id2=${id}`,
      this.getOptions()
    ).toPromise();
    let res1 = isNullOrUndefined(await _res1) ? [] : await _res1;
    let res2 = isNullOrUndefined(await _res2) ? [] : await _res2;
    return res1.concat(res2);
  }
}
