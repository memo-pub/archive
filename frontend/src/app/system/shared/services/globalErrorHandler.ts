import { ErrorHandler, Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { GlobalService } from 'src/app/shared/services/global.service';
@Injectable()
export class GlobalErrorHandler implements ErrorHandler {
  constructor(private globalService: GlobalService) {}
  handleError(error) {
    this.globalService.hasError.next(true);
    // IMPORTANT: Rethrow the error otherwise it gets swallowed
    // throw error;
  }
}
