import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { HttpClient } from '@angular/common/http';
import { AuthService } from 'src/app/shared/services/auth.service';
import { isNullOrUndefined } from 'util';
import { ConfigService } from 'src/app/shared/services/config.service';

@Injectable({
  providedIn: 'root',
})
export class SelectionitemService extends BaseService {
  constructor(
    public http: HttpClient,
    public configService: ConfigService,
    public authService: AuthService
  ) {
    super(http, configService, authService, 'selectionitem');
  }

  async getBySelectionId(id: number) {
    return this.get(
      `${this.path}/?info=true&selection_id=${id}`,
      this.getOptions()
    ).toPromise();
  }

  async getBySelectionId_count(id: number) {
    return this.get(
      `${this.path}/_getrowcount_?selection_id=${id}`,
      this.getOptions()
    ).toPromise();
  }

  async getWithOffsetLimit(offset, limit, id: number) {
    return this.get(
      `${this.path}/?info=true&orderby=byid&offset=${offset}&limit=${limit}&selection_id=${id}`,
      this.getOptions()
    ).toPromise();
  }

  async getWithOffsetLimitOrder(
    offset: number,
    limit: number,
    order: string,
    id: number
  ) {
    return this.get(
      `${this.path}/?info=true&orderby=${order}&offset=${offset}&limit=${limit}&selection_id=${id}`,
      this.getOptions()
    ).toPromise();
  }

  async getShortfilter_count(searchStr: string, id: number) {
    let str = `${this.path}/_getrowcount_?info=true&shortfilter=${encodeURI(
      searchStr
    )}&selection_id=${id}`;
    return this.get(str, this.getOptions()).toPromise();
  }

  async getShortfilterOpisFondDelo_count(
    searchStr: string,
    fund: string,
    list_no: string,
    file_no: string,
    id: number,
    exact: boolean
  ) {
    let filterstr = exact ? 'shortfilter_exact' : 'shortfilter';
    let str = `${this.path}/_getrowcount_?info=true&selection_id=${id}`;
    let prefix = '&';
    if (!isNullOrUndefined(searchStr) && searchStr.length > 0) {
      str += `${prefix}${filterstr}=${encodeURI(searchStr)}`;
      prefix = '&';
    }
    if (!isNullOrUndefined(fund) && fund.length > 0) {
      str += `${prefix}fund=${fund}`;
      prefix = '&';
    }
    if (!isNullOrUndefined(list_no) && list_no.length > 0) {
      str += `${prefix}list_no=${list_no}`;
      prefix = '&';
    }
    if (!isNullOrUndefined(file_no) && file_no.length > 0) {
      str += `${prefix}file_no=${file_no}`;
      prefix = '&';
    }
    return this.get(str, this.getOptions()).toPromise();
  }

  async getWithShortfilterOffsetLimitOrder(
    searchStr: string,
    offset: number,
    limit: number,
    order: string,
    id: number
  ) {
    return this.get(
      `${this.path}/?info=true&shortfilter=${encodeURI(
        searchStr
      )}&orderby=${order}&offset=${offset}&limit=${limit}&selection_id=${id}`,
      this.getOptions()
    ).toPromise();
  }

  async getWithShortfilterOpisFondDeloOffsetLimitOrder(
    searchStr: string,
    offset: number,
    limit: number,
    order: string,
    fund: string,
    list_no: string,
    file_no: string,
    id: number,
    exact: boolean
  ) {
    let filterstr = exact ? 'shortfilter_exact' : 'shortfilter';
    let str = `${this.path}/?info=true&orderby=${order}&offset=${offset}&limit=${limit}&selection_id=${id}`;
    if (!isNullOrUndefined(searchStr) && searchStr.length > 0) {
      str += `&${filterstr}=${encodeURI(searchStr)}`;
    }
    if (!isNullOrUndefined(fund) && fund.length > 0) {
      str += `&fund=${fund}`;
    }
    if (!isNullOrUndefined(list_no) && list_no.length > 0) {
      str += `&list_no=${list_no}`;
    }
    if (!isNullOrUndefined(file_no) && file_no.length > 0) {
      str += `&file_no=${file_no}`;
    }
    return this.get(str, this.getOptions()).toPromise();
  }

  async deleteSelectionitem(ids) {
    return this.post(`deleteselectionitem`, ids, this.getOptions()).toPromise();
  }

  async addselectionitem(data) {
    data.pickerusage_t_us_date1 = isNullOrUndefined(data.pickerusage_t_us_date1)
      ? data.pickerusage_t_us_date1
      : new Date(data.pickerusage_t_us_date1).toDateString();
    data.pickerusage_t_us_date2 = isNullOrUndefined(data.pickerusage_t_us_date2)
      ? data.pickerusage_t_us_date2
      : new Date(data.pickerusage_t_us_date2).toDateString();
    data.birth = isNullOrUndefined(data.birth)
      ? data.birth
      : new Date(data.birth).toDateString();
    data.death = isNullOrUndefined(data.death)
      ? data.death
      : new Date(data.death).toDateString();
    data.inform_dat = isNullOrUndefined(data.inform_dat)
      ? data.inform_dat
      : new Date(data.inform_dat).toDateString();
    data.arrival_dat = isNullOrUndefined(data.arrival_dat)
      ? data.arrival_dat
      : new Date(data.arrival_dat).toDateString();
    data.depart_dat = isNullOrUndefined(data.depart_dat)
      ? data.depart_dat
      : new Date(data.depart_dat).toDateString();
    data.help_dat = isNullOrUndefined(data.help_dat)
      ? data.help_dat
      : new Date(data.help_dat).toDateString();
    data.join_dat = isNullOrUndefined(data.join_dat)
      ? data.join_dat
      : new Date(data.join_dat).toDateString();
    data.dismis_dat = isNullOrUndefined(data.dismis_dat)
      ? data.dismis_dat
      : new Date(data.dismis_dat).toDateString();
    data.reabil_dat = isNullOrUndefined(data.reabil_dat)
      ? data.reabil_dat
      : new Date(data.reabil_dat).toDateString();
    data.repress_dat = isNullOrUndefined(data.repress_dat)
      ? data.repress_dat
      : new Date(data.repress_dat).toDateString();
    data.us_date = isNullOrUndefined(data.us_date)
      ? data.us_date
      : new Date(data.us_date).toDateString();
    data.inq_date = isNullOrUndefined(data.inq_date)
      ? data.inq_date
      : new Date(data.inq_date).toDateString();
    data.sentense_date = isNullOrUndefined(data.sentense_date)
      ? data.sentense_date
      : new Date(data.sentense_date).toDateString();
    data.leave_dat = isNullOrUndefined(data.leave_dat)
      ? data.leave_dat
      : new Date(data.leave_dat).toDateString();
    data.arrival_dat = isNullOrUndefined(data.arrival_dat)
      ? data.arrival_dat
      : new Date(data.arrival_dat).toDateString();
    data.personal_t_birth_from = isNullOrUndefined(data.personal_t_birth_from)
      ? data.personal_t_birth_from
      : new Date(data.personal_t_birth_from).toDateString();
    data.personal_t_birth_to = isNullOrUndefined(data.personal_t_birth_to)
      ? data.personal_t_birth_to
      : new Date(data.personal_t_birth_to).toDateString();
    data.personal_t_death1 = isNullOrUndefined(data.personal_t_death1)
      ? data.personal_t_death1
      : new Date(data.personal_t_death1).toDateString();
    data.personal_t_death2 = isNullOrUndefined(data.personal_t_death2)
      ? data.personal_t_death2
      : new Date(data.personal_t_death2).toDateString();
    data.activity_t_arrival_dat1 = isNullOrUndefined(
      data.activity_t_arrival_dat1
    )
      ? data.activity_t_arrival_dat1
      : new Date(data.activity_t_arrival_dat1).toDateString();
    data.activity_t_arrival_dat2 = isNullOrUndefined(
      data.activity_t_arrival_dat2
    )
      ? data.activity_t_arrival_dat2
      : new Date(data.activity_t_arrival_dat2).toDateString();
    data.activity_t_depart_dat1 = isNullOrUndefined(data.activity_t_depart_dat1)
      ? data.activity_t_depart_dat1
      : new Date(data.activity_t_depart_dat1).toDateString();
    data.activity_t_depart_dat2 = isNullOrUndefined(data.activity_t_depart_dat2)
      ? data.activity_t_depart_dat2
      : new Date(data.activity_t_depart_dat2).toDateString();
    data.org_t_join_dat1 = isNullOrUndefined(data.org_t_join_dat1)
      ? data.org_t_join_dat1
      : new Date(data.org_t_join_dat1).toDateString();
    data.org_t_join_dat2 = isNullOrUndefined(data.org_t_join_dat2)
      ? data.org_t_join_dat2
      : new Date(data.org_t_join_dat2).toDateString();
    data.org_t_dismis_dat1 = isNullOrUndefined(data.org_t_dismis_dat1)
      ? data.org_t_dismis_dat1
      : new Date(data.org_t_dismis_dat1).toDateString();
    data.org_t_dismis_dat2 = isNullOrUndefined(data.org_t_dismis_dat2)
      ? data.org_t_dismis_dat2
      : new Date(data.org_t_dismis_dat2).toDateString();
    data.repress_t_repress_dat1 = isNullOrUndefined(data.repress_t_repress_dat1)
      ? data.repress_t_repress_dat1
      : new Date(data.repress_t_repress_dat1).toDateString();
    data.repress_t_repress_dat2 = isNullOrUndefined(data.repress_t_repress_dat2)
      ? data.repress_t_repress_dat2
      : new Date(data.repress_t_repress_dat2).toDateString();
    data.repress_t_reabil_dat1 = isNullOrUndefined(data.repress_t_reabil_dat1)
      ? data.repress_t_reabil_dat1
      : new Date(data.repress_t_reabil_dat1).toDateString();
    data.repress_t_reabil_dat2 = isNullOrUndefined(data.repress_t_reabil_dat2)
      ? data.repress_t_reabil_dat2
      : new Date(data.repress_t_reabil_dat2).toDateString();
    data.court_t_inq_date1 = isNullOrUndefined(data.court_t_inq_date1)
      ? data.court_t_inq_date1
      : new Date(data.court_t_inq_date1).toDateString();
    data.court_t_inq_date2 = isNullOrUndefined(data.court_t_inq_date2)
      ? data.court_t_inq_date2
      : new Date(data.court_t_inq_date2).toDateString();
    data.court_t_sentense_date1 = isNullOrUndefined(data.court_t_sentense_date1)
      ? data.court_t_sentense_date1
      : new Date(data.court_t_sentense_date1).toDateString();
    data.court_t_sentense_date2 = isNullOrUndefined(data.court_t_sentense_date2)
      ? data.court_t_sentense_date2
      : new Date(data.court_t_sentense_date2).toDateString();
    data.impris_t_arrival_dat1 = isNullOrUndefined(data.impris_t_arrival_dat1)
      ? data.impris_t_arrival_dat1
      : new Date(data.impris_t_arrival_dat1).toDateString();
    data.impris_t_arrival_dat2 = isNullOrUndefined(data.impris_t_arrival_dat2)
      ? data.impris_t_arrival_dat2
      : new Date(data.impris_t_arrival_dat2).toDateString();
    data.impris_t_leave_dat1 = isNullOrUndefined(data.impris_t_leave_dat1)
      ? data.impris_t_leave_dat1
      : new Date(data.impris_t_leave_dat1).toDateString();
    data.impris_t_leave_dat2 = isNullOrUndefined(data.impris_t_leave_dat2)
      ? data.impris_t_leave_dat2
      : new Date(data.impris_t_leave_dat2).toDateString();
    // data.sour_t_sour_date = isNullOrUndefined(data.sour_t_sour_date)
    //   ? data.sour_t_sour_date
    //   : new Date(data.sour_t_sour_date).toDateString();
    data.help_t_help_dat1 = isNullOrUndefined(data.help_t_help_dat1)
      ? data.help_t_help_dat1
      : new Date(data.help_t_help_dat1).toDateString();
    data.help_t_help_dat2 = isNullOrUndefined(data.help_t_help_dat2)
      ? data.help_t_help_dat2
      : new Date(data.help_t_help_dat2).toDateString();
    return this.post(`addselectionitem`, data, this.getOptions()).toPromise();
  }

  getSimpleList(id) {
    return this.getFile(
      `downloadexcel?selection_id=${id}&type=1`,
      this.getOptions()
    );
  }
  getOrgtList(id) {
    return this.getFile(
      `downloadexcelobd?selection_id=${id}`,
      this.getOptions()
    );
  }
  getActivitytList(id) {
    return this.getFile(
      `downloadexcelrub?selection_id=${id}`,
      this.getOptions()
    );
  }
  getRepresstList(id) {
    return this.getFile(
      `downloadexcelrepr?selection_id=${id}`,
      this.getOptions()
    );
  }
  getRepresstGpr(id) {
    return this.getFile(
      `downloadexcelreprgpr?selection_id=${id}`,
      this.getOptions()
    );
  }
  getExcelkart(id) {
    return this.getFile(
      `downloadexcelkart?personal_code=${id}`,
      this.getOptions()
    );
  }
  getExcelkarts(id) {
    return this.getFile(
      `downloadexcelkart/zip?personal_code=${id}`,
      this.getOptions()
    );
  }
  _getSimpleList(id) {
    return this.get(
      `downloadexcel?selection_id=${id}&type=1`,
      this.getOptions()
    );
  }
}
