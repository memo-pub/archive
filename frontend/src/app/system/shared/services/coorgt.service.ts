import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { BaseService } from './base.service';
import { AuthService } from '../../../shared/services/auth.service';
import { ConfigService } from 'src/app/shared/services/config.service';

@Injectable({
  providedIn: 'root'
})
export class CoorgtService extends BaseService {

  constructor(
    public http: HttpClient,
    public configService: ConfigService,
    public authService: AuthService,
  ) {
    super(http, configService, authService, 'coorg_t');
  }

  getByOrgid(org_id) {
    return this.get(`${this.path}/?org_id=${org_id}`, this.getOptions()).toPromise();
  }
}
