import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { BaseService } from './base.service';
import { AuthService } from '../../../shared/services/auth.service';
import { isNullOrUndefined } from 'util';
import { ConfigService } from 'src/app/shared/services/config.service';

@Injectable({
  providedIn: 'root'
})
export class PlacetService extends BaseService {
  constructor(
    public http: HttpClient,
    public configService: ConfigService,
    public authService: AuthService
  ) {
    super(http, configService, authService, 'place_t');
  }

  async getWithOffsetLimit(offset, limit) {
    return this.get(
      `${this.path}/?order=byid&offset=${offset}&limit=${limit}`,
      this.getOptions()
    ).toPromise();
  }

  async getAll_count() {
    return this.get(`${this.path}/_getrowcount_`, this.getOptions()).toPromise();
  }

  async getWithFilterLimit(limit: number, searchStr) {
    let str = `${this.path}/?orderby=byfilter&limit=${limit}&filter=${encodeURI(
      searchStr
    )}&cur_mark=*`;
    return this.get(str, this.getOptions()).toPromise();
  }

  async getByPlace_num(place_num: number) {
    let str = `${this.path}/?place_num=${place_num}&cur_mark=*`;
    return this.get(str, this.getOptions()).toPromise();
  }

  async getWithFilter_count(searchStr) {
    let str = `${this.path}/_getrowcount_?filter=${encodeURI(searchStr)}`;
    return this.get(str, this.getOptions()).toPromise();
  }

  async getWithFilterOffsetLimit(offset: number, limit: number, searchStr) {
    let str = `${
      this.path
    }/?orderby=byplace_num&limit=${limit}&offset=${offset}&filter=${encodeURI(
      searchStr
    )}`;
    return this.get(str, this.getOptions()).toPromise();
  }
}
