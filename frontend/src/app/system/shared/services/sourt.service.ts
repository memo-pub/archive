import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { HttpClient } from '@angular/common/http';
import { AuthService } from 'src/app/shared/services/auth.service';
import { ConfigService } from 'src/app/shared/services/config.service';
import { ListOfT } from 'src/app/shared/models/listoft.model';

@Injectable({
  providedIn: 'root',
})
export class SourtService extends BaseService {
  constructor(
    public http: HttpClient,
    public configService: ConfigService,
    public authService: AuthService
  ) {
    super(http, configService, authService, 'sour_t');
  }

  getByPersonalcode(personal_code: number) {
    let str = `${this.path}/?personal_code=${personal_code}`;
    return this.get(str, this.getOptions()).toPromise();
  }

  async getOneById(id, sourfolderfile = false) {
    let str = `${this.path}/${id}`;
    if (sourfolderfile === true) {
      str += `?sourfolderfile=true`;
      str += '&getjwt=true';
    }
    return this.get(str, this.getOptions()).toPromise();
  }

  async deleteSelectionitem(sour_ids) {
    return this.post(`deletesour_t`, sour_ids, this.getOptions()).toPromise();
  }

  async getFilter_count_check(
    fund?: string,
    list_no?: string,
    file_no?: string
  ) {
    let str = `${this.path}/_getrowcount_?file_no_ext_empty=false`;
    if (fund && fund !== '') {
      str += `&fund=${fund}`;
    }
    if (list_no && list_no !== '') {
      str += `&list_no=${list_no}`;
    }
    if (file_no && file_no !== '') {
      str += `&file_no=${file_no}`;
    }
    return this.get(str, this.getOptions()).toPromise();
  }

  async getFilter_count(
    fund?: string,
    list_no?: string,
    file_no?: string,
    file_no_ext?: string,
    file_no_ext_noneed?: boolean,
    sour_name_filter?: string,
    sour_type_filter?: number,
    pub_type?: number,
    // sour_date_filter?: string,
    filter_sour_date_from?: string,
    filter_sour_date_to?: string,
    searchtxt?: string,
    searchflds?: any[]
  ) {
    let d = '?';
    let str = `${this.path}/_getrowcount_`;
    if (fund && fund !== '') {
      str += `${d}fund=${fund}`;
      d = '&';
    }
    if (list_no && list_no !== '') {
      str += `${d}list_no=${list_no}`;
      d = '&';
    }
    if (file_no && file_no !== '') {
      str += `${d}file_no=${file_no}`;
      d = '&';
    }
    if (file_no_ext_noneed) {
      str += `${d}file_no_ext_empty=${true}`;
      d = '&';
    } else {
      if (file_no_ext && file_no_ext !== '') {
        str += `${d}file_no_ext=${file_no_ext}`;
        d = '&';
      }
    }
    if (sour_name_filter && sour_name_filter !== '') {
      str += `${d}sour_name_filter=${sour_name_filter}`;
      d = '&';
    }
    if (pub_type) {
      str += `${d}pub_type=${pub_type}`;
      d = '&';
    }

    if (filter_sour_date_from) {
      str += `${d}filter_sour_date_from=${filter_sour_date_from}`;
      d = '&';
    }
    if (filter_sour_date_to) {
      str += `${d}filter_sour_date_to=${filter_sour_date_to}`;
      d = '&';
    }
    // if (sour_date_filter) {
    //   str += `${d}sour_date_filter=${sour_date_filter}`;
    //   d = '&';
    // }
    if (sour_type_filter /* && sour_date_filter !== ''*/) {
      str += `${d}sour_type_filter=${sour_type_filter}&sour_type_join=true`;
      d = '&';
    }
    if (searchtxt && searchflds && searchtxt !== '' && searchflds.length > 0) {
      str += `${d}searchtxt=${encodeURI(searchtxt)}`;
      d = '&';
      for (let i = 0; i < searchflds.length; i++) {
        str += `${d}searchflds=${searchflds[i]}`;
     }
    }
    return this.get(str, this.getOptions()).toPromise();
  }
  async getShortFilter(
    limit: number,
    sour_name_filter?: string,
    fund?: string,
    list_no?: string,
    file_no?: string,
    file_no_ext?: string,
    file_no_ext_noneed?: boolean
  ) {
    let str = `${this.path}/`;
    str += `?limit=${limit}`;
    if (sour_name_filter && sour_name_filter !== '') {
      str += `&sour_name_filter=${sour_name_filter}`;
    }
    if (fund && fund !== '') {
      str += `&fund=${fund}`;
    }
    if (list_no && list_no !== '') {
      str += `&list_no=${list_no}`;
    }
    if (file_no && file_no !== '') {
      str += `&file_no=${file_no}`;
    }
    if (file_no_ext && file_no_ext !== '') {
      str += `&file_no_ext=${file_no_ext}`;
    }
    if (file_no_ext_noneed) {
      str += `&file_no_ext_empty=${true}`;
    } else {
      if (file_no_ext && file_no_ext !== '') {
        str += `&file_no_ext=${file_no_ext}`;
      }
    }
    return this.get(str, this.getOptions()).toPromise();
  }
  async getFilter(
    orderby: string,
    offset: number,
    limit: number,
    fund?: string,
    list_no?: string,
    file_no?: string,
    file_no_ext?: string,
    file_no_ext_noneed?: boolean,
    sour_name_filter?: string,
    sour_type_filter?: number,
    pub_type?: number,
    // sour_date_filter?: string,
    filter_sour_date_from?: string,
    filter_sour_date_to?: string,
    searchtxt?: string,
    searchflds?: any[]
  ) {
    let d = '&';
    let str = `${this.path}/`;
    str += `?offset=${offset}&limit=${limit}&sour_type_join=true&orderby=${orderby}&extra_person=true`;
    if (fund && fund !== '') {
      str += `${d}fund=${fund}`;
      d = '&';
    }
    if (list_no && list_no !== '') {
      str += `${d}list_no=${list_no}`;
      d = '&';
    }
    if (file_no && file_no !== '') {
      str += `${d}file_no=${file_no}`;
      d = '&';
    }
    if (file_no_ext_noneed) {
      str += `${d}file_no_ext_empty=${true}`;
      d = '&';
    } else {
      if (file_no_ext && file_no_ext !== '') {
        str += `${d}file_no_ext=${file_no_ext}`;
        d = '&';
      }
    }
    // if (file_no_ext && !file_no_ext_noneed) {
    //   str += `${d}file_no_ext=${file_no_ext}`;
    //   d = '&';
    // } else {
    //   str += `${d}file_no_ext_empty=${true}`;
    //   d = '&';
    // }
    if (sour_name_filter && sour_name_filter !== '') {
      str += `${d}sour_name_filter=${sour_name_filter}`;
      d = '&';
    }
    if (pub_type) {
      str += `${d}pub_type=${pub_type}`;
      d = '&';
    }
    if (filter_sour_date_from) {
      str += `${d}filter_sour_date_from=${filter_sour_date_from}`;
      d = '&';
    }
    if (filter_sour_date_to) {
      str += `${d}filter_sour_date_to=${filter_sour_date_to}`;
      d = '&';
    }
    // if (sour_date_filter) {
    //   str += `${d}sour_date_filter=${sour_date_filter}`;
    //   d = '&';
    // }
    if (sour_type_filter /*&& sour_date_filter !== ''*/) {
      str += `${d}sour_type_filter=${sour_type_filter}`;
      d = '&';
    }
    if (searchtxt && searchflds && searchtxt !== '' && searchflds.length > 0) {
      str += `${d}searchtxt=${encodeURI(searchtxt)}`;
      d = '&';
      for (let i = 0; i < searchflds.length; i++) {
        str += `${d}searchflds=${searchflds[i]}`;
      }
    }
    return this.get(str, this.getOptions()).toPromise();
  }

  async getnextkey(
    orderby: string,
    id: number,
    fund?: string,
    list_no?: string,
    file_no?: string,
    file_no_ext?: string,
    file_no_ext_noneed?: boolean,
    sour_name_filter?: string,
    sour_type_filter?: number,
    pub_type?: number,
    // sour_date_filter?: string
    filter_sour_date_from?: string,
    filter_sour_date_to?: string
  ) {
    let str = `${this.path}/${id}/_getnextkey_`;
    let d = '&';
    str += `?sour_type_join=true&orderby=${orderby}`;
    if (fund && fund !== '') {
      str += `${d}fund=${fund}`;
      d = '&';
    }
    if (list_no && list_no !== '') {
      str += `${d}list_no=${list_no}`;
      d = '&';
    }
    if (file_no && file_no !== '') {
      str += `${d}file_no=${file_no}`;
      d = '&';
    }
    if (file_no_ext && file_no_ext !== '') {
      str += `${d}file_no_ext=${file_no_ext}`;
      d = '&';
    }
    if (file_no_ext_noneed) {
      str += `${d}file_no_ext_empty=${true}`;
      d = '&';
    } else {
      if (file_no_ext && file_no_ext !== '') {
        str += `${d}file_no_ext=${file_no_ext}`;
        d = '&';
      }
    }
    if (sour_name_filter && sour_name_filter !== '') {
      str += `${d}sour_name_filter=${sour_name_filter}`;
      d = '&';
    }
    if (pub_type) {
      str += `${d}pub_type=${pub_type}`;
      d = '&';
    }
    if (filter_sour_date_from) {
      str += `${d}filter_sour_date_from=${filter_sour_date_from}`;
      d = '&';
    }
    if (filter_sour_date_to) {
      str += `${d}filter_sour_date_to=${filter_sour_date_to}`;
      d = '&';
    }
    // if (sour_date_filter) {
    //   str += `${d}sour_date_filter=${sour_date_filter}`;
    //   d = '&';
    // }
    if (sour_type_filter /*&& sour_date_filter !== ''*/) {
      str += `${d}sour_type_filter=${sour_type_filter}`;
      d = '&';
    }
    return this.get(str, this.getOptions()).toPromise();
  }

  async getpreviouskey(
    orderby: string,
    id: number,
    fund?: string,
    list_no?: string,
    file_no?: string,
    file_no_ext?: string,
    file_no_ext_noneed?: boolean,
    sour_name_filter?: string,
    sour_type_filter?: number,
    pub_type?: number,
    // sour_date_filter?: string
    filter_sour_date_from?: string,
    filter_sour_date_to?: string
  ) {
    let str = `${this.path}/${id}/_getpreviouskey_`;
    let d = '&';
    str += `?sour_type_join=true&orderby=${orderby}`;
    if (fund && fund !== '') {
      str += `${d}fund=${fund}`;
      d = '&';
    }
    if (list_no && list_no !== '') {
      str += `${d}list_no=${list_no}`;
      d = '&';
    }
    if (file_no && file_no !== '') {
      str += `${d}file_no=${file_no}`;
      d = '&';
    }
    if (file_no_ext && file_no_ext !== '') {
      str += `${d}file_no_ext=${file_no_ext}`;
      d = '&';
    }
    if (file_no_ext_noneed) {
      str += `${d}file_no_ext_empty=${true}`;
      d = '&';
    } else {
      if (file_no_ext && file_no_ext !== '') {
        str += `${d}file_no_ext=${file_no_ext}`;
        d = '&';
      }
    }
    if (sour_name_filter && sour_name_filter !== '') {
      str += `${d}sour_name_filter=${sour_name_filter}`;
      d = '&';
    }
    if (pub_type) {
      str += `${d}pub_type=${pub_type}`;
      d = '&';
    }

    if (filter_sour_date_from) {
      str += `${d}filter_sour_date_from=${filter_sour_date_from}`;
      d = '&';
    }
    if (filter_sour_date_to) {
      str += `${d}filter_sour_date_to=${filter_sour_date_to}`;
      d = '&';
    }
    // if (sour_date_filter) {
    //   str += `${d}sour_date_filter=${sour_date_filter}`;
    //   d = '&';
    // }
    if (sour_type_filter /*&& sour_date_filter !== ''*/) {
      str += `${d}sour_type_filter=${sour_type_filter}`;
      d = '&';
    }
    return this.get(str, this.getOptions()).toPromise();
  }

  getWithOffsetLimit(offset, limit, nochildren = false) {
    let str = `${this.path}/?order=byid&offset=${offset}&limit=${limit}`;
    if (nochildren) {
      str += '&nochildren=true';
    }
    return this.get(str, this.getOptions()).toPromise();
  }
}
