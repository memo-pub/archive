import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../../../shared/services/auth.service';
import { isNullOrUndefined } from 'util';
import { ConfigService } from 'src/app/shared/services/config.service';

@Injectable({
  providedIn: 'root',
})
export class SourtlinkService extends BaseService {
  constructor(
    public http: HttpClient,
    public configService: ConfigService,
    public authService: AuthService
  ) {
    super(http, configService, authService, 'sour_t_link');
  }

  async getAllWithNamesById(id) {
    let _res1 = this.get(
      `${this.path}/?st1=true&st2=true&sour_t_id1=${id}`,
      this.getOptions()
    ).toPromise();
    let _res2 = this.get(
      `${this.path}/?st1=true&st2=true&sour_t_id2=${id}`,
      this.getOptions()
    ).toPromise();
    let res1 = isNullOrUndefined(await _res1) ? [] : await _res1;
    let res2 = isNullOrUndefined(await _res2) ? [] : await _res2;
    return res1.concat(res2);
  }
}
