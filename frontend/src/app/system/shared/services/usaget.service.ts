import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { HttpClient } from '@angular/common/http';
import { AuthService } from 'src/app/shared/services/auth.service';
import { ConfigService } from 'src/app/shared/services/config.service';

@Injectable({
  providedIn: 'root',
})
export class UsagetService extends BaseService {
  constructor(
    public http: HttpClient,
    public configService: ConfigService,
    public authService: AuthService
  ) {
    super(http, configService, authService, 'usage_t');
  }

  getByArchivefolderId(archivefolder_id) {
    return this.get(
      `${this.path}/?archivefolder_id=${archivefolder_id}`,
      this.getOptions()
    ).toPromise();
  }
}
