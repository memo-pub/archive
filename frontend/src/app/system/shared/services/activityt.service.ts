import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { BaseService } from './base.service';
import { AuthService } from '../../../shared/services/auth.service';
import { ConfigService } from 'src/app/shared/services/config.service';

@Injectable({
  providedIn: 'root'
})
export class ActivitytService extends BaseService {
  constructor(
    public http: HttpClient,
    public configService: ConfigService,
    public authService: AuthService
  ) {
    super(http, configService, authService, 'activity_t');
  }

  getByPersonalcode(personal_code) {
    return this.get(
      `${this.path}/?personal_code=${personal_code}`,
      this.getOptions()
    ).toPromise();
  }
}
