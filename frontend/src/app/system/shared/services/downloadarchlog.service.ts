import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { BaseService } from './base.service';
import { AuthService } from '../../../shared/services/auth.service';
import { ConfigService } from 'src/app/shared/services/config.service';

@Injectable({
  providedIn: 'root',
})
export class DownloadarchlogService extends BaseService {
  constructor(
    public http: HttpClient,
    public configService: ConfigService,
    public authService: AuthService
  ) {
    super(http, configService, authService, 'downloadarchlog');
  }

  getAf_history(id) {
    return this.getFile(
      `downloadarchlog?archivefolder_id=${id}`,
      this.getOptions()
    );
  }
}
