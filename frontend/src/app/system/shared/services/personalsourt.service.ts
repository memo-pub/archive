import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { HttpClient } from '@angular/common/http';
import { AuthService } from 'src/app/shared/services/auth.service';
import { ConfigService } from 'src/app/shared/services/config.service';

@Injectable({
  providedIn: 'root',
})
export class PersonalsourtService extends BaseService {
  constructor(
    public http: HttpClient,
    public configService: ConfigService,
    public authService: AuthService
  ) {
    super(http, configService, authService, 'personal_sour_t');
  }

  getByPersonalcodeSourId(personal_code: number, sour_id: number) {
    let str = `${this.path}/?personal_code=${personal_code}&sour_id=${sour_id}`;
    return this.get(str, this.getOptions()).toPromise();
  }

  getByPersonalcode(personal_code: number) {
    let str = `${this.path}/?personal_code=${personal_code}&sour_dan=true&orderby=bysour_no`;
    return this.get(str, this.getOptions()).toPromise();
  }
}
