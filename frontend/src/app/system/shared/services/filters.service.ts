import { Injectable } from '@angular/core';
import { PersonalTFilter } from 'src/app/shared/models/personalt.model';
import { ArchivefolderFilter } from 'src/app/shared/models/archivefolder.model';
import {
  FieldjoinFilter,
  MuserFilter,
} from 'src/app/shared/models/muser.model';
import { OpisFilter } from 'src/app/shared/models/opis.model';
import { PlaceTFilter } from 'src/app/shared/models/placet.model';
import { SelectionFilter } from 'src/app/shared/models/selection.model';
import { SourtFilter } from 'src/app/shared/models/sourt.model';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class FiltersService {
  /**Новые фильтра, старые убрать */
  public personalTDialogFilter: PersonalTFilter;
  public personalTListFilter: PersonalTFilter;
  public selectionEditFilter: PersonalTFilter;
  public archivefolderListFilter: ArchivefolderFilter;
  public muserListFilter: MuserFilter;
  public fieldjoinListFilter: FieldjoinFilter;
  public opisListFilter: OpisFilter;
  public placeTListFilter: PlaceTFilter;
  public selectionListFilter: SelectionFilter;
  public selectionSourtFilter: SourtFilter;
  public newDoc = false; // признак перехода в дело по кнопке добавить
  public tempOpisid: number | null;
  public sourtPlusOne = {
    fund: null,
    list_no: null,
    file_no: null,
    file_no_ext: null,
    sour_no: null,
    newnom: null,
  };
  public pinPersonal: BehaviorSubject<number>;

  constructor() {
    this.pinPersonal = new BehaviorSubject(
      JSON.parse(window.localStorage.getItem('code'))
    );
    this.personalTDialogFilter = new PersonalTFilter();
    this.personalTListFilter = new PersonalTFilter();
    this.selectionEditFilter = new PersonalTFilter();
    this.archivefolderListFilter = new ArchivefolderFilter();
    this.muserListFilter = new MuserFilter();
    this.fieldjoinListFilter = new FieldjoinFilter();
    this.opisListFilter = new OpisFilter();
    this.placeTListFilter = new PlaceTFilter();
    this.selectionListFilter = new SelectionFilter();
    this.selectionSourtFilter = new SourtFilter();
  }
}
