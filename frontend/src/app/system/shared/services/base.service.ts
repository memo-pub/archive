import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Inject, Optional } from '@angular/core';

import { BaseApi } from '../../../shared/core/base-api';
import { AuthService } from '../../../shared/services/auth.service';
import { Muser } from '../../../shared/models/muser.model';
import { isNullOrUndefined } from 'util';
import { ConfigService } from 'src/app/shared/services/config.service';

export class BaseService extends BaseApi {
  // loggedInUser: Muser;
  // options: HttpHeaders;

  constructor(
    public http: HttpClient,
    public configService: ConfigService,
    public authService: AuthService,
    @Inject('pathStr') @Optional() public path: string
  ) {
    super(http, configService);
    this.path = path;
    // this.loggedInUser = this.authService.getCurUser();
    // this.options = new HttpHeaders();
    // this.options = this.options.set(
    //   'Authorization',
    //   'JWT ' + this.loggedInUser.token
    // );
    // this.options = this.options.append('Content-Type', 'application/json');
  }

  getOptions(): HttpHeaders {
    let options = new HttpHeaders();
    if (this.authService.isLoggedIn()) {
      options = options.set(
        'Authorization',
        'JWT ' + this.authService.getCurUser().token
      );
    }
    options = options.append('Content-Type', 'application/json');
    return options;
  }

  async getAll() {
    return this.get(`${this.path}`, this.getOptions()).toPromise();
  }
  async getOneById(id, tree = false, ment_pers_data = false) {
    let str = `${this.path}/${id}`;
    if (tree === true) {
      str += `?tree=true`;
    }
    if (ment_pers_data === true) {
      str += `&ment_pers_data=true`;
    }
    return this.get(str, this.getOptions()).toPromise();
  }

  async postOne(data) {
    /**фикс дат для сафари */
    data.us_date_end = isNullOrUndefined(data.us_date_end)
      ? data.us_date_end
      : new Date(data.us_date_end).toDateString();
    data.us_date_begin = isNullOrUndefined(data.us_date_begin)
      ? data.us_date_begin
      : new Date(data.us_date_begin).toDateString();
    data.dismis_dat_begin = isNullOrUndefined(data.dismis_dat_begin)
      ? data.dismis_dat_begin
      : new Date(data.dismis_dat_begin).toDateString();
    data.dismis_dat_end = isNullOrUndefined(data.dismis_dat_end)
      ? data.dismis_dat_end
      : new Date(data.dismis_dat_end).toDateString();
    data.join_dat_end = isNullOrUndefined(data.join_dat_end)
      ? data.join_dat_end
      : new Date(data.join_dat_end).toDateString();
    data.join_dat_begin = isNullOrUndefined(data.join_dat_begin)
      ? data.join_dat_begin
      : new Date(data.join_dat_begin).toDateString();
    data.arrival_dat_begin = isNullOrUndefined(data.arrival_dat_begin)
      ? data.arrival_dat_begin
      : new Date(data.arrival_dat_begin).toDateString();
    data.arrival_dat_end = isNullOrUndefined(data.arrival_dat_end)
      ? data.arrival_dat_end
      : new Date(data.arrival_dat_end).toDateString();
    data.leave_dat_begin = isNullOrUndefined(data.leave_dat_begin)
      ? data.leave_dat_begin
      : new Date(data.leave_dat_begin).toDateString();
    data.leave_dat_end = isNullOrUndefined(data.leave_dat_end)
      ? data.leave_dat_end
      : new Date(data.leave_dat_end).toDateString();
    data.sentense_date_begin = isNullOrUndefined(data.sentense_date_begin)
      ? data.sentense_date_begin
      : new Date(data.sentense_date_begin).toDateString();
    data.sentense_date_end = isNullOrUndefined(data.sentense_date_end)
      ? data.sentense_date_end
      : new Date(data.sentense_date_end).toDateString();
    data.inq_date_begin = isNullOrUndefined(data.inq_date_begin)
      ? data.inq_date_begin
      : new Date(data.inq_date_begin).toDateString();
    data.inq_date_end = isNullOrUndefined(data.inq_date_end)
      ? data.inq_date_end
      : new Date(data.inq_date_end).toDateString();
    data.repress_dat_begin = isNullOrUndefined(data.repress_dat_begin)
      ? data.repress_dat_begin
      : new Date(data.repress_dat_begin).toDateString();
    data.repress_dat_end = isNullOrUndefined(data.repress_dat_end)
      ? data.repress_dat_end
      : new Date(data.repress_dat_end).toDateString();
    data.reabil_dat_begin = isNullOrUndefined(data.reabil_dat_begin)
      ? data.reabil_dat_begin
      : new Date(data.reabil_dat_begin).toDateString();
    data.reabil_dat_end = isNullOrUndefined(data.reabil_dat_end)
      ? data.reabil_dat_end
      : new Date(data.reabil_dat_end).toDateString();
    data.depart_dat_begin = isNullOrUndefined(data.depart_dat_begin)
      ? data.depart_dat_begin
      : new Date(data.depart_dat_begin).toDateString();
    data.depart_dat_end = isNullOrUndefined(data.depart_dat_end)
      ? data.depart_dat_end
      : new Date(data.depart_dat_end).toDateString();
    data.arrival_dat_begin = isNullOrUndefined(data.arrival_dat_begin)
      ? data.arrival_dat_begin
      : new Date(data.arrival_dat_begin).toDateString();
    data.arrival_dat_end = isNullOrUndefined(data.arrival_dat_end)
      ? data.arrival_dat_end
      : new Date(data.arrival_dat_end).toDateString();
    data.sour_date_begin = isNullOrUndefined(data.sour_date_begin)
      ? data.sour_date_begin
      : new Date(data.sour_date_begin).toDateString();
    data.sour_date_end = isNullOrUndefined(data.sour_date_end)
      ? data.sour_date_end
      : new Date(data.sour_date_end).toDateString();
    data.birth = isNullOrUndefined(data.birth)
      ? data.birth
      : new Date(data.birth).toDateString();
    data.birth_begin = isNullOrUndefined(data.birth_begin)
      ? data.birth_begin
      : new Date(data.birth_begin).toDateString();
    data.birth_end = isNullOrUndefined(data.birth_end)
      ? data.birth_end
      : new Date(data.birth_end).toDateString();
    data.death = isNullOrUndefined(data.death)
      ? data.death
      : new Date(data.death).toDateString();
    data.death_begin = isNullOrUndefined(data.death_begin)
      ? data.death_begin
      : new Date(data.death_begin).toDateString();
    data.death_end = isNullOrUndefined(data.death_end)
      ? data.death_end
      : new Date(data.death_end).toDateString();
    data.inform_dat = isNullOrUndefined(data.inform_dat)
      ? data.inform_dat
      : new Date(data.inform_dat).toDateString();
    data.arrival_dat = isNullOrUndefined(data.arrival_dat)
      ? data.arrival_dat
      : new Date(data.arrival_dat).toDateString();
    data.depart_dat = isNullOrUndefined(data.depart_dat)
      ? data.depart_dat
      : new Date(data.depart_dat).toDateString();
    data.help_dat = isNullOrUndefined(data.help_dat)
      ? data.help_dat
      : new Date(data.help_dat).toDateString();
    data.join_dat = isNullOrUndefined(data.join_dat)
      ? data.join_dat
      : new Date(data.join_dat).toDateString();
    data.dismis_dat = isNullOrUndefined(data.dismis_dat)
      ? data.dismis_dat
      : new Date(data.dismis_dat).toDateString();
    data.reabil_dat = isNullOrUndefined(data.reabil_dat)
      ? data.reabil_dat
      : new Date(data.reabil_dat).toDateString();
    data.repress_dat = isNullOrUndefined(data.repress_dat)
      ? data.repress_dat
      : new Date(data.repress_dat).toDateString();
    data.us_date = isNullOrUndefined(data.us_date)
      ? data.us_date
      : new Date(data.us_date).toDateString();
    data.inq_date = isNullOrUndefined(data.inq_date)
      ? data.inq_date
      : new Date(data.inq_date).toDateString();
    data.sentense_date = isNullOrUndefined(data.sentense_date)
      ? data.sentense_date
      : new Date(data.sentense_date).toDateString();
    data.leave_dat = isNullOrUndefined(data.leave_dat)
      ? data.leave_dat
      : new Date(data.leave_dat).toDateString();
    data.arrival_dat = isNullOrUndefined(data.arrival_dat)
      ? data.arrival_dat
      : new Date(data.arrival_dat).toDateString();
    data.personal_t_birth_from = isNullOrUndefined(data.personal_t_birth_from)
      ? data.personal_t_birth_from
      : new Date(data.personal_t_birth_from).toDateString();
    data.personal_t_birth_to = isNullOrUndefined(data.personal_t_birth_to)
      ? data.personal_t_birth_to
      : new Date(data.personal_t_birth_to).toDateString();
    data.personal_t_death1 = isNullOrUndefined(data.personal_t_death1)
      ? data.personal_t_death1
      : new Date(data.personal_t_death1).toDateString();
    data.personal_t_death2 = isNullOrUndefined(data.personal_t_death2)
      ? data.personal_t_death2
      : new Date(data.personal_t_death2).toDateString();
    data.activity_t_arrival_dat1 = isNullOrUndefined(
      data.activity_t_arrival_dat1
    )
      ? data.activity_t_arrival_dat1
      : new Date(data.activity_t_arrival_dat1).toDateString();
    data.activity_t_arrival_dat2 = isNullOrUndefined(
      data.activity_t_arrival_dat2
    )
      ? data.activity_t_arrival_dat2
      : new Date(data.activity_t_arrival_dat2).toDateString();
    data.activity_t_depart_dat1 = isNullOrUndefined(data.activity_t_depart_dat1)
      ? data.activity_t_depart_dat1
      : new Date(data.activity_t_depart_dat1).toDateString();
    data.activity_t_depart_dat2 = isNullOrUndefined(data.activity_t_depart_dat2)
      ? data.activity_t_depart_dat2
      : new Date(data.activity_t_depart_dat2).toDateString();
    data.org_t_join_dat1 = isNullOrUndefined(data.org_t_join_dat1)
      ? data.org_t_join_dat1
      : new Date(data.org_t_join_dat1).toDateString();
    data.org_t_join_dat2 = isNullOrUndefined(data.org_t_join_dat2)
      ? data.org_t_join_dat2
      : new Date(data.org_t_join_dat2).toDateString();
    data.org_t_dismis_dat1 = isNullOrUndefined(data.org_t_dismis_dat1)
      ? data.org_t_dismis_dat1
      : new Date(data.org_t_dismis_dat1).toDateString();
    data.org_t_dismis_dat2 = isNullOrUndefined(data.org_t_dismis_dat2)
      ? data.org_t_dismis_dat2
      : new Date(data.org_t_dismis_dat2).toDateString();
    data.repress_t_repress_dat1 = isNullOrUndefined(data.repress_t_repress_dat1)
      ? data.repress_t_repress_dat1
      : new Date(data.repress_t_repress_dat1).toDateString();
    data.repress_t_repress_dat2 = isNullOrUndefined(data.repress_t_repress_dat2)
      ? data.repress_t_repress_dat2
      : new Date(data.repress_t_repress_dat2).toDateString();
    data.repress_t_reabil_dat1 = isNullOrUndefined(data.repress_t_reabil_dat1)
      ? data.repress_t_reabil_dat1
      : new Date(data.repress_t_reabil_dat1).toDateString();
    data.repress_t_reabil_dat2 = isNullOrUndefined(data.repress_t_reabil_dat2)
      ? data.repress_t_reabil_dat2
      : new Date(data.repress_t_reabil_dat2).toDateString();
    data.court_t_inq_date1 = isNullOrUndefined(data.court_t_inq_date1)
      ? data.court_t_inq_date1
      : new Date(data.court_t_inq_date1).toDateString();
    data.court_t_inq_date2 = isNullOrUndefined(data.court_t_inq_date2)
      ? data.court_t_inq_date2
      : new Date(data.court_t_inq_date2).toDateString();
    data.court_t_sentense_date1 = isNullOrUndefined(data.court_t_sentense_date1)
      ? data.court_t_sentense_date1
      : new Date(data.court_t_sentense_date1).toDateString();
    data.court_t_sentense_date2 = isNullOrUndefined(data.court_t_sentense_date2)
      ? data.court_t_sentense_date2
      : new Date(data.court_t_sentense_date2).toDateString();
    data.impris_t_arrival_dat1 = isNullOrUndefined(data.impris_t_arrival_dat1)
      ? data.impris_t_arrival_dat1
      : new Date(data.impris_t_arrival_dat1).toDateString();
    data.impris_t_arrival_dat2 = isNullOrUndefined(data.impris_t_arrival_dat2)
      ? data.impris_t_arrival_dat2
      : new Date(data.impris_t_arrival_dat2).toDateString();
    data.impris_t_leave_dat1 = isNullOrUndefined(data.impris_t_leave_dat1)
      ? data.impris_t_leave_dat1
      : new Date(data.impris_t_leave_dat1).toDateString();
    data.impris_t_leave_dat2 = isNullOrUndefined(data.impris_t_leave_dat2)
      ? data.impris_t_leave_dat2
      : new Date(data.impris_t_leave_dat2).toDateString();
    data.sour_t_sour_date = isNullOrUndefined(data.sour_t_sour_date)
      ? data.sour_t_sour_date
      : new Date(data.sour_t_sour_date).toDateString();
    data.help_t_help_dat1 = isNullOrUndefined(data.help_t_help_dat1)
      ? data.help_t_help_dat1
      : new Date(data.help_t_help_dat1).toDateString();
    data.help_t_help_dat2 = isNullOrUndefined(data.help_t_help_dat2)
      ? data.help_t_help_dat2
      : new Date(data.help_t_help_dat2).toDateString();
    return this.post(`${this.path}`, data, this.getOptions()).toPromise();
  }

  async putOneById(id, data) {
    data.us_date_end = isNullOrUndefined(data.us_date_end)
      ? data.us_date_end
      : new Date(data.us_date_end).toDateString();
    data.us_date_begin = isNullOrUndefined(data.us_date_begin)
      ? data.us_date_begin
      : new Date(data.us_date_begin).toDateString();
    data.dismis_dat_begin = isNullOrUndefined(data.dismis_dat_begin)
      ? data.dismis_dat_begin
      : new Date(data.dismis_dat_begin).toDateString();
    data.dismis_dat_end = isNullOrUndefined(data.dismis_dat_end)
      ? data.dismis_dat_end
      : new Date(data.dismis_dat_end).toDateString();
    data.join_dat_end = isNullOrUndefined(data.join_dat_end)
      ? data.join_dat_end
      : new Date(data.join_dat_end).toDateString();
    data.join_dat_begin = isNullOrUndefined(data.join_dat_begin)
      ? data.join_dat_begin
      : new Date(data.join_dat_begin).toDateString();
    data.arrival_dat_begin = isNullOrUndefined(data.arrival_dat_begin)
      ? data.arrival_dat_begin
      : new Date(data.arrival_dat_begin).toDateString();
    data.arrival_dat_end = isNullOrUndefined(data.arrival_dat_end)
      ? data.arrival_dat_end
      : new Date(data.arrival_dat_end).toDateString();
    data.leave_dat_begin = isNullOrUndefined(data.leave_dat_begin)
      ? data.leave_dat_begin
      : new Date(data.leave_dat_begin).toDateString();
    data.leave_dat_end = isNullOrUndefined(data.leave_dat_end)
      ? data.leave_dat_end
      : new Date(data.leave_dat_end).toDateString();
    data.sentense_date_begin = isNullOrUndefined(data.sentense_date_begin)
      ? data.sentense_date_begin
      : new Date(data.sentense_date_begin).toDateString();
    data.sentense_date_end = isNullOrUndefined(data.sentense_date_end)
      ? data.sentense_date_end
      : new Date(data.sentense_date_end).toDateString();
    data.inq_date_begin = isNullOrUndefined(data.inq_date_begin)
      ? data.inq_date_begin
      : new Date(data.inq_date_begin).toDateString();
    data.inq_date_end = isNullOrUndefined(data.inq_date_end)
      ? data.inq_date_end
      : new Date(data.inq_date_end).toDateString();
    data.repress_dat_begin = isNullOrUndefined(data.repress_dat_begin)
      ? data.repress_dat_begin
      : new Date(data.repress_dat_begin).toDateString();
    data.repress_dat_end = isNullOrUndefined(data.repress_dat_end)
      ? data.repress_dat_end
      : new Date(data.repress_dat_end).toDateString();
    data.reabil_dat_begin = isNullOrUndefined(data.reabil_dat_begin)
      ? data.reabil_dat_begin
      : new Date(data.reabil_dat_begin).toDateString();
    data.reabil_dat_end = isNullOrUndefined(data.reabil_dat_end)
      ? data.reabil_dat_end
      : new Date(data.reabil_dat_end).toDateString();
    data.depart_dat_begin = isNullOrUndefined(data.depart_dat_begin)
      ? data.depart_dat_begin
      : new Date(data.depart_dat_begin).toDateString();
    data.depart_dat_end = isNullOrUndefined(data.depart_dat_end)
      ? data.depart_dat_end
      : new Date(data.depart_dat_end).toDateString();
    data.arrival_dat_begin = isNullOrUndefined(data.arrival_dat_begin)
      ? data.arrival_dat_begin
      : new Date(data.arrival_dat_begin).toDateString();
    data.arrival_dat_end = isNullOrUndefined(data.arrival_dat_end)
      ? data.arrival_dat_end
      : new Date(data.arrival_dat_end).toDateString();
    data.sour_date_begin = isNullOrUndefined(data.sour_date_begin)
      ? data.sour_date_begin
      : new Date(data.sour_date_begin).toDateString();
    data.sour_date_end = isNullOrUndefined(data.sour_date_end)
      ? data.sour_date_end
      : new Date(data.sour_date_end).toDateString();
    data.pub_date = isNullOrUndefined(data.pub_date)
      ? data.pub_date
      : new Date(data.pub_date).toDateString();
    data.birth = isNullOrUndefined(data.birth)
      ? data.birth
      : new Date(data.birth).toDateString();
    data.birth_begin = isNullOrUndefined(data.birth_begin)
      ? data.birth_begin
      : new Date(data.birth_begin).toDateString();
    data.birth_end = isNullOrUndefined(data.birth_end)
      ? data.birth_end
      : new Date(data.birth_end).toDateString();
    data.death = isNullOrUndefined(data.death)
      ? data.death
      : new Date(data.death).toDateString();
    data.death_begin = isNullOrUndefined(data.death_begin)
      ? data.death_begin
      : new Date(data.death_begin).toDateString();
    data.death_end = isNullOrUndefined(data.death_end)
      ? data.death_end
      : new Date(data.death_end).toDateString();
    data.inform_dat = isNullOrUndefined(data.inform_dat)
      ? data.inform_dat
      : new Date(data.inform_dat).toDateString();
    data.arrival_dat = isNullOrUndefined(data.arrival_dat)
      ? data.arrival_dat
      : new Date(data.arrival_dat).toDateString();
    data.depart_dat = isNullOrUndefined(data.depart_dat)
      ? data.depart_dat
      : new Date(data.depart_dat).toDateString();
    data.help_dat = isNullOrUndefined(data.help_dat)
      ? data.help_dat
      : new Date(data.help_dat).toDateString();
    data.join_dat = isNullOrUndefined(data.join_dat)
      ? data.join_dat
      : new Date(data.join_dat).toDateString();
    data.dismis_dat = isNullOrUndefined(data.dismis_dat)
      ? data.dismis_dat
      : new Date(data.dismis_dat).toDateString();
    data.reabil_dat = isNullOrUndefined(data.reabil_dat)
      ? data.reabil_dat
      : new Date(data.reabil_dat).toDateString();
    data.repress_dat = isNullOrUndefined(data.repress_dat)
      ? data.repress_dat
      : new Date(data.repress_dat).toDateString();
    data.us_date = isNullOrUndefined(data.us_date)
      ? data.us_date
      : new Date(data.us_date).toDateString();
    data.inq_date = isNullOrUndefined(data.inq_date)
      ? data.inq_date
      : new Date(data.inq_date).toDateString();
    data.sentense_date = isNullOrUndefined(data.sentense_date)
      ? data.sentense_date
      : new Date(data.sentense_date).toDateString();
    data.leave_dat = isNullOrUndefined(data.leave_dat)
      ? data.leave_dat
      : new Date(data.leave_dat).toDateString();
    data.arrival_dat = isNullOrUndefined(data.arrival_dat)
      ? data.arrival_dat
      : new Date(data.arrival_dat).toDateString();
    data.personal_t_birth_from = isNullOrUndefined(data.personal_t_birth_from)
      ? data.personal_t_birth_from
      : new Date(data.personal_t_birth_from).toDateString();
    data.personal_t_birth_to = isNullOrUndefined(data.personal_t_birth_to)
      ? data.personal_t_birth_to
      : new Date(data.personal_t_birth_to).toDateString();
    data.personal_t_death1 = isNullOrUndefined(data.personal_t_death1)
      ? data.personal_t_death1
      : new Date(data.personal_t_death1).toDateString();
    data.personal_t_death2 = isNullOrUndefined(data.personal_t_death2)
      ? data.personal_t_death2
      : new Date(data.personal_t_death2).toDateString();
    data.activity_t_arrival_dat1 = isNullOrUndefined(
      data.activity_t_arrival_dat1
    )
      ? data.activity_t_arrival_dat1
      : new Date(data.activity_t_arrival_dat1).toDateString();
    data.activity_t_arrival_dat2 = isNullOrUndefined(
      data.activity_t_arrival_dat2
    )
      ? data.activity_t_arrival_dat2
      : new Date(data.activity_t_arrival_dat2).toDateString();
    data.activity_t_depart_dat1 = isNullOrUndefined(data.activity_t_depart_dat1)
      ? data.activity_t_depart_dat1
      : new Date(data.activity_t_depart_dat1).toDateString();
    data.activity_t_depart_dat2 = isNullOrUndefined(data.activity_t_depart_dat2)
      ? data.activity_t_depart_dat2
      : new Date(data.activity_t_depart_dat2).toDateString();
    data.org_t_join_dat1 = isNullOrUndefined(data.org_t_join_dat1)
      ? data.org_t_join_dat1
      : new Date(data.org_t_join_dat1).toDateString();
    data.org_t_join_dat2 = isNullOrUndefined(data.org_t_join_dat2)
      ? data.org_t_join_dat2
      : new Date(data.org_t_join_dat2).toDateString();
    data.org_t_dismis_dat1 = isNullOrUndefined(data.org_t_dismis_dat1)
      ? data.org_t_dismis_dat1
      : new Date(data.org_t_dismis_dat1).toDateString();
    data.org_t_dismis_dat2 = isNullOrUndefined(data.org_t_dismis_dat2)
      ? data.org_t_dismis_dat2
      : new Date(data.org_t_dismis_dat2).toDateString();
    data.repress_t_repress_dat1 = isNullOrUndefined(data.repress_t_repress_dat1)
      ? data.repress_t_repress_dat1
      : new Date(data.repress_t_repress_dat1).toDateString();
    data.repress_t_repress_dat2 = isNullOrUndefined(data.repress_t_repress_dat2)
      ? data.repress_t_repress_dat2
      : new Date(data.repress_t_repress_dat2).toDateString();
    data.repress_t_reabil_dat1 = isNullOrUndefined(data.repress_t_reabil_dat1)
      ? data.repress_t_reabil_dat1
      : new Date(data.repress_t_reabil_dat1).toDateString();
    data.repress_t_reabil_dat2 = isNullOrUndefined(data.repress_t_reabil_dat2)
      ? data.repress_t_reabil_dat2
      : new Date(data.repress_t_reabil_dat2).toDateString();
    data.court_t_inq_date1 = isNullOrUndefined(data.court_t_inq_date1)
      ? data.court_t_inq_date1
      : new Date(data.court_t_inq_date1).toDateString();
    data.court_t_inq_date2 = isNullOrUndefined(data.court_t_inq_date2)
      ? data.court_t_inq_date2
      : new Date(data.court_t_inq_date2).toDateString();
    data.court_t_sentense_date1 = isNullOrUndefined(data.court_t_sentense_date1)
      ? data.court_t_sentense_date1
      : new Date(data.court_t_sentense_date1).toDateString();
    data.court_t_sentense_date2 = isNullOrUndefined(data.court_t_sentense_date2)
      ? data.court_t_sentense_date2
      : new Date(data.court_t_sentense_date2).toDateString();
    data.impris_t_arrival_dat1 = isNullOrUndefined(data.impris_t_arrival_dat1)
      ? data.impris_t_arrival_dat1
      : new Date(data.impris_t_arrival_dat1).toDateString();
    data.impris_t_arrival_dat2 = isNullOrUndefined(data.impris_t_arrival_dat2)
      ? data.impris_t_arrival_dat2
      : new Date(data.impris_t_arrival_dat2).toDateString();
    data.impris_t_leave_dat1 = isNullOrUndefined(data.impris_t_leave_dat1)
      ? data.impris_t_leave_dat1
      : new Date(data.impris_t_leave_dat1).toDateString();
    data.impris_t_leave_dat2 = isNullOrUndefined(data.impris_t_leave_dat2)
      ? data.impris_t_leave_dat2
      : new Date(data.impris_t_leave_dat2).toDateString();
    data.sour_t_sour_date = isNullOrUndefined(data.sour_t_sour_date)
      ? data.sour_t_sour_date
      : new Date(data.sour_t_sour_date).toDateString();
    data.help_t_help_dat1 = isNullOrUndefined(data.help_t_help_dat1)
      ? data.help_t_help_dat1
      : new Date(data.help_t_help_dat1).toDateString();
    data.help_t_help_dat2 = isNullOrUndefined(data.help_t_help_dat2)
      ? data.help_t_help_dat2
      : new Date(data.help_t_help_dat2).toDateString();
    return this.put(`${this.path}/` + id, data, this.getOptions()).toPromise();
  }

  async deleteOneById(id, body?: any) {
    return this.delete(
      `${this.path}/` + id,
      this.getOptions(),
      body
    ).toPromise();
  }
}
