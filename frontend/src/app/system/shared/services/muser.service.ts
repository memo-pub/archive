import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { AuthService } from '../../../shared/services/auth.service';
import { BaseService } from './base.service';
import { ConfigService } from 'src/app/shared/services/config.service';

@Injectable({
  providedIn: 'root'
})
export class MuserService extends BaseService {
  constructor(
    public http: HttpClient,
    public configService: ConfigService,
    public authService: AuthService
  ) {
    super(http, configService, authService, 'muser');
  }

  async getArchstatistic(date1, date2) {
    return this.get(
      `/archstatistic?tm1=${date1}&tm2=${date2}`,
      this.getOptions()
    ).toPromise();
  }

  async getScanstatistic(date1, date2) {
    return this.get(
      `/scanstatistic?tm1=${date1}&tm2=${date2}`,
      this.getOptions()
    ).toPromise();
  }
  async getPersonal(date1, date2) {
    return this.get(
      `/personalstatistic?tm1=${date1}&tm2=${date2}`,
      this.getOptions()
    ).toPromise();
  }
}
