import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { HttpClient } from '@angular/common/http';
import { AuthService } from 'src/app/shared/services/auth.service';
import { ConfigService } from 'src/app/shared/services/config.service';

@Injectable({
  providedIn: 'root'
})
export class CoimptService extends BaseService {

  constructor(
    public http: HttpClient,
    public configService: ConfigService,
    public authService: AuthService,
  ) {
    super(http, configService, authService, 'coimp_t');
  }

  getByImpid(imp_id) {
    return this.get(`${this.path}/?imp_id=${imp_id}`, this.getOptions()).toPromise();
  }
}

