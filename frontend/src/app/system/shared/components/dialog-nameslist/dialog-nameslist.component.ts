import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-dialog-nameslist',
  templateUrl: './dialog-nameslist.component.html',
  styleUrls: ['./dialog-nameslist.component.css'],
})
export class DialogNameslistComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<DialogNameslistComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {}
}
