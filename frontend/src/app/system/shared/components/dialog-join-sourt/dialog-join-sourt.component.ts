import { Component, Inject, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NgxSpinnerService } from 'ngx-spinner';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { PersonalT } from 'src/app/shared/models/personalt.model';
import { SourT } from 'src/app/shared/models/sourt.model';
import { GlobalService } from 'src/app/shared/services/global.service';
import { PersonaltService } from '../../services/personalt.service';
import { SourtService } from '../../services/sourt.service';

@Component({
  selector: 'app-dialog-join-sourt',
  templateUrl: './dialog-join-sourt.component.html',
  styleUrls: ['./dialog-join-sourt.component.css'],
})
export class DialogJoinSourtComponent implements OnInit {
  personalSearch = false;
  fund;
  list_no;
  file_no;

  sourts: SourT[];
  selectedSourts: SourT[] = [];

  human_code: FormControl;
  timerId;
  filteredPersonalts: Observable<PersonalT[]>;
  personalts: PersonalT[];

  constructor(
    public dialogRef: MatDialogRef<DialogJoinSourtComponent>,
    private sourtService: SourtService,
    private personaltService: PersonaltService,
    private globalService: GlobalService,
    private spinnerService: NgxSpinnerService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {
    this.fund = this.data.fund;
    this.list_no = this.data.list_no;
    this.file_no = this.data.file_no;
    this.human_code = new FormControl(null);
    this.updatePage();
  }

  async getData() {}

  async updatePage() {
    this.selectedSourts.length = 0;
    this.sourts = [];
    try {
      this.spinnerService.show();
      if (!this.personalSearch) {
        let sourts = this.sourtService.getFilter(
          'bypos',
          0,
          100,
          this.fund,
          this.list_no,
          this.file_no
        );
        this.sourts = !(await sourts) ? [] : await sourts;
      } else {
        if (this.human_code.value && this.human_code.value.code) {
          let personal = await this.personaltService.getOneById(
            this.human_code.value.code,
            true,
            true
          );
          this.sourts = personal.sour_t ? personal.sour_t : [];
        }
      }
    } catch (err) {
      console.error(`Ошибка ${err}`);
    } finally {
      this.spinnerService.hide();
    }
  }

  getPubtype(id) {
    let p = this.globalService.pubTypes.find((item) => item.id === id);
    return p ? p.value : '';
  }

  private _filterPersonalt(name: string): PersonalT[] {
    const filterValue = name.toLowerCase();
    return !this.personalts
      ? null
      : this.personalts
          .filter((personalt) => personalt.surname && personalt.surname !== '')
          .filter(
            (personalt) =>
              personalt.surname.toLowerCase().indexOf(filterValue) !== -1
          );
  }

  inputLink_familyt_human_code() {
    clearTimeout(this.timerId);
    this.timerId = setTimeout(async () => {
      this.personalts = await this.personaltService.getWithShortfilterLimit(
        100,
        this.human_code.value
      );
      if (this.personalts) {
        try {
          this.filteredPersonalts = (<FormControl>(
            this.human_code
          )).valueChanges.pipe(
            startWith<string | PersonalT>(''),
            map((value) => (typeof value === 'string' ? value : value.surname)),
            map((name) =>
              name ? this._filterPersonalt(name) : [...this.personalts]
            )
          );
        } catch {}
      }
    }, 500);
  }

  displayFn_human_code(personal?: PersonalT): string | undefined {
    if (!personal) {
      return undefined;
    }
    let str = '';
    str += personal.code + '. ';
    str += personal.surname ? personal.surname + ' ' : '';
    str += personal.fname ? personal.fname + ' ' : '';
    str += personal.lname ? personal.lname + ' ' : '';
    return personal ? str : undefined;
  }

  isSelected(id): boolean {
    let p = this.selectedSourts.find((item) => item.id == id);
    return p ? true : false;
  }

  togglePersonal(sourt) {
    let idx = this.selectedSourts.findIndex((item) => item.id === sourt.id);
    if (idx === -1) {
      this.selectedSourts.push(sourt);
    } else {
      this.selectedSourts.splice(idx, 1);
    }
  }
}
