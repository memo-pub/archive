import {
  Component,
  Input,
  Output,
  EventEmitter,
  HostListener,
  ViewChild,
  ElementRef,
  Renderer2,
} from '@angular/core';
import {
  trigger,
  transition,
  state,
  style,
  animate,
} from '@angular/animations';

export interface LightboxEvent {
  caption: string;
  icon: string;
  name: string;
}
export interface LightboxImage {
  src: string;
  caption: string;
  type?: string;
  id?: number | string;
  ext?: string; // safari audio
  orientation: number;
}

@Component({
  selector: 'app-lightbox',
  templateUrl: './lightbox.component.html',
  styleUrls: ['./lightbox.component.scss'],
  animations: [
    trigger('fade', [
      state('void', style({ opacity: 0 })),
      transition('void <=> *', [animate(250)]),
    ]),
  ],
})
export class LightboxComponent {
  /**
   * Функция поворота
   */
  @Input() showRotate = true;
  /**
   * Массив изображений
   */
  @Input() images: LightboxImage[] = [];
  /**
   * Массив дополнительных событий в панели управления
   */
  @Input() customEvents: LightboxEvent[] = [];

  @Input() wait = false;

  /**
   * События перелистывания вперед
   */
  @Output('onNext') next = new EventEmitter<null>();
  /**
   * События перелистывания назад
   */
  @Output('onPrevious') previous = new EventEmitter<null>();
  /**
   * События закрытия lightbox
   */
  @Output('onClose') close = new EventEmitter<null>();
  /**
   * Событие срабатывание события из customEvents, передает name события и индекс текущего изображения
   */
  @Output('onCustomEvent') customEvent = new EventEmitter<{
    name: string;
    index: number;
  }>();
  /**
   * Событие перелистывания, передает индекс нового изображения, LightboxImage, направление перелистывания
   */
  @Output('onChange') change = new EventEmitter<{
    index: number;
    image: LightboxImage;
    forward: boolean;
  }>();
  /**
   * Событие изменения списка изображений (pushImg, popImg), возвращает новый список
   */
  @Output('onChangeImages') changeImages = new EventEmitter<LightboxImage[]>();
  @Output('onorientation') orientation = new EventEmitter<{
    id: number | string;
    val: number;
  }>();

  showLightbox = false;
  currentIndex = 0;
  showImg = true;
  scale = 1;

  drag = false;

  @ViewChild('imgBlock') imgBlock: ElementRef;

  @HostListener('document:keydown', ['$event']) onKeydownLeftHandler(event) {
    if (this.showLightbox) {
      if (event.key === 'Escape' || event.code === 'Escape') {
        this.onClose();
      } else if (event.key === 'l' || event.code === 'KeyL') {
        if (this.showRotate) {
          this.onorientation(true);
        }
      } else if (event.key === 'r' || event.code === 'KeyR') {
        if (this.showRotate) {
          this.onorientation();
        }
      } else if (event.key === 'ArrowRight' || event.code === 'ArrowRight') {
        if (!this.wait) {
          this.onNext();
        }
      } else if (event.key === 'ArrowLeft' || event.code === 'ArrowLeft') {
        if (!this.wait) {
          this.onPrevious();
        }
      }
    }
  }

  @HostListener('document:mousedown', ['$event']) onMouseDown(e: MouseEvent) {
    if (this.scale > 1) {
      this.drag = true;
    }
  }

  @HostListener('mouseup') onMouseUp() {
    this.drag = false;
  }

  @HostListener('document:dragstart', ['$event']) onDragstart(e: MouseEvent) {
    e.preventDefault();
    return false;
  }
  @HostListener('document:mousemove', ['$event']) onMouseMove(e: MouseEvent) {
    if (this.drag === true) {
      e.preventDefault();
      this.renderer.setStyle(
        this.imgBlock.nativeElement,
        'top',
        `${this.imgBlock.nativeElement.offsetTop + e.movementY}px`
      );
      this.renderer.setStyle(
        this.imgBlock.nativeElement,
        'left',
        `${this.imgBlock.nativeElement.offsetLeft + e.movementX}px`
      );
      return false;
    }
  }

  constructor(private renderer: Renderer2) {}

  resetTransform() {
    this.scale = 1;
    this.drag = false;
  }
  onNext() {
    if (this.currentIndex + 1 < this.images.length) {
      this.showImg = false;
      this.resetTransform();
      this.next.emit();
      this.change.emit({
        index: this.currentIndex++,
        image: this.images[this.currentIndex],
        forward: true,
      });
      setTimeout(() => {
        this.showImg = true;
      }, 100);
    }
  }
  onPrevious() {
    if (this.currentIndex > 0) {
      this.showImg = false;
      this.resetTransform();
      this.previous.emit();
      this.change.emit({
        index: this.currentIndex--,
        image: this.images[this.currentIndex],
        forward: false,
      });
      setTimeout(() => {
        this.showImg = true;
      }, 100);
    }
  }
  onClose() {
    this.showLightbox = false;
    this.close.emit();
  }
  onZoom(type: boolean) {
    if (type === true) {
      this.scale += 0.1;
    } else {
      this.scale = this.scale > 1 ? this.scale - 0.1 : this.scale;
    }
  }
  onCustomEvent(name) {
    this.customEvent.emit({ name, index: this.currentIndex });
  }
  /**
   * Открыть лайтбокс
   * @param {number} index
   */
  open(index = 0) {
    this.currentIndex = index;
    this.showLightbox = true;
    this.resetTransform();
  }
  /**
   * Добавить изображение в конец списка
   * @param {LightboxImage} img
   */
  pushImg(img: LightboxImage) {
    this.images.push(img);
    this.changeImages.emit(this.images);
    return { index: this.currentIndex, images: this.images };
  }
  /**
   * Добавить изображение в начало списка
   * @param {LightboxImage} img
   */
  unshiftImg(img: LightboxImage) {
    this.images.unshift(img);
    this.currentIndex++;
    this.changeImages.emit(this.images);
    return { index: this.currentIndex, images: this.images };
  }

  onorientation(rev = false) {
    if (!rev) {
      if (!this.images[this.currentIndex].orientation) {
        this.images[this.currentIndex].orientation = 1;
      } else if (this.images[this.currentIndex].orientation === 1) {
        this.images[this.currentIndex].orientation = 2;
      } else if (this.images[this.currentIndex].orientation === 2) {
        this.images[this.currentIndex].orientation = 3;
      } else if (this.images[this.currentIndex].orientation === 3) {
        this.images[this.currentIndex].orientation = 0;
      }
    }
    if (rev) {
      if (!this.images[this.currentIndex].orientation) {
        this.images[this.currentIndex].orientation = 3;
      } else if (this.images[this.currentIndex].orientation === 1) {
        this.images[this.currentIndex].orientation = 0;
      } else if (this.images[this.currentIndex].orientation === 2) {
        this.images[this.currentIndex].orientation = 1;
      } else if (this.images[this.currentIndex].orientation === 3) {
        this.images[this.currentIndex].orientation = 2;
      }
    }
    this.orientation.emit({
      id: this.images[this.currentIndex].id,
      val: this.images[this.currentIndex].orientation,
    });
  }

  // isOgg(): boolean {
  //   return this.images[this.currentIndex].caption
  // }
}
