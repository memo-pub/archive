/**Компонент для пагинатора */
import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent {
  final = false;

  @Input() page: number;
  @Input() count: number;
  @Input() perPage: number;
  @Input() loading: boolean;
  @Input() pagesToShow: number;

  @Output() goPrev = new EventEmitter<boolean>();
  @Output() goNext = new EventEmitter<boolean>();
  @Output() goPrev10 = new EventEmitter<number>();
  @Output() goNext10 = new EventEmitter<number>();
  @Output() goPage = new EventEmitter<number>();

  constructor() {}

  getMin(): number {
    return this.perPage * this.page - this.perPage + 1;
  }

  getMax(): number {
    let max = this.perPage * this.page;
    if (max > this.count) {
      max = this.count;
    }
    return max;
  }

  onPage(n: number): void {
    this.final = false;
    this.goPage.emit(n);
  }

  onPrev(): void {
    this.final = false;
    this.goPrev.emit(true);
  }

  onNext(next: boolean): void {
    this.final = false;
    this.goNext.emit(next);
  }

  onLast(next: boolean): void {
    this.final = true;
    this.goNext.emit(next);
  }

  onPrev10(x = 1): void {
    this.final = false;
    this.goPrev10.emit(x);
  }

  onNext10(x = 1): void {
    this.final = false;
    this.goNext10.emit(x);
  }

  totalPages(): number {
    return Math.ceil(this.count / this.perPage) || 0;
  }

  lastPage(): boolean {
    return this.perPage * this.page >= this.count;
  }

  getPages(): number[] {
    const pages: number[] = [];
    if (this.final === true) {
      for (
        let i = this.totalPages() - this.pagesToShow;
        i <= this.totalPages();
        i++
      ) {
        pages.push(i);
      }
    } else {
      const c = Math.ceil(this.count / this.perPage);
      const p = this.page || 1;
      const pagesToShow = this.pagesToShow || 9;
      pages.push(p);
      const times = pagesToShow - 1;
      for (let i = 0; i < times; i++) {
        if (pages.length < pagesToShow) {
          if (Math.min.apply(null, pages) > 1) {
            pages.push(Math.min.apply(null, pages) - 1);
          }
        }
        if (pages.length < pagesToShow) {
          if (Math.max.apply(null, pages) < c) {
            pages.push(Math.max.apply(null, pages) + 1);
          }
        }
      }
      pages.sort((a, b) => a - b);
    }
    return pages;
  }
}
