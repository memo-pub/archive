import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterFile',
})
export class FilterFilePipe implements PipeTransform {
  transform(value: any[], str: string): any[] {
    if (!str || !Array.isArray(value)) {
      return value;
    } else {
      let res = value.filter(
        (item) => item.name.toLowerCase().indexOf(str.toLowerCase()) !== -1
      );
      return res;
    }
  }
}
