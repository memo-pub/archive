import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../../../../shared/services/auth.service';
import { MuserService } from '../../services/muser.service';
import { Muser } from '../../../../shared/models/muser.model';
import { GlobalService } from 'src/app/shared/services/global.service';
import { FiltersService } from '../../services/filters.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css'],
})
export class SidebarComponent implements OnInit {
  loadedStr = '';
  pinPersonal: number;
  constructor(
    private globalService: GlobalService,
    private filtersService: FiltersService,
    private authService: AuthService,
    private muserService: MuserService,
    public router: Router
  ) {
    this.filtersService.pinPersonal.subscribe((code) => {
      this.pinPersonal = code;
      this.card.link = `${this.pinPersonal}`;
    });
    this.globalService.loadedStr.subscribe((str) => {
      this.loadedStr = str;
    });
  }

  // mainMenu = [];
  // extraMenu = [];

  menuArchives = [];
  menuDocuments = [];
  menuPersonals = [];
  menuAdmin = [];
  menuArchivesOpened = true;
  menuDocumentsOpened = true;
  menuPersonalsOpened = true;
  menuAdminOpened = true;
  loggedInUser: Muser;

  card = {
    modulLink: '/sys/person/personaltedit',
    link: null,
    text: 'Карточка',
    icon: 'fa-id-card-o',
  };

  async ngOnInit() {
    try {
      let loggedInUser = this.muserService.getOneById(
        this.authService.getCurUser().id
      );
      this.loggedInUser = await loggedInUser;
      if (this.loggedInUser.isadmin) {
        this.menuAdmin = [
          {
            modulLink: '/sys/users',
            link: 'report',
            text: 'Отчеты',
            icon: 'fa-table',
            avialibleLinks: ['/sys/users/report'],
          },
          {
            modulLink: '/sys/users',
            link: 'config',
            text: 'Настройки',
            icon: 'fa-cog',
            avialibleLinks: ['/sys/users/config'],
          },
          {
            modulLink: '/sys/users',
            link: 'list',
            text: 'Пользователи',
            icon: 'fa-user-circle-o',
            avialibleLinks: ['/sys/users/list', '/sys/users/edit'],
          },
          {
            modulLink: '/sys/person',
            link: 'stat',
            text: 'Статистика',
            icon: 'fa-file-text-o',
            avialibleLinks: ['/sys/person/stat', '/sys/person/statedit'],
          },
        ];
        this.menuArchives = [
          {
            modulLink: '/sys/arch',
            link: 'list',
            text: 'Дела',
            icon: 'fa-file-text-o',
            avialibleLinks: ['/sys/arch/list', '/sys/arch/edit'],
          },
          // {
          //   modulLink: '/sys/arch',
          //   link: 'o-list',
          //   text: 'Опись',
          //   icon: 'fa-list',
          //   avialibleLinks: ['/sys/arch/o-list', '/sys/arch/o-edit'],
          // },
          {
            modulLink: '/sys/arch',
            link: 'f-list',
            text: 'Фонды и описи',
            icon: 'fa-list',
            avialibleLinks: ['/sys/arch/f-list', '/sys/arch/f-edit'],
          },
          {
            modulLink: '/sys/arch',
            link: 'files',
            text: 'Файлы',
            icon: 'fa-file-image-o',
            avialibleLinks: ['/sys/arch/files/list'],
          },
        ];
        this.menuDocuments = [
          {
            modulLink: '/sys/person',
            link: 'doclist',
            text: 'Документы',
            icon: 'fa-file',
            avialibleLinks: ['/sys/person/doclist', '/sys/person/doc'],
          },
        ];
        this.menuPersonals = [
          {
            modulLink: '/sys/person',
            link: 'personaltlist',
            text: 'Персоналии',
            icon: 'fa-id-card-o',
            avialibleLinks: [
              '/sys/person/personaltlist',
              '/sys/person/personaltedit',
              '/sys/person/activitytedit',
              '/sys/person/represstedit',
              '/sys/person/courttedit',
              '/sys/person/impristedit',
              '/sys/person/orgtedit',
              '/sys/person/sourtedit',
              '/sys/person/helptedit',
            ],
          },
          {
            modulLink: '/sys/person',
            link: 'nationlist',
            text: 'Национальности',
            icon: 'fa-users',
            avialibleLinks: [
              '/sys/person/nationlist',
              '/sys/person/nationedit',
            ],
          },
          {
            modulLink: '/sys/person',
            link: 'listoftlist',
            text: 'Словари',
            icon: 'fa-book',
            avialibleLinks: [
              '/sys/person/listoftlist',
              '/sys/person/listoftedit',
            ],
          },
          {
            modulLink: '/sys/person',
            link: 'placetlist',
            text: 'Объекты',
            icon: 'fa-globe',
            avialibleLinks: [
              '/sys/person/placetlist',
              '/sys/person/placetedit',
            ],
          },
          {
            modulLink: '/sys/person',
            link: 'search',
            text: 'Поиск по полям',
            icon: 'fa-search',
            avialibleLinks: ['/sys/person/search'],
          },
          {
            modulLink: '/sys/person',
            link: 'selectionlist',
            text: 'Выборки',
            icon: 'fa-filter',
            avialibleLinks: [
              '/sys/person/selectionlist',
              '/sys/person/selectionedit',
            ],
          },
          {
            modulLink: '/sys/person',
            link: 'number',
            text: 'Нумерация',
            icon: 'fa-sort-numeric-asc',
            avialibleLinks: ['sys/person/number'],
          },
        ];
      } else if (this.loggedInUser.usertype === 1) {
        this.menuArchives = [
          {
            modulLink: '/sys/arch',
            link: 'list',
            text: 'Дела',
            icon: 'fa-file-text-o',
            avialibleLinks: ['/sys/arch/list', '/sys/arch/edit'],
          },
          // {
          //   modulLink: '/sys/arch',
          //   link: 'o-list',
          //   text: 'Опись',
          //   icon: 'fa-list',
          //   avialibleLinks: ['/sys/arch/o-list', '/sys/arch/o-edit'],
          // },
          {
            modulLink: '/sys/arch',
            link: 'f-list',
            text: 'Фонды и описи',
            icon: 'fa-list',
            avialibleLinks: ['/sys/arch/f-list', '/sys/arch/f-edit'],
          },
          {
            modulLink: '/sys/arch',
            link: 'files',
            text: 'Файлы',
            icon: 'fa-file-image-o',
            avialibleLinks: ['/sys/arch/files/list'],
          },
        ];
      } else if (this.loggedInUser.usertype === 2) {
        this.menuArchives = [
          {
            modulLink: '/sys/arch',
            link: 'list',
            text: 'Дела',
            icon: 'fa-file-text-o',
            avialibleLinks: ['/sys/arch/list', '/sys/arch/edit'],
          },
          {
            modulLink: '/sys/arch',
            link: 'f-list',
            text: 'Фонд',
            icon: 'fa-list',
            avialibleLinks: ['/sys/arch/f-list', '/sys/arch/f-edit'],
          },
          {
            modulLink: '/sys/arch',
            link: 'files',
            text: 'Файлы',
            icon: 'fa-file-image-o',
            avialibleLinks: ['/sys/arch/files/list'],
          },
        ];
      } else if (this.loggedInUser.usertype === 3) {
        this.menuArchives = [
          {
            modulLink: '/sys/arch',
            link: 'list',
            text: 'Дела',
            icon: 'fa-file-text-o',
            avialibleLinks: ['/sys/arch/list', '/sys/arch/edit'],
          },
          {
            modulLink: '/sys/arch',
            link: 'f-list',
            text: 'Фонд',
            icon: 'fa-list',
            avialibleLinks: ['/sys/arch/f-list', '/sys/arch/f-edit'],
          },
        ];
        this.menuDocuments = [
          {
            modulLink: '/sys/person',
            link: 'doclist',
            text: 'Документы',
            icon: 'fa-file',
            avialibleLinks: ['/sys/person/doclist', '/sys/person/doc'],
          },
        ];
        this.menuPersonals = [
          {
            modulLink: '/sys/person',
            link: 'personaltlist',
            text: 'Персоналии',
            icon: 'fa-id-card-o',
            avialibleLinks: [
              '/sys/person/personaltlist',
              '/sys/person/personaltedit',
              '/sys/person/activitytedit',
              '/sys/person/represstedit',
              '/sys/person/courttedit',
              '/sys/person/impristedit',
              '/sys/person/orgtedit',
              '/sys/person/sourtedit',
              '/sys/person/helptedit',
            ],
          },
          {
            modulLink: '/sys/person',
            link: 'nationlist',
            text: 'Национальности',
            icon: 'fa-users',
            avialibleLinks: [
              '/sys/person/nationlist',
              '/sys/person/nationedit',
            ],
          },
          {
            modulLink: '/sys/person',
            link: 'listoftlist',
            text: 'Словари',
            icon: 'fa-book',
            avialibleLinks: [
              '/sys/person/listoftlist',
              '/sys/person/listoftedit',
            ],
          },
          {
            modulLink: '/sys/person',
            link: 'placetlist',
            text: 'Объекты',
            icon: 'fa-globe',
            avialibleLinks: [
              '/sys/person/placetlist',
              '/sys/person/placetedit',
            ],
          },
          {
            modulLink: '/sys/person',
            link: 'search',
            text: 'Поиск по полям',
            icon: 'fa-search',
            avialibleLinks: ['/sys/person/search'],
          },
          {
            modulLink: '/sys/person',
            link: 'selectionlist',
            text: 'Выборки',
            icon: 'fa-filter',
            avialibleLinks: [
              '/sys/person/selectionlist',
              '/sys/person/selectionedit',
            ],
          },
        ];
      } else if (this.loggedInUser.usertype === 4) {
        this.menuArchives = [
          {
            modulLink: '/sys/arch',
            link: 'list',
            text: 'Дела',
            icon: 'fa-file-text-o',
            avialibleLinks: ['/sys/arch/list', '/sys/arch/edit'],
          }
        ];
        this.menuPersonals = [
          {
            modulLink: '/sys/person',
            link: 'personaltlist',
            text: 'Персоналии',
            icon: 'fa-id-card-o',
            avialibleLinks: [
              '/sys/person/personaltlist',
              '/sys/person/personaltedit',
              '/sys/person/activitytedit',
              '/sys/person/represstedit',
              '/sys/person/courttedit',
              '/sys/person/impristedit',
              '/sys/person/orgtedit',
              '/sys/person/sourtedit',
              '/sys/person/helptedit',
            ],
          },
          {
            modulLink: '/sys/person',
            link: 'search',
            text: 'Поиск по полям',
            icon: 'fa-search',
            avialibleLinks: ['/sys/person/search'],
          },
          {
            modulLink: '/sys/person',
            link: 'selectionlist',
            text: 'Выборки',
            icon: 'fa-filter',
            avialibleLinks: [
              '/sys/person/selectionlist',
              '/sys/person/selectionedit',
            ],
          },
        ];
        // this.menuPersonals.unshift({
        //   modulLink: '/sys/person',
        //   link: `personaltedit/${this.pinPersonal}`,
        //   text: 'Карточка',
        //   icon: 'fa-id-card-o',
        // });
        // this.extraMenu = [
        //   {
        //     modulLink: '/sys/person',
        //     link: 'personaltlist',
        //     text: 'Персоналии',
        //     icon: 'fa-id-card-o',
        //   },
        //   {
        //     modulLink: '/sys/person',
        //     link: 'search',
        //     text: 'Поиск по полям',
        //     icon: 'fa-search',
        //   },
        //   {
        //     modulLink: '/sys/person',
        //     link: 'selectionlist',
        //     text: 'Выборки',
        //     icon: 'fa-filter',
        //   },
        // ];
      }
    } catch (err) {
      if (err.status === 401) {
        console.error(`Ошибка авторизации`);
        this.authService.logout();
      } else {
        console.error(`Ошибка ${err}`);
      }
    }
  }

  isActive(avialibleLinks) {
    let res = false;
    avialibleLinks.forEach((link) => {
      res = res || this.router.url.indexOf(link) !== -1;
    });
    return res;
  }

  // onLink(str1, str2) {
  //   let str = str1 + '/'+str2
  //   console.log(str);
  //   this.router.navigate([str]);
  // }
}
