import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NgxSpinnerService } from 'ngx-spinner';
import { ArchivefolderfileService } from '../../services/archivefolderfile.service';

@Component({
  selector: 'app-dialog-pdf-viewer',
  templateUrl: './dialog-pdf-viewer.component.html',
  styleUrls: ['./dialog-pdf-viewer.component.css'],
})
export class DialogPdfViewerComponent implements OnInit {
  src: string;
  constructor(
    public dialogRef: MatDialogRef<DialogPdfViewerComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private archivefolderfileService: ArchivefolderfileService,
    private spinnerService: NgxSpinnerService
  ) {}

  ngOnInit(): void {
    this.spinnerService.show();

    let key;
    if (this.data.file?.fondfile) {
      key = 'fondfile';
    }
    if (this.data.file?.archivefile) {
      key = 'archivefile';
    }
    if (this.data.file?.opisfile) {
      key = 'opisfile';
    }
    if (this.data.file) {
      this.src = this.archivefolderfileService.generateLink(
        this.data.file.id || this.data.file.file_id,
        this.data.file.jwt,
        this.data.file[key].storage ||
          this.data.file[key].storage ||
          this.data.file[key].storage
      );
    }

    this.spinnerService.hide();
  }
}
