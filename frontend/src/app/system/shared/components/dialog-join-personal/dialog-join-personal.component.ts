import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { NgxSpinnerService } from 'ngx-spinner';
import { PersonalT } from 'src/app/shared/models/personalt.model';
import { DateparseService } from 'src/app/shared/services/dateparse.service';
import { DialogPersonalEditComponent } from 'src/app/system/personal/personal-edit/dialog-personal-edit/dialog-personal-edit.component';
import { FiltersService } from '../../services/filters.service';
import { PersonaltService } from '../../services/personalt.service';

@Component({
  selector: 'app-dialog-join-personal',
  templateUrl: './dialog-join-personal.component.html',
  styleUrls: ['./dialog-join-personal.component.css'],
})
export class DialogJoinPersonalComponent implements OnInit {
  selectedPersonals: PersonalT[] = [];
  start = 0;
  limit = 200;
  timer;

  personals: PersonalT[];

  constructor(
    public dialogRef: MatDialogRef<DialogJoinPersonalComponent>,
    public filtersService: FiltersService,
    private dialog: MatDialog,
    private personaltService: PersonaltService,
    public dateparseService: DateparseService,
    private spinnerService: NgxSpinnerService
  ) {}

  ngOnInit(): void {
    this.updateData();
  }

  async updateData() {
    clearTimeout(this.timer);
    this.timer = setTimeout(async () => {
      this.spinnerService.show();
      let start = 0;
      if (
        this.filtersService.personalTDialogFilter.searchStr.length +
          this.filtersService.personalTDialogFilter.searchStr_fond.length +
          this.filtersService.personalTDialogFilter.searchStr_opis.length +
          this.filtersService.personalTDialogFilter.searchStr_delo.length >
        0
      ) {
        this.spinnerService.show();
        this.start = 0;

        try {
          let personals =
            this.personaltService.getWithShortfilterOpisFondDeloOffsetLimitOrder(
              this.filtersService.personalTDialogFilter.searchStr,
              start,
              this.limit,
              this.filtersService.personalTDialogFilter.searchOrder,
              this.filtersService.personalTDialogFilter.searchStr_fond,
              this.filtersService.personalTDialogFilter.searchStr_opis,
              this.filtersService.personalTDialogFilter.searchStr_delo,
              this.filtersService.personalTDialogFilter.searchStr_delo_ext,
              this.filtersService.personalTDialogFilter.filterExact,
              this.filtersService.personalTDialogFilter.filterExactExt
            );
          this.personals = !(await personals) ? [] : await personals;
        } catch (err) {
          console.error(err);
        } finally {
          this.spinnerService.hide();
        }
      } else {
        // try {
        //   let personals = this.personaltService.getWithOffsetLimitOrder(
        //     this.start,
        //     this.limit,
        //     this.filtersService.personalTDialogFilter.searchOrder
        //   );
        //   this.personals = !(await personals) ? [] : await personals;
        // } catch (err) {
        //   console.error(err);
        // } finally {
          this.spinnerService.hide();
        // }
      }
    }, 1000);
  }

  isSelected(code): boolean {
    let p = this.selectedPersonals.find((item) => item.code == code);
    return p ? true : false;
  }

  togglePersonal(personal) {
    let idx = this.selectedPersonals.findIndex(
      (item) => item.code === personal.code
    );
    if (idx === -1) {
      this.selectedPersonals.push(personal);
    } else {
      this.selectedPersonals.splice(idx, 1);
    }
  }

  onAddPersonalT() {
    let dialogRef = this.dialog.open(DialogPersonalEditComponent, {
      panelClass: 'app-dialog-extend',
    });
    dialogRef.afterClosed().subscribe(async (result) => {
      if (result) {
        try {
          let personal = await this.personaltService.postOne(result);
          this.selectedPersonals.push(personal);
          this.dialogRef.close(this.selectedPersonals);
        } catch (err) {}
      }
    });
  }
}
