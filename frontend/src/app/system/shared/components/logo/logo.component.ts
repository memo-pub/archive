import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-logo',
  templateUrl: './logo.component.html',
  styleUrls: ['./logo.component.css'],
})
export class LogoComponent implements OnInit {
  page = '';
  pageNames = {
    '/sys/users/list': 'Пользователи',
    '/sys/users/config': 'Настройки',
    '/sys/users/report': 'Отчеты',
    '/sys/users/edit/myprofile': 'Профиль',
    '/sys/users/edit/newprofile': 'Добавление пользователя',
    '/sys/users/edit/number': 'Редактирование пользователя',
    '/sys/arch/list': 'Дела',
    '/sys/arch/f-list': 'Фонды и описи',
    '/sys/arch/f-edit/number': 'Редактирование фонда',
    '/sys/arch/edit/newdoc': 'Добавление документа',
    '/sys/arch/o-edit/newdoc': 'Добавление описи',
    '/sys/arch/edit/number': 'Редактирование дела',
    '/sys/arch/o-edit/number': 'Редактирование описи',
    '/sys/person/personaltlist': 'Персоналии',
    '/sys/person/personaltedit/newdoc': 'Добавление персоналии',
    '/sys/person/personaltedit/number': 'Редактирование персоналии',
    '/sys/person/personaltedit/view': 'Просмотр персоналии',
    '/sys/person/nationlist': 'Национальности',
    '/sys/person/nationedit/newdoc': 'Добавление национальности',
    '/sys/person/nationedit/number': 'Редактирование национальности',
    '/sys/person/listoftlist': 'Словари',
    '/sys/person/listoftedit/newdoc': 'Добавление словаря',
    '/sys/person/listoftedit/number': 'Редактирование словаря',
    '/sys/person/placetlist': 'Объекты',
    '/sys/person/placetedit/newdoc': 'Добавление объекта',
    '/sys/person/placetedit/number': 'Редактирование объекта',
    '/sys/person/stat': 'Статистика',
    '/sys/person/stat/number': 'Статистика',
    '/sys/person/statedit/number': 'Статистика',
    '/sys/person/selectionlist': 'Выборки',
    '/sys/arch/files/list': 'Файлы',
    '/sys/arch/files/edit/number': 'Редактор файлов',
    '/sys/person/doclist': 'Документы',
    '/sys/person/doc': 'Документ',
  };

  constructor(public router: Router) {
    this.page = this.router.url;
  }

  ngOnInit() {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.page = this.router.url;
        if (this.page.search(/\/sys\/person\/doc/) !== -1) {
          this.page = '/sys/person/doc';
        }
        if (this.page.search(/\/sys\/users\/edit\/\d+/) !== -1) {
          this.page = '/sys/users/edit/number';
        }
        if (this.page.search(/\/sys\/arch\/f-edit\/\d+/) !== -1) {
          this.page = '/sys/arch/f-edit/number';
        }
        if (this.page.search(/\/sys\/arch\/edit\/\d+/) !== -1) {
          this.page = '/sys/arch/edit/number';
        }
        if (this.page.search(/\/sys\/arch\/o-edit\/\d+/) !== -1) {
          this.page = '/sys/arch/o-edit/number';
        }
        if (this.page.search(/\/sys\/person\/personaltedit\/\d+/) !== -1) {
          this.page = '/sys/person/personaltedit/number';
        }
        if (this.page.search(/v_\d+/) !== -1) {
          this.page = '/sys/person/personaltedit/view';
        }
        if (this.page.search(/\/sys\/person\/nationedit\/\d+/) !== -1) {
          this.page = '/sys/person/nationedit/number';
        }
        if (this.page.search(/\/sys\/person\/listoftedit\/\d+/) !== -1) {
          this.page = '/sys/person/listoftedit/number';
        }
        if (this.page.search(/\/sys\/person\/listoftedit\/newdoc/) !== -1) {
          this.page = '/sys/person/listoftedit/newdoc';
        }
        if (this.page.search(/\/sys\/person\/placetedit\/\d+/) !== -1) {
          this.page = '/sys/person/placetedit/number';
        }
        if (this.page.search(/\/sys\/person\/orgtedit\/edit/) !== -1) {
          this.page = '/sys/person/personaltedit/number';
        }
        if (this.page.search(/\/sys\/person\/orgtedit\/new/) !== -1) {
          this.page = '/sys/person/personaltedit/number';
        }
        if (this.page.search(/\/sys\/person\/activitytedit\/edit/) !== -1) {
          this.page = '/sys/person/personaltedit/number';
        }
        if (this.page.search(/\/sys\/person\/activitytedit\/new/) !== -1) {
          this.page = '/sys/person/personaltedit/number';
        }
        if (this.page.search(/\/sys\/person\/represstedit\/new/) !== -1) {
          this.page = '/sys/person/personaltedit/number';
        }
        if (this.page.search(/\/sys\/person\/represstedit\/edit/) !== -1) {
          this.page = '/sys/person/personaltedit/number';
        }
        if (this.page.search(/\/sys\/person\/courttedit\/new/) !== -1) {
          this.page = '/sys/person/personaltedit/number';
        }
        if (this.page.search(/\/sys\/person\/courttedit\/edit/) !== -1) {
          this.page = '/sys/person/personaltedit/number';
        }
        if (this.page.search(/\/sys\/person\/impristedit\/new/) !== -1) {
          this.page = '/sys/person/personaltedit/number';
        }
        if (this.page.search(/\/sys\/person\/impristedit\/edit/) !== -1) {
          this.page = '/sys/person/personaltedit/number';
        }
        if (this.page.search(/\/sys\/arch\/files\/edit/) !== -1) {
          this.page = '/sys/arch/files/edit/number';
        }
        if (this.page.search(/\/sys\/person\/stat\/\d+/) !== -1) {
          this.page = '/sys/person/stat/number';
        }
        if (this.page.search(/\/sys\/person\/statedit\/\d+/) !== -1) {
          this.page = '/sys/person/statedit/number';
        }
      }
    });
  }
}
