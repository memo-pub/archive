import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NgxSpinnerService } from 'ngx-spinner';
import { SourT } from 'src/app/shared/models/sourt.model';
import { DateparseService } from 'src/app/shared/services/dateparse.service';
import { SourtService } from '../../services/sourt.service';

@Component({
  selector: 'app-dialog-set-document-parent',
  templateUrl: './dialog-set-document-parent.component.html',
  styleUrls: ['./dialog-set-document-parent.component.css'],
})
export class DialogSetDocumentParentComponent implements OnInit {
  sourt_name: string;
  fund: string;
  list_no: string;
  file_no: string;
  file_no_ext?: string;
  file_no_ext_noneed?: boolean;
  timer;
  sourts: SourT[] = [];
  constructor(
    private spinnerService: NgxSpinnerService,
    private sourtService: SourtService,
    public dateparseService: DateparseService,
    public dialogRef: MatDialogRef<DialogSetDocumentParentComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit(): void {
    if (this.data.selectedSourt) {
      this.sourts.push(this.data.selectedSourt);
    }
  }

  async getData(spinner = true) {
    if (
      this.sourt_name ||
      this.fund ||
      this.list_no ||
      this.file_no ||
      this.file_no_ext
    ) {
      clearTimeout(this.timer);
      this.timer = setTimeout(async () => {
        this.spinnerService.show();
        try {
          this.sourts = await this.sourtService.getShortFilter(
            100,
            this.sourt_name,
            this.fund,
            this.list_no,
            this.file_no,
            this.file_no_ext,
            this.file_no_ext_noneed
          );
        } catch (err) {
          console.error(err);
          this.sourts.length = 0;
        } finally {
          this.spinnerService.hide();
        }
      }, 800);
    }
  }
}
