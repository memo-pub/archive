import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { sourT_width } from 'src/app/shared/models/sourt.model';
import { ArchivefolderfileService } from '../../services/archivefolderfile.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { isNullOrUndefined } from 'util';
import { GlobalService } from 'src/app/shared/services/global.service';
import {
  LightboxImage,
  LightboxComponent,
} from '../lightbox/lightbox.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { saveAs as importedSaveAs } from 'file-saver';
import { DialogPdfViewerComponent } from '../dialog-pdf-viewer/dialog-pdf-viewer.component';

@Component({
  selector: 'app-dialog-select-files',
  templateUrl: './dialog-select-files.component.html',
  styleUrls: ['./dialog-select-files.component.css'],
})
export class DialogSelectFilesComponent implements OnInit {
  inProgress = false;
  inProgressVal = 0;
  loaded = 0;

  /**Галерея */
  imgBufsize = 3; // /2 буфер изображений в галерее
  lightboxImages: LightboxImage[] = [];
  @ViewChild('lightbox', { static: true }) lightbox: LightboxComponent;
  galleryWait = false;
  sourT_width = sourT_width;
  files = [];
  folder_files_img = [];
  filter_filename;

  timer;

  constructor(
    private dialog: MatDialog,
    public dialogRef: MatDialogRef<DialogSelectFilesComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private archivefolderfileService: ArchivefolderfileService,
    public globalService: GlobalService,
    public snackBar: MatSnackBar,
    private spinnerService: NgxSpinnerService
  ) {}

  ngOnInit() {
    this.getData();
  }

  isValid() {
    let res =
      this.data.fund &&
      this.data.fund !== '' &&
      this.data.list_no &&
      this.data.list_no !== '' &&
      this.data.file_no &&
      this.data.file_no !== '';
    return res;
  }

  async getData(spinner = true) {
    if (this.isValid()) {
      clearTimeout(this.timer);
      this.timer = setTimeout(async () => {
        this.files.length = 0;
        this.lightboxImages.length = 0;
        this.spinnerService.show();
        try {
          let _files = this.archivefolderfileService.getByFilter(
            this.data.fund,
            this.data.list_no,
            this.data.file_no,
            this.data.file_no_ext,
            this.data.file_no_ext_noneed,
            this.data.id,
            this.filter_filename
          );
          let files = isNullOrUndefined(await _files) ? [] : await _files;
          files.forEach((element) => {
            if (
              this.data.folder_files.findIndex(
                (item) => item.file_id === element.id
              ) !== -1
            ) {
              element.blocked = true;
            }
            this.files.push(element);
            // if (
            //   this.data.folder_files.findIndex(
            //     (item) => item.id === element.id
            //   ) === -1
            // ) {
            //   this.files.push(element);
            // }
          });
          this.folder_files_img = this.files.filter(
            (selectedArchivefolderfile) =>
              this.globalService.isImgtype(
                selectedArchivefolderfile.archivefile.filename
              ) ||
              this.globalService.isVideotype(
                selectedArchivefolderfile.archivefile.filename
              ) ||
              this.globalService.isAudiotype(
                selectedArchivefolderfile.archivefile.filename
              )
          );
          // this.folder_files_img.sort((a, b) =>
          //   a.archivefile.filename > b.archivefile.filename ? 1 : -1
          // );
        } catch (err) {
          console.error(err);
          this.files = [];
        } finally {
          this.spinnerService.hide();
        }
      }, 300);
    }
  }

  toggleSelection() {
    this.data.selectedFiles.length = 0;
    this.files.forEach((file) => {
      if (file.checked === true) {
        this.data.selectedFiles.push(file);
      }
    });
  }

  openPdf(selectedArchivefolderfile) {
    this.dialog.open(DialogPdfViewerComponent, {
      panelClass: 'app-dialog-extend',
      data: {
        file: selectedArchivefolderfile,
      },
    });
  }

  async onShowImg(id, selectedArchivefolderfile) {
    if (
      this.globalService.isPdftype(
        selectedArchivefolderfile.archivefile.filename
      )
    ) {
      this.openPdf(selectedArchivefolderfile);
      return;
    }
    let archivefolderfiles: any[];
    let parchivefolderfiles: any[];
    let narchivefolderfiles: any[];
    this.lightboxImages.length = 0;
    let idx = this.folder_files_img.findIndex((item) => item.id === id);
    parchivefolderfiles = this.folder_files_img.slice(
      idx,
      this.imgBufsize + idx > this.folder_files_img.length
        ? this.folder_files_img.length
        : this.imgBufsize + idx
    );
    narchivefolderfiles = this.folder_files_img.slice(
      idx - this.imgBufsize > 0 ? idx - this.imgBufsize : 0,
      idx
    );
    archivefolderfiles = [...narchivefolderfiles, ...parchivefolderfiles];
    archivefolderfiles.sort((a, b) =>
      a.archivefile.filename > b.archivefile.filename ? 1 : -1
    );
    let fname: string;
    this.folder_files_img.map((item) => {
      if (item.id === id) {
        fname = item.archivefile.filename;
      }
    });
    this.spinnerService.show();
    this.inProgress = true;
    this.inProgressVal += 100 / archivefolderfiles.length;
    await Promise.all(
      archivefolderfiles.map((item) => {
        return new Promise(async (resolve) => {
          let resfile;
          if (
            this.globalService.isImgtype(item.archivefile.filename) === true ||
            this.globalService.isVideotype(item.archivefile.filename) ===
              true ||
            this.globalService.isAudiotype(item.archivefile.filename) === true
          ) {
            try {
              let file;
              let type;
              let ext;
              if (
                this.globalService.isImgtype(item.archivefile.filename) === true
              ) {
                type = 'image';
                file = await this.archivefolderfileService.getFilePreviewById(
                  item.id
                );
                resfile = new Blob([file], { type: 'image/jpg' });
                let reader = new FileReader();
                reader.readAsDataURL(resfile);
                reader.onloadend = () => {
                  let base64data = String(reader.result);
                  this.lightboxImages.push({
                    src: base64data,
                    caption: item.archivefile.filename,
                    id: item.id,
                    type,
                    ext,
                    orientation: item.orientation,
                  });
                  resolve(null);
                };
              } else if (
                this.globalService.isVideotype(item.archivefile.filename) ===
                true
              ) {
                type = 'video';
                ext = item.archivefile.filename
                  .toLowerCase()
                  .split('.')
                  .reverse()[0];
                this.lightboxImages.push({
                  src: this.archivefolderfileService.generateLink(
                    item.id,
                    item.jwt,
                    item.archivefile.storage
                  ),
                  caption: item.archivefile.filename,
                  id: item.id,
                  type,
                  ext,
                  orientation: item.orientation,
                });
                resolve(null);
              } else {
                type = 'audio';
                ext = item.archivefile.filename
                  .toLowerCase()
                  .split('.')
                  .reverse()[0];
                this.lightboxImages.push({
                  src: this.archivefolderfileService.generateLink(
                    item.id,
                    item.jwt,
                    item.archivefile.storage
                  ),
                  caption: item.archivefile.filename,
                  id: item.id,
                  type,
                  ext,
                  orientation: item.orientation,
                });
                resolve(null);
              }
              this.inProgressVal += 100 / archivefolderfiles.length;
            } catch (err) {
              resolve(null);
            }
          } else {
            resolve(null);
          }
        });
      })
    );
    this.spinnerService.hide();
    this.lightboxImages.sort((a, b) => (a.caption > b.caption ? 1 : -1));
    this.inProgress = false;
    setTimeout(() => {
      let idx = this.lightboxImages.findIndex((item) => item.caption === fname);
      this.lightbox.open(idx);
    }, 300);
  }

  async addPrevImg() {
    let firstId = this.lightboxImages[0].id;
    let idx = this.folder_files_img.findIndex((item) => item.id === +firstId);
    if (idx === 0 || idx === -1) {
      return false;
    } else {
      this.spinnerService.show();
      this.galleryWait = true;
      let file;
      let type;
      let resfile;
      try {
        if (
          this.globalService.isImgtype(
            this.folder_files_img[idx - 1].archivefile.filename
          ) === true
        ) {
          type = 'image';
          file = await this.archivefolderfileService.getFilePreviewById(
            this.folder_files_img[idx - 1].id
          );
          resfile = new Blob([file], { type: 'image/jpg' });
          let reader = new FileReader();
          reader.readAsDataURL(resfile);
          reader.onloadend = () => {
            let base64data = String(reader.result);
            this.lightbox.unshiftImg({
              src: base64data,
              caption: this.folder_files_img[idx - 1].archivefile.filename,
              id: this.folder_files_img[idx - 1].id,
              type,
              orientation: this.folder_files_img[idx - 1].orientation,
            });
          };
        } else if (
          this.globalService.isVideotype(
            this.folder_files_img[idx - 1].archivefile.filename
          ) === true
        ) {
          type = 'video';
          let ext = this.folder_files_img[idx - 1].archivefile.filename
            .toLowerCase()
            .split('.')
            .reverse()[0];
          this.lightbox.unshiftImg({
            src: this.archivefolderfileService.generateLink(
              this.folder_files_img[idx - 1].id,
              this.folder_files_img[idx - 1].jwt,
              this.folder_files_img[idx - 1].archivefile.storage
            ),
            caption: this.folder_files_img[idx - 1].archivefile.filename,
            id: this.folder_files_img[idx - 1].id,
            type,
            ext,
            orientation: this.folder_files_img[idx - 1].orientation,
          });
        } else {
          type = 'audio';
          let ext = this.folder_files_img[idx - 1].archivefile.filename
            .toLowerCase()
            .split('.')
            .reverse()[0];
          this.lightbox.unshiftImg({
            src: this.archivefolderfileService.generateLink(
              this.folder_files_img[idx - 1].id,
              this.folder_files_img[idx - 1].jwt,
              this.folder_files_img[idx - 1].archivefile.storage
            ),
            caption: this.folder_files_img[idx - 1].archivefile.filename,
            id: this.folder_files_img[idx - 1].id,
            type,
            ext,
            orientation: this.folder_files_img[idx - 1].orientation,
          });
        }
      } catch {
        this.openSnackBar(
          `Ошибка при обработке изображения ${
            this.folder_files_img[idx - 1].archivefile.filename
          }`,
          'Ok'
        );
      } finally {
        this.spinnerService.hide();
        this.galleryWait = false;
      }
      return true;
    }
  }

  async addNextImg() {
    let lastId = +this.lightboxImages[this.lightboxImages.length - 1].id;
    let idx = this.folder_files_img.findIndex((item) => item.id === +lastId);
    if (idx + 1 > this.folder_files_img.length - 1 || idx === -1) {
      return false;
    } else {
      this.spinnerService.show();
      this.galleryWait = true;
      let file;
      let type;
      let resfile;
      try {
        if (
          this.globalService.isImgtype(
            this.folder_files_img[idx + 1].archivefile.filename
          ) === true
        ) {
          type = 'image';
          file = await this.archivefolderfileService.getFilePreviewById(
            this.folder_files_img[idx + 1].id
          );
          resfile = new Blob([file], { type: 'image/jpg' });
          let reader = new FileReader();
          reader.readAsDataURL(resfile);
          reader.onloadend = () => {
            let base64data = String(reader.result);
            this.lightbox.pushImg({
              src: base64data,
              caption: this.folder_files_img[idx + 1].archivefile.filename,
              id: this.folder_files_img[idx + 1].id,
              type,
              orientation: this.folder_files_img[idx + 1].orientation,
            });
          };
        } else if (
          this.globalService.isVideotype(
            this.folder_files_img[idx + 1].archivefile.filename
          ) === true
        ) {
          type = 'video';
          let ext = this.folder_files_img[idx + 1].archivefile.filename
            .toLowerCase()
            .split('.')
            .reverse()[0];
          this.lightbox.pushImg({
            src: this.archivefolderfileService.generateLink(
              this.folder_files_img[idx + 1].id,
              this.folder_files_img[idx + 1].jwt,
              this.folder_files_img[idx + 1].archivefile.storage
            ),
            caption: this.folder_files_img[idx + 1].archivefile.filename,
            id: this.folder_files_img[idx + 1].id,
            type,
            ext,
            orientation: this.folder_files_img[idx + 1].orientation,
          });
        } else {
          type = 'audio';
          let ext = this.folder_files_img[idx + 1].archivefile.filename
            .toLowerCase()
            .split('.')
            .reverse()[0];
          this.lightbox.pushImg({
            src: this.archivefolderfileService.generateLink(
              this.folder_files_img[idx + 1].id,
              this.folder_files_img[idx + 1].jwt,
              this.folder_files_img[idx + 1].archivefile.storage
            ),
            caption: this.folder_files_img[idx + 1].archivefile.filename,
            id: this.folder_files_img[idx + 1].id,
            type,
            ext,
            orientation: this.folder_files_img[idx + 1].orientation,
          });
        }
      } catch (err) {
        console.error(err);
        this.openSnackBar(
          `Ошибка при обработке изображения ${
            this.folder_files_img[idx + 1].archivefile.filename
          }`,
          'Ok'
        );
      } finally {
        this.spinnerService.hide();
        this.galleryWait = false;
      }
      return true;
    }
  }

  async previewChange(e) {
    if (e.forward === false) {
      await this.addPrevImg();
    } else {
      await this.addNextImg();
    }
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
    });
  }

  async onDownloadArchivefolderfile(id) {
    this.spinnerService.show();
    let fileName = '';
    for (let i = 0; i < this.files.length; i++) {
      if (!isNullOrUndefined(this.files[i])) {
        if (id === this.files[i].id) {
          fileName = this.files[i].archivefile.filename;
        }
      }
    }
    try {
      let blob = this.archivefolderfileService.downloadOneById(id);
      this.inProgress = true;
      this.inProgressVal = 0;
      blob.subscribe((event) => {
        if (event.type === HttpEventType.DownloadProgress) {
          const percentDone = Math.round((100 * event.loaded) / event.total);
          this.inProgressVal = percentDone;
        } else if (event instanceof HttpResponse) {
          this.inProgress = false;
          this.inProgressVal = 0;
          this.spinnerService.hide();
          importedSaveAs(event.body, fileName);
        }
      });
    } catch (err) {
      console.error(`Ошибка ${err}`);
      this.spinnerService.hide();
      this.inProgress = false;
      this.inProgressVal = 0;
      this.globalService.hasError.next(false);
    }
  }

  selectAll() {
    this.data.selectedFiles.length = 0;
    this.files.forEach((file) => {
      if (!file.blocked) {
        file.checked = true;
        this.data.selectedFiles.push(file);
      }
    });
  }

  copyFilename(file) {
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = file.archivefile.file;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
  }
  // async onOrientation(obj) {
  //   console.log(obj)
  // this.spinnerService.show();
  // let idx = this.selectedArchivefolderfiles.findIndex(
  //   (item) => item.id === obj.id
  // );
  // this.selectedArchivefolderfiles[idx].orientation = obj.val;
  // try {
  //   await this.archivefolderfileService.putOneById(obj.id, {
  //     orientation: obj.val,
  //   });
  // } catch (err) {
  //   this.globalService.exceptionHandling(err);
  // } finally {
  //   this.spinnerService.hide();
  // }
  // }
}
