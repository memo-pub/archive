import { HttpEventType, HttpResponse } from '@angular/common/http';
import {
  Component,
  ElementRef,
  Inject,
  OnInit,
  ViewChild,
} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SharedModule } from 'src/app/shared/shared.module';
import { ArchivefolderfileService } from 'src/app/system/shared/services/archivefolderfile.service';
import { ArchivefolderService } from '../../services/archivefolder.service';
import { FormControl, FormGroup } from '@angular/forms';
import { ALLOWED_EXTENSIONS } from '../../constants/allowed-extensions';

@Component({
  selector: 'app-dialog-file-loader',
  templateUrl: './dialog-file-loader.component.html',
  styleUrls: ['./dialog-file-loader.component.css'],
})
export class DialogFileLoaderComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<DialogFileLoaderComponent>,
    private archivefolderfileService: ArchivefolderfileService,
    private archivefolderService: ArchivefolderService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}
  @ViewChild('filesInput', { static: true }) filesInput: ElementRef;
  archivefolderForm: FormGroup;
  changeStatusForm: FormGroup;
  files = [];
  wrongNameFiles = [];
  alreadyExist = [];
  erroredFiles = [];
  scannedDelos = [];
  notExisted = [];
  notAllowedExtensions = [];
  inProgressVal = 0;
  inProgress = false;
  totalFiles = 0;
  loadedFiles = 0;
  loaded = false;

  async ngOnInit() {
    this.changeStatusForm = new FormGroup({
      changeStatus: new FormControl(true),
    });

    this.archivefolderForm = new FormGroup({
      status: new FormControl(0, []),
      page_cnt: new FormControl(0, []),
    });
  }

  onStatusChange() {
    this.changeStatusForm.patchValue({
      changeStatus: false,
    });
  }

  onOpenFilesInput() {
    this.filesInput.nativeElement.value = '';
    this.filesInput.nativeElement.click();
  }

  onFilesChangeInput(event) {
    for (let i = 0; i < event.srcElement.files.length; i++) {
      this.files.push(event.srcElement.files[i]);
    }
    this.totalFiles = this.files.length;
  }

  onFilesChange(file: FileList) {
    this.files = this.files.concat(file);
    this.totalFiles = this.files.length;
  }

  onDelFileById(id) {
    this.files.splice(id, 1);
    this.totalFiles = this.files.length;
  }

  onDelFiles() {
    this.files.length = 0;
    this.totalFiles = 0;
  }

  async onSaveFiles() {
    const files = this.files.sort((a, b) => a - b);
    const allowedExtensions = ALLOWED_EXTENSIONS.uploader;

    this.inProgress = true;
    for (const file of files) {
      if (
        allowedExtensions.includes(files[0].name.split('.').pop().toLowerCase())
      ) {
        const fileInfo = file.name.split('-').map((el) => +el);
        const hasDoubleDash = file.name.includes('--');
        let deloId;
        if (!!fileInfo[0] && !!fileInfo[1] && !!fileInfo[2] && !hasDoubleDash) {
          deloId = await this.archivefolderfileService.getArchiveFolderId(
            fileInfo[0],
            fileInfo[1],
            fileInfo[2]
          );
          if (deloId?.id && file) {
            const formData = new FormData();
            formData.append('archivefolder_id', deloId?.id);
            formData.append('archivefile', file);
            const selectedArchivefolderfiles =
              await this.archivefolderfileService.getAllByParentId(
                deloId?.id,
                true
              );
            const hasDuplicate =
              !!selectedArchivefolderfiles &&
              selectedArchivefolderfiles.some(
                (el) => el?.archivefile?.filename === file?.name
              );
            if (!hasDuplicate) {
              const res = await this.archivefolderfileService
                .uploadOne(formData)
                .toPromise();
              if (res && res.status === 500) {
                this.inProgressVal += 1;
                this.erroredFiles.push(file);
              } else if (res instanceof HttpResponse) {
                this.inProgressVal += 1;
                this.scannedDelos.push(deloId?.id);
              }
            } else {
              this.alreadyExist.push(file);
              this.inProgressVal += 1;
            }
          } else if (deloId === null) {
            this.notExisted.push(file);
            this.inProgressVal += 1;
          }
        } else {
          this.wrongNameFiles.push(file);
          this.inProgressVal += 1;
        }
      } else {
        this.notAllowedExtensions.push(file);
        this.inProgressVal += 1;
      }
    }
    if (this.changeStatusForm.value.changeStatus) {
      const setStatusScanned = async () => {
        const uniqueDelosId = new Set(this.scannedDelos);
        for (const id of Array.from(uniqueDelosId)) {
          const pageCnt = await this.archivefolderfileService.getAllByParentId(
            id,
            true
          );
          this.archivefolderForm.patchValue({
            status: 2,
            page_cnt: pageCnt.length ?? 1,
          });
          const putArchivefolderForm = this.archivefolderService.putOneById(
            id,
            this.archivefolderForm.getRawValue()
          );
          await putArchivefolderForm;
        }
      };
      setStatusScanned();
    }
    this.inProgress = false;
    this.loaded = true;
    this.files = [];
    this.loadedFiles =
      this.totalFiles -
      this.alreadyExist.length -
      this.wrongNameFiles.length -
      this.notExisted.length -
      this.erroredFiles.length -
      this.notAllowedExtensions.length;
  }
}
