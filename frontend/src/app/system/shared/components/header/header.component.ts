import { Component, OnInit } from '@angular/core';

import { AuthService } from '../../../../shared/services/auth.service';
import { Muser } from '../../../../shared/models/muser.model';
import { MuserService } from './../../services/muser.service';
import { Router } from '@angular/router';
import { GlobalService } from 'src/app/shared/services/global.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  loggedInUser: Muser;

  constructor(
    private authService: AuthService,
    private muserService: MuserService
  ) {}

  async ngOnInit() {
    try {
      let loggedInUser = this.muserService.getOneById(
        this.authService.getCurUser().id
      );
      this.loggedInUser = await loggedInUser;
    } catch (err) {
      if (err.status === 401) {
        console.error(`Ошибка авторизации`);
        this.authService.logout();
      } else {
        console.error(`Ошибка ${err}`);
      }
    }
  }

  logout() {
    this.authService.logout();
  }
}
