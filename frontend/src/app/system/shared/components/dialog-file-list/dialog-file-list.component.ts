import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-dialog-file-list',
  templateUrl: './dialog-file-list.component.html',
  styleUrls: ['./dialog-file-list.component.css'],
})
export class DialogFileListComponent implements OnInit {
  fileName = '';

  constructor(
    public dialogRef: MatDialogRef<DialogFileListComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit(): void {}

  selectFile(file) {
    if (!file.disabled) {
      file.selected = !file.selected;
    }
  }

  getSelectedFiles() {
    return this.data.files.filter((item) => item.selected);
  }
}
