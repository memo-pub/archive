import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-dialog-two-select',
  templateUrl: './dialog-two-select.component.html',
  styleUrls: ['./dialog-two-select.component.css'],
})
export class DialogTwoSelectComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<DialogTwoSelectComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {}

  onNoClick(): void {
    this.dialogRef.close();
  }
}
