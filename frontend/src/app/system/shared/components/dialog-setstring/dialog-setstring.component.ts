import { Component, OnInit, Inject, HostListener } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-dialog-setstring',
  templateUrl: './dialog-setstring.component.html',
  styleUrls: ['./dialog-setstring.component.css'],
})
export class DialogSetstringComponent implements OnInit {
  @HostListener('document:keydown.enter', ['$event']) onKeydownHandler(
    event: KeyboardEvent
  ) {
    if (this.data.str && this.data.str != '') {
      this.dialogRef.close(this.data.str);
    }
  }
  constructor(
    public dialogRef: MatDialogRef<DialogSetstringComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {}

  onNoClick(): void {
    this.dialogRef.close(false);
  }
}
