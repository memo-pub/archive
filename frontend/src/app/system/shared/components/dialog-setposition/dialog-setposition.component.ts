import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-dialog-setposition',
  templateUrl: './dialog-setposition.component.html',
  styleUrls: ['./dialog-setposition.component.css'],
})
export class DialogSetpositionComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<DialogSetpositionComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit(): void {}
}
