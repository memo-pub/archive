import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
    selector: 'app-dialog-save-no',
    templateUrl: './dialog-save-no.component.html',
    styleUrls: ['./dialog-save-no.component.css']
})
export class DialogSaveNoComponent implements OnInit {

    constructor(
        public dialogRef: MatDialogRef<DialogSaveNoComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) { }

    ngOnInit() {
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

}
