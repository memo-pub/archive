import { Pipe, PipeTransform } from '@angular/core';

import { Muser } from './../../../shared/models/muser.model';
import { isNullOrUndefined } from 'util';

@Pipe({
  name: 'musersFilter'
})
export class MusersFilterPipe implements PipeTransform {

  transform(persons: Muser[], str: string): any {
    if (isNullOrUndefined(persons) || str === '' || persons.length === 0) {
      return persons;
    }
    return persons.filter((person) => {
      return person.name.toLowerCase().indexOf(str.toLowerCase()) !== -1 ||
        person.surname.toLowerCase().indexOf(str.toLowerCase()) !== -1 ||
        person.login.toLowerCase().indexOf(str.toLowerCase()) !== -1 ||
        person.id === +str;
    });
  }

}
