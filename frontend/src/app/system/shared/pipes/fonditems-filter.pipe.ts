import { Pipe, PipeTransform } from '@angular/core';
import { Fond } from 'src/app/shared/models/fond.model';
import { isNullOrUndefined } from 'util';

@Pipe({
  name: 'fonditemsFilter'
})
export class FonditemsFilterPipe implements PipeTransform {
  transform(fondItems: Fond[], str: string): any {
    if (isNullOrUndefined(fondItems) || str === '' || fondItems.length === 0) {
      return fondItems;
    }
    return fondItems.filter(opis => {
      return opis.id === +str || opis.fond === +str;
    });
  }
}
