import { Pipe, PipeTransform } from '@angular/core';
import { NatT } from 'src/app/shared/models/natt.model';
import { isNullOrUndefined } from 'util';

@Pipe({
  name: 'nattFilter'
})
export class NattFilterPipe implements PipeTransform {
  transform(nations: NatT[], str: string): any {
    if (isNullOrUndefined(nations) || str === '' || nations.length === 0) {
      return nations;
    }
    return nations.filter(nation => {
      return (
        nation.nation.toLowerCase().indexOf(str.toLowerCase()) !== -1 ||
        nation.nation_rmk.toLowerCase().indexOf(str.toLowerCase()) !== -1 ||
        nation.id === +str
      );
    });
  }
}
