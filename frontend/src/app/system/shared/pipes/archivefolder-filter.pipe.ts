import { Pipe, PipeTransform } from '@angular/core';
import { Archivefolder } from '../../../shared/models/archivefolder.model';
import { isNullOrUndefined } from 'util';

@Pipe({
  name: 'archivefolderFilter'
})
export class ArchivefolderFilterPipe implements PipeTransform {
  transform(archivefolders: Archivefolder[], str: string): any {
    if (
      isNullOrUndefined(archivefolders) ||
      str === '' ||
      archivefolders.length === 0
    ) {
      return archivefolders;
    }
    return archivefolders.filter(archivefolder => {
      return (
        archivefolder.name.toLowerCase().indexOf(str.toLowerCase()) !== -1 ||
        archivefolder.delo
          .toString()
          .toLowerCase()
          .indexOf(str.toLowerCase()) !== -1
      );
    });
  }
}
