import { Pipe, PipeTransform } from '@angular/core';
import { isNullOrUndefined } from 'util';

@Pipe({
  name: 'fieldjoinFilter',
})
export class FieldjoinFilterPipe implements PipeTransform {
  transform(fieldjoins: any[], str: string): any {
    if (
      isNullOrUndefined(fieldjoins) ||
      str === '' ||
      fieldjoins.length === 0
    ) {
      return fieldjoins;
    }
    return fieldjoins.filter((fieldjoin) => {
      if (fieldjoin.section_name || fieldjoin.field_name) {
        return (
          fieldjoin.section_name.toLowerCase().indexOf(str.toLowerCase()) !==
            -1 ||
          fieldjoin.field_name.toLowerCase().indexOf(str.toLowerCase()) !== -1
        );
      } else {
        return fieldjoin.value.toLowerCase().indexOf(str.toLowerCase()) !== -1;
      }
    });
  }
}
