import { Pipe, PipeTransform } from '@angular/core';
import { Opis } from '../../../shared/models/opis.model';
import { isNullOrUndefined } from 'util';

@Pipe({
  name: 'opisitemsFilter'
})
export class OpisitemsFilterPipe implements PipeTransform {
  transform(opisItems: Opis[], str: string): any {
    if (isNullOrUndefined(opisItems) || str === '' || opisItems.length === 0) {
      return opisItems;
    }
    return opisItems.filter(opis => {
      return (
        opis.id === +str ||
        opis.opis_name === +str ||
        opis.trudindex === parseFloat(str.replace(/[,]+/g, '.')) ||
        opis.fond === +str
      );
    });
  }
}
