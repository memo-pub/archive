import { Pipe, PipeTransform } from '@angular/core';
import { ListOfT } from 'src/app/shared/models/listoft.model';
import { isNullOrUndefined } from 'util';

@Pipe({
    name: 'listoftFilter'
})
export class ListoftFilterPipe implements PipeTransform {

    transform(lists: ListOfT[], str: string): any {
        if (isNullOrUndefined(lists) || str === '' || lists.length === 0) {
            return lists;
        }
        return lists.filter((list) => {
            return String(list.name_ent).toLowerCase().indexOf(str.toLowerCase()) !== -1 ||
            String(list.name_rmk).toLowerCase().indexOf(str.toLowerCase()) !== -1 ||
            String(list.death_type).toLowerCase().indexOf(str.toLowerCase()) !== -1 ||
            String(list.name_rmk).toLowerCase().indexOf(str.toLowerCase()) !== -1 ||
            String(list.death_rmk).toLowerCase().indexOf(str.toLowerCase()) !== -1 ||
            +list.id === +str;
        });
    }

}
