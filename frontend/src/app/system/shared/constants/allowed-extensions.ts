export const ALLOWED_EXTENSIONS = {
  archivefolder: [
    'jpg',
    'xls',
    'xlsx',
    'png',
    'gif',
    'bmp',
    'jpeg',
    'pdf',
    'tiff',
    'tif',
    'doc',
    'docx',
    'avi',
    'mp4',
    'mkv',
    'mp3',
    'wav',
    'ogg',
    'ogv',
    'm4v',
    'mts',
    'mpeg',
  ],
  fond: [
    'jpg',
    'png',
    'gif',
    'bmp',
    'jpeg',
    'pdf',
    'tiff',
    'tif',
    'doc',
    'docx',
    'xls',
    'xlsx',
  ],
  opis: [
    'jpg',
    'png',
    'gif',
    'bmp',
    'jpeg',
    'pdf',
    'tiff',
    'tif',
    'doc',
    'docx',
    'xls',
    'xlsx',
  ],
  uploader: [
    'jpg',
    'xls',
    'xlsx',
    'png',
    'gif',
    'bmp',
    'jpeg',
    'pdf',
    'tiff',
    'tif',
    'doc',
    'docx',
    'avi',
    'mp4',
    'mkv',
    'mp3',
    'wav',
    'ogg',
    'ogv',
    'm4v',
    'mts',
    'mpeg',
  ]
};
