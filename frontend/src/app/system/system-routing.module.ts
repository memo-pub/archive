import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SystemComponent } from './system.component';
import { AuthGuard } from '../shared/services/auth.guard';
import { MainGuard } from '../shared/services/main.guard';
import { ExtraGuard } from '../shared/services/extra.guard';

const routes: Routes = [
  {
    path: 'sys',
    component: SystemComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'users',
        canActivate: [AuthGuard],
        loadChildren: () => import('./muser/muser.module').then(m => m.MuserModule)
      },
      {
        path: 'arch',
        canActivate: [AuthGuard],
        loadChildren: () => import('./marchive/marchive.module').then(m => m.MarchiveModule)
      },
      {
        path: 'person',
        canActivate: [ExtraGuard],
        loadChildren: () => import('./personal/personal.module').then(m => m.PersonalModule)
      }
    ]
  },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SystemRoutingModule {}
