import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { AuthService } from 'src/app/shared/services/auth.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { FieldjoinService } from '../../shared/services/fieldjoin.service';
import { FiltersService } from '../../shared/services/filters.service';

@Component({
  selector: 'app-stat-list',
  templateUrl: './stat-list.component.html',
  styleUrls: ['./stat-list.component.css'],
})
export class StatListComponent implements OnInit {
  fieldjoins: any[];
  start = 0;
  end = 0;
  limit = 100;

  constructor(
    public filtersService: FiltersService,
    private globalService: GlobalService,
    private authService: AuthService,
    private fieldjoinService: FieldjoinService,
    private router: Router,
    private spinnerService: NgxSpinnerService
  ) {}

  ngOnInit() {
    this.getData();
    this.end = this.limit;
  }

  async getData() {
    this.spinnerService.show();
    try {
      let fieldjoins = this.fieldjoinService.getAll();
      this.fieldjoins = !(await fieldjoins) ? [] : await fieldjoins;
    } catch (err) {
      if (err.status === 401) {
        console.error(`Ошибка авторизации`);
        this.authService.logout();
      } else {
        console.error(`Ошибка ${err}`);
      }
    } finally {
      this.spinnerService.hide();
    }
  }

  goToPage(n: number): void {
    this.filtersService.fieldjoinListFilter.page = n;
    this.updatePage();
  }

  onNext(): void {
    this.filtersService.fieldjoinListFilter.page++;
    this.updatePage();
  }

  onPrev(): void {
    this.filtersService.fieldjoinListFilter.page = 1;
    this.updatePage();
  }

  onNext10(n = 1): void {
    this.filtersService.fieldjoinListFilter.page += 10 * n;
    this.updatePage();
  }

  onPrev10(n = 1): void {
    this.filtersService.fieldjoinListFilter.page -= 10 * n;
    this.updatePage();
  }

  updatePage() {
    this.start =
      this.limit * (this.filtersService.fieldjoinListFilter.page - 1);
    this.end = this.start + this.limit;
  }

  onEdit(field) {
    if (field.repl) {
      this.router.navigate(['sys/person/statedit', field.id]);
    } else {
      this.router.navigate([this.router.url, field.id]);
    }
  }

  updateData() {
    if (this.filtersService.fieldjoinListFilter.searchStr.length > 0) {
      this.start = 0;
      this.end = this.limit;
      this.filtersService.fieldjoinListFilter.page = 1;
    } else {
      this.start =
        this.limit * (this.filtersService.fieldjoinListFilter.page - 1);
      this.end = this.start + this.limit;
    }
  }
}
