import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import {HttpClientModule} from '@angular/common/http';
import { MatDialogModule} from '@angular/material/dialog';
import { FieldjoinFilterPipe } from '../../shared/pipes/fieldjoin-filter.pipe';

import { StatListComponent } from './stat-list.component';

describe('StatListComponent', () => {
  let component: StatListComponent;
  let fixture: ComponentFixture<StatListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatListComponent, FieldjoinFilterPipe],
            imports: [RouterTestingModule, MatSnackBarModule, HttpClientTestingModule, MatDialogModule],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
