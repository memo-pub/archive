import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import {HttpClientModule} from '@angular/common/http';
import { MatDialogModule} from '@angular/material/dialog';
import { NattFilterPipe } from '../../shared/pipes/natt-filter.pipe';

import { NationListComponent } from './nation-list.component';

describe('NationListComponent', () => {
    let component: NationListComponent;
    let fixture: ComponentFixture<NationListComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [NationListComponent, NattFilterPipe],
            imports: [RouterTestingModule, MatSnackBarModule, HttpClientTestingModule, MatDialogModule],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(NationListComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
