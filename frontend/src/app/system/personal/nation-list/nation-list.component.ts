import { Component, OnInit, HostListener, ViewChild } from '@angular/core';
import { Muser } from 'src/app/shared/models/muser.model';
import { NatT } from 'src/app/shared/models/natt.model';
import { PageEvent, MatPaginator } from '@angular/material/paginator';
import { NattService } from '../../shared/services/natt.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { AuthService } from 'src/app/shared/services/auth.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { MuserService } from '../../shared/services/muser.service';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-nation-list',
  templateUrl: './nation-list.component.html',
  styleUrls: ['./nation-list.component.css'],
})
export class NationListComponent implements OnInit {
  loggedInUser: Muser;
  searchStr = '';
  nations: NatT[];

  // MatPaginator Inputs
  length = 100;
  pageSize = 100;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  pageEvent: PageEvent;
  start = 0;
  end = 0;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  aditStr = 'Редактировать';

  /**Для пагинатора */
  page = 1;
  limit = 100;

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.aditStr =
      event.target.innerWidth < 700
        ? '<i class="fa fa-pencil" aria-hidden="true"></i>'
        : 'Редактировать';
  }

  constructor(
    private globalService: GlobalService,
    private authService: AuthService,
    private muserService: MuserService,
    private nattService: NattService,
    private router: Router,
    private spinnerService: NgxSpinnerService
  ) {}

  async ngOnInit() {
    this.aditStr =
      window.screen.width < 700
        ? '<i class="fa fa-pencil" aria-hidden="true"></i>'
        : 'Редактировать';
    try {
      let loggedInUser = this.muserService.getOneById(
        this.authService.getCurUser().id
      );
      let nations = this.nattService.getAll();
      this.loggedInUser = await loggedInUser;
      this.nations = isNullOrUndefined(await nations) ? [] : await nations;
      this.length = this.nations.length;
      if (this.length > 100) {
        this.pageSizeOptions.push(this.length);
      }
      this.end = this.pageSize;
    } catch (err) {
      this.globalService.exceptionHandling(err);
    } finally {
      this.spinnerService.hide();
    }
  }

  updateData() {
    if (this.searchStr.length > 0) {
      this.start = 0;
      // this.end = this.paginator.pageSize;
      this.end = this.limit;
      // this.paginator.firstPage();
      this.page = 1;
    } else {
      // this.start = this.paginator.pageSize * this.paginator.pageIndex;
      // this.end = this.start + this.paginator.pageSize;
      this.start = this.limit * (this.page - 1);
      this.end = this.start + this.limit;
    }
  }

  setPageSizeOptions(event: PageEvent) {
    this.start = event.pageSize * event.pageIndex;
    this.end = this.start + event.pageSize;
    return event;
  }

  onAddNation() {
    this.router.navigate(['/sys/person/nationedit/newdoc']);
  }

  onEditNation(id) {
    this.router.navigate([`/sys/person/nationedit/${id}`]);
  }

  goToPage(n: number): void {
    this.page = n;
    this.updatePage();
  }

  onNext(): void {
    this.page++;
    this.updatePage();
  }

  onPrev(): void {
    this.page = 1;
    this.updatePage();
  }

  onNext10(n = 1): void {
    this.page += 10 * n;
    this.updatePage();
  }

  onPrev10(n = 1): void {
    this.page -= 10 * n;
    this.updatePage();
  }

  async updatePage() {
    this.start = this.limit * (this.page - 1);
    this.end = this.start + this.limit;
  }
}
