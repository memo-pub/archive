import { HttpEventType, HttpResponse } from '@angular/common/http';
import { HostListener } from '@angular/core';
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Subscription } from 'rxjs';
import { ListOfT } from 'src/app/shared/models/listoft.model';
import { map, startWith } from 'rxjs/operators';
import {
  SourT,
  SourtFilter,
  sourT_width,
} from 'src/app/shared/models/sourt.model';
import { GlobalService } from 'src/app/shared/services/global.service';
import { DialogSelectFilesComponent } from '../../shared/components/dialog-select-files/dialog-select-files.component';
import {
  LightboxImage,
  LightboxComponent,
} from '../../shared/components/lightbox/lightbox.component';
import { ArchivefolderfileService } from '../../shared/services/archivefolderfile.service';
import { FiltersService } from '../../shared/services/filters.service';
import { ListoftService } from '../../shared/services/listoft.service';
import { SourtService } from '../../shared/services/sourt.service';
import { SourtypeService } from '../../shared/services/sourtype.service';
import { saveAs as importedSaveAs } from 'file-saver';
import { DialogYesNoComponent } from '../../shared/components/dialog-yes-no/dialog-yes-no.component';
import { SourfiletService } from '../../shared/services/sourfilet.service';
import { ArchivefolderService } from '../../shared/services/archivefolder.service';
import { DialogSaveNoComponent } from '../../shared/components/dialog-save-no/dialog-save-no.component';
import { DialogJoinPersonalComponent } from '../../shared/components/dialog-join-personal/dialog-join-personal.component';
import { PersonalsourtService } from '../../shared/services/personalsourt.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DateparseService } from 'src/app/shared/services/dateparse.service';
import { SourtlinkService } from '../../shared/services/sour_t_link.service';
import { Observable } from 'rxjs';
import { DialogPdfViewerComponent } from '../../shared/components/dialog-pdf-viewer/dialog-pdf-viewer.component';
import { DialogSetDocumentParentComponent } from '../../shared/components/dialog-set-document-parent/dialog-set-document-parent.component';
import { AuthService } from 'src/app/shared/services/auth.service';
import { PublicSiteActionsService } from '../../shared/services/publicsiteactions.service';
import { Archivefolder } from 'src/app/shared/models/archivefolder.model';
import { DialogOkComponent } from '../../shared/components/dialog-ok/dialog-ok.component';
import { PUBLICATION_MESSAGES } from '../../shared/constants/publication-strings';

@Component({
  selector: 'app-document-edit',
  templateUrl: './document-edit.component.html',
  styleUrls: ['./document-edit.component.css'],
})
export class DocumentEditComponent implements OnInit, OnDestroy {
  constructor(
    public dateparseService: DateparseService,
    public globalService: GlobalService,
    private authService: AuthService,
    public filtersService: FiltersService,
    private archivefolderService: ArchivefolderService,
    private sourtService: SourtService,
    private sourfiletService: SourfiletService,
    private archivefolderfileService: ArchivefolderfileService,
    private sourtypeService: SourtypeService,
    private personalsourtService: PersonalsourtService,
    private listoftService: ListoftService,
    private activatedRoute: ActivatedRoute,
    private sourtlinkService: SourtlinkService,
    private router: Router,
    private dialog: MatDialog,
    private spinnerService: NgxSpinnerService,
    private publicSiteActions: PublicSiteActionsService,
    private snackBar: MatSnackBar
  ) {
    this.activeRouterObserver = this.activatedRoute.params.subscribe(
      (param) => {
        this.id = param.id ? +param.id : null;
        if (this.inited) {
          this.getData();
        }
      }
    );
  }
  id: number;
  activeRouterObserver: Subscription;
  sourtForm: FormGroup;

  sour_marks = [];

  sourt: SourT;
  selectedSourtOldData: SourT;
  parentSourts: SourT[] = [];
  parentSourt: SourT;
  sourts: SourT[] = [];
  sour_typeInForm: any[] = [];
  reprod_typeInForm: any[] = [];
  sour_types: ListOfT[] = [];
  reprod_types: ListOfT[] = [];
  reprod_type_ids: string;
  sour_type: any[] = [];
  viewOnly: boolean;

  nextId: number;
  previousId: number;
  first = false;
  last = false;
  link_type = 1;
  sourT_width = sourT_width;

  folder_files_to_add = [];
  folder_files = [];
  folder_files_img = [];

  inited = false;

  inProgress = false;
  inProgressVal = 0;
  loaded = 0;

  sourtlinks = [];
  timerId;
  /**Галерея */
  imgBufsize = 3;
  lightboxImages: LightboxImage[] = [];
  @ViewChild('lightbox', { static: true }) lightbox: LightboxComponent;
  galleryWait = false;

  bufFund;
  bufList_no;
  bufFile_no;

  delo;
  deloTimer;

  filteredsourts: Observable<SourT[]>;
  myControl = new FormControl();
  links = [];
  curDate: string;

  link_types = [
    {
      key: 1,
      value: 'Дубликат',
    },
  ];
  archivefolderInfo: Archivefolder;
  isPublicationUnblocked: boolean;
  @HostListener('window:keydown', ['$event'])
  onKeyPress(event: KeyboardEvent) {
    if ((event.ctrlKey || event.metaKey) && event.key == 's') {
      event.preventDefault();
      if (this.sourtForm.valid) {
        this.onSave();
      }
    }
  }

  ngOnDestroy() {
    this.activeRouterObserver.unsubscribe();
  }

  ngOnInit(): void {
    this.viewOnly = this.authService.getCurUser().usertype === 4;
    this.curDate = new Date().toISOString().split('T')[0];
    this.sourtForm = new FormGroup({
      parent_id: new FormControl({ value: null, disabled: this.viewOnly }, []),
      id: new FormControl({ value: null, disabled: this.viewOnly }, []),
      sour_no: new FormControl({ value: null, disabled: true }, []),
      sour_name: new FormControl({ value: null, disabled: this.viewOnly }, []),
      sour_code: new FormControl({ value: null, disabled: this.viewOnly }, []),
      mark_mem: new FormControl({ value: true, disabled: this.viewOnly }, []),
      place_so: new FormControl({ value: null, disabled: this.viewOnly }, []),
      sour_rmk: new FormControl({ value: null, disabled: this.viewOnly }, []),
      acs_rmk: new FormControl({ value: null, disabled: this.viewOnly }, []),
      category: new FormControl({ value: null, disabled: this.viewOnly }, []),
      fund: new FormControl({ value: this.bufFund, disabled: this.viewOnly }, [
        Validators.required,
      ]),
      list_no: new FormControl(
        { value: this.bufList_no, disabled: this.viewOnly },
        [Validators.required]
      ),
      file_no: new FormControl(
        { value: this.bufFile_no, disabled: this.viewOnly },
        [Validators.required]
      ),
      file_no_ext: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      sour_type: new FormControl({ value: null, disabled: this.viewOnly }, []),
      sour_mark: new FormControl({ value: 'К', disabled: this.viewOnly }, []),
      sour_date: new FormControl({ value: null, disabled: this.viewOnly }, []),
      sour_date_begin: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      sour_date_end: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      sour_date_begin_end: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      pub_type: new FormControl({ value: 4, disabled: this.viewOnly }, []),

      reprod_type_ids: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      reprod_typeInForm: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      reprod_rmk: new FormControl({ value: null, disabled: this.viewOnly }, []),
      published: new FormControl({ value: null, disabled: this.viewOnly }, []),
      mustPublish: new FormControl(null),
      mustUnpublish: new FormControl(null),
      pub_change_date: new FormControl(null),
    });
    this.sour_marks = this.globalService.sour_mark;
    this.getData();
    this.inited = true;
  }

  async getData() {
    this.links.length = 0;
    this.sour_typeInForm.length = 0;
    this.reprod_typeInForm.length = 0;
    this.folder_files_to_add.length = 0;
    this.folder_files.length = 0;
    this.folder_files_img.length = 0;
    this.lightboxImages.length = 0;
    this.sourtForm.reset();
    this.spinnerService.show();

    try {
      this.sourts = await this.sourtService.getWithOffsetLimit(0, 100);
      if (this.id) {
        let sourtlinks = this.sourtlinkService.getAllWithNamesById(this.id);
        let idNext = this.sourtService.getnextkey(
          this.filtersService.selectionSourtFilter.searchOrder,
          this.id,
          this.filtersService.selectionSourtFilter.fund,
          this.filtersService.selectionSourtFilter.list_no,
          this.filtersService.selectionSourtFilter.file_no,
          this.filtersService.selectionSourtFilter.file_no_ext,
          this.filtersService.selectionSourtFilter.file_no_ext_noneed,
          this.filtersService.selectionSourtFilter.sour_name_filter,
          this.filtersService.selectionSourtFilter.sour_type_filter,
          this.filtersService.selectionSourtFilter.pub_type,
          this.filtersService.selectionSourtFilter.filter_sour_date_from,
          this.filtersService.selectionSourtFilter.filter_sour_date_to
        );
        let idPrevious = this.sourtService.getpreviouskey(
          this.filtersService.selectionSourtFilter.searchOrder,
          this.id,
          this.filtersService.selectionSourtFilter.fund,
          this.filtersService.selectionSourtFilter.list_no,
          this.filtersService.selectionSourtFilter.file_no,
          this.filtersService.selectionSourtFilter.file_no_ext,
          this.filtersService.selectionSourtFilter.file_no_ext_noneed,
          this.filtersService.selectionSourtFilter.sour_name_filter,
          this.filtersService.selectionSourtFilter.sour_type_filter,
          this.filtersService.selectionSourtFilter.pub_type,
          this.filtersService.selectionSourtFilter.filter_sour_date_from,
          this.filtersService.selectionSourtFilter.filter_sour_date_to
        );
        this.nextId = (await idNext) ? +(await idNext).nextkey : null;
        this.previousId = (await idPrevious)
          ? +(await idPrevious).previouskey
          : null;
        let sourt = this.sourtService.getOneById(this.id, true);
        this.sourt = await sourt;
        this.selectedSourtOldData = this.sourt;
        let sour_type = this.sourtypeService.getBySourid(this.id);
        this.sour_type = !(await sour_type) ? [] : await sour_type;
        this.sourtlinks = (await sourtlinks) || [];
        this.sourtlinks.map((sourtlink) => {
          sourtlink.checkToDel = false;
          if (this.sourtlinks) {
            this.sourts = this.sourts.filter(
              (sourts) =>
                !(
                  sourts.id === sourtlink.sour_t_id1 ||
                  sourts.id === sourtlink.sour_t_id2
                )
            );
          }
        });
        this.sourts = this.sourts.filter((sourt) => sourt.id !== this.id);
      }

      this.filteredsourts = this.myControl.valueChanges.pipe(
        startWith<string | SourT>(''),
        map((value) =>
          typeof value === 'string' ? value : !value ? null : value.sour_name
        ),
        map((name) => (name ? this._filter(name) : [...this.sourts]))
      );

      let sour_types = this.listoftService.getWithFilter('SOUR_TYPE');
      this.sour_types = !(await sour_types) ? [] : await sour_types;
      this.sour_types.sort((a, b) => {
        if (a.name_ent < b.name_ent) {
          return -1;
        }
        if (a.name_ent > b.name_ent) {
          return 1;
        }
        return 0;
      });
      let reprod_types = this.listoftService.getWithFilter('REPROD_TYPE');
      this.reprod_types = !(await reprod_types) ? [] : await reprod_types;
      this.reprod_types.sort((a, b) => {
        if (a.name_ent < b.name_ent) {
          return -1;
        }
        if (a.name_ent > b.name_ent) {
          return 1;
        }
        return 0;
      });
      this.sour_typeInForm = [];

      this.sour_type.forEach((sour_type) => {
        let element = this.sour_types.find(
          (item) => item.id === sour_type.sour_type_id
        );
        if (element) {
          this.sour_typeInForm.push(element);
        }
      });

      this.sour_typeInForm.sort((a, b) => {
        if (a.name_ent < b.name_ent) {
          return -1;
        }
        if (a.last_nom > b.last_nom) {
          return 1;
        }
        return 0;
      });

      if (this.sourt?.reprod_type_ids) {
        this.reprod_typeInForm = [];

        this.sourt.reprod_type_ids.split(',').forEach((reprod_type) => {
          this.reprod_typeInForm.push(
            this.reprod_types.find((item) => item.id === +reprod_type)
          );
        });

        this.reprod_typeInForm.sort((a, b) => {
          if (a.name_ent < b.name_ent) {
            return -1;
          }
          if (a.last_nom > b.last_nom) {
            return 1;
          }
          return 0;
        });
      }

      if (this.sourt) {
        const currentArchivefolder =
          await this.archivefolderService.getWithOpis_data(
            this.sourt.file_no,
            this.sourt.fund,
            this.sourt.list_no,
            this.sourt.file_no_ext
          );
        this.archivefolderInfo = currentArchivefolder;
        this.isPublicationUnblocked =
          this.archivefolderInfo && this.archivefolderInfo[0].published === '*'
            ? true
            : false;
        this.sourtForm.patchValue({
          parent_id: this.sourt.parent_id,
          id: this.sourt.id,
          sour_no: this.sourt.sour_no,
          sour_name: this.sourt.sour_name,
          sour_code: this.sourt.sour_code,
          mark_mem: true,
          place_so: this.sourt.place_so,
          sour_rmk: this.sourt.sour_rmk,
          acs_rmk: this.sourt.acs_rmk,
          category: this.sourt.category,
          fund: this.sourt.fund,
          list_no: this.sourt.list_no,
          file_no: this.sourt.file_no,
          file_no_ext: this.sourt.file_no_ext,
          sour_type: this.sour_typeInForm,
          sour_mark: this.sourt.sour_mark,
          sour_date: this.sourt.sour_date,
          sour_date_begin: this.sourt.sour_date_begin,
          sour_date_end: this.sourt.sour_date_end,
          reprod_typeInForm: this.reprod_typeInForm,
          reprod_rmk: this.sourt.reprod_rmk,
          pub_type: this.sourt.pub_type || 4,
          published: this.sourt.published === '*' ? true : false,
          mustPublish: this.sourt.mustPublish,
          mustUnpublish: this.sourt.mustUnpublish,
          pub_change_date: this.sourt.pub_change_date,
        });
        this.dateparseService.setSourDate(
          this.sourtForm,
          'sour_date_begin_end',
          this.sourt.sour_date_begin,
          this.sourt.sour_date_end
        );
        this.folder_files = !this.sourt.folder_file
          ? []
          : this.sourt.folder_file;

        this.folder_files.forEach((element) => {
          element.checkToDel = false;
        });
        this.folder_files.sort((a, b) =>
          a.archivefile.filename > b.archivefile.filename ? 1 : -1
        );
        this.folder_files_img = this.folder_files.filter(
          (selectedArchivefolderfile) =>
            this.globalService.isImgtype(
              selectedArchivefolderfile.archivefile.filename
            ) ||
            this.globalService.isVideotype(
              selectedArchivefolderfile.archivefile.filename
            ) ||
            this.globalService.isAudiotype(
              selectedArchivefolderfile.archivefile.filename
            )
        );
        this.folder_files_img.sort((a, b) =>
          a.archivefile.filename > b.archivefile.filename ? 1 : -1
        );
        this.parentSourt = this.sourt.parent_data;
        if (this.parentSourt) {
          this.parentSourt.id = this.sourt.parent_id;
        }
      } else {
        this.sourtForm.patchValue({
          parent_id: null,
          pub_type: 4,
          mark_mem: true,
          fund: this.filtersService.sourtPlusOne.fund,
          list_no: this.filtersService.sourtPlusOne.list_no,
          file_no: this.filtersService.sourtPlusOne.file_no,
          file_no_ext: this.filtersService.sourtPlusOne.file_no_ext,
          // sour_no: this.filtersService.sourtPlusOne.sour_no + 1,
          sour_mark: 'К',
          published: true,
          mustPublish: '*',
          mustUnpublish: null,
          pub_change_date: this.curDate,
        });
      }
    } catch (err) {
      this.globalService.exceptionHandling(err);
    } finally {
      this.spinnerService.hide();
    }
    this.checkDelo();
  }

  private _filter(name: string): SourT[] {
    const filterValue = name.toLowerCase();
    return !this.sourts
      ? null
      : this.sourts.filter(
          (sourt) => sourt.sour_name.toLowerCase().indexOf(filterValue) !== -1
        );
  }

  async getPublicSiteActionType(docForm?) {
    const oldData = this.selectedSourtOldData;
    const newData = this.sourtForm;
    if (oldData && newData) {
      if (oldData.published !== newData.value.published) {
        const isActionDelete =
          this.sourtForm.value['published'] === null ? '*' : null;
        const isActionUpdate =
          this.sourtForm.value['published'] === '*' ? '*' : null;
        if (isActionDelete !== isActionUpdate) {
          await this.publicSiteActions.postOne({
            entity_id: this.id,
            entity_type: 5,
            action_delete: isActionDelete,
            action_update: isActionUpdate,
          });
        } else {
          // doc deleted
          await this.publicSiteActions.postOne({
            entity_id: this.id,
            entity_type: 5,
            action_delete: '*',
            action_update: null,
          });
        }
      }
    }

    if (!oldData) {
      // added new doc
      await this.publicSiteActions.postOne({
        entity_id: docForm,
        entity_type: 5,
        action_delete: null,
        action_update: '*',
      });
    }
  }

  async onPubToggleChange() {
    if (!this.isPublicationUnblocked) {
      this.dialog.open(DialogOkComponent, {
        data: {
          title:
            PUBLICATION_MESSAGES.CHANGE_PARENT_STATUS,
        },
      });
      this.sourtForm.patchValue({
        published: this.sourt.published === '*' ? true : false,
      });
    }
  }

  onSave(silent = false, addnew = false, update = true) {
    return new Promise(async (resolve, reject) => {
      let res = false;
      this.spinnerService.show();
      try {
        if (
          !this.sourtForm.getRawValue()['sour_date'] ||
          this.sourtForm.getRawValue()['sour_date'] === ''
        ) {
          this.sourtForm.patchValue({
            sour_date: null,
          });
        }
        if (
          this.sourtForm.getRawValue()['mark_mem'] === true ||
          this.sourtForm.getRawValue()['mark_mem'] === '*'
        ) {
          this.sourtForm.patchValue({
            mark_mem: '*',
          });
        } else {
          this.sourtForm.patchValue({
            mark_mem: null,
          });
        }

        if (
          this.sourtForm.value['published'] === true ||
          this.sourtForm.value['published'] === '*'
        ) {
          this.sourtForm.patchValue({
            published: '*',
          });
        } else {
          this.sourtForm.patchValue({
            published: null,
          });
        }
        const oldPublicationStatus = this.sourtForm.value.published;
        const newPublicationStatus = this.sourtForm.value.published;
        if (oldPublicationStatus !== newPublicationStatus) {
          if (
            this.sourtForm.value['published'] === true ||
            this.sourtForm.value['published'] === '*'
          ) {
            this.sourtForm.patchValue({
              mustPublish: '*',
              mustUnpublish: null,
              pub_change_date: this.curDate,
            });
          } else {
            this.sourtForm.patchValue({
              mustPublish: null,
              mustUnpublish: '*',
              pub_change_date: this.curDate,
            });
          }
        }

        if (this.sourtForm.getRawValue().reprod_typeInForm) {
          const generateReprodStr = (reprodTypes) => {
            let reprodString = '';
            reprodTypes.forEach((el) => (reprodString += `${el.id},`));
            return reprodString.substring(0, reprodString.length - 1);
          };

          this.sourtForm.patchValue({
            reprod_type_ids: generateReprodStr(
              this.sourtForm.getRawValue().reprod_typeInForm
            ),
          });
        }
        if (this.id) {
          let elementsToDel = [];
          if (this.sour_type) {
            this.sour_type.forEach((sour_type) => {
              let elementInForm = this.sourtForm
                .getRawValue()
                .sour_type.find((item) => item.id === sour_type.sour_type_id);
              if (!elementInForm) {
                let res = this.sourtypeService.deleteOneById(sour_type.id);
                elementsToDel.push(res);
              }
            });
          }
          let elementsToPost = [];
          if (this.sourtForm.getRawValue().sour_type) {
            this.sourtForm
              .getRawValue()
              .sour_type.forEach((sour_typeInForm) => {
                let sour_type = this.sour_type.find(
                  (item) => item.sour_type_id === sour_typeInForm.id
                );
                if (!sour_type) {
                  let obj = {
                    sour_id: this.id,
                    sour_type_id: sour_typeInForm.id,
                  };
                  let res = this.sourtypeService.postOne(obj);
                  elementsToPost.push(res);
                }
              });
          }
          let filesToDel = [];
          if (this.folder_files) {
            this.folder_files.forEach((file_to_del) => {
              if (file_to_del.checkToDel === true) {
                let res = this.sourfiletService.deleteOneById(
                  file_to_del.link_id
                );
                filesToDel.push(res);
              }
            });
          }
          let filesToPost = [];
          if (this.folder_files_to_add) {
            this.folder_files_to_add.forEach((file_to_add) => {
              let obj = {
                sour_id: this.id,
                archivefolderfile_id: file_to_add.id,
              };
              let res = this.sourfiletService.postOne(obj);
              filesToPost.push(res);
            });
          }
          let deletechildrens = Promise.all(
            this.sourt.children.map(async (child) => {
              if (child.checkToDel) {
                return this.sourtService.putOneById(child.id, {
                  parent_id: null,
                });
              }
            })
          );
          let deletesourtlinks = Promise.all(
            this.sourtlinks.map(async (sourtlink) => {
              if (sourtlink.checkToDel) {
                if (sourtlink.link_type === 1) {
                  return this.sourtlinkService.deleteOneById(sourtlink.id, {
                    id_fordelete:
                      +sourtlink.sour_t_id1 === +this.id
                        ? sourtlink.sour_t_id2
                        : sourtlink.sour_t_id1,
                  });
                }
              }
            })
          );
          let addsourtlinks = Promise.all(
            this.links.map(async (link) => {
              await this.sourtlinkService.postOne({
                sour_t_id1: this.id,
                sour_t_id2: link.val.id,
                link_type: link.link_type,
              });
            })
          );
          await deletechildrens;
          await deletesourtlinks;
          await addsourtlinks;
          await Promise.all(elementsToDel);
          await Promise.all(elementsToPost);
          await Promise.all(filesToPost);
          await Promise.all(filesToDel);
          this.folder_files_to_add.length = 0;
          let obj = this.sourtForm.getRawValue();
          // delete obj.sour_date;
          await this.sourtService.putOneById(this.id, obj);
          await this.getPublicSiteActionType();
          if (addnew === true) {
            this.filtersService.sourtPlusOne.newnom = `${
              this.sourtForm.getRawValue().sour_no
            }`;
          }
        } else {
          this.sourtForm.removeControl('id');
          let obj = this.sourtForm.value;
          if (this.filtersService.sourtPlusOne.newnom) {
            obj['newnom'] = +this.filtersService.sourtPlusOne.newnom + 1;
          }
          // delete obj.sour_date;
          let postForm = await this.sourtService.postOne(obj);
          this.id = +postForm.id;
          if (addnew === true) {
            this.filtersService.sourtPlusOne.newnom = postForm.sour_no;
          }
          let elementsToAdd = [];
          if (this.sourtForm.getRawValue().sour_type) {
            this.sourtForm
              .getRawValue()
              .sour_type.forEach((sour_typeInForm) => {
                let obj = {
                  sour_id: this.id,
                  sour_type_id: sour_typeInForm.id,
                };
                let res = this.sourtypeService.postOne(obj);
                elementsToAdd.push(res);
              });
          }
          let filesToAdd = [];
          if (this.folder_files_to_add) {
            this.folder_files_to_add.forEach((file_to_add) => {
              let obj = {
                sour_id: this.id,
                archivefolderfile_id: file_to_add.id,
              };
              let res = this.sourfiletService.postOne(obj);
              filesToAdd.push(res);
            });
          }
          let addsourtlinks = Promise.all(
            this.links.map(async (link) => {
              let addsourtlink = await this.sourtlinkService.postOne({
                sour_t_id1: this.id,
                sour_t_id2: link.val.id,
                link_type: link.link_type,
              });
            })
          );
          await addsourtlinks;
          await Promise.all(elementsToAdd);
          await Promise.all(filesToAdd);
          this.router.navigate(['sys/person/doc', this.id]);
        }
        if (addnew === true) {
          this.bufFund = this.sourtForm.getRawValue().fund;
          this.bufList_no = this.sourtForm.getRawValue().list_no;
          this.bufFile_no = this.sourtForm.getRawValue().file_no;
          this.filtersService.sourtPlusOne.fund = this.sourtForm.value.fund;
          this.filtersService.sourtPlusOne.list_no =
            this.sourtForm.value.list_no;
          this.filtersService.sourtPlusOne.file_no =
            this.sourtForm.value.file_no;
          this.filtersService.sourtPlusOne.file_no_ext =
            this.sourtForm.value.file_no_ext;
          // this.filtersService.sourtPlusOne.sour_no = this.sourtForm.value.sour_no;
          this.router.navigate(['sys/person/doc']);
          this.id = null;
        } else {
          this.bufFund = null;
          this.bufList_no = null;
          this.bufFile_no = null;
          this.filtersService.sourtPlusOne.newnom = null;
          this.filtersService.sourtPlusOne.fund = null;
          this.filtersService.sourtPlusOne.list_no = null;
          this.filtersService.sourtPlusOne.file_no = null;
          this.filtersService.sourtPlusOne.file_no_ext = null;
          this.filtersService.sourtPlusOne.sour_no = null;
        }
        this.getPublicSiteActionType(this.id);
        if (update) {
          await this.getData();
        }
        if (!silent) {
          this.globalService.openSnackBar(`Сохранено.`, 'Ok');
        }
        res = true;
      } catch (err) {
        if (err.status === 401) {
          console.error(`Ошибка авторизации`);
        } else if (err.status === 409) {
          this.sourtForm.controls['sour_no'].setErrors({ incorrect: true });
          this.globalService.openSnackBar(`${err.error['error']}`, 'Ok');
        } else {
          console.error(`Ошибка ${err}`);
          this.globalService.openSnackBar(`Ошибка ${err.status}`, 'Ok');
        }
        resolve(false);
      } finally {
        this.links.length = 0;
        this.spinnerService.hide();
        resolve(true);
        return res;
      }
    });
  }

  onDelLinkById(id) {
    this.sourts = this.sourts.concat(this.links.splice(id, 1));
    this.myControl.patchValue({});
  }

  async onMove(str) {
    this.goToPage(str);
  }

  linkSourt(id) {
    this.router.navigate(['sys/person/doc', id]);
  }

  async goToPage(str) {
    if (str === 'next') {
      if (this.nextId) {
        this.linkSourt(this.nextId);
        // this.id = this.nextId;
      }
    } else if (str === 'previous') {
      if (this.previousId) {
        this.linkSourt(this.previousId);
        // this.id = this.previousId;
      }
    }
  }

  onAddFiles() {
    let dialogRef = this.dialog.open(DialogSelectFilesComponent, {
      panelClass: 'app-dialog-extend',
      position: { top: '100px' },
      data: {
        id: this.id,
        fund: this.sourtForm.value.fund,
        list_no: this.sourtForm.value.list_no,
        file_no: this.sourtForm.value.file_no,
        file_no_ext: this.sourtForm.value.file_no_ext,
        file_no_ext_noneed:
          this.sourtForm.value.file_no_ext &&
          this.sourtForm.value.file_no_ext !== ''
            ? false
            : true,
        folder_files: this.folder_files,
        selectedFiles: [],
      },
    });
    dialogRef.afterClosed().subscribe(async (result) => {
      if (result && result !== false) {
        this.folder_files_to_add = [...result.selectedFiles];
      }
    });
  }

  openPdf(selectedArchivefolderfile) {
    this.dialog.open(DialogPdfViewerComponent, {
      panelClass: 'app-dialog-extend',
      data: {
        file: selectedArchivefolderfile,
      },
    });
  }

  async onShowImg(id, selectedArchivefolderfile) {
    if (
      this.globalService.isPdftype(
        selectedArchivefolderfile.archivefile.filename
      )
    ) {
      this.openPdf(selectedArchivefolderfile);
      return;
    }
    let archivefolderfiles: any[];
    let parchivefolderfiles: any[];
    let narchivefolderfiles: any[];
    this.lightboxImages.length = 0;
    let idx = this.folder_files_img.findIndex((item) => item.file_id === id);
    parchivefolderfiles = this.folder_files_img.slice(
      idx,
      this.imgBufsize + idx > this.folder_files_img.length
        ? this.folder_files_img.length
        : this.imgBufsize + idx
    );
    narchivefolderfiles = this.folder_files_img.slice(
      idx - this.imgBufsize > 0 ? idx - this.imgBufsize : 0,
      idx
    );
    archivefolderfiles = [...narchivefolderfiles, ...parchivefolderfiles];
    archivefolderfiles.sort((a, b) =>
      a.archivefile.filename > b.archivefile.filename ? 1 : -1
    );
    let fname: string;
    this.folder_files_img.map((item) => {
      if (item.file_id === id) {
        fname = item.archivefile.filename;
      }
    });
    this.spinnerService.show();
    this.inProgress = true;
    this.inProgressVal += 100 / archivefolderfiles.length;
    await Promise.all(
      archivefolderfiles.map((item) => {
        return new Promise(async (resolve) => {
          let resfile;
          if (
            this.globalService.isImgtype(item.archivefile.filename) === true ||
            this.globalService.isVideotype(item.archivefile.filename) ===
              true ||
            this.globalService.isAudiotype(item.archivefile.filename) === true
          ) {
            try {
              let file;
              let type;
              let ext;
              if (
                this.globalService.isImgtype(item.archivefile.filename) === true
              ) {
                type = 'image';
                file = await this.archivefolderfileService.getFilePreviewById(
                  item.file_id
                );
                resfile = new Blob([file], { type: 'image/jpg' });
                let reader = new FileReader();
                reader.readAsDataURL(resfile);
                reader.onloadend = () => {
                  let base64data = String(reader.result);
                  this.lightboxImages.push({
                    src: base64data,
                    caption: item.archivefile.filename,
                    id: item.file_id,
                    type,
                    ext,
                    orientation: item.orientation,
                  });
                  resolve(null);
                };
              } else if (
                this.globalService.isVideotype(item.archivefile.filename) ===
                true
              ) {
                type = 'video';
                ext = item.archivefile.filename
                  .toLowerCase()
                  .split('.')
                  .reverse()[0];
                this.lightboxImages.push({
                  src: this.archivefolderfileService.generateLink(
                    item.file_id,
                    item.jwt,
                    item.archivefile.storage
                  ),
                  caption: item.archivefile.filename,
                  id: item.file_id,
                  type,
                  ext,
                  orientation: item.orientation,
                });
                resolve(null);
              } else {
                type = 'audio';
                ext = item.archivefile.filename
                  .toLowerCase()
                  .split('.')
                  .reverse()[0];
                this.lightboxImages.push({
                  src: this.archivefolderfileService.generateLink(
                    item.file_id,
                    item.jwt,
                    item.archivefile.storage
                  ),
                  caption: item.archivefile.filename,
                  id: item.file_id,
                  type,
                  ext,
                  orientation: item.orientation,
                });
                resolve(null);
              }
              this.inProgressVal += 100 / archivefolderfiles.length;
            } catch (err) {
              resolve(null);
            }
          } else {
            resolve(null);
          }
        });
      })
    );
    this.spinnerService.hide();
    this.lightboxImages.sort((a, b) => (a.caption > b.caption ? 1 : -1));
    this.inProgress = false;
    setTimeout(() => {
      let idx = this.lightboxImages.findIndex((item) => item.caption === fname);
      this.lightbox.open(idx);
    }, 300);
  }

  async addPrevImg() {
    let firstId = this.lightboxImages[0].id;
    let idx = this.folder_files_img.findIndex(
      (item) => item.file_id === +firstId
    );
    if (idx === 0 || idx === -1) {
      return false;
    } else {
      this.spinnerService.show();
      this.galleryWait = true;
      let file;
      let type;
      let resfile;
      try {
        if (
          this.globalService.isImgtype(
            this.folder_files_img[idx - 1].archivefile.filename
          ) === true
        ) {
          type = 'image';
          file = await this.archivefolderfileService.getFilePreviewById(
            this.folder_files_img[idx - 1].file_id
          );
          resfile = new Blob([file], { type: 'image/jpg' });
          let reader = new FileReader();
          reader.readAsDataURL(resfile);
          reader.onloadend = () => {
            let base64data = String(reader.result);
            this.lightbox.unshiftImg({
              src: base64data,
              caption: this.folder_files_img[idx - 1].archivefile.filename,
              id: this.folder_files_img[idx - 1].file_id,
              type,
              orientation: this.folder_files_img[idx - 1].orientation,
            });
          };
        } else if (
          this.globalService.isVideotype(
            this.folder_files_img[idx - 1].archivefile.filename
          ) === true
        ) {
          type = 'video';
          let ext = this.folder_files_img[idx - 1].archivefile.filename
            .toLowerCase()
            .split('.')
            .reverse()[0];
          this.lightbox.unshiftImg({
            src: this.archivefolderfileService.generateLink(
              this.folder_files_img[idx - 1].file_id,
              this.folder_files_img[idx - 1].jwt,
              this.folder_files_img[idx - 1].archivefile.storage
            ),
            caption: this.folder_files_img[idx - 1].archivefile.filename,
            id: this.folder_files_img[idx - 1].file_id,
            type,
            ext,
            orientation: this.folder_files_img[idx - 1].orientation,
          });
        } else {
          type = 'audio';
          let ext = this.folder_files_img[idx - 1].archivefile.filename
            .toLowerCase()
            .split('.')
            .reverse()[0];
          this.lightbox.unshiftImg({
            src: this.archivefolderfileService.generateLink(
              this.folder_files_img[idx - 1].file_id,
              this.folder_files_img[idx - 1].jwt,
              this.folder_files_img[idx - 1].archivefile.storage
            ),
            caption: this.folder_files_img[idx - 1].archivefile.filename,
            id: this.folder_files_img[idx - 1].file_id,
            type,
            ext,
            orientation: this.folder_files_img[idx - 1].orientation,
          });
        }
      } catch {
        this.globalService.openSnackBar(
          `Ошибка при обработке изображения ${
            this.folder_files_img[idx - 1].archivefile.filename
          }`,
          'Ok'
        );
      } finally {
        this.spinnerService.hide();
        this.galleryWait = false;
      }
      return true;
    }
  }

  async addNextImg() {
    let lastId = +this.lightboxImages[this.lightboxImages.length - 1].id;
    let idx = this.folder_files_img.findIndex(
      (item) => item.file_id === +lastId
    );
    if (idx + 1 > this.folder_files_img.length - 1 || idx === -1) {
      return false;
    } else {
      this.spinnerService.show();
      this.galleryWait = true;
      let file;
      let type;
      let resfile;
      try {
        if (
          this.globalService.isImgtype(
            this.folder_files_img[idx + 1].archivefile.filename
          ) === true
        ) {
          type = 'image';
          file = await this.archivefolderfileService.getFilePreviewById(
            this.folder_files_img[idx + 1].file_id
          );
          resfile = new Blob([file], { type: 'image/jpg' });
          let reader = new FileReader();
          reader.readAsDataURL(resfile);
          reader.onloadend = () => {
            let base64data = String(reader.result);
            this.lightbox.pushImg({
              src: base64data,
              caption: this.folder_files_img[idx + 1].archivefile.filename,
              id: this.folder_files_img[idx + 1].file_id,
              type,
              orientation: this.folder_files_img[idx + 1].orientation,
            });
          };
        } else if (
          this.globalService.isVideotype(
            this.folder_files_img[idx + 1].archivefile.filename
          ) === true
        ) {
          type = 'video';
          let ext = this.folder_files_img[idx + 1].archivefile.filename
            .toLowerCase()
            .split('.')
            .reverse()[0];
          this.lightbox.pushImg({
            src: this.archivefolderfileService.generateLink(
              this.folder_files_img[idx + 1].file_id,
              this.folder_files_img[idx + 1].jwt,
              this.folder_files_img[idx + 1].archivefile.storage
            ),
            caption: this.folder_files_img[idx + 1].archivefile.filename,
            id: this.folder_files_img[idx + 1].file_id,
            type,
            ext,
            orientation: this.folder_files_img[idx + 1].orientation,
          });
        } else {
          type = 'audio';
          let ext = this.folder_files_img[idx + 1].archivefile.filename
            .toLowerCase()
            .split('.')
            .reverse()[0];
          this.lightbox.pushImg({
            src: this.archivefolderfileService.generateLink(
              this.folder_files_img[idx + 1].file_id,
              this.folder_files_img[idx + 1].jwt,
              this.folder_files_img[idx + 1].archivefile.storage
            ),
            caption: this.folder_files_img[idx + 1].archivefile.filename,
            id: this.folder_files_img[idx + 1].file_id,
            type,
            ext,
            orientation: this.folder_files_img[idx + 1].orientation,
          });
        }
      } catch (err) {
        console.error(err);
        this.globalService.openSnackBar(
          `Ошибка при обработке изображения ${
            this.folder_files_img[idx + 1].archivefile.filename
          }`,
          'Ok'
        );
      } finally {
        this.spinnerService.hide();
        this.galleryWait = false;
      }
      return true;
    }
  }

  async previewChange(e) {
    if (e.forward === false) {
      await this.addPrevImg();
    } else {
      await this.addNextImg();
    }
  }

  onDelFileById(idx) {
    this.folder_files_to_add.splice(idx, 1);
  }

  onDelFiles() {
    this.folder_files_to_add.length = 0;
  }

  toggleCheckToDel(idx) {
    this.folder_files[idx].checkToDel = !this.folder_files[idx].checkToDel;
  }

  async onDownloadArchivefolderfile(id) {
    this.spinnerService.show();
    let fileName = '';
    for (let i = 0; i < this.folder_files.length; i++) {
      if (this.folder_files[i]) {
        if (id === this.folder_files[i].file_id) {
          fileName = this.folder_files[i].archivefile.filename;
        }
      }
    }
    try {
      let blob = this.archivefolderfileService.downloadOneById(id);
      this.inProgress = true;
      this.inProgressVal = 0;
      blob.subscribe((event) => {
        if (event.type === HttpEventType.DownloadProgress) {
          const percentDone = Math.round((100 * event.loaded) / event.total);
          this.inProgressVal = percentDone;
        } else if (event instanceof HttpResponse) {
          this.inProgress = false;
          this.inProgressVal = 0;
          this.spinnerService.hide();
          importedSaveAs(event.body, fileName);
        }
      });
    } catch (err) {
      console.error(`Ошибка ${err}`);
      this.spinnerService.hide();
      this.inProgress = false;
      this.inProgressVal = 0;
      this.globalService.hasError.next(false);
    }
  }

  async onDel() {
    let dialogRef = this.dialog.open(DialogYesNoComponent, {
      data: {
        title: 'Удалить документ?',
        question: ``,
      },
    });
    dialogRef.afterClosed().subscribe(async (result) => {
      if (result === true) {
        this.spinnerService.show();
        try {
          await this.sourtService.deleteOneById(this.id);
          this.getPublicSiteActionType();
          this.router.navigate(['sys/person/doclist']);
          this.globalService.openSnackBar(`Удалено.`, 'Ok');
        } catch (err) {
          console.error(`Ошибка при удалении ${err}`);
          this.globalService.openSnackBar(`Ошибка ${err.status}`, 'Ok');
        } finally {
          this.spinnerService.hide();
        }
      }
    });
  }

  linkList() {
    this.router.navigate(['sys/person/doclist']);
  }

  async checkDelo() {
    if (this.sourtForm.value.file_no > 99999) {
      this.sourtForm.patchValue({
        file_no: +`${this.sourtForm.value.file_no}`.substr(0, 5),
      });
    }
    if (this.sourtForm.value.list_no > 9999) {
      this.sourtForm.patchValue({
        list_no: +`${this.sourtForm.value.list_no}`.substr(0, 4),
      });
    }
    if (this.sourtForm.value.fund > 9999) {
      this.sourtForm.patchValue({
        fund: +`${this.sourtForm.value.fund}`.substr(0, 4),
      });
    }
    clearTimeout(this.deloTimer);
    if (this.isFondopisdeloValid()) {
      this.deloTimer = setTimeout(async () => {
        try {
          let doc = this.archivefolderService.getWithOpis_data(
            this.sourtForm.value.file_no,
            this.sourtForm.value.fund,
            this.sourtForm.value.list_no
          );
          this.delo = !(await doc) ? null : (await doc)[0];
        } catch (err) {
          this.checkDelo = null;
        } finally {
        }
      }, 400);
    } else {
      this.delo = null;
    }
  }

  isFondopisdeloValid(): boolean {
    let res = true;
    if (
      isNaN(parseFloat(this.sourtForm.value.fund)) ||
      isNaN(parseFloat(this.sourtForm.value.list_no)) ||
      isNaN(parseFloat(this.sourtForm.value.file_no))
    ) {
      res = false;
    }
    return res;
  }

  linkDocument(id) {
    this.router.navigate(['sys/person/doc', id]);
  }

  linkDelo() {
    this.router.navigate(['sys/arch/edit', this.delo.id]);
  }

  linkPersonal(id) {
    this.router.navigate(['sys/person/personaltedit', id]);
  }

  async canDeactivate() {
    if (this.hasChanges() === true) {
      let dialogRef = this.dialog.open(DialogSaveNoComponent, {
        data: {
          title: 'Сохранить изменения?',
          question: ``,
        },
      });
      let res = await new Promise((resolve, reject) => {
        dialogRef.afterClosed().subscribe(async (result) => {
          if (result === true) {
            let _res = await this.onSave(true, false, false);
            if (_res === true) {
              resolve(true);
            } else {
              resolve(false);
            }
          } else if (result === false) {
            resolve(true);
          } else {
            resolve(false);
          }
        });
      });
      return res;
    }
    return true;
  }

  hasChanges(): boolean {
    let res = this.sourtForm.dirty && this.sourtForm.touched;

    if (this.folder_files_to_add && this.folder_files_to_add.length > 0) {
      res = true;
    }

    let filesToDel = [];
    if (this.folder_files) {
      this.folder_files.forEach((file_to_del) => {
        if (file_to_del.checkToDel === true) {
          let res = this.sourfiletService.deleteOneById(file_to_del.link_id);
          filesToDel.push(res);
        }
      });
    }

    if (filesToDel && filesToDel.length > 0) {
      res = true;
    }

    return res;
  }

  joinPersonal() {
    let dialogRef = this.dialog.open(DialogJoinPersonalComponent, {
      data: {
        fund: this.sourtForm.value.fund,
        list_no: this.sourtForm.value.list_no,
        file_no: this.sourtForm.value.file_no,
      },
      panelClass: 'app-dialog-extend',
    });
    dialogRef.afterClosed().subscribe(async (result) => {
      if (result) {
        let toAdd = [];
        result.forEach((p) => {
          let d;
          if (this.sourt.personals) {
            d = this.sourt.personals.find((item) => item.code == p.code);
          }
          if (!d) {
            toAdd.push(p);
          }
        });
        try {
          await Promise.all(
            toAdd.map((item) =>
              this.personalsourtService.postOne({
                sour_id: this.id,
                personal_code: item.code,
              })
            )
          );
          toAdd.forEach((element) => {
            this.sourt.personals.unshift(element);
          });
          this.sourt.personals.sort((a, b) => a.code - b.code);
        } catch (e) {}
      }
    });
  }

  unlinkPersonal(link_id, e: MouseEvent) {
    e.stopPropagation();
    let dialogRef = this.dialog.open(DialogYesNoComponent, {
      data: {
        title: 'Отсоединить источник?',
        question: ``,
      },
    });
    dialogRef.afterClosed().subscribe(async (result) => {
      if (result === true) {
        this.spinnerService.show();
        try {
          await this.personalsourtService.deleteOneById(link_id);
          this.openSnackBar(`Отсоединено.`, 'Ok');
          await this.getData();
        } catch (e) {
        } finally {
          this.spinnerService.hide();
        }
      }
    });
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
    });
  }

  onLinkSourt(id) {
    this.router.navigate(['/sys/person/doc', id]);
  }

  onLinkDocuments() {
    this.filtersService.selectionSourtFilter = new SourtFilter();
    this.filtersService.selectionSourtFilter.fund =
      this.sourtForm.getRawValue().fund;
    this.filtersService.selectionSourtFilter.list_no =
      this.sourtForm.getRawValue().list_no;

    this.filtersService.selectionSourtFilter.file_no =
      this.sourtForm.getRawValue().file_no;
    this.filtersService.selectionSourtFilter.file_no_ext =
      this.sourtForm.getRawValue().file_no_ext;
    if (
      !this.sourtForm.getRawValue().file_no_ext ||
      this.sourtForm.getRawValue().file_no_ext === ''
    ) {
      this.filtersService.selectionSourtFilter.file_no_ext_noneed = true;
    }
    this.router.navigate(['sys', 'person', 'doclist']);
  }

  copyFilename(file) {
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = file.archivefile.file;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
  }

  toggleCheckToDelLink(id, sourtlink) {
    if (sourtlink.link_type === 1 && sourtlink.checkToDel !== true) {
      let dialogRef = this.dialog.open(DialogYesNoComponent, {
        data: {
          title:
            'Разрывая данную связь вы разрываете все связи типа "дубликат" у целевого документа. Продолжить?',
          question: ``,
        },
      });
      dialogRef.afterClosed().subscribe(async (result) => {
        if (result === true) {
          this.sourtlinks[id].checkToDel = !this.sourtlinks[id].checkToDel;
        }
      });
    } else {
      this.sourtlinks[id].checkToDel = !this.sourtlinks[id].checkToDel;
    }
  }

  async onAddsourtlink(result) {
    let id = result.id;
    let l = this.sourtlinks.find(
      (link) => link.sour_t_id2 === id || link.sour_t_id1 === id
    );
    if (!l) {
      this.links.push({ val: result, link_type: 1 }); // link_type
      this.sourts = this.sourts.filter((item) => item.id !== result.id);
    } else {
      this.openSnackBar(`Связь с делом уже установлена`, 'Ok');
    }
    this.myControl.patchValue({});
  }

  inputLink() {
    clearTimeout(this.timerId);
    this.timerId = setTimeout(async () => {
      this.sourts = await this.sourtService.getShortFilter(
        100,
        this.myControl.value
      );

      if (this.sourts) {
        try {
          this.sourts = this.sourts.filter((sourt) => sourt.id !== this.id);
          this.links.map((link) => {
            this.sourts = this.sourts.filter(
              (sourt) => sourt.id !== link.val.id
            );
          });
          this.filteredsourts = this.myControl.valueChanges.pipe(
            startWith<string | SourT>(''),
            map((value) =>
              typeof value === 'string'
                ? value
                : !value
                ? null
                : value.sour_name
            ),
            map((name) => (name ? this._filter(name) : [...this.sourts]))
          );
        } catch {}
      }
    }, 500);
  }

  displayFn(user?: SourT): string | undefined {
    return user ? user.sour_name : undefined;
  }

  setDubl() {
    let dialogRef = this.dialog.open(DialogSetDocumentParentComponent, {
      panelClass: 'app-dialog-extend',
      data: {
        selectedSourt: {},
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.onAddsourtlink(result);
      }
    });
  }
  removeParents() {
    this.sourtForm.patchValue({ parent_id: null });
    this.parentSourt = null;
  }
  setParents() {
    let dialogRef = this.dialog.open(DialogSetDocumentParentComponent, {
      panelClass: 'app-dialog-extend',
      data: {
        selectedSourt: this.parentSourt || {},
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.sourtForm.patchValue({ parent_id: result.id });
        this.parentSourt = result;
      }
    });
  }
}
