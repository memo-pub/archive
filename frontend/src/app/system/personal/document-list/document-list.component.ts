import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem,
} from '@angular/cdk/drag-drop';
import { Component, OnInit } from '@angular/core';
import { NgModel } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ListOfT } from 'src/app/shared/models/listoft.model';
import { SourT } from 'src/app/shared/models/sourt.model';
import { AuthService } from 'src/app/shared/services/auth.service';
import { DateparseService } from 'src/app/shared/services/dateparse.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { DialogSetpositionComponent } from '../../shared/components/dialog-setposition/dialog-setposition.component';
import { DiskinfoService } from '../../shared/services/diskinfo.service';
import { FiltersService } from '../../shared/services/filters.service';
import { ListoftService } from '../../shared/services/listoft.service';
import { MoveordernomService } from '../../shared/services/moveordernom.service';
import { SourtService } from '../../shared/services/sourt.service';
import { Muser } from 'src/app/shared/models/muser.model';

@Component({
  selector: 'app-document-list',
  templateUrl: './document-list.component.html',
  styleUrls: ['./document-list.component.css'],
})
export class DocumentListComponent implements OnInit {
  sortEnable = false;
  sourts: SourT[];
  sour_types: ListOfT[];
  total = 0;
  limit = 100;

  postfixId = '';
  postfixPos = '';
  postfixDate = '';

  searchflds: any[];
  loggedInUser: Muser;

  selection: number[] = [];

  constructor(
    public authService: AuthService,
    public dateparseService: DateparseService,
    public globalService: GlobalService,
    public filtersService: FiltersService,
    private listoftService: ListoftService,
    private diskinfoService: DiskinfoService,
    private sourtService: SourtService,
    private moveordernomService: MoveordernomService,
    private router: Router,
    private dialog: MatDialog,
    private spinnerService: NgxSpinnerService,
    public snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.setOrgerStr();
    this.getData();
    this.loggedInUser = this.authService.getCurUser();
    this.filtersService.sourtPlusOne.fund = null;
    this.filtersService.sourtPlusOne.list_no = null;
    this.filtersService.sourtPlusOne.file_no = null;
    this.filtersService.sourtPlusOne.file_no_ext = null;
    this.filtersService.sourtPlusOne.sour_no = null;
    this.searchflds = this.globalService.sourtSearchflds;
  }

  async getData() {
    this.spinnerService.show();
    try {
      let sour_types = this.listoftService.getWithFilter('SOUR_TYPE');
      let length = this.sourtService.getFilter_count(
        this.filtersService.selectionSourtFilter.fund,
        this.filtersService.selectionSourtFilter.list_no,
        this.filtersService.selectionSourtFilter.file_no,
        this.filtersService.selectionSourtFilter.file_no_ext,
        this.filtersService.selectionSourtFilter.file_no_ext_noneed,
        this.filtersService.selectionSourtFilter.sour_name_filter,
        this.filtersService.selectionSourtFilter.sour_type_filter,
        this.filtersService.selectionSourtFilter.pub_type,
        this.filtersService.selectionSourtFilter.filter_sour_date_from,
        this.filtersService.selectionSourtFilter.filter_sour_date_to,
        this.filtersService.selectionSourtFilter.searchtxt,
        this.filtersService.selectionSourtFilter.searchflds
      );
      this.sour_types = !(await sour_types) ? [] : await sour_types;
      this.sour_types.sort((a, b) => {
        if (a.name_ent < b.name_ent) {
          return -1;
        }
        if (a.name_ent > b.name_ent) {
          return 1;
        }
        return 0;
      });
      this.total = (await length).rowcount;
      if (this.authService.getCurUser().isadmin) {
        let sizes = await this.diskinfoService.getAll();
        if (Array.isArray(sizes)) {
          for (let i = 0; i < sizes.length; i++) {
            if (!sizes[i].error) {
              if (!window.localStorage.getItem(sizes[i].mount)) {
                if (+sizes[i].available.split('MB')[0] < 20000) {
                  this.openSnackBar(
                    `Осталось менее 20гб в папке ${sizes[i].mount}`,
                    'Ok'
                  );
                  window.localStorage.setItem(sizes[i].mount, '20');
                  continue;
                }
                if (+sizes[i].available.split('MB')[0] < 10000) {
                  this.openSnackBar(
                    `Осталось менее 10гб в папке ${sizes[i].mount}`,
                    'Ok'
                  );
                  window.localStorage.setItem(sizes[i].mount, '10');
                  continue;
                }
                if (+sizes[i].available.split('MB')[0] < 5000) {
                  this.openSnackBar(
                    `Осталось менее 5гб в папке ${sizes[i].mount}`,
                    'Ok'
                  );
                  window.localStorage.setItem(sizes[i].mount, '5');
                  continue;
                }
              } else {
                if (
                  +window.localStorage.getItem(sizes[i].mount) <= 10 &&
                  +sizes[i].available.split('MB')[0] > 10000
                ) {
                  window.localStorage.clearItem(sizes[i].mount);
                  continue;
                }
                if (
                  +window.localStorage.getItem(sizes[i].mount) <= 5 &&
                  +sizes[i].available.split('MB')[0] > 5000
                ) {
                  window.localStorage.clearItem(sizes[i].mount);
                  continue;
                }
                if (
                  +window.localStorage.getItem(sizes[i].mount) > 10 &&
                  +sizes[i].available.split('MB')[0] < 10000
                ) {
                  this.openSnackBar(
                    `Осталось менее 10гб в папке ${sizes[i].mount}`,
                    'Ok'
                  );
                  window.localStorage.setItem(sizes[i].mount, '10');
                  continue;
                }
                if (
                  +window.localStorage.getItem(sizes[i].mount) > 5 &&
                  +sizes[i].available.split('MB')[0] < 5000
                ) {
                  this.openSnackBar(
                    `Осталось менее 5гб в папке ${sizes[i].mount}`,
                    'Ok'
                  );
                  window.localStorage.setItem(sizes[i].mount, '5');
                  continue;
                }
              }
            }
          }
        }
      }
      await this.updatePage();
    } catch (err) {
      this.globalService.exceptionHandling(err);
    } finally {
      this.spinnerService.hide();
    }
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 100000,
    });
  }

  async checkEmpty() {
    if (
      !this.filtersService.selectionSourtFilter.file_no_ext ||
      this.filtersService.selectionSourtFilter.file_no_ext === ''
    ) {
      if (
        this.filtersService.selectionSourtFilter.fund &&
        this.filtersService.selectionSourtFilter.fund !== '' &&
        this.filtersService.selectionSourtFilter.list_no &&
        this.filtersService.selectionSourtFilter.list_no !== '' &&
        this.filtersService.selectionSourtFilter.file_no &&
        this.filtersService.selectionSourtFilter.file_no !== ''
      ) {
        try {
          let length = this.sourtService.getFilter_count_check(
            this.filtersService.selectionSourtFilter.fund,
            this.filtersService.selectionSourtFilter.list_no,
            this.filtersService.selectionSourtFilter.file_no
          );
          let total = (await length).rowcount;
          if (+total === 0) {
            this.filtersService.selectionSourtFilter.file_no_ext_noneed = true;
          } else {
            this.filtersService.selectionSourtFilter.file_no_ext_noneed = false;
          }
        } catch (err) {}
      }
    }
  }

  async updatePage(check = false) {
    this.selection.length = 0;
    // this.filtersService.selectionSourtFilter.page = 1;
    if (check) {
      await this.checkEmpty();
    }
    this.sortEnable =
      this.filtersService.selectionSourtFilter.fund &&
      this.filtersService.selectionSourtFilter.fund !== '' &&
      this.filtersService.selectionSourtFilter.list_no &&
      this.filtersService.selectionSourtFilter.list_no !== '' &&
      this.filtersService.selectionSourtFilter.file_no &&
      this.filtersService.selectionSourtFilter.file_no !== '' &&
      ((this.filtersService.selectionSourtFilter.file_no_ext &&
        this.filtersService.selectionSourtFilter.file_no_ext !== '') ||
        this.filtersService.selectionSourtFilter.file_no_ext_noneed) &&
      this.filtersService.selectionSourtFilter.searchOrder === 'bypos';
    // this.sortEnable = true;
    let start =
      this.limit * (this.filtersService.selectionSourtFilter.page - 1);
    try {
      this.spinnerService.show();
      let length = this.sourtService.getFilter_count(
        this.filtersService.selectionSourtFilter.fund,
        this.filtersService.selectionSourtFilter.list_no,
        this.filtersService.selectionSourtFilter.file_no,
        this.filtersService.selectionSourtFilter.file_no_ext,
        this.filtersService.selectionSourtFilter.file_no_ext_noneed,
        this.filtersService.selectionSourtFilter.sour_name_filter,
        this.filtersService.selectionSourtFilter.sour_type_filter,
        this.filtersService.selectionSourtFilter.pub_type,
        this.filtersService.selectionSourtFilter.filter_sour_date_from,
        this.filtersService.selectionSourtFilter.filter_sour_date_to,
        this.filtersService.selectionSourtFilter.searchtxt,
        this.filtersService.selectionSourtFilter.searchflds
      );
      this.total = (await length).rowcount;
      let sourts = this.sourtService.getFilter(
        this.filtersService.selectionSourtFilter.searchOrder,
        start,
        this.limit,
        this.filtersService.selectionSourtFilter.fund,
        this.filtersService.selectionSourtFilter.list_no,
        this.filtersService.selectionSourtFilter.file_no,
        this.filtersService.selectionSourtFilter.file_no_ext,
        this.filtersService.selectionSourtFilter.file_no_ext_noneed,
        this.filtersService.selectionSourtFilter.sour_name_filter,
        this.filtersService.selectionSourtFilter.sour_type_filter,
        this.filtersService.selectionSourtFilter.pub_type,
        this.filtersService.selectionSourtFilter.filter_sour_date_from,
        this.filtersService.selectionSourtFilter.filter_sour_date_to,
        this.filtersService.selectionSourtFilter.searchtxt,
        this.filtersService.selectionSourtFilter.searchflds
      );
      this.sourts = !(await sourts) ? [] : await sourts;
    } catch (err) {
      console.error(`Ошибка ${err}`);
    } finally {
      this.spinnerService.hide();
    }
  }

  onAdd() {
    this.router.navigate(['sys/person/doc']);
  }

  getPubtype(id) {
    let r = this.globalService.pubTypes.find((item) => item.id === id);
    return r ? r.value : '';
  }

  async drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    }
    this.spinnerService.show();
    try {
      let idx = event.currentIndex > event.previousIndex ? -1 : 1;
      let newArr = await this.moveordernomService.move(
        'sour_t',
        this.sourts[event.currentIndex].id,
        this.sourts[event.currentIndex + idx].sour_no
      );
      this.updatePage();
    } catch (err) {
      this.globalService.exceptionHandling(err);
    } finally {
      this.spinnerService.hide();
    }
  }

  async selectOrder(type: string) {
    switch (type) {
      case 'byid':
        this.filtersService.selectionSourtFilter.searchOrder =
          this.filtersService.selectionSourtFilter.searchOrder === 'byid'
            ? 'byiddesc'
            : 'byid';
        break;
      case 'bypos':
        this.filtersService.selectionSourtFilter.searchOrder =
          this.filtersService.selectionSourtFilter.searchOrder === 'bypos'
            ? 'byposdesc'
            : 'bypos';
        break;
      case 'bysour_date':
        this.filtersService.selectionSourtFilter.searchOrder =
          this.filtersService.selectionSourtFilter.searchOrder === 'bysour_date'
            ? 'bysour_date_desc'
            : 'bysour_date';
        break;
    }
    this.filtersService.selectionSourtFilter.page = 1;
    this.setOrgerStr();
    this.updatePage();
  }

  setOrgerStr() {
    switch (this.filtersService.selectionSourtFilter.searchOrder) {
      case 'byid':
        this.postfixId =
          '<i class="fa fa-sort-amount-asc" aria-hidden="true"></i>';
        this.postfixPos = '';
        this.postfixDate = '';
        break;
      case 'byiddesc':
        this.postfixId =
          '<i class="fa fa-sort-amount-desc" aria-hidden="true"></i>';
        this.postfixPos = '';
        this.postfixDate = '';
        break;
      case 'bypos':
        this.postfixId = '';
        this.postfixDate = '';
        this.postfixPos =
          '<i class="fa fa-sort-amount-asc" aria-hidden="true"></i>';
        break;
      case 'byposdesc':
        this.postfixId = '';
        this.postfixDate = '';
        this.postfixPos =
          '<i class="fa fa-sort-amount-desc" aria-hidden="true"></i>';
        break;
      case 'bysour_date':
        this.postfixId = '';
        this.postfixDate =
          '<i class="fa fa-sort-amount-asc" aria-hidden="true"></i>';
        this.postfixPos = '';
        break;
      case 'bysour_date_desc':
        this.postfixId = '';
        this.postfixDate =
          '<i class="fa fa-sort-amount-desc" aria-hidden="true"></i>';
        this.postfixPos = '';
        break;
    }
  }

  linkSourt(id) {
    this.router.navigate(['sys/person/doc', id]);
  }

  stop(e) {
    e.stopPropagation();
  }

  goToPage(n: number): void {
    this.filtersService.selectionSourtFilter.page = n;
    this.updatePage();
  }

  onNext(): void {
    this.filtersService.selectionSourtFilter.page++;
    this.updatePage();
  }

  onPrev(): void {
    this.filtersService.selectionSourtFilter.page = 1;
    this.updatePage();
  }

  onNext10(n = 1): void {
    this.filtersService.selectionSourtFilter.page += 10 * n;
    this.updatePage();
  }

  onPrev10(n = 1): void {
    this.filtersService.selectionSourtFilter.page -= 10 * n;
    this.updatePage();
  }

  masterToggle() {
    let allSelected = this.isAllSelected();
    for (let i = 0; i < this.sourts.length; i++) {
      let pos = this.selection.findIndex((item) => item === this.sourts[i].id);
      if (pos === -1) {
        if (allSelected === false) {
          this.selection.push(this.sourts[i].id);
        }
      } else {
        if (allSelected === true) {
          this.selection.splice(pos, 1);
        }
      }
    }
  }

  toggleSelection(id) {
    let pos = this.selection.findIndex((item) => item === id);
    if (pos === -1) {
      this.selection.push(id);
    } else {
      this.selection.splice(pos, 1);
    }
  }

  isAllSelected(): boolean {
    if (!this.sourts || this.sourts.length === 0) {
      return false;
    }
    let res = true;
    for (let i = 0; i < this.sourts.length; i++) {
      if (
        this.selection.findIndex((item) => item === this.sourts[i].id) === -1
      ) {
        res = false;
      }
    }
    return res;
  }

  isSelected(id): boolean {
    return this.selection.findIndex((item) => item === id) !== -1;
  }

  async onDelSelected() {
    this.spinnerService.show();
    try {
      await this.sourtService.deleteSelectionitem({
        sour_ids: this.selection,
      });
      this.updatePage();
    } catch (err) {
      this.globalService.exceptionHandling(err);
    } finally {
      this.spinnerService.hide();
    }
  }

  deselectAll(select: NgModel, select1?: NgModel) {
    select.update.emit([]);
    if (select1) {
      select1.update.emit([]);
    }
  }

  onMove(e: MouseEvent, sourt) {
    e.stopPropagation();
    let dialogRef = this.dialog.open(DialogSetpositionComponent, {
      panelClass: 'app-dialog-base',
      data: {
        describtion: `Укажите новую позицию`,
        min: 1,
        max: 1000,
      },
    });
    dialogRef.afterClosed().subscribe(async (result) => {
      if (result) {
        try {
          await this.moveordernomService.move('sour_t', sourt.id, result);
          await this.updatePage();
        } catch (err) {
          this.globalService.exceptionHandling(err);
        } finally {
          this.spinnerService.hide();
        }
      }
    });
  }

  set_date_filter_from() {
    if (this.filtersService.selectionSourtFilter.filter_sour_date_from_date) {
      let offset = new Date(
        this.filtersService.selectionSourtFilter.filter_sour_date_from_date
      ).getTimezoneOffset();
      let d = new Date(
        new Date(
          this.filtersService.selectionSourtFilter.filter_sour_date_from_date
        ).getTime() -
          offset * 60 * 1000
      );
      this.filtersService.selectionSourtFilter.filter_sour_date_from = d
        .toISOString()
        .slice(0, 10);
    } else {
      this.filtersService.selectionSourtFilter.filter_sour_date_from = null;
    }
  }
  set_date_filter_to() {
    if (this.filtersService.selectionSourtFilter.filter_sour_date_to_date) {
      let offset = new Date(
        this.filtersService.selectionSourtFilter.filter_sour_date_to_date
      ).getTimezoneOffset();
      let d = new Date(
        new Date(
          this.filtersService.selectionSourtFilter.filter_sour_date_to_date
        ).getTime() -
          offset * 60 * 1000
      );
      this.filtersService.selectionSourtFilter.filter_sour_date_to = d
        .toISOString()
        .slice(0, 10);
    } else {
      this.filtersService.selectionSourtFilter.filter_sour_date_to = null;
    }
  }
}
