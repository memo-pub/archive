import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { PersonalT } from 'src/app/shared/models/personalt.model';
import { GlobalService } from 'src/app/shared/services/global.service';
import { AuthService } from 'src/app/shared/services/auth.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { isNullOrUndefined } from 'util';
import { PersonaltService } from '../../shared/services/personalt.service';
import { FiltersService } from '../../shared/services/filters.service';
import { DateparseService } from 'src/app/shared/services/dateparse.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DiskinfoService } from '../../shared/services/diskinfo.service';

@Component({
  selector: 'app-personal-list',
  templateUrl: './personal-list.component.html',
  styleUrls: ['./personal-list.component.css'],
})
export class PersonalListComponent implements OnInit {
  // searchOrder = 'bycode';
  // searchStr = '';
  // searchStr_fond = '';
  // searchStr_opis = '';
  // searchStr_delo = '';

  viewOnly: boolean; // тип интерфейса

  postfixCode = '';
  postfixSurname = '';
  postfixDelo = '';

  // loggedInUser: Muser;
  personals: PersonalT[];

  // MatPaginator Inputs
  // length = 100;
  // pageSize = 100;
  // pageSizeOptions: number[] = [5, 10, 25, 100];
  // pageEvent: PageEvent;
  start = 0;
  end = 0;
  // @ViewChild(MatPaginator) paginator: MatPaginator;

  // aditStr = 'Редактировать';

  /**Для пагинатора */
  total = 0;
  // page = 1;
  limit = 100;

  // filterExact = false;

  // @HostListener('window:resize', ['$event'])
  // onResize(event) {
  //   this.aditStr =
  //     event.target.innerWidth < 700
  //       ? '<i class="fa fa-pencil" aria-hidden="true"></i>'
  //       : 'Редактировать';
  // }

  constructor(
    public filtersService: FiltersService,
    private globalService: GlobalService,
    private authService: AuthService,
    private personaltService: PersonaltService,
    private router: Router,
    public dateparseService: DateparseService,
    private spinnerService: NgxSpinnerService,
    public snackBar: MatSnackBar,
    private diskinfoService: DiskinfoService
  ) {}

  async ngOnInit() {
    // this.aditStr =
    //   window.screen.width < 700
    //     ? '<i class="fa fa-pencil" aria-hidden="true"></i>'
    //     : 'Редактировать';
    this.viewOnly = this.authService.getCurUser().usertype === 4;
    try {
      // let loggedInUser = this.muserService.getOneById(this.authService.getCurUser().id);
      // this.page = this.globalService.personaltlistPage;
      // this.filterExact = this.globalService.personaltlistFilterExact;
      // this.searchOrder = isNullOrUndefined(
      //   this.globalService.personaltlistFilter
      // )
      //   ? []
      //   : this.globalService.personaltlistFilter.searchOrder;
      // let personals = this.personaltService.getAll();
      // this.loggedInUser = await loggedInUser;
      // if (!isNullOrUndefined(this.pageEvent)) {
      //   this.pageEvent.pageIndex = 0;
      // }
      // this.page = 1;
      this.filtersService.personalTListFilter.newFond = null;
      this.filtersService.personalTListFilter.newOpis = null;
      // let length = this.personaltService.getAll_count();
      let length = this.personaltService.getShortfilterOpisFondDelo_count(
        this.filtersService.personalTListFilter.searchStr,
        this.filtersService.personalTListFilter.searchStr_fond,
        this.filtersService.personalTListFilter.searchStr_opis,
        this.filtersService.personalTListFilter.searchStr_delo,
        this.filtersService.personalTListFilter.searchStr_delo_ext,
        this.filtersService.personalTListFilter.filterExact,
        this.filtersService.personalTListFilter.filterExactExt
      );
      let start =
        this.limit * (this.filtersService.personalTListFilter.page - 1);
      // let personals = this.personaltService.getWithOffsetLimit(
      //   start,
      //   this.limit
      // );
      this.total = (await length).rowcount;
      // this.personals = isNullOrUndefined(await personals)
      //   ? []
      //   : await personals;
      this.end = this.limit;
      await this.setOrgerStr();
      await this.updatePage();
    } catch (err) {
      if (err.status === 401) {
        console.error(`Ошибка авторизации`);
        this.authService.logout();
      } else {
        console.error(`Ошибка ${err}`);
      }
    } finally {
      this.spinnerService.hide();
    }
    let obj = {
      scroll: 0,
      selectedOrgt: false,
      selectedActivityt: false,
      selectedRepresst: false,
      selectedHelp: false,
      selectedSourt: false,
      selectedUsaget: false,
      selectedMentioned: false,
    };
    this.globalService.personal_edit = obj;
    let obj2 = {
      scroll: 0,
      selectedCourtt: false,
    };
    this.globalService.represst_edit = obj2;
    let obj1 = {
      scroll: 0,
      selectedImprist: false,
    };
    this.globalService.courtt_edit = obj1;
    if (this.authService.getCurUser().isadmin) {
      let sizes = await this.diskinfoService.getAll();
      if (Array.isArray(sizes)) {
        for (let i = 0; i < sizes.length; i++) {
          if (!sizes[i].error) {
            if (!window.localStorage.getItem(sizes[i].mount)) {
              if (+sizes[i].available.split('MB')[0] < 20000) {
                this.openSnackBar(
                  `Осталось менее 20гб в папке ${sizes[i].mount}`,
                  'Ok'
                );
                window.localStorage.setItem(sizes[i].mount, '20');
                continue;
              }
              if (+sizes[i].available.split('MB')[0] < 10000) {
                this.openSnackBar(
                  `Осталось менее 10гб в папке ${sizes[i].mount}`,
                  'Ok'
                );
                window.localStorage.setItem(sizes[i].mount, '10');
                continue;
              }
              if (+sizes[i].available.split('MB')[0] < 5000) {
                this.openSnackBar(
                  `Осталось менее 5гб в папке ${sizes[i].mount}`,
                  'Ok'
                );
                window.localStorage.setItem(sizes[i].mount, '5');
                continue;
              }
            } else {
              if (
                +window.localStorage.getItem(sizes[i].mount) <= 10 &&
                +sizes[i].available.split('MB')[0] > 10000
              ) {
                window.localStorage.clearItem(sizes[i].mount);
                continue;
              }
              if (
                +window.localStorage.getItem(sizes[i].mount) <= 5 &&
                +sizes[i].available.split('MB')[0] > 5000
              ) {
                window.localStorage.clearItem(sizes[i].mount);
                continue;
              }
              if (
                +window.localStorage.getItem(sizes[i].mount) > 10 &&
                +sizes[i].available.split('MB')[0] < 10000
              ) {
                this.openSnackBar(
                  `Осталось менее 10гб в папке ${sizes[i].mount}`,
                  'Ok'
                );
                window.localStorage.setItem(sizes[i].mount, '10');
                continue;
              }
              if (
                +window.localStorage.getItem(sizes[i].mount) > 5 &&
                +sizes[i].available.split('MB')[0] < 5000
              ) {
                this.openSnackBar(
                  `Осталось менее 5гб в папке ${sizes[i].mount}`,
                  'Ok'
                );
                window.localStorage.setItem(sizes[i].mount, '5');
                continue;
              }
            }
          }
        }
      }
    }
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 100000,
    });
  }

  async updateData() {
    this.filtersService.personalTListFilter.page = 1;
    // this.globalService.personaltlistFilterExact = this.filterExact;
    let start = this.limit * (this.filtersService.personalTListFilter.page - 1);
    if (
      this.filtersService.personalTListFilter.searchStr.length +
        this.filtersService.personalTListFilter.searchStr_fond.length +
        this.filtersService.personalTListFilter.searchStr_opis.length +
        this.filtersService.personalTListFilter.searchStr_delo.length +
        this.filtersService.personalTListFilter.searchStr_delo_ext.length >
      0
    ) {
      this.spinnerService.show();
      this.start = 0;
      // this.end = this.paginator.pageSize;
      this.end = this.limit;
      // this.paginator.firstPage();

      try {
        let length = this.personaltService.getShortfilterOpisFondDelo_count(
          this.filtersService.personalTListFilter.searchStr,
          this.filtersService.personalTListFilter.searchStr_fond,
          this.filtersService.personalTListFilter.searchStr_opis,
          this.filtersService.personalTListFilter.searchStr_delo,
          this.filtersService.personalTListFilter.searchStr_delo_ext,
          this.filtersService.personalTListFilter.filterExact,
          this.filtersService.personalTListFilter.filterExactExt
        );
        // let length = this.personaltService.getShortfilter_count(this.searchStr);
        let personals =
          this.personaltService.getWithShortfilterOpisFondDeloOffsetLimitOrder(
            this.filtersService.personalTListFilter.searchStr,
            start,
            // this.paginator.pageSize,
            this.limit,
            this.filtersService.personalTListFilter.searchOrder,
            this.filtersService.personalTListFilter.searchStr_fond,
            this.filtersService.personalTListFilter.searchStr_opis,
            this.filtersService.personalTListFilter.searchStr_delo,
            this.filtersService.personalTListFilter.searchStr_delo_ext,
            this.filtersService.personalTListFilter.filterExact,
            this.filtersService.personalTListFilter.filterExactExt
          );
        // this.personaltService.getWithShortfilterOffsetLimitOrder(this.searchStr, 0, this.paginator.pageSize, this.searchOrder);
        // this.length = (await length).rowcount;
        this.total = (await length).rowcount;
        this.personals = isNullOrUndefined(await personals)
          ? []
          : await personals;
      } catch (err) {
        console.error(err);
      } finally {
        this.spinnerService.hide();
      }
    } else {
      // this.start = this.paginator.pageSize * this.paginator.pageIndex;
      // this.end = this.start + this.paginator.pageSize;
      this.filtersService.personalTListFilter.page = 1;
      this.start =
        this.limit * (this.filtersService.personalTListFilter.page - 1);
      this.end = this.start + this.limit;
      // this.paginator.firstPage();
      try {
        let length = this.personaltService.getAll_count();
        let personals = this.personaltService.getWithOffsetLimitOrder(
          this.start,
          // this.paginator.pageSize,
          this.limit,
          this.filtersService.personalTListFilter.searchOrder
        );
        // this.length = (await length).rowcount;
        this.total = (await length).rowcount;
        this.personals = isNullOrUndefined(await personals)
          ? []
          : await personals;
      } catch (err) {
        console.error(err);
      } finally {
        this.spinnerService.hide();
      }
    }
  }

  // setPageSizeOptions(event: PageEvent) {
  // 	this.start = event.pageSize * event.pageIndex;
  // 	this.end = this.start + event.pageSize;
  // 	return event;
  // }

  // async setPageSizeOptions(event: PageEvent) {
  //   let start = event.pageSize * event.pageIndex;
  //   try {
  //     this.spinnerService.show();
  //     let personals;
  //     if (
  //       this.filtersService.personalTListFilter.searchStr.length +
  //         this.filtersService.personalTListFilter.searchStr_fond.length +
  //         this.filtersService.personalTListFilter.searchStr_opis.length +
  //         this.filtersService.personalTListFilter.searchStr_delo.length >
  //       0
  //     ) {
  //       personals = this.personaltService.getWithShortfilterOpisFondDeloOffsetLimitOrder(
  //         this.filtersService.personalTListFilter.searchStr,
  //         start,
  //         // this.paginator.pageSize,
  //         this.limit,
  //         this.filtersService.personalTListFilter.searchOrder,
  //         this.filtersService.personalTListFilter.searchStr_fond,
  //         this.filtersService.personalTListFilter.searchStr_opis,
  //         this.filtersService.personalTListFilter.searchStr_delo,
  //         this.filtersService.personalTListFilter.filterExact
  //       );
  //     } else {
  //       personals = this.personaltService.getWithOffsetLimitOrder(
  //         start,
  //         event.pageSize,
  //         this.filtersService.personalTListFilter.searchOrder
  //       );
  //     }
  //     this.personals = isNullOrUndefined(await personals)
  //       ? []
  //       : await personals;
  //   } catch (err) {
  //     console.error(`Ошибка ${err}`);
  //   } finally {
  //     this.spinnerService.hide();
  //   }
  //   this.pageSize = event.pageSize;
  //   this.pageEvent = event;
  // }

  onAddPersonalt() {
    this.router.navigate(['/sys/person/personaltedit/newdoc']);
  }

  onEditPersonalt(id) {
    let str = this.viewOnly
      ? `/sys/person/personaltedit/v_${id}`
      : `/sys/person/personaltedit/${id}`;
    this.router.navigate([str]);
  }

  async selectOrder(type: string) {
    switch (type) {
      case 'bycode':
        this.filtersService.personalTListFilter.searchOrder =
          this.filtersService.personalTListFilter.searchOrder === 'bycode'
            ? 'bycodedesc'
            : 'bycode';
        break;
      case 'bysurname':
        this.filtersService.personalTListFilter.searchOrder =
          this.filtersService.personalTListFilter.searchOrder === 'bysurname'
            ? 'bysurnamedesc'
            : 'bysurname';
        break;
      case 'bydelo':
        this.filtersService.personalTListFilter.searchOrder =
          this.filtersService.personalTListFilter.searchOrder === 'bydelo'
            ? 'bydelodesc'
            : 'bydelo';
        break;
    }
    this.setOrgerStr();
    try {
      this.spinnerService.show();
      // let personals = this.personaltService.getWithOffsetLimitOrder(0, this.pageSize, this.searchOrder);
      let personals =
        this.personaltService.getWithShortfilterOpisFondDeloOffsetLimitOrder(
          this.filtersService.personalTListFilter.searchStr,
          0,
          // this.paginator.pageSize,
          this.limit,
          this.filtersService.personalTListFilter.searchOrder,
          this.filtersService.personalTListFilter.searchStr_fond,
          this.filtersService.personalTListFilter.searchStr_opis,
          this.filtersService.personalTListFilter.searchStr_delo,
          this.filtersService.personalTListFilter.searchStr_delo_ext,
          this.filtersService.personalTListFilter.filterExact,
          this.filtersService.personalTListFilter.filterExactExt
        );
      this.personals = isNullOrUndefined(await personals)
        ? []
        : await personals;
    } catch (err) {
      console.error(`Ошибка ${err}`);
    } finally {
      // if (!isNullOrUndefined(this.pageEvent)) {
      //   this.pageEvent.pageIndex = 0;
      // }
      this.filtersService.personalTListFilter.page = 1;
      this.spinnerService.hide();
    }
  }

  setOrgerStr() {
    switch (this.filtersService.personalTListFilter.searchOrder) {
      case 'bycode':
        this.postfixCode =
          '<i class="fa fa-sort-amount-asc" aria-hidden="true"></i>';
        this.postfixSurname = '';
        this.postfixDelo = '';
        break;
      case 'bycodedesc':
        this.postfixCode =
          '<i class="fa fa-sort-amount-desc" aria-hidden="true"></i>';
        this.postfixSurname = '';
        this.postfixDelo = '';
        break;
      case 'bysurname':
        this.postfixCode = '';
        this.postfixDelo = '';
        this.postfixSurname =
          '<i class="fa fa-sort-amount-asc" aria-hidden="true"></i>';
        break;
      case 'bysurnamedesc':
        this.postfixCode = '';
        this.postfixDelo = '';
        this.postfixSurname =
          '<i class="fa fa-sort-amount-desc" aria-hidden="true"></i>';
        break;
      case 'bydelo':
        this.postfixCode = '';
        this.postfixSurname = '';
        this.postfixDelo =
          '<i class="fa fa-sort-amount-asc" aria-hidden="true"></i>';
        break;
      case 'bydelodesc':
        this.postfixCode = '';
        this.postfixSurname = '';
        this.postfixDelo =
          '<i class="fa fa-sort-amount-desc" aria-hidden="true"></i>';
        break;
    }
  }

  goToPage(n: number): void {
    this.filtersService.personalTListFilter.page = n;
    this.updatePage();
  }

  onNext(): void {
    this.filtersService.personalTListFilter.page++;
    this.updatePage();
  }

  onPrev(): void {
    this.filtersService.personalTListFilter.page = 1;
    this.updatePage();
  }

  onNext10(n = 1): void {
    this.filtersService.personalTListFilter.page += 10 * n;
    this.updatePage();
  }

  onPrev10(n = 1): void {
    this.filtersService.personalTListFilter.page -= 10 * n;
    this.updatePage();
  }

  async updatePage() {
    // this.globalService.personaltlistPage = this.filtersService.personalTListFilter.page;
    let start = this.limit * (this.filtersService.personalTListFilter.page - 1);
    try {
      this.spinnerService.show();
      let personals;
      if (
        this.filtersService.personalTListFilter.searchStr.length +
          this.filtersService.personalTListFilter.searchStr_fond.length +
          this.filtersService.personalTListFilter.searchStr_opis.length +
          this.filtersService.personalTListFilter.searchStr_delo.length +
          this.filtersService.personalTListFilter.searchStr_delo_ext.length >
        0
      ) {
        personals =
          this.personaltService.getWithShortfilterOpisFondDeloOffsetLimitOrder(
            this.filtersService.personalTListFilter.searchStr,
            start,
            // this.paginator.pageSize,
            this.limit,
            this.filtersService.personalTListFilter.searchOrder,
            this.filtersService.personalTListFilter.searchStr_fond,
            this.filtersService.personalTListFilter.searchStr_opis,
            this.filtersService.personalTListFilter.searchStr_delo,
            this.filtersService.personalTListFilter.searchStr_delo_ext,
            this.filtersService.personalTListFilter.filterExact,
            this.filtersService.personalTListFilter.filterExactExt
          );
      } else {
        personals = this.personaltService.getWithOffsetLimitOrder(
          start,
          this.limit,
          this.filtersService.personalTListFilter.searchOrder
        );
      }
      this.personals = isNullOrUndefined(await personals)
        ? []
        : await personals;
    } catch (err) {
      console.error(`Ошибка ${err}`);
    } finally {
      this.spinnerService.hide();
    }
  }
}
