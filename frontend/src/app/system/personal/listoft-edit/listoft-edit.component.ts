import { Component, OnInit, HostListener, OnDestroy } from '@angular/core';
import { DialogYesNoComponent } from '../../shared/components/dialog-yes-no/dialog-yes-no.component';
import { ListOfT, listOfT_width } from 'src/app/shared/models/listoft.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Muser } from 'src/app/shared/models/muser.model';
import { AuthService } from 'src/app/shared/services/auth.service';
import { MuserService } from '../../shared/services/muser.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ListoftService } from '../../shared/services/listoft.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { DeathreasontService } from '../../shared/services/deathreasont.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-listoft-edit',
  templateUrl: './listoft-edit.component.html',
  styleUrls: ['./listoft-edit.component.css'],
})
export class ListoftEditComponent implements OnInit, OnDestroy {
  listOfT_width = listOfT_width;

  id: number;
  intType: string | any = ''; // тип интерфейса
  loggedInUser: Muser;
  listoftForm: FormGroup;

  selectedListoft: ListOfT;
  selectedName_fIdx = 0;

  name_f: any[];
  activeRouterObserver: Subscription;
  ngOnDestroy(): void {
    // this.activeRouterObserver.unsubscribe();
  }
  @HostListener('window:keydown', ['$event'])
  onKeyPress(event: KeyboardEvent) {
    if ((event.ctrlKey || event.metaKey) && event.keyCode == 83) {
      event.preventDefault();
      if (!this.listoftForm.invalid && this.intType !== 'myprofile') {
        this.onSaveListoftForm();
      }
    }
  }

  constructor(
    private authService: AuthService,
    private globalService: GlobalService,
    private muserService: MuserService,
    private listoftService: ListoftService,
    private deathreasontService: DeathreasontService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private spinnerService: NgxSpinnerService,
    public dialog: MatDialog,
    public snackBar: MatSnackBar
  ) {
    this.activeRouterObserver = this.activatedRoute.params.subscribe(
      (param) => {
        this.id = param.id;
        if (param.id.indexOf('newdoc') !== -1) {
          this.intType = 'newdoc';
          this.selectedName_fIdx = +param.id.split('-')[1];
        } else {
          this.id = +param.id.split('-')[0];
          this.selectedName_fIdx = +param.id.split('-')[1];
          this.intType = 'editdoc';
        }
      }
    );
  }

  async ngOnInit() {
    this.name_f = this.globalService.name_f;
    if (this.selectedName_fIdx === 19) {
      this.listoftForm = new FormGroup({
        name_f: new FormControl({ value: null, disabled: true }, [
          Validators.required,
        ]),
        death_type: new FormControl(null, [Validators.required]),
        death_rmk: new FormControl(null, [Validators.required]),
      });
    } else if (this.selectedName_fIdx === 18) {
      this.listoftForm = new FormGroup({
        name_f: new FormControl({ value: null, disabled: true }, [
          Validators.required,
        ]),
        name_ent: new FormControl(null, [Validators.required]),
        name_rmk: new FormControl(null),
      });
    } else {
      this.listoftForm = new FormGroup({
        name_f: new FormControl({ value: null, disabled: true }, [
          Validators.required,
        ]),
        name_ent: new FormControl(null, [Validators.required]),
        name_rmk: new FormControl(null, [Validators.required]),
      });
    }
    try {
      let loggedInUser = this.muserService.getOneById(
        this.authService.getCurUser().id
      );
      this.loggedInUser = await loggedInUser;
    } catch (err) {
      this.globalService.exceptionHandling(err);
    }
    this.getData();
  }

  async getData() {
    this.spinnerService.show();
    try {
      switch (this.intType) {
        case 'editdoc': // редактирование описи
          /**Причина смерти в таблице death_reason_t */
          if (this.selectedName_fIdx === 19) {
            let selectedListoft = this.deathreasontService.getOneById(this.id);
            this.selectedListoft = await selectedListoft;
            this.listoftForm.patchValue({
              name_f: this.name_f[this.selectedName_fIdx].key,
              death_type: this.selectedListoft.death_type,
              death_rmk: this.selectedListoft.death_rmk,
            });
          } else {
            let selectedListoft = this.listoftService.getOneById(this.id);
            this.selectedListoft = await selectedListoft;
            this.listoftForm.patchValue({
              name_f: this.selectedListoft.name_f,
              name_ent: this.selectedListoft.name_ent,
              name_rmk: this.selectedListoft.name_rmk,
            });
          }
          break;
        case 'newdoc': // создание описи
          this.listoftForm.reset();
          this.listoftForm.patchValue({
            name_f: this.name_f[this.selectedName_fIdx].key,
          });
          break;
        default:
      }
    } catch (err) {
      this.globalService.exceptionHandling(err);
    } finally {
      this.spinnerService.hide();
    }
  }

  async onSaveListoftForm() {
    this.spinnerService.show();
    try {
      switch (this.intType) {
        case 'editdoc': // редактирование документа
          if (this.selectedName_fIdx === 18) {
            let putOpisForm = this.deathreasontService.putOneById(
              this.id,
              this.listoftForm.value
            );
            await putOpisForm;
          } else {
            let putOpisForm = this.listoftService.putOneById(
              this.id,
              this.listoftForm.getRawValue()
            );
            await putOpisForm;
          }
          break;
        case 'newdoc': // создание документа
          if (this.selectedName_fIdx === 19) {
            let addOpisForm = this.deathreasontService.postOne(
              this.listoftForm.value
            );
            await addOpisForm;
          } else {
            let addOpisForm = this.listoftService.postOne(
              this.listoftForm.getRawValue()
            );
            await addOpisForm;
          }
          break;
        default:
      }
      this.getData();
      this.openSnackBar(`Сохранено.`, 'Ok');
    } catch (err) {
      if (err.status === 401) {
        console.error(`Ошибка авторизации`);
        this.authService.logout();
      } else if (err.status === 409) {
        this.openSnackBar(`${err.error['error']}`, 'Ok');
      } else {
        console.error(`Ошибка ${err}`);
        this.openSnackBar(`Ошибка ${err.status}`, 'Ok');
      }
    } finally {
      this.spinnerService.hide();
    }
  }

  async onDelListoft() {
    let dialogRef = this.dialog.open(DialogYesNoComponent, {
      data: {
        title: 'Удалить запись?',
        question: ``,
      },
    });
    dialogRef.afterClosed().subscribe(async (result) => {
      if (result === true) {
        this.spinnerService.show();
        try {
          if (this.selectedName_fIdx === 19) {
            let resDelOpis = await this.deathreasontService.deleteOneById(
              this.id
            );
          } else {
            let resDelOpis = await this.listoftService.deleteOneById(this.id);
          }
          this.router.navigate(['/sys/person/listoftlist']);
          this.openSnackBar(`Удалено.`, 'Ok');
        } catch (err) {
          console.error(`Ошибка при удалении описи ${err}`);
          this.globalService.exceptionHandling(err);
        } finally {
          this.spinnerService.hide();
        }
      }
    });
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 10000,
    });
  }
}
