import { Component, OnInit, HostListener, OnDestroy } from '@angular/core';
import { DialogYesNoComponent } from '../../shared/components/dialog-yes-no/dialog-yes-no.component';
import { Muser } from 'src/app/shared/models/muser.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PlaceT, placeT_width } from 'src/app/shared/models/placet.model';
import { AuthService } from 'src/app/shared/services/auth.service';
import { MuserService } from '../../shared/services/muser.service';
import { PlacetService } from '../../shared/services/placet.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-placet-edit',
  templateUrl: './placet-edit.component.html',
  styleUrls: ['./placet-edit.component.css']
})
export class PlacetEditComponent implements OnInit, OnDestroy {
  placeT_width = placeT_width;

  id: number;
  intType: string | any = ''; // тип интерфейса
  loggedInUser: Muser;
  placeForm: FormGroup;

  selectedPlace: PlaceT;
  activeRouterObserver: Subscription;
  ngOnDestroy(): void {
    // this.activeRouterObserver.unsubscribe();
  }

  @HostListener('window:keydown', ['$event'])
  onKeyPress(event: KeyboardEvent) {
    if ((event.ctrlKey || event.metaKey) && event.keyCode == 83) {
      event.preventDefault();
      if (!this.placeForm.invalid && this.intType !== 'myprofile') {
        this.onSavePlacetForm();
      }
    }
  }

  constructor(
    private authService: AuthService,
    private muserService: MuserService,
    private placetService: PlacetService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private spinnerService: NgxSpinnerService,
    public dialog: MatDialog,
    public snackBar: MatSnackBar
  ) {
    this.activeRouterObserver = this.activatedRoute.params.subscribe(param => {
      this.id = param.id;
      if (param.id === 'newdoc') {
        this.intType = 'newdoc';
      } else {
        this.id = +param.id;
        this.intType = 'editdoc';
      }
    });
  }

  async ngOnInit() {
    this.placeForm = new FormGroup({
      place_num: new FormControl(null, []),
      beg_dat: new FormControl(null, []),
      end_dat: new FormControl(null, []),
      place_name: new FormControl(null, []),
      place_rem: new FormControl(null, []),
      status: new FormControl(null, []),
      place_code: new FormControl(null, []),
      cur_mark: new FormControl(null, [])
    });

    try {
      let loggedInUser = this.muserService.getOneById(
        this.authService.getCurUser().id
      );
      this.loggedInUser = await loggedInUser;
    } catch (err) {
      if (err.status === 401) {
        console.error(`Ошибка авторизации`);
        this.authService.logout();
      } else {
        console.error(`Ошибка ${err}`);
      }
    }
    this.getData();
  }

  async getData() {
    this.spinnerService.show();
    try {
      switch (this.intType) {
        case 'editdoc': // редактирование
          let selectedPlace = this.placetService.getOneById(this.id);
          this.selectedPlace = await selectedPlace;
          this.placeForm.patchValue({
            place_num: this.selectedPlace.place_num,
            beg_dat: this.selectedPlace.beg_dat,
            end_dat: this.selectedPlace.end_dat,
            place_name: this.selectedPlace.place_name,
            place_rem: this.selectedPlace.place_rem,
            status: this.selectedPlace.status,
            place_code: this.selectedPlace.place_code,
            cur_mark: this.selectedPlace.cur_mark === '*' ? true : false
          });
          break;
        case 'newdoc': // создание
          this.placeForm.reset();
          break;
        default:
      }
    } catch (err) {
      if (err.status === 401) {
        console.error(`Ошибка авторизации`);
        this.authService.logout();
      } else {
        console.error(`Ошибка ${err}`);
      }
    } finally {
      this.spinnerService.hide();
    }
  }

  async onSavePlacetForm() {
    this.spinnerService.show();
    try {
      if (this.placeForm.value['cur_mark'] === true) {
        this.placeForm.value['cur_mark'] = '*';
      } else {
        this.placeForm.value['cur_mark'] = ' ';
      }
      switch (this.intType) {
        case 'editdoc': // редактирование документа
          let putplaceForm = this.placetService.putOneById(
            this.id,
            this.placeForm.value
          );
          await putplaceForm;
          break;
        case 'newdoc': // создание документа
          let addPlaceForm = this.placetService.postOne(this.placeForm.value);
          await addPlaceForm;
          break;
        default:
      }
      this.getData();
      this.openSnackBar(`Сохранено.`, 'Ok');
    } catch (err) {
      if (err.status === 401) {
        console.error(`Ошибка авторизации`);
        this.authService.logout();
      } else if (err.status === 409) {
        this.openSnackBar(`${err.error['error']}`, 'Ok');
      } else {
        console.error(`Ошибка ${err}`);
        this.openSnackBar(`Ошибка ${err.status}`, 'Ok');
      }
    } finally {
      this.spinnerService.hide();
    }
  }

  async onDelNation() {
    let dialogRef = this.dialog.open(DialogYesNoComponent, {
      data: {
        title: 'Удалить объект?',
        question: ``
      }
    });
    dialogRef.afterClosed().subscribe(async result => {
      if (result === true) {
        this.spinnerService.show();
        try {
          let resDelOpis = await this.placetService.deleteOneById(this.id);
          this.router.navigate(['/sys/person/placetlist']);
          this.openSnackBar(`Удалено.`, 'Ok');
        } catch (err) {
          console.error(`Ошибка при удалении описи ${err}`);
          this.openSnackBar(`Ошибка ${err.status}`, 'Ok');
        } finally {
          this.spinnerService.hide();
        }
      }
    });
  }

  setCurmark() {
    this.placeForm.patchValue({
      cur_mark: +this.placeForm.value.end_dat === 9999 ? true : false
    });
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 10000
    });
  }
}
