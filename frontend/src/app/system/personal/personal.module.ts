import { NgModule } from '@angular/core';

import { PersonalRoutingModule } from './personal-routing.module';
import { PersonalComponent } from './personal.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { NationListComponent } from './nation-list/nation-list.component';
import { NationEditComponent } from './nation-edit/nation-edit.component';
import { NattFilterPipe } from '../shared/pipes/natt-filter.pipe';
import { PlacetListComponent } from './placet-list/placet-list.component';
import { PlacetEditComponent } from './placet-edit/placet-edit.component';
import { ListoftEditComponent } from './listoft-edit/listoft-edit.component';
import { ListoftListComponent } from './listoft-list/listoft-list.component';
import { ListoftFilterPipe } from '../shared/pipes/listoft-filter.pipe';
import { PersonalListComponent } from './personal-list/personal-list.component';
import { PersonalEditComponent } from './personal-edit/personal-edit.component';
import { OrgtEditComponent } from './personal-edit/orgt-edit/orgt-edit.component';
import { ActivitytEditComponent } from './personal-edit/activityt-edit/activityt-edit.component';
import { RepresstEditComponent } from './personal-edit/represst-edit/represst-edit.component';
import { CourttEditComponent } from './personal-edit/represst-edit/courtt-edit/courtt-edit.component';
import { ImpristEditComponent } from './personal-edit/represst-edit/courtt-edit/imprist-edit/imprist-edit.component';
import { HelptEditComponent } from './personal-edit/helpt-edit/helpt-edit.component';
import { SourtEditComponent } from './personal-edit/sourt-edit/sourt-edit.component';
import { SelectionListComponent } from './selection-list/selection-list.component';
import { SelectionEditComponent } from './selection-edit/selection-edit.component';
import { SelectionitemEditComponent } from './selection-edit/selectionitem-edit/selectionitem-edit.component';
import { SearchPageComponent } from './search-page/search-page.component';
import { NumberPageComponent } from './number-page/number-page.component';
import { StatListComponent } from './stat-list/stat-list.component';
import { StatEditComponent } from './stat-edit/stat-edit.component';
import { StatItemComponent } from './stat-item/stat-item.component';
import { FieldjoinFilterPipe } from '../shared/pipes/fieldjoin-filter.pipe';
import { DocumentListComponent } from './document-list/document-list.component';
import { DocumentEditComponent } from './document-edit/document-edit.component';

@NgModule({
  imports: [SharedModule, PersonalRoutingModule],
  declarations: [
    PersonalComponent,
    NationListComponent,
    NationEditComponent,
    NattFilterPipe,
    ListoftFilterPipe,
    PlacetListComponent,
    PlacetEditComponent,
    ListoftEditComponent,
    ListoftListComponent,
    PersonalListComponent,
    PersonalEditComponent,
    OrgtEditComponent,
    ActivitytEditComponent,
    RepresstEditComponent,
    CourttEditComponent,
    ImpristEditComponent,
    HelptEditComponent,
    SourtEditComponent,
    SelectionListComponent,
    SelectionEditComponent,
    SelectionitemEditComponent,
    SearchPageComponent,
    NumberPageComponent,
    StatListComponent,
    StatEditComponent,
    StatItemComponent,
    FieldjoinFilterPipe,
    DocumentListComponent,
    DocumentEditComponent,
  ],
})
export class PersonalModule {}
