import { Component, OnInit, HostListener, OnDestroy } from '@angular/core';
import { Muser } from 'src/app/shared/models/muser.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NatT, natT_width } from 'src/app/shared/models/natt.model';
import { NattService } from '../../shared/services/natt.service';
import { AuthService } from 'src/app/shared/services/auth.service';
import { MuserService } from '../../shared/services/muser.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DialogYesNoComponent } from '../../shared/components/dialog-yes-no/dialog-yes-no.component';
import { Subscription } from 'rxjs';
import { GlobalService } from 'src/app/shared/services/global.service';

@Component({
  selector: 'app-nation-edit',
  templateUrl: './nation-edit.component.html',
  styleUrls: ['./nation-edit.component.css'],
})
export class NationEditComponent implements OnInit, OnDestroy {

  constructor(
    private authService: AuthService,
    private muserService: MuserService,
    private globalService: GlobalService,
    private nattService: NattService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private spinnerService: NgxSpinnerService,
    public dialog: MatDialog,
    public snackBar: MatSnackBar
  ) {
    this.activeRouterObserver = this.activatedRoute.params.subscribe(
      (param) => {
        this.id = param.id;
        if (param.id === 'newdoc') {
          this.intType = 'newdoc';
        } else {
          this.id = +param.id;
          this.intType = 'editdoc';
        }
      }
    );
  }
  natT_width = natT_width;

  id: number;
  intType: string | any = ''; // тип интерфейса
  loggedInUser: Muser;
  nationForm: FormGroup;
  activeRouterObserver: Subscription;
  selectedNation: NatT;
  ngOnDestroy(): void {
    // this.activeRouterObserver.unsubscribe();
  }
  @HostListener('window:keydown', ['$event'])
  onKeyPress(event: KeyboardEvent) {
    if ((event.ctrlKey || event.metaKey) && event.keyCode == 83) {
      event.preventDefault();
      if (!this.nationForm.invalid && this.intType !== 'myprofile') {
        this.onSaveNationForm();
      }
    }
  }

  async ngOnInit() {
    this.nationForm = new FormGroup({
      nation: new FormControl(null, [Validators.required]),
      nation_rmk: new FormControl(null, [Validators.required]),
    });
    try {
      let loggedInUser = this.muserService.getOneById(
        this.authService.getCurUser().id
      );
      this.loggedInUser = await loggedInUser;
    } catch (err) {
      if (err.status === 401) {
        console.error(`Ошибка авторизации`);
        this.authService.logout();
      } else {
        console.error(`Ошибка ${err}`);
      }
    }
    this.getData();
  }

  async getData() {
    this.spinnerService.show();
    try {
      switch (this.intType) {
        case 'editdoc': // редактирование описи
          let selectedNation = this.nattService.getOneById(this.id);
          this.selectedNation = await selectedNation;
          this.nationForm.patchValue({
            nation: this.selectedNation.nation,
            nation_rmk: this.selectedNation.nation_rmk,
          });
          break;
        case 'newdoc': // создание описи
          this.nationForm.reset();
          break;
        default:
      }
    } catch (err) {
      if (err.status === 401) {
        console.error(`Ошибка авторизации`);
        this.authService.logout();
      } else {
        console.error(`Ошибка ${err}`);
      }
    } finally {
      this.spinnerService.hide();
    }
  }

  async onSaveNationForm() {
    this.spinnerService.show();
    try {
      switch (this.intType) {
        case 'editdoc': // редактирование документа
          let putOpisForm = this.nattService.putOneById(
            this.id,
            this.nationForm.value
          );
          await putOpisForm;
          break;
        case 'newdoc': // создание документа
          let addOpisForm = this.nattService.postOne(this.nationForm.value);
          await addOpisForm;
          break;
        default:
      }
      this.getData();
      this.openSnackBar(`Сохранено.`, 'Ok');
    } catch (err) {
      this.globalService.exceptionHandling(err);
    } finally {
      this.spinnerService.hide();
    }
  }

  async onDelNation() {
    let dialogRef = this.dialog.open(DialogYesNoComponent, {
      data: {
        title: 'Удалить национальность?',
        question: ``,
      },
    });
    dialogRef.afterClosed().subscribe(async (result) => {
      if (result === true) {
        this.spinnerService.show();
        try {
          let resDelOpis = await this.nattService.deleteOneById(this.id);
          this.router.navigate(['/sys/person/nationlist']);
          this.openSnackBar(`Удалено.`, 'Ok');
        } catch (err) {
          console.error(`Ошибка при удалении описи ${err}`);
          this.globalService.exceptionHandling(err);
        } finally {
          this.spinnerService.hide();
        }
      }
    });
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 10000,
    });
  }
}
