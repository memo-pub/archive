import { Component, OnInit, ViewChild } from '@angular/core';
import { Selection } from 'src/app/shared/models/selection.model';
import { MatDialog } from '@angular/material/dialog';
import { PageEvent, MatPaginator } from '@angular/material/paginator';
import { SelectionService } from '../../shared/services/selection.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { AuthService } from 'src/app/shared/services/auth.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { isNullOrUndefined } from 'util';
import { DialogSetstringComponent } from '../../shared/components/dialog-setstring/dialog-setstring.component';
import { FiltersService } from '../../shared/services/filters.service';
import { PersonalTFilter } from 'src/app/shared/models/personalt.model';

@Component({
  selector: 'app-selection-list',
  templateUrl: './selection-list.component.html',
  styleUrls: ['./selection-list.component.css'],
})
export class SelectionListComponent implements OnInit {
  // searchOrder = 'bysdatedesc';
  // searchStr = '';

  postfixId = '';
  postfixName = '';
  postfixSdate = '';

  selections: Selection[];

  // MatPaginator Inputs
  // length = 100;
  // pageSize = 100;
  // pageSizeOptions: number[] = [5, 10, 25, 100];
  // pageEvent: PageEvent;
  start = 0;
  end = 0;
  // @ViewChild(MatPaginator) paginator: MatPaginator;

  /**Для пагинатора */
  total = 0;
  // page = 1;
  limit = 100;

  constructor(
    public filterService: FiltersService,
    private authService: AuthService,
    private selectionService: SelectionService,
    private globalService: GlobalService,
    public dialog: MatDialog,
    private router: Router,
    private spinnerService: NgxSpinnerService
  ) {}

  async ngOnInit() {
    try {
      // this.globalService.selectioneditPage = 0;
      // this.globalService.selectioneditPageSize = 10;
      // this.globalService.selectionitemFilter = {};
      // this.searchOrder = isNullOrUndefined(
      //   this.globalService.selectionlistFilter
      // )
      //   ? this.searchOrder
      //   : this.globalService.selectionlistFilter.searchOrder;
      // if (!isNullOrUndefined(this.pageEvent)) {
      //   this.pageEvent.pageIndex = 0;
      // }
      // this.page = 1;
      this.spinnerService.show();
      let length = this.selectionService.getFilter_count(
        this.filterService.selectionListFilter.searchStr
      );
      this.start =
        this.limit * (this.filterService.selectionListFilter.page - 1);
      let selections = this.selectionService.getWithFilterOffsetLimitOrder(
        this.filterService.selectionListFilter.searchStr,
        this.start,
        this.limit,
        this.filterService.selectionListFilter.searchOrder
      );
      this.total = (await length).rowcount;
      this.selections = isNullOrUndefined(await selections)
        ? []
        : await selections;
      this.end = this.limit;
    } catch (err) {
      this.globalService.exceptionHandling(err);
    } finally {
      this.setOrderStr();
      this.filterService.selectionEditFilter = new PersonalTFilter();
      this.spinnerService.hide();
    }
  }

  async updateData() {
    if (this.filterService.selectionListFilter.searchStr.length > 0) {
      this.spinnerService.show();
      this.start = 0;
      // this.end = this.paginator.pageSize;
      this.end = this.limit;
      // this.paginator.firstPage();
      this.filterService.selectionListFilter.page = 1;
      try {
        let length = this.selectionService.getFilter_count(
          this.filterService.selectionListFilter.searchStr
        );
        let selections = this.selectionService.getWithFilterOffsetLimitOrder(
          this.filterService.selectionListFilter.searchStr,
          0,
          this.limit,
          this.filterService.selectionListFilter.searchOrder
        );
        this.total = (await length).rowcount;
        this.selections = isNullOrUndefined(await selections)
          ? []
          : await selections;
      } catch (err) {
        console.error(err);
      } finally {
        this.spinnerService.hide();
      }
    } else {
      // this.start = this.paginator.pageSize * this.paginator.pageIndex;
      this.filterService.selectionListFilter.page = 1;
      this.start =
        this.limit * (this.filterService.selectionListFilter.page - 1);
      // this.end = this.start + this.paginator.pageSize;
      this.end = this.start + this.limit;
      // this.paginator.firstPage();
      try {
        let length = this.selectionService.getAll_count();
        let selections = this.selectionService.getWithOffsetLimitOrder(
          this.start,
          // this.paginator.pageSize,
          this.limit,
          this.filterService.selectionListFilter.searchOrder
        );
        this.total = (await length).rowcount;
        this.selections = isNullOrUndefined(await selections)
          ? []
          : await selections;
      } catch (err) {
        this.globalService.exceptionHandling(err);
      } finally {
        this.spinnerService.hide();
      }
    }
  }

  // async setPageSizeOptions(event: PageEvent) {
  //   let start = event.pageSize * event.pageIndex;
  //   try {
  //     this.spinnerService.show();
  //     let selections;
  //     if (this.filterService.selectionListFilter.searchStr.length > 0) {
  //       selections = this.selectionService.getWithFilterOffsetLimitOrder(
  //         this.filterService.selectionListFilter.searchStr,
  //         start,
  //         // this.paginator.pageSize,
  //         this.limit,
  //         this.filterService.selectionListFilter.searchOrder
  //       );
  //     } else {
  //       selections = this.selectionService.getWithOffsetLimitOrder(
  //         start,
  //         event.pageSize,
  //         this.filterService.selectionListFilter.searchOrder
  //       );
  //     }
  //     this.selections = isNullOrUndefined(await selections)
  //       ? []
  //       : await selections;
  //   } catch (err) {
  //     console.error(`Ошибка ${err}`);
  //   } finally {
  //     this.spinnerService.hide();
  //   }
  //   this.pageSize = event.pageSize;
  //   this.pageEvent = event;
  // }

  async selectOrder(type: string) {
    switch (type) {
      case 'byid':
        this.filterService.selectionListFilter.searchOrder =
          this.filterService.selectionListFilter.searchOrder === 'byid'
            ? 'byiddesc'
            : 'byid';
        break;
      case 'byname':
        this.filterService.selectionListFilter.searchOrder =
          this.filterService.selectionListFilter.searchOrder === 'byname'
            ? 'bynamedesc'
            : 'byname';
        break;
      case 'bysdate':
        this.filterService.selectionListFilter.searchOrder =
          this.filterService.selectionListFilter.searchOrder === 'bysdate'
            ? 'bysdatedesc'
            : 'bysdate';
        break;
    }
    this.setOrderStr();
    try {
      this.spinnerService.show();
      // let selections = this.selectionService.getWithOffsetLimitOrder(0, this.pageSize, this.searchOrder);
      let selections = this.selectionService.getWithFilterOffsetLimitOrder(
        this.filterService.selectionListFilter.searchStr,
        0,
        // this.paginator.pageSize,
        this.limit,
        this.filterService.selectionListFilter.searchOrder
      );
      this.selections = isNullOrUndefined(await selections)
        ? []
        : await selections;
    } catch (err) {
      this.globalService.exceptionHandling(err);
    } finally {
      // if (!isNullOrUndefined(this.pageEvent)) {
      //   this.pageEvent.pageIndex = 0;
      // }
      this.filterService.selectionListFilter.page = 1;
      this.spinnerService.hide();
    }
  }

  setOrderStr() {
    switch (this.filterService.selectionListFilter.searchOrder) {
      case 'byid':
        this.postfixId =
          '<i class="fa fa-sort-amount-asc" aria-hidden="true"></i>';
        this.postfixName = '';
        this.postfixSdate = '';
        break;
      case 'byiddesc':
        this.postfixId =
          '<i class="fa fa-sort-amount-desc" aria-hidden="true"></i>';
        this.postfixName = '';
        this.postfixSdate = '';
        break;
      case 'byname':
        this.postfixId = '';
        this.postfixName =
          '<i class="fa fa-sort-amount-asc" aria-hidden="true"></i>';
        this.postfixSdate = '';
        break;
      case 'bynamedesc':
        this.postfixId = '';
        this.postfixName =
          '<i class="fa fa-sort-amount-desc" aria-hidden="true"></i>';
        this.postfixSdate = '';
        break;
      case 'bysdate':
        this.postfixId = '';
        this.postfixName = '';
        this.postfixSdate =
          '<i class="fa fa-sort-amount-asc" aria-hidden="true"></i>';
        break;
      case 'bysdatedesc':
        this.postfixId = '';
        this.postfixName = '';
        this.postfixSdate =
          '<i class="fa fa-sort-amount-desc" aria-hidden="true"></i>';
        break;
    }
  }

  onAddSelection() {
    let dialogRef = this.dialog.open(DialogSetstringComponent, {
      data: {
        title: 'Введите название выборки',
        question: ``,
      },
    });
    dialogRef.afterClosed().subscribe(async (result) => {
      if (typeof result === 'string') {
        try {
          let res = await this.selectionService.postOne({ name: result });
          this.onEditSelection(res.id);
        } catch (err) {
          this.globalService.exceptionHandling(err);
        }
      }
    });
  }

  onEditSelection(id) {
    this.router.navigate([`/sys/person/selectionedit/${id}`]);
  }

  async onDel(id) {
    try {
      await this.selectionService.deleteOneById(id);
      this.updateData();
    } catch (err) {
      this.globalService.exceptionHandling(err);
    }
  }

  goToPage(n: number): void {
    this.filterService.selectionListFilter.page = n;
    this.updatePage();
  }

  onNext(): void {
    this.filterService.selectionListFilter.page++;
    this.updatePage();
  }

  onPrev(): void {
    this.filterService.selectionListFilter.page = 1;
    this.updatePage();
  }

  onNext10(n = 1): void {
    this.filterService.selectionListFilter.page += 10 * n;
    this.updatePage();
  }

  onPrev10(n = 1): void {
    this.filterService.selectionListFilter.page -= 10 * n;
    this.updatePage();
  }

  async updatePage() {
    let start = this.limit * (this.filterService.selectionListFilter.page - 1);
    try {
      this.spinnerService.show();
      let selections;
      if (this.filterService.selectionListFilter.searchStr.length > 0) {
        selections = this.selectionService.getWithFilterOffsetLimitOrder(
          this.filterService.selectionListFilter.searchStr,
          start,
          this.limit,
          this.filterService.selectionListFilter.searchOrder
        );
      } else {
        selections = this.selectionService.getWithOffsetLimitOrder(
          start,
          this.limit,
          this.filterService.selectionListFilter.searchOrder
        );
      }
      this.selections = isNullOrUndefined(await selections)
        ? []
        : await selections;
    } catch (err) {
      this.globalService.exceptionHandling(err);
    } finally {
      this.spinnerService.hide();
    }
  }
}
