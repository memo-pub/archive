import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { AuthService } from 'src/app/shared/services/auth.service';
import { FieldjoinService } from '../../shared/services/fieldjoin.service';

@Component({
  selector: 'app-stat-item',
  templateUrl: './stat-item.component.html',
  styleUrls: ['./stat-item.component.css'],
})
export class StatItemComponent implements OnInit {
  id: number;
  fieldjoins: any[];
  start = 0;
  end = 0;
  page = 0;
  limit = 100;
  searchStr = '';
  constructor(
    private authService: AuthService,
    private fieldjoinService: FieldjoinService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private spinnerService: NgxSpinnerService,
    public dialog: MatDialog,
    public snackBar: MatSnackBar
  ) {
    this.activatedRoute.params.subscribe((param) => {
      this.id = +param.id;
    });
  }

  ngOnInit() {
    this.getData();
    this.end = this.limit;
  }

  async getData() {
    this.spinnerService.show();
    try {
      let fieldjoins = this.fieldjoinService.getOneById(this.id);
      this.fieldjoins = !(await fieldjoins) ? [] : await fieldjoins;
    } catch (err) {
      if (err.status === 401) {
        console.error(`Ошибка авторизации`);
        this.authService.logout();
      } else {
        console.error(`Ошибка ${err}`);
      }
    } finally {
      this.spinnerService.hide();
    }
  }

  goToPage(n: number): void {
    this.page = n;
    this.updatePage();
  }

  onNext(): void {
    this.page++;
    this.updatePage();
  }

  onPrev(): void {
    this.page = 1;
    this.updatePage();
  }

  onNext10(n = 1): void {
    this.page += 10 * n;
    this.updatePage();
  }

  onPrev10(n = 1): void {
    this.page -= 10 * n;
    this.updatePage();
  }

  updatePage() {
    this.start = this.limit * (this.page - 1);
    this.end = this.start + this.limit;
  }

  updateData() {
    if (this.searchStr.length > 0) {
      this.start = 0;
      this.end = this.limit;
      this.page = 1;
    } else {
      this.start = this.limit * (this.page - 1);
      this.end = this.start + this.limit;
    }
  }
}
