import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { SelectionItem } from 'src/app/shared/models/selectionitem.model';
import { MatDialog } from '@angular/material/dialog';
import { PageEvent, MatPaginator } from '@angular/material/paginator';
import { SelectionitemService } from '../../shared/services/selectionitem.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { AuthService } from 'src/app/shared/services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { isNullOrUndefined } from 'util';
import { SelectionService } from '../../shared/services/selection.service';
import { DialogSetstringComponent } from '../../shared/components/dialog-setstring/dialog-setstring.component';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { saveAs as importedSaveAs } from 'file-saver';
import { FiltersService } from '../../shared/services/filters.service';
import { Subscription } from 'rxjs';
import { DateparseService } from 'src/app/shared/services/dateparse.service';

@Component({
  selector: 'app-selection-edit',
  templateUrl: './selection-edit.component.html',
  styleUrls: ['./selection-edit.component.css'],
})
export class SelectionEditComponent implements OnInit, OnDestroy {
  id: number;

  title = '';

  // searchOrder = 'bycode';
  // searchStr = '';
  // searchStr_fond = '';
  // searchStr_opis = '';
  // searchStr_delo = '';

  postfixCode = '';
  postfixSurname = '';
  postfixDelo = '';
  postfixSour = '';

  selectionitems: SelectionItem[];

  // length = 100;
  // pageSize = 100;
  // pageSizeOptions: number[] = [5, 10, 25, 100];
  // pageEvent: PageEvent;
  start = 0;
  end = 0;
  // @ViewChild(MatPaginator) paginator: MatPaginator;

  selection: number[] = []; // выбранные id

  inProgress = false;
  inProgressVal = 0;

  // filterExact = false;

  /**Для пагинатора */
  total = 0;
  // page = 1;
  limit = 100;
  activeRouterObserver: Subscription;
  ngOnDestroy(): void {
    // this.activeRouterObserver.unsubscribe();
  }

  constructor(
    public filtersService: FiltersService,
    private globalService: GlobalService,
    private authService: AuthService,
    public dialog: MatDialog,
    private selectionitemService: SelectionitemService,
    private selectionService: SelectionService,
    public dateparseService: DateparseService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private spinnerService: NgxSpinnerService
  ) {
    this.activeRouterObserver = this.activatedRoute.params.subscribe(
      (param) => {
        this.id = +param.id;
      }
    );
  }

  async ngOnInit() {
    try {
      // this.filterExact = this.globalService.personaltlistFilterExact;
      let selection = this.selectionService.getOneById(this.id);
      // this.searchOrder =
      //   isNullOrUndefined(this.globalService.selectionitemFilter) ||
      //   isNullOrUndefined(this.globalService.selectionitemFilter.searchOrder)
      //     ? ''
      //     : this.globalService.selectionitemFilter.searchOrder;
      // this.searchStr =
      //   isNullOrUndefined(this.globalService.selectionitemFilter) ||
      //   isNullOrUndefined(this.globalService.selectionitemFilter.searchStr)
      //     ? ''
      //     : this.globalService.selectionitemFilter.searchStr;
      // if (!isNullOrUndefined(this.pageEvent)) {
      //   this.pageEvent.pageIndex = this.globalService.selectioneditPage;
      // }
      // this.page = this.globalService.selectioneditPage + 1;
      // this.pageSize = this.globalService.selectioneditPageSize;
      let length = this.selectionitemService.getBySelectionId_count(this.id);
      // let start =
      //   this.globalService.selectioneditPage *
      //   this.globalService.selectioneditPageSize;
      // let start =
      //   (this.filtersService.selectionEditFilter.page - 1) * this.limit;
      // let selectionitems = this.selectionitemService.getWithOffsetLimit(
      //   start,
      //   this.limit,
      //   this.id
      // );
      // this.length = (await length).rowcount;
      this.total = (await length).rowcount;
      // this.selectionitems = isNullOrUndefined(await selectionitems)
      //   ? []
      //   : await selectionitems;
      this.end = this.limit;
      this.title = (await selection).name;
    } catch (err) {
      this.globalService.exceptionHandling(err);
    } finally {
      this.updateData(false);
      this.setOrgerStr();
      let obj = {
        scroll: 0,
        selectedOrgt: false,
        selectedActivityt: false,
        selectedRepresst: false,
        selectedHelp: false,
        selectedSourt: false,
        selectedUsaget: false,
        selectedMentioned: false,
      };
      this.globalService.personal_edit = obj;
      let obj2 = {
        scroll: 0,
        selectedCourtt: false,
      };
      this.globalService.represst_edit = obj2;
      let obj1 = {
        scroll: 0,
        selectedImprist: false,
      };
      this.globalService.courtt_edit = obj1;
      this.spinnerService.hide();
    }
  }

  async updateData(reset = true) {
    // this.globalService.selectionitemFilter.searchStr = this.searchStr;
    // this.globalService.personaltlistFilterExact = this.filterExact;
    if (reset === true) {
      this.filtersService.selectionEditFilter.page = 1;
    }
    if (
      this.filtersService.selectionEditFilter.searchStr.length +
        this.filtersService.selectionEditFilter.searchStr_fond.length +
        this.filtersService.selectionEditFilter.searchStr_opis.length +
        this.filtersService.selectionEditFilter.searchStr_delo.length >
      0
    ) {
      this.spinnerService.show();
      this.start =
        (this.filtersService.selectionEditFilter.page - 1) * this.limit;
      // this.page = this.globalService.selectioneditPage + 1;
      // this.end = this.start + this.paginator.pageSize;
      this.end = this.start + this.limit;
      // this.paginator.firstPage();
      // this.filtersService.selectionEditFilter.page = 1;
      // this.paginator.pageIndex = this.globalService.selectioneditPage;
      try {
        // let length = this.selectionitemService.getShortfilter_count(this.searchStr, this.id);
        let length = this.selectionitemService.getShortfilterOpisFondDelo_count(
          this.filtersService.selectionEditFilter.searchStr,
          this.filtersService.selectionEditFilter.searchStr_fond,
          this.filtersService.selectionEditFilter.searchStr_opis,
          this.filtersService.selectionEditFilter.searchStr_delo,
          this.id,
          this.filtersService.selectionEditFilter.filterExact
        );
        let selectionitems = this.selectionitemService.getWithShortfilterOpisFondDeloOffsetLimitOrder(
          this.filtersService.selectionEditFilter.searchStr,
          this.start,
          // this.paginator.pageSize,
          this.limit,
          this.filtersService.selectionEditFilter.searchOrder,
          this.filtersService.selectionEditFilter.searchStr_fond,
          this.filtersService.selectionEditFilter.searchStr_opis,
          this.filtersService.selectionEditFilter.searchStr_delo,
          this.id,
          this.filtersService.selectionEditFilter.filterExact
        );
        // this.selectionitemService.getWithShortfilterOffsetLimitOrder(
        //   this.searchStr, 0, this.paginator.pageSize, this.searchOrder, this.id);
        // this.length = (await length).rowcount;
        this.total = (await length).rowcount;
        this.selectionitems = isNullOrUndefined(await selectionitems)
          ? []
          : await selectionitems;
      } catch (err) {
        this.globalService.exceptionHandling(err);
      } finally {
        this.spinnerService.hide();
      }
    } else {
      this.start =
        (this.filtersService.selectionEditFilter.page - 1) * this.limit;
      // this.end = this.start + this.paginator.pageSize;
      this.end = this.start + this.limit;
      // this.paginator.pageIndex = this.globalService.selectioneditPage;
      // this.filtersService.selectionEditFilter.page =
      //   this.globalService.selectioneditPage + 1;
      // this.paginator.firstPage();
      try {
        let length = this.selectionitemService.getBySelectionId_count(this.id);
        let selectionitems = this.selectionitemService.getWithOffsetLimitOrder(
          this.start,
          // this.paginator.pageSize,
          this.limit,
          this.filtersService.selectionEditFilter.searchOrder,
          this.id
        );
        // this.length = (await length).rowcount;
        this.total = (await length).rowcount;
        this.selectionitems = isNullOrUndefined(await selectionitems)
          ? []
          : await selectionitems;
      } catch (err) {
        this.globalService.exceptionHandling(err);
      } finally {
        this.spinnerService.hide();
      }
    }
  }

  // async setPageSizeOptions(event: PageEvent) {
  //   this.globalService.selectioneditPage = event.pageIndex;
  //   this.globalService.selectioneditPageSize = event.pageSize;
  //   let start = event.pageSize * event.pageIndex;
  //   try {
  //     this.spinnerService.show();
  //     let selectionitems;
  //     if (
  //       this.searchStr.length +
  //         this.searchStr_fond.length +
  //         this.searchStr_opis.length +
  //         this.searchStr_delo.length >
  //       0
  //     ) {
  //       selectionitems = this.selectionitemService.getWithShortfilterOpisFondDeloOffsetLimitOrder(
  //         this.searchStr,
  //         start,
  //         // this.paginator.pageSize,
  //         this.limit,
  //         this.searchOrder,
  //         this.searchStr_fond,
  //         this.searchStr_opis,
  //         this.searchStr_delo,
  //         this.id,
  //         this.filterExact
  //       );
  //       // this.selectionitemService.getWithShortfilterOffsetLimitOrder(
  //       //   this.searchStr, start, this.paginator.pageSize, this.searchOrder, this.id);
  //     } else {
  //       selectionitems = this.selectionitemService.getWithOffsetLimitOrder(
  //         start,
  //         event.pageSize,
  //         this.searchOrder,
  //         this.id
  //       );
  //     }
  //     this.selectionitems = isNullOrUndefined(await selectionitems)
  //       ? []
  //       : await selectionitems;
  //   } catch (err) {
  //     console.error(`Ошибка ${err}`);
  //   } finally {
  //     this.spinnerService.hide();
  //   }
  //   this.pageSize = event.pageSize;
  //   this.pageEvent = event;
  // }

  async selectOrder(type: string) {
    switch (type) {
      case 'bycode':
        this.filtersService.selectionEditFilter.searchOrder =
          this.filtersService.selectionEditFilter.searchOrder === 'bycode'
            ? 'bycodedesc'
            : 'bycode';
        break;
      case 'bysurname':
        this.filtersService.selectionEditFilter.searchOrder =
          this.filtersService.selectionEditFilter.searchOrder === 'bysurname'
            ? 'bysurnamedesc'
            : 'bysurname';
        break;
      case 'bydelo':
        this.filtersService.selectionEditFilter.searchOrder =
          this.filtersService.selectionEditFilter.searchOrder === 'bydelo'
            ? 'bydelodesc'
            : 'bydelo';
        break;
      case 'bysour_cnt':
        this.filtersService.selectionEditFilter.searchOrder =
          this.filtersService.selectionEditFilter.searchOrder === 'bysour_cnt'
            ? 'bysour_cntdesc'
            : 'bysour_cnt';
        break;
    }
    // this.globalService.selectionitemFilter.searchOrder = this.searchOrder;
    this.setOrgerStr();
    try {
      this.spinnerService.show();
      let selectionitems = this.selectionitemService.getWithOffsetLimitOrder(
        0,
        this.limit,
        this.filtersService.selectionEditFilter.searchOrder,
        this.id
      );
      this.selectionitems = isNullOrUndefined(await selectionitems)
        ? []
        : await selectionitems;
    } catch (err) {
      this.globalService.exceptionHandling(err);
    } finally {
      // if (!isNullOrUndefined(this.pageEvent)) {
      //   this.pageEvent.pageIndex = 0;
      // }
      this.filtersService.selectionEditFilter.page = 1;
      this.spinnerService.hide();
    }
  }

  setOrgerStr() {
    switch (this.filtersService.selectionEditFilter.searchOrder) {
      case 'bycode':
        this.postfixCode =
          '<i class="fa fa-sort-amount-asc" aria-hidden="true"></i>';
        this.postfixSurname = '';
        this.postfixDelo = '';
        this.postfixSour = '';
        break;
      case 'bycodedesc':
        this.postfixCode =
          '<i class="fa fa-sort-amount-desc" aria-hidden="true"></i>';
        this.postfixSurname = '';
        this.postfixDelo = '';
        this.postfixSour = '';
        break;
      case 'bysurname':
        this.postfixCode = '';
        this.postfixDelo = '';
        this.postfixSour = '';
        this.postfixSurname =
          '<i class="fa fa-sort-amount-asc" aria-hidden="true"></i>';
        break;
      case 'bysurnamedesc':
        this.postfixCode = '';
        this.postfixDelo = '';
        this.postfixSour = '';
        this.postfixSurname =
          '<i class="fa fa-sort-amount-desc" aria-hidden="true"></i>';
        break;
      case 'bydelo':
        this.postfixCode = '';
        this.postfixSurname = '';
        this.postfixSour = '';
        this.postfixDelo =
          '<i class="fa fa-sort-amount-asc" aria-hidden="true"></i>';
        break;
      case 'bydelodesc':
        this.postfixCode = '';
        this.postfixSurname = '';
        this.postfixSour = '';
        this.postfixDelo =
          '<i class="fa fa-sort-amount-desc" aria-hidden="true"></i>';
        break;
      case 'bysour_cnt':
        this.postfixCode = '';
        this.postfixSurname = '';
        this.postfixSour =
          '<i class="fa fa-sort-amount-asc" aria-hidden="true"></i>';
        this.postfixDelo = '';
        break;
      case 'bysour_cntdesc':
        this.postfixCode = '';
        this.postfixSurname = '';
        this.postfixSour =
          '<i class="fa fa-sort-amount-desc" aria-hidden="true"></i>';
        this.postfixDelo = '';
        break;
    }
  }

  onEditPersonalt(id) {
    this.router.navigate([`/sys/person/personaltedit/v_${id}`]);
  }

  masterToggle() {
    let allSelected = this.isAllSelected();
    for (let i = 0; i < this.selectionitems.length; i++) {
      let pos = this.selection.findIndex(
        (item) => item === this.selectionitems[i].id
      );
      if (pos === -1) {
        if (allSelected === false) {
          this.selection.push(this.selectionitems[i].id);
        }
      } else {
        if (allSelected === true) {
          this.selection.splice(pos, 1);
        }
      }
    }
  }

  toggleSelection(id) {
    let pos = this.selection.findIndex((item) => item === id);
    if (pos === -1) {
      this.selection.push(id);
    } else {
      this.selection.splice(pos, 1);
    }
  }

  isAllSelected(): boolean {
    if (isNullOrUndefined(this.selectionitems)) {
      return false;
    }
    if (this.selectionitems.length === 0) {
      return false;
    }
    let res = true;
    for (let i = 0; i < this.selectionitems.length; i++) {
      if (
        this.selection.findIndex(
          (item) => item === this.selectionitems[i].id
        ) === -1
      ) {
        res = false;
      }
    }
    return res;
  }

  isSelected(id): boolean {
    return this.selection.findIndex((item) => item === id) !== -1;
  }

  async onDelSelected() {
    this.spinnerService.show();
    try {
      await this.selectionitemService.deleteSelectionitem({
        ids: this.selection,
      });
      this.selection.length = 0;
      this.updateData();
    } catch (err) {
      this.globalService.exceptionHandling(err);
    } finally {
      this.spinnerService.hide();
    }
  }

  onEdittitle() {
    let dialogRef = this.dialog.open(DialogSetstringComponent, {
      data: {
        title: 'Введите название выборки',
        str: this.title,
        question: ``,
      },
    });
    dialogRef.afterClosed().subscribe(async (result) => {
      if (typeof result === 'string') {
        try {
          await this.selectionService.putOneById(this.id, { name: result });
          this.title = result;
        } catch (err) {
          this.globalService.exceptionHandling(err);
        }
      }
    });
  }

  async onDel(id) {
    this.spinnerService.show();
    try {
      let obj = { ids: id };
      await this.selectionitemService.deleteSelectionitem(obj);
      this.selection.length = 0;
      this.updateData();
    } catch (err) {
      this.globalService.exceptionHandling(err);
    } finally {
      this.spinnerService.hide();
    }
  }

  async onGetsimplelist() {
    this.spinnerService.show();
    try {
      let blob = this.selectionitemService.getSimpleList(this.id);
      this.inProgress = true;
      this.inProgressVal = 0;
      blob.subscribe((event) => {
        if (event.type === HttpEventType.DownloadProgress) {
          const percentDone = Math.round((100 * event.loaded) / event.total);
          this.inProgressVal = percentDone;
        } else if (event instanceof HttpResponse) {
          this.inProgress = false;
          this.inProgressVal = 0;
          this.spinnerService.hide();
          // FileSaver.saveAs(event.body, `Простой список_id${this.id}.xlsx`);
          importedSaveAs(event.body, `Простой список_id${this.id}.xlsx`);
          // this.fileSaverService.save(event.body, 'filename.xlsx', 'xlsx');
        } else if (event.status === 409) {
          this.inProgress = false;
          this.inProgressVal = 0;
          this.spinnerService.hide();
        }
      });
    } catch (err) {
      this.globalService.exceptionHandling(err);
      this.spinnerService.hide();
    }
  }

  async onGetorgt() {
    this.spinnerService.show();
    try {
      let blob = this.selectionitemService.getOrgtList(this.id);
      this.inProgress = true;
      this.inProgressVal = 0;
      blob.subscribe((event) => {
        if (event.type === HttpEventType.DownloadProgress) {
          const percentDone = Math.round((100 * event.loaded) / event.total);
          this.inProgressVal = percentDone;
        } else if (event instanceof HttpResponse) {
          this.inProgress = false;
          this.inProgressVal = 0;
          this.spinnerService.hide();
          importedSaveAs(
            event.body,
            `Общественная деятельность_id${this.id}.xlsx`
          );
        } else if (event.status === 409) {
          this.inProgress = false;
          this.inProgressVal = 0;
          this.spinnerService.hide();
        }
      });
    } catch (err) {
      this.globalService.exceptionHandling(err);
      this.spinnerService.hide();
    }
  }

  async onGetactivityt() {
    this.spinnerService.show();
    try {
      let blob = this.selectionitemService.getActivitytList(this.id);
      this.inProgress = true;
      this.inProgressVal = 0;
      blob.subscribe((event) => {
        if (event.type === HttpEventType.DownloadProgress) {
          const percentDone = Math.round((100 * event.loaded) / event.total);
          this.inProgressVal = percentDone;
        } else if (event instanceof HttpResponse) {
          this.inProgress = false;
          this.inProgressVal = 0;
          this.spinnerService.hide();
          importedSaveAs(
            event.body,
            `Профессиональная деятельность_id${this.id}.xlsx`
          );
        } else if (event.status === 409) {
          this.inProgress = false;
          this.inProgressVal = 0;
          this.spinnerService.hide();
        }
      });
    } catch (err) {
      this.globalService.exceptionHandling(err);
      this.spinnerService.hide();
    }
  }

  async onGetrepresst() {
    this.spinnerService.show();
    try {
      let blob = this.selectionitemService.getRepresstList(this.id);
      this.inProgress = true;
      this.inProgressVal = 0;
      blob.subscribe((event) => {
        if (event.type === HttpEventType.DownloadProgress) {
          const percentDone = Math.round((100 * event.loaded) / event.total);
          this.inProgressVal = percentDone;
        } else if (event instanceof HttpResponse) {
          this.inProgress = false;
          this.inProgressVal = 0;
          this.spinnerService.hide();
          importedSaveAs(event.body, `Репрессии_id${this.id}.xlsx`);
        } else if (event.status === 409) {
          this.inProgress = false;
          this.inProgressVal = 0;
          this.spinnerService.hide();
        }
      });
    } catch (err) {
      this.globalService.exceptionHandling(err);
      this.spinnerService.hide();
    }
  }

  async onGetGpr() {
    this.spinnerService.show();
    try {
      let blob = this.selectionitemService.getRepresstGpr(this.id);
      this.inProgress = true;
      this.inProgressVal = 0;
      blob.subscribe((event) => {
        if (event.type === HttpEventType.DownloadProgress) {
          const percentDone = Math.round((100 * event.loaded) / event.total);
          this.inProgressVal = percentDone;
        } else if (event instanceof HttpResponse) {
          this.inProgress = false;
          this.inProgressVal = 0;
          this.spinnerService.hide();
          importedSaveAs(event.body, `ЖПР_id${this.id}.xlsx`);
        } else if (event.status === 409) {
          this.inProgress = false;
          this.inProgressVal = 0;
          this.spinnerService.hide();
        }
      });
    } catch (err) {
      this.globalService.exceptionHandling(err);
      this.spinnerService.hide();
    }
  }

  onAddSelectionitems() {
    this.router.navigate([`/sys/person/selectionitemedit/${this.id}`]);
  }

  goToPage(n: number): void {
    this.filtersService.selectionEditFilter.page = n;
    this.updatePage();
  }

  onNext(): void {
    this.filtersService.selectionEditFilter.page++;
    this.updatePage();
  }

  onPrev(): void {
    this.filtersService.selectionEditFilter.page = 1;
    this.updatePage();
  }

  onNext10(n = 1): void {
    this.filtersService.selectionEditFilter.page += 10 * n;
    this.updatePage();
  }

  onPrev10(n = 1): void {
    this.filtersService.selectionEditFilter.page -= 10 * n;
    this.updatePage();
  }

  async updatePage() {
    // this.globalService.selectioneditPage = this.page - 1;
    // this.globalService.selectioneditPageSize = this.limit;
    let start = this.limit * (this.filtersService.selectionEditFilter.page - 1);
    try {
      this.spinnerService.show();
      let selectionitems;
      if (
        this.filtersService.selectionEditFilter.searchStr.length +
          this.filtersService.selectionEditFilter.searchStr_fond.length +
          this.filtersService.selectionEditFilter.searchStr_opis.length +
          this.filtersService.selectionEditFilter.searchStr_delo.length >
        0
      ) {
        selectionitems = this.selectionitemService.getWithShortfilterOpisFondDeloOffsetLimitOrder(
          this.filtersService.selectionEditFilter.searchStr,
          start,
          // this.paginator.pageSize,
          this.limit,
          this.filtersService.selectionEditFilter.searchOrder,
          this.filtersService.selectionEditFilter.searchStr_fond,
          this.filtersService.selectionEditFilter.searchStr_opis,
          this.filtersService.selectionEditFilter.searchStr_delo,
          this.id,
          this.filtersService.selectionEditFilter.filterExact
        );
        // this.selectionitemService.getWithShortfilterOffsetLimitOrder(
        //   this.searchStr, start, this.paginator.pageSize, this.searchOrder, this.id);
      } else {
        selectionitems = this.selectionitemService.getWithOffsetLimitOrder(
          start,
          this.limit,
          this.filtersService.selectionEditFilter.searchOrder,
          this.id
        );
      }
      this.selectionitems = isNullOrUndefined(await selectionitems)
        ? []
        : await selectionitems;
    } catch (err) {
      this.globalService.exceptionHandling(err);
    } finally {
      this.spinnerService.hide();
    }
  }
}
