import { Component, OnInit, HostListener, OnDestroy } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { SelectionitemService } from 'src/app/system/shared/services/selectionitem.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { isNullOrUndefined } from 'util';
import { GlobalService } from 'src/app/shared/services/global.service';
import { PlaceT } from 'src/app/shared/models/placet.model';
import { Observable, Subscription } from 'rxjs';
import { PlacetService } from 'src/app/system/shared/services/placet.service';
import { startWith, map } from 'rxjs/operators';
import { NatT } from 'src/app/shared/models/natt.model';
import { NattService } from 'src/app/system/shared/services/natt.service';
import { ListoftService } from 'src/app/system/shared/services/listoft.service';
import { ListOfT } from 'src/app/shared/models/listoft.model';
import { selectionItem_width } from 'src/app/shared/models/selectionitem.model';

@Component({
  selector: 'app-selectionitem-edit',
  templateUrl: './selectionitem-edit.component.html',
  styleUrls: ['./selectionitem-edit.component.css'],
})
export class SelectionitemEditComponent implements OnInit, OnDestroy {
  selectionItem_width = selectionItem_width;

  id: number;

  filterForm: FormGroup;
  yesno: any[];
  yesnoq: any[];
  sex: any[];
  nations: NatT[];
  origins: ListOfT[];
  educs: ListOfT[];
  specs: ListOfT[];
  spheres: ListOfT[];
  particips: ListOfT[];
  org_codes: ListOfT[];
  repress_types: ListOfT[];
  reasons: ListOfT[];
  protests: ListOfT[];
  help_types: ListOfT[];
  impris_types: ListOfT[];
  rehab_reases: ListOfT[];
  us_types: ListOfT[];

  myControl_birth_place: FormControl;
  myControl_nation_id: FormControl;
  myControl_residence_code: FormControl;
  filteredPlacets: Observable<PlaceT[]>;
  filteredNations: Observable<NatT[]>;
  timerId;

  placets: PlaceT[];

  sentenses: ListOfT[];
  myControl_sentenses: FormControl;
  filteredSentenses: Observable<ListOfT[]>;

  sour_types: ListOfT[];
  myControl_sour_type: FormControl;
  filteredSour_type: Observable<ListOfT[]>;

  viewOnly = false;

  phoneMask: any[] = [
    '+',
    /[1-9]/,
    ' ',
    '(',
    /[1-9]/,
    /\d/,
    /\d/,
    ')',
    ' ',
    /\d/,
    /\d/,
    /\d/,
    '-',
    /\d/,
    /\d/,
    /\d/,
    /\d/,
    /\d/,
  ];
  activeRouterObserver: Subscription;
  ngOnDestroy(): void {
    // this.activeRouterObserver.unsubscribe();
  }
  @HostListener('window:keydown', ['$event'])
  onKeyPress(event: KeyboardEvent) {
    if ((event.ctrlKey || event.metaKey) && event.keyCode == 83) {
      event.preventDefault();
      if (this.viewOnly === true) {
        this.onAdd();
      }
    }
  }

  constructor(
    private selectionitemService: SelectionitemService,
    private activatedRoute: ActivatedRoute,
    private globalService: GlobalService,
    private listoftService: ListoftService,
    private placetService: PlacetService,
    private nattService: NattService,
    private router: Router,
    private spinnerService: NgxSpinnerService,
    public dialog: MatDialog,
    public snackBar: MatSnackBar
  ) {
    this.activeRouterObserver = this.activatedRoute.params.subscribe(
      (param) => {
        this.id = param.id;
      }
    );
  }

  async ngOnInit() {
    this.filterForm = new FormGroup({
      selection_id: new FormControl(
        { value: +this.id, disabled: this.viewOnly },
        []
      ),
      personal_t_code: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      personal_t_type_r: new FormControl(
        { value: undefined, disabled: this.viewOnly },
        []
      ),
      personal_t_type_i: new FormControl(
        { value: undefined, disabled: this.viewOnly },
        []
      ),
      personal_t_type_a: new FormControl(
        { value: undefined, disabled: this.viewOnly },
        []
      ),
      personal_t_mark: new FormControl(
        { value: undefined, disabled: this.viewOnly },
        []
      ),
      personal_t_surname: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      personal_t_fname: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      personal_t_lname: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      personal_t_sex: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      personal_t_birth_from: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      personal_t_birth_to: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      personal_t_birth_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      personal_t_death1: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      personal_t_death2: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      personal_t_death_date_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      personal_t_birth_place: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      personal_t_nation: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      personal_t_citiz: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      personal_t_origin: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      personal_t_educ: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      personal_t_spec: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      personal_t_origin_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      personal_t_educ_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      personal_t_spec_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      personal_t_address: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      personal_t_phone: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      personal_t_notice: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      personal_t_commentariy: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      personal_t_place: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      personal_t_mentioned_person: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),

      impris_t_place_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      impris_t_prison_name_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      impris_t_impris_no: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      impris_t_prison_name: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      impris_t_arrival_dat1: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      impris_t_arrival_dat2: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      impris_t_arrival_dat_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      impris_t_leave_dat1: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      impris_t_leave_dat2: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      impris_t_leave_dat_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      impris_t_reason: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      impris_t_reason_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      impris_t_work: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      impris_t_protest: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      impris_t_protest_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      impris_t_place: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      impris_t_impris_type: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),

      sour_t_sour_date_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      sour_t_sour_name: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      sour_t_sour_code: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      sour_t_mark_mem: new FormControl(
        { value: undefined, disabled: this.viewOnly },
        []
      ),
      sour_t_sour_type_id: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      sour_t_sour_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      sour_t_sour_mark: new FormControl(
        { value: undefined, disabled: this.viewOnly },
        []
      ),
      sour_t_place_so: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      sour_t_sour_date: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),

      activity_t_sphere_id: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      activity_t_sphere: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      activity_t_prof: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      activity_t_post: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      activity_t_arrival_dat1: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      activity_t_arrival_dat2: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      activity_t_arrival_dat_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      activity_t_depart_dat1: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      activity_t_depart_dat2: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      activity_t_depart_dat_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      activity_t_ent1_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      activity_t_ent_code: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      activity_t_place: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),

      org_t_org_code: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      // org_t_org_code_id: new FormControl(
      //   { value: null, disabled: this.viewOnly },
      //   []
      // ),
      org_t_org_name: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      org_t_particip: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      org_t_particip_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      org_t_com_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      org_t_join_dat1: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      org_t_join_dat2: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      org_t_join_dat_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      org_t_dismis_dat1: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      org_t_dismis_dat2: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      org_t_dismis_dat_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),

      repress_t_repress_no: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      repress_t_repress_type: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      repress_t_repress_type_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      repress_t_repress_dat1: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      repress_t_repress_dat2: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      repress_t_repress_dat_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      repress_t_reabil_dat1: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      repress_t_reabil_mark: new FormControl(
        { value: undefined, disabled: this.viewOnly },
        []
      ),
      repress_t_reabil_dat2: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      repress_t_reabil_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      repress_t_repress_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      repress_t_place: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),

      text_t_text_line: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),

      court_t_trial_no: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      court_t_inq_name: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      court_t_inq_date1: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      court_t_inq_date2: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      court_t_inq_date_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      court_t_court_name: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      court_t_sentense_date1: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      court_t_sentense_date2: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      court_t_sentense_date_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      court_t_article: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      court_t_sentense: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      court_t_trial_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      court_t_inq_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      court_t_place_i: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      court_t_place_j: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),

      co_t_surname: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      co_t_fname: new FormControl({ value: null, disabled: this.viewOnly }, []),
      co_t_lname: new FormControl({ value: null, disabled: this.viewOnly }, []),
      co_t_code: new FormControl({ value: null, disabled: this.viewOnly }, []),

      help_t_help_typ: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      help_t_help_typ_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      help_t_whom: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      help_t_help_dat1: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      help_t_help_dat2: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      repress_t_rehabil_org: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      repress_t_rehabil_reason_id: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      personal_t_fund: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      personal_t_list_no: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      personal_t_file_no: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      usage_t_us_type_id: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      usage_t_us_date2: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      usage_t_us_date1: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      usage_t_sour_code: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      usage_t_sour_txt: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
    });
    this.yesno = this.globalService.yesno;
    this.yesnoq = this.globalService.yesnoq;
    this.sex = this.globalService.sex;
    this.myControl_birth_place = new FormControl({
      value: null,
      disabled: this.viewOnly,
    });
    this.myControl_nation_id = new FormControl({
      value: null,
      disabled: this.viewOnly,
    });
    this.myControl_residence_code = new FormControl({
      value: null,
      disabled: this.viewOnly,
    });
    this.myControl_sentenses = new FormControl({
      value: null,
      disabled: this.viewOnly,
    });
    this.myControl_sour_type = new FormControl({
      value: null,
      disabled: this.viewOnly,
    });
    try {
      let nations = this.nattService.getAll();
      let origins = this.listoftService.getWithFilter('ORIGIN');
      let educs = this.listoftService.getWithFilter('EDUC');
      let specs = this.listoftService.getWithFilter('SPEC');
      let spheres = this.listoftService.getWithFilter('SPHERE');
      let particips = this.listoftService.getWithFilter('PARTICIP');
      let org_codes = this.listoftService.getWithFilter('ORG_CODE');
      let repress_types = this.listoftService.getWithFilter('REPRESS_TYPE');
      let sentenses = this.listoftService.getWithFilter('SENTENSE');
      let reasons = this.listoftService.getWithFilter('REASON');
      let sour_types = this.listoftService.getWithFilter('SOUR_TYPE');
      let protests = this.listoftService.getWithFilter('PROTEST');
      let help_types = this.listoftService.getWithFilter('HELP_TYP');
      let impris_types = this.listoftService.getWithFilter('IMPRIS_TYPE');
      let rehab_reases = this.listoftService.getWithFilter('REHAB_REAS');
      let us_types = this.listoftService.getWithFilter('US_TYPE');
      this.impris_types = isNullOrUndefined(await impris_types)
        ? []
        : await impris_types;
      this.nations = isNullOrUndefined(await nations) ? [] : await nations;
      this.origins = isNullOrUndefined(await origins) ? [] : await origins;
      this.educs = isNullOrUndefined(await educs) ? [] : await educs;
      this.specs = isNullOrUndefined(await specs) ? [] : await specs;
      this.spheres = isNullOrUndefined(await spheres) ? [] : await spheres;
      this.particips = isNullOrUndefined(await particips)
        ? []
        : await particips;
      this.org_codes = isNullOrUndefined(await org_codes)
        ? []
        : await org_codes;
      this.repress_types = isNullOrUndefined(await repress_types)
        ? []
        : await repress_types;
      this.sentenses = isNullOrUndefined(await sentenses)
        ? []
        : await sentenses;
      this.reasons = isNullOrUndefined(await reasons) ? [] : await reasons;
      this.protests = isNullOrUndefined(await protests) ? [] : await protests;
      this.sour_types = isNullOrUndefined(await sour_types)
        ? []
        : await sour_types;
      this.help_types = isNullOrUndefined(await help_types)
        ? []
        : await help_types;
      this.rehab_reases = isNullOrUndefined(await rehab_reases)
        ? []
        : await rehab_reases;
      this.us_types = isNullOrUndefined(await us_types) ? [] : await us_types;
      this.rehab_reases.sort((a, b) => (a.name_ent < b.name_ent ? -1 : 1));
      this.help_types.sort((a, b) => (a.name_ent < b.name_ent ? -1 : 1));
      this.sour_types.sort((a, b) => (a.name_ent < b.name_ent ? -1 : 1));
      this.protests.sort((a, b) => (a.name_ent < b.name_ent ? -1 : 1));
      this.reasons.sort((a, b) => (a.name_ent < b.name_ent ? -1 : 1));
      this.repress_types.sort((a, b) => (a.name_ent < b.name_ent ? -1 : 1));
      this.org_codes.sort((a, b) => (a.name_ent < b.name_ent ? -1 : 1));
      this.particips.sort((a, b) => (a.name_ent < b.name_ent ? -1 : 1));
      this.spheres.sort((a, b) => (a.name_ent < b.name_ent ? -1 : 1));
      this.nations.sort((a, b) => (a.nation_rmk < b.nation_rmk ? -1 : 1));
      this.specs.sort((a, b) => (a.name_ent < b.name_ent ? -1 : 1));
      this.educs.sort((a, b) => (a.name_ent < b.name_ent ? -1 : 1));
      this.origins.sort((a, b) => (a.name_ent < b.name_ent ? -1 : 1));
      this.us_types.sort((a, b) => (a.name_ent < b.name_ent ? -1 : 1));
      this.filteredSour_type = this.myControl_sour_type.valueChanges.pipe(
        startWith(''),
        map((sour_type) =>
          sour_type ? this._filter(sour_type) : [...this.sour_types]
        )
      );
      this.filteredSentenses = this.myControl_sentenses.valueChanges.pipe(
        startWith(''),
        map((sentense) =>
          sentense ? this._filter(sentense) : [...this.sentenses]
        )
      );
    } catch (err) {
      this.globalService.exceptionHandling(err);
    } finally {
      this.filteredNations = this.myControl_nation_id.valueChanges.pipe(
        startWith(NatT),
        map((value) => {
          if (isNullOrUndefined(value)) {
            return null;
          }
          return typeof value === 'string' ? value : value.name;
        }),
        map((name) => (name ? this._filterNatt(name) : [...this.nations]))
      );
    }
  }

  async onAdd() {
    let res = [];
    // if (!isNullOrUndefined(this.myControl_birth_place.value) && this.myControl_birth_place.value !== '') {
    // 	if (!isNullOrUndefined(this.myControl_birth_place.value.place_code)) {
    // 		this.filterForm.patchValue({
    // 			'personal_t_birth_place': this.myControl_birth_place.value.place_code
    // 		});
    // 	} else {
    // 		let dialogRef = this.dialog.open(DialogYesNoComponent, {
    // 			data: {
    // 				title: 'Введено не корректное место рождения.',
    // 				question: `Продолжить?`
    // 			}
    // 		});
    // 		await new Promise((resolve, reject) => {
    // 			dialogRef.afterClosed().subscribe(async result => {
    // 				if (result === true) {
    // 					this.myControl_birth_place.reset();
    // 					this.filterForm.patchValue({
    // 						'personal_t_birth_place': null
    // 					});
    // 					resolve();
    // 				} else {
    // 					return;
    // 				}
    // 			});
    // 		});
    // 		this.myControl_birth_place.reset();
    // 		this.filterForm.patchValue({
    // 			'personal_t_birth_place': null
    // 		});
    // 	}
    // } else {
    // 	this.myControl_birth_place.reset();
    // 	this.filterForm.patchValue({
    // 		'personal_t_birth_place': null
    // 	});
    // }
    // if (!isNullOrUndefined(this.myControl_residence_code.value) && this.myControl_residence_code.value !== '') {
    // 	if (!isNullOrUndefined(this.myControl_residence_code.value.place_code)) {
    // 		this.filterForm.patchValue({
    // 			'personal_t_place': this.myControl_residence_code.value.place_code
    // 		});
    // 	} else {
    // 		let dialogRef = this.dialog.open(DialogYesNoComponent, {
    // 			data: {
    // 				title: 'Введено не корректное место жительства.',
    // 				question: `Продолжить?`
    // 			}
    // 		});
    // 		await new Promise((resolve, reject) => {
    // 			dialogRef.afterClosed().subscribe(async result => {
    // 				if (result === true) {
    // 					this.myControl_residence_code.reset();
    // 					this.filterForm.patchValue({
    // 						'personal_t_place': null
    // 					});
    // 					resolve();
    // 				} else {
    // 					return;
    // 				}
    // 			});
    // 		});
    // 		this.myControl_residence_code.reset();
    // 		this.filterForm.patchValue({
    // 			'personal_t_place': null
    // 		});
    // 	}
    // } else {
    // 	this.myControl_residence_code.reset();
    // 	this.filterForm.patchValue({
    // 		'personal_t_place': null
    // 	});
    // }
    if (!isNullOrUndefined(this.myControl_nation_id.value)) {
      this.filterForm.patchValue({
        personal_t_nation: this.myControl_nation_id.value.nation_rmk,
      });
    }
    if (
      !isNullOrUndefined(this.myControl_sentenses.value) &&
      this.myControl_sentenses.value !== ''
    ) {
      this.filterForm.patchValue({
        court_t_sentense: this.myControl_sentenses.value,
      });
    }
    // if (
    //   !isNullOrUndefined(this.myControl_sour_type.value) &&
    //   this.myControl_sour_type.value !== ''
    // ) {
    //   this.filterForm.patchValue({
    //     sour_t_sour_type: this.myControl_sour_type.value,
    //   });
    // }
    // if (this.filterForm.value['personal_t_type_r'] === true) {
    //   this.filterForm.patchValue({
    //     'personal_t_type_r': '*'
    //   });
    // } else {
    //   this.filterForm.patchValue({
    //     'personal_t_type_r': null
    //   });
    // }
    // if (this.filterForm.value['personal_t_type_i'] === true) {
    //   this.filterForm.patchValue({
    //     'personal_t_type_i': '*'
    //   });
    // } else {
    //   this.filterForm.patchValue({
    //     'personal_t_type_i': null
    //   });
    // }
    // if (this.filterForm.value['personal_t_type_a'] === true) {
    //   this.filterForm.patchValue({
    //     'personal_t_type_a': '*'
    //   });
    // } else {
    //   this.filterForm.patchValue({
    //     'personal_t_type_a': null
    //   });
    // }
    if (!isNullOrUndefined(this.filterForm.value.personal_t_code)) {
      this.filterForm.patchValue({
        personal_t_code: `${this.filterForm.value.personal_t_code}`,
      });
    }
    try {
      this.spinnerService.show();
      let obj = this.filterForm.value;
      let count = 0;
      for (let prop in obj) {
        if (obj.hasOwnProperty(prop)) {
          if (isNullOrUndefined(obj[prop]) || obj[prop] === '') {
            switch (prop) {
              case 'personal_t_type_r':
              case 'personal_t_type_i':
              case 'personal_t_type_a':
              case 'personal_t_mark':
              case 'sour_t_mark_mem':
              case 'repress_t_reabil_mark':
                if (obj[prop] !== undefined) {
                  count++;
                }
                break;
              default:
                delete obj[prop];
                break;
            }
          } else {
            count++;
          }
        }
      }
      if (count === 1) {
        this.openSnackBar(`Не выбрано ни одного фильтра`, 'Ok');
        return;
      }
      let _res = this.selectionitemService.addselectionitem(obj);
      res = isNullOrUndefined(await _res) ? [] : await _res;
      if (res.length > 0) {
        this.goBack();
      }
    } catch (err) {
      this.globalService.exceptionHandling(err);
    } finally {
      this.spinnerService.hide();
      this.openSnackBar(`Добавлено ${res.length} персоналий`, 'Ok');
    }
  }

  goBack() {
    this.router.navigate([`/sys/person/selectionedit/${this.id}`]);
  }

  resetBirth1() {
    this.filterForm.patchValue({
      personal_t_birth_from: '',
    });
  }
  resetUsage_t_us_date1() {
    this.filterForm.patchValue({
      usage_t_us_date1: '',
    });
  }
  resetUsage_t_us_date2() {
    this.filterForm.patchValue({
      usage_t_us_date2: '',
    });
  }

  resetBirth2() {
    this.filterForm.patchValue({
      personal_t_birth_to: '',
    });
  }

  resetDeath1() {
    this.filterForm.patchValue({
      personal_t_death1: '',
    });
  }

  resetDeath2() {
    this.filterForm.patchValue({
      personal_t_death2: '',
    });
  }

  resetArrival_dat1() {
    this.filterForm.patchValue({
      impris_t_arrival_dat1: '',
    });
  }
  resetArrival_dat2() {
    this.filterForm.patchValue({
      impris_t_arrival_dat2: '',
    });
  }
  resetLeave_dat1() {
    this.filterForm.patchValue({
      impris_t_leave_dat1: '',
    });
  }
  resetLeave_dat2() {
    this.filterForm.patchValue({
      impris_t_leave_dat2: '',
    });
  }
  resetSour_dat1() {
    this.filterForm.patchValue({
      sour_t_sour_date: '',
    });
  }
  resetActivity_t_arrival_dat1() {
    this.filterForm.patchValue({
      activity_t_arrival_dat1: '',
    });
  }
  resetActivity_t_arrival_dat2() {
    this.filterForm.patchValue({
      activity_t_arrival_dat2: '',
    });
  }
  resetActivity_t_depart_dat1() {
    this.filterForm.patchValue({
      activity_t_depart_dat1: '',
    });
  }
  resetActivity_t_depart_dat2() {
    this.filterForm.patchValue({
      activity_t_depart_dat2: '',
    });
  }
  resetOrg_t_join_dat1() {
    this.filterForm.patchValue({
      org_t_join_dat1: '',
    });
  }
  resetOrg_t_join_dat2() {
    this.filterForm.patchValue({
      org_t_join_dat2: '',
    });
  }
  resetOrg_t_dismis_dat1() {
    this.filterForm.patchValue({
      org_t_dismis_dat1: '',
    });
  }
  resetOrg_t_dismis_dat2() {
    this.filterForm.patchValue({
      org_t_dismis_dat2: '',
    });
  }
  resetRepress_t_repress_dat1() {
    this.filterForm.patchValue({
      repress_t_repress_dat1: '',
    });
  }
  resetRepress_t_repress_dat2() {
    this.filterForm.patchValue({
      repress_t_repress_dat2: '',
    });
  }
  resetRepress_t_reabil_dat1() {
    this.filterForm.patchValue({
      repress_t_reabil_dat1: '',
    });
  }
  resetRepress_t_reabil_dat2() {
    this.filterForm.patchValue({
      repress_t_reabil_dat2: '',
    });
  }
  resethelp_t_help_dat1() {
    this.filterForm.patchValue({
      help_t_help_dat1: '',
    });
  }
  resethelp_t_help_dat2() {
    this.filterForm.patchValue({
      help_t_help_dat2: '',
    });
  }
  resetcourt_t_inq_date1() {
    this.filterForm.patchValue({
      court_t_inq_date1: '',
    });
  }
  resetcourt_t_inq_date2() {
    this.filterForm.patchValue({
      court_t_inq_date2: '',
    });
  }
  resetcourt_t_sentense_date1() {
    this.filterForm.patchValue({
      court_t_sentense_date1: '',
    });
  }
  resetcourt_t_sentense_date2() {
    this.filterForm.patchValue({
      court_t_sentense_date2: '',
    });
  }

  displayFn_birth_place(place?: PlaceT): string | undefined {
    return place
      ? place.place_num + '. ' + place.place_code + '. ' + place.place_name
      : undefined;
  }

  displayFn_nation_id(nat?: NatT): string | undefined {
    return nat ? nat.nation_rmk : undefined;
  }

  inputLink_birth_place() {
    clearTimeout(this.timerId);
    this.timerId = setTimeout(async () => {
      this.placets = await this.placetService.getWithFilterLimit(
        100,
        this.myControl_birth_place.value
      );
      if (!isNullOrUndefined(this.placets)) {
        try {
          this.filteredPlacets = this.myControl_birth_place.valueChanges.pipe(
            startWith<string | PlaceT>(''),
            map((value) =>
              typeof value === 'string' ? value : value.place_name
            ),
            map((name) => (name ? this._filter(name) : [...this.placets]))
          );
        } catch {}
      }
    }, 500);
  }

  inputLink_residence_code() {
    clearTimeout(this.timerId);
    this.timerId = setTimeout(async () => {
      this.placets = await this.placetService.getWithFilterLimit(
        100,
        this.myControl_residence_code.value
      );
      if (!isNullOrUndefined(this.placets)) {
        try {
          this.filteredPlacets = this.myControl_residence_code.valueChanges.pipe(
            startWith<string | PlaceT>(''),
            map((value) =>
              typeof value === 'string' ? value : value.place_name
            ),
            map((name) => (name ? this._filter(name) : [...this.placets]))
          );
        } catch {}
      }
    }, 500);
  }

  private _filter(name: string): PlaceT[] {
    const filterValue = name.toLowerCase();
    return isNullOrUndefined(this.placets)
      ? null
      : this.placets.filter(
          (placet) =>
            placet.place_name.toLowerCase().indexOf(filterValue) !== -1
        );
  }

  private _filterNatt(value: string): NatT[] {
    const filterValue = value.toLowerCase();
    return this.nations.filter((option) =>
      option.nation.toLowerCase().includes(filterValue)
    );
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 9000,
    });
  }
}
