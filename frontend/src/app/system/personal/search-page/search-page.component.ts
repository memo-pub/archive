import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { FieldsearchService } from '../../shared/services/fieldsearch.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { isNullOrUndefined } from 'util';
import { GlobalService } from 'src/app/shared/services/global.service';
import { SelectionService } from '../../shared/services/selection.service';
import { DialogSaveToSelectionitemComponent } from './dialog-save-to-selectionitem/dialog-save-to-selectionitem.component';
import { MatDialog } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';
import { DialogSetstringComponent } from '../../shared/components/dialog-setstring/dialog-setstring.component';
import { SelectionitemService } from '../../shared/services/selectionitem.service';

@Component({
  selector: 'app-search-page',
  templateUrl: './search-page.component.html',
  styleUrls: ['./search-page.component.css'],
})
export class SearchPageComponent implements OnInit {
  searchStr = '';
  results: any[];
  tables: any[];
  selections: Selection[];

  constructor(
    private spinnerService: NgxSpinnerService,
    private globalService: GlobalService,
    private fieldsearchService: FieldsearchService,
    private selectionService: SelectionService,
    private selectionitemService: SelectionitemService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar
  ) {}

  async ngOnInit() {
    this.spinnerService.show();
    try {
      let selections = this.selectionService.getAll();
      this.selections = !(await selections) ? [] : await selections;
    } catch (e) {
      this.globalService.exceptionHandling(e);
    } finally {
      this.spinnerService.hide();
    }
    this.tables = this.globalService.tables;
  }

  async updateData() {
    this.spinnerService.show();
    try {
      let results = this.fieldsearchService.getByStr(this.searchStr);
      this.results = isNullOrUndefined(await results) ? [] : await results;
    } catch (err) {
      this.globalService.exceptionHandling(err);
    } finally {
      this.spinnerService.hide();
    }
  }

  getTableByKey(key: string) {
    let res = this.tables.find((table) => table.key === key);
    return isNullOrUndefined(res) ? null : res.value;
  }

  onSetFilter(field) {
    let dialogRef = this.dialog.open(DialogSaveToSelectionitemComponent, {
      panelClass: 'app-dialog-extend',
      data: {
        selections: this.selections,
        selectionName: '',
        selectionId: null,
      },
    });
    dialogRef.afterClosed().subscribe(async (result) => {
      if (result !== null || result !== undefined) {
        let obj = { selection_id: null };
        if (result === false) {
          let dialogRef1 = this.dialog.open(DialogSetstringComponent, {
            data: {
              title: 'Введите название выборки',
              str: '',
            },
          });
          dialogRef1.afterClosed().subscribe(async (result) => {
            if (typeof result === 'string') {
              this.spinnerService.show();
              try {
                let res = await this.selectionService.postOne({
                  name: result,
                });
                obj.selection_id = res.id;
                obj[field.filter] = this.searchStr;
                await this.selectionitemService.addselectionitem(obj);
                let selections = this.selectionService.getAll();
                this.selections = !(await selections) ? [] : await selections;
              } catch (err) {
                this.globalService.exceptionHandling(err);
              } finally {
                this.spinnerService.hide();
              }
            }
          });
        } else {
          obj.selection_id = result.id;
          obj[field.filter] = this.searchStr;
          this.spinnerService.show();
          try {
            await this.selectionitemService.addselectionitem(obj);
            let selections = this.selectionService.getAll();
            this.selections = !(await selections) ? [] : await selections;
          } catch (err) {
            this.globalService.exceptionHandling(err);
          } finally {
            this.spinnerService.hide();
          }
        }
      }
    });
  }
}
