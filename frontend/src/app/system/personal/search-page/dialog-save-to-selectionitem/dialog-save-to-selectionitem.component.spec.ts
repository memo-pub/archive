    import { async, ComponentFixture, TestBed } from '@angular/core/testing';
    import { RouterTestingModule } from '@angular/router/testing';
    import { MatSnackBarModule } from '@angular/material/snack-bar';
    import { HttpClientTestingModule } from '@angular/common/http/testing';
    import {
    MatDialogModule,
    MatDialogRef,
    MAT_DIALOG_DATA,
    } from '@angular/material/dialog';
    import { MatAutocompleteModule } from '@angular/material/autocomplete';

import { DialogSaveToSelectionitemComponent } from './dialog-save-to-selectionitem.component';

describe('DialogSaveToSelectionitemComponent', () => {
  let component: DialogSaveToSelectionitemComponent;
  let fixture: ComponentFixture<DialogSaveToSelectionitemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DialogSaveToSelectionitemComponent],
      imports: [
        RouterTestingModule,
        MatSnackBarModule,
        HttpClientTestingModule,
        MatDialogModule,
        MatAutocompleteModule,
      ],
      providers: [
        {
          provide: MatDialogRef,
          useValue: {},
        },
        { provide: MAT_DIALOG_DATA, useValue: {} },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogSaveToSelectionitemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
