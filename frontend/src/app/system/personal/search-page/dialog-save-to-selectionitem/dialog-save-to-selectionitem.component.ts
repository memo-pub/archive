import { Component, Inject, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { map } from 'rxjs/internal/operators/map';
import { startWith } from 'rxjs/operators';

@Component({
  selector: 'app-dialog-save-to-selectionitem',
  templateUrl: './dialog-save-to-selectionitem.component.html',
  styleUrls: ['./dialog-save-to-selectionitem.component.css'],
})
export class DialogSaveToSelectionitemComponent implements OnInit {
  myControl = new FormControl();
  filteredOptions: Observable<any[]>;

  constructor(
    public dialogRef: MatDialogRef<DialogSaveToSelectionitemComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {
    this.filteredOptions = this.myControl.valueChanges.pipe(
      startWith(''),
      map((value) => (typeof value === 'string' ? value : value.name)),
      map((name) => (name ? this._filter(name) : this.data.selections.slice()))
    );
  }

  isValid() {
    return false;
  }

  displayFn(user: any): string {
    return user && user.name ? user.name : '';
  }

  private _filter(name: string): any[] {
    const filterValue = name.toLowerCase();

    return this.data.selections.filter(
      (option) => option.name.toLowerCase().indexOf(filterValue) === 0
    );
  }

  onSave() {
    this.dialogRef.close(this.myControl.value);
  }
}
