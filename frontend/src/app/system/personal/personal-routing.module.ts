import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PersonalComponent } from './personal.component';
import { NationEditComponent } from './nation-edit/nation-edit.component';
import { NationListComponent } from './nation-list/nation-list.component';
import { PlacetListComponent } from './placet-list/placet-list.component';
import { PlacetEditComponent } from './placet-edit/placet-edit.component';
import { ListoftEditComponent } from './listoft-edit/listoft-edit.component';
import { ListoftListComponent } from './listoft-list/listoft-list.component';
import { PersonalEditComponent } from './personal-edit/personal-edit.component';
import { PersonalListComponent } from './personal-list/personal-list.component';
import { OrgtEditComponent } from './personal-edit/orgt-edit/orgt-edit.component';
import { ActivitytEditComponent } from './personal-edit/activityt-edit/activityt-edit.component';
import { RepresstEditComponent } from './personal-edit/represst-edit/represst-edit.component';
import { CourttEditComponent } from './personal-edit/represst-edit/courtt-edit/courtt-edit.component';
import { ImpristEditComponent } from './personal-edit/represst-edit/courtt-edit/imprist-edit/imprist-edit.component';
import { HelptEditComponent } from './personal-edit/helpt-edit/helpt-edit.component';
import { SourtEditComponent } from './personal-edit/sourt-edit/sourt-edit.component';
import { CanDeactivateGuard } from 'src/app/shared/services/can-deactivate.guard';
import { SelectionListComponent } from './selection-list/selection-list.component';
import { SelectionEditComponent } from './selection-edit/selection-edit.component';
import { SelectionitemEditComponent } from './selection-edit/selectionitem-edit/selectionitem-edit.component';
import { SearchPageComponent } from './search-page/search-page.component';
import { NumberPageComponent } from './number-page/number-page.component';
import { StatListComponent } from './stat-list/stat-list.component';
import { StatEditComponent } from './stat-edit/stat-edit.component';
import { StatItemComponent } from './stat-item/stat-item.component';
import { DocumentListComponent } from './document-list/document-list.component';
import { DocumentEditComponent } from './document-edit/document-edit.component';

const routes: Routes = [
  {
    path: '',
    component: PersonalComponent,
    children: [
      {
        path: 'personaltedit/:id',
        component: PersonalEditComponent,
        canDeactivate: [CanDeactivateGuard],
      },
      { path: 'orgtedit/:id', component: OrgtEditComponent },
      { path: 'activitytedit/:id', component: ActivitytEditComponent },
      { path: 'represstedit/:id', component: RepresstEditComponent },
      { path: 'courttedit/:id', component: CourttEditComponent },
      { path: 'impristedit/:id', component: ImpristEditComponent },
      { path: 'helptedit/:id', component: HelptEditComponent },
      { path: 'sourtedit/:id', component: SourtEditComponent },
      { path: 'personaltlist', component: PersonalListComponent },
      { path: 'nationedit/:id', component: NationEditComponent },
      { path: 'nationlist', component: NationListComponent },
      { path: 'listoftedit/:id', component: ListoftEditComponent },
      { path: 'listoftlist', component: ListoftListComponent },
      { path: 'placetedit/:id', component: PlacetEditComponent },
      { path: 'placetlist', component: PlacetListComponent },
      { path: 'selectionedit/:id', component: SelectionEditComponent },
      { path: 'selectionlist', component: SelectionListComponent },
      { path: 'selectionitemedit/:id', component: SelectionitemEditComponent },
      { path: 'stat', component: StatListComponent },
      { path: 'statedit/:id', component: StatEditComponent },
      { path: 'stat/:id', component: StatItemComponent },
      { path: 'search', component: SearchPageComponent },
      { path: 'number', component: NumberPageComponent },
      { path: 'doclist', component: DocumentListComponent },
      {
        path: 'doc',
        component: DocumentEditComponent,
      },
      {
        path: 'doc/:id',
        component: DocumentEditComponent,
        canDeactivate: [CanDeactivateGuard],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PersonalRoutingModule {}
