import {
  Component,
  OnInit,
  AfterContentChecked,
  HostListener,
  OnDestroy,
  ViewEncapsulation,
} from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PlaceT } from 'src/app/shared/models/placet.model';
import { Observable, Subscription } from 'rxjs';
import { PersonalT } from 'src/app/shared/models/personalt.model';
import { AuthService } from 'src/app/shared/services/auth.service';
import { PersonaltService } from 'src/app/system/shared/services/personalt.service';
import { ListoftService } from 'src/app/system/shared/services/listoft.service';
import { PlacetService } from 'src/app/system/shared/services/placet.service';
import { ImpristService } from 'src/app/system/shared/services/imprist.service';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CoimptService } from 'src/app/system/shared/services/coimpt.service';
import { isNullOrUndefined } from 'util';
import { DialogYesNoComponent } from 'src/app/system/shared/components/dialog-yes-no/dialog-yes-no.component';
import { DialogSaveNoComponent } from 'src/app/system/shared/components/dialog-save-no/dialog-save-no.component';
import { startWith, map } from 'rxjs/operators';
import { ListOfT } from 'src/app/shared/models/listoft.model';
import { GlobalService } from 'src/app/shared/services/global.service';
import { DialogPersonalEditComponent } from '../../../dialog-personal-edit/dialog-personal-edit.component';
import { coimpT_width } from 'src/app/shared/models/coimpt.model';
import { imprisT_width } from 'src/app/shared/models/imprist.model';
import { DateparseService } from 'src/app/shared/services/dateparse.service';

@Component({
  selector: 'app-imprist-edit',
  templateUrl: './imprist-edit.component.html',
  styleUrls: ['./imprist-edit.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class ImpristEditComponent
  implements OnInit, AfterContentChecked, OnDestroy {
  constructor(
    public dateparseService: DateparseService,
    private authService: AuthService,
    private globalService: GlobalService,
    private personaltService: PersonaltService,
    private listoftService: ListoftService,
    private placetService: PlacetService,
    private impristService: ImpristService,
    private coimptService: CoimptService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private spinnerService: NgxSpinnerService,
    public dialog: MatDialog,
    public snackBar: MatSnackBar
  ) {
    // this.activeRouterObserver.unsubscribe();
    this.activeRouterObserver = this.activatedRoute.params.subscribe(
      (param) => {
        let str = param.id.split('_');
        if (str[0] === 'new') {
          this.intType = 'newdoc';
          this.selectedCourttid = +str[3];
          this.selectedRepid = +str[2];
          this.selectedPersonalcode = +str[1];
          this.viewOnly = false;
        } else {
          if (str[1] === 'v') {
            this.id = +str[5];
            this.selectedCourttid = +str[4];
            this.selectedRepid = +str[3];
            this.selectedPersonalcode = +str[2];
            this.intType = 'editdoc';
            this.viewOnly = true;
          } else {
            this.id = +str[4];
            this.selectedCourttid = +str[3];
            this.selectedRepid = +str[2];
            this.selectedPersonalcode = +str[1];
            this.intType = 'editdoc';
            this.viewOnly = false;
          }
        }
        this.viewOnly =
          this.viewOnly === false
            ? this.authService.getCurUser().usertype === 4
            : true;
        if (this.inited) {
          this.getData();
        }
      }
    );
    // this.globalService.routerObserver.unsubscribe();
    this.globalService.routerObserver = router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        if (val.url.indexOf('impristedit') !== -1) {
          this.globalService.breadcrumbs.length = 0;
          this.globalService.breadcrumbs.push(
            {
              label: 'персоналии',
              url:
                this.viewOnly === true
                  ? `/sys/person/personaltedit/v_${this.selectedPersonalcode}`
                  : `/sys/person/personaltedit/${this.selectedPersonalcode}`,
            },
            {
              label: 'репрессия',
              url:
                this.viewOnly === true
                  ? `/sys/person/represstedit/edit_v_${this.selectedPersonalcode}_${this.selectedRepid}`
                  : `/sys/person/represstedit/edit_${this.selectedPersonalcode}_${this.selectedRepid}`,
            },
            {
              label: 'следствие и судебный процесс',
              url:
                this.viewOnly === true
                  ? `/sys/person/courttedit/edit_v_${this.selectedPersonalcode}_${this.selectedRepid}_${this.selectedCourttid}`
                  : `/sys/person/courttedit/edit_${this.selectedPersonalcode}_${this.selectedRepid}_${this.selectedCourttid}`,
            },
            { label: 'заключение' }
          );
          this.breadcrumbs = this.globalService.breadcrumbs;
        }
      }
    });
  }
  coimpT_width = coimpT_width;
  imprisT_width = imprisT_width;

  timerId;
  id: number;
  intType: string | any = ''; // тип интерфейса
  viewOnly: boolean;
  selectedPersonalcode: number;
  selectedPersonal: PersonalT;
  selectedRepid: number;
  selectedCourttid: number;

  impristForm: FormGroup;
  coimptForms: FormGroup[] = [];

  coimptidTodel: number[] = [];

  placets: PlaceT[];
  filteredPlacets: Observable<PlaceT[]>;

  personalts: PersonalT[];
  filteredPersonalts: Observable<PersonalT[]>;

  myControl_human_code: FormControl[] = [];
  myControl_geoplace_code: FormControl;

  impris_types: ListOfT[];
  reasons: ListOfT[];
  protests: ListOfT[];

  valid: boolean;

  breadcrumbs = [];
  activeRouterObserver: Subscription;

  newnom: number;

  first = false;
  last = false;
  selectedImprists: any[] = [];
  inited = false;

  @HostListener('window:keydown', ['$event'])
  onKeyPress(event: KeyboardEvent) {
    if ((event.ctrlKey || event.metaKey) && event.keyCode == 83) {
      event.preventDefault();
      if (!this.viewOnly && this.valid && this.intType !== 'myprofile') {
        this.onSaveForm();
      }
    }
  }

  ngOnDestroy(): void {
    this.globalService.routerObserver.unsubscribe();
    // this.activeRouterObserver.unsubscribe();
  }

  onMove(str) {
    let idx = this.selectedImprists.findIndex((item) => item.id === this.id);
    if (str === 'next') {
      let id = this.selectedImprists[idx + 1].id;
      let url = this.router.url;
      let arr = url.split('_');
      arr[arr.length - 1] = String(id);
      url = arr.join('_');
      this.router.navigate([url]);
    } else if (str === 'previous') {
      let id = this.selectedImprists[idx - 1].id;
      let url = this.router.url;
      let arr = url.split('_');
      arr[arr.length - 1] = String(id);
      url = arr.join('_');
      this.router.navigate([url]);
    }
  }

  async ngOnInit() {
    this.impristForm = new FormGroup({
      id: new FormControl({ value: null, disabled: this.viewOnly }, []),
      crt_id: new FormControl({ value: null, disabled: this.viewOnly }, []),
      personal_code: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      impris_no: new FormControl({ value: null, disabled: true }, [
        Validators.required,
      ]),
      // impris_type: new FormControl(
      //   { value: null, disabled: this.viewOnly },
      //   []
      // ),
      impris_type_id: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      prison_name: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      // arrival_dat: new FormControl({ value: null, disabled: true }, []),
      arrival_dat_begin: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      arrival_dat_end: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      arrival_dat_begin_end: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      // leave_dat: new FormControl({ value: null, disabled: true }, []),
      leave_dat_begin: new FormControl({ value: null, disabled: true }, []),
      leave_dat_end: new FormControl({ value: null, disabled: true }, []),
      leave_dat_begin_end: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      // reason: new FormControl({ value: null, disabled: this.viewOnly }, []),
      reason_id: new FormControl({ value: null, disabled: this.viewOnly }, []),
      work: new FormControl({ value: null, disabled: this.viewOnly }, []),
      // protest: new FormControl({ value: null, disabled: this.viewOnly }, []),
      protest_id: new FormControl({ value: null, disabled: this.viewOnly }, []),
      work_rmk: new FormControl({ value: null, disabled: this.viewOnly }, []),
      protest_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      prison_name_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      prison_place_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      arrival_dat_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      leave_dat_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      reason_rmk: new FormControl({ value: null, disabled: this.viewOnly }, []),
      place_num: new FormControl({ value: null, disabled: this.viewOnly }, []),
      general_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
    });
    this.myControl_geoplace_code = new FormControl({
      value: null,
      disabled: this.viewOnly,
    });
    try {
      let impris_types = this.listoftService.getWithFilter('IMPRIS_TYPE');
      let reasons = this.listoftService.getWithFilter('REASON');
      let protests = this.listoftService.getWithFilter('PROTEST');
      this.impris_types = isNullOrUndefined(await impris_types)
        ? []
        : await impris_types;
      this.reasons = isNullOrUndefined(await reasons) ? [] : await reasons;
      this.protests = isNullOrUndefined(await protests) ? [] : await protests;
      this.protests.sort((a, b) => (a.name_ent < b.name_ent ? -1 : 1));
      this.reasons.sort((a, b) => (a.name_ent < b.name_ent ? -1 : 1));
      this.impris_types.sort((a, b) => (a.name_ent < b.name_ent ? -1 : 1));
    } catch (err) {
      if (err.status === 401) {
        console.error(`Ошибка авторизации`);
        this.authService.logout();
      } else {
        console.error(`Ошибка ${err}`);
      }
    }
    await this.getData();
    this.inited = true;
  }

  ngAfterContentChecked(): void {
    this.isValid();
  }

  async getData() {
    this.spinnerService.show();
    let selectedPersonal;
    try {
      selectedPersonal = this.personaltService.getBycode(
        this.selectedPersonalcode
      );
    } catch (err) {
      if (err.status === 401) {
        console.error(`Ошибка авторизации`);
        this.authService.logout();
      } else {
        console.error(`Ошибка ${err}`);
      }
    }
    this.coimptForms.length = 0;
    switch (this.intType) {
      case 'editdoc': // редактирование
        try {
          this.myControl_human_code.length = 0;
          let selectedImprest = await this.impristService.getOneById(this.id);
          this.selectedImprists = await this.impristService.getByCrtid(
            selectedImprest.crt_id
          );
          this.first = false;
          this.last = false;
          let idx = this.selectedImprists.findIndex(
            (item) => item.id === this.id
          );
          if (idx === 0) {
            this.first = true;
          }
          if (idx === this.selectedImprists.length - 1) {
            this.last = true;
          }
          this.impristForm.reset();
          this.impristForm.patchValue({
            personal_code: selectedImprest.personal_code,
            crt_id: selectedImprest.crt_id,
            impris_no: selectedImprest.impris_no,
            // impris_type: selectedImprest.impris_type,
            impris_type_id: selectedImprest.impris_type_id,
            prison_name: selectedImprest.prison_name,
            // arrival_dat: selectedImprest.arrival_dat,
            arrival_dat_begin: selectedImprest.arrival_dat_begin,
            arrival_dat_end: selectedImprest.arrival_dat_end,
            // leave_dat: selectedImprest.leave_dat,
            leave_dat_begin: selectedImprest.leave_dat_begin,
            leave_dat_end: selectedImprest.leave_dat_end,
            // reason: selectedImprest.reason,
            reason_id: selectedImprest.reason_id,
            work: selectedImprest.work,
            // protest: selectedImprest.protest,
            protest_id: selectedImprest.protest_id,
            work_rmk: selectedImprest.work_rmk,
            protest_rmk: selectedImprest.protest_rmk,
            prison_name_rmk: selectedImprest.prison_name_rmk,
            prison_place_rmk: selectedImprest.prison_place_rmk,
            arrival_dat_rmk: selectedImprest.arrival_dat_rmk,
            leave_dat_rmk: selectedImprest.leave_dat_rmk,
            reason_rmk: selectedImprest.reason_rmk,
            place_num: selectedImprest.place_num,
            general_rmk: selectedImprest.general_rmk,
          });
          this.dateparseService.setSourDate(
            this.impristForm,
            'leave_dat_begin_end',
            selectedImprest.leave_dat_begin,
            selectedImprest.leave_dat_end
          );
          this.dateparseService.setSourDate(
            this.impristForm,
            'arrival_dat_begin_end',
            selectedImprest.arrival_dat_begin,
            selectedImprest.arrival_dat_end
          );
          if (!isNullOrUndefined(selectedImprest.place_num)) {
            let _geoplace_code = await this.placetService.getByPlace_num(
              selectedImprest.place_num
            );
            this.myControl_geoplace_code.reset();
            if (
              !isNullOrUndefined(_geoplace_code) &&
              _geoplace_code.length > 0
            ) {
              this.myControl_geoplace_code.setValue(_geoplace_code[0]);
            }
          }

          let _selectedCoimpts = this.coimptService.getByImpid(
            selectedImprest.id
          );
          let selectedCoimpts = isNullOrUndefined(await _selectedCoimpts)
            ? []
            : await _selectedCoimpts;
          this.coimptForms.length = 0;
          for (let j = 0; j < selectedCoimpts.length; j++) {
            this.coimptForms.push(
              new FormGroup({
                id: new FormControl(
                  { value: null, disabled: this.viewOnly },
                  []
                ),
                imp_id: new FormControl(
                  { value: null, disabled: this.viewOnly },
                  []
                ),
                human_code: new FormControl(
                  { value: null, disabled: this.viewOnly },
                  [Validators.required]
                ),
                role: new FormControl(
                  { value: null, disabled: this.viewOnly },
                  []
                ),
                role_rmk: new FormControl(
                  { value: null, disabled: this.viewOnly },
                  []
                ),
                sign_mark: new FormControl(
                  { value: null, disabled: this.viewOnly },
                  []
                ),
              })
            );
            this.myControl_human_code.push(
              new FormControl({ value: null, disabled: this.viewOnly })
            );
            this.coimptForms[j].patchValue({
              id: selectedCoimpts[j].id,
              imp_id: selectedCoimpts[j].imp_id,
              human_code: selectedCoimpts[j].human_code,
              role: selectedCoimpts[j].role,
              role_rmk: selectedCoimpts[j].role_rmk,
              sign_mark: selectedCoimpts[j].sign_mark,
            });
            if (!isNullOrUndefined(selectedCoimpts[j].human_code)) {
              let person = await this.personaltService.getBycode(
                selectedCoimpts[j].human_code
              );
              if (!isNullOrUndefined(person) && person.length > 0) {
                this.myControl_human_code[j].setValue(person[0]);
              }
            }
          }
        } catch (err) {
          if (err.status === 401) {
            console.error(`Ошибка авторизации`);
            this.authService.logout();
          } else {
            console.error(`Ошибка ${err}`);
          }
        } finally {
          this.spinnerService.hide();
        }
        break;
      case 'newdoc': // создание
        this.last = true;
        this.first = true;
        try {
          let _imprs = this.impristService.getByCrtid(this.selectedCourttid);
          let imprs = isNullOrUndefined(await _imprs) ? [] : await _imprs;
          let num: number;
          if (imprs.length === 0) {
            num = 1;
          } else {
            imprs.sort((a, b) => b.impris_no - a.impris_no);
            num = imprs[0].impris_no + 1;
          }
          this.impristForm.reset();
          this.impristForm.patchValue({
            impris_no: this.newnom || num,
          });
        } catch (err) {
          if (err.status === 401) {
            console.error(`Ошибка авторизации`);
            this.authService.logout();
          } else {
            console.error(`Ошибка ${err}`);
          }
        } finally {
          this.spinnerService.hide();
        }
        break;
      default:
    }
    this.selectedPersonal = (await selectedPersonal)[0];
    this.spinnerService.hide();
    // } catch (err) {
    //   if (err.status === 401) {
    //     console.error(`Ошибка авторизации`);
    //     this.authService.logout();
    //   } else {
    //     console.error(`Ошибка ${err}`);
    //   }
    // } finally {
    // }
  }

  addCoimpt() {
    this.coimptForms.push(
      new FormGroup({
        imp_id: new FormControl(null, []),
        human_code: new FormControl(null, [Validators.required]),
        role: new FormControl(null, []),
        role_rmk: new FormControl(null, []),
        sign_mark: new FormControl(null, []),
      })
    );
    this.myControl_human_code.push(new FormControl(null));
  }

  delCoimpt(idx) {
    if (!isNullOrUndefined(this.coimptForms[idx].value.id)) {
      this.coimptidTodel.push(this.coimptForms[idx].value.id);
    }
    this.coimptForms.splice(idx, 1);
    this.myControl_human_code.splice(idx, 1);
  }

  async onDel() {
    let dialogRef = this.dialog.open(DialogYesNoComponent, {
      data: {
        title: 'Удалить заключение?',
        question: ``,
      },
    });
    dialogRef.afterClosed().subscribe(async (result) => {
      if (result === true) {
        this.spinnerService.show();
        try {
          let _coimpts = this.coimptService.getByImpid(this.id);
          let coimpts = isNullOrUndefined(await _coimpts) ? [] : await _coimpts;
          await Promise.all(
            coimpts.map(async (coimpt) => {
              return this.coimptService.deleteOneById(coimpt.id);
            })
          );
          await this.impristService.deleteOneById(this.id);
          this.router.navigate([
            `/sys/person/courttedit/edit_${this.selectedPersonalcode}_${this.selectedRepid}_${this.selectedCourttid}`,
          ]);
          this.openSnackBar(`Удалено.`, 'Ok');
        } catch (err) {
          console.error(`Ошибка при удалении ${err}`);
          this.openSnackBar(`Ошибка ${err.status}`, 'Ok');
        } finally {
          this.spinnerService.hide();
        }
      }
    });
  }

  /**
   * Функция возвращает true, если бли внесены изменения
   */
  hasChanges(): boolean {
    let res = false;
    if (this.impristForm.dirty || this.myControl_geoplace_code.dirty) {
      res = true;
    }
    this.coimptForms.map((form) => {
      if (form.dirty) {
        res = true;
      }
    });
    this.myControl_human_code.map((control) => {
      if (control.dirty) {
        res = true;
      }
    });
    return res;
  }

  async changePubStatus() {
    const isPublished = this.selectedPersonal.published === '*';
    if (isPublished) {
      const curDate = new Date().toISOString().split('T')[0];

      this.selectedPersonal.mustPublish = '*';
      this.selectedPersonal.mustUnpublish = null;
      this.selectedPersonal.pub_change_date = curDate;

      await this.personaltService.putOneById(
        this.selectedPersonalcode,
        this.selectedPersonal
      );
    }
  }

  /**
   * Функция сохраняет форму. Silent для показа сообщения. Возвращает true в случаеуспешного сохранения
   */
  async onSaveForm(silent = false, addnew = false) {
    let res = false;
    try {
      // if (
      //   !isNullOrUndefined(this.impristForm.getRawValue()['arrival_dat']) &&
      //   this.impristForm.getRawValue()['arrival_dat'] !== ''
      // ) {
      //   if (
      //     !(
      //       new Date(this.impristForm.getRawValue()['arrival_dat']) instanceof
      //         Date &&
      //       !isNaN(
      //         new Date(this.impristForm.getRawValue()['arrival_dat']).valueOf()
      //       )
      //     )
      //   ) {
      //     this.openSnackBar(`Введена не существующая дата прибытия`, 'Ok');
      //     return false;
      //   }
      // } else {
      //   this.impristForm.patchValue({
      //     arrival_dat: null,
      //   });
      // }
      // if (
      //   !isNullOrUndefined(this.impristForm.getRawValue()['leave_dat']) &&
      //   this.impristForm.getRawValue()['leave_dat'] !== ''
      // ) {
      //   if (
      //     !(
      //       new Date(this.impristForm.getRawValue()['leave_dat']) instanceof
      //         Date &&
      //       !isNaN(
      //         new Date(this.impristForm.getRawValue()['leave_dat']).valueOf()
      //       )
      //     )
      //   ) {
      //     this.openSnackBar(`Введена не существующая дата убытия`, 'Ok');
      //     return false;
      //   }
      // } else {
      //   this.impristForm.patchValue({
      //     leave_dat: null,
      //   });
      // }
      if (
        !isNullOrUndefined(this.myControl_geoplace_code.value) &&
        this.myControl_geoplace_code.value !== ''
      ) {
        if (!isNullOrUndefined(this.myControl_geoplace_code.value.place_num)) {
          this.impristForm.patchValue({
            place_num: this.myControl_geoplace_code.value.place_num,
          });
        } else {
          let dialogRef = this.dialog.open(DialogYesNoComponent, {
            data: {
              title: 'Введено не корректное место жительства.',
              question: `Продолжить?`,
            },
          });
          await new Promise((resolve, reject) => {
            dialogRef.afterClosed().subscribe(async (result) => {
              if (result === true) {
                this.myControl_geoplace_code.reset();
                this.impristForm.patchValue({
                  place_num: null,
                });
                resolve(null);
              } else {
                return false;
              }
            });
          });
          this.myControl_geoplace_code.reset();
          this.impristForm.patchValue({
            place_num: null,
          });
        }
      } else {
        this.myControl_geoplace_code.reset();
        this.impristForm.patchValue({
          place_num: null,
        });
      }
      this.spinnerService.show();
      switch (this.intType) {
        case 'editdoc': // редактирование документа
          /**Удаляем дочерние записи, удаленные из формы*/
          await Promise.all(
            this.coimptidTodel.map((id) => {
              return this.coimptService.deleteOneById(id);
            })
          );
          /**Сохранение дочерних сущностей */
          for (let j = 0; j < this.coimptForms.length; j++) {
            if (!isNullOrUndefined(this.myControl_human_code[j].value)) {
              this.coimptForms[j].patchValue({
                human_code: this.myControl_human_code[j].value.code,
              });
            }
            this.coimptForms[j].patchValue({
              imp_id: this.id,
            });
            if (
              this.coimptForms[j].value['sign_mark'] === true ||
              this.coimptForms[j].value['sign_mark'] === '*'
            ) {
              this.coimptForms[j].patchValue({
                sign_mark: '*',
              });
            } else {
              this.coimptForms[j].patchValue({
                sign_mark: null,
              });
            }
            if (isNullOrUndefined(this.coimptForms[j].value.id)) {
              this.coimptForms[j].removeControl('id');
              await this.coimptService.postOne(this.coimptForms[j].value);
            } else {
              if (
                this.coimptForms[j].touched ||
                this.myControl_human_code[j].touched
              ) {
                await this.coimptService.putOneById(
                  this.coimptForms[j].value.id,
                  this.coimptForms[j].value
                );
              }
            }
          }
          // if (!isNullOrUndefined(this.myControl_geoplace_code.value)) {
          // 	this.impristForm.patchValue({
          // 		'place_num': this.myControl_geoplace_code.value.place_num
          // 	});
          // }
          await this.impristService.putOneById(
            this.id,
            this.impristForm.getRawValue()
          );
          await this.changePubStatus();
          break;
        case 'newdoc': // создание документа
          this.impristForm.patchValue({
            personal_code: this.selectedPersonalcode,
          });
          this.impristForm.patchValue({
            crt_id: this.selectedCourttid,
          });
          this.impristForm.removeControl('id');
          // if (!isNullOrUndefined(this.myControl_geoplace_code.value)) {
          // 	this.impristForm.patchValue({
          // 		'place_num': this.myControl_geoplace_code.value.place_num
          // 	});
          // }
          let dataPost = this.impristForm.getRawValue();
          if (this.newnom) {
            dataPost['newnom'] = this.newnom;
            delete dataPost.impris_no;
          }
          let postForm = await this.impristService.postOne(dataPost);
          this.id = +postForm.id;
          this.intType = 'editdoc';
          /**Сохранение дочерних сущностей */
          for (let j = 0; j < this.coimptForms.length; j++) {
            if (!isNullOrUndefined(this.myControl_human_code[j].value)) {
              this.coimptForms[j].patchValue({
                human_code: this.myControl_human_code[j].value.code,
              });
            }
            this.coimptForms[j].patchValue({
              imp_id: this.id,
            });
            if (
              this.coimptForms[j].value['sign_mark'] === true ||
              this.coimptForms[j].value['sign_mark'] === '*'
            ) {
              this.coimptForms[j].patchValue({
                sign_mark: '*',
              });
            } else {
              this.coimptForms[j].patchValue({
                sign_mark: null,
              });
            }
            this.coimptForms[j].removeControl('id');
            await this.coimptService.postOne(this.coimptForms[j].value);
          }
          break;
        default:
      }
      if (addnew) {
        this.newnom = this.impristForm.getRawValue().impris_no + 1;
      } else {
        this.newnom = null;
      }

      if (addnew === true) {
        // this.router.navigate([
        //   `sys/person/person/impristedit/new_${this.selectedPersonalcode}_${this.selectedRepid}_${this.selectedCourttid}_0`,
        // ]);
        this.intType = 'newdoc';
      }
      if (!silent) {
        this.openSnackBar(`Сохранено.`, 'Ok');
      }
      res = true;
      // if (!addnew) {
      await this.getData();
      // }
    } catch (err) {
      if (err.status === 401) {
        console.error(`Ошибка авторизации`);
        this.authService.logout();
      } else if (err.status === 409) {
        this.impristForm.controls['impris_no'].setErrors({ incorrect: true });
        this.openSnackBar(`${err.error['error']}`, 'Ok');
      } else {
        console.error(`Ошибка ${err}`);
        this.openSnackBar(`Ошибка ${err.status}`, 'Ok');
      }
      throw false;
    } finally {
      this.spinnerService.hide();
      return res;
    }
  }

  async goBack(url = '') {
    let str = '/sys/person/courttedit/edit_';
    str += this.viewOnly ? 'v_' : '';
    str += `${this.selectedPersonalcode}_${this.selectedRepid}_${this.selectedCourttid}`;
    let saved = true;
    if (this.hasChanges() && this.valid) {
      let dialogRef = this.dialog.open(DialogSaveNoComponent, {
        data: {
          title: 'Сохранить изменения?',
          question: ``,
        },
      });
      dialogRef.afterClosed().subscribe(async (result) => {
        if (result === true) {
          saved = await this.onSaveForm(true);
          if (saved === true) {
            if (url === '') {
              this.router.navigate([str]);
            } else {
              this.router.navigate([url]);
            }
          }
        } else if (result === false) {
          if (url === '') {
            this.router.navigate([str]);
          } else {
            this.router.navigate([url]);
          }
        } else {
          return;
        }
      });
    } else if (this.hasChanges() && !this.valid) {
      let dialogRef = this.dialog.open(DialogYesNoComponent, {
        data: {
          title: 'Изменения не будут сохранены!',
          question: `Не заполнены обязательные поля. Уйти со страницы?`,
        },
      });
      dialogRef.afterClosed().subscribe(async (result) => {
        if (result === true) {
          if (url === '') {
            this.router.navigate([str]);
          } else {
            this.router.navigate([url]);
          }
        } else {
          return;
        }
      });
    } else {
      if (url === '') {
        this.router.navigate([str]);
      } else {
        this.router.navigate([url]);
      }
    }
  }

  isValid() {
    let res = this.impristForm.valid;
    this.myControl_human_code.map((control) => {
      if (
        !(
          typeof control.value === 'object' && !isNullOrUndefined(control.value)
        )
      ) {
        res = false;
      }
    });
    this.valid = res;
  }

  inputLink_geoplace_code() {
    clearTimeout(this.timerId);
    this.timerId = setTimeout(async () => {
      this.placets = await this.placetService.getWithFilterLimit(
        100,
        this.myControl_geoplace_code.value
      );
      if (!isNullOrUndefined(this.placets)) {
        try {
          this.filteredPlacets = this.myControl_geoplace_code.valueChanges.pipe(
            startWith<string | PlaceT>(''),
            map((value) => {
              if (!isNullOrUndefined(value)) {
                return typeof value === 'string' ? value : value.place_name;
              }
            }),
            map((name) => (name ? this._filter(name) : [...this.placets]))
          );
        } catch {}
      }
    }, 500);
  }

  private _filter(name: string): PlaceT[] {
    const filterValue = name.toLowerCase();
    return isNullOrUndefined(this.placets)
      ? null
      : this.placets.filter(
          (placet) =>
            placet.place_name.toLowerCase().indexOf(filterValue) !== -1
        );
  }

  displayFn_residence_code(place?: PlaceT): string | undefined {
    return place
      ? place.place_num + '. ' + place.place_code + '. ' + place.place_name
      : undefined;
  }

  inputLink_human_code(j) {
    clearTimeout(this.timerId);
    this.timerId = setTimeout(async () => {
      this.personalts = await this.personaltService.getWithShortfilterLimit(
        100,
        this.myControl_human_code[j].value
      );
      if (!isNullOrUndefined(this.personalts)) {
        try {
          this.filteredPersonalts = (<FormControl>(
            this.myControl_human_code[j]
          )).valueChanges.pipe(
            startWith<string | PersonalT>(''),
            map((value) => (typeof value === 'string' ? value : value.surname)),
            map((name) =>
              name ? this._filterPersonalt(name) : [...this.personalts]
            )
          );
        } catch {}
      }
    }, 500);
  }

  private _filterPersonalt(name: string): PersonalT[] {
    const filterValue = name.toLowerCase();
    return isNullOrUndefined(this.personalts)
      ? null
      : this.personalts
          .filter(
            (personalt) =>
              !isNullOrUndefined(personalt.surname) && personalt.surname !== ''
          )
          .filter(
            (personalt) =>
              personalt.surname.toLowerCase().indexOf(filterValue) !== -1
          );
  }

  displayFn_human_code(personal?: PersonalT): string | undefined {
    if (!personal) {
      return undefined;
    }
    let str = '';
    str += personal.code + '. ';
    str += !isNullOrUndefined(personal.surname) ? personal.surname + ' ' : '';
    str += !isNullOrUndefined(personal.fname) ? personal.fname + ' ' : '';
    str += !isNullOrUndefined(personal.lname) ? personal.lname : '';
    return personal ? str : undefined;
  }

  // resetArrival_dat() {
  //   this.impristForm.patchValue({
  //     arrival_dat: null,
  //   });
  // }

  // resetLeave_dat() {
  //   this.impristForm.patchValue({
  //     leave_dat: null,
  //   });
  // }

  onAddPersonalT(idx) {
    this.filteredPersonalts = null;
    let dialogRef = this.dialog.open(DialogPersonalEditComponent, {
      panelClass: 'app-dialog-extend',
    });
    dialogRef.afterClosed().subscribe(async (result) => {
      if (result !== false) {
        let res = await this.personaltService.postOne(result);
        this.myControl_human_code[idx].setValue(res);
      }
    });
  }

  onLinkPersonalT(id) {
    let str = this.viewOnly
      ? `/sys/person/personaltedit/v_${id}`
      : `/sys/person/personaltedit/${id}`;
    this.router.navigate([str]);
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
    });
  }
}
