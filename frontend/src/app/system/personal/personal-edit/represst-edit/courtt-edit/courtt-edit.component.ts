import {
  Component,
  OnInit,
  AfterContentChecked,
  HostListener,
  OnDestroy,
  ViewEncapsulation,
} from '@angular/core';
import { AuthService } from 'src/app/shared/services/auth.service';
import { PersonaltService } from 'src/app/system/shared/services/personalt.service';
import { ListoftService } from 'src/app/system/shared/services/listoft.service';
import { PlacetService } from 'src/app/system/shared/services/placet.service';
import { CourttService } from 'src/app/system/shared/services/courtt.service';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CocrttService } from 'src/app/system/shared/services/cocrtt.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PlaceT } from 'src/app/shared/models/placet.model';
import { Observable, Subscription } from 'rxjs';
import { PersonalT } from 'src/app/shared/models/personalt.model';
import { ListOfT } from 'src/app/shared/models/listoft.model';
import { isNullOrUndefined } from 'util';
import { ImprisT } from 'src/app/shared/models/imprist.model';
import { ImpristService } from 'src/app/system/shared/services/imprist.service';
import { DialogYesNoComponent } from 'src/app/system/shared/components/dialog-yes-no/dialog-yes-no.component';
import { DialogSaveNoComponent } from 'src/app/system/shared/components/dialog-save-no/dialog-save-no.component';
import { startWith, map } from 'rxjs/operators';
import { GlobalService } from 'src/app/shared/services/global.service';
import { DialogPersonalEditComponent } from '../../dialog-personal-edit/dialog-personal-edit.component';
import { cocrT_width } from 'src/app/shared/models/cocrt.model';
import { courtT_width } from 'src/app/shared/models/courtt.model';
import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem,
} from '@angular/cdk/drag-drop';
import { MoveordernomService } from 'src/app/system/shared/services/moveordernom.service';
import { DateparseService } from 'src/app/shared/services/dateparse.service';
import { RepresstService } from 'src/app/system/shared/services/represst.service';

@Component({
  selector: 'app-courtt-edit',
  templateUrl: './courtt-edit.component.html',
  styleUrls: ['./courtt-edit.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class CourttEditComponent
  implements OnInit, AfterContentChecked, OnDestroy {
  constructor(
    public dateparseService: DateparseService,
    private authService: AuthService,
    public globalService: GlobalService,
    private personaltService: PersonaltService,
    private listoftService: ListoftService,
    private placetService: PlacetService,
    private represstService: RepresstService,
    private courttService: CourttService,
    private cocrttService: CocrttService,
    private impristService: ImpristService,
    private moveordernomService: MoveordernomService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private spinnerService: NgxSpinnerService,
    public dialog: MatDialog,
    public snackBar: MatSnackBar
  ) {
    this.activeRouterObserver = this.activatedRoute.params.subscribe(
      (param) => {
        let str = param.id.split('_');
        if (str[0] === 'new') {
          this.intType = 'newdoc';
          this.selectedRepid = +str[2];
          this.selectedPersonalcode = +str[1];
          this.viewOnly = false;
        } else {
          if (str[1] === 'v') {
            this.id = +str[4];
            this.selectedRepid = +str[3];
            this.selectedPersonalcode = +str[2];
            this.intType = 'editdoc';
            this.viewOnly = true;
          } else {
            this.id = +str[3];
            this.selectedRepid = +str[2];
            this.selectedPersonalcode = +str[1];
            this.intType = 'editdoc';
            this.viewOnly = false;
          }
        }
        this.viewOnly =
          this.viewOnly === false
            ? this.authService.getCurUser().usertype === 4
            : true;
      }
    );
    this.globalService.routerObserver = router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        if (val.url.indexOf('courttedit') !== -1) {
          this.globalService.breadcrumbs.length = 0;
          this.globalService.breadcrumbs.push(
            {
              label: 'персоналии',
              url:
                this.viewOnly === true
                  ? `/sys/person/personaltedit/v_${this.selectedPersonalcode}`
                  : `/sys/person/personaltedit/${this.selectedPersonalcode}`,
            },
            {
              label: 'репрессия',
              url:
                this.viewOnly === true
                  ? `/sys/person/represstedit/edit_v_${this.selectedPersonalcode}_${this.selectedRepid}`
                  : `/sys/person/represstedit/edit_${this.selectedPersonalcode}_${this.selectedRepid}`,
            },
            { label: 'следствие и судебный процесс' }
          );
          this.breadcrumbs = this.globalService.breadcrumbs;
        }
      }
    });
  }
  cocrT_width = cocrT_width;
  courtT_width = courtT_width;

  timerId;
  id: number;
  intType: string | any = ''; // тип интерфейса
  viewOnly: boolean;
  selectedPersonalcode: number;
  selectedPersonal: PersonalT;
  selectedRepid: number;

  courttForm: FormGroup;
  cocrttForms: FormGroup[] = [];

  cocrttidTodel: number[] = [];

  placets: PlaceT[];
  filteredPlacets: Observable<PlaceT[]>;

  personalts: PersonalT[];
  filteredPersonalts: Observable<PersonalT[]>;
  filteredSentenses: Observable<ListOfT[]>;

  myControl_human_code: FormControl[] = [];
  myControl_code_place_num_i: FormControl;
  myControl_code_place_num_j: FormControl;
  selectedImprists: ImprisT[] = [];

  sentenses: ListOfT[];
  impris_types: ListOfT[];
  myControl_sentenses: FormControl;

  valid: boolean;

  breadcrumbs = [];

  scroll = 0;
  activeRouterObserver: Subscription;

  sentensPos = 0;
  sentensPatternsOpen = false;
  sentensPatterns = [
    { value: 'БПП', title: 'Без права переписки' },
    { value: 'ИТЛ', title: 'Исправительно-трудовой лагерь' },
    { value: 'КИ', title: 'Конфискация имущества' },
    { value: 'ПП', title: 'Поражение в правах' },
    { value: 'ВМН', title: 'Высшая мера наказания (расстрел)' },
    { value: 'ССЫЛКА', title: '' },
    { value: 'ТЮРЬМА', title: '' },
    { value: 'ПОЛИТИЗОЛЯТОР', title: '' },
    { value: 'ПРИНУДИТЕЛЬНОЕ ЛЕЧЕНИЕ', title: '' },
    { value: 'АДМИНИСТРАТИВНАЯ ССЫЛКА', title: '' },
    { value: 'ВЫСЫЛКА', title: '' },
    { value: 'РАСКУЛАЧИВАНИЕ', title: '' },
    { value: 'ВЫСЫЛКА С СЕМЬЁЙ', title: '' },
    { value: 'ВЫСЫЛКА СЕМЬИ', title: '' },
    { value: 'БЕССРОЧНАЯ ССЫЛКА', title: '' },
    { value: 'СПЕЦПОСЕЛЕНИЕ', title: '' },
    { value: 'ВЫСЕЛЕНИЕ', title: '' },
    { value: 'ЛИШЕНИЕ ВОИНСКОГО ЗВАНИЯ', title: '' },
    { value: 'ОТПРАВКА НА ФРОНТ', title: '' },
    { value: 'ШТРАФБАТ', title: '' },
    { value: 'ТРУДАРМИЯ', title: '' },
    { value: 'ОСВОБОЖДЕНИЕ', title: '' },
    { value: 'ОСВОБОЖДЕНИЕ ЗА ОТСУТСТВИЕМ СОСТАВА ПРЕСТУПЛЕНИЯ', title: '' },
    { value: 'ДЕЛО ПРЕКРАЩЕНО', title: '' },
    { value: 'АМНИСТИЯ', title: '' },
    { value: 'ОТМЕНА ПРИГОВОРА', title: '' },
    { value: 'ОТМЕНА ПРИНУДИТЕЛЬНОГО ЛЕЧЕНИЯ', title: '' },
  ];
  articlePos = 0;
  articlePatternsOpen = false;
  articlePatterns = [
    { value: '58-1', title: '' },
    { value: '58-2', title: '' },
    { value: '58-8.4', title: '' },
    { value: '58-1.А', title: '' },
    { value: '17-58-1', title: '' },
    { value: 'КРД', title: 'контрреволюционная деятельность' },
    { value: 'КРТД', title: 'контрреволюционная троцкистская деятельность' },
    { value: 'СОЭ', title: 'социально опасный элемент' },
    { value: 'СВЭ', title: 'социально вредный элемент' },
    { value: 'АСА', title: 'антисоветская агитация' },
    { value: 'ЧСИР', title: 'член семьи изменника родины' },
    { value: 'КРА', title: 'контрреволюционная агитация' },
    { value: 'КРП', title: 'контрреволюционная пропаганда' },
    { value: 'КРТА', title: 'контрреволюционная троцкистская агитация' },
    { value: 'АСД', title: 'антисоветская деятельность' },
    { value: 'КРГ', title: 'контрреволюционная группа' },
    { value: 'Нац. принадлежность', title: '' },
  ];

  @HostListener('window:scroll', ['$event'])
  getScroll(event) {
    this.scroll = window.pageYOffset;
  }

  @HostListener('window:keydown', ['$event'])
  onKeyPress(event: KeyboardEvent) {
    if ((event.ctrlKey || event.metaKey) && event.keyCode == 83) {
      event.preventDefault();
      if (!this.viewOnly && this.valid && this.intType !== 'myprofile') {
        this.onSaveForm();
      }
    }
  }

  ngOnDestroy(): void {
    this.globalService.routerObserver.unsubscribe();
    // this.activeRouterObserver.unsubscribe();
  }

  async ngOnInit() {
    this.courttForm = new FormGroup({
      id: new FormControl({ value: null, disabled: this.viewOnly }, []),
      personal_code: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      rep_id: new FormControl({ value: null, disabled: this.viewOnly }, []),
      trial_no: new FormControl({ value: null, disabled: true }, [
        Validators.required,
      ]),
      inq_name: new FormControl({ value: null, disabled: this.viewOnly }, []),
      // inq_date: new FormControl({ value: null, disabled: true }, []),
      inq_date_begin: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      inq_date_end: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      inq_date_begin_end: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      court_name: new FormControl({ value: null, disabled: this.viewOnly }, []),
      // sentense_date: new FormControl({ value: null, disabled: true }, []),
      sentense_date_begin: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      sentense_date_end: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      sentense_date_begin_end: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      article: new FormControl({ value: null, disabled: this.viewOnly }, []),
      sentense: new FormControl({ value: null, disabled: this.viewOnly }, []),
      inq_name_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      inq_date_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      court_name_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      trial_rmk: new FormControl({ value: null, disabled: this.viewOnly }, []),
      sentense_date_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      article_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      sentense_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      inq_rmk: new FormControl({ value: null, disabled: this.viewOnly }, []),
      place_num_i: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      place_num_j: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      place_i_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      place_j_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
    });
    this.myControl_sentenses = new FormControl({
      value: null,
      disabled: this.viewOnly,
    });
    this.myControl_code_place_num_i = new FormControl({
      value: null,
      disabled: this.viewOnly,
    });
    this.myControl_code_place_num_j = new FormControl({
      value: null,
      disabled: this.viewOnly,
    });
    try {
      let impris_types = this.listoftService.getWithFilter('IMPRIS_TYPE');
      let sentenses = this.listoftService.getWithFilter('SENTENSE');
      this.sentenses = isNullOrUndefined(await sentenses)
        ? []
        : await sentenses;
      this.impris_types = isNullOrUndefined(await impris_types)
        ? []
        : await impris_types;
      this.filteredSentenses = this.myControl_sentenses.valueChanges.pipe(
        startWith(''),
        map((sentense) =>
          sentense ? this._filter(sentense) : [...this.sentenses]
        )
      );
      await this.getData();
    } catch (err) {
      this.globalService.exceptionHandling(err);
    }
  }

  ngAfterContentChecked(): void {
    this.isValid();
  }

  async getData() {
    this.spinnerService.show();
    let selectedPersonal;
    try {
      selectedPersonal = this.personaltService.getBycode(
        this.selectedPersonalcode
      );
    } catch (err) {
      this.globalService.exceptionHandling(err);
    }
    switch (this.intType) {
      case 'editdoc': // редактирование
        try {
          this.myControl_human_code.length = 0;
          this.cocrttForms.length = 0;
          let selectedCourtt = await this.courttService.getOneById(this.id);
          this.courttForm.reset();
          this.courttForm.patchValue({
            personal_code: selectedCourtt.personal_code,
            rep_id: selectedCourtt.rep_id,
            repress_no: selectedCourtt.repress_no,
            trial_no: selectedCourtt.trial_no,
            inq_name: selectedCourtt.inq_name,
            // inq_date: selectedCourtt.inq_date,
            inq_date_begin: selectedCourtt.inq_date_begin,
            inq_date_end: selectedCourtt.inq_date_begin_end,
            court_name: selectedCourtt.court_name,
            // sentense_date: selectedCourtt.sentense_date,
            sentense_date_begin: selectedCourtt.sentense_date_begin,
            sentense_date_end: selectedCourtt.sentense_date_end,
            article: selectedCourtt.article,
            sentense: selectedCourtt.sentense,
            inq_name_rmk: selectedCourtt.inq_name_rmk,
            inq_date_rmk: selectedCourtt.inq_date_rmk,
            court_name_rmk: selectedCourtt.court_name_rmk,
            trial_rmk: selectedCourtt.trial_rmk,
            sentense_date_rmk: selectedCourtt.sentense_date_rmk,
            article_rmk: selectedCourtt.article_rmk,
            sentense_rmk: selectedCourtt.sentense_rmk,
            inq_rmk: selectedCourtt.inq_rmk,
            place_num_i: selectedCourtt.place_num_i,
            place_num_j: selectedCourtt.place_num_j,
            place_i_rmk: selectedCourtt.place_i_rmk,
            place_j_rmk: selectedCourtt.place_j_rmk,
          });
          if (selectedCourtt.sentense) {
            this.sentensPos = selectedCourtt.sentense.length;
          }
          if (selectedCourtt.article) {
            this.articlePos = selectedCourtt.article.length;
          }
          this.dateparseService.setSourDate(
            this.courttForm,
            'sentense_date_begin_end',
            selectedCourtt.sentense_date_begin,
            selectedCourtt.sentense_date_end
          );
          this.dateparseService.setSourDate(
            this.courttForm,
            'inq_date_begin_end',
            selectedCourtt.inq_date_begin,
            selectedCourtt.inq_date_end
          );
          this.myControl_sentenses.reset();
          this.myControl_sentenses.setValue(selectedCourtt.sentense);
          let selectedImprists = this.impristService.getByCrtid(
            selectedCourtt.id
          );
          if (!isNullOrUndefined(selectedCourtt.place_num_i)) {
            let _geoplace_code = await this.placetService.getByPlace_num(
              selectedCourtt.place_num_i
            );
            this.myControl_code_place_num_i.reset();
            if (
              !isNullOrUndefined(_geoplace_code) &&
              _geoplace_code.length > 0
            ) {
              this.myControl_code_place_num_i.setValue(_geoplace_code[0]);
            }
          }
          if (!isNullOrUndefined(selectedCourtt.place_num_j)) {
            let _geoplace_code = await this.placetService.getByPlace_num(
              selectedCourtt.place_num_j
            );
            this.myControl_code_place_num_j.reset();
            if (
              !isNullOrUndefined(_geoplace_code) &&
              _geoplace_code.length > 0
            ) {
              this.myControl_code_place_num_j.setValue(_geoplace_code[0]);
            }
          }

          let _selectedCocrtts = this.cocrttService.getByCrtid(
            selectedCourtt.id
          );
          let selectedCocrtts = isNullOrUndefined(await _selectedCocrtts)
            ? []
            : await _selectedCocrtts;
          for (let j = 0; j < selectedCocrtts.length; j++) {
            this.cocrttForms.push(
              new FormGroup({
                id: new FormControl(
                  { value: null, disabled: this.viewOnly },
                  []
                ),
                crt_id: new FormControl(
                  { value: null, disabled: this.viewOnly },
                  []
                ),
                human_code: new FormControl(
                  { value: null, disabled: this.viewOnly },
                  [Validators.required]
                ),
                role: new FormControl(
                  { value: null, disabled: this.viewOnly },
                  []
                ),
                role_rmk: new FormControl(
                  { value: null, disabled: this.viewOnly },
                  []
                ),
                sign_mark: new FormControl(
                  { value: null, disabled: this.viewOnly },
                  []
                ),
              })
            );
            this.myControl_human_code.push(
              new FormControl({
                value: null,
                disabled: this.viewOnly,
              })
            );
            this.cocrttForms[j].patchValue({
              id: selectedCocrtts[j].id,
              crt_id: selectedCocrtts[j].crt_id,
              human_code: selectedCocrtts[j].human_code,
              role: selectedCocrtts[j].role,
              role_rmk: selectedCocrtts[j].role_rmk,
              sign_mark: selectedCocrtts[j].sign_mark,
            });
            if (!isNullOrUndefined(selectedCocrtts[j].human_code)) {
              let person = await this.personaltService.getBycode(
                selectedCocrtts[j].human_code
              );
              if (!isNullOrUndefined(person) && (await person.length) > 0) {
                this.myControl_human_code[j].setValue(person[0]);
              }
            }
          }
          this.selectedImprists = isNullOrUndefined(await selectedImprists)
            ? []
            : await selectedImprists;
          this.selectedImprists.sort((a, b) =>
            a.impris_no > b.impris_no ? 1 : -1
          );
          let obj = {
            scroll: this.globalService.courtt_edit.scroll,
            selectedImprist: this.selectedImprists.length > 0 ? true : false,
          };
          this.globalService.courtt_edit = obj;
        } catch (err) {
          this.globalService.exceptionHandling(err);
        } finally {
          this.spinnerService.hide();
        }
        break;
      case 'newdoc': // создание
        try {
          let _represst = this.represstService.getOneById(this.selectedRepid);
          let _courts = this.courttService.getByRepid(this.selectedRepid);
          let courts = isNullOrUndefined(await _courts) ? [] : await _courts;
          let represst = await _represst;
          let num: number;
          if (courts.length === 0) {
            num = 1;
          } else {
            courts.sort((a, b) => b.trial_no - a.trial_no);
            num = courts[0].trial_no + 1;
          }
          this.courttForm.reset();
          this.courttForm.patchValue({
            trial_no: num,
          });
          if (represst.repress_dat_begin) {
            this.courttForm.patchValue({
              inq_date_begin: represst.repress_dat_begin,
              inq_date_end: represst.repress_dat_end,
            });
            this.dateparseService.setSourDate(
              this.courttForm,
              'inq_date_begin_end',
              represst.repress_dat_begin,
              represst.repress_dat_end
            );
          }
        } catch (err) {
          this.globalService.exceptionHandling(err);
        } finally {
          this.spinnerService.hide();
        }
        break;
      default:
    }
    this.selectedPersonal = (await selectedPersonal)[0];
    this.spinnerService.hide();
    setTimeout(() => {
      window.scrollTo(0, this.globalService.courtt_edit.scroll);
    }, 200);
    // } catch (err) {
    //   if (err.status === 401) {
    //     console.error(`Ошибка авторизации`);
    //     this.authService.logout();
    //   } else {
    //     console.error(`Ошибка ${err}`);
    //   }
    // } finally {
    //   this.spinnerService.hide();
    //   setTimeout(() => {
    //     window.scrollTo(0, this.globalService.courtt_edit.scroll);
    //   }, 200);
    // }
  }

  addCocrtt() {
    this.cocrttForms.push(
      new FormGroup({
        crt_id: new FormControl(null, []),
        human_code: new FormControl(null, [Validators.required]),
        role: new FormControl(null, []),
        role_rmk: new FormControl(null, []),
        sign_mark: new FormControl(null, []),
      })
    );
    this.myControl_human_code.push(new FormControl(null));
  }

  delCocrtt(idx) {
    if (!isNullOrUndefined(this.cocrttForms[idx].value.id)) {
      this.cocrttidTodel.push(this.cocrttForms[idx].value.id);
    }
    this.cocrttForms.splice(idx, 1);
    this.myControl_human_code.splice(idx, 1);
  }

  async onDel() {
    let dialogRef = this.dialog.open(DialogYesNoComponent, {
      data: {
        title: 'Удалить судебный процесс?',
        question: ``,
      },
    });
    dialogRef.afterClosed().subscribe(async (result) => {
      if (result === true) {
        this.spinnerService.show();
        try {
          let _cocrtts = this.cocrttService.getByCrtid(this.id);
          let cocrtts = isNullOrUndefined(await _cocrtts) ? [] : await _cocrtts;
          await Promise.all(
            cocrtts.map(async (corept) => {
              return this.cocrttService.deleteOneById(corept.id);
            })
          );
          await this.courttService.deleteOneById(this.id);
          // this.router.navigate([`/sys/person/personaltedit/${this.selectedPersonalcode}`]);
          this.router.navigate([
            `/sys/person/represstedit/edit_${this.selectedPersonalcode}_${this.selectedRepid}`,
          ]);
          this.openSnackBar(`Удалено.`, 'Ok');
        } catch (err) {
          console.error(`Ошибка при удалении ${err}`);
          this.globalService.exceptionHandling(err);
        } finally {
          this.spinnerService.hide();
        }
      }
    });
  }

  /**
   * Функция возвращает true, если бли внесены изменения
   */
  hasChanges(): boolean {
    let res = false;
    if (
      this.courttForm.dirty ||
      this.myControl_code_place_num_i.dirty ||
      this.myControl_code_place_num_j.dirty ||
      this.myControl_sentenses.dirty
    ) {
      res = true;
    }
    this.cocrttForms.map((form) => {
      if (form.dirty) {
        res = true;
      }
    });
    this.myControl_human_code.map((control) => {
      if (control.dirty) {
        res = true;
      }
    });
    return res;
  }

  async changePubStatus() {
    const isPublished = this.selectedPersonal.published === '*';
    if (isPublished) {
      const curDate = new Date().toISOString().split('T')[0];
      this.selectedPersonal.mustPublish = '*';
      this.selectedPersonal.mustUnpublish = null;
      this.selectedPersonal.pub_change_date = curDate;

      await this.personaltService.putOneById(
        this.selectedPersonalcode,
        this.selectedPersonal
      );
    }
  }

  /**
   * Функция сохраняет форму. Silent для показа сообщения. Возвращает true в случаеуспешного сохранения
   */
  async onSaveForm(silent = false, update = true) {
    let res = false;
    try {
      // if (
      //   !isNullOrUndefined(this.courttForm.getRawValue()['inq_date']) &&
      //   this.courttForm.getRawValue()['inq_date'] !== ''
      // ) {
      //   if (
      //     !(
      //       new Date(this.courttForm.getRawValue()['inq_date']) instanceof
      //         Date &&
      //       !isNaN(
      //         new Date(this.courttForm.getRawValue()['inq_date']).valueOf()
      //       )
      //     )
      //   ) {
      //     this.openSnackBar(
      //       `Введена не существующая дата начала следствия`,
      //       'Ok'
      //     );
      //     return false;
      //   }

      // } else {
      //   this.courttForm.patchValue({
      //     inq_date: null,
      //   });
      // }
      // if (
      //   !isNullOrUndefined(this.courttForm.getRawValue()['sentense_date']) &&
      //   this.courttForm.getRawValue()['sentense_date'] !== ''
      // ) {
      //   if (
      //     !(
      //       new Date(this.courttForm.getRawValue()['sentense_date']) instanceof
      //         Date &&
      //       !isNaN(
      //         new Date(this.courttForm.getRawValue()['sentense_date']).valueOf()
      //       )
      //     )
      //   ) {
      //     this.openSnackBar(
      //       `Введена не существующая дата вынесения приговора`,
      //       'Ok'
      //     );
      //     return false;
      //   }

      // } else {
      //   this.courttForm.patchValue({
      //     sentense_date: null,
      //   });
      // }
      if (
        !isNullOrUndefined(this.myControl_code_place_num_i.value) &&
        this.myControl_code_place_num_i.value !== ''
      ) {
        if (
          !isNullOrUndefined(this.myControl_code_place_num_i.value.place_num)
        ) {
          this.courttForm.patchValue({
            place_num_i: this.myControl_code_place_num_i.value.place_num,
          });
        } else {
          let dialogRef = this.dialog.open(DialogYesNoComponent, {
            data: {
              title: 'Введено не корректное место следствия.',
              question: `Продолжить?`,
            },
          });
          await new Promise((resolve, reject) => {
            dialogRef.afterClosed().subscribe(async (result) => {
              if (result === true) {
                this.myControl_code_place_num_i.reset();
                this.courttForm.patchValue({
                  place_num_i: null,
                });
                resolve(null);
              } else {
                return false;
              }
            });
          });
          this.myControl_code_place_num_i.reset();
          this.courttForm.patchValue({
            place_num_i: null,
          });
        }
      } else {
        this.myControl_code_place_num_i.reset();
        this.courttForm.patchValue({
          place_num_i: null,
        });
      }
      if (
        !isNullOrUndefined(this.myControl_code_place_num_j.value) &&
        this.myControl_code_place_num_j.value !== ''
      ) {
        if (
          !isNullOrUndefined(this.myControl_code_place_num_j.value.place_num)
        ) {
          this.courttForm.patchValue({
            place_num_j: this.myControl_code_place_num_j.value.place_num,
          });
        } else {
          let dialogRef = this.dialog.open(DialogYesNoComponent, {
            data: {
              title: 'Введено не корректное место судебного процесса.',
              question: `Продолжить?`,
            },
          });
          await new Promise((resolve, reject) => {
            dialogRef.afterClosed().subscribe(async (result) => {
              if (result === true) {
                this.myControl_code_place_num_j.reset();
                this.courttForm.patchValue({
                  place_num_j: null,
                });
                resolve(null);
              } else {
                return false;
              }
            });
          });
          this.myControl_code_place_num_j.reset();
          this.courttForm.patchValue({
            place_num_j: null,
          });
        }
      } else {
        this.myControl_code_place_num_j.reset();
        this.courttForm.patchValue({
          place_num_j: null,
        });
      }
      if (!isNullOrUndefined(this.myControl_sentenses.value)) {
        // фикс null при создании формы
        this.courttForm.patchValue({
          sentense: this.myControl_sentenses.value,
        });
      }
      this.spinnerService.show();
      switch (this.intType) {
        case 'editdoc': // редактирование документа
          /**Удаляем дочерние записи, удаленные из формы*/
          await Promise.all(
            this.cocrttidTodel.map((id) => {
              return this.cocrttService.deleteOneById(id);
            })
          );
          /**Сохранение дочерних сущностей */
          for (let j = 0; j < this.cocrttForms.length; j++) {
            if (
              !isNullOrUndefined(this.myControl_human_code[j].value) &&
              !isNullOrUndefined(this.myControl_human_code[j].value.code)
            ) {
              if (!isNullOrUndefined(this.myControl_human_code[j].value)) {
                this.cocrttForms[j].patchValue({
                  human_code: this.myControl_human_code[j].value.code,
                });
              }
              this.cocrttForms[j].patchValue({
                crt_id: this.id,
              });
              if (
                this.cocrttForms[j].value['sign_mark'] === true ||
                this.cocrttForms[j].value['sign_mark'] === '*'
              ) {
                this.cocrttForms[j].patchValue({
                  sign_mark: '*',
                });
              } else {
                this.cocrttForms[j].patchValue({
                  sign_mark: null,
                });
              }
              if (isNullOrUndefined(this.cocrttForms[j].value.id)) {
                this.cocrttForms[j].removeControl('id');
                await this.cocrttService.postOne(this.cocrttForms[j].value);
              } else {
                if (
                  this.cocrttForms[j].touched ||
                  this.myControl_human_code[j].touched
                ) {
                  await this.cocrttService.putOneById(
                    this.cocrttForms[j].value.id,
                    this.cocrttForms[j].value
                  );
                }
              }
            } else {
              this.openSnackBar(
                `Не верно указано лицо, упоминаемое в связи с судебным процессом #${
                  j + 1
                }`,
                'Ok'
              );
              return false;
            }
          }
          // if (!isNullOrUndefined(this.myControl_code_place_num_i.value)) {
          // 	this.courttForm.patchValue({
          // 		'place_num_i': this.myControl_code_place_num_i.value.place_num
          // 	});
          // }
          // if (!isNullOrUndefined(this.myControl_code_place_num_j.value)) {
          // 	this.courttForm.patchValue({
          // 		'place_num_j': this.myControl_code_place_num_j.value.place_num
          // 	});
          // }
          await this.courttService.putOneById(
            this.id,
            this.courttForm.getRawValue()
          );
          await this.changePubStatus();
          break;
        case 'newdoc': // создание документа
          this.courttForm.patchValue({
            personal_code: this.selectedPersonalcode,
          });
          this.courttForm.patchValue({
            rep_id: this.selectedRepid,
          });
          this.courttForm.removeControl('id');
          // if (!isNullOrUndefined(this.myControl_code_place_num_i.value)) {
          // 	this.courttForm.patchValue({
          // 		'place_num_i': this.myControl_code_place_num_i.value.place_num
          // 	});
          // }
          // if (!isNullOrUndefined(this.myControl_code_place_num_j.value)) {
          // 	this.courttForm.patchValue({
          // 		'place_num_j': this.myControl_code_place_num_j.value.place_num
          // 	});
          // }
          let postForm = await this.courttService.postOne(
            this.courttForm.getRawValue()
          );
          this.id = +postForm.id;
          this.intType = 'editdoc';
          /**Сохранение дочерних сущностей */
          for (let j = 0; j < this.cocrttForms.length; j++) {
            if (
              !isNullOrUndefined(this.myControl_human_code[j].value) &&
              !isNullOrUndefined(this.myControl_human_code[j].value.code)
            ) {
              if (!isNullOrUndefined(this.myControl_human_code[j].value)) {
                this.cocrttForms[j].patchValue({
                  human_code: this.myControl_human_code[j].value.code,
                });
              }
              this.cocrttForms[j].patchValue({
                crt_id: this.id,
              });
              if (
                this.cocrttForms[j].value['sign_mark'] === true ||
                this.cocrttForms[j].value['sign_mark'] === '*'
              ) {
                this.cocrttForms[j].patchValue({
                  sign_mark: '*',
                });
              } else {
                this.cocrttForms[j].patchValue({
                  sign_mark: null,
                });
              }
              this.cocrttForms[j].removeControl('id');
              await this.cocrttService.postOne(this.cocrttForms[j].value);
            } else {
              this.openSnackBar(
                `Не верно указано лицо, упоминаемое в связи с судебным процессом #${
                  j + 1
                }`,
                'Ok'
              );
              return false;
            }
          }
          break;
        default:
      }
      if (update === true) {
        await this.getData();
      }
      if (!silent) {
        this.openSnackBar(`Сохранено.`, 'Ok');
      }
      res = true;
    } catch (err) {
      if (err.status === 401) {
        console.error(`Ошибка авторизации`);
        this.authService.logout();
      } else if (err.status === 409) {
        this.courttForm.controls['trial_no'].setErrors({ incorrect: true });
        this.openSnackBar(`${err.error['error']}`, 'Ok');
      } else {
        console.error(`Ошибка ${err}`);
        this.openSnackBar(`Ошибка ${err.status}`, 'Ok');
      }
      throw false;
    } finally {
      this.spinnerService.hide();
      return res;
    }
  }

  async goBack(url = '') {
    let str = '/sys/person/represstedit/edit_';
    str += this.viewOnly ? 'v_' : '';
    str += `${this.selectedPersonalcode}_${this.selectedRepid}`;
    let saved = true;
    if (this.hasChanges() && this.valid) {
      let dialogRef = this.dialog.open(DialogSaveNoComponent, {
        data: {
          title: 'Сохранить изменения?',
          question: ``,
        },
      });
      dialogRef.afterClosed().subscribe(async (result) => {
        if (result === true) {
          saved = await this.onSaveForm(true);
          if (saved === true) {
            // this.router.navigate([`/sys/person/personaltedit/${this.selectedPersonalcode}`]);
            if (url === '') {
              this.router.navigate([str]);
              // this.router.navigate([`/sys/person/represstedit/edit_${this.selectedPersonalcode}_${this.selectedRepid}`]);
            } else {
              this.router.navigate([url]);
            }
          }
        } else if (result === false) {
          // this.router.navigate([`/sys/person/personaltedit/${this.selectedPersonalcode}`]);
          if (url === '') {
            this.router.navigate([str]);
            // this.router.navigate([`/sys/person/represstedit/edit_${this.selectedPersonalcode}_${this.selectedRepid}`]);
          } else {
            this.router.navigate([url]);
          }
        } else {
          return;
        }
      });
    } else if (this.hasChanges() && !this.valid) {
      let dialogRef = this.dialog.open(DialogYesNoComponent, {
        data: {
          title: 'Изменения не будут сохранены!',
          question: `Не заполнены обязательные поля. Уйти со страницы?`,
        },
      });
      dialogRef.afterClosed().subscribe(async (result) => {
        if (result === true) {
          // this.router.navigate([`/sys/person/personaltedit/${this.selectedPersonalcode}`]);
          if (url === '') {
            this.router.navigate([str]);
            // this.router.navigate([`/sys/person/represstedit/edit_${this.selectedPersonalcode}_${this.selectedRepid}`]);
          } else {
            this.router.navigate([url]);
          }
        } else {
          return;
        }
      });
    } else {
      // this.router.navigate([`/sys/person/personaltedit/${this.selectedPersonalcode}`]);
      if (url === '') {
        this.router.navigate([str]);
        // this.router.navigate([`/sys/person/represstedit/edit_${this.selectedPersonalcode}_${this.selectedRepid}`]);
      } else {
        this.router.navigate([url]);
      }
    }
  }

  isValid() {
    let res = this.courttForm.valid;
    this.myControl_human_code.map((control) => {
      if (
        !(
          typeof control.value === 'object' && !isNullOrUndefined(control.value)
        )
      ) {
        res = false;
      }
    });
    this.valid = res;
  }

  inputLink_geoplace_code_i() {
    clearTimeout(this.timerId);
    this.timerId = setTimeout(async () => {
      this.placets = await this.placetService.getWithFilterLimit(
        100,
        this.myControl_code_place_num_i.value
      );
      if (!isNullOrUndefined(this.placets)) {
        try {
          this.filteredPlacets =
            this.myControl_code_place_num_i.valueChanges.pipe(
              startWith<string | PlaceT>(''),
              map((value) => {
                if (!isNullOrUndefined(value)) {
                  return typeof value === 'string' ? value : value.place_name;
                }
              }),
              map((name) => (name ? this._filter(name) : [...this.placets]))
            );
        } catch {}
      }
    }, 500);
  }
  inputLink_geoplace_code_j() {
    clearTimeout(this.timerId);
    this.timerId = setTimeout(async () => {
      this.placets = await this.placetService.getWithFilterLimit(
        100,
        this.myControl_code_place_num_j.value
      );
      if (!isNullOrUndefined(this.placets)) {
        try {
          this.filteredPlacets =
            this.myControl_code_place_num_j.valueChanges.pipe(
              startWith<string | PlaceT>(''),
              map((value) => {
                if (!isNullOrUndefined(value)) {
                  return typeof value === 'string' ? value : value.place_name;
                }
              }),
              map((name) => (name ? this._filter(name) : [...this.placets]))
            );
        } catch {}
      }
    }, 500);
  }

  private _filter(name: string): PlaceT[] {
    const filterValue = name.toLowerCase();
    return isNullOrUndefined(this.placets)
      ? null
      : this.placets.filter(
          (placet) =>
            placet.place_name.toLowerCase().indexOf(filterValue) !== -1
        );
  }

  displayFn_residence_code(place?: PlaceT): string | undefined {
    return place
      ? place.place_num + '. ' + place.place_code + '. ' + place.place_name
      : undefined;
  }

  inputLink_human_code(j) {
    clearTimeout(this.timerId);
    this.timerId = setTimeout(async () => {
      this.personalts = await this.personaltService.getWithShortfilterLimit(
        100,
        this.myControl_human_code[j].value
      );
      if (!isNullOrUndefined(this.personalts)) {
        try {
          this.filteredPersonalts = (<FormControl>(
            this.myControl_human_code[j]
          )).valueChanges.pipe(
            startWith<string | PersonalT>(''),
            map((value) => (typeof value === 'string' ? value : value.surname)),
            map((name) =>
              name ? this._filterPersonalt(name) : [...this.personalts]
            )
          );
        } catch {}
      }
    }, 500);
  }

  private _filterPersonalt(name: string): PersonalT[] {
    const filterValue = name.toLowerCase();
    return isNullOrUndefined(this.personalts)
      ? null
      : this.personalts
          .filter(
            (personalt) =>
              !isNullOrUndefined(personalt.surname) && personalt.surname !== ''
          )
          .filter(
            (personalt) =>
              personalt.surname.toLowerCase().indexOf(filterValue) !== -1
          );
    // this.personalts.filter(personalt => personalt.surname.toLowerCase().indexOf(filterValue) !== -1);
  }

  displayFn_human_code(personal?: PersonalT): string | undefined {
    if (!personal) {
      return undefined;
    }
    let str = '';
    str += personal.code + '. ';
    str += !isNullOrUndefined(personal.surname) ? personal.surname + ' ' : '';
    str += !isNullOrUndefined(personal.fname) ? personal.fname + ' ' : '';
    str += !isNullOrUndefined(personal.lname) ? personal.lname : '';
    return personal ? str : undefined;
  }

  // resetInq_date() {
  //   this.courttForm.patchValue({
  //     inq_date: null,
  //   });
  // }

  // resetSentense_date() {
  //   this.courttForm.patchValue({
  //     sentense_date: null,
  //   });
  // }

  async addImprist() {
    if (true) {
      try {
        await this.onSaveForm(true);
      } catch {
        return;
      }
    }
    let obj = {
      scroll: this.scroll,
      selectedImprist: true,
    };
    this.globalService.courtt_edit = obj;
    this.router.navigate([
      `/sys/person/impristedit/new_${this.selectedPersonalcode}_${this.selectedRepid}_${this.id}_0`,
    ]);
  }

  async editImprist(id) {
    if (this.hasChanges()) {
      try {
        await this.onSaveForm(true);
      } catch {
        return;
      }
    }
    let str = '/sys/person/impristedit/edit_';
    str += this.viewOnly ? 'v_' : '';
    str += `${this.selectedPersonalcode}_${this.selectedRepid}_${this.id}_${id}`;
    let obj = {
      scroll: this.scroll,
      selectedImprist: true,
    };
    this.globalService.courtt_edit = obj;
    this.router.navigate([str]);
    // this.router.navigate([`/sys/person/impristedit/edit_${this.selectedPersonalcode}_${this.selectedRepid}_${this.id}_${id}`]);
  }

  onAddPersonalT(idx) {
    this.filteredPersonalts = null;
    let dialogRef = this.dialog.open(DialogPersonalEditComponent, {
      panelClass: 'app-dialog-extend',
    });
    dialogRef.afterClosed().subscribe(async (result) => {
      if (result !== false) {
        let res = await this.personaltService.postOne(result);
        this.myControl_human_code[idx].setValue(res);
      }
    });
  }

  onLinkPersonalT(id) {
    let str = this.viewOnly
      ? `/sys/person/personaltedit/v_${id}`
      : `/sys/person/personaltedit/${id}`;
    this.router.navigate([str]);
  }

  getImpristypeByid(id: number): string {
    let impris_type = this.impris_types.find((item) => item.id === id);
    let str = '';
    if (!isNullOrUndefined(impris_type)) {
      str = `${impris_type.name_ent}`;
      // if (
      //   !isNullOrUndefined(impris_type.name_rmk) &&
      //   impris_type.name_rmk.length > 0
      // ) {
      //   str += ` (${impris_type.name_rmk})`;
      // }
    }
    return str;
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
    });
  }

  stop(e) {
    e.stopPropagation();
  }
  async drop(event: CdkDragDrop<string[]>, tableName: string) {
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    }
    try {
      // this.spinnerService.show();
      let newArr;
      switch (tableName) {
        case 'impris_t':
          newArr = await this.moveordernomService.move(
            tableName,
            this.selectedImprists[event.currentIndex].id,
            event.currentIndex + 1
          );
          this.selectedImprists = [...newArr];
          break;

        default:
          break;
      }
    } catch (err) {
      this.globalService.exceptionHandling(err);
    } finally {
      // this.spinnerService.hide();
    }
  }

  setArticlePos(field: HTMLInputElement) {
    this.articlePos = field.selectionStart;
    // console.log(field.selectionStart);
  }

  insertArticlePattern(pattarn) {
    let str = this.courttForm.value.article || '';
    let res =
      str.slice(0, this.articlePos) +
      pattarn.value +
      ' ' +
      str.slice(this.articlePos);
    this.articlePos += pattarn.value.length + 1;
    if (res.length > 100) {
      res = res.slice(0, 100);
    }
    this.courttForm.patchValue({ article: res });
  }

  setSentensPos(field: HTMLInputElement) {
    this.sentensPos = field.selectionStart;
    // console.log(field.selectionStart);
  }

  insertSentensPattern(pattarn) {
    let str = this.myControl_sentenses.value || '';
    let res =
      str.slice(0, this.sentensPos) +
      pattarn.value +
      ' ' +
      str.slice(this.sentensPos);
    this.sentensPos += pattarn.value.length + 1;
    if (res.length > 50) {
      res = res.slice(0, 50);
    }
    this.myControl_sentenses.setValue(res);
    // this.courttForm.patchValue({ article: res });
  }
}
