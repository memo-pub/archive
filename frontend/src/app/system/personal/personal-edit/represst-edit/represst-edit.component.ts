import {
  Component,
  OnInit,
  AfterContentChecked,
  HostListener,
  OnDestroy,
  ViewEncapsulation,
} from '@angular/core';
import { AuthService } from 'src/app/shared/services/auth.service';
import { PersonaltService } from 'src/app/system/shared/services/personalt.service';
import { ListoftService } from 'src/app/system/shared/services/listoft.service';
import { PlacetService } from 'src/app/system/shared/services/placet.service';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ListOfT } from 'src/app/shared/models/listoft.model';
import { isNullOrUndefined } from 'util';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { RepresstService } from 'src/app/system/shared/services/represst.service';
import { CoreptService } from 'src/app/system/shared/services/corept.service';
import { DialogYesNoComponent } from 'src/app/system/shared/components/dialog-yes-no/dialog-yes-no.component';
import { DialogSaveNoComponent } from 'src/app/system/shared/components/dialog-save-no/dialog-save-no.component';
import { PlaceT } from 'src/app/shared/models/placet.model';
import { Observable, Subscription } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { PersonalT } from 'src/app/shared/models/personalt.model';
import { CourtT } from 'src/app/shared/models/courtt.model';
import { CourttService } from 'src/app/system/shared/services/courtt.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { DialogPersonalEditComponent } from '../dialog-personal-edit/dialog-personal-edit.component';
import { corepT_width } from 'src/app/shared/models/corept.model';
import { repressT_width } from 'src/app/shared/models/represst.model';
import { MoveordernomService } from 'src/app/system/shared/services/moveordernom.service';
import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem,
} from '@angular/cdk/drag-drop';
import { RehabilreasontService } from 'src/app/system/shared/services/rehabilreasont.service';
import { DateparseService } from 'src/app/shared/services/dateparse.service';
import { ActivityT } from 'src/app/shared/models/activityt.model';
import { ActivitytService } from 'src/app/system/shared/services/activityt.service';

@Component({
  selector: 'app-represst-edit',
  templateUrl: './represst-edit.component.html',
  styleUrls: ['./represst-edit.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class RepresstEditComponent
  implements OnInit, AfterContentChecked, OnDestroy {
  constructor(
    public dateparseService: DateparseService,
    private authService: AuthService,
    public globalService: GlobalService,
    private personaltService: PersonaltService,
    private activitytService: ActivitytService,
    private listoftService: ListoftService,
    private placetService: PlacetService,
    private represstService: RepresstService,
    private coreptService: CoreptService,
    private courttService: CourttService,
    private rehabilreasontService: RehabilreasontService,
    private moveordernomService: MoveordernomService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private spinnerService: NgxSpinnerService,
    public dialog: MatDialog,
    public snackBar: MatSnackBar
  ) {
    this.activeRouterObserver = this.activatedRoute.params.subscribe(
      (param) => {
        let str = param.id.split('_');
        if (str[0] === 'new') {
          this.intType = 'newdoc';
          this.selectedPersonalcode = +str[1];
          this.viewOnly = false;
        } else {
          if (str[1] === 'v') {
            this.id = +str[3];
            this.selectedPersonalcode = +str[2];
            this.intType = 'editdoc';
            this.viewOnly = true;
          } else {
            this.id = +str[2];
            this.selectedPersonalcode = +str[1];
            this.intType = 'editdoc';
            this.viewOnly = false;
          }
        }
        this.viewOnly =
          this.viewOnly === false
            ? this.authService.getCurUser().usertype === 4
            : true;
      }
    );
    this.globalService.routerObserver = router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        if (val.url.indexOf('represstedit') !== -1) {
          this.globalService.breadcrumbs.length = 0;
          this.globalService.breadcrumbs.push(
            {
              label: 'персоналии',
              url:
                this.viewOnly === true
                  ? `/sys/person/personaltedit/v_${this.selectedPersonalcode}`
                  : `/sys/person/personaltedit/${this.selectedPersonalcode}`,
            },
            { label: 'репрессия' }
          );
          this.breadcrumbs = this.globalService.breadcrumbs;
        }
      }
    });
  }
  corepT_width = corepT_width;
  repressT_width = repressT_width;

  timerId;
  id: number;
  intType: string | any = ''; // тип интерфейса
  viewOnly: boolean;
  selectedPersonalcode: number;
  selectedPersonal: PersonalT;

  represstForm: FormGroup;
  coreptForms: FormGroup[] = [];

  coreptidTodel: number[] = [];

  placets: PlaceT[];
  filteredPlacets: Observable<PlaceT[]>;

  personalts: PersonalT[];
  filteredPersonalts: Observable<PersonalT[]>;

  myControl_human_code: FormControl[] = [];
  myControl_geoplace_code: FormControl;

  rehabilreasont: any[] = [];
  rehabilreasontInForm: any[] = [];

  repress_types: ListOfT[];
  rehab_reases: ListOfT[];
  sentenses: ListOfT[];

  selectedCourtts: CourtT[] = [];

  valid: boolean;

  breadcrumbs = [];

  scroll = 0;
  activeRouterObserver: Subscription;

  rehabil_orgPos = 0;
  rehabil_orgPatternsOpen = false;
  rehabil_orgPatterns = [
    { value: 'ВК', title: 'Военная коллегия' },
    { value: 'ВС', title: 'Верховных суд' },
    { value: 'ВТ', title: 'Военный трибунал (существуют в Военных округах)' },
    { value: 'ИЦ', title: 'Информационный центр' },
    {
      value: 'ГИАЦ МВД',
      title: 'Главный информационный и аналитический центр',
    },
    {
      value: 'ГВП',
      title:
        'Главная военная прокуратура (во всех остальных случаях Прокуратура полностью)',
    },
    { value: 'ГУ МВД', title: '' },
    { value: 'УВД', title: '' },
    { value: 'ГУВД', title: '' },
    { value: 'УНКВД', title: '' },
    { value: 'УМВД', title: '' },
    { value: 'Облисполком', title: '' },
    { value: 'Райсовет', title: '' },
    { value: 'Транспортная коллегия', title: '' },
    { value: 'Прокуратура', title: '' },
    { value: 'Судебная коллегия по уголовным делам ВС', title: '' },
    { value: 'ВО', title: 'Военный округ. Московский ВО, Северный ВО и т.п.' },
    { value: 'обл.', title: 'областной. Ленинградский обл. cуд' },
    { value: 'гор.', title: 'городской. Московский гор. cуд' },
    { value: 'г.', title: 'города, город' },
    { value: 'краев.', title: 'краевой' },
  ];

  selectedActivityts: ActivityT[] = [];

  @HostListener('window:scroll', ['$event'])
  getScroll(event) {
    this.scroll = window.pageYOffset;
  }

  @HostListener('window:keydown', ['$event'])
  onKeyPress(event: KeyboardEvent) {
    if ((event.ctrlKey || event.metaKey) && event.keyCode == 83) {
      event.preventDefault();
      if (!this.viewOnly && this.valid && this.intType !== 'myprofile') {
        this.onSaveForm();
      }
    }
  }

  ngOnDestroy(): void {
    this.globalService.routerObserver.unsubscribe();
    // this.activeRouterObserver.unsubscribe();
  }

  async ngOnInit() {
    let obj = {
      scroll: this.scroll,
      selectedImprist: false,
    };
    this.globalService.courtt_edit = obj;
    this.represstForm = new FormGroup({
      id: new FormControl({ value: null, disabled: this.viewOnly }, []),
      personal_code: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      repress_no: new FormControl({ value: null, disabled: true }, [
        Validators.required,
      ]),
      repress_type: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      repress_type_id: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      court_mark: new FormControl({ value: null, disabled: this.viewOnly }, []),
      // repress_dat: new FormControl({ value: null, disabled: true }, []),
      repress_dat_begin: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      repress_dat_end: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      repress_dat_begin_end: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      // geoplace_code: new FormControl(
      //   { value: null, disabled: this.viewOnly },
      //   []
      // ),
      reabil_mark: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      // reabil_dat: new FormControl({ value: null, disabled: this.viewOnly }, []),
      reabil_dat: new FormControl(
        {
          value: null,
          disabled: true,
        },
        []
      ),
      reabil_dat_begin: new FormControl(
        {
          value: null,
          disabled: this.viewOnly === true,
        },
        []
      ),
      reabil_dat_end: new FormControl(
        {
          value: null,
          disabled: this.viewOnly === true,
        },
        []
      ),
      reabil_dat_begin_end: new FormControl(
        {
          value: null,
          disabled: this.viewOnly === true,
        },
        []
      ),
      repress_type_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      repress_dat_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      geoplace_code_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      // reabil_rmk: new FormControl({ value: null, disabled: this.viewOnly }, []),
      reabil_rmk: new FormControl(
        {
          value: null,
          disabled: this.viewOnly === true,
        },
        []
      ),
      repress_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      place_num: new FormControl({ value: null, disabled: this.viewOnly }, []),
      rehabil_org: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      rehabil_reason_id: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      activityts: new FormControl({ value: [], disabled: this.viewOnly }, []),
    });
    this.myControl_geoplace_code = new FormControl({
      value: null,
      disabled: this.viewOnly,
    });
    try {
      let repress_types = this.listoftService.getWithFilter('REPRESS_TYPE');
      let rehab_reases = this.listoftService.getWithFilter('REHAB_REAS');
      let sentenses = this.listoftService.getWithFilter('SENTENSE');
      this.sentenses = isNullOrUndefined(await sentenses)
        ? []
        : await sentenses;
      this.repress_types = isNullOrUndefined(await repress_types)
        ? []
        : await repress_types;
      this.repress_types.sort((a, b) => (a.name_ent < b.name_ent ? -1 : 1));
      this.rehab_reases = isNullOrUndefined(await rehab_reases)
        ? []
        : await rehab_reases;
      this.rehab_reases.sort((a, b) => (a.name_ent < b.name_ent ? -1 : 1));
      await this.getData();
    } catch (err) {
      this.globalService.exceptionHandling(err);
    } finally {
      setTimeout(() => {
        window.scrollTo(0, this.globalService.represst_edit.scroll);
      }, 200);
    }
  }

  ngAfterContentChecked(): void {
    this.isValid();
  }

  async getData() {
    this.spinnerService.show();
    let selectedPersonal;
    let selectedActivityts;
    try {
      selectedPersonal = this.personaltService.getBycode(
        this.selectedPersonalcode
      );
      selectedActivityts = this.activitytService.getByPersonalcode(
        this.selectedPersonalcode
      );
    } catch (err) {
      this.globalService.exceptionHandling(err);
    }
    switch (this.intType) {
      case 'editdoc': // редактирование
        try {
          this.myControl_human_code.length = 0;
          this.coreptForms.length = 0;
          let _selectedRepresst = this.represstService.getOneById(this.id);
          let rehabilreasont = this.rehabilreasontService.getByRepid(this.id);
          let selectedRepresst = await _selectedRepresst;
          this.rehabilreasont = isNullOrUndefined(await rehabilreasont)
            ? []
            : await rehabilreasont;

          this.rehabilreasontInForm = this.rehabilreasont.map(
            (rehabilreasont) => {
              let rehab_rease = this.rehab_reases.find(
                (item) => item.id === rehabilreasont.reason_id
              );
              if (!isNullOrUndefined(rehab_rease)) {
                return rehab_rease;
              }
            }
          );
          this.rehabilreasontInForm.sort((a, b) => {
            if (a.name_ent < b.name_ent) {
              return -1;
            }
            if (a.last_nom > b.last_nom) {
              return 1;
            }
            return 0;
          });
          this.represstForm.reset();
          this.represstForm.patchValue({
            id: selectedRepresst.id,
            personal_code: selectedRepresst.personal_code,
            repress_no: selectedRepresst.repress_no,
            repress_type: selectedRepresst.repress_type,
            repress_type_id: selectedRepresst.repress_type_id,
            court_mark: selectedRepresst.court_mark === '*' ? true : false,
            // repress_dat: selectedRepresst.repress_dat,
            repress_dat_begin: selectedRepresst.repress_dat_begin,
            repress_dat_end: selectedRepresst.repress_dat_end,
            // geoplace_code: selectedRepresst.geoplace_code,
            reabil_mark: selectedRepresst.reabil_mark === '*' ? true : false,
            reabil_dat: selectedRepresst.reabil_dat,
            reabil_dat_begin: selectedRepresst.reabil_dat_begin,
            reabil_dat_end: selectedRepresst.reabil_dat_end,
            repress_type_rmk: selectedRepresst.repress_type_rmk,
            repress_dat_rmk: selectedRepresst.repress_dat_rmk,
            geoplace_code_rmk: selectedRepresst.geoplace_code_rmk,
            reabil_rmk: selectedRepresst.reabil_rmk,
            repress_rmk: selectedRepresst.repress_rmk,
            place_num: selectedRepresst.place_num,
            rehabil_org: selectedRepresst.rehabil_org,
            rehabil_reason_id: this.rehabilreasontInForm,
          });
          if (selectedRepresst.rehabil_org) {
            this.rehabil_orgPos = selectedRepresst.rehabil_org.length;
          }
          this.dateparseService.setSourDate(
            this.represstForm,
            'repress_dat_begin_end',
            selectedRepresst.repress_dat_begin,
            selectedRepresst.repress_dat_end
          );
          this.dateparseService.setSourDate(
            this.represstForm,
            'reabil_dat_begin_end',
            selectedRepresst.reabil_dat_begin,
            selectedRepresst.reabil_dat_end
          );
          let selectedCourtts = this.courttService.getByRepid(
            selectedRepresst.id
          );
          if (!isNullOrUndefined(selectedRepresst.place_num)) {
            let _geoplace_code = await this.placetService.getByPlace_num(
              selectedRepresst.place_num
            );
            this.myControl_geoplace_code.reset();
            if (
              !isNullOrUndefined(_geoplace_code) &&
              _geoplace_code.length > 0
            ) {
              this.myControl_geoplace_code.setValue(_geoplace_code[0]);
            }
          }

          let _selectedCorepts = this.coreptService.getByRepid(
            selectedRepresst.id
          );
          let selectedCorepts = isNullOrUndefined(await _selectedCorepts)
            ? []
            : await _selectedCorepts;
          for (let j = 0; j < selectedCorepts.length; j++) {
            this.coreptForms.push(
              new FormGroup({
                id: new FormControl(
                  { value: null, disabled: this.viewOnly },
                  []
                ),
                rep_id: new FormControl(
                  { value: null, disabled: this.viewOnly },
                  []
                ),
                human_code: new FormControl(
                  { value: null, disabled: this.viewOnly },
                  [Validators.required]
                ),
                role: new FormControl(
                  { value: null, disabled: this.viewOnly },
                  []
                ),
                role_rmk: new FormControl(
                  { value: null, disabled: this.viewOnly },
                  []
                ),
                sign_mark: new FormControl(
                  { value: null, disabled: this.viewOnly },
                  []
                ),
                cotype: new FormControl(null),
              })
            );
            this.myControl_human_code.push(
              new FormControl({ value: null, disabled: this.viewOnly })
            );
            this.coreptForms[j].patchValue({
              id: selectedCorepts[j].id,
              rep_id: selectedCorepts[j].rep_id,
              human_code: selectedCorepts[j].human_code,
              role: selectedCorepts[j].role,
              role_rmk: selectedCorepts[j].role_rmk,
              sign_mark: selectedCorepts[j].sign_mark,
              cotype: selectedCorepts[j].cotype,
            });
            if (!isNullOrUndefined(selectedCorepts[j].human_code)) {
              let person = await this.personaltService.getBycode(
                selectedCorepts[j].human_code
              );
              if (!isNullOrUndefined(person) && person.length > 0) {
                this.myControl_human_code[j].setValue(person[0]);
              }
            }
          }
          this.selectedCourtts = isNullOrUndefined(await selectedCourtts)
            ? []
            : await selectedCourtts;
          this.selectedCourtts.sort((a, b) =>
            a.trial_no > b.trial_no ? 1 : -1
          );
          let obj = {
            scroll: this.globalService.represst_edit.scroll,
            selectedCourtt: this.selectedCourtts.length > 0 ? true : false,
          };
          this.globalService.represst_edit = obj;
        } catch (err) {
          this.globalService.exceptionHandling(err);
        } finally {
          this.spinnerService.hide();
        }
        break;
      case 'newdoc': // создание
        try {
          let _represses = this.represstService.getByPersonalcode(
            this.selectedPersonalcode
          );
          let represses = isNullOrUndefined(await _represses)
            ? []
            : await _represses;
          let num: number;
          if (represses.length === 0) {
            num = 1;
          } else {
            represses.sort((a, b) => b.repress_no - a.repress_no);
            num = represses[0].repress_no + 1;
          }
          this.represstForm.reset();
          this.represstForm.patchValue({
            repress_no: num,
          });
        } catch (err) {
          this.globalService.exceptionHandling(err);
        } finally {
          this.spinnerService.hide();
        }
        break;
      default:
    }
    this.selectedPersonal = (await selectedPersonal)[0];
    this.selectedActivityts = (await selectedActivityts) || [];
    this.selectedActivityts = this.selectedActivityts.filter(
      (item) => !item.rep_id || item.rep_id === this.id
    );
    this.represstForm.patchValue({
      activityts: this.selectedActivityts
        .filter((item) => item.rep_id === this.id)
        .map((item) => item.id),
    });
    this.spinnerService.hide();
    this.reabChange();
  }

  addCorept(cotype: number) {
    this.coreptForms.push(
      new FormGroup({
        rep_id: new FormControl(null, []),
        human_code: new FormControl(null, [Validators.required]),
        role: new FormControl(null, []),
        role_rmk: new FormControl(null, []),
        sign_mark: new FormControl(null, []),
        cotype: new FormControl(cotype),
      })
    );
    this.myControl_human_code.push(new FormControl(null));
  }

  delCorept(idx) {
    if (!isNullOrUndefined(this.coreptForms[idx].value.id)) {
      this.coreptidTodel.push(this.coreptForms[idx].value.id);
    }
    this.coreptForms.splice(idx, 1);
    this.myControl_human_code.splice(idx, 1);
  }

  async onDel() {
    let dialogRef = this.dialog.open(DialogYesNoComponent, {
      data: {
        title: 'Удалить репрессию?',
        question: ``,
      },
    });
    dialogRef.afterClosed().subscribe(async (result) => {
      if (result === true) {
        this.spinnerService.show();
        try {
          let _corepts = this.coreptService.getByRepid(this.id);
          let corepts = isNullOrUndefined(await _corepts) ? [] : await _corepts;
          await Promise.all(
            corepts.map(async (corept) => {
              return this.coreptService.deleteOneById(corept.id);
            })
          );
          await this.represstService.deleteOneById(this.id);
          this.router.navigate([
            `/sys/person/personaltedit/${this.selectedPersonalcode}`,
          ]);
          this.openSnackBar(`Удалено.`, 'Ok');
        } catch (err) {
          console.error(`Ошибка при удалении ${err}`);
          this.globalService.exceptionHandling(err);
        } finally {
          this.spinnerService.hide();
        }
      }
    });
  }

  /**
   * Функция возвращает true, если бли внесены изменения
   */
  hasChanges(): boolean {
    let res = false;
    if (this.represstForm.dirty || this.myControl_geoplace_code.dirty) {
      res = true;
    }
    this.coreptForms.map((form) => {
      if (form.dirty) {
        res = true;
      }
    });
    this.myControl_human_code.map((control) => {
      if (control.dirty) {
        res = true;
      }
    });
    return res;
  }

  async changePubStatus() {
    const isPublished = this.selectedPersonal.published === '*';

    if (isPublished) {
      const curDate = new Date().toISOString().split('T')[0];
      this.selectedPersonal.mustPublish = '*';
      this.selectedPersonal.mustUnpublish = null;
      this.selectedPersonal.pub_change_date = curDate;

      await this.personaltService.putOneById(
        this.selectedPersonalcode,
        this.selectedPersonal
      );
    }
  }

  /**
   * Функция сохраняет форму. Silent для показа сообщения. Возвращает true в случаеуспешного сохранения
   */
  async onSaveForm(silent = false, update = true) {
    let res = false;
    try {
      // if (
      //   !isNullOrUndefined(this.represstForm.getRawValue()['repress_dat']) &&
      //   this.represstForm.getRawValue()['repress_dat'] !== ''
      // ) {
      //   if (
      //     !(
      //       new Date(this.represstForm.getRawValue()['repress_dat']) instanceof
      //         Date &&
      //       !isNaN(
      //         new Date(this.represstForm.getRawValue()['repress_dat']).valueOf()
      //       )
      //     )
      //   ) {
      //     this.openSnackBar(
      //       `Введена не существующая дата начала репрессии`,
      //       'Ok'
      //     );
      //     return false;
      //   }

      // } else {
      //   this.represstForm.patchValue({
      //     repress_dat: null,
      //   });
      // }
      // if (
      //   !isNullOrUndefined(this.represstForm.getRawValue()['reabil_dat']) &&
      //   this.represstForm.getRawValue()['reabil_dat'] !== ''
      // ) {
      //   if (
      //     !(
      //       new Date(this.represstForm.getRawValue()['reabil_dat']) instanceof
      //         Date &&
      //       !isNaN(
      //         new Date(this.represstForm.getRawValue()['reabil_dat']).valueOf()
      //       )
      //     )
      //   ) {
      //     this.openSnackBar(`Введена не существующая дата реабилитации`, 'Ok');
      //     return false;
      //   }
      // } else {
      //   this.represstForm.patchValue({
      //     reabil_dat: null,
      //   });
      // }
      if (
        this.represstForm.getRawValue()['court_mark'] === true ||
        this.represstForm.getRawValue()['court_mark'] === '*'
      ) {
        this.represstForm.patchValue({
          court_mark: '*',
        });
      } else {
        this.represstForm.patchValue({
          court_mark: null,
        });
      }
      if (
        this.represstForm.getRawValue()['reabil_mark'] === true ||
        this.represstForm.getRawValue()['reabil_mark'] === '*'
      ) {
        this.represstForm.patchValue({
          reabil_mark: '*',
        });
      } else {
        this.represstForm.patchValue({
          reabil_mark: null,
        });
      }
      if (
        !isNullOrUndefined(this.myControl_geoplace_code.value) &&
        this.myControl_geoplace_code.value !== ''
      ) {
        if (!isNullOrUndefined(this.myControl_geoplace_code.value.place_num)) {
          this.represstForm.patchValue({
            place_num: this.myControl_geoplace_code.value.place_num,
          });
        } else {
          let dialogRef = this.dialog.open(DialogYesNoComponent, {
            data: {
              title: 'Введено не корректное место жительства.',
              question: `Продолжить?`,
            },
          });
          await new Promise((resolve, reject) => {
            dialogRef.afterClosed().subscribe(async (result) => {
              if (result === true) {
                this.myControl_geoplace_code.reset();
                this.represstForm.patchValue({
                  place_num: null,
                });
                resolve(null);
              } else {
                return false;
              }
            });
          });
          this.myControl_geoplace_code.reset();
          this.represstForm.patchValue({
            place_num: null,
          });
        }
      } else {
        this.myControl_geoplace_code.reset();
        this.represstForm.patchValue({
          place_num: null,
        });
      }
      this.spinnerService.show();
      switch (this.intType) {
        case 'editdoc': // редактирование документа
          /**Удаляем не выбранные места работы*/
          await Promise.all(
            this.selectedActivityts.map((item) => {
              if (!this.represstForm.value.activityts.includes(item.id)) {
                return this.activitytService.putOneById(item.id, {
                  rep_id: null,
                });
              }
            })
          );
          /**Удаляем дочерние записи, удаленные из формы*/
          await Promise.all(
            this.coreptidTodel.map((id) => {
              return this.coreptService.deleteOneById(id);
            })
          );
          /**Сохранение дочерних сущностей */
          await Promise.all(
            this.represstForm.value.activityts.map((id) => {
              return this.activitytService.putOneById(id, {
                rep_id: this.id,
              });
            })
          );
          for (let j = 0; j < this.coreptForms.length; j++) {
            if (
              !isNullOrUndefined(this.myControl_human_code[j].value) &&
              !isNullOrUndefined(this.myControl_human_code[j].value.code)
            ) {
              if (!isNullOrUndefined(this.myControl_human_code[j].value)) {
                this.coreptForms[j].patchValue({
                  human_code: this.myControl_human_code[j].value.code,
                });
              }
              if (
                this.coreptForms[j].value['sign_mark'] === true ||
                this.coreptForms[j].value['sign_mark'] === '*'
              ) {
                this.coreptForms[j].patchValue({
                  sign_mark: '*',
                });
              } else {
                this.coreptForms[j].patchValue({
                  sign_mark: null,
                });
              }
              this.coreptForms[j].patchValue({
                rep_id: this.id,
              });
              if (isNullOrUndefined(this.coreptForms[j].value.id)) {
                this.coreptForms[j].removeControl('id');
                if (
                  !isNullOrUndefined(this.coreptForms[j].value.rep_id) &&
                  !isNullOrUndefined(this.coreptForms[j].value.human_code)
                ) {
                  await this.coreptService.postOne(this.coreptForms[j].value);
                }
              } else {
                if (
                  this.coreptForms[j].touched ||
                  this.myControl_human_code[j].touched
                ) {
                  await this.coreptService.putOneById(
                    this.coreptForms[j].value.id,
                    this.coreptForms[j].value
                  );
                }
              }
            } else {
              this.openSnackBar(
                `Не верно указано лицо, упоминаемое в связи с репрессией #${
                  j + 1
                }`,
                'Ok'
              );
              return false;
            }
          }
          let rehabilreasontToDel = [];
          if (!isNullOrUndefined(this.rehabilreasont)) {
            this.rehabilreasont.forEach((rehabilreasont) => {
              let rehabilreasontInForm = this.represstForm
                .getRawValue()
                .rehabil_reason_id.find(
                  (item) => item.id === rehabilreasont.reason_id
                );
              if (isNullOrUndefined(rehabilreasontInForm)) {
                let res = this.rehabilreasontService.deleteOneById(
                  rehabilreasont.id
                );
                rehabilreasontToDel.push(res);
              }
            });
          }
          let rehabilreasontToPost = [];
          if (
            !isNullOrUndefined(
              this.represstForm.getRawValue().rehabil_reason_id
            )
          ) {
            this.represstForm
              .getRawValue()
              .rehabil_reason_id.forEach((rehabilreasontInForm) => {
                let rehabilreasont = this.rehabilreasont.find(
                  (item) => item.reason_id === rehabilreasontInForm.id
                );
                if (isNullOrUndefined(rehabilreasont)) {
                  let obj = {
                    rep_id: this.id,
                    reason_id: rehabilreasontInForm.id,
                  };
                  let res = this.rehabilreasontService.postOne(obj);
                  rehabilreasontToPost.push(res);
                }
              });
          }
          await Promise.all(rehabilreasontToDel);
          await Promise.all(rehabilreasontToPost);
          let dataPut = { ...this.represstForm.getRawValue() };
          delete dataPut.rehabil_reason_id;
          await this.represstService.putOneById(this.id, dataPut);
          await this.changePubStatus();
          break;
        case 'newdoc': // создание документа
          this.represstForm.patchValue({
            personal_code: this.selectedPersonalcode,
          });
          this.represstForm.removeControl('id');
          let dataPost = { ...this.represstForm.getRawValue() };
          delete dataPost.rehabil_reason_id;
          let postForm = await this.represstService.postOne(dataPost);
          this.id = +postForm.id;
          await Promise.all(
            this.represstForm.value.activityts.map((id) => {
              return this.activitytService.putOneById(id, {
                rep_id: this.id,
              });
            })
          );
          this.intType = 'editdoc';
          let rehabilreasontToAdd = [];
          if (
            !isNullOrUndefined(
              this.represstForm.getRawValue().rehabil_reason_id
            )
          ) {
            this.represstForm
              .getRawValue()
              .rehabil_reason_id.forEach((rehabilreasontInForm) => {
                let obj = {
                  rep_id: this.id,
                  reason_id: rehabilreasontInForm.id,
                };
                let res = this.rehabilreasontService.postOne(obj);
                rehabilreasontToAdd.push(res);
              });
          }
          await Promise.all(rehabilreasontToAdd);
          /**Сохранение дочерних сущностей */
          for (let j = 0; j < this.coreptForms.length; j++) {
            if (
              !isNullOrUndefined(this.myControl_human_code[j].value) &&
              !isNullOrUndefined(this.myControl_human_code[j].value.code)
            ) {
              if (!isNullOrUndefined(this.myControl_human_code[j].value)) {
                this.coreptForms[j].patchValue({
                  human_code: this.myControl_human_code[j].value.code,
                });
              }
              if (
                this.coreptForms[j].value['sign_mark'] === true ||
                this.coreptForms[j].value['sign_mark'] === '*'
              ) {
                this.coreptForms[j].patchValue({
                  sign_mark: '*',
                });
              } else {
                this.coreptForms[j].patchValue({
                  sign_mark: null,
                });
              }
              this.coreptForms[j].patchValue({
                rep_id: this.id,
              });
              this.coreptForms[j].removeControl('id');
              await this.coreptService.postOne(this.coreptForms[j].value);
            } else {
              this.openSnackBar(
                `Не верно указано лицо, упоминаемое в связи с репрессией #${
                  j + 1
                }`,
                'Ok'
              );
              return false;
            }
          }
          break;
        default:
      }
      if (update === true) {
        await this.getData();
      }
      if (!silent) {
        this.openSnackBar(`Сохранено.`, 'Ok');
      }
      res = true;
    } catch (err) {
      if (err.status === 401) {
        console.error(`Ошибка авторизации`);
        this.authService.logout();
      } else if (err.status === 409) {
        this.represstForm.controls['repress_no'].setErrors({ incorrect: true });
        this.openSnackBar(`${err.error['error']}`, 'Ok');
      } else {
        console.error(`Ошибка ${err}`);
        this.openSnackBar(`Ошибка ${err.status}`, 'Ok');
      }
      throw false;
    } finally {
      this.spinnerService.hide();
      return res;
    }
  }

  async goBack(url = '') {
    let str = '/sys/person/personaltedit/';
    str += this.viewOnly ? 'v_' : '';
    str += `${this.selectedPersonalcode}`;
    let saved = true;
    if (this.hasChanges() && this.valid) {
      let dialogRef = this.dialog.open(DialogSaveNoComponent, {
        data: {
          title: 'Сохранить изменения?',
          question: ``,
        },
      });
      dialogRef.afterClosed().subscribe(async (result) => {
        if (result === true) {
          saved = await this.onSaveForm(true);
          if (saved === true) {
            if (url === '') {
              this.router.navigate([str]);
              // this.router.navigate([`/sys/person/personaltedit/${this.selectedPersonalcode}`]);
            } else {
              this.router.navigate([url]);
            }
          }
        } else if (result === false) {
          if (url === '') {
            this.router.navigate([str]);
            // this.router.navigate([`/sys/person/personaltedit/${this.selectedPersonalcode}`]);
          } else {
            this.router.navigate([url]);
          }
        } else {
          return;
        }
      });
    } else if (this.hasChanges() && !this.valid) {
      let dialogRef = this.dialog.open(DialogYesNoComponent, {
        data: {
          title: 'Изменения не будут сохранены!',
          question: `Не заполнены обязательные поля. Уйти со страницы?`,
        },
      });
      dialogRef.afterClosed().subscribe(async (result) => {
        if (result === true) {
          if (url === '') {
            this.router.navigate([str]);
            // this.router.navigate([`/sys/person/personaltedit/${this.selectedPersonalcode}`]);
          } else {
            this.router.navigate([url]);
          }
        } else {
          return;
        }
      });
    } else {
      if (url === '') {
        this.router.navigate([str]);
        // this.router.navigate([`/sys/person/personaltedit/${this.selectedPersonalcode}`]);
      } else {
        this.router.navigate([url]);
      }
    }
  }

  isValid() {
    let res = this.represstForm.valid;
    this.myControl_human_code.map((control) => {
      if (
        !(
          typeof control.value === 'object' && !isNullOrUndefined(control.value)
        )
      ) {
        res = false;
      }
    });
    this.valid = res;
  }

  inputLink_geoplace_code() {
    clearTimeout(this.timerId);
    this.timerId = setTimeout(async () => {
      this.placets = await this.placetService.getWithFilterLimit(
        100,
        this.myControl_geoplace_code.value
      );
      if (!isNullOrUndefined(this.placets)) {
        try {
          this.filteredPlacets = this.myControl_geoplace_code.valueChanges.pipe(
            startWith<string | PlaceT>(''),
            map((value) => {
              if (!isNullOrUndefined(value)) {
                return typeof value === 'string' ? value : value.place_name;
              }
            }),
            map((name) => (name ? this._filter(name) : [...this.placets]))
          );
        } catch {}
      }
    }, 500);
  }

  private _filter(name: string): PlaceT[] {
    const filterValue = name.toLowerCase();
    return isNullOrUndefined(this.placets)
      ? null
      : this.placets.filter(
          (placet) =>
            placet.place_name.toLowerCase().indexOf(filterValue) !== -1
        );
  }

  displayFn_residence_code(place?: PlaceT): string | undefined {
    return place
      ? place.place_num + '. ' + place.place_code + '. ' + place.place_name
      : undefined;
  }

  inputLink_human_code(j) {
    clearTimeout(this.timerId);
    this.timerId = setTimeout(async () => {
      this.personalts = await this.personaltService.getWithShortfilterLimit(
        100,
        this.myControl_human_code[j].value
      );
      if (!isNullOrUndefined(this.personalts)) {
        try {
          this.filteredPersonalts = (<FormControl>(
            this.myControl_human_code[j]
          )).valueChanges.pipe(
            startWith<string | PersonalT>(''),
            map((value) => (typeof value === 'string' ? value : value.surname)),
            map((name) =>
              name ? this._filterPersonalt(name) : [...this.personalts]
            )
          );
        } catch {}
      }
    }, 500);
  }

  private _filterPersonalt(name: string): PersonalT[] {
    const filterValue = name.toLowerCase();
    return isNullOrUndefined(this.personalts)
      ? null
      : this.personalts
          .filter(
            (personalt) =>
              !isNullOrUndefined(personalt.surname) && personalt.surname !== ''
          )
          .filter(
            (personalt) =>
              personalt.surname.toLowerCase().indexOf(filterValue) !== -1
          );
    // this.personalts.filter(personalt => personalt.surname.toLowerCase().indexOf(filterValue) !== -1);
  }

  displayFn_human_code(personal?: PersonalT): string | undefined {
    if (!personal) {
      return undefined;
    }
    let str = '';
    str += personal.code + '. ';
    str += !isNullOrUndefined(personal.surname) ? personal.surname + ' ' : '';
    str += !isNullOrUndefined(personal.fname) ? personal.fname + ' ' : '';
    str += !isNullOrUndefined(personal.lname) ? personal.lname : '';
    return personal ? str : undefined;
  }

  // resetRepress_dat() {
  //   this.represstForm.patchValue({
  //     repress_dat: null,
  //   });
  // }

  // resetReabil_dat() {
  //   this.represstForm.patchValue({
  //     reabil_dat: null,
  //   });
  // }

  async addCourtt() {
    // if (this.hasChanges()) {
    if (true) {
      try {
        await this.onSaveForm(true);
      } catch {
        return;
      }
    }
    let obj = {
      scroll: this.scroll,
      selectedCourtt: true,
    };
    this.globalService.represst_edit = obj;
    this.router.navigate([
      `/sys/person/courttedit/new_${this.selectedPersonalcode}_${this.id}_0`,
    ]);
  }

  async editCourtt(id) {
    if (this.hasChanges() === true) {
      try {
        await this.onSaveForm(true, false);
      } catch {
        return;
      }
    }
    let str = '/sys/person/courttedit/edit_';
    str += this.viewOnly ? 'v_' : '';
    str += `${this.selectedPersonalcode}_${this.id}_${id}`;
    let obj = {
      scroll: this.scroll,
      selectedCourtt: true,
    };
    this.globalService.represst_edit = obj;
    this.router.navigate([str]);
  }

  onAddPersonalT(idx) {
    this.filteredPersonalts = null;
    let dialogRef = this.dialog.open(DialogPersonalEditComponent, {
      panelClass: 'app-dialog-extend',
    });
    dialogRef.afterClosed().subscribe(async (result) => {
      if (result !== false) {
        let res = await this.personaltService.postOne(result);
        this.myControl_human_code[idx].setValue(res);
      }
    });
  }

  onLinkPersonalT(id) {
    let str = this.viewOnly
      ? `/sys/person/personaltedit/v_${id}`
      : `/sys/person/personaltedit/${id}`;
    this.router.navigate([str]);
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
    });
  }

  stop(e) {
    e.stopPropagation();
  }
  async drop(event: CdkDragDrop<string[]>, tableName: string) {
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    }
    try {
      // this.spinnerService.show();
      let newArr;
      switch (tableName) {
        case 'court_t':
          newArr = await this.moveordernomService.move(
            tableName,
            this.selectedCourtts[event.currentIndex].id,
            event.currentIndex + 1
          );
          this.selectedCourtts = [...newArr];
          break;

        default:
          break;
      }
    } catch (err) {
      this.globalService.exceptionHandling(err);
    } finally {
      // this.spinnerService.hide();
    }
  }
  reabChange() {
    if (
      this.represstForm.getRawValue().reabil_mark === true &&
      this.viewOnly !== true
    ) {
      this.represstForm.controls.reabil_rmk.enable();
      this.represstForm.controls.reabil_dat.enable();
    } else {
      this.represstForm.controls.reabil_rmk.disable();
      this.represstForm.controls.reabil_dat.disable();
    }
  }

  setRehabil_orgPos(field: HTMLInputElement) {
    this.rehabil_orgPos = field.selectionStart;
  }

  insertRehabil_orgPattern(pattarn) {
    let str = this.represstForm.value.rehabil_org || '';
    let res =
      str.slice(0, this.rehabil_orgPos) +
      pattarn.value +
      ' ' +
      str.slice(this.rehabil_orgPos);
    this.represstForm.patchValue({ rehabil_org: res });
    this.rehabil_orgPos += pattarn.value.length + 1;
  }
}
