import {
  Component,
  OnInit,
  HostListener,
  OnDestroy,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/shared/services/auth.service';
import { SourtService } from 'src/app/system/shared/services/sourt.service';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DialogYesNoComponent } from 'src/app/system/shared/components/dialog-yes-no/dialog-yes-no.component';
import { isNullOrUndefined } from 'util';
import { DialogSaveNoComponent } from 'src/app/system/shared/components/dialog-save-no/dialog-save-no.component';
import { GlobalService } from 'src/app/shared/services/global.service';
import { ListOfT } from 'src/app/shared/models/listoft.model';
import { Subscription } from 'rxjs';
import { saveAs as importedSaveAs } from 'file-saver';
import { ListoftService } from 'src/app/system/shared/services/listoft.service';
import { PersonaltService } from 'src/app/system/shared/services/personalt.service';
import { PersonalT } from 'src/app/shared/models/personalt.model';
import { sourT_width, SourT } from 'src/app/shared/models/sourt.model';
import { SourtypeService } from 'src/app/system/shared/services/sourtype.service';
import { ArchivefolderfileService } from 'src/app/system/shared/services/archivefolderfile.service';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { DialogSelectFilesComponent } from 'src/app/system/shared/components/dialog-select-files/dialog-select-files.component';
import { SourfiletService } from 'src/app/system/shared/services/sourfilet.service';
import {
  LightboxImage,
  LightboxComponent,
} from 'src/app/system/shared/components/lightbox/lightbox.component';
import { PersonalsourtService } from 'src/app/system/shared/services/personalsourt.service';
import { FiltersService } from 'src/app/system/shared/services/filters.service';
import { DialogJoinPersonalComponent } from 'src/app/system/shared/components/dialog-join-personal/dialog-join-personal.component';
import { ArchivefolderService } from 'src/app/system/shared/services/archivefolder.service';
import { DateparseService } from 'src/app/shared/services/dateparse.service';
import { DialogPdfViewerComponent } from 'src/app/system/shared/components/dialog-pdf-viewer/dialog-pdf-viewer.component';
import { SourtlinkService } from 'src/app/system/shared/services/sour_t_link.service';
import { DialogSetDocumentParentComponent } from 'src/app/system/shared/components/dialog-set-document-parent/dialog-set-document-parent.component';

@Component({
  selector: 'app-sourt-edit',
  templateUrl: './sourt-edit.component.html',
  styleUrls: ['./sourt-edit.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class SourtEditComponent implements OnInit, OnDestroy {

  constructor(
    public dateparseService: DateparseService,
    private authService: AuthService,
    private personaltService: PersonaltService,
    private listoftService: ListoftService,
    private sourtService: SourtService,
    private personalsourtService: PersonalsourtService,
    private archivefolderService: ArchivefolderService,
    private sourfiletService: SourfiletService,
    private sourtypeService: SourtypeService,
    private archivefolderfileService: ArchivefolderfileService,
    private filtersService: FiltersService,
    private activatedRoute: ActivatedRoute,
    private sourtlinkService: SourtlinkService,
    private router: Router,
    private spinnerService: NgxSpinnerService,
    public globalService: GlobalService,
    public dialog: MatDialog,
    public snackBar: MatSnackBar
  ) {
    this.activeRouterObserver = this.activatedRoute.params.subscribe(
      (param) => {
        let str = param.id.split('_');
        if (str[0] === 'new') {
          this.intType = 'newdoc';
          this.selectedPersonalcode = +str[1];
          this.viewOnly = false;
        } else {
          if (str[1] === 'v') {
            this.id = +str[3];
            this.selectedPersonalcode = +str[2];
            this.intType = 'editdoc';
            this.viewOnly = true;
          } else {
            this.id = +str[2];
            this.selectedPersonalcode = +str[1];
            this.intType = 'editdoc';
            this.viewOnly = false;
          }
        }
        this.viewOnly =
          this.viewOnly === false
            ? this.authService.getCurUser().usertype === 4
            : true;
        if (this.inited === true) {
          this.ngOnInit();
        }
      }
    );
    this.globalService.routerObserver = router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        if (val.url.indexOf('sourtedit') !== -1) {
          this.globalService.breadcrumbs.length = 0;
          this.globalService.breadcrumbs.push(
            {
              label: 'персоналии',
              url:
                this.viewOnly === true
                  ? `/sys/person/personaltedit/v_${this.selectedPersonalcode}`
                  : `/sys/person/personaltedit/${this.selectedPersonalcode}`,
            },
            { label: 'источник' }
          );
          this.breadcrumbs = this.globalService.breadcrumbs;
        }
      }
    });
  }
  sourT_width = sourT_width;

  timerId;
  id: number;
  link_id: number;
  intType: string | any = ''; // тип интерфейса
  viewOnly: boolean;
  selectedPersonalcode: number;
  selectedPersonal: PersonalT;

  sourtForm: FormGroup;

  sour_marks = [];

  breadcrumbs = [];

  sour_types: ListOfT[];
  sour_type: any[];
  sour_typeInForm: any[];
  folder_files_to_add = [];
  folder_files = [];
  folder_files_img = [];
  // myControl_sour_type: FormControl;
  // filteredSour_type: Observable<ListOfT[]>;
  activeRouterObserver: Subscription;

  inProgress = false;
  inProgressVal = 0;
  loaded = 0;

  nextId: number;
  previousId: number;
  first = false;
  last = false;
  selectedSourts: SourT[] = [];
  selectedSourt: SourT;
  inited = false;

  parentSourts: SourT[] = [];
  parentSourt: SourT;

  /**Галерея */
  imgBufsize = 3; // /2 буфер изображений в галерее
  lightboxImages: LightboxImage[] = [];
  @ViewChild('lightbox', { static: true }) lightbox: LightboxComponent;
  galleryWait = false;
  delo;
  deloTimer;

  links = [];

  link_types = [
    {
      key: 1,
      value: 'Дубликат',
    },
  ];

  sourtlinks = [];
  @HostListener('window:keydown', ['$event'])
  onKeyPress(event: KeyboardEvent) {
    if ((event.ctrlKey || event.metaKey) && event.keyCode == 83) {
      event.preventDefault();
      if (
        !this.viewOnly &&
        !this.sourtForm.invalid &&
        this.intType !== 'myprofile'
      ) {
        this.onSaveForm();
      }
    }
  }

  ngOnDestroy(): void {
    this.globalService.routerObserver.unsubscribe();
    // this.activeRouterObserver.unsubscribe();
  }

  async ngOnInit() {
    // this.myControl_sour_type = new FormControl({
    //   value: null,
    //   disabled: this.viewOnly,
    // });
    this.sourtForm = new FormGroup({
      parent_id: new FormControl({ value: null, disabled: false }, []),
      id: new FormControl({ value: null, disabled: this.viewOnly }, []),
      personal_code: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      sour_no: new FormControl({ value: null, disabled: true }, [
        Validators.required,
      ]),
      sour_name: new FormControl({ value: null, disabled: this.viewOnly }, []),
      sour_code: new FormControl({ value: null, disabled: this.viewOnly }, []),
      mark_mem: new FormControl({ value: true, disabled: this.viewOnly }, []),
      place_so: new FormControl({ value: null, disabled: this.viewOnly }, []),
      sour_rmk: new FormControl({ value: null, disabled: this.viewOnly }, []),

      category: new FormControl({ value: null, disabled: this.viewOnly }, []),
      fund: new FormControl({ value: null, disabled: this.viewOnly }, [
        Validators.required,
      ]),
      list_no: new FormControl({ value: null, disabled: this.viewOnly }, [
        Validators.required,
      ]),
      file_no: new FormControl({ value: null, disabled: this.viewOnly }, [
        Validators.required,
      ]),
      file_no_ext: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      sour_type: new FormControl({ value: null, disabled: this.viewOnly }, []),
      sour_mark: new FormControl({ value: 'К', disabled: this.viewOnly }, []),
      sour_date: new FormControl({ value: null, disabled: this.viewOnly }, []),
      sour_date_begin: new FormControl({ value: null, disabled: this.viewOnly  }, []),
      sour_date_end: new FormControl({ value: null, disabled: this.viewOnly  }, []),
      sour_date_begin_end: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      pub_type: new FormControl({ value: 4, disabled: this.viewOnly }, []),
      reprod_type_id: new FormControl({ value: null, disabled: this.viewOnly }, []),
    });
    this.sour_marks = this.globalService.sour_mark;
    // try {
    // let sour_types = this.listoftService.getWithFilter('SOUR_TYPE');
    // this.sour_types = isNullOrUndefined(await sour_types)
    //   ? []
    //   : await sour_types;
    await this.getData();
    this.lightboxImages = [];
    // this.filteredSour_type = this.myControl_sour_type.valueChanges.pipe(
    //   startWith(''),
    //   map((sour_type) =>
    //     sour_type ? this._filter(sour_type) : [...this.sour_types]
    //   )
    // );
    // } catch (err) {
    //   if (err.status === 401) {
    //     console.error(`Ошибка авторизации`);
    //     this.authService.logout();
    //   } else {
    //     console.error(`Ошибка ${err}`);
    //   }
    // } finally {
    // }
    this.inited = true;
  }

  async getData() {
    this.spinnerService.show();
    // this.sour_type.length = 0;
    // this.sour_typeInForm.length = 0;
    this.links.length = 0;
    this.sourtlinks.length = 0;
    this.parentSourts = null;
    this.folder_files_to_add.length = 0;
    this.folder_files.length = 0;
    this.folder_files_img.length = 0;
    this.lightboxImages.length = 0;
    let selectedPersonal;
    try {
      selectedPersonal = this.personaltService.getBycode(
        this.selectedPersonalcode
      );
      let sour_types = this.listoftService.getWithFilter('SOUR_TYPE');
      this.sour_types = isNullOrUndefined(await sour_types)
        ? []
        : await sour_types;
      this.sour_types.sort((a, b) => {
        if (a.name_ent < b.name_ent) {
          return -1;
        }
        if (a.name_ent > b.name_ent) {
          return 1;
        }
        return 0;
      });
    } catch (err) {
      if (err.status === 401) {
        console.error(`Ошибка авторизации`);
        this.authService.logout();
      } else {
        console.error(`Ошибка ${err}`);
      }
    }
    this.selectedPersonal = (await selectedPersonal)[0];
    switch (this.intType) {
      case 'editdoc': // редактирование
        try {
          let sourtlinks = this.sourtlinkService.getAllWithNamesById(this.id);
          let _link = this.personalsourtService.getByPersonalcodeSourId(
            this.selectedPersonalcode,
            this.id
          );
          this.selectedSourt = await this.sourtService.getOneById(
            this.id,
            true
          );
          // let selectedSourts = this.sourtService.getByPersonalcode(
          //   this.selectedPersonalcode
          // );
          let selectedSourts = this.personalsourtService.getByPersonalcode(
            this.selectedPersonalcode
          );
          let _sour_type = this.sourtypeService.getBySourid(this.id);
          this.selectedSourts = isNullOrUndefined(await selectedSourts)
            ? []
            : await selectedSourts;
          this.sour_type = isNullOrUndefined(await _sour_type)
            ? []
            : await _sour_type;
          this.sour_typeInForm = [];

          this.sour_type.forEach((sour_type) => {
            let element = this.sour_types.find(
              (item) => item.id === sour_type.sour_type_id
            );

            if (!isNullOrUndefined(element)) {
              this.sour_typeInForm.push(element);
            }
          });
          this.sour_typeInForm.sort((a, b) => {
            if (a.name_ent < b.name_ent) {
              return -1;
            }
            if (a.last_nom > b.last_nom) {
              return 1;
            }
            return 0;
          });
          this.sourtForm.reset();
          this.sourtForm.patchValue({
            parent_id: this.selectedSourt.parent_id,
            id: this.selectedSourt.id,
            personal_code: this.selectedSourt.personal_code,
            sour_no: this.selectedSourt.sour_no,
            sour_name: this.selectedSourt.sour_name,
            sour_code: this.selectedSourt.sour_code,
            // 'mark_mem': selectedSourt.mark_mem,
            mark_mem: this.selectedSourt.mark_mem === '*' ? true : false,
            place_so: this.selectedSourt.place_so,
            sour_rmk: this.selectedSourt.sour_rmk,
            category: this.selectedSourt.category,
            fund: this.selectedSourt.fund,
            list_no: this.selectedSourt.list_no,
            file_no: this.selectedSourt.file_no,
            file_no_ext: this.selectedSourt.file_no_ext,
            sour_type: this.sour_typeInForm,
            // sour_type: selectedSourt.sour_type,
            sour_mark: this.selectedSourt.sour_mark,
            sour_date: this.selectedSourt.sour_date,
            sour_date_begin: this.selectedSourt.sour_date_begin,
            sour_date_end: this.selectedSourt.sour_date_end,
            pub_type: this.selectedSourt.pub_type || 4,
          });
          this.dateparseService.setSourDate(
            this.sourtForm,
            'sour_date_begin_end',
            this.selectedSourt.sour_date_begin,
            this.selectedSourt.sour_date_end
          );
          // this.myControl_sour_type.reset();
          // this.myControl_sour_type.setValue(selectedSourt.sour_type);
          this.folder_files = isNullOrUndefined(this.selectedSourt.folder_file)
            ? []
            : this.selectedSourt.folder_file;
          this.folder_files.forEach((element) => {
            element.checkToDel = false;
          });
          this.folder_files.sort((a, b) =>
            a.archivefile.filename > b.archivefile.filename ? 1 : -1
          );
          this.folder_files_img = this.folder_files.filter(
            (selectedArchivefolderfile) =>
              this.globalService.isImgtype(
                selectedArchivefolderfile.archivefile.filename
              ) ||
              this.globalService.isVideotype(
                selectedArchivefolderfile.archivefile.filename
              ) ||
              this.globalService.isAudiotype(
                selectedArchivefolderfile.archivefile.filename
              )
          );
          this.folder_files_img.sort((a, b) =>
            a.archivefile.filename > b.archivefile.filename ? 1 : -1
          );
          /**Определяем id следущего и предыдущего источников */
          // this.selectedSourts.sort((a, b) => (a.id < b.id ? 1 : -1));
          // this.selectedSourts.sort((a, b) => (a.sour_no > b.sour_no ? 1 : -1));
          let idx = this.selectedSourts.findIndex((s) => s.sour_id === this.id);

          this.first = idx === 0;

          this.last = idx === this.selectedSourts.length - 1;

          if (idx > 0) {
            this.previousId = this.selectedSourts[idx - 1].sour_id;
          } else {
            this.previousId = null;
          }
          if (idx < this.selectedSourts.length - 1) {
            this.nextId = this.selectedSourts[idx + 1].sour_id;
          } else {
            this.nextId = null;
          }
          this.link_id = (await _link)[0].id;
          this.sourtlinks = (await sourtlinks) || [];
          this.sourtlinks.map((sourtlink) => {
            sourtlink.checkToDel = false;
          });
          this.parentSourt = this.selectedSourt.parent_data;
          if (this.parentSourt) {
            this.parentSourt.id = this.selectedSourt.parent_id;
          }
        } catch (err) {
          if (err.status === 401) {
            console.error(`Ошибка авторизации`);
            this.authService.logout();
          } else {
            console.error(`Ошибка ${err}`);
          }
        } finally {
          this.spinnerService.hide();
        }
        break;
      case 'newdoc': // создание
        this.selectedSourt = null;
        try {
          let _sours = this.personalsourtService.getByPersonalcode(
            this.selectedPersonalcode
          );

          let sours = isNullOrUndefined(await _sours) ? [] : await _sours;
          let num: number;
          if (sours.length === 0) {
            num = 1;
          } else {
            sours.sort((a, b) => b.sour_no - a.sour_no);
            num = sours[0].sour_no + 1;
          }
          this.sourtForm.reset();
          this.sourtForm.patchValue({
            mark_mem: true,
            fund: this.filtersService.sourtPlusOne.fund,
            list_no: this.filtersService.sourtPlusOne.list_no,
            file_no: this.filtersService.sourtPlusOne.file_no,
            sour_no: this.filtersService.sourtPlusOne.sour_no
              ? this.filtersService.sourtPlusOne.sour_no + 1
              : num,
          });
          // let selectedPersonal = await this.personaltService.getBycode(this.selectedPersonalcode);
          if (!isNullOrUndefined(this.selectedPersonal)) {
            this.sourtForm.patchValue({
              fund: this.selectedPersonal.fund,
              list_no: this.selectedPersonal.list_no,
              file_no: this.selectedPersonal.file_no,
              file_no_ext: this.selectedPersonal.file_no_ext,
              sour_mark: 'К',
            });
          }
        } catch (err) {
          if (err.status === 401) {
            console.error(`Ошибка авторизации`);
            this.authService.logout();
          } else {
            console.error(`Ошибка ${err}`);
          }
        } finally {
          this.spinnerService.hide();
        }
        break;
      default:
    }
    this.spinnerService.hide();
  }

  async onDel() {
    let dialogRef = this.dialog.open(DialogYesNoComponent, {
      data: {
        title: 'Отсоединить источник?',
        question: ``,
      },
    });
    dialogRef.afterClosed().subscribe(async (result) => {
      if (result === true) {
        this.spinnerService.show();
        try {
          await this.personalsourtService.deleteOneById(this.link_id);
          // await this.sourtService.deleteOneById(this.id);
          this.router.navigate([
            `/sys/person/personaltedit/${this.selectedPersonalcode}`,
          ]);
          this.openSnackBar(`Отсоединено.`, 'Ok');
        } catch (err) {
          console.error(`Ошибка при отсоединении ${err}`);
          this.openSnackBar(`Ошибка ${err.status}`, 'Ok');
        } finally {
          this.spinnerService.hide();
        }
      }
    });
  }

  onDelComplete() {
    let dialogRef = this.dialog.open(DialogYesNoComponent, {
      data: {
        title: 'Удалить источник?',
        question: ``,
      },
    });
    dialogRef.afterClosed().subscribe(async (result) => {
      if (result === true) {
        this.spinnerService.show();
        try {
          // await this.personalsourtService.deleteOneById(this.link_id);
          await this.sourtService.deleteOneById(this.id);
          this.router.navigate([
            `/sys/person/personaltedit/${this.selectedPersonalcode}`,
          ]);
          this.openSnackBar(`Удалено.`, 'Ok');
        } catch (err) {
          console.error(`Ошибка при удалении ${err}`);
          this.openSnackBar(`Ошибка ${err.status}`, 'Ok');
        } finally {
          this.spinnerService.hide();
        }
      }
    });
  }

  /**
   * Функция возвращает true, если бли внесены изменения
   */
  hasChanges(): boolean {
    let res = false;
    if (this.sourtForm.dirty) {
      res = true;
    }
    return res;
  }

  /**
   * Функция сохраняет форму. Silent для показа сообщения. Возвращает true в случаеуспешного сохранения
   */
  async onSaveForm(silent = false, addnew = false) {
    let res = false;
    this.spinnerService.show();
    try {
      // if (!isNullOrUndefined(this.myControl_sour_type.value)) {
      //   this.sourtForm.patchValue({
      //     sour_type: this.myControl_sour_type.value,
      //   });
      // }
      if (
        !isNullOrUndefined(this.sourtForm.getRawValue()['sour_date']) &&
        this.sourtForm.getRawValue()['sour_date'] !== ''
      ) {
        // if (!(new Date(this.sourtForm.getRawValue()['sour_date']) instanceof Date &&
        //   !isNaN(new Date(this.sourtForm.getRawValue()['sour_date']).valueOf()))) {
        //   this.openSnackBar(`Введена несуществующая дата источника`, 'Ok');
        //   return false;
        // }
        // let help_dat = new Date(this.usagetForm.value['help_dat']);
        // this.usagetForm.patchValue({
        // 	'help_dat': help_dat.toDateString()
        // });
      } else {
        this.sourtForm.patchValue({
          sour_date: null,
        });
      }
      if (
        this.sourtForm.getRawValue()['mark_mem'] === true ||
        this.sourtForm.getRawValue()['mark_mem'] === '*'
      ) {
        this.sourtForm.patchValue({
          mark_mem: '*',
        });
      } else {
        this.sourtForm.patchValue({
          mark_mem: null,
        });
      }
      switch (this.intType) {
        case 'editdoc': // редактирование документа
          let elementsToDel = [];
          if (!isNullOrUndefined(this.sour_type)) {
            this.sour_type.forEach((sour_type) => {
              let elementInForm = this.sourtForm
                .getRawValue()
                .sour_type.find((item) => item.id === sour_type.sour_type_id);
              if (isNullOrUndefined(elementInForm)) {
                let res = this.sourtypeService.deleteOneById(sour_type.id);
                elementsToDel.push(res);
              }
            });
          }
          let elementsToPost = [];
          if (!isNullOrUndefined(this.sourtForm.getRawValue().sour_type)) {
            this.sourtForm
              .getRawValue()
              .sour_type.forEach((sour_typeInForm) => {
                let sour_type = this.sour_type.find(
                  (item) => item.sour_type_id === sour_typeInForm.id
                );
                if (isNullOrUndefined(sour_type)) {
                  let obj = {
                    sour_id: this.id,
                    sour_type_id: sour_typeInForm.id,
                  };
                  let res = this.sourtypeService.postOne(obj);
                  elementsToPost.push(res);
                }
              });
          }
          let filesToDel = [];
          if (!isNullOrUndefined(this.folder_files)) {
            this.folder_files.forEach((file_to_del) => {
              if (file_to_del.checkToDel === true) {
                let res = this.sourfiletService.deleteOneById(
                  file_to_del.link_id
                );
                filesToDel.push(res);
              }
            });
          }
          let filesToPost = [];
          if (!isNullOrUndefined(this.folder_files_to_add)) {
            this.folder_files_to_add.forEach((file_to_add) => {
              let obj = {
                sour_id: this.id,
                archivefolderfile_id: file_to_add.id,
              };
              let res = this.sourfiletService.postOne(obj);
              filesToPost.push(res);
            });
          }
          let deletesourtlinks = Promise.all(
            this.sourtlinks.map(async (sourtlink) => {
              if (sourtlink.checkToDel) {
                if (sourtlink.link_type === 1) {
                  return this.sourtlinkService.deleteOneById(sourtlink.id, {
                    id_fordelete:
                      +sourtlink.sour_t_id1 === +this.id
                        ? sourtlink.sour_t_id2
                        : sourtlink.sour_t_id1,
                  });
                }
              }
            })
          );
          let addsourtlinks = Promise.all(
            this.links.map(async (link) => {
              await this.sourtlinkService.postOne({
                sour_t_id1: this.id,
                sour_t_id2: link.val.id,
                link_type: link.link_type,
              });
            })
          );
          let deletechildrens = Promise.all(
            this.selectedSourt.children.map(async (child) => {
              if (child.checkToDel) {
                return this.sourtService.putOneById(child.id, {
                  parent_id: null,
                });
              }
            })
          );
          await deletechildrens;
          await deletesourtlinks;
          await addsourtlinks;
          await Promise.all(elementsToDel);
          await Promise.all(elementsToPost);
          await Promise.all(filesToPost);
          await Promise.all(filesToDel);
          this.folder_files_to_add.length = 0;
          let putobj = this.sourtForm.getRawValue();
          // delete putobj.sour_date;
          await this.sourtService.putOneById(this.id, putobj);
          if (addnew === true) {
            this.filtersService.sourtPlusOne.newnom = `${
              this.sourtForm.getRawValue().sour_no
            }`;
          }
          break;
        case 'newdoc': // создание документа
          this.sourtForm.patchValue({
            personal_code: this.selectedPersonalcode,
          });
          this.sourtForm.removeControl('id');
          let obj = this.sourtForm.value;
          if (this.filtersService.sourtPlusOne.newnom) {
            obj['newnom'] = +this.filtersService.sourtPlusOne.newnom + 1;
          }
          // delete obj.sour_date;
          let postForm = await this.sourtService.postOne(obj);
          this.id = +postForm.id;
          this.intType = 'editdoc';
          if (addnew === true) {
            this.filtersService.sourtPlusOne.newnom = postForm.sour_no;
          }
          let elementsToAdd = [];
          if (!isNullOrUndefined(this.sourtForm.getRawValue().sour_type)) {
            this.sourtForm
              .getRawValue()
              .sour_type.forEach((sour_typeInForm) => {
                let obj = {
                  sour_id: this.id,
                  sour_type_id: sour_typeInForm.id,
                };
                let res = this.sourtypeService.postOne(obj);
                elementsToAdd.push(res);
              });
          }
          let filesToAdd = [];
          if (!isNullOrUndefined(this.folder_files_to_add)) {
            this.folder_files_to_add.forEach((file_to_add) => {
              let obj = {
                sour_id: this.id,
                archivefolderfile_id: file_to_add.id,
              };
              let res = this.sourfiletService.postOne(obj);
              filesToPost.push(res);
            });
          }
          let _addsourtlinks = Promise.all(
            this.links.map(async (link) => {
              let addsourtlink = await this.sourtlinkService.postOne({
                sour_t_id1: this.id,
                sour_t_id2: link.val.id,
                link_type: link.link_type,
              });
            })
          );
          await _addsourtlinks;
          await Promise.all(elementsToAdd);
          await Promise.all(filesToAdd);
          this.router.navigate([
            `sys/person/sourtedit/edit_${this.selectedPersonalcode}_${this.id}`,
          ]);
          break;
          break;
        default:
      }
      if (addnew === true) {
        this.filtersService.sourtPlusOne.fund = this.sourtForm.value.fund;
        this.filtersService.sourtPlusOne.list_no = this.sourtForm.value.list_no;
        this.filtersService.sourtPlusOne.file_no = this.sourtForm.value.file_no;
        this.filtersService.sourtPlusOne.sour_no = this.sourtForm.value.sour_no;
        this.router.navigate([
          `sys/person/sourtedit/new_${this.selectedPersonalcode}`,
        ]);
        this.intType = 'newdoc';
      } else {
        this.filtersService.sourtPlusOne.fund = null;
        this.filtersService.sourtPlusOne.list_no = null;
        this.filtersService.sourtPlusOne.file_no = null;
        this.filtersService.sourtPlusOne.sour_no = null;
        this.filtersService.sourtPlusOne.newnom = null;
      }
      await this.getData();
      if (!silent) {
        this.openSnackBar(`Сохранено.`, 'Ok');
      }
      res = true;
    } catch (err) {
      if (err.status === 401) {
        console.error(`Ошибка авторизации`);
        this.authService.logout();
      } else if (err.status === 409) {
        this.sourtForm.controls['sour_no'].setErrors({ incorrect: true });
        this.openSnackBar(`${err.error['error']}`, 'Ok');
      } else {
        console.error(`Ошибка ${err}`);
        this.openSnackBar(`Ошибка ${err.status}`, 'Ok');
      }
    } finally {
      this.spinnerService.hide();
      return res;
    }
  }

  async goBack() {
    let str = '/sys/person/personaltedit/';
    str += this.viewOnly ? 'v_' : '';
    str += `${this.selectedPersonalcode}`;
    let saved = true;
    if (this.hasChanges() && this.sourtForm.valid) {
      let dialogRef = this.dialog.open(DialogSaveNoComponent, {
        data: {
          title: 'Сохранить изменения?',
          question: ``,
        },
      });
      dialogRef.afterClosed().subscribe(async (result) => {
        if (result === true) {
          saved = await this.onSaveForm(true);
          if (saved === true) {
            this.router.navigate([str]);
            // this.router.navigate([`/sys/person/personaltedit/${this.selectedPersonalcode}`]);
          }
        } else if (result === false) {
          this.router.navigate([str]);
          // this.router.navigate([`/sys/person/personaltedit/${this.selectedPersonalcode}`]);
        } else {
          return;
        }
      });
    } else if (this.hasChanges() && this.sourtForm.valid) {
      let dialogRef = this.dialog.open(DialogYesNoComponent, {
        data: {
          title: 'Изменения не будут сохранены!',
          question: `Не заполнены обязательные поля. Уйти со страницы?`,
        },
      });
      dialogRef.afterClosed().subscribe(async (result) => {
        if (result === true) {
          this.router.navigate([str]);
          // this.router.navigate([`/sys/person/personaltedit/${this.selectedPersonalcode}`]);
        } else {
          return;
        }
      });
    } else {
      this.router.navigate([str]);
      // this.router.navigate([`/sys/person/personaltedit/${this.selectedPersonalcode}`]);
    }
  }

  resetSour_date() {
    this.sourtForm.patchValue({
      sour_date: null,
    });
  }

  // private _filter(value: string) {
  //   return this.sour_types;
  // }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
    });
  }

  toggleCheckToDel(idx) {
    this.folder_files[idx].checkToDel = !this.folder_files[idx].checkToDel;
  }

  async onDownloadArchivefolderfile(id) {
    this.spinnerService.show();
    let fileName = '';
    for (let i = 0; i < this.folder_files.length; i++) {
      if (!isNullOrUndefined(this.folder_files[i])) {
        if (id === this.folder_files[i].file_id) {
          fileName = this.folder_files[i].archivefile.filename;
        }
      }
    }
    try {
      let blob = this.archivefolderfileService.downloadOneById(id);
      this.inProgress = true;
      this.inProgressVal = 0;
      blob.subscribe((event) => {
        if (event.type === HttpEventType.DownloadProgress) {
          const percentDone = Math.round((100 * event.loaded) / event.total);
          this.inProgressVal = percentDone;
        } else if (event instanceof HttpResponse) {
          this.inProgress = false;
          this.inProgressVal = 0;
          this.spinnerService.hide();
          importedSaveAs(event.body, fileName);
        }
      });
    } catch (err) {
      console.error(`Ошибка ${err}`);
      this.spinnerService.hide();
      this.inProgress = false;
      this.inProgressVal = 0;
      this.globalService.hasError.next(false);
    }
  }

  onAddFiles() {
    let dialogRef = this.dialog.open(DialogSelectFilesComponent, {
      panelClass: 'app-dialog-extend',
      position: { top: '100px' },
      data: {
        fund: this.sourtForm.value.fund,
        list_no: this.sourtForm.value.list_no,
        file_no: this.sourtForm.value.file_no,
        folder_files: this.folder_files,
        selectedFiles: [],
      },
    });
    dialogRef.afterClosed().subscribe(async (result) => {
      if (result && result !== false) {
        this.folder_files_to_add = [...result.selectedFiles];
      }
    });
  }

  onDelFileById(idx) {
    this.folder_files_to_add.splice(idx, 1);
  }
  onDelFiles() {
    this.folder_files_to_add.length = 0;
  }

  openPdf(selectedArchivefolderfile) {
    this.dialog.open(DialogPdfViewerComponent, {
      panelClass: 'app-dialog-extend',
      data: {
        file: selectedArchivefolderfile,
      },
    });
  }

  async onShowImg(id, selectedArchivefolderfile) {
    if (
      this.globalService.isPdftype(
        selectedArchivefolderfile.archivefile.filename
      )
    ) {
      this.openPdf(selectedArchivefolderfile);
      return;
    }
    let archivefolderfiles: any[];
    let parchivefolderfiles: any[];
    let narchivefolderfiles: any[];
    this.lightboxImages.length = 0;
    let idx = this.folder_files_img.findIndex((item) => item.file_id === id);
    parchivefolderfiles = this.folder_files_img.slice(
      idx,
      this.imgBufsize + idx > this.folder_files_img.length
        ? this.folder_files_img.length
        : this.imgBufsize + idx
    );
    narchivefolderfiles = this.folder_files_img.slice(
      idx - this.imgBufsize > 0 ? idx - this.imgBufsize : 0,
      idx
    );
    archivefolderfiles = [...narchivefolderfiles, ...parchivefolderfiles];
    archivefolderfiles.sort((a, b) =>
      a.archivefile.filename > b.archivefile.filename ? 1 : -1
    );
    let fname: string;
    this.folder_files_img.map((item) => {
      if (item.file_id === id) {
        fname = item.archivefile.filename;
      }
    });
    this.spinnerService.show();
    this.inProgress = true;
    this.inProgressVal += 100 / archivefolderfiles.length;
    await Promise.all(
      archivefolderfiles.map((item) => {
        return new Promise(async (resolve) => {
          let resfile;
          if (
            this.globalService.isImgtype(item.archivefile.filename) === true ||
            this.globalService.isVideotype(item.archivefile.filename) ===
              true ||
            this.globalService.isAudiotype(item.archivefile.filename) === true
          ) {
            try {
              let file;
              let type;
              let ext;
              if (
                this.globalService.isImgtype(item.archivefile.filename) === true
              ) {
                type = 'image';
                file = await this.archivefolderfileService.getFilePreviewById(
                  item.file_id
                );
                resfile = new Blob([file], { type: 'image/jpg' });
                let reader = new FileReader();
                reader.readAsDataURL(resfile);
                reader.onloadend = () => {
                  let base64data = String(reader.result);
                  this.lightboxImages.push({
                    src: base64data,
                    caption: item.archivefile.filename,
                    id: item.file_id,
                    type,
                    ext,
                    orientation: item.orientation,
                  });
                  resolve(null);
                };
              } else if (
                this.globalService.isVideotype(item.archivefile.filename) ===
                true
              ) {
                type = 'video';
                ext = item.archivefile.filename
                  .toLowerCase()
                  .split('.')
                  .reverse()[0];
                this.lightboxImages.push({
                  src: this.archivefolderfileService.generateLink(
                    item.file_id,
                    item.jwt,
                    item.archivefile.storage
                  ),
                  caption: item.archivefile.filename,
                  id: item.file_id,
                  type,
                  ext,
                  orientation: item.orientation,
                });
                resolve(null);
              } else {
                type = 'audio';
                ext = item.archivefile.filename
                  .toLowerCase()
                  .split('.')
                  .reverse()[0];
                this.lightboxImages.push({
                  src: this.archivefolderfileService.generateLink(
                    item.file_id,
                    item.jwt,
                    item.archivefile.storage
                  ),
                  caption: item.archivefile.filename,
                  id: item.file_id,
                  type,
                  ext,
                  orientation: item.orientation,
                });
                resolve(null);
              }
              this.inProgressVal += 100 / archivefolderfiles.length;
            } catch (err) {
              resolve(null);
            }
          } else {
            resolve(null);
          }
        });
      })
    );
    this.spinnerService.hide();
    this.lightboxImages.sort((a, b) => (a.caption > b.caption ? 1 : -1));
    this.inProgress = false;
    setTimeout(() => {
      let idx = this.lightboxImages.findIndex((item) => item.caption === fname);
      this.lightbox.open(idx);
    }, 300);
  }

  async addPrevImg() {
    let firstId = this.lightboxImages[0].id;
    let idx = this.folder_files_img.findIndex(
      (item) => item.file_id === +firstId
    );
    if (idx === 0 || idx === -1) {
      return false;
    } else {
      this.spinnerService.show();
      this.galleryWait = true;
      let file;
      let type;
      let resfile;
      try {
        if (
          this.globalService.isImgtype(
            this.folder_files_img[idx - 1].archivefile.filename
          ) === true
        ) {
          type = 'image';
          file = await this.archivefolderfileService.getFilePreviewById(
            this.folder_files_img[idx - 1].file_id
          );
          resfile = new Blob([file], { type: 'image/jpg' });
          let reader = new FileReader();
          reader.readAsDataURL(resfile);
          reader.onloadend = () => {
            let base64data = String(reader.result);
            this.lightbox.unshiftImg({
              src: base64data,
              caption: this.folder_files_img[idx - 1].archivefile.filename,
              id: this.folder_files_img[idx - 1].file_id,
              type,
              orientation: this.folder_files_img[idx - 1].orientation,
            });
          };
        } else if (
          this.globalService.isVideotype(
            this.folder_files_img[idx - 1].archivefile.filename
          ) === true
        ) {
          type = 'video';
          let ext = this.folder_files_img[idx - 1].archivefile.filename
            .toLowerCase()
            .split('.')
            .reverse()[0];
          this.lightbox.unshiftImg({
            src: this.archivefolderfileService.generateLink(
              this.folder_files_img[idx - 1].file_id,
              this.folder_files_img[idx - 1].jwt,
              this.folder_files_img[idx - 1].archivefile.storage
            ),
            caption: this.folder_files_img[idx - 1].archivefile.filename,
            id: this.folder_files_img[idx - 1].file_id,
            type,
            ext,
            orientation: this.folder_files_img[idx - 1].orientation,
          });
        } else {
          type = 'audio';
          let ext = this.folder_files_img[idx - 1].archivefile.filename
            .toLowerCase()
            .split('.')
            .reverse()[0];
          this.lightbox.unshiftImg({
            src: this.archivefolderfileService.generateLink(
              this.folder_files_img[idx - 1].file_id,
              this.folder_files_img[idx - 1].jwt,
              this.folder_files_img[idx - 1].archivefile.storage
            ),
            caption: this.folder_files_img[idx - 1].archivefile.filename,
            id: this.folder_files_img[idx - 1].file_id,
            type,
            ext,
            orientation: this.folder_files_img[idx - 1].orientation,
          });
        }
      } catch {
        this.openSnackBar(
          `Ошибка при обработке изображения ${
            this.folder_files_img[idx - 1].archivefile.filename
          }`,
          'Ok'
        );
      } finally {
        this.spinnerService.hide();
        this.galleryWait = false;
      }
      return true;
    }
  }

  async addNextImg() {
    let lastId = +this.lightboxImages[this.lightboxImages.length - 1].id;
    let idx = this.folder_files_img.findIndex(
      (item) => item.file_id === +lastId
    );
    if (idx + 1 > this.folder_files_img.length - 1 || idx === -1) {
      return false;
    } else {
      this.spinnerService.show();
      this.galleryWait = true;
      let file;
      let type;
      let resfile;
      try {
        if (
          this.globalService.isImgtype(
            this.folder_files_img[idx + 1].archivefile.filename
          ) === true
        ) {
          type = 'image';
          file = await this.archivefolderfileService.getFilePreviewById(
            this.folder_files_img[idx + 1].file_id
          );
          resfile = new Blob([file], { type: 'image/jpg' });
          let reader = new FileReader();
          reader.readAsDataURL(resfile);
          reader.onloadend = () => {
            let base64data = String(reader.result);
            this.lightbox.pushImg({
              src: base64data,
              caption: this.folder_files_img[idx + 1].archivefile.filename,
              id: this.folder_files_img[idx + 1].file_id,
              type,
              orientation: this.folder_files_img[idx + 1].orientation,
            });
          };
        } else if (
          this.globalService.isVideotype(
            this.folder_files_img[idx + 1].archivefile.filename
          ) === true
        ) {
          type = 'video';
          let ext = this.folder_files_img[idx + 1].archivefile.filename
            .toLowerCase()
            .split('.')
            .reverse()[0];
          this.lightbox.pushImg({
            src: this.archivefolderfileService.generateLink(
              this.folder_files_img[idx + 1].file_id,
              this.folder_files_img[idx + 1].jwt,
              this.folder_files_img[idx + 1].archivefile.storage
            ),
            caption: this.folder_files_img[idx + 1].archivefile.filename,
            id: this.folder_files_img[idx + 1].file_id,
            type,
            ext,
            orientation: this.folder_files_img[idx + 1].orientation,
          });
        } else {
          type = 'audio';
          let ext = this.folder_files_img[idx + 1].archivefile.filename
            .toLowerCase()
            .split('.')
            .reverse()[0];
          this.lightbox.pushImg({
            src: this.archivefolderfileService.generateLink(
              this.folder_files_img[idx + 1].file_id,
              this.folder_files_img[idx + 1].jwt,
              this.folder_files_img[idx + 1].archivefile.storage
            ),
            caption: this.folder_files_img[idx + 1].archivefile.filename,
            id: this.folder_files_img[idx + 1].file_id,
            type,
            ext,
            orientation: this.folder_files_img[idx + 1].orientation,
          });
        }
      } catch (err) {
        console.error(err);
        this.openSnackBar(
          `Ошибка при обработке изображения ${
            this.folder_files_img[idx + 1].archivefile.filename
          }`,
          'Ok'
        );
      } finally {
        this.spinnerService.hide();
        this.galleryWait = false;
      }
      return true;
    }
  }

  async previewChange(e) {
    if (e.forward === false) {
      await this.addPrevImg();
    } else {
      await this.addNextImg();
    }
  }

  async onMove(str) {
    this.goToPage(str);
  }

  async goToPage(str) {
    if (str === 'next') {
      if (!isNullOrUndefined(this.nextId) && !isNullOrUndefined(this.nextId)) {
        // this.router.navigate([`/sys/arch/edit/${this.nextId}`]);
        // console.log(`/sys/arch/edit/${this.nextId}`);
        this.editSourt(this.nextId);
        this.id = this.nextId;
      }
    } else if (str === 'previous') {
      if (!isNullOrUndefined(this.previousId)) {
        // this.router.navigate([`/sys/arch/edit/${this.previousId}`]);
        // console.log(`/sys/arch/edit/${this.previousId}`);
        this.editSourt(this.previousId);
        this.id = this.previousId;
      }
    }
  }
  async editSourt(id) {
    let str = '/sys/person/sourtedit/edit_';
    str += this.viewOnly ? 'v_' : '';
    str += `${this.selectedPersonalcode}_${id}`;
    this.router.navigate([str]);
  }

  linkPersonal(id) {
    this.router.navigate(['sys/person/personaltedit', id]);
  }

  // joinPersonal() {
  //   let dialogRef = this.dialog.open(DialogJoinPersonalComponent, {
  //     data: {
  //       fund: this.sourtForm.value.fund,
  //       list_no: this.sourtForm.value.list_no,
  //       file_no: this.sourtForm.value.file_no,
  //     },
  //     panelClass: 'app-dialog-extend',
  //   });
  //   dialogRef.afterClosed().subscribe(async (result) => {
  //     if (result) {
  //       let d;
  //       if (this.selectedSourt.personals) {
  //         d = this.selectedSourt.personals.find(
  //           (item) => item.code == result.code
  //         );
  //       } else {
  //         this.selectedSourt.personals = [];
  //       }
  //       if (d) {
  //         this.globalService.openSnackBar('Персоналия уже присоединена', 'Ok');
  //       } else {
  //         try {
  //           await this.personalsourtService.postOne({
  //             sour_id: this.id,
  //             personal_code: result.code,
  //           });
  //           this.selectedSourt.personals.unshift(result);
  //         } catch (e) {}
  //       }
  //     }
  //   });
  // }
  joinPersonal() {
    let dialogRef = this.dialog.open(DialogJoinPersonalComponent, {
      data: {
        fund: this.sourtForm.value.fund,
        list_no: this.sourtForm.value.list_no,
        file_no: this.sourtForm.value.file_no,
      },
      panelClass: 'app-dialog-extend',
    });
    dialogRef.afterClosed().subscribe(async (result) => {
      if (result) {
        let toAdd = [];
        result.forEach((p) => {
          let d;
          if (this.selectedSourt.personals) {
            d = this.selectedSourt.personals.find(
              (item) => item.code == p.code
            );
          }
          if (!d) {
            toAdd.push(p);
          }
        });
        try {
          await Promise.all(
            toAdd.map((item) =>
              this.personalsourtService.postOne({
                sour_id: this.id,
                personal_code: item.code,
              })
            )
          );
          toAdd.forEach((element) => {
            this.selectedSourt.personals.unshift(element);
          });
          this.selectedSourt.personals.sort((a, b) => a.code - b.code);
        } catch (e) {}
      }
    });
  }

  async checkDelo() {
    if (this.sourtForm.value.file_no > 99999) {
      this.sourtForm.patchValue({
        file_no: +`${this.sourtForm.value.file_no}`.substr(0, 5),
      });
    }
    if (this.sourtForm.value.list_no > 9999) {
      this.sourtForm.patchValue({
        list_no: +`${this.sourtForm.value.list_no}`.substr(0, 4),
      });
    }
    if (this.sourtForm.value.fund > 9999) {
      this.sourtForm.patchValue({
        fund: +`${this.sourtForm.value.fund}`.substr(0, 4),
      });
    }
    clearTimeout(this.deloTimer);
    if (this.isFondopisdeloValid()) {
      this.deloTimer = setTimeout(async () => {
        try {
          let doc = this.archivefolderService.getWithOpis_data(
            this.sourtForm.value.file_no,
            this.sourtForm.value.fund,
            this.sourtForm.value.list_no
          );
          this.delo = !(await doc) ? null : (await doc)[0];
        } catch (err) {
          this.checkDelo = null;
        } finally {
        }
      }, 400);
    } else {
      this.delo = null;
    }
  }

  isFondopisdeloValid(): boolean {
    let res = true;
    if (
      isNaN(parseFloat(this.sourtForm.value.fund)) ||
      isNaN(parseFloat(this.sourtForm.value.list_no)) ||
      isNaN(parseFloat(this.sourtForm.value.file_no))
    ) {
      res = false;
    }
    return res;
  }

  linkDelo() {
    this.router.navigate(['sys/arch/edit', this.delo.id]);
  }

  copyFilename(file) {
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = file.archivefile.file;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
  }

  toggleCheckToDelLink(id, sourtlink) {
    if (sourtlink.link_type === 1 && sourtlink.checkToDel !== true) {
      let dialogRef = this.dialog.open(DialogYesNoComponent, {
        data: {
          title:
            'Разрывая данную связь вы разрываете все связи типа "дубликат" у целевого документа. Продолжить?',
          question: ``,
        },
      });
      dialogRef.afterClosed().subscribe(async (result) => {
        if (result === true) {
          this.sourtlinks[id].checkToDel = !this.sourtlinks[id].checkToDel;
        }
      });
    } else {
      this.sourtlinks[id].checkToDel = !this.sourtlinks[id].checkToDel;
    }
  }

  async onAddsourtlink(result) {
    let id = result.id;
    let l = this.sourtlinks.find(
      (link) => link.sour_t_id2 === id || link.sour_t_id1 === id
    );
    if (!l) {
      this.links.push({ val: result, link_type: 1 }); // link_type
    } else {
      this.openSnackBar(`Связь с делом уже установлена`, 'Ok');
    }
  }

  setDubl() {
    let dialogRef = this.dialog.open(DialogSetDocumentParentComponent, {
      panelClass: 'app-dialog-extend',
      data: {
        selectedSourt: {},
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.onAddsourtlink(result);
      }
    });
  }

  removeParents() {
    this.sourtForm.patchValue({ parent_id: null });
    this.parentSourt = null;
  }

  setParents() {
    let dialogRef = this.dialog.open(DialogSetDocumentParentComponent, {
      panelClass: 'app-dialog-extend',
      data: {
        selectedSourt: this.parentSourt || {},
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.sourtForm.patchValue({ parent_id: result.id });
        this.parentSourt = result;
      }
    });
  }

  linkDocument(id) {
    this.router.navigate(['sys/person/doc', id]);
  }

  onLinkSourt(id) {
    this.router.navigate(['/sys/person/doc', id]);
  }
  unlinkPersonal(link_id, e: MouseEvent) {
    e.stopPropagation();
    let dialogRef = this.dialog.open(DialogYesNoComponent, {
      data: {
        title: 'Отсоединить источник?',
        question: ``,
      },
    });
    dialogRef.afterClosed().subscribe(async (result) => {
      if (result === true) {
        this.spinnerService.show();
        try {
          await this.personalsourtService.deleteOneById(link_id);
          this.openSnackBar(`Отсоединено.`, 'Ok');
          await this.getData();
        } catch (e) {
        } finally {
          this.spinnerService.hide();
        }
      }
    });
  }
}
