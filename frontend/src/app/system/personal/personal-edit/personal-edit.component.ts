import {
  Component,
  OnInit,
  ViewChild,
  HostListener,
  OnDestroy,
} from '@angular/core';
import {
  PersonalT,
  personalT_width,
} from 'src/app/shared/models/personalt.model';
import { Muser } from 'src/app/shared/models/muser.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/shared/services/auth.service';
import { MuserService } from '../../shared/services/muser.service';
import { NattService } from '../../shared/services/natt.service';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DialogYesNoComponent } from '../../shared/components/dialog-yes-no/dialog-yes-no.component';
import { PersonaltService } from '../../shared/services/personalt.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { PlaceT } from 'src/app/shared/models/placet.model';
import { Observable, Subscription } from 'rxjs';
import { PlacetService } from '../../shared/services/placet.service';
import { isNullOrUndefined } from 'util';
import { startWith, map } from 'rxjs/operators';
import { NatT } from 'src/app/shared/models/natt.model';
import { CoorgtService } from '../../shared/services/coorgt.service';
import { OrgtService } from '../../shared/services/orgt.service';
import { ListOfT } from 'src/app/shared/models/listoft.model';
import { ListoftService } from '../../shared/services/listoft.service';
import { OrgT } from 'src/app/shared/models/orgt.model';
import { ActivityT } from 'src/app/shared/models/activityt.model';
import { ActivitytService } from '../../shared/services/activityt.service';
import { CoacttService } from '../../shared/services/coactt.service';
import { RepressT } from 'src/app/shared/models/represst.model';
import { InformtService } from '../../shared/services/informt.service';
import { FamilytService } from '../../shared/services/familyt.service';
import { HelpT } from 'src/app/shared/models/helpt.model';
import { SourT, SourtFilter } from 'src/app/shared/models/sourt.model';
import { UsageT } from 'src/app/shared/models/usaget.model';
import { DialogSaveNoComponent } from '../../shared/components/dialog-save-no/dialog-save-no.component';
import { ArchivefolderService } from '../../shared/services/archivefolder.service';
import { DialogPersonalEditComponent } from './dialog-personal-edit/dialog-personal-edit.component';
import { Location } from '@angular/common';
import { OpisService } from '../../shared/services/opis.service';
import { Opis } from 'src/app/shared/models/opis.model';
import { DialogMarchiveEditComponent } from './dialog-marchive-edit/dialog-marchive-edit.component';
import { FiltersService } from '../../shared/services/filters.service';
import { familyT_width } from 'src/app/shared/models/familyt.model';
import { informT_width } from 'src/app/shared/models/informt.model';
import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem,
} from '@angular/cdk/drag-drop';
import { MoveordernomService } from '../../shared/services/moveordernom.service';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { saveAs as importedSaveAs } from 'file-saver';
import { SelectionitemService } from '../../shared/services/selectionitem.service';
import { MentionedtService } from '../../shared/services/mentionedt.service';
import { PersonalshifrtService } from '../../shared/services/personalshifrt.service';
import { DialogJoinSourtComponent } from '../../shared/components/dialog-join-sourt/dialog-join-sourt.component';
import { PersonalsourtService } from '../../shared/services/personalsourt.service';
import { DateparseService } from 'src/app/shared/services/dateparse.service';
import { PublicSiteActionsService } from '../../shared/services/publicsiteactions.service';
import { PersonalDbLinkService } from '../../shared/services/personaldbLinkt.service';

@Component({
  selector: 'app-personal-edit',
  templateUrl: './personal-edit.component.html',
  styleUrls: ['./personal-edit.component.css'],
})
export class PersonalEditComponent implements OnInit, OnDestroy {
  familyT_width = familyT_width;
  informT_width = informT_width;
  personalT_width = personalT_width;

  id: number | any;
  intType: string | any = ''; // тип интерфейса
  viewOnly: boolean; // тип интерфейса
  loggedInUser: Muser;
  personaltForm: FormGroup;

  informtForms: FormGroup[] = [];
  informtidTodel: number[] = [];
  informt_human_code: FormControl[] = [];

  familytForms: FormGroup[] = [];
  familytidTodel: number[] = [];
  familyt_human_code: FormControl[] = [];

  mentionedtForms: FormGroup[] = [];
  mentionedtidTodel: number[] = [];
  mentionedt_human_code: FormControl[] = [];

  texttForms: FormGroup[] = [];
  texttidTodel: number[] = [];

  filteredPersonalts: Observable<PersonalT[]>;

  selectedPersonalt: PersonalT;
  selectedPersonaltOldData: PersonalT;
  selectedOrgts: OrgT[] = [];
  selectedActivityts: ActivityT[] = [];
  selectedRepressts: RepressT[] = [];
  selectedHelpts: HelpT[] = [];
  selectedSourts: SourT[] = [];
  // selectedUsagets: UsageT[] = [];
  selectedExtrashifrs = [];
  selectedDbLinks = [];
  extrashifrsIdTodel = [];
  dbLinksIdTodel = [];
  extrashifrForms: FormGroup[] = [];
  dbLinkForms: FormGroup[] = [];

  yesno: any[];
  yesnoq: any[];
  sex: any[];
  actionTypes: any[];
  tableTypes: any[];
  origins: ListOfT[];
  educs: ListOfT[];
  specs: ListOfT[];
  repress_types: ListOfT[];
  us_types: ListOfT[];
  sour_types: ListOfT[];

  myControl_birth_place: FormControl;
  myControl_death_place: FormControl;
  myControl_nation_id: FormControl;
  myControl_residence_code: FormControl;
  timerId;
  placets: PlaceT[];
  personalts: PersonalT[];
  filteredPlacets: Observable<PlaceT[]>;
  nations: NatT[];
  filteredNations: Observable<NatT[]>;

  citizes: string[] = [];
  myControl_citizes: FormControl;
  filteredCitizes: Observable<string[]>;

  phoneMask: any[] = [
    '+',
    /[1-9]/,
    ' ',
    '(',
    /[1-9]/,
    /\d/,
    /\d/,
    ')',
    ' ',
    /\d/,
    /\d/,
    /\d/,
    '-',
    /\d/,
    /\d/,
    /\d/,
    /\d/,
    /\d/,
  ];

  scroll = 0;

  mentioned: PersonalT[]; // упоминаемые

  delo: any; // дело, связанное с персоналией
  delos: any[] = []; // дело, связанное с персоналией
  checkDelo = true; // статус проверки существования дела
  deloTimer; // таймер проверки дела при инпуте дела, описи, фонда
  opisItems: Opis[];

  statusTypes: any[];
  curDate: string;

  inited = false;
  activeRouterObserver: Subscription;

  isPinned = false;

  @HostListener('window:scroll', ['$event'])
  getScroll(event) {
    this.scroll = window.pageYOffset;
  }

  @HostListener('window:keydown', ['$event'])
  onKeyPress(event: KeyboardEvent) {
    if ((event.ctrlKey || event.metaKey) && event.keyCode == 83) {
      event.preventDefault();
      if (!this.personaltForm.invalid && this.intType !== 'myprofile') {
        this.onSaveForm();
      }
    }
  }

  constructor(
    public dateparseService: DateparseService,
    public authService: AuthService,
    private muserService: MuserService,
    public globalService: GlobalService,
    private personaltService: PersonaltService,
    private publicSiteActions: PublicSiteActionsService,
    private archivefolderService: ArchivefolderService,
    private listoftService: ListoftService,
    private nattService: NattService,
    private opisService: OpisService,
    private placetService: PlacetService,
    private orgtService: OrgtService,
    private coorgtService: CoorgtService,
    private selectionitemService: SelectionitemService,
    private personalsourtService: PersonalsourtService,
    private activitytService: ActivitytService,
    private coacttService: CoacttService,
    private informtService: InformtService,
    private familytService: FamilytService,
    private mentionedtService: MentionedtService,
    private moveordernomService: MoveordernomService,
    private personalshifrtService: PersonalshifrtService,
    private personaldbLinkService: PersonalDbLinkService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private spinnerService: NgxSpinnerService,
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    private location: Location,
    public filtersService: FiltersService
  ) {
    // this.activeRouterObserver = this.activatedRoute.params.subscribe(param => {
    //   this.id = param.id;
    //   if (param.id === 'newdoc') {
    //     this.intType = 'newdoc';
    //     this.viewOnly = false;
    //   } else {
    //     let str = param.id.split('_');
    //     if (str[0] === 'v') {
    //       this.id = +str[1];
    //       this.intType = 'editdoc';
    //       this.viewOnly = true;
    //     } else {
    //       this.id = +param.id;
    //       this.intType = 'editdoc';
    //       this.viewOnly = false;
    //     }
    //   }
    // });
    this.id = this.activatedRoute.snapshot.params['id'];
    if (this.activatedRoute.snapshot.params['id'] === 'newdoc') {
      this.intType = 'newdoc';
      this.viewOnly = false;
    } else {
      let str = this.activatedRoute.snapshot.params['id'].split('_');
      if (str[0] === 'v') {
        this.id = +str[1];
        this.intType = 'editdoc';
        this.viewOnly = true;
      } else {
        this.id = +this.activatedRoute.snapshot.params['id'];
        this.intType = 'editdoc';
        this.viewOnly = false;
      }
    }
    this.viewOnly =
      this.viewOnly === false
        ? this.authService.getCurUser().usertype === 4
        : true;

    this.globalService.routerObserver = router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        if (val.url.indexOf('personaltedit') !== -1) {
          this.globalService.breadcrumbs.length = 0;
          if (this.inited === true) {
            this.ngOnInit();
          }
        }
      }
    });
    this.filtersService.pinPersonal.subscribe((code) => {
      this.isPinned = this.id == code;
    });
  }

  ngOnDestroy(): void {
    this.globalService.routerObserver.unsubscribe();
    this.activeRouterObserver.unsubscribe();
  }

  async ngOnInit() {
    this.filtersService.sourtPlusOne.fund = null;
    this.filtersService.sourtPlusOne.list_no = null;
    this.filtersService.sourtPlusOne.file_no = null;
    this.filtersService.sourtPlusOne.sour_no = null;
    this.inited = true;
    this.informt_human_code.length = 0;
    this.familyt_human_code.length = 0;
    this.mentionedt_human_code.length = 0;
    this.informtForms.length = 0;
    this.familytForms.length = 0;
    this.mentionedtForms.length = 0;
    this.extrashifrForms.length = 0;
    this.dbLinkForms.length = 0;
    this.delos.length = 0;
    let obj = {
      scroll: this.scroll,
      selectedCourtt: false,
    };
    this.globalService.represst_edit = obj;
    let obj1 = {
      scroll: this.scroll,
      selectedImprist: false,
    };
    this.globalService.courtt_edit = obj1;
    // this.globalService.backUrl = '/';
    this.citizes = this.globalService.citizes;
    this.statusTypes = this.globalService.statusTypes;
    this.myControl_citizes = new FormControl({
      value: null,
      disabled: this.viewOnly,
    });
    this.filteredCitizes = this.myControl_citizes.valueChanges.pipe(
      startWith(''),
      map((name) => (name ? this.citizes : [...this.citizes]))
    );
    this.curDate = new Date().toISOString().split('T')[0];
    this.personaltForm = new FormGroup({
      mark: new FormControl({ value: null, disabled: this.viewOnly }, []),
      type_r: new FormControl({ value: null, disabled: this.viewOnly }, []),
      published: new FormControl({ value: null, disabled: this.viewOnly }, []),
      mustPublish: new FormControl(null),
      mustUnpublish: new FormControl(null),
      pub_change_date: new FormControl(null),
      type_i: new FormControl({ value: null, disabled: this.viewOnly }, []),
      type_a: new FormControl({ value: null, disabled: this.viewOnly }, []),
      surname: new FormControl({ value: null, disabled: this.viewOnly }, []),
      surname_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      public_rmk: new FormControl({ value: null, disabled: this.viewOnly }, []),
      general_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      fname: new FormControl({ value: null, disabled: this.viewOnly }, []),
      fname_rmk: new FormControl({ value: null, disabled: this.viewOnly }, []),
      lname: new FormControl({ value: null, disabled: this.viewOnly }, []),
      lname_rmk: new FormControl({ value: null, disabled: this.viewOnly }, []),
      sex: new FormControl({ value: null, disabled: this.viewOnly }, []),
      // birth: new FormControl({ value: null, disabled: true }, []),
      birth_begin: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      birth_end: new FormControl({ value: null, disabled: this.viewOnly }, []),
      birth_begin_end: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      birth_place: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      nation_id: new FormControl({ value: null, disabled: this.viewOnly }, []),
      citiz: new FormControl({ value: null, disabled: this.viewOnly }, []),
      origin: new FormControl({ value: null, disabled: this.viewOnly }, []),
      educ: new FormControl({ value: null, disabled: this.viewOnly }, []),
      spec: new FormControl({ value: null, disabled: this.viewOnly }, []),
      origin_id: new FormControl({ value: null, disabled: this.viewOnly }, []),
      educ_id: new FormControl({ value: null, disabled: this.viewOnly }, []),
      spec_id: new FormControl({ value: null, disabled: this.viewOnly }, []),
      death_mark: new FormControl({ value: '?', disabled: this.viewOnly }, []),
      // death: new FormControl({ value: null, disabled: true }, []),
      death_begin: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      death_end: new FormControl({ value: null, disabled: this.viewOnly }, []),
      death_begin_end: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      birth_place_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      death_place_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      birth_rmk: new FormControl({ value: null, disabled: this.viewOnly }, []),
      nation_rmk: new FormControl({ value: null, disabled: this.viewOnly }, []),
      citiz_rmk: new FormControl({ value: null, disabled: this.viewOnly }, []),
      origin_rmk: new FormControl({ value: null, disabled: this.viewOnly }, []),
      educ_rmk: new FormControl({ value: null, disabled: this.viewOnly }, []),
      spec_rmk: new FormControl({ value: null, disabled: this.viewOnly }, []),
      death_date_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      residence_code: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      address: new FormControl({ value: null, disabled: this.viewOnly }, []),
      phone: new FormControl({ value: null, disabled: this.viewOnly }, []),
      email: new FormControl({ value: null, disabled: this.viewOnly }, []),
      residence_code_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      address_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      phone_rmk: new FormControl({ value: null, disabled: this.viewOnly }, []),
      notice: new FormControl({ value: null, disabled: this.viewOnly }, []),
      in_date: new FormControl({ value: null, disabled: this.viewOnly }, []),
      place_num: new FormControl({ value: null, disabled: this.viewOnly }, []),
      birth_place_num: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      death_place_num: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      death_type: new FormControl({ value: null, disabled: this.viewOnly }, []),
      fund: new FormControl({ value: null, disabled: this.viewOnly }, []),
      list_no: new FormControl({ value: null, disabled: this.viewOnly }, []),
      file_no: new FormControl({ value: null, disabled: this.viewOnly }, []),
      file_no_ext: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      real_surname_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      url: new FormControl({ value: null, disabled: this.viewOnly }, []),
      url2: new FormControl({ value: null, disabled: this.viewOnly }, []),
      url3: new FormControl({ value: null, disabled: this.viewOnly }, []),
      mentioned_person: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
    });
    this.myControl_birth_place = new FormControl({
      value: null,
      disabled: this.viewOnly,
    });
    this.myControl_death_place = new FormControl({
      value: null,
      disabled: this.viewOnly,
    });
    this.myControl_nation_id = new FormControl({
      value: null,
      disabled: this.viewOnly,
    });
    this.myControl_residence_code = new FormControl({
      value: null,
      disabled: this.viewOnly,
    });
    this.yesno = this.globalService.yesno;
    this.yesnoq = this.globalService.yesnoq;
    this.sex = this.globalService.sex;
    this.actionTypes = this.globalService.actionTypes;
    this.tableTypes = this.globalService.tableTypes;
    try {
      let loggedInUser = this.muserService.getOneById(
        this.authService.getCurUser().id
      );
      this.loggedInUser = await loggedInUser;
      let nations = this.nattService.getAll();
      let origins = this.listoftService.getWithFilter('ORIGIN');
      let educs = this.listoftService.getWithFilter('EDUC');
      let specs = this.listoftService.getWithFilter('SPEC');
      let repress_types = this.listoftService.getWithFilter('REPRESS_TYPE');
      let us_types = this.listoftService.getWithFilter('US_TYPE');
      let opisItems = this.opisService.getAll();
      // let org_codes = this.listoftService.getWithFilter('ORG_CODE');
      // let org_names = this.listoftService.getWithFilter('ORG_NAME');
      // let spheres = this.listoftService.getWithFilter('SPHERE');
      this.origins = isNullOrUndefined(await origins) ? [] : await origins;
      this.educs = isNullOrUndefined(await educs) ? [] : await educs;
      this.specs = isNullOrUndefined(await specs) ? [] : await specs;
      // this.org_codes = (isNullOrUndefined(await org_codes)) ? [] : await org_codes;
      // this.org_names = (isNullOrUndefined(await org_names)) ? [] : await org_names;
      // this.spheres = (isNullOrUndefined(await spheres)) ? [] : await spheres;
      this.nations = isNullOrUndefined(await nations) ? [] : await nations;
      this.repress_types = isNullOrUndefined(await repress_types)
        ? []
        : await repress_types;
      this.us_types = isNullOrUndefined(await us_types) ? [] : await us_types;
      this.opisItems = isNullOrUndefined(await opisItems)
        ? []
        : await opisItems;
      this.opisItems.sort((a, b) => a.fond - b.fond);
      this.nations.sort((a, b) => (a.nation_rmk < b.nation_rmk ? -1 : 1));
      this.specs.sort((a, b) => (a.name_ent < b.name_ent ? -1 : 1));
      this.educs.sort((a, b) => (a.name_ent < b.name_ent ? -1 : 1));
      this.us_types.sort((a, b) => (a.name_ent < b.name_ent ? -1 : 1));
      this.origins.sort((a, b) => (a.name_ent < b.name_ent ? -1 : 1));
      if (isNullOrUndefined(this.activeRouterObserver)) {
        this.activeRouterObserver = this.activatedRoute.params.subscribe(
          async (param) => {
            this.id = param.id;
            if (param.id === 'newdoc') {
              this.intType = 'newdoc';
              this.viewOnly = false;
              await this.getData();
            } else {
              let str = param.id.split('_');
              if (str[0] === 'v') {
                this.id = +str[1];
                this.intType = 'editdoc';
                this.viewOnly = true;
              } else {
                this.id = +param.id;
                this.intType = 'editdoc';
                this.viewOnly = false;
              }
              await this.getData();
            }
            this.viewOnly =
              this.viewOnly === false
                ? this.authService.getCurUser().usertype === 4
                : true;
          }
        );
      }
    } catch (err) {
      this.globalService.exceptionHandling(err);
    } finally {
      this.filteredNations = this.myControl_nation_id.valueChanges.pipe(
        startWith(NatT),
        map((value) => {
          if (isNullOrUndefined(value)) {
            return null;
          }
          return typeof value === 'string' ? value : value.name;
        }),
        map((name) => (name ? this._filterNatt(name) : [...this.nations]))
      );
    }
  }

  async getData() {
    this.spinnerService.show();
    this.informtForms.length = 0;
    this.familytForms.length = 0;
    this.mentionedtForms.length = 0;
    this.extrashifrForms.length = 0;
    this.dbLinkForms.length = 0;
    this.delos.length = 0;
    if (this.authService.getCurUser().usertype === 4) {
      this.viewOnly = true;
    }
    try {
      let sour_types = this.listoftService.getWithFilter('SOUR_TYPE');
      this.sour_types = isNullOrUndefined(await sour_types)
        ? []
        : await sour_types;
      switch (this.intType) {
        case 'editdoc': // редактирование
          // this.texttForms.length = 0;
          let selectedPersonalt = this.personaltService.getOneById(
            this.id,
            true,
            true
          );
          this.selectedPersonalt = await selectedPersonalt;
          this.selectedPersonaltOldData = this.selectedPersonalt;

          this.personaltForm.reset();
          this.personaltForm.patchValue({
            mark: this.selectedPersonalt.mark === '*' ? true : false,
            type_i: this.selectedPersonalt.type_i === '*' ? true : false,
            type_a: this.selectedPersonalt.type_a === '*' ? true : false,
            type_r: this.selectedPersonalt.type_r === '*' ? true : false,
            published: this.selectedPersonalt.published === '*' ? true : false,
            mustPublish: this.selectedPersonalt.mustPublish,
            mustUnpublish: this.selectedPersonalt.mustUnpublish,
            pub_change_date: this.selectedPersonalt.pub_change_date,
            surname: this.selectedPersonalt.surname,
            surname_rmk: this.selectedPersonalt.surname_rmk,
            public_rmk: this.selectedPersonalt.public_rmk,
            general_rmk: this.selectedPersonalt.general_rmk,
            fname: this.selectedPersonalt.fname,
            fname_rmk: this.selectedPersonalt.fname_rmk,
            lname: this.selectedPersonalt.lname,
            lname_rmk: this.selectedPersonalt.lname_rmk,
            sex: this.selectedPersonalt.sex,
            // birth: this.globalService.dateFromDateString(
            //   this.selectedPersonalt.birth
            // ),
            birth_begin: this.globalService.dateFromDateString(
              this.selectedPersonalt.birth_begin
            ),
            birth_end: this.globalService.dateFromDateString(
              this.selectedPersonalt.birth_end
            ),
            birth_place: this.selectedPersonalt.birth_place,
            birth_rmk: this.selectedPersonalt.birth_rmk,
            nation_id: this.selectedPersonalt.nation_id,
            citiz: this.selectedPersonalt.citiz,
            origin: this.selectedPersonalt.origin,
            educ: this.selectedPersonalt.educ,
            spec: this.selectedPersonalt.spec,
            origin_id: this.selectedPersonalt.origin_id,
            educ_id: this.selectedPersonalt.educ_id,
            spec_id: this.selectedPersonalt.spec_id,
            death_mark: this.selectedPersonalt.death_mark,
            // death: this.globalService.dateFromDateString(
            //   this.selectedPersonalt.death
            // ),
            death_begin: this.globalService.dateFromDateString(
              this.selectedPersonalt.death_begin
            ),
            death_end: this.globalService.dateFromDateString(
              this.selectedPersonalt.death_end
            ),
            birth_place_rmk: this.selectedPersonalt.birth_place_rmk,
            nation_rmk: this.selectedPersonalt.nation_rmk,
            citiz_rmk: this.selectedPersonalt.citiz_rmk,
            origin_rmk: this.selectedPersonalt.origin_rmk,
            educ_rmk: this.selectedPersonalt.educ_rmk,
            spec_rmk: this.selectedPersonalt.spec_rmk,
            death_date_rmk: this.selectedPersonalt.death_date_rmk,
            residence_code: this.selectedPersonalt.residence_code,
            address: this.selectedPersonalt.address,
            phone: this.selectedPersonalt.phone,
            email: this.selectedPersonalt.email,
            residence_code_rmk: this.selectedPersonalt.residence_code_rmk,
            address_rmk: this.selectedPersonalt.address_rmk,
            phone_rmk: this.selectedPersonalt.phone_rmk,
            notice: this.selectedPersonalt.notice,
            in_date: this.selectedPersonalt.in_date,
            place_num: this.selectedPersonalt.place_num,
            birth_place_num: this.selectedPersonalt.birth_place_num,
            death_place_num: this.selectedPersonalt.death_place_num,
            death_place_rmk: this.selectedPersonalt.death_place_rmk,
            death_type: this.selectedPersonalt.death_type,
            fund: this.selectedPersonalt.fund,
            list_no: this.selectedPersonalt.list_no,
            file_no: this.selectedPersonalt.file_no,
            file_no_ext: this.selectedPersonalt.file_no_ext,
            real_surname_rmk: this.selectedPersonalt.real_surname_rmk,
            url: this.selectedPersonalt.url,
            url2: this.selectedPersonalt.url2,
            url3: this.selectedPersonalt.url3,
            mentioned_person: !isNullOrUndefined(
              this.selectedPersonalt['ment_p'].code
            )
              ? this.selectedPersonalt['ment_p']
              : null,
          });
          this.dateparseService.setSourDate(
            this.personaltForm,
            'birth_begin_end',
            this.selectedPersonalt.birth_begin,
            this.selectedPersonalt.birth_end
          );
          this.dateparseService.setSourDate(
            this.personaltForm,
            'death_begin_end',
            this.selectedPersonalt.death_begin,
            this.selectedPersonalt.death_end
          );
          let doc;
          if (this.isFondopisdeloValid()) {
            doc = this.archivefolderService.getWithOpis_data(
              this.personaltForm.value.file_no,
              this.personaltForm.value.fund,
              this.personaltForm.value.list_no
            );
          }
          let _birth_place;
          if (!isNullOrUndefined(this.selectedPersonalt.birth_place_num)) {
            _birth_place = this.placetService.getByPlace_num(
              this.selectedPersonalt.birth_place_num
            );
          }
          let _death_place;
          if (!isNullOrUndefined(this.selectedPersonalt.death_place_num)) {
            _death_place = this.placetService.getByPlace_num(
              this.selectedPersonalt.death_place_num
            );
          }
          let _residence_code;
          if (!isNullOrUndefined(this.selectedPersonalt.place_num)) {
            _residence_code = this.placetService.getByPlace_num(
              this.selectedPersonalt.place_num
            );
          }
          if (!isNullOrUndefined(this.selectedPersonalt.birth_place_num)) {
            this.myControl_birth_place.reset();
            if (
              !isNullOrUndefined(await _birth_place) &&
              (await _birth_place).length > 0
            ) {
              this.myControl_birth_place.setValue((await _birth_place)[0]);
            }
          } else {
            this.myControl_birth_place.setValue(null);
            this.myControl_birth_place.reset();
          }
          if (!isNullOrUndefined(this.selectedPersonalt.death_place_num)) {
            this.myControl_death_place.reset();
            if (
              !isNullOrUndefined(await _death_place) &&
              (await _death_place).length > 0
            ) {
              this.myControl_death_place.setValue((await _death_place)[0]);
            }
          } else {
            this.myControl_death_place.setValue(null);
            this.myControl_death_place.reset();
          }
          if (!isNullOrUndefined(this.selectedPersonalt.place_num)) {
            this.myControl_residence_code.reset();
            if (
              !isNullOrUndefined(await _residence_code) &&
              (await _residence_code).length > 0
            ) {
              this.myControl_residence_code.setValue(
                (await _residence_code)[0]
              );
            }
          }
          // let _selectedFamilyts = this.familytService.getByPersonalcode(this.selectedPersonalt.code);
          // let _selectedInformts = this.informtService.getByPersonalcode(this.selectedPersonalt.code);
          // let _selectedTextts = this.texttService.getByPersonalcode(
          //   this.selectedPersonalt.code
          // );
          // let selectedInformts = (isNullOrUndefined(await _selectedInformts)) ? [] : await _selectedInformts;
          // let selectedFamilyts = (isNullOrUndefined(await _selectedFamilyts)) ? [] : await _selectedFamilyts;
          // let selectedTextts = (isNullOrUndefined(await _selectedTextts)) ? [] : await _selectedTextts;
          let selectedInformts = isNullOrUndefined(
            this.selectedPersonalt['inform_t']
          )
            ? []
            : this.selectedPersonalt['inform_t'];
          let selectedFamilyts = isNullOrUndefined(
            this.selectedPersonalt['family_t']
          )
            ? []
            : this.selectedPersonalt['family_t'];
          let selectedMentionedts = isNullOrUndefined(
            this.selectedPersonalt['mentioned_t']
          )
            ? []
            : this.selectedPersonalt['mentioned_t'];
          let selectedTextts = isNullOrUndefined(
            this.selectedPersonalt['text_t']
          )
            ? []
            : this.selectedPersonalt['text_t'];
          for (let j = 0; j < selectedInformts.length; j++) {
            this.informtForms.push(
              new FormGroup({
                id: new FormControl(
                  { value: null, disabled: this.viewOnly },
                  []
                ),
                personal_code: new FormControl(
                  { value: null, disabled: this.viewOnly },
                  []
                ),
                human_code: new FormControl(
                  { value: null, disabled: this.viewOnly },
                  [Validators.required]
                ),
                role: new FormControl(
                  { value: null, disabled: this.viewOnly },
                  []
                ),
                role_rmk: new FormControl(
                  { value: null, disabled: this.viewOnly },
                  []
                ),
                inform_dat: new FormControl(
                  { value: null, disabled: this.viewOnly },
                  []
                ),
                type_r: new FormControl(null),
              })
            );
            this.informt_human_code.push(
              new FormControl({ value: null, disabled: this.viewOnly })
            );
            this.informtForms[j].patchValue({
              id: selectedInformts[j].id,
              personal_code: selectedInformts[j].personal_code,
              human_code: selectedInformts[j].human_code,
              role: selectedInformts[j].role,
              role_rmk: selectedInformts[j].role_rmk,
              inform_dat: selectedInformts[j].inform_dat,
              type_r: selectedInformts[j].type_r,
            });
            if (!isNullOrUndefined(selectedInformts[j].human_code)) {
              // let person = await this.personaltService.getBycode(selectedInformts[j].human_code);
              // if (!isNullOrUndefined(person) && person.length > 0) {
              // this.informt_human_code[j].setValue(person[0]);
              // }
              this.informt_human_code[j].setValue(selectedInformts[j]);
            }
          }
          for (let j = 0; j < selectedFamilyts.length; j++) {
            this.familytForms.push(
              new FormGroup({
                id: new FormControl(
                  { value: null, disabled: this.viewOnly },
                  []
                ),
                personal_code: new FormControl(
                  { value: null, disabled: this.viewOnly },
                  []
                ),
                human_code: new FormControl(
                  { value: null, disabled: this.viewOnly },
                  [Validators.required]
                ),
                role: new FormControl(
                  { value: null, disabled: this.viewOnly },
                  []
                ),
                role_rmk: new FormControl(
                  { value: null, disabled: this.viewOnly },
                  []
                ),
                type_r: new FormControl(null),
              })
            );
            this.familyt_human_code.push(
              new FormControl({ value: null, disabled: this.viewOnly })
            );
            this.familytForms[j].patchValue({
              id: selectedFamilyts[j].id,
              personal_code: selectedFamilyts[j].personal_code,
              human_code: selectedFamilyts[j].human_code,
              role: selectedFamilyts[j].role,
              role_rmk: selectedFamilyts[j].role_rmk,
              type_r: selectedFamilyts[j].type_r,
            });
            if (!isNullOrUndefined(selectedFamilyts[j].human_code)) {
              // let person = await this.personaltService.getBycode(selectedFamilyts[j].human_code);
              this.familyt_human_code[j].setValue(selectedFamilyts[j]);
            }

            if (this.authService.getCurUser().usertype === 4) {
              this.familyt_human_code.forEach((el) => el.disable());
              this.mentionedt_human_code.forEach((el) => el.disable());
              this.familytForms.forEach((el) => el.disable());
              this.mentionedtForms.forEach((el) => el.disable());
              this.informtForms.forEach((el) => el.disable());
              this.informt_human_code.forEach((el) => el.disable());
            }
          }
          for (let j = 0; j < selectedMentionedts.length; j++) {
            this.mentionedtForms.push(
              new FormGroup({
                id: new FormControl(
                  { value: null, disabled: this.viewOnly },
                  []
                ),
                personal_code: new FormControl(
                  { value: null, disabled: this.viewOnly },
                  []
                ),
                human_code: new FormControl(
                  { value: null, disabled: this.viewOnly },
                  [Validators.required]
                ),
                role: new FormControl(
                  { value: null, disabled: this.viewOnly },
                  []
                ),
                role_rmk: new FormControl(
                  { value: null, disabled: this.viewOnly },
                  []
                ),
                type_r: new FormControl(null),
                type_a: new FormControl(null),
              })
            );
            this.mentionedt_human_code.push(
              new FormControl({ value: null, disabled: this.viewOnly })
            );
            this.mentionedtForms[j].patchValue({
              id: selectedMentionedts[j].id,
              personal_code: selectedMentionedts[j].personal_code,
              human_code: selectedMentionedts[j].human_code,
              role: selectedMentionedts[j].role,
              role_rmk: selectedMentionedts[j].role_rmk,
              type_r: selectedMentionedts[j].type_r,
              type_a: selectedMentionedts[j].type_a,
            });
            if (!isNullOrUndefined(selectedMentionedts[j].human_code)) {
              // let person = await this.personaltService.getBycode(selectedFamilyts[j].human_code);
              this.mentionedt_human_code[j].setValue(selectedMentionedts[j]);
            }
          }
          for (let j = 0; j < selectedTextts.length; j++) {
            this.texttForms.push(
              new FormGroup({
                id: new FormControl(
                  { value: null, disabled: this.viewOnly },
                  []
                ),
                personal_code: new FormControl(
                  { value: null, disabled: this.viewOnly },
                  []
                ),
                line_no: new FormControl(
                  { value: null, disabled: this.viewOnly },
                  []
                ),
                text_line: new FormControl(
                  { value: null, disabled: this.viewOnly },
                  []
                ),
              })
            );
            this.texttForms[j].patchValue({
              id: selectedTextts[j].id,
              personal_code: selectedTextts[j].personal_code,
              line_no: selectedTextts[j].line_no,
              text_line: selectedTextts[j].text_line,
            });
          }
          this.myControl_nation_id.reset();
          this.myControl_nation_id.setValue(
            this.nations.find(
              (nation) => nation.id === +this.selectedPersonalt.nation_id
            )
          );
          this.myControl_citizes.reset();
          this.myControl_citizes.setValue(this.selectedPersonalt.citiz);
          // this.selectedActivityts = (isNullOrUndefined(await selectedActivityts)) ? [] : await selectedActivityts;
          this.selectedActivityts = isNullOrUndefined(
            this.selectedPersonalt['activity_t']
          )
            ? []
            : this.selectedPersonalt['activity_t'];
          this.selectedRepressts = isNullOrUndefined(
            this.selectedPersonalt['repress_t']
          )
            ? []
            : this.selectedPersonalt['repress_t'];
          this.selectedHelpts = isNullOrUndefined(
            this.selectedPersonalt['help_t']
          )
            ? []
            : this.selectedPersonalt['help_t'];
          this.selectedSourts = isNullOrUndefined(
            this.selectedPersonalt['sour_t']
          )
            ? []
            : this.selectedPersonalt['sour_t'];
          // this.selectedUsagets = isNullOrUndefined(
          //   this.selectedPersonalt['usage_t']
          // )
          //   ? []
          //   : this.selectedPersonalt['usage_t'];
          this.selectedOrgts = isNullOrUndefined(
            this.selectedPersonalt['org_t']
          )
            ? []
            : this.selectedPersonalt['org_t'];
          this.mentioned = isNullOrUndefined(
            this.selectedPersonalt['mentioned']
          )
            ? []
            : this.selectedPersonalt['mentioned'];
          this.selectedDbLinks =
            await this.personaldbLinkService.getByPersonalcode(
              this.selectedPersonalt.code
            );

          if (this.selectedDbLinks && this.selectedDbLinks.length > 0) {
            for (let j = 0; j < this.selectedDbLinks.length; j++) {
              this.dbLinkForms.push(
                new FormGroup({
                  id: new FormControl(
                    {
                      value: this.selectedDbLinks[j].id,
                      disabled: this.viewOnly,
                    },
                    []
                  ),
                  personal_code: new FormControl(
                    {
                      value: this.selectedDbLinks[j].personal_code,
                      disabled: this.viewOnly,
                    },
                    []
                  ),
                  link_id: new FormControl(
                    {
                      value: this.selectedDbLinks[j].link_id,
                      disabled: this.viewOnly,
                    },
                    []
                  ),
                  link_id_rmk: new FormControl(
                    {
                      value: this.selectedDbLinks[j].link_id_rmk,
                      disabled: this.viewOnly,
                    },
                    []
                  ),
                })
              );
            }
          }
          this.selectedExtrashifrs = this.selectedPersonalt['extra_shifr']
            ? this.selectedPersonalt['extra_shifr']
            : [];
          for (let j = 0; j < this.selectedExtrashifrs.length; j++) {
            this.extrashifrForms.push(
              new FormGroup({
                id: new FormControl(
                  {
                    value: this.selectedExtrashifrs[j].id,
                    disabled: this.viewOnly,
                  },
                  []
                ),
                personal_code: new FormControl(
                  {
                    value: this.selectedExtrashifrs[j].personal_code,
                    disabled: this.viewOnly,
                  },
                  []
                ),
                fund: new FormControl(
                  {
                    value: this.selectedExtrashifrs[j].fund,
                    disabled: this.viewOnly,
                  },
                  []
                ),
                list_no: new FormControl(
                  {
                    value: this.selectedExtrashifrs[j].list_no,
                    disabled: this.viewOnly,
                  },
                  []
                ),
                file_no: new FormControl(
                  {
                    value: this.selectedExtrashifrs[j].file_no,
                    disabled: this.viewOnly,
                  },
                  []
                ),
                file_no_ext: new FormControl(
                  {
                    value: this.selectedExtrashifrs[j].file_no_ext,
                    disabled: this.viewOnly,
                  },
                  []
                ),
              })
            );
            this.delos.push(null);
          }
          this.selectedActivityts.sort((a, b) =>
            a.act_no > b.act_no ? 1 : -1
          );
          this.selectedRepressts.sort((a, b) =>
            a.repress_no > b.repress_no ? 1 : -1
          );
          this.selectedOrgts.sort((a, b) => (a.org_no > b.org_no ? 1 : -1));
          this.selectedHelpts.sort((a, b) => (a.help_no > b.help_no ? 1 : -1));
          // this.selectedSourts.sort((a, b) => (a.sour_no > b.sour_no ? 1 : -1));
          // this.selectedUsagets.sort((a, b) => (a.us_no > b.us_no ? 1 : -1));
          let obj = {
            scroll: this.globalService.personal_edit.scroll,
            selectedOrgt: this.selectedOrgts.length > 0 ? true : false,
            selectedActivityt:
              this.selectedActivityts.length > 0 ? true : false,
            selectedRepresst: this.selectedRepressts.length > 0 ? true : false,
            selectedHelp: this.selectedHelpts.length > 0 ? true : false,
            selectedSourt: this.selectedSourts.length > 0 ? true : false,
            // selectedUsaget: this.selectedUsagets.length > 0 ? true : false,
            selectedMentioned: this.mentioned.length > 0 ? true : false,
          };
          this.globalService.personal_edit = obj;
          if (this.isFondopisdeloValid()) {
            this.delo = isNullOrUndefined(await doc) ? null : (await doc)[0];
            this.checkDelo = false;
          }
          for (let k = 0; k < this.extrashifrForms.length; k++) {
            this.onCheckDelos(k);
          }
          break;
        case 'newdoc': // создание
          this.personaltForm.reset();
          if (
            !isNullOrUndefined(this.filtersService.personalTListFilter.newFond)
          ) {
            this.personaltForm.patchValue({
              fund: this.filtersService.personalTListFilter.newFond,
            });
          }
          if (
            !isNullOrUndefined(this.filtersService.personalTListFilter.newOpis)
          ) {
            this.personaltForm.patchValue({
              list_no: this.filtersService.personalTListFilter.newOpis,
            });
          }
          break;
        default:
      }
      this.setDeathdateEnable();
    } catch (err) {
      this.globalService.exceptionHandling(err);
    } finally {
      this.spinnerService.hide();
      setTimeout(() => {
        window.scrollTo(0, this.globalService.personal_edit.scroll);
      }, 200);
    }
  }

  addInformt() {
    this.informtForms.push(
      new FormGroup({
        personal_code: new FormControl(null, []),
        human_code: new FormControl(null, [Validators.required]),
        role: new FormControl(null, []),
        role_rmk: new FormControl(null, []),
        inform_dat: new FormControl(null, []),
      })
    );
    this.informt_human_code.push(new FormControl(null));
  }

  delInformt(idx) {
    if (!isNullOrUndefined(this.informtForms[idx].value.id)) {
      this.informtidTodel.push(this.informtForms[idx].value.id);
    }
    this.informtForms.splice(idx, 1);
    this.informt_human_code.splice(idx, 1);
  }

  addExtrashifr() {
    this.extrashifrForms.push(
      new FormGroup({
        id: new FormControl(
          {
            value: null,
            disabled: this.viewOnly,
          },
          []
        ),
        personal_code: new FormControl(
          {
            value: null,
            disabled: this.viewOnly,
          },
          []
        ),
        fund: new FormControl(
          {
            value: null,
            disabled: this.viewOnly,
          },
          [Validators.required]
        ),
        list_no: new FormControl(
          {
            value: null,
            disabled: this.viewOnly,
          },
          []
        ),
        file_no: new FormControl(
          {
            value: null,
            disabled: this.viewOnly,
          },
          []
        ),
        file_no_ext: new FormControl(
          {
            value: null,
            disabled: this.viewOnly,
          },
          []
        ),
      })
    );
    this.delos.push(null);
  }
  delExtrashifr(idx) {
    if (this.extrashifrForms[idx].value.id) {
      this.extrashifrsIdTodel.push(this.extrashifrForms[idx].value.id);
    }
    this.extrashifrForms.splice(idx, 1);
    this.delos.splice(idx, 1);
  }
  addFamilyt() {
    this.familytForms.push(
      new FormGroup({
        personal_code: new FormControl(null, []),
        human_code: new FormControl(null, [Validators.required]),
        role: new FormControl(null, []),
        role_rmk: new FormControl(null, []),
      })
    );
    this.familyt_human_code.push(new FormControl(null));
  }

  delFamilyt(idx) {
    if (!isNullOrUndefined(this.familytForms[idx].value.id)) {
      this.familytidTodel.push(this.familytForms[idx].value.id);
    }
    this.familytForms.splice(idx, 1);
    this.familyt_human_code.splice(idx, 1);
  }
  addDBLinkt() {
    this.dbLinkForms.push(
      new FormGroup({
        id: new FormControl(
          {
            value: null,
            disabled: this.viewOnly,
          },
          []
        ),
        personal_code: new FormControl(
          {
            value: null,
            disabled: this.viewOnly,
          },
          []
        ),
        link_id: new FormControl(
          {
            value: null,
            disabled: this.viewOnly,
          },
          []
        ),
        link_id_rmk: new FormControl(
          {
            value: null,
            disabled: this.viewOnly,
          },
          []
        ),
      })
    );
  }

  delDBLinkt(idx) {
    if (this.dbLinkForms[idx].value.id) {
      this.dbLinksIdTodel.push(this.dbLinkForms[idx].value.id);
    }
    this.dbLinkForms.splice(idx, 1);
  }

  addMentionedt() {
    this.mentionedtForms.push(
      new FormGroup({
        personal_code: new FormControl(null, []),
        human_code: new FormControl(null, [Validators.required]),
        role: new FormControl(null, []),
        role_rmk: new FormControl(null, []),
      })
    );
    this.mentionedt_human_code.push(new FormControl(null));
  }

  delMentionedt(idx) {
    if (!isNullOrUndefined(this.mentionedtForms[idx].value.id)) {
      this.mentionedtidTodel.push(this.mentionedtForms[idx].value.id);
    }
    this.mentionedtForms.splice(idx, 1);
    this.mentionedt_human_code.splice(idx, 1);
  }

  addTextt() {
    this.texttForms.push(
      new FormGroup({
        personal_code: new FormControl(null, []),
        line_no: new FormControl(null, []),
        text_line: new FormControl(null, []),
      })
    );
  }

  delTextt(idx) {
    if (!isNullOrUndefined(this.texttForms[idx].value.id)) {
      this.texttidTodel.push(this.texttForms[idx].value.id);
    }
    this.texttForms.splice(idx, 1);
  }

  async addOrgt() {
    if (this.hasChanges() || this.intType === 'newdoc') {
      await this.onSaveForm(true);
    }
    let obj = {
      scroll: this.scroll,
      selectedOrgt: true,
      selectedActivityt: false,
      selectedRepresst: false,
      selectedHelp: false,
      selectedSourt: false,
      selectedUsaget: false,
      selectedMentioned: false,
    };
    this.globalService.personal_edit = obj;
    this.router.navigate([
      `/sys/person/orgtedit/new_${this.selectedPersonalt.code}_0`,
    ]);
  }

  async addActivityt() {
    if (this.hasChanges() || this.intType === 'newdoc') {
      await this.onSaveForm(true);
    }
    let obj = {
      scroll: this.scroll,
      selectedOrgt: false,
      selectedActivityt: true,
      selectedRepresst: false,
      selectedHelp: false,
      selectedSourt: false,
      selectedUsaget: false,
      selectedMentioned: false,
    };
    this.globalService.personal_edit = obj;
    this.spinnerService.hide();
    this.router.navigate([
      `/sys/person/activitytedit/new_${this.selectedPersonalt.code}`,
    ]);
  }

  async addRepresst() {
    if (this.hasChanges() || this.intType === 'newdoc') {
      await this.onSaveForm(true);
    }
    let obj = {
      scroll: this.scroll,
      selectedOrgt: false,
      selectedActivityt: false,
      selectedRepresst: true,
      selectedHelp: false,
      selectedSourt: false,
      selectedUsaget: false,
      selectedMentioned: false,
    };
    this.globalService.personal_edit = obj;
    this.router.navigate([
      `/sys/person/represstedit/new_${this.selectedPersonalt.code}`,
    ]);
  }

  async addHelpt() {
    if (this.hasChanges() || this.intType === 'newdoc') {
      await this.onSaveForm(true);
    }
    let obj = {
      scroll: this.scroll,
      selectedOrgt: false,
      selectedActivityt: false,
      selectedRepresst: false,
      selectedHelp: true,
      selectedSourt: false,
      selectedUsaget: false,
      selectedMentioned: false,
    };
    this.globalService.personal_edit = obj;
    this.router.navigate([
      `/sys/person/helptedit/new_${this.selectedPersonalt.code}`,
    ]);
  }

  async addUsaget() {
    if (this.hasChanges() || this.intType === 'newdoc') {
      await this.onSaveForm(true);
    }
    let obj = {
      scroll: this.scroll,
      selectedOrgt: false,
      selectedActivityt: false,
      selectedRepresst: false,
      selectedHelp: false,
      selectedSourt: false,
      selectedUsaget: true,
      selectedMentioned: false,
    };
    this.globalService.personal_edit = obj;
    this.router.navigate([
      `/sys/person/usagetedit/new_${this.selectedPersonalt.code}`,
    ]);
  }

  async addSourt() {
    if (this.hasChanges() || this.intType === 'newdoc') {
      await this.onSaveForm(true);
    }
    let obj = {
      scroll: this.scroll,
      selectedOrgt: false,
      selectedActivityt: false,
      selectedRepresst: false,
      selectedHelp: false,
      selectedSourt: true,
      selectedUsaget: false,
      selectedMentioned: false,
    };
    this.globalService.personal_edit = obj;
    this.router.navigate([
      `/sys/person/sourtedit/new_${this.selectedPersonalt.code}`,
    ]);
  }

  async editOrgt(id) {
    if (this.hasChanges()) {
      await this.onSaveForm(true);
    }
    let str = '/sys/person/orgtedit/edit_';
    str += this.viewOnly ? 'v_' : '';
    str += `${this.selectedPersonalt.code}_${id}`;
    let obj = {
      scroll: this.scroll,
      selectedOrgt: true,
      selectedActivityt: false,
      selectedRepresst: false,
      selectedHelp: false,
      selectedSourt: false,
      selectedUsaget: false,
      selectedMentioned: false,
    };
    this.globalService.personal_edit = obj;
    this.router.navigate([str]);
  }

  async editActivityt(id) {
    if (this.hasChanges()) {
      await this.onSaveForm(true);
    }
    let str = '/sys/person/activitytedit/edit_';
    str += this.viewOnly ? 'v_' : '';
    str += `${this.selectedPersonalt.code}_${id}`;
    let obj = {
      scroll: this.scroll,
      selectedOrgt: false,
      selectedActivityt: true,
      selectedRepresst: false,
      selectedHelp: false,
      selectedSourt: false,
      selectedUsaget: false,
      selectedMentioned: false,
    };
    this.globalService.personal_edit = obj;
    this.router.navigate([str]);
    // this.router.navigate([`/sys/person/activitytedit/edit_${this.selectedPersonalt.code}_${id}`]);
  }

  async editRepresst(id) {
    if (this.hasChanges()) {
      await this.onSaveForm(true);
    }
    let str = '/sys/person/represstedit/edit_';
    str += this.viewOnly ? 'v_' : '';
    str += `${this.selectedPersonalt.code}_${id}`;
    let obj = {
      scroll: this.scroll,
      selectedOrgt: false,
      selectedActivityt: false,
      selectedRepresst: true,
      selectedHelp: false,
      selectedSourt: false,
      selectedUsaget: false,
      selectedMentioned: false,
    };
    this.globalService.personal_edit = obj;
    this.router.navigate([str]);
    // this.router.navigate([`/sys/person/represstedit/edit_${this.selectedPersonalt.code}_${id}`]);
  }

  async editHelpt(id) {
    if (this.hasChanges()) {
      await this.onSaveForm(true);
    }
    let str = '/sys/person/helptedit/edit_';
    str += this.viewOnly ? 'v_' : '';
    str += `${this.selectedPersonalt.code}_${id}`;
    let obj = {
      scroll: this.scroll,
      selectedOrgt: false,
      selectedActivityt: false,
      selectedRepresst: false,
      selectedHelp: true,
      selectedSourt: false,
      selectedUsaget: false,
      selectedMentioned: false,
    };
    this.globalService.personal_edit = obj;
    this.router.navigate([str]);
    // this.router.navigate([`/sys/person/helptedit/edit_${this.selectedPersonalt.code}_${id}`]);
  }

  async editUsaget(id) {
    if (this.hasChanges()) {
      await this.onSaveForm(true);
    }
    let str = '/sys/person/usagetedit/edit_';
    str += this.viewOnly ? 'v_' : '';
    str += `${this.selectedPersonalt.code}_${id}`;
    let obj = {
      scroll: this.scroll,
      selectedOrgt: false,
      selectedActivityt: false,
      selectedRepresst: false,
      selectedHelp: false,
      selectedSourt: false,
      selectedUsaget: true,
      selectedMentioned: false,
    };
    this.globalService.personal_edit = obj;
    this.router.navigate([str]);
    // this.router.navigate([`/sys/person/usagetedit/edit_${this.selectedPersonalt.code}_${id}`]);
  }

  async editSourt(id) {
    if (this.hasChanges()) {
      await this.onSaveForm(true);
    }
    let str = '/sys/person/sourtedit/edit_';
    str += this.viewOnly ? 'v_' : '';
    str += `${this.selectedPersonalt.code}_${id}`;
    let obj = {
      scroll: this.scroll,
      selectedOrgt: false,
      selectedActivityt: false,
      selectedRepresst: false,
      selectedHelp: false,
      selectedSourt: true,
      selectedUsaget: false,
      selectedMentioned: false,
    };
    this.globalService.personal_edit = obj;
    this.router.navigate([str]);
    // this.router.navigate([`/sys/person/sourtedit/edit_${this.selectedPersonalt.code}_${id}`]);
  }

  setUntouched() {
    this.personaltForm.markAsUntouched();
    this.myControl_birth_place.markAsUntouched();
    this.myControl_death_place.markAsUntouched();
    this.myControl_nation_id.markAsUntouched();
    this.myControl_residence_code.markAsUntouched();
    this.informtForms.map((form) => {
      form.markAsUntouched();
    });
    this.informt_human_code.map((control) => {
      control.markAsUntouched();
    });
    this.familytForms.map((form) => {
      form.markAsUntouched();
    });
    this.familyt_human_code.map((control) => {
      control.markAsUntouched();
    });
    this.mentionedtForms.map((form) => {
      form.markAsUntouched();
    });
    this.mentionedt_human_code.map((control) => {
      control.markAsUntouched();
    });
  }

  /**
   * Функция возвращает true, если бли внесены изменения
   */
  hasChanges(): boolean {
    let res =
      (this.personaltForm.dirty && this.personaltForm.touched) ||
      (this.myControl_birth_place.dirty &&
        this.myControl_birth_place.touched) ||
      (this.myControl_death_place.dirty &&
        this.myControl_death_place.touched) ||
      (this.myControl_nation_id.dirty && this.myControl_nation_id.touched) ||
      (this.myControl_residence_code.dirty &&
        this.myControl_residence_code.touched);
    this.informtForms.map((form) => {
      if (form.dirty) {
        res = true;
      }
    });
    this.informt_human_code.map((control) => {
      if (control.dirty && control.touched) {
        res = true;
      }
    });
    this.familytForms.map((form) => {
      if (form.dirty && form.touched) {
        res = true;
      }
    });
    if (this.extrashifrsIdTodel && this.extrashifrsIdTodel.length > 0) {
      res = true;
    }
    if (this.dbLinksIdTodel && this.dbLinksIdTodel.length > 0) {
      res = true;
    }
    this.extrashifrForms.map((form) => {
      if (form.dirty && form.touched) {
        res = true;
      }
    });
    this.familyt_human_code.map((control) => {
      if (control.dirty && control.touched) {
        res = true;
      }
    });
    this.mentionedtForms.map((form) => {
      if (form.dirty && form.touched) {
        res = true;
      }
    });
    this.mentionedt_human_code.map((control) => {
      if (control.dirty && control.touched) {
        res = true;
      }
    });
    // this.texttForms.map((form) => {
    //   if (form.dirty) {
    //     res = true;
    //   }
    // });
    return res;
  }

  async getPublicSiteActionType(delPers?) {
    const oldData = this.selectedPersonaltOldData;
    const newData = this.personaltForm;

    if (!delPers && oldData && oldData?.published !== newData.value.published) {
      const isActionDelete =
        this.personaltForm.value['published'] === null ? '*' : null;
      const isActionUpdate =
        this.personaltForm.value['published'] === '*' ? '*' : null;
      await this.publicSiteActions.postOne({
        entity_id: this.id,
        entity_type: 1,
        action_delete: isActionDelete,
        action_update: isActionUpdate,
        legacyId: null,
      });
    }
    if (!oldData && !delPers) {
      await this.publicSiteActions.postOne({
        entity_id: this.id,
        entity_type: 1,
        action_delete: null,
        action_update: '*',
        legacyId: null,
      });
    }
    if (delPers) {
      await this.publicSiteActions.postOne({
        entity_id: this.id,
        entity_type: 1,
        action_delete: '*',
        action_update: null,
        legacyId: `ID-pers-${this.id}`,
      });
    }
  }

  async checkFormChanges(oldData: PersonalT, newData: PersonalT) {
    const isChanged = Object.keys(oldData).some(
      (key) => oldData[key] !== newData[key]
    );

    const isPublished = newData.published === '*';

    if (isChanged && isPublished) {
      this.personaltForm.patchValue({
        mustPublish: '*',
        mustUnpublish: null,
        pub_change_date: this.curDate,
      });

      const archiveFolder = this.delo;

      if (archiveFolder?.published) {
        archiveFolder.mustPublish = '*';
        archiveFolder.mustUnpublish = null;
        archiveFolder.pub_change_date = this.curDate;

        const putArchivefolderForm = this.archivefolderService.putOneById(
          archiveFolder.id,
          archiveFolder
        );

        await putArchivefolderForm;
      }
    }
  }

  /**
   * Функция сохраняет форму. Silent для показа сообщения
   */
  async onSaveForm(silent = false, addNew = false) {
    try {
      if (
        !isNullOrUndefined(this.myControl_birth_place.value) &&
        this.myControl_birth_place.value !== ''
      ) {
        if (!isNullOrUndefined(this.myControl_birth_place.value.place_num)) {
          this.personaltForm.patchValue({
            birth_place_num: this.myControl_birth_place.value.place_num,
          });
        } else {
          let dialogRef = this.dialog.open(DialogYesNoComponent, {
            data: {
              title: 'Введено не корректное место рождения.',
              question: `Продолжить?`,
            },
          });
          await new Promise((resolve, reject) => {
            dialogRef.afterClosed().subscribe(async (result) => {
              if (result === true) {
                this.myControl_birth_place.reset();
                this.personaltForm.patchValue({
                  birth_place_num: null,
                });
                resolve(null);
              } else {
                return false;
              }
            });
          });
          this.myControl_birth_place.reset();
          this.personaltForm.patchValue({
            birth_place_num: null,
          });
        }
      } else {
        this.myControl_birth_place.reset();
        this.personaltForm.patchValue({
          birth_place_num: null,
        });
      }

      if (
        !isNullOrUndefined(this.myControl_death_place.value) &&
        this.myControl_death_place.value !== ''
      ) {
        if (!isNullOrUndefined(this.myControl_death_place.value.place_num)) {
          this.personaltForm.patchValue({
            death_place_num: this.myControl_death_place.value.place_num,
          });
        } else {
          let dialogRef = this.dialog.open(DialogYesNoComponent, {
            data: {
              title: 'Введено не корректное место смерти.',
              question: `Продолжить?`,
            },
          });
          await new Promise((resolve, reject) => {
            dialogRef.afterClosed().subscribe(async (result) => {
              if (result === true) {
                this.myControl_death_place.reset();
                this.personaltForm.patchValue({
                  death_place_num: null,
                });
                resolve(null);
              } else {
                return false;
              }
            });
          });
          this.myControl_death_place.reset();
          this.personaltForm.patchValue({
            death_place_num: null,
          });
        }
      } else {
        this.myControl_death_place.reset();
        this.personaltForm.patchValue({
          death_place_num: null,
        });
      }

      if (
        !isNullOrUndefined(this.myControl_residence_code.value) &&
        this.myControl_residence_code.value !== ''
      ) {
        if (!isNullOrUndefined(this.myControl_residence_code.value.place_num)) {
          this.personaltForm.patchValue({
            place_num: this.myControl_residence_code.value.place_num,
          });
        } else {
          let dialogRef = this.dialog.open(DialogYesNoComponent, {
            data: {
              title: 'Введено не корректное место жительства.',
              question: `Продолжить?`,
            },
          });
          await new Promise((resolve, reject) => {
            dialogRef.afterClosed().subscribe(async (result) => {
              if (result === true) {
                this.myControl_residence_code.reset();
                this.personaltForm.patchValue({
                  place_num: null,
                });
                resolve(null);
              } else {
                return false;
              }
            });
          });
          this.myControl_residence_code.reset();
          this.personaltForm.patchValue({
            place_num: null,
          });
        }
      } else {
        this.myControl_residence_code.reset();
        this.personaltForm.patchValue({
          place_num: null,
        });
      }

      if (!isNullOrUndefined(this.myControl_nation_id.value)) {
        this.personaltForm.patchValue({
          nation_id: this.myControl_nation_id.value.id,
        });
      }

      if (!isNullOrUndefined(this.myControl_citizes.value)) {
        this.personaltForm.patchValue({
          citiz: this.myControl_citizes.value,
        });
      }

      if (
        !isNullOrUndefined(this.personaltForm.value.mentioned_person) &&
        !isNullOrUndefined(this.personaltForm.value.mentioned_person.code)
      ) {
        let code = this.personaltForm.value.mentioned_person.code;
        this.personaltForm.patchValue({
          mentioned_person: code,
        });
      } else {
        this.personaltForm.patchValue({
          mentioned_person: null,
        });
      }

      if (
        this.personaltForm.value['mark'] === true ||
        this.personaltForm.value['mark'] === '*'
      ) {
        this.personaltForm.patchValue({
          mark: '*',
        });
      } else {
        this.personaltForm.patchValue({
          mark: null,
        });
      }

      if (
        this.personaltForm.value['type_r'] === true ||
        this.personaltForm.value['type_r'] === '*'
      ) {
        this.personaltForm.patchValue({
          type_r: '*',
        });
      } else {
        this.personaltForm.patchValue({
          type_r: null,
        });
      }

      if (
        this.personaltForm.value['type_i'] === true ||
        this.personaltForm.value['type_i'] === '*'
      ) {
        this.personaltForm.patchValue({
          type_i: '*',
        });
      } else {
        this.personaltForm.patchValue({
          type_i: null,
        });
      }

      if (
        this.personaltForm.value['type_a'] === true ||
        this.personaltForm.value['type_a'] === '*'
      ) {
        this.personaltForm.patchValue({
          type_a: '*',
        });
      } else {
        this.personaltForm.patchValue({
          type_a: null,
        });
      }

      if (
        this.personaltForm.value['published'] === true ||
        this.personaltForm.value['published'] === '*'
      ) {
        this.personaltForm.patchValue({
          published: '*',
        });
      } else {
        this.personaltForm.patchValue({
          published: null,
        });
      }

      if (this.selectedPersonalt) {
        const oldPublicationStatus = this.selectedPersonalt;
        const newPublicationStatus = this.personaltForm.value.published;

        if (oldPublicationStatus !== newPublicationStatus) {
          if (
            this.personaltForm.value['published'] === true ||
            this.personaltForm.value['published'] === '*'
          ) {
            this.personaltForm.patchValue({
              mustPublish: '*',
              mustUnpublish: null,
              pub_change_date: this.curDate,
            });
          } else {
            this.personaltForm.patchValue({
              mustPublish: null,
              mustUnpublish: '*',
              pub_change_date: this.curDate,
            });
          }
        }
      }

      if (
        (this.personaltForm.value['death_begin_end'] &&
          this.personaltForm.value['death_begin_end'] !== '') ||
        (this.personaltForm.value['death_date_rmk'] &&
          this.personaltForm.value['death_date_rmk'] !== '')
      ) {
        this.personaltForm.patchValue({
          death_mark: 'Н',
        });
      }
    } catch (err) {
      this.openSnackBar(`Ошибка при подготовке даных формы.`, 'Ok');
      console.error(err);
    }
    this.spinnerService.show();
    switch (this.intType) {
      case 'editdoc': // редактирование документа
        /**Удаляем дочерние записи, удаленные из формы*/
        try {
          let extrashifrsIdTodel = Promise.all(
            this.extrashifrsIdTodel.map((id) => {
              return this.personalshifrtService.deleteOneById(id);
            })
          );
          let dbLinkIdTodel = Promise.all(
            this.dbLinksIdTodel.map((id) => {
              return this.personaldbLinkService.deleteOneById(id);
            })
          );
          let informtidTodel = Promise.all(
            this.informtidTodel.map((id) => {
              return this.informtService.deleteOneById(id);
            })
          );
          let familytidTodel = Promise.all(
            this.familytidTodel.map((id) => {
              return this.familytService.deleteOneById(id);
            })
          );
          let mentionedtidTodel = Promise.all(
            this.mentionedtidTodel.map((id) => {
              return this.mentionedtService.deleteOneById(id);
            })
          );
          await Promise.all([
            informtidTodel,
            familytidTodel,
            mentionedtidTodel,
            extrashifrsIdTodel,
            dbLinkIdTodel,
          ]);
        } catch (err) {
          this.openSnackBar(`Ошибка при удалении дочерних элементов.`, 'Ok');
          console.error(err);
          this.spinnerService.hide();
          return false;
        }
        /**Сохранение дочерних сущностей */
        try {
          for (let j = 0; j < this.dbLinkForms.length; j++) {
            if (this.dbLinkForms[j].value) {
              this.dbLinkForms[j].patchValue({
                personal_code: this.id,
              });
              if (!this.dbLinkForms[j].value.id) {
                this.dbLinkForms[j].removeControl('id');
                await this.personaldbLinkService.postOne(
                  this.dbLinkForms[j].value
                );
              } else {
                if (this.dbLinkForms[j].touched) {
                  await this.personaldbLinkService.putOneById(
                    this.dbLinkForms[j].value.id,
                    this.dbLinkForms[j].value
                  );
                }
              }
            }
          }
        } catch (err) {
          this.openSnackBar(`Ошибка сохранения связи с другими БД.`, 'Ok');
          console.error(err);
          this.spinnerService.hide();
          return false;
        }
        try {
          for (let j = 0; j < this.extrashifrForms.length; j++) {
            if (
              this.extrashifrForms[j].value
              // &&
              // (this.extrashifrForms[j].value.fund ||
              //   this.extrashifrForms[j].value.list_no ||
              //   this.extrashifrForms[j].value.file_no)
            ) {
              this.extrashifrForms[j].patchValue({
                personal_code: this.id,
              });
              if (!this.extrashifrForms[j].value.id) {
                this.extrashifrForms[j].removeControl('id');
                await this.personalshifrtService.postOne(
                  this.extrashifrForms[j].value
                );
              } else {
                if (this.extrashifrForms[j].touched) {
                  await this.personalshifrtService.putOneById(
                    this.extrashifrForms[j].value.id,
                    this.extrashifrForms[j].value
                  );
                }
              }
            }
            // else {
            //   this.openSnackBar(`Не верно указан шифр #${j + 1}`, 'Ok');
            //   return false;
            // }
          }
        } catch (err) {
          this.openSnackBar(`Ошибка сохранении шифра.`, 'Ok');
          console.error(err);
          this.spinnerService.hide();
          return false;
        }
        try {
          for (let j = 0; j < this.informtForms.length; j++) {
            if (
              !isNullOrUndefined(this.informt_human_code[j].value) &&
              !isNullOrUndefined(this.informt_human_code[j].value.code)
            ) {
              if (!isNullOrUndefined(this.informt_human_code[j].value)) {
                this.informtForms[j].patchValue({
                  human_code: this.informt_human_code[j].value.code,
                });
              }
              this.informtForms[j].patchValue({
                personal_code: this.id,
              });
              if (isNullOrUndefined(this.informtForms[j].value.id)) {
                this.informtForms[j].removeControl('id');
                await this.informtService.postOne(this.informtForms[j].value);
              } else {
                if (
                  this.informtForms[j].touched ||
                  this.informt_human_code[j].touched
                ) {
                  await this.informtService.putOneById(
                    this.informtForms[j].value.id,
                    this.informtForms[j].value
                  );
                }
              }
            } else {
              this.openSnackBar(`Не верно указан информант #${j + 1}`, 'Ok');
              return false;
            }
          }
        } catch (err) {
          this.openSnackBar(`Ошибка сохранении информантов.`, 'Ok');
          console.error(err);
          this.spinnerService.hide();
          return false;
        }
        try {
          for (let j = 0; j < this.familytForms.length; j++) {
            if (
              !isNullOrUndefined(this.familyt_human_code[j].value) &&
              !isNullOrUndefined(this.familyt_human_code[j].value.code)
            ) {
              if (!isNullOrUndefined(this.familyt_human_code[j].value)) {
                this.familytForms[j].patchValue({
                  human_code: this.familyt_human_code[j].value.code,
                });
              }
              this.familytForms[j].patchValue({
                personal_code: this.id,
              });
              if (isNullOrUndefined(this.familytForms[j].value.id)) {
                this.familytForms[j].removeControl('id');
                await this.familytService.postOne(this.familytForms[j].value);
              } else {
                if (
                  this.familytForms[j].touched ||
                  this.familyt_human_code[j].touched
                ) {
                  await this.familytService.putOneById(
                    this.familytForms[j].value.id,
                    this.familytForms[j].value
                  );
                }
              }
            } else {
              this.openSnackBar(`Не верно указан член семьи #${j + 1}`, 'Ok');
              return false;
            }
          }
        } catch (err) {
          this.openSnackBar(`Ошибка сохранении семьи репрессированного.`, 'Ok');
          console.error(err);
          this.spinnerService.hide();
          return false;
        }
        try {
          for (let j = 0; j < this.mentionedtForms.length; j++) {
            if (
              !isNullOrUndefined(this.mentionedt_human_code[j].value) &&
              !isNullOrUndefined(this.mentionedt_human_code[j].value.code)
            ) {
              if (!isNullOrUndefined(this.mentionedt_human_code[j].value)) {
                this.mentionedtForms[j].patchValue({
                  human_code: this.mentionedt_human_code[j].value.code,
                });
              }
              this.mentionedtForms[j].patchValue({
                personal_code: this.id,
              });
              if (isNullOrUndefined(this.mentionedtForms[j].value.id)) {
                this.mentionedtForms[j].removeControl('id');
                await this.mentionedtService.postOne(
                  this.mentionedtForms[j].value
                );
              } else {
                if (
                  this.mentionedtForms[j].touched ||
                  this.mentionedt_human_code[j].touched
                ) {
                  await this.mentionedtService.putOneById(
                    this.mentionedtForms[j].value.id,
                    this.mentionedtForms[j].value
                  );
                }
              }
            } else {
              this.openSnackBar(`Не верно указан упоминаемый #${j + 1}`, 'Ok');
              return false;
            }
          }
        } catch (err) {
          this.openSnackBar(`Ошибка сохранении упоминаемых.`, 'Ok');
          console.error(err);
          this.spinnerService.hide();
          return false;
        }
        try {
          this.checkFormChanges(
            this.selectedPersonalt,
            this.personaltForm.value
          );
          await this.personaltService.putOneById(
            this.id,
            this.personaltForm.value
          );
          await this.getPublicSiteActionType();
        } catch (err) {
          this.spinnerService.hide();
          this.openSnackBar(`Ошибка сохранении основной формы.`, 'Ok');
          return false;
        }
        break;
      case 'newdoc': // создание документа
        try {
          let postForm = await this.personaltService.postOne(
            this.personaltForm.value
          );
          this.id = +postForm.code;
          await this.getPublicSiteActionType();
        } catch (err) {
          this.openSnackBar(`Ошибка сохранении основной формы.`, 'Ok');
          console.error(err);
          this.spinnerService.hide();
          return false;
        }
        /**Сохранение дочерних сущностей */
        try {
          for (let j = 0; j < this.extrashifrForms.length; j++) {
            if (
              this.extrashifrForms[j].value
              // &&
              // (this.extrashifrForms[j].value.fund ||
              //   this.extrashifrForms[j].value.list_no ||
              //   this.extrashifrForms[j].value.file_no)
            ) {
              this.extrashifrForms[j].patchValue({
                personal_code: this.id,
              });
              this.extrashifrForms[j].removeControl('id');
              await this.personalshifrtService.postOne(
                this.extrashifrForms[j].value
              );
            }
          }
        } catch (err) {
          this.openSnackBar(`Ошибка сохранении шифров.`, 'Ok');
          console.error(err);
          this.spinnerService.hide();
          return false;
        }
        try {
          for (let j = 0; j < this.informtForms.length; j++) {
            if (
              !isNullOrUndefined(this.informt_human_code[j].value) &&
              !isNullOrUndefined(this.informt_human_code[j].value.code)
            ) {
              if (!isNullOrUndefined(this.informt_human_code[j].value)) {
                this.informtForms[j].patchValue({
                  human_code: this.informt_human_code[j].value.code,
                });
              }
              this.informtForms[j].patchValue({
                personal_code: this.id,
              });
              this.informtForms[j].removeControl('id');
              await this.informtService.postOne(this.informtForms[j].value);
            }
          }
        } catch (err) {
          this.openSnackBar(`Ошибка сохранении информантов.`, 'Ok');
          console.error(err);
          this.spinnerService.hide();
          return false;
        }
        try {
          for (let j = 0; j < this.familytForms.length; j++) {
            if (
              !isNullOrUndefined(this.familyt_human_code[j].value) &&
              !isNullOrUndefined(this.familyt_human_code[j].value.code)
            ) {
              if (!isNullOrUndefined(this.familyt_human_code[j].value)) {
                this.familytForms[j].patchValue({
                  human_code: this.familyt_human_code[j].value.code,
                });
              }
              this.familytForms[j].patchValue({
                personal_code: this.id,
              });
              this.familytForms[j].removeControl('id');
              await this.familytService.postOne(this.familytForms[j].value);
            }
          }
        } catch (err) {
          this.openSnackBar(`Ошибка сохранении семьи репрессированного.`, 'Ok');
          console.error(err);
          this.spinnerService.hide();
          return false;
        }
        try {
          for (let j = 0; j < this.mentionedtForms.length; j++) {
            if (
              !isNullOrUndefined(this.mentionedt_human_code[j].value) &&
              !isNullOrUndefined(this.mentionedt_human_code[j].value.code)
            ) {
              if (!isNullOrUndefined(this.mentionedt_human_code[j].value)) {
                this.mentionedtForms[j].patchValue({
                  human_code: this.mentionedt_human_code[j].value.code,
                });
              }
              this.mentionedtForms[j].patchValue({
                personal_code: this.id,
              });
              this.mentionedtForms[j].removeControl('id');
              await this.mentionedtService.postOne(
                this.mentionedtForms[j].value
              );
            }
          }
        } catch (err) {
          this.openSnackBar(`Ошибка сохранении упоминаемых.`, 'Ok');
          this.globalService.exceptionHandling(err);
          this.spinnerService.hide();
          return false;
        }
        // for (let j = 0; j < this.texttForms.length; j++) {
        //   this.texttForms[j].patchValue({
        //     personal_code: this.id
        //   });
        //   this.texttForms[j].removeControl('id');
        //   await this.texttService.postOne(this.texttForms[j].value);
        // }
        if (addNew === false) {
          this.intType = 'editdoc';
        } else {
          this.intType = 'newdoc';
          this.filtersService.personalTListFilter.newFond =
            this.personaltForm.value.fund;
          this.filtersService.personalTListFilter.newOpis =
            this.personaltForm.value.list_no;
          await this.getData();
          return true;
        }
        break;
      default:
    }
    this.setUntouched();
    await this.getData();
    if (!silent) {
      this.openSnackBar(`Сохранено.`, 'Ok');
    }
    this.spinnerService.hide();
    return true;
  }

  async onDel() {
    let dialogRef = this.dialog.open(DialogYesNoComponent, {
      data: {
        title: 'Удалить персоналии?',
        question: ``,
      },
    });
    dialogRef.afterClosed().subscribe(async (result) => {
      if (result === true) {
        this.spinnerService.show();
        try {
          let _orgts = this.orgtService.getByPersonalcode(
            this.selectedPersonalt.code
          );
          let orgts = isNullOrUndefined(await _orgts) ? [] : await _orgts;
          await Promise.all(
            orgts.map(async (orgt) => {
              let _coorgts = this.coorgtService.getByOrgid(orgt.id);
              let coorgts = isNullOrUndefined(await _coorgts)
                ? []
                : await _coorgts;
              await Promise.all(
                coorgts.map(async (coorgt) => {
                  return this.coorgtService.deleteOneById(coorgt.id);
                })
              );
              return this.orgtService.deleteOneById(orgt.id);
            })
          );
          let _actyvitits = this.activitytService.getByPersonalcode(
            this.selectedPersonalt.code
          );
          let actyvitits = isNullOrUndefined(await _actyvitits)
            ? []
            : await _actyvitits;
          await Promise.all(
            actyvitits.map(async (actyvitit) => {
              let _coactts = this.coacttService.getByActid(actyvitit.id);
              let coactts = isNullOrUndefined(await _coactts)
                ? []
                : await _coactts;
              await Promise.all(
                coactts.map(async (coactt) => {
                  return this.coacttService.deleteOneById(coactt.id);
                })
              );
              return this.activitytService.deleteOneById(actyvitit.id);
            })
          );

          const dbLinks = await this.personaldbLinkService.getByPersonalcode(
            this.selectedPersonalt.code
          );
          if (dbLinks) {
            for (const dbLink of dbLinks) {
              await this.personaldbLinkService.deleteOneById(dbLink.id);
            }
          }
          await this.personaltService.deleteOneById(this.id);
          await this.getPublicSiteActionType(true);
          this.router.navigate(['/sys/person/personaltlist']);
          this.openSnackBar(`Удалено.`, 'Ok');
        } catch (err) {
          console.error(`Ошибка при удалении описи ${err}`);
          this.globalService.exceptionHandling(err);
        } finally {
          this.spinnerService.hide();
        }
      }
    });
  }

  displayFn_birth_place(place?: PlaceT): string | undefined {
    return place
      ? place.place_num + '. ' + place.place_code + '. ' + place.place_name
      : undefined;
  }

  displayFn_nation_id(nat?: NatT): string | undefined {
    return nat ? nat.nation : undefined;
  }

  private _filter(name: string): PlaceT[] {
    const filterValue = name.toLowerCase();
    return isNullOrUndefined(this.placets)
      ? null
      : this.placets.filter(
          (placet) =>
            placet.place_name.toLowerCase().indexOf(filterValue) !== -1
        );
  }

  private _filterNatt(value: string): NatT[] {
    const filterValue = value.toLowerCase();
    return this.nations.filter((option) =>
      option.nation.toLowerCase().includes(filterValue)
    );
  }

  private _filterCitiz(value: string) {
    return this.citizes;
  }

  inputLink_birth_place() {
    clearTimeout(this.timerId);
    this.timerId = setTimeout(async () => {
      this.placets = await this.placetService.getWithFilterLimit(
        100,
        this.myControl_birth_place.value
      );
      // this.placets = (isNullOrUndefined(placets)) ? [] : placets;
      if (!isNullOrUndefined(this.placets)) {
        try {
          this.filteredPlacets = this.myControl_birth_place.valueChanges.pipe(
            startWith<string | PlaceT>(''),
            map((value) =>
              typeof value === 'string' ? value : value.place_name
            ),
            map((name) => (name ? this._filter(name) : [...this.placets]))
          );
        } catch {}
      }
    }, 500);
  }
  inputLink_death_place() {
    clearTimeout(this.timerId);
    this.timerId = setTimeout(async () => {
      this.placets = await this.placetService.getWithFilterLimit(
        100,
        this.myControl_death_place.value
      );
      // this.placets = (isNullOrUndefined(placets)) ? [] : placets;
      if (!isNullOrUndefined(this.placets)) {
        try {
          this.filteredPlacets = this.myControl_death_place.valueChanges.pipe(
            startWith<string | PlaceT>(''),
            map((value) =>
              typeof value === 'string' ? value : value.place_name
            ),
            map((name) => (name ? this._filter(name) : [...this.placets]))
          );
        } catch {}
      }
    }, 500);
  }
  inputLink_residence_code() {
    clearTimeout(this.timerId);
    this.timerId = setTimeout(async () => {
      this.placets = await this.placetService.getWithFilterLimit(
        100,
        this.myControl_residence_code.value
      );
      if (!isNullOrUndefined(this.placets)) {
        try {
          this.filteredPlacets =
            this.myControl_residence_code.valueChanges.pipe(
              startWith<string | PlaceT>(''),
              map((value) => {
                if (!isNullOrUndefined(value)) {
                  return typeof value === 'string' ? value : value.place_name;
                }
              }),
              map((name) => (name ? this._filter(name) : [...this.placets]))
            );
        } catch {}
      }
    }, 500);
  }

  inputLink_informt_human_code(j) {
    clearTimeout(this.timerId);
    this.timerId = setTimeout(async () => {
      this.personalts = await this.personaltService.getWithShortfilterLimit(
        100,
        this.informt_human_code[j].value
      );
      if (!isNullOrUndefined(this.personalts)) {
        try {
          this.filteredPersonalts = (<FormControl>(
            this.informt_human_code[j]
          )).valueChanges.pipe(
            startWith<string | PersonalT>(''),
            map((value) => (typeof value === 'string' ? value : value.surname)),
            map((name) =>
              name ? this._filterPersonalt(name) : [...this.personalts]
            )
          );
        } catch {}
      }
    }, 500);
  }

  inputLink_mentioned_person_human_code() {
    clearTimeout(this.timerId);
    this.timerId = setTimeout(async () => {
      this.personalts = await this.personaltService.getWithShortfilterLimit(
        100,
        this.personaltForm.value.mentioned_person
      );
      if (!isNullOrUndefined(this.personalts)) {
        try {
          this.filteredPersonalts = (<FormControl>(
            this.personaltForm.controls.mentioned_person
          )).valueChanges.pipe(
            startWith<string | PersonalT>(''),
            map((value) => (typeof value === 'string' ? value : value.surname)),
            map((name) =>
              name ? this._filterPersonalt(name) : [...this.personalts]
            )
          );
        } catch {}
      }
    }, 500);
  }

  inputLink_familyt_human_code(j) {
    clearTimeout(this.timerId);
    this.timerId = setTimeout(async () => {
      this.personalts = await this.personaltService.getWithShortfilterLimit(
        100,
        this.familyt_human_code[j].value
      );
      if (!isNullOrUndefined(this.personalts)) {
        try {
          this.filteredPersonalts = (<FormControl>(
            this.familyt_human_code[j]
          )).valueChanges.pipe(
            startWith<string | PersonalT>(''),
            map((value) => (typeof value === 'string' ? value : value.surname)),
            map((name) =>
              name ? this._filterPersonalt(name) : [...this.personalts]
            )
          );
        } catch {}
      }
    }, 500);
  }
  inputLink_mentionedt_human_code(j) {
    clearTimeout(this.timerId);
    this.timerId = setTimeout(async () => {
      this.personalts = await this.personaltService.getWithShortfilterLimit(
        100,
        this.mentionedt_human_code[j].value
      );
      if (!isNullOrUndefined(this.personalts)) {
        try {
          this.filteredPersonalts = (<FormControl>(
            this.mentionedt_human_code[j]
          )).valueChanges.pipe(
            startWith<string | PersonalT>(''),
            map((value) => (typeof value === 'string' ? value : value.surname)),
            map((name) =>
              name ? this._filterPersonalt(name) : [...this.personalts]
            )
          );
        } catch {}
      }
    }, 500);
  }

  private _filterPersonalt(name: string): PersonalT[] {
    const filterValue = name.toLowerCase();
    return isNullOrUndefined(this.personalts)
      ? null
      : this.personalts
          .filter(
            (personalt) =>
              !isNullOrUndefined(personalt.surname) && personalt.surname !== ''
          )
          .filter(
            (personalt) =>
              personalt.surname.toLowerCase().indexOf(filterValue) !== -1
          );
  }

  displayFn_human_code(personal?: PersonalT): string | undefined {
    if (!personal) {
      return undefined;
    }
    let str = '';
    str += personal.code + '. ';
    str += !isNullOrUndefined(personal.surname) ? personal.surname + ' ' : '';
    str += !isNullOrUndefined(personal.fname) ? personal.fname + ' ' : '';
    str += !isNullOrUndefined(personal.lname) ? personal.lname + ' ' : '';
    return personal ? str : undefined;
  }

  // resetBirth() {
  //   this.personaltForm.patchValue({
  //     birth: '',
  //   });
  // }

  // resetDeath() {
  //   this.personaltForm.patchValue({
  //     death: '',
  //   });
  // }

  async canDeactivate() {
    if (this.hasChanges() === true) {
      let dialogRef = this.dialog.open(DialogSaveNoComponent, {
        data: {
          title: 'Сохранить изменения?',
          question: ``,
        },
      });
      let res = await new Promise((resolve, reject) => {
        dialogRef.afterClosed().subscribe(async (result) => {
          if (result === true) {
            let _res = await this.onSaveForm(true);
            if (_res === true) {
              resolve(true);
            } else {
              resolve(false);
            }
          } else if (result === false) {
            resolve(true);
          } else {
            resolve(false);
          }
        });
      });
      return res;
    }
    return true;
  }

  isFondopisdeloValid(): boolean {
    let res = true;
    if (
      isNaN(parseFloat(this.personaltForm.value.fund)) ||
      isNaN(parseFloat(this.personaltForm.value.list_no)) ||
      isNaN(parseFloat(this.personaltForm.value.file_no))
    ) {
      res = false;
    }
    return res;
  }

  isExtraFondopisdeloValid(value): boolean {
    let res = true;
    if (
      isNaN(parseFloat(value.fund)) ||
      isNaN(parseFloat(value.list_no)) ||
      isNaN(parseFloat(value.file_no))
    ) {
      res = false;
    }
    return res;
  }

  async onOpendelo() {
    try {
      let doc = await this.archivefolderService.getWithOpis_data(
        this.personaltForm.value.file_no,
        this.personaltForm.value.fund,
        this.personaltForm.value.list_no,
        this.personaltForm.value.file_no_ext
      );
      if (isNullOrUndefined(doc)) {
        this.openSnackBar(`Дел не найдено`, 'Ok');
      } else {
        // this.globalService.backUrl = this.router.url;
        this.router.navigate([`/sys/arch/edit/${doc[0].id}`]);
      }
    } catch (err) {
      this.globalService.exceptionHandling(err);
    }
  }
  async onOpendelos(idx) {
    try {
      let doc = await this.archivefolderService.getWithOpis_data(
        this.extrashifrForms[idx].value.file_no,
        this.extrashifrForms[idx].value.fund,
        this.extrashifrForms[idx].value.list_no,
        this.extrashifrForms[idx].value.file_no_ext
      );
      if (isNullOrUndefined(doc)) {
        this.openSnackBar(`Дел не найдено`, 'Ok');
      } else {
        // this.globalService.backUrl = this.router.url;
        this.router.navigate([`/sys/arch/edit/${doc[0].id}`]);
      }
    } catch (err) {
      this.globalService.exceptionHandling(err);
    }
  }

  onAddPersonalTinform(idx) {
    this.filteredPersonalts = null;
    let dialogRef = this.dialog.open(DialogPersonalEditComponent, {
      panelClass: 'app-dialog-extend',
    });
    dialogRef.afterClosed().subscribe(async (result) => {
      if (!isNullOrUndefined(result) && result !== false) {
        // console.log(result);
        // this.informt_human_code[idx].reset();
        let res = await this.personaltService.postOne(result);
        this.informt_human_code[idx].setValue(res);
      }
    });
  }
  onAddPersonalTfamily(idx) {
    this.filteredPersonalts = null;
    let dialogRef = this.dialog.open(DialogPersonalEditComponent, {
      panelClass: 'app-dialog-extend',
    });
    dialogRef.afterClosed().subscribe(async (result) => {
      if (!isNullOrUndefined(result) && result !== false) {
        let res = await this.personaltService.postOne(result);
        this.familyt_human_code[idx].setValue(res);
      }
    });
  }
  onAddPersonalTmentionedt(idx) {
    this.filteredPersonalts = null;
    let dialogRef = this.dialog.open(DialogPersonalEditComponent, {
      panelClass: 'app-dialog-extend',
    });
    dialogRef.afterClosed().subscribe(async (result) => {
      if (!isNullOrUndefined(result) && result !== false) {
        let res = await this.personaltService.postOne(result);
        this.mentionedt_human_code[idx].setValue(res);
      }
    });
  }

  getCheck(flag) {
    return flag === true
      ? '<i class="fa fa-check" aria-hidden="true"></i>'
      : '';
  }

  getValueByKey(key): string {
    return isNullOrUndefined(this.statusTypes.find((item) => item.key === key))
      ? ''
      : this.statusTypes.find((item) => item.key === key).value;
  }
  getActionValueByKey(key): string {
    return isNullOrUndefined(this.actionTypes.find((item) => item.key === key))
      ? ''
      : this.actionTypes.find((item) => item.key === key).value;
  }
  getTableValueByKey(key): string {
    let str = '';
    key.forEach((element) => {
      let name = isNullOrUndefined(
        this.tableTypes.find((item) => item.key === element)
      )
        ? element
        : this.tableTypes.find((item) => item.key === element).value;
      str += `${name}; `;
    });
    return str;
    // return isNullOrUndefined(this.tableTypes.find((item) => item.key === key))
    //   ? key
    //   : this.tableTypes.find((item) => item.key === key).value;
  }

  onLinkPersonalT(id) {
    let str = this.viewOnly
      ? `/sys/person/personaltedit/v_${id}`
      : `/sys/person/personaltedit/${id}`;
    this.router.navigate([str]);
  }

  goBack() {
    this.location.back();
  }

  onCheckDelo() {
    this.checkDelo = true;
    if (this.personaltForm.value.file_no > 99999) {
      this.personaltForm.patchValue({
        file_no: +`${this.personaltForm.value.file_no}`.substr(0, 5),
      });
    }
    if (this.personaltForm.value.list_no > 9999) {
      this.personaltForm.patchValue({
        list_no: +`${this.personaltForm.value.list_no}`.substr(0, 4),
      });
    }
    if (this.personaltForm.value.fund > 9999) {
      this.personaltForm.patchValue({
        fund: +`${this.personaltForm.value.fund}`.substr(0, 4),
      });
    }
    clearTimeout(this.deloTimer);
    if (this.isFondopisdeloValid()) {
      this.deloTimer = setTimeout(async () => {
        try {
          let doc = this.archivefolderService.getWithOpis_data(
            this.personaltForm.value.file_no,
            this.personaltForm.value.fund,
            this.personaltForm.value.list_no,
            this.personaltForm.value.file_no_ext
          );
          this.delo = isNullOrUndefined(await doc) ? null : (await doc)[0];
        } catch (err) {
          this.checkDelo = null;
        } finally {
          this.checkDelo = false;
        }
      }, 400);
    }
  }

  async onCheckDelos(idx) {
    if (this.extrashifrForms[idx].value.file_no > 99999) {
      this.extrashifrForms[idx].patchValue({
        file_no: +`${this.extrashifrForms[idx].value.file_no}`.substr(0, 5),
      });
    }
    if (this.extrashifrForms[idx].value.list_no > 9999) {
      this.extrashifrForms[idx].patchValue({
        list_no: +`${this.extrashifrForms[idx].value.list_no}`.substr(0, 4),
      });
    }
    if (this.extrashifrForms[idx].value.fund > 9999) {
      this.extrashifrForms[idx].patchValue({
        fund: +`${this.extrashifrForms[idx].value.fund}`.substr(0, 4),
      });
    }
    // clearTimeout(this.deloTimer);
    if (this.isFondopisdeloValid()) {
      // this.deloTimer = setTimeout(async () => {
      try {
        if (
          this.extrashifrForms[idx].value.file_no &&
          this.extrashifrForms[idx].value.fund &&
          this.extrashifrForms[idx].value.list_no
        ) {
          let doc = this.archivefolderService.getWithOpis_data(
            this.extrashifrForms[idx].value.file_no,
            this.extrashifrForms[idx].value.fund,
            this.extrashifrForms[idx].value.list_no,
            this.extrashifrForms[idx].value.file_no_ext
          );
          this.delos[idx] = isNullOrUndefined(await doc)
            ? null
            : (await doc)[0];
        } else {
          this.delos[idx] = null;
        }
      } catch (err) {
      } finally {
      }
      // }, 400);
    }
  }

  onCreateDelo() {
    if (isNullOrUndefined(this.opisItems) || this.opisItems.length === 0) {
      this.openSnackBar(`Не заполнен список описей.`, 'Ok');
      return;
    }
    let name = !isNullOrUndefined(this.personaltForm.getRawValue().surname)
      ? this.personaltForm.getRawValue().surname + ' '
      : '';
    name += !isNullOrUndefined(this.personaltForm.getRawValue().fname)
      ? this.personaltForm.getRawValue().fname + ' '
      : '';
    name += !isNullOrUndefined(this.personaltForm.getRawValue().lname)
      ? this.personaltForm.getRawValue().lname
      : '';
    // Фонд = {{ opisItem.fond }}. Опись = {{ opisItem.opis_name }}.
    let opis = this.opisItems.find((oitem) => {
      return (
        +oitem.fond === +this.personaltForm.getRawValue().fund &&
        +oitem.opis_name === +this.personaltForm.getRawValue().list_no
      );
    });
    if (
      !isNullOrUndefined(this.personaltForm.value['birth_begin']) &&
      new Date(this.personaltForm.value['birth_begin']) instanceof Date &&
      !isNaN(new Date(this.personaltForm.value['birth_begin']).valueOf())
    ) {
      name +=
        ', ' + new Date(this.personaltForm.value['birth_begin']).getFullYear();
    } else if (!isNullOrUndefined(this.personaltForm.value['birth_rmk'])) {
      name += ', ' + this.personaltForm.value['birth_rmk'];
    }
    if (
      !isNullOrUndefined(this.personaltForm.value['death_end']) &&
      new Date(this.personaltForm.value['death_end']) instanceof Date &&
      !isNaN(new Date(this.personaltForm.value['death_end']).valueOf())
    ) {
      name +=
        '-' + new Date(this.personaltForm.value['death_end']).getFullYear();
    } else if (
      !isNullOrUndefined(this.personaltForm.value['death_begin']) &&
      new Date(this.personaltForm.value['death_begin']) instanceof Date &&
      !isNaN(new Date(this.personaltForm.value['death_begin']).valueOf())
    ) {
      name +=
        '-' + new Date(this.personaltForm.value['death_begin']).getFullYear();
    } else if (!isNullOrUndefined(this.personaltForm.value['death_date_rmk'])) {
      name += ', ' + this.personaltForm.value['death_date_rmk'];
    }
    let archivefolderForm: FormGroup;
    archivefolderForm = new FormGroup({
      name: new FormControl(name, []),
      page_cnt: new FormControl(0, [Validators.required]),
      opis_id: new FormControl(isNullOrUndefined(opis) ? null : opis.id, [
        Validators.required,
      ]),
      delo: new FormControl(this.personaltForm.getRawValue().file_no, [
        Validators.required,
      ]),
    });
    let dialogRef = this.dialog.open(DialogMarchiveEditComponent, {
      panelClass: 'app-dialog-extend',
      data: {
        form: archivefolderForm.value,
        opisItems: this.opisItems,
      },
    });
    dialogRef.afterClosed().subscribe(async (result) => {
      if (!isNullOrUndefined(result) && result !== false) {
        try {
          this.spinnerService.show();
          this.delo = await this.archivefolderService.postOne(result);
          // let data = this.opisItems.find(item => item.id === result.opis_id);
          // if (!isNullOrUndefined(data)) {
          //   this.personaltForm.patchValue({
          //     fund: data.fond,
          //     list_no: data.opis_name
          //   });
          // }
          if (this.id === 'newdoc') {
            await this.onSaveForm(true, false);
          }
          let doc = await this.archivefolderService.getWithOpis_data(
            this.personaltForm.value.file_no,
            this.personaltForm.value.fund,
            this.personaltForm.value.list_no,
            this.personaltForm.value.file_no_ext
          );
          let gen = await this.archivefolderService.postDescription(
            this.id,
            doc[0].delo,
            doc[0].opis_name,
            doc[0].fond,
            doc[0].delo_extra
          );
        } catch (err) {
          this.globalService.exceptionHandling(err);
        } finally {
          this.spinnerService.hide();
        }
      }
    });
  }

  async generateDelo() {
    this.spinnerService.show();
    try {
      let doc = await this.archivefolderService.getWithOpis_data(
        this.personaltForm.value.file_no,
        this.personaltForm.value.fund,
        this.personaltForm.value.list_no,
        this.personaltForm.value.file_no_ext
      );
      let gen = await this.archivefolderService.postDescription(
        this.id,
        doc[0].delo,
        doc[0].opis_name,
        doc[0].fond,
        doc[0].delo_extra
      );
    } catch (err) {
      this.globalService.exceptionHandling(err);
    } finally {
      this.spinnerService.hide();
    }
  }

  async copyCommonRmk() {
    this.spinnerService.show();
    try {
      this.personaltForm.patchValue({
        public_rmk: this.selectedPersonalt.surname_rmk,
      });
    } catch (err) {
      this.globalService.exceptionHandling(err);
    } finally {
      this.spinnerService.hide();
    }
  }

  async generateDelos(idx) {
    this.spinnerService.show();
    try {
      const doc = await this.archivefolderService.getWithOpis_data(
        this.extrashifrForms[idx].value.file_no,
        this.extrashifrForms[idx].value.fund,
        this.extrashifrForms[idx].value.list_no,
        this.extrashifrForms[idx].value.file_no_ext
      );
      if (doc) {
        const gen = await this.archivefolderService.postDescription(
          this.id,
          doc[0].delo,
          doc[0].opis_name,
          doc[0].fond,
          doc[0].delo_extra
        );
      } else {
        this.openSnackBar(`Дел не найдено`, 'Ok');
      }
    } catch (err) {
      this.globalService.exceptionHandling(err);
    } finally {
      this.spinnerService.hide();
    }
  }

  getRepress_typesById(id: number) {
    let repress_type = this.repress_types.find((item) => item.id === id);
    return isNullOrUndefined(repress_type) ? '' : repress_type.name_ent;
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
    });
  }

  stop(e) {
    e.stopPropagation();
  }
  async drop(event: CdkDragDrop<string[]>, tableName: string) {
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    }

    try {
      // this.spinnerService.show();
      let newArr;
      switch (tableName) {
        case 'activity_t':
          newArr = await this.moveordernomService.move(
            tableName,
            this.selectedActivityts[event.currentIndex].id,
            event.currentIndex + 1
          );
          this.selectedActivityts = [...newArr];
          break;
        case 'repress_t':
          newArr = await this.moveordernomService.move(
            tableName,
            this.selectedRepressts[event.currentIndex].id,
            event.currentIndex + 1
          );
          this.selectedRepressts = [...newArr];
          break;
        case 'org_t':
          newArr = await this.moveordernomService.move(
            tableName,
            this.selectedOrgts[event.currentIndex].id,
            event.currentIndex + 1
          );
          this.selectedOrgts = [...newArr];
          break;
        case 'sour_t':
          newArr = await this.moveordernomService.move(
            tableName,
            this.selectedSourts[event.currentIndex].sour_id,
            event.currentIndex + 1
          );
          this.selectedSourts = [...newArr];
          break;
        case 'help_t':
          newArr = await this.moveordernomService.move(
            tableName,
            this.selectedHelpts[event.currentIndex].id,
            event.currentIndex + 1
          );
          this.selectedHelpts = [...newArr];
          break;
        // case 'usage_t':
        //   newArr = await this.moveordernomService.move(
        //     tableName,
        //     this.selectedUsagets[event.currentIndex].id,
        //     event.currentIndex + 1
        //   );
        //   this.selectedUsagets = [...newArr];
        //   break;

        default:
          break;
      }
    } catch (err) {
      this.globalService.exceptionHandling(err);
    } finally {
      // this.spinnerService.hide();
    }
  }

  getUstypeById(id: number) {
    return this.us_types.find((item) => item.id === id);
  }

  async downloadCard() {
    this.spinnerService.show();
    try {
      let blob = await this.selectionitemService.getExcelkart(this.id);

      blob.subscribe((event) => {
        if (event.type === HttpEventType.DownloadProgress) {
          const percentDone = Math.round((100 * event.loaded) / event.total);
          // console.log(percentDone);
        } else if (event instanceof HttpResponse) {
          if (event.body.type.includes('application')) {
            importedSaveAs(event.body, `Карточка_code${this.id}.xlsx`);
          } else {
            importedSaveAs(event.body, `Карточка_code${this.id}.zip`);
          }
          this.spinnerService.hide();
        } else if (event.status === 409) {
          this.spinnerService.hide();
        }
      });
      // console.log(blob);
    } catch (err) {
      this.globalService.exceptionHandling(err);
    }
  }

  async downloadCards() {
    try {
      this.spinnerService.show();
      let blob = this.selectionitemService.getExcelkarts(this.id);
      blob.subscribe((event) => {
        if (event.type === HttpEventType.DownloadProgress) {
          const percentDone = Math.round((100 * event.loaded) / event.total);
        } else if (event instanceof HttpResponse) {
          this.spinnerService.hide();
          if (event.body.type.includes('application')) {
            importedSaveAs(event.body, `Карточки_code${this.id}.xlsx`);
          } else {
            importedSaveAs(event.body, `Карточки_code${this.id}.zip`);
          }
          // importedSaveAs(event.body, `Карточки_code${this.id}.zip`);
        } else if (event.status === 409) {
          this.spinnerService.hide();
        }
      });
    } catch (err) {
      this.globalService.exceptionHandling(err);
    }
  }

  linkUrl() {
    let str = '';
    if (this.personaltForm.getRawValue().url.indexOf('//') === -1) {
      str = '//';
    }
    str += this.personaltForm.getRawValue().url;
    window.open(str, '_blank');
  }
  linkUrl2() {
    let str = '';
    if (this.personaltForm.getRawValue().url2.indexOf('//') === -1) {
      str = '//';
    }
    str += this.personaltForm.getRawValue().url2;
    window.open(str, '_blank');
  }
  linkUrl3() {
    let str = '';
    if (this.personaltForm.getRawValue().url3.indexOf('//') === -1) {
      str = '//';
    }
    str += this.personaltForm.getRawValue().url3;
    window.open(str, '_blank');
  }

  joinSourt(e: MouseEvent) {
    e.stopPropagation();
    let dialogRef = this.dialog.open(DialogJoinSourtComponent, {
      data: {
        fund: this.personaltForm.value.fund,
        list_no: this.personaltForm.value.list_no,
        file_no: this.personaltForm.value.file_no,
      },
      panelClass: 'app-dialog-extend',
    });
    dialogRef.afterClosed().subscribe(async (result) => {
      if (result) {
        let toAdd = [];
        result.forEach((res) => {
          let id = res.sour_id || res.id;
          let d;
          if (this.selectedSourts) {
            d = this.selectedSourts.find((item) => item.sour_id == id);
          }
          if (!d) {
            res['sour_id'] = id;
            toAdd.push(res);
          }
        });
        try {
          this.spinnerService.show();
          await Promise.all(
            toAdd.map((item) =>
              this.personalsourtService.postOne({
                sour_id: item.sour_id,
                personal_code: this.id,
              })
            )
          );
          let selectedPersonalt = await this.personaltService.getOneById(
            this.id,
            true,
            true
          );
          this.selectedSourts = !selectedPersonalt['sour_t']
            ? []
            : selectedPersonalt['sour_t'];
          // toAdd.forEach((element) => {
          //   this.selectedSourts.unshift(element);
          // });
          // this.selectedSourts.sort((a, b) => a.sour_no - b.sour_no);
        } catch (e) {
        } finally {
          this.spinnerService.hide();
        }
      }
      // if (result) {
      //   let id = result.sour_id || result.id;
      //   let d = this.selectedSourts.find((item) => item.sour_id === id);
      //   if (d) {
      //     this.globalService.openSnackBar('Источник уже присоединен', 'Ok');
      //   } else {
      //     try {
      //       await this.personalsourtService.postOne({
      //         sour_id: id,
      //         personal_code: this.id,
      //       });
      //       this.selectedSourts.unshift(result);
      //     } catch (e) {}
      //   }
      // }
    });
  }

  pinPerson() {
    this.filtersService.pinPersonal.next(this.id);
    window.localStorage.setItem('code', JSON.stringify(this.id));
  }
  unpinPerson() {
    this.filtersService.pinPersonal.next(null);
    window.localStorage.removeItem('code');
  }

  onLinkDocuments(e: MouseEvent, sourt) {
    e.stopPropagation();
    this.filtersService.selectionSourtFilter = new SourtFilter();
    this.filtersService.selectionSourtFilter.fund = sourt.fund;
    this.filtersService.selectionSourtFilter.list_no = sourt.list_no;

    this.filtersService.selectionSourtFilter.file_no = sourt.file_no;
    this.filtersService.selectionSourtFilter.file_no_ext = sourt.file_no_ext;
    // if (sourt.file_no_ext) {
    //   this.filtersService.selectionSourtFilter.file_no_ext_noneed = false;
    // }
    if (!sourt.file_no_ext || sourt.file_no_ext === '') {
      this.filtersService.selectionSourtFilter.file_no_ext_noneed = true;
    }
    this.router.navigate(['sys', 'person', 'doclist']);
  }

  setDeathdateEnable() {
    if (
      this.personaltForm.value.death_mark != 'Д' &&
      this.authService.getCurUser().usertype !== 4
    ) {
      this.personaltForm.controls['death_begin_end'].enable();
    } else {
      this.personaltForm.controls['death_begin_end'].disable();
    }
    // personaltForm.value?.death_mark == 'Н' ? false : true"
  }
}
