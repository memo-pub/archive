import {
  Component,
  OnInit,
  AfterContentChecked,
  HostListener,
  OnDestroy,
  ViewEncapsulation,
} from '@angular/core';
import { AuthService } from 'src/app/shared/services/auth.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { PersonaltService } from 'src/app/system/shared/services/personalt.service';
import { ListoftService } from 'src/app/system/shared/services/listoft.service';
import { PlacetService } from 'src/app/system/shared/services/placet.service';
import { ActivitytService } from 'src/app/system/shared/services/activityt.service';
import { CoacttService } from 'src/app/system/shared/services/coactt.service';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ListOfT } from 'src/app/shared/models/listoft.model';
import { PersonalT } from 'src/app/shared/models/personalt.model';
import { Observable, Subscription } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { isNullOrUndefined } from 'util';
import { DialogYesNoComponent } from 'src/app/system/shared/components/dialog-yes-no/dialog-yes-no.component';
import { PlaceT } from 'src/app/shared/models/placet.model';
import { startWith, map } from 'rxjs/operators';
import { DialogSaveNoComponent } from 'src/app/system/shared/components/dialog-save-no/dialog-save-no.component';
import { DialogPersonalEditComponent } from '../dialog-personal-edit/dialog-personal-edit.component';
import { RepressT } from 'src/app/shared/models/represst.model';
import {
  ActivityT,
  activityT_width,
} from 'src/app/shared/models/activityt.model';
import { coactT_width } from 'src/app/shared/models/coactt.model';
import { ActspheretService } from 'src/app/system/shared/services/actspheret.service';
import { DateparseService } from 'src/app/shared/services/dateparse.service';

@Component({
  selector: 'app-activityt-edit',
  templateUrl: './activityt-edit.component.html',
  styleUrls: ['./activityt-edit.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class ActivitytEditComponent
  implements OnInit, OnDestroy, AfterContentChecked {
  constructor(
    public dateparseService: DateparseService,
    private authService: AuthService,
    private globalService: GlobalService,
    private personaltService: PersonaltService,
    private listoftService: ListoftService,
    private placetService: PlacetService,
    private activitytService: ActivitytService,
    private actspheretService: ActspheretService,
    private coacttService: CoacttService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private spinnerService: NgxSpinnerService,
    public dialog: MatDialog,
    public snackBar: MatSnackBar
  ) {
    this.activeRouterObserver = this.activatedRoute.params.subscribe(
      (param) => {
        let str = param.id.split('_');
        if (str[0] === 'new') {
          this.intType = 'newdoc';
          this.selectedPersonalcode = +str[1];
          this.viewOnly = false;
        } else {
          if (str[1] === 'v') {
            this.id = +str[3];
            this.selectedPersonalcode = +str[2];
            this.intType = 'editdoc';
            this.viewOnly = true;
          } else {
            this.id = +str[2];
            this.selectedPersonalcode = +str[1];
            this.intType = 'editdoc';
            this.viewOnly = false;
          }
        }
        this.viewOnly =
          this.viewOnly === false
            ? this.authService.getCurUser().usertype === 4
            : true;
        this.getData();
      }
    );
    this.globalService.routerObserver = router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        if (val.url.indexOf('activitytedit') !== -1) {
          this.globalService.breadcrumbs.length = 0;
          this.globalService.breadcrumbs.push(
            {
              label: 'персоналии',
              url:
                this.viewOnly === true
                  ? `/sys/person/personaltedit/v_${this.selectedPersonalcode}`
                  : `/sys/person/personaltedit/${this.selectedPersonalcode}`,
            },
            { label: 'учеба и профессиональная деятельность' }
          );
          this.breadcrumbs = this.globalService.breadcrumbs;
        }
      }
    });
  }
  activityT_width = activityT_width;
  coactT_width = coactT_width;

  timerId;
  id: number;
  intType: string | any = ''; // тип интерфейса
  viewOnly: boolean; // тип интерфейса
  selectedPersonalcode: number;
  selectedPersonal: PersonalT;
  selectedRepressts: RepressT[] = [];

  actspheret: any[];
  actspheretInForm: any[];
  repress_types: ListOfT[];
  org_codes: ListOfT[];
  org_names: ListOfT[];
  spheres: ListOfT[];
  personalts: PersonalT[];
  filteredPersonalts: Observable<PersonalT[]>;
  placets: PlaceT[];
  filteredPlacets: Observable<PlaceT[]>;

  coacttidTodel: number[] = [];

  activitytForm: FormGroup;
  coacttForms: FormGroup[] = [];

  myControl_human_code: FormControl[] = [];
  myControl_geoplace_code: FormControl;

  valid: boolean;

  breadcrumbs = [];
  activeRouterObserver: Subscription;

  newnom: number;

  first = false;
  last = false;
  selectedActivityts: ActivityT[] = [];
  @HostListener('window:keydown', ['$event'])
  onKeyPress(event: KeyboardEvent) {
    if ((event.ctrlKey || event.metaKey) && event.keyCode == 83) {
      event.preventDefault();
      if (!this.viewOnly && this.valid && this.intType !== 'myprofile') {
        this.onSaveForm();
      }
    }
  }

  ngOnDestroy(): void {
    this.globalService.routerObserver.unsubscribe();
    // this.activeRouterObserver.unsubscribe();
  }

  async ngOnInit() {
    this.activitytForm = new FormGroup({
      id: new FormControl({ value: null, disabled: this.viewOnly }, []),
      personal_code: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      act_no: new FormControl({ value: null, disabled: true }, [
        Validators.required,
      ]),
      sphere: new FormControl({ value: null, disabled: this.viewOnly }, []),
      prof: new FormControl({ value: null, disabled: this.viewOnly }, []),
      post: new FormControl({ value: null, disabled: this.viewOnly }, []),
      // arrival_dat: new FormControl({ value: null, disabled: true }, []),
      arrival_dat_begin: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      arrival_dat_end: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      arrival_dat_begin_end: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      // depart_dat: new FormControl({ value: null, disabled: true }, []),
      depart_dat_begin: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      depart_dat_end: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      depart_dat_begin_end: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      sphere_rmk: new FormControl({ value: null, disabled: this.viewOnly }, []),
      post_rmk: new FormControl({ value: null, disabled: this.viewOnly }, []),
      prof_rmk: new FormControl({ value: null, disabled: this.viewOnly }, []),
      ent1_rmk: new FormControl({ value: null, disabled: this.viewOnly }, []),
      arrival_dat_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      depart_dat_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      general_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      ent_code: new FormControl({ value: null, disabled: this.viewOnly }, []),
      // geoplace_code: new FormControl(
      //   { value: null, disabled: this.viewOnly },
      //   []
      // ),
      place_num: new FormControl({ value: null, disabled: this.viewOnly }, []),
      geoplace_code_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      // range_mark: new FormControl({ value: null, disabled: this.viewOnly }, []),
      // rep_mark: new FormControl({ value: null, disabled: this.viewOnly }, []),
      rep_id: new FormControl({ value: null, disabled: this.viewOnly }, []),
    });
    this.myControl_geoplace_code = new FormControl({
      value: null,
      disabled: this.viewOnly,
    });
    try {
      let repress_types = this.listoftService.getWithFilter('REPRESS_TYPE');
      let org_codes = this.listoftService.getWithFilter('ORG_CODE');
      let org_names = this.listoftService.getWithFilter('ORG_NAME');
      let spheres = this.listoftService.getWithFilter('SPHERE');
      this.org_codes = isNullOrUndefined(await org_codes)
        ? []
        : await org_codes;
      this.org_names = isNullOrUndefined(await org_names)
        ? []
        : await org_names;
      this.spheres = isNullOrUndefined(await spheres) ? [] : await spheres;
      this.repress_types = isNullOrUndefined(await repress_types)
        ? []
        : await repress_types;
      this.spheres.sort((a, b) => (a.name_ent < b.name_ent ? -1 : 1));
      await this.getData();
    } catch (err) {
      this.globalService.exceptionHandling(err);
    } finally {
    }
  }

  ngAfterContentChecked(): void {
    this.isValid();
  }

  onMove(str) {
    let idx = this.selectedActivityts.findIndex((item) => item.id === this.id);
    if (str === 'next') {
      let id = this.selectedActivityts[idx + 1].id;
      let url = this.router.url;
      let arr = url.split('_');
      arr[arr.length - 1] = String(id);
      url = arr.join('_');
      this.router.navigate([url]);
    } else if (str === 'previous') {
      let id = this.selectedActivityts[idx - 1].id;
      let url = this.router.url;
      let arr = url.split('_');
      arr[arr.length - 1] = String(id);
      url = arr.join('_');
      this.router.navigate([url]);
    }
  }

  async getData() {
    this.spinnerService.show();
    this.activitytForm.reset();
    let selectedPersonal;
    let selectedPersonalt;
    let _selectedPersonalt;
    try {
      selectedPersonal = this.personaltService.getBycode(
        this.selectedPersonalcode
      );
      _selectedPersonalt = this.personaltService.getOneById(
        this.selectedPersonalcode,
        true
      );
      selectedPersonalt = await _selectedPersonalt;
      this.selectedRepressts = isNullOrUndefined(selectedPersonalt['repress_t'])
        ? []
        : selectedPersonalt['repress_t'];
      this.selectedActivityts = isNullOrUndefined(
        selectedPersonalt['activity_t']
      )
        ? []
        : selectedPersonalt['activity_t'];
    } catch (err) {
      this.globalService.exceptionHandling(err);
    }
    switch (this.intType) {
      case 'editdoc': // редактирование
        this.first = false;
        this.last = false;
        let idx = this.selectedActivityts.findIndex(
          (item) => item.id === this.id
        );
        if (idx === 0) {
          this.first = true;
        }
        if (idx === this.selectedActivityts.length - 1) {
          this.last = true;
        }
        try {
          this.myControl_human_code.length = 0;
          this.coacttForms.length = 0;
          let _selectedActivityt = this.activitytService.getOneById(this.id);
          let actspheret = this.actspheretService.getByActid(this.id);
          let selectedActivityt = await _selectedActivityt;
          this.actspheret = isNullOrUndefined(await actspheret)
            ? []
            : await actspheret;

          this.actspheretInForm = [];
          this.actspheret.forEach((actspheret) => {
            let sphere = this.spheres.find(
              (item) => item.id === actspheret.sphere_id
            );
            if (!isNullOrUndefined(sphere)) {
              this.actspheretInForm.push(sphere);
            }
          });
          this.actspheretInForm.sort((a, b) => {
            if (a.name_ent < b.name_ent) {
              return -1;
            }
            if (a.name_ent > b.name_ent) {
              return 1;
            }
            return 0;
          });
          this.activitytForm.patchValue({
            id: selectedActivityt.id,
            personal_code: selectedActivityt.personal_code,
            act_no: selectedActivityt.act_no,
            sphere: this.actspheretInForm,
            // sphere: selectedActivityt.sphere,
            prof: selectedActivityt.prof,
            post: selectedActivityt.post,
            // arrival_dat: selectedActivityt.arrival_dat,
            arrival_dat_begin: selectedActivityt.arrival_dat_begin,
            arrival_dat_end: selectedActivityt.arrival_dat_end,
            // depart_dat: selectedActivityt.depart_dat,
            depart_dat_begin: selectedActivityt.depart_dat_begin,
            depart_dat_end: selectedActivityt.depart_dat_end,
            sphere_rmk: selectedActivityt.sphere_rmk,
            prof_rmk: selectedActivityt.prof_rmk,
            ent1_rmk: selectedActivityt.ent1_rmk,
            post_rmk: selectedActivityt.post_rmk,
            arrival_dat_rmk: selectedActivityt.arrival_dat_rmk,
            depart_dat_rmk: selectedActivityt.depart_dat_rmk,
            general_rmk: selectedActivityt.general_rmk,
            ent_code: selectedActivityt.ent_code,
            // geoplace_code: selectedActivityt.geoplace_code,
            place_num: selectedActivityt.place_num,
            geoplace_code_rmk: selectedActivityt.geoplace_code_rmk,
            // range_mark: selectedActivityt.range_mark,
            // rep_mark: selectedActivityt.rep_mark,
            rep_id: selectedActivityt.rep_id,
          });
          if (!isNullOrUndefined(selectedActivityt.place_num)) {
            let _geoplace_code = await this.placetService.getByPlace_num(
              selectedActivityt.place_num
            );
            this.myControl_geoplace_code.reset();
            this.myControl_geoplace_code.setValue(_geoplace_code[0]);
          }
          this.dateparseService.setSourDate(
            this.activitytForm,
            'arrival_dat_begin_end',
            selectedActivityt.arrival_dat_begin,
            selectedActivityt.arrival_dat_end
          );
          this.dateparseService.setSourDate(
            this.activitytForm,
            'depart_dat_begin_end',
            selectedActivityt.depart_dat_begin,
            selectedActivityt.depart_dat_end
          );

          // this.myControl_geoplace_code.setValue({ place_code: selectedActivityt.geoplace_code });
          let _selectedCoactts = this.coacttService.getByActid(
            selectedActivityt.id
          );
          let selectedCoactts = isNullOrUndefined(await _selectedCoactts)
            ? []
            : await _selectedCoactts;
          for (let j = 0; j < selectedCoactts.length; j++) {
            this.coacttForms.push(
              new FormGroup({
                id: new FormControl(
                  { value: null, disabled: this.viewOnly },
                  []
                ),
                org_id: new FormControl(
                  { value: null, disabled: this.viewOnly },
                  []
                ),
                human_code: new FormControl(
                  { value: null, disabled: this.viewOnly },
                  [Validators.required]
                ),
                role: new FormControl(
                  { value: null, disabled: this.viewOnly },
                  []
                ),
                role_rmk: new FormControl(
                  { value: null, disabled: this.viewOnly },
                  []
                ),
                sign_mark: new FormControl(
                  { value: null, disabled: this.viewOnly },
                  []
                ),
              })
            );
            this.myControl_human_code.push(
              new FormControl({ value: null, disabled: this.viewOnly })
            );
            this.coacttForms[j].patchValue({
              id: selectedCoactts[j].id,
              act_id: selectedCoactts[j].act_id,
              human_code: selectedCoactts[j].human_code,
              role: selectedCoactts[j].role,
              role_rmk: selectedCoactts[j].role_rmk,
              sign_mark: selectedCoactts[j].sign_mark,
            });
            if (!isNullOrUndefined(selectedCoactts[j].human_code)) {
              let person = await this.personaltService.getBycode(
                selectedCoactts[j].human_code
              );
              this.myControl_human_code[j].setValue(person[0]);
            }
          }
        } catch (err) {
          this.globalService.exceptionHandling(err);
        }
        break;
      case 'newdoc': // создание
        this.last = true;
        this.first = true;
        this.myControl_human_code.length = 0;
        this.coacttForms.length = 0;
        this.myControl_geoplace_code.setValue(null);
        this.myControl_geoplace_code.reset();
        try {
          let _activityts = this.activitytService.getByPersonalcode(
            this.selectedPersonalcode
          );
          let activityts = isNullOrUndefined(await _activityts)
            ? []
            : await _activityts;
          let num: number;
          if (activityts.length === 0) {
            num = 1;
          } else {
            activityts.sort((a, b) => b.act_no - a.act_no);
            num = activityts[0].act_no + 1;
          }
          this.activitytForm.patchValue({
            act_no: this.newnom || num,
          });
        } catch (err) {
          this.globalService.exceptionHandling(err);
        }
        break;
      default:
    }
    // this.selectedPersonal = await _selectedPersonalt;
    this.selectedPersonal = (await selectedPersonal)[0];
    this.spinnerService.hide();
  }

  addCoactt() {
    this.coacttForms.push(
      new FormGroup({
        act_id: new FormControl(null, []),
        human_code: new FormControl(null, [Validators.required]),
        role: new FormControl(null, []),
        role_rmk: new FormControl(null, []),
        sign_mark: new FormControl(null, []),
      })
    );
    this.myControl_human_code.push(new FormControl(null));
  }

  delCoactt(idx) {
    if (!isNullOrUndefined(this.coacttForms[idx].value.id)) {
      this.coacttidTodel.push(this.coacttForms[idx].value.id);
    }
    this.coacttForms.splice(idx, 1);
    this.myControl_human_code.splice(idx, 1);
  }

  async onDel() {
    let dialogRef = this.dialog.open(DialogYesNoComponent, {
      data: {
        title: 'Удалить деятельность?',
        question: ``,
      },
    });
    dialogRef.afterClosed().subscribe(async (result) => {
      if (result === true) {
        this.spinnerService.show();
        try {
          let _coactts = this.coacttService.getByActid(this.id);
          let coactts = isNullOrUndefined(await _coactts) ? [] : await _coactts;
          await Promise.all(
            coactts.map(async (coactt) => {
              return this.coacttService.deleteOneById(coactt.id);
            })
          );
          await this.activitytService.deleteOneById(this.id);
          this.router.navigate([
            `/sys/person/personaltedit/${this.selectedPersonalcode}`,
          ]);
          this.openSnackBar(`Удалено.`, 'Ok');
        } catch (err) {
          console.error(`Ошибка при удалении ${err}`);
          this.openSnackBar(`Ошибка ${err.status}`, 'Ok');
        } finally {
          this.spinnerService.hide();
        }
      }
    });
  }
  /**
   * Функция возвращает true, если бли внесены изменения
   */
  hasChanges(): boolean {
    let res = false;
    if (this.activitytForm.dirty || this.myControl_geoplace_code.dirty) {
      res = true;
    }
    this.coacttForms.map((form) => {
      if (form.dirty) {
        res = true;
      }
    });
    this.myControl_human_code.map((control) => {
      if (control.dirty) {
        res = true;
      }
    });
    return res;
  }

  async changePubStatus() {
    const isPublished = this.selectedPersonal.published === '*';
    if (isPublished) {
      const curDate = new Date().toISOString().split('T')[0];
      this.selectedPersonal.mustPublish = '*';
      this.selectedPersonal.mustUnpublish = null;
      this.selectedPersonal.pub_change_date = curDate;




      await this.personaltService.putOneById(
        this.selectedPersonalcode,
        this.selectedPersonal
      );
    }
  }

  /**
   * Функция сохраняет форму. Silent для показа сообщения. Возвращает true в случаеуспешного сохранения
   */
  async onSaveForm(silent = false, addnew = false) {
    let res = false;
    try {
      // if (
      //   !isNullOrUndefined(this.activitytForm.getRawValue()['arrival_dat']) &&
      //   this.activitytForm.getRawValue()['arrival_dat'] !== ''
      // ) {
      //   if (
      //     !(
      //       new Date(this.activitytForm.getRawValue()['arrival_dat']) instanceof
      //         Date &&
      //       !isNaN(
      //         new Date(
      //           this.activitytForm.getRawValue()['arrival_dat']
      //         ).valueOf()
      //       )
      //     )
      //   ) {
      //     this.openSnackBar(`Введена не существующая дате вступления`, 'Ok');
      //     return false;
      //   }

      // } else {
      //   this.activitytForm.patchValue({
      //     arrival_dat: null,
      //   });
      // }
      // if (
      //   !isNullOrUndefined(this.activitytForm.getRawValue()['depart_dat']) &&
      //   this.activitytForm.getRawValue()['depart_dat'] !== ''
      // ) {
      //   if (
      //     !(
      //       new Date(this.activitytForm.getRawValue()['depart_dat']) instanceof
      //         Date &&
      //       !isNaN(
      //         new Date(this.activitytForm.getRawValue()['depart_dat']).valueOf()
      //       )
      //     )
      //   ) {
      //     this.openSnackBar(`Введена не существующая дате увольнения`, 'Ok');
      //     return false;
      //   }
      // } else {
      //   this.activitytForm.patchValue({
      //     depart_dat: null,
      //   });
      // }
      if (
        !isNullOrUndefined(this.myControl_geoplace_code.value) &&
        this.myControl_geoplace_code.value !== ''
      ) {
        if (!isNullOrUndefined(this.myControl_geoplace_code.value.place_num)) {
          this.activitytForm.patchValue({
            place_num: this.myControl_geoplace_code.value.place_num,
          });
        } else {
          let dialogRef = this.dialog.open(DialogYesNoComponent, {
            data: {
              title: 'Введено не корректное место жительства.',
              question: `Продолжить?`,
            },
          });
          await new Promise((resolve, reject) => {
            dialogRef.afterClosed().subscribe(async (result) => {
              if (result === true) {
                this.myControl_geoplace_code.reset();
                this.activitytForm.patchValue({
                  place_num: null,
                });
                resolve(null);
              } else {
                return false;
              }
            });
          });
          this.myControl_geoplace_code.reset();
          this.activitytForm.patchValue({
            place_num: null,
          });
        }
      } else {
        this.myControl_geoplace_code.reset();
        this.activitytForm.patchValue({
          place_num: null,
        });
      }
      this.spinnerService.show();
      switch (this.intType) {
        case 'editdoc': // редактирование документа
          /**Удаляем дочерние записи, удаленные из формы*/
          await Promise.all(
            this.coacttidTodel.map((id) => {
              return this.coacttService.deleteOneById(id);
            })
          );
          /**Сохранение дочерних сущностей */
          for (let j = 0; j < this.coacttForms.length; j++) {
            if (
              !isNullOrUndefined(this.myControl_human_code[j].value) &&
              !isNullOrUndefined(this.myControl_human_code[j].value.code)
            ) {
              if (!isNullOrUndefined(this.myControl_human_code[j].value)) {
                this.coacttForms[j].patchValue({
                  human_code: this.myControl_human_code[j].value.code,
                });
              }
              if (
                this.coacttForms[j].value['sign_mark'] === true ||
                this.coacttForms[j].value['sign_mark'] === '*'
              ) {
                this.coacttForms[j].patchValue({
                  sign_mark: '*',
                });
              } else {
                this.coacttForms[j].patchValue({
                  sign_mark: null,
                });
              }
              this.coacttForms[j].patchValue({
                act_id: this.id,
              });
              if (isNullOrUndefined(this.coacttForms[j].value.id)) {
                this.coacttForms[j].removeControl('id');
                await this.coacttService.postOne(this.coacttForms[j].value);
              } else {
                if (
                  this.coacttForms[j].touched ||
                  this.myControl_human_code[j].touched
                ) {
                  await this.coacttService.putOneById(
                    this.coacttForms[j].value.id,
                    this.coacttForms[j].value
                  );
                }
              }
            } else {
              this.openSnackBar(
                `Не верно указано лицо, упоминаемое в связи с организацией #${
                  j + 1
                }`,
                'Ok'
              );
              return false;
            }
          }
          // if (!isNullOrUndefined(this.myControl_geoplace_code.value)) {
          // 	this.activitytForm.patchValue({
          // 		'place_num': this.myControl_geoplace_code.value.place_num
          // 	});
          // }
          // есть ли изменения в списке сфер деятельности
          let actspheretToDel = [];
          if (!isNullOrUndefined(this.actspheret)) {
            this.actspheret.forEach((actspheret) => {
              let sphereInForm = this.activitytForm
                .getRawValue()
                .sphere.find((item) => item.id === actspheret.sphere_id);
              if (isNullOrUndefined(sphereInForm)) {
                let res = this.actspheretService.deleteOneById(actspheret.id);
                actspheretToDel.push(res);
              }
            });
          }
          let actspheretToPost = [];
          if (!isNullOrUndefined(this.activitytForm.getRawValue().sphere)) {
            this.activitytForm.getRawValue().sphere.forEach((sphereInForm) => {
              let actspheret = this.actspheret.find(
                (item) => item.sphere_id === sphereInForm.id
              );
              if (isNullOrUndefined(actspheret)) {
                let obj = { act_id: this.id, sphere_id: sphereInForm.id };
                let res = this.actspheretService.postOne(obj);
                actspheretToPost.push(res);
              }
            });
          }
          await Promise.all(actspheretToDel);
          await Promise.all(actspheretToPost);
          let dataPut = { ...this.activitytForm.getRawValue() };
          delete dataPut.sphere;
          await this.activitytService.putOneById(this.id, dataPut);
          await this.changePubStatus();
          break;
        case 'newdoc': // создание документа
          this.activitytForm.patchValue({
            personal_code: this.selectedPersonalcode,
          });
          this.activitytForm.removeControl('id');
          // if (!isNullOrUndefined(this.myControl_geoplace_code.value)) {
          // 	this.activitytForm.patchValue({
          // 		'place_num': this.myControl_geoplace_code.value.place_num
          // 	});
          // }
          let dataPost = { ...this.activitytForm.getRawValue() };
          delete dataPost.sphere;
          if (this.newnom) {
            dataPost['newnom'] = this.newnom;
            delete dataPost.act_no;
          }
          let postForm = await this.activitytService.postOne(dataPost);
          this.id = +postForm.id;
          this.intType = 'editdoc';
          let actspheretToAdd = [];
          if (!isNullOrUndefined(this.activitytForm.getRawValue().sphere)) {
            this.activitytForm.getRawValue().sphere.forEach((sphereInForm) => {
              let obj = { act_id: this.id, sphere_id: sphereInForm.id };
              let res = this.actspheretService.postOne(obj);
              actspheretToAdd.push(res);
            });
          }
          await Promise.all(actspheretToAdd);
          /**Сохранение дочерних сущностей */
          for (let j = 0; j < this.coacttForms.length; j++) {
            if (
              !isNullOrUndefined(this.myControl_human_code[j].value) &&
              !isNullOrUndefined(this.myControl_human_code[j].value.code)
            ) {
              if (!isNullOrUndefined(this.myControl_human_code[j].value)) {
                this.coacttForms[j].patchValue({
                  human_code: this.myControl_human_code[j].value.code,
                });
              }
              if (
                this.coacttForms[j].value['sign_mark'] === true ||
                this.coacttForms[j].value['sign_mark'] === '*'
              ) {
                this.coacttForms[j].patchValue({
                  sign_mark: '*',
                });
              } else {
                this.coacttForms[j].patchValue({
                  sign_mark: null,
                });
              }
              this.coacttForms[j].patchValue({
                act_id: this.id,
              });
              this.coacttForms[j].removeControl('id');
              await this.coacttService.postOne(this.coacttForms[j].value);
            } else {
              this.openSnackBar(
                `Не верно указано лицо, упоминаемое в связи с организацией #${
                  j + 1
                }`,
                'Ok'
              );
              return false;
            }
          }
          this.router.navigate([
            `sys/person/activitytedit/edit_${this.selectedPersonalcode}_${this.id}`,
          ]);
          break;
        default:
      }
      if (addnew) {
        this.newnom = this.activitytForm.getRawValue().act_no + 1;
      } else {
        this.newnom = null;
      }
      if (addnew === true) {
        this.router.navigate([
          `sys/person/activitytedit/new_${this.selectedPersonalcode}`,
        ]);
        this.intType = 'newdoc';
      }
      await this.getData();

      if (!silent) {
        this.openSnackBar(`Сохранено.`, 'Ok');
      }
      res = true;
    } catch (err) {
      if (err.status === 401) {
        console.error(`Ошибка авторизации`);
        this.authService.logout();
      } else if (err.status === 409) {
        this.activitytForm.controls['act_no'].setErrors({ incorrect: true });
        this.openSnackBar(`${err.error['error']}`, 'Ok');
      } else {
        console.error(`Ошибка ${err}`);
        this.openSnackBar(`Ошибка ${err.status}`, 'Ok');
      }
    } finally {
      this.spinnerService.hide();
      return res;
    }
  }

  async goBack() {
    let str = '/sys/person/personaltedit/';
    str += this.viewOnly ? 'v_' : '';
    str += `${this.selectedPersonalcode}`;
    let saved = true;
    if (this.hasChanges() && this.valid) {
      let dialogRef = this.dialog.open(DialogSaveNoComponent, {
        data: {
          title: 'Сохранить изменения?',
          question: ``,
        },
      });
      dialogRef.afterClosed().subscribe(async (result) => {
        if (result === true) {
          saved = await this.onSaveForm(true);
          if (saved === true) {
            this.router.navigate([str]);
            // this.router.navigate([`/sys/person/personaltedit/${this.selectedPersonalcode}`]);
          }
        } else if (result === false) {
          this.router.navigate([str]);
          // this.router.navigate([`/sys/person/personaltedit/${this.selectedPersonalcode}`]);
        } else {
          return;
        }
      });
    } else if (this.hasChanges() && !this.valid) {
      let dialogRef = this.dialog.open(DialogYesNoComponent, {
        data: {
          title: 'Изменения не будут сохранены!',
          question: `Не заполнены обязательные поля. Уйти со страницы?`,
        },
      });
      dialogRef.afterClosed().subscribe(async (result) => {
        if (result === true) {
          this.router.navigate([str]);
          // this.router.navigate([`/sys/person/personaltedit/${this.selectedPersonalcode}`]);
        } else {
          return;
        }
      });
    } else {
      this.router.navigate([str]);
      // this.router.navigate([`/sys/person/personaltedit/${this.selectedPersonalcode}`]);
    }
  }

  isValid() {
    let res = this.activitytForm.valid;
    this.myControl_human_code.map((control) => {
      if (
        !(
          typeof control.value === 'object' && !isNullOrUndefined(control.value)
        )
      ) {
        res = false;
      }
    });
    this.valid = res;
  }

  inputLink_geoplace_code() {
    clearTimeout(this.timerId);
    this.timerId = setTimeout(async () => {
      this.placets = await this.placetService.getWithFilterLimit(
        100,
        this.myControl_geoplace_code.value
      );
      if (!isNullOrUndefined(this.placets)) {
        try {
          this.filteredPlacets = this.myControl_geoplace_code.valueChanges.pipe(
            startWith<string | PlaceT>(''),
            map((value) => {
              if (!isNullOrUndefined(value)) {
                return typeof value === 'string' ? value : value.place_name;
              }
            }),
            map((name) => (name ? this._filter(name) : [...this.placets]))
          );
        } catch {}
      }
    }, 500);
  }

  private _filter(name: string): PlaceT[] {
    const filterValue = name.toLowerCase();
    return isNullOrUndefined(this.placets)
      ? null
      : this.placets.filter(
          (placet) =>
            placet.place_name.toLowerCase().indexOf(filterValue) !== -1
        );
  }

  displayFn_residence_code(place?: PlaceT): string | undefined {
    return place
      ? place.place_num + '. ' + place.place_code + '. ' + place.place_name
      : undefined;
  }

  inputLink_human_code(j) {
    clearTimeout(this.timerId);
    this.timerId = setTimeout(async () => {
      this.personalts = await this.personaltService.getWithShortfilterLimit(
        100,
        this.myControl_human_code[j].value
      );
      if (!isNullOrUndefined(this.personalts)) {
        try {
          this.filteredPersonalts = (<FormControl>(
            this.myControl_human_code[j]
          )).valueChanges.pipe(
            startWith<string | PersonalT>(''),
            map((value) => (typeof value === 'string' ? value : value.surname)),
            map((name) =>
              name ? this._filterPersonalt(name) : [...this.personalts]
            )
          );
        } catch {}
      }
    }, 500);
  }

  private _filterPersonalt(name: string): PersonalT[] {
    const filterValue = name.toLowerCase();
    return isNullOrUndefined(this.personalts)
      ? null
      : this.personalts
          .filter(
            (personalt) =>
              !isNullOrUndefined(personalt.surname) && personalt.surname !== ''
          )
          .filter(
            (personalt) =>
              personalt.surname.toLowerCase().indexOf(filterValue) !== -1
          );
    // this.personalts.filter(personalt => personalt.surname.toLowerCase().indexOf(filterValue) !== -1);
  }

  displayFn_human_code(personal?: PersonalT): string | undefined {
    // if (!personal) {
    //   return undefined;
    // }
    // let str = '';
    // str += personal.code + '. ';
    // str += (!isNullOrUndefined(personal.surname)) ? personal.surname + ' ' : '';
    // str += (!isNullOrUndefined(personal.fname)) ? personal.fname + ' ' : '';
    // str += (!isNullOrUndefined(personal.lname)) ? personal.lname : '';
    // return personal ? str : undefined;
    if (!personal) {
      return undefined;
    }
    let str = '';
    str += personal.code + '. ';
    str += !isNullOrUndefined(personal.surname) ? personal.surname + ' ' : '';
    str += !isNullOrUndefined(personal.fname) ? personal.fname + ' ' : '';
    str += !isNullOrUndefined(personal.lname) ? personal.lname + ' ' : '';
    // if (!isNullOrUndefined(personal.birth)) {
    //   str += (!isNullOrUndefined(personal.birth)) ? new Date(personal.birth).toLocaleDateString() : '';
    // } else {
    //   str += (!isNullOrUndefined(personal.birth_rmk)) ? personal.birth_rmk : '';
    // }
    return personal ? str : undefined;
  }

  // resetArrival_dat() {
  //   this.activitytForm.patchValue({
  //     arrival_dat: null,
  //   });
  // }

  // resetDepart_dat() {
  //   this.activitytForm.patchValue({
  //     depart_dat: null,
  //   });
  // }

  onAddPersonalT(idx) {
    this.filteredPersonalts = null;
    let dialogRef = this.dialog.open(DialogPersonalEditComponent, {
      panelClass: 'app-dialog-extend',
    });
    dialogRef.afterClosed().subscribe(async (result) => {
      if (result !== false) {
        let res = await this.personaltService.postOne(result);
        this.myControl_human_code[idx].setValue(res);
      }
    });
  }

  onLinkPersonalT(id) {
    let str = this.viewOnly
      ? `/sys/person/personaltedit/v_${id}`
      : `/sys/person/personaltedit/${id}`;
    this.router.navigate([str]);
  }

  getRepress_typesById(id: number) {
    let repress_type = this.repress_types.find((item) => item.id === id);
    return isNullOrUndefined(repress_type) ? '' : repress_type.name_ent;
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
    });
  }
}
