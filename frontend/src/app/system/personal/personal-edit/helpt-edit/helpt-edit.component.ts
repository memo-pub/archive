import {
  Component,
  OnInit,
  HostListener,
  OnDestroy,
  ViewEncapsulation,
} from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ListOfT } from 'src/app/shared/models/listoft.model';
import { isNullOrUndefined } from 'util';
import { AuthService } from 'src/app/shared/services/auth.service';
import { ListoftService } from 'src/app/system/shared/services/listoft.service';
import { HelptService } from 'src/app/system/shared/services/helpt.service';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DialogYesNoComponent } from 'src/app/system/shared/components/dialog-yes-no/dialog-yes-no.component';
import { DialogSaveNoComponent } from 'src/app/system/shared/components/dialog-save-no/dialog-save-no.component';
import { GlobalService } from 'src/app/shared/services/global.service';
import { PersonaltService } from 'src/app/system/shared/services/personalt.service';
import { PersonalT } from 'src/app/shared/models/personalt.model';
import { helpT_width } from 'src/app/shared/models/helpt.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-helpt-edit',
  templateUrl: './helpt-edit.component.html',
  styleUrls: ['./helpt-edit.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class HelptEditComponent implements OnInit, OnDestroy {
  helpT_width = helpT_width;

  timerId;
  id: number;
  intType: string | any = ''; // тип интерфейса
  viewOnly: boolean;
  selectedPersonalcode: number;
  selectedPersonal: PersonalT;

  help_types: ListOfT[];

  helptForm: FormGroup;

  breadcrumbs = [];
  activeRouterObserver: Subscription;

  @HostListener('window:keydown', ['$event'])
  onKeyPress(event: KeyboardEvent) {
    if ((event.ctrlKey || event.metaKey) && event.keyCode == 83) {
      event.preventDefault();
      if (
        !this.viewOnly &&
        !this.helptForm.invalid &&
        this.intType !== 'myprofile'
      ) {
        this.onSaveForm();
      }
    }
  }

  constructor(
    private authService: AuthService,
    private globalService: GlobalService,
    private listoftService: ListoftService,
    private personaltService: PersonaltService,
    private helptService: HelptService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private spinnerService: NgxSpinnerService,
    public dialog: MatDialog,
    public snackBar: MatSnackBar
  ) {
    this.activeRouterObserver = this.activatedRoute.params.subscribe(
      (param) => {
        let str = param.id.split('_');
        if (str[0] === 'new') {
          this.intType = 'newdoc';
          this.selectedPersonalcode = +str[1];
          this.viewOnly = false;
        } else {
          if (str[1] === 'v') {
            this.id = +str[3];
            this.selectedPersonalcode = +str[2];
            this.intType = 'editdoc';
            this.viewOnly = true;
          } else {
            this.id = +str[2];
            this.selectedPersonalcode = +str[1];
            this.intType = 'editdoc';
            this.viewOnly = false;
          }
        }
        this.viewOnly =
          this.viewOnly === false
            ? this.authService.getCurUser().usertype === 4
            : true;
      }
    );
    this.globalService.routerObserver = router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        if (val.url.indexOf('helptedit') !== -1) {
          this.globalService.breadcrumbs.length = 0;
          this.globalService.breadcrumbs.push(
            {
              label: 'персоналии',
              url:
                this.viewOnly === true
                  ? `/sys/person/personaltedit/v_${this.selectedPersonalcode}`
                  : `/sys/person/personaltedit/${this.selectedPersonalcode}`,
            },
            { label: 'сведения о помощи' }
          );
          this.breadcrumbs = this.globalService.breadcrumbs;
        }
      }
    });
  }

  ngOnDestroy(): void {
    this.globalService.routerObserver.unsubscribe();
    // this.activeRouterObserver.unsubscribe();
  }

  async ngOnInit() {
    this.helptForm = new FormGroup({
      id: new FormControl({ value: null, disabled: this.viewOnly }, []),
      personal_code: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      help_no: new FormControl({ value: null, disabled: true }, [
        Validators.required,
      ]),
      help_typ: new FormControl({ value: null, disabled: this.viewOnly }, []),
      help_typ_id: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      help_typ_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      help_dat: new FormControl({ value: null, disabled: this.viewOnly }, []),
      whom: new FormControl({ value: null, disabled: this.viewOnly }, []),
      whom_rmk: new FormControl({ value: null, disabled: this.viewOnly }, []),
    });
    try {
      let help_types = this.listoftService.getWithFilter('HELP_TYP');
      this.help_types = isNullOrUndefined(await help_types)
        ? []
        : await help_types;
      this.help_types.sort((a, b) => (a.name_ent < b.name_ent ? -1 : 1));
      await this.getData();
    } catch (err) {
      this.globalService.exceptionHandling(err);
    } finally {
    }
  }

  async getData() {
    this.spinnerService.show();
    let selectedPersonal;
    try {
      selectedPersonal = this.personaltService.getBycode(
        this.selectedPersonalcode
      );
    } catch (err) {
      this.globalService.exceptionHandling(err);
    }
    switch (this.intType) {
      case 'editdoc': // редактирование
        try {
          let selectedHelpt = await this.helptService.getOneById(this.id);
          this.helptForm.reset();
          this.helptForm.patchValue({
            id: selectedHelpt.id,
            personal_code: selectedHelpt.personal_code,
            help_no: selectedHelpt.help_no,
            help_typ: selectedHelpt.help_typ,
            help_typ_id: selectedHelpt.help_typ_id,
            help_typ_rmk: selectedHelpt.help_typ_rmk,
            help_dat: selectedHelpt.help_dat,
            whom: selectedHelpt.whom,
            whom_rmk: selectedHelpt.whom_rmk,
          });
        } catch (err) {
          this.globalService.exceptionHandling(err);
        } finally {
          this.spinnerService.hide();
        }
        break;
      case 'newdoc': // создание
        try {
          let _helps = this.helptService.getByPersonalcode(
            this.selectedPersonalcode
          );
          let helps = isNullOrUndefined(await _helps) ? [] : await _helps;
          let num: number;
          if (helps.length === 0) {
            num = 1;
          } else {
            helps.sort((a, b) => b.help_no - a.help_no);
            num = helps[0].help_no + 1;
          }
          this.helptForm.reset();
          this.helptForm.patchValue({
            help_no: num,
          });
        } catch (err) {
          this.globalService.exceptionHandling(err);
        } finally {
          this.spinnerService.hide();
        }
        break;
      default:
    }
    this.selectedPersonal = (await selectedPersonal)[0];
    this.spinnerService.hide();
  }

  async onDel() {
    let dialogRef = this.dialog.open(DialogYesNoComponent, {
      data: {
        title: 'Удалить сведения о помощи?',
        question: ``,
      },
    });
    dialogRef.afterClosed().subscribe(async (result) => {
      if (result === true) {
        this.spinnerService.show();
        try {
          await this.helptService.deleteOneById(this.id);
          this.router.navigate([
            `/sys/person/personaltedit/${this.selectedPersonalcode}`,
          ]);
          this.openSnackBar(`Удалено.`, 'Ok');
        } catch (err) {
          console.error(`Ошибка при удалении ${err}`);
          this.openSnackBar(`Ошибка ${err.status}`, 'Ok');
        } finally {
          this.spinnerService.hide();
        }
      }
    });
  }

  /**
   * Функция возвращает true, если бли внесены изменения
   */
  hasChanges(): boolean {
    let res = false;
    if (this.helptForm.dirty) {
      res = true;
    }
    return res;
  }

  /**
   * Функция сохраняет форму. Silent для показа сообщения. Возвращает true в случаеуспешного сохранения
   */
  async onSaveForm(silent = false) {
    let res = false;
    this.spinnerService.show();
    try {
      if (
        !isNullOrUndefined(this.helptForm.getRawValue()['help_dat']) &&
        this.helptForm.getRawValue()['help_dat'] !== ''
      ) {
        if (
          !(
            new Date(this.helptForm.getRawValue()['help_dat']) instanceof
              Date &&
            !isNaN(new Date(this.helptForm.getRawValue()['help_dat']).valueOf())
          )
        ) {
          this.openSnackBar(`Введена не существующая дата помощи`, 'Ok');
          return false;
        }
        // let help_dat = new Date(this.helptForm.getRawValue()['help_dat']);
        // this.helptForm.patchValue({
        // 	'help_dat': help_dat.toDateString()
        // });
      } else {
        this.helptForm.patchValue({
          help_dat: null,
        });
      }
      switch (this.intType) {
        case 'editdoc': // редактирование документа
          await this.helptService.putOneById(
            this.id,
            this.helptForm.getRawValue()
          );
          break;
        case 'newdoc': // создание документа
          this.helptForm.patchValue({
            personal_code: this.selectedPersonalcode,
          });
          this.helptForm.removeControl('id');
          let postForm = await this.helptService.postOne(
            this.helptForm.getRawValue()
          );
          this.id = +postForm.id;
          this.intType = 'editdoc';
          break;
        default:
      }
      await this.getData();
      if (!silent) {
        this.openSnackBar(`Сохранено.`, 'Ok');
      }
      res = true;
    } catch (err) {
      if (err.status === 401) {
        console.error(`Ошибка авторизации`);
        this.authService.logout();
      } else if (err.status === 409) {
        this.helptForm.controls['help_no'].setErrors({ incorrect: true });
        this.openSnackBar(`${err.error['error']}`, 'Ok');
      } else {
        console.error(`Ошибка ${err}`);
        this.openSnackBar(`Ошибка ${err.status}`, 'Ok');
      }
    } finally {
      this.spinnerService.hide();
      return res;
    }
  }

  async goBack() {
    let str = '/sys/person/personaltedit/';
    str += this.viewOnly ? 'v_' : '';
    str += `${this.selectedPersonalcode}`;
    let saved = true;
    if (this.hasChanges() && this.helptForm.valid) {
      let dialogRef = this.dialog.open(DialogSaveNoComponent, {
        data: {
          title: 'Сохранить изменения?',
          question: ``,
        },
      });
      dialogRef.afterClosed().subscribe(async (result) => {
        if (result === true) {
          saved = await this.onSaveForm(true);
          if (saved === true) {
            this.router.navigate([str]);
            // this.router.navigate([`/sys/person/personaltedit/${this.selectedPersonalcode}`]);
          }
        } else if (result === false) {
          this.router.navigate([str]);
          // this.router.navigate([`/sys/person/personaltedit/${this.selectedPersonalcode}`]);
        } else {
          return;
        }
      });
    } else if (this.hasChanges() && this.helptForm.valid) {
      let dialogRef = this.dialog.open(DialogYesNoComponent, {
        data: {
          title: 'Изменения не будут сохранены!',
          question: `Не заполнены обязательные поля. Уйти со страницы?`,
        },
      });
      dialogRef.afterClosed().subscribe(async (result) => {
        if (result === true) {
          this.router.navigate([str]);
          // this.router.navigate([`/sys/person/personaltedit/${this.selectedPersonalcode}`]);
        } else {
          return;
        }
      });
    } else {
      this.router.navigate([str]);
      // this.router.navigate([`/sys/person/personaltedit/${this.selectedPersonalcode}`]);
    }
  }

  resetHelp_dat() {
    this.helptForm.patchValue({
      help_dat: null,
    });
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
    });
  }
}
