import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormGroup, FormControl } from '@angular/forms';
import { GlobalService } from 'src/app/shared/services/global.service';
import { isNullOrUndefined } from 'util';
import { personalT_width } from 'src/app/shared/models/personalt.model';
import { DateparseService } from 'src/app/shared/services/dateparse.service';

@Component({
  selector: 'app-dialog-personal-edit',
  templateUrl: './dialog-personal-edit.component.html',
  styleUrls: ['./dialog-personal-edit.component.css'],
})
export class DialogPersonalEditComponent implements OnInit {
  personalT_width = personalT_width;

  title = 'Добавление персоналии';

  personaltForm: FormGroup;
  sex: any[];

  constructor(
    private globalService: GlobalService,
    private snackBar: MatSnackBar,
    public dialogRef: MatDialogRef<DialogPersonalEditComponent>,
    public dateparseService: DateparseService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.personaltForm = new FormGroup({
      mark: new FormControl(null, []),
      type_r: new FormControl(null, []),
      type_i: new FormControl(null, []),
      type_a: new FormControl(null, []),
      surname: new FormControl(null, []),
      fname: new FormControl(null, []),
      lname: new FormControl(null, []),
      sex: new FormControl(null, []),
      // birth: new FormControl(null, []),
      birth_begin: new FormControl({ value: null, disabled: false }, []),
      birth_end: new FormControl({ value: null, disabled: false }, []),
      birth_begin_end: new FormControl({ value: null, disabled: false }, []),
      birth_rmk: new FormControl(null, []),
      surname_rmk: new FormControl(null, []),
      published: new FormControl({ value: null, disabled: false }, []),
    });
  }

  ngOnInit() {
    this.sex = this.globalService.sex;
  }

  resetBirth() {
    this.personaltForm.patchValue({
      birth: '',
    });
  }

  onSave() {
    // if (
    //   !isNullOrUndefined(this.personaltForm.value['birth']) &&
    //   this.personaltForm.value['birth'] !== ''
    // ) {
    //   if (
    //     !(
    //       new Date(this.personaltForm.value['birth']) instanceof Date &&
    //       !isNaN(new Date(this.personaltForm.value['birth']).valueOf())
    //     )
    //   ) {
    //     this.openSnackBar(`Введена несуществующая дата рождения`, 'Ok');
    //     return;
    //   }
    // } else {
    //   this.personaltForm.patchValue({
    //     birth: null,
    //   });
    // }
    if (this.personaltForm.value['mark'] === true) {
      this.personaltForm.patchValue({
        mark: '*',
      });
    } else {
      this.personaltForm.patchValue({
        mark: null,
      });
    }
    if (this.personaltForm.value['type_r'] === true) {
      this.personaltForm.patchValue({
        type_r: '*',
      });
    } else {
      this.personaltForm.patchValue({
        type_r: null,
      });
    }
    if (this.personaltForm.value['type_i'] === true) {
      this.personaltForm.patchValue({
        type_i: '*',
      });
    } else {
      this.personaltForm.patchValue({
        type_i: null,
      });
    }
    if (this.personaltForm.value['type_a'] === true) {
      this.personaltForm.patchValue({
        type_a: '*',
      });
    } else {
      this.personaltForm.patchValue({
        type_a: null,
      });
    }
    this.dialogRef.close(this.personaltForm.getRawValue());
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
    });
  }
}
