import {
  Component,
  OnInit,
  AfterContentChecked,
  HostListener,
  OnDestroy,
  ViewEncapsulation,
} from '@angular/core';
import { AuthService } from 'src/app/shared/services/auth.service';
import { PersonaltService } from 'src/app/system/shared/services/personalt.service';
import { ListoftService } from 'src/app/system/shared/services/listoft.service';
import { OrgtService } from 'src/app/system/shared/services/orgt.service';
import { CoorgtService } from 'src/app/system/shared/services/coorgt.service';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ListOfT } from 'src/app/shared/models/listoft.model';
import { isNullOrUndefined } from 'util';
import { PersonalT } from 'src/app/shared/models/personalt.model';
import { Observable, Subscription } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { DialogYesNoComponent } from 'src/app/system/shared/components/dialog-yes-no/dialog-yes-no.component';
import { DialogSaveNoComponent } from 'src/app/system/shared/components/dialog-save-no/dialog-save-no.component';
import { GlobalService } from 'src/app/shared/services/global.service';
import { DialogPersonalEditComponent } from '../dialog-personal-edit/dialog-personal-edit.component';
import { coorgT_width } from 'src/app/shared/models/coorgt.model';
import { orgT_width } from 'src/app/shared/models/orgt.model';
import { DateparseService } from 'src/app/shared/services/dateparse.service';

@Component({
  selector: 'app-orgt-edit',
  templateUrl: './orgt-edit.component.html',
  styleUrls: ['./orgt-edit.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class OrgtEditComponent
  implements OnInit, AfterContentChecked, OnDestroy {
  coorgT_width = coorgT_width;
  orgT_width = orgT_width;

  timerId;
  id: number;
  intType: string | any = ''; // тип интерфейса
  viewOnly: boolean;
  selectedPersonalcode: number;
  selectedPersonal: PersonalT;

  org_codes: ListOfT[];
  org_names: ListOfT[];
  particips: ListOfT[];
  personalts: PersonalT[];
  filteredPersonalts: Observable<PersonalT[]>;
  filteredOrg_names: Observable<ListOfT[]>;

  coorgidTodel: number[] = [];

  orgtForm: FormGroup;
  coorgtForms: FormGroup[] = [];

  myControl_human_code: FormControl[] = [];
  myControl_org_names: FormControl;

  valid: boolean;

  breadcrumbs = [];
  activeRouterObserver: Subscription;

  @HostListener('window:keydown', ['$event'])
  onKeyPress(event: KeyboardEvent) {
    if ((event.ctrlKey || event.metaKey) && event.keyCode == 83) {
      event.preventDefault();
      if (!this.viewOnly && this.valid && this.intType !== 'myprofile') {
        this.onSaveForm();
      }
    }
  }

  constructor(
    public dateparseService: DateparseService,
    private authService: AuthService,
    private globalService: GlobalService,
    private personaltService: PersonaltService,
    private listoftService: ListoftService,
    private orgtService: OrgtService,
    private coorgtService: CoorgtService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private spinnerService: NgxSpinnerService,
    public dialog: MatDialog,
    public snackBar: MatSnackBar
  ) {
    this.activeRouterObserver = this.activatedRoute.params.subscribe(
      (param) => {
        let str = param.id.split('_');
        if (str[0] === 'new') {
          this.intType = 'newdoc';
          this.selectedPersonalcode = +str[1];
          this.viewOnly = false;
        } else {
          if (str[1] === 'v') {
            this.id = +str[3];
            this.selectedPersonalcode = +str[2];
            this.intType = 'editdoc';
            this.viewOnly = true;
          } else {
            this.id = +str[2];
            this.selectedPersonalcode = +str[1];
            this.intType = 'editdoc';
            this.viewOnly = false;
          }
        }
        this.viewOnly =
          this.viewOnly === false
            ? this.authService.getCurUser().usertype === 4
            : true;
      }
    );
    this.globalService.routerObserver = router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        if (val.url.indexOf('orgtedit') !== -1) {
          this.globalService.breadcrumbs.length = 0;
          this.globalService.breadcrumbs.push(
            {
              label: 'персоналии',
              url:
                this.viewOnly === true
                  ? `/sys/person/personaltedit/v_${this.selectedPersonalcode}`
                  : `/sys/person/personaltedit/${this.selectedPersonalcode}`,
            },
            { label: 'участие в общественных организациях' }
          );
          this.breadcrumbs = this.globalService.breadcrumbs;
        }
      }
    });
  }

  ngOnDestroy(): void {
    this.globalService.routerObserver.unsubscribe();
    // this.activeRouterObserver.unsubscribe();
  }

  async ngOnInit() {
    this.orgtForm = new FormGroup({
      id: new FormControl({ value: null, disabled: this.viewOnly }, []),
      personal_code: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      org_no: new FormControl({ value: null, disabled: true }, [
        Validators.required,
      ]),
      org_code: new FormControl({ value: null, disabled: this.viewOnly }, []),
      org_code_id: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      org_name: new FormControl({ value: null, disabled: this.viewOnly }, []),
      // join_dat: new FormControl({ value: null, disabled: true }, []),
      join_dat_begin: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      join_dat_end: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      join_dat_begin_end: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      // dismis_dat: new FormControl({ value: null, disabled: true }, []),
      dismis_dat_begin: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      dismis_dat_end: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      dismis_dat_begin_end: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      particip: new FormControl({ value: null, disabled: this.viewOnly }, []),
      particip_id: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      com_rmk: new FormControl({ value: null, disabled: this.viewOnly }, []),
      ent_name_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      join_dat_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      dismis_dat_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      particip_rmk: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
    });
    this.myControl_org_names = new FormControl({
      value: null,
      disabled: this.viewOnly,
    });
    try {
      let org_codes = this.listoftService.getWithFilter('ORG_CODE');
      let org_names = this.listoftService.getWithFilter('ORG_NAME');
      let particips = this.listoftService.getWithFilter('PARTICIP');
      this.org_codes = isNullOrUndefined(await org_codes)
        ? []
        : await org_codes;
      this.org_names = isNullOrUndefined(await org_names)
        ? []
        : await org_names;
      this.particips = isNullOrUndefined(await particips)
        ? []
        : await particips;
      this.org_codes.sort((a, b) => (a.name_ent < b.name_ent ? -1 : 1));
      this.org_names.sort((a, b) => (a.name_ent < b.name_ent ? -1 : 1));
      this.filteredOrg_names = this.myControl_org_names.valueChanges.pipe(
        startWith(''),
        map((org_name) =>
          org_name ? this._filter(org_name) : [...this.org_names]
        )
      );
      await this.getData();
    } catch (err) {
      this.globalService.exceptionHandling(err);
    }
  }

  ngAfterContentChecked(): void {
    this.isValid();
  }

  async getData() {
    this.spinnerService.show();
    let selectedPersonal;
    try {
      selectedPersonal = this.personaltService.getBycode(
        this.selectedPersonalcode
      );
    } catch (err) {
      this.globalService.exceptionHandling(err);
    }
    switch (this.intType) {
      case 'editdoc': // редактирование
        try {
          this.myControl_human_code.length = 0;
          this.coorgtForms.length = 0;
          let selectedOrgt = await this.orgtService.getOneById(this.id);
          this.orgtForm.reset();
          this.orgtForm.patchValue({
            id: selectedOrgt.id,
            personal_code: selectedOrgt.personal_code,
            org_no: selectedOrgt.org_no,
            org_code: selectedOrgt.org_code,
            org_code_id: selectedOrgt.org_code_id,
            org_name: selectedOrgt.org_name,
            // join_dat: selectedOrgt.join_dat,
            join_dat_begin: selectedOrgt.join_dat_begin,
            join_dat_end: selectedOrgt.join_dat_end,
            // dismis_dat: selectedOrgt.dismis_dat,
            dismis_dat_begin: selectedOrgt.dismis_dat_begin,
            dismis_dat_end: selectedOrgt.dismis_dat_end,
            particip: selectedOrgt.particip,
            particip_id: selectedOrgt.particip_id,
            com_rmk: selectedOrgt.com_rmk,
            ent_name_rmk: selectedOrgt.ent_name_rmk,
            join_dat_rmk: selectedOrgt.join_dat_rmk,
            dismis_dat_rmk: selectedOrgt.dismis_dat_rmk,
            particip_rmk: selectedOrgt.particip_rmk,
          });
          this.dateparseService.setSourDate(
            this.orgtForm,
            'join_dat_begin_end',
            selectedOrgt.join_dat_begin,
            selectedOrgt.join_dat_end
          );
          this.dateparseService.setSourDate(
            this.orgtForm,
            'dismis_dat_begin_end',
            selectedOrgt.dismis_dat_begin,
            selectedOrgt.dismis_dat_end
          );
          this.myControl_org_names.reset();
          this.myControl_org_names.setValue(selectedOrgt.org_name);
          let _selectedCoOrgts = this.coorgtService.getByOrgid(selectedOrgt.id);
          let selectedCoOrgts = isNullOrUndefined(await _selectedCoOrgts)
            ? []
            : await _selectedCoOrgts;
          for (let j = 0; j < selectedCoOrgts.length; j++) {
            this.coorgtForms.push(
              new FormGroup({
                id: new FormControl(
                  { value: null, disabled: this.viewOnly },
                  []
                ),
                org_id: new FormControl(
                  { value: null, disabled: this.viewOnly },
                  []
                ),
                human_code: new FormControl(
                  { value: null, disabled: this.viewOnly },
                  [Validators.required]
                ),
                role: new FormControl(
                  { value: null, disabled: this.viewOnly },
                  []
                ),
                role_rmk: new FormControl(
                  { value: null, disabled: this.viewOnly },
                  []
                ),
                sign_mark: new FormControl(
                  { value: null, disabled: this.viewOnly },
                  []
                ),
              })
            );
            this.myControl_human_code.push(
              new FormControl({ value: null, disabled: this.viewOnly })
            );
            this.coorgtForms[j].patchValue({
              id: selectedCoOrgts[j].id,
              org_id: selectedCoOrgts[j].org_id,
              human_code: selectedCoOrgts[j].human_code,
              role: selectedCoOrgts[j].role,
              role_rmk: selectedCoOrgts[j].role_rmk,
              sign_mark: selectedCoOrgts[j].sign_mark,
            });
            if (!isNullOrUndefined(selectedCoOrgts[j].human_code)) {
              let person = await this.personaltService.getBycode(
                selectedCoOrgts[j].human_code
              );
              if (!isNullOrUndefined(person) && (await person.length) > 0) {
                this.myControl_human_code[j].setValue(person[0]);
              }
            }
          }
        } catch (err) {
          this.globalService.exceptionHandling(err);
        } finally {
          this.spinnerService.hide();
        }
        break;
      case 'newdoc': // создание
        try {
          this.myControl_org_names.setValue(null);
          this.coorgtForms.length = 0;
          let _orgs = this.orgtService.getByPersonalcode(
            this.selectedPersonalcode
          );
          let orgs = isNullOrUndefined(await _orgs) ? [] : await _orgs;
          let num: number;
          if (orgs.length === 0) {
            num = 1;
          } else {
            orgs.sort((a, b) => b.org_no - a.org_no);
            num = orgs[0].org_no + 1;
          }
          this.orgtForm.reset();
          this.orgtForm.patchValue({
            org_no: num,
          });
        } catch (err) {
          this.globalService.exceptionHandling(err);
        } finally {
          this.spinnerService.hide();
        }
        break;
      default:
    }
    this.selectedPersonal = (await selectedPersonal)[0];
    this.spinnerService.hide();
  }

  addCoorgt() {
    this.coorgtForms.push(
      new FormGroup({
        org_id: new FormControl(null, []),
        human_code: new FormControl(null, [Validators.required]),
        role: new FormControl(null, []),
        role_rmk: new FormControl(null, []),
        sign_mark: new FormControl(null, []),
      })
    );
    this.myControl_human_code.push(new FormControl(null));
  }

  delCoorgt(idx) {
    if (!isNullOrUndefined(this.coorgtForms[idx].value.id)) {
      this.coorgidTodel.push(this.coorgtForms[idx].value.id);
    }
    this.coorgtForms.splice(idx, 1);
    this.myControl_human_code.splice(idx, 1);
  }

  async onDel() {
    let dialogRef = this.dialog.open(DialogYesNoComponent, {
      data: {
        title: 'Удалить организацию?',
        question: ``,
      },
    });
    dialogRef.afterClosed().subscribe(async (result) => {
      if (result === true) {
        this.spinnerService.show();
        try {
          let _coorgts = this.coorgtService.getByOrgid(this.id);
          let coorgts = isNullOrUndefined(await _coorgts) ? [] : await _coorgts;
          await Promise.all(
            coorgts.map(async (coorgt) => {
              return this.coorgtService.deleteOneById(coorgt.id);
            })
          );
          await this.orgtService.deleteOneById(this.id);
          this.router.navigate([
            `/sys/person/personaltedit/${this.selectedPersonalcode}`,
          ]);
          this.openSnackBar(`Удалено.`, 'Ok');
        } catch (err) {
          console.error(`Ошибка при удалении ${err}`);
          this.globalService.exceptionHandling(err);
        } finally {
          this.spinnerService.hide();
        }
      }
    });
  }

  /**
   * Функция возвращает true, если бли внесены изменения
   */
  hasChanges(): boolean {
    let res = false;
    if (this.orgtForm.dirty || this.myControl_org_names.dirty) {
      res = true;
    }
    this.coorgtForms.map((form) => {
      if (form.dirty) {
        res = true;
      }
    });
    this.myControl_human_code.map((control) => {
      if (control.dirty) {
        res = true;
      }
    });
    return res;
  }

  async changePubStatus() {
    const isPublished = this.selectedPersonal.published === '*';

    if (isPublished) {
      const curDate = new Date().toISOString().split('T')[0];
      this.selectedPersonal.mustPublish = '*';
      this.selectedPersonal.mustUnpublish = null;
      this.selectedPersonal.pub_change_date = curDate;

      await this.personaltService.putOneById(
        this.selectedPersonalcode,
        this.selectedPersonal
      );
    }
  }

  /**
   * Функция сохраняет форму. Silent для показа сообщения. Возвращает true в случаеуспешного сохранения
   */
  async onSaveForm(silent = false, addnew = false) {
    let res = false;
    this.spinnerService.show();
    try {
      if (!isNullOrUndefined(this.myControl_org_names.value)) {
        // фикс null при создании формы
        this.orgtForm.patchValue({
          org_name: this.myControl_org_names.value,
        });
      }
      // if (
      //   !isNullOrUndefined(this.orgtForm.getRawValue()['join_dat']) &&
      //   this.orgtForm.getRawValue()['join_dat'] !== ''
      // ) {
      //   if (
      //     !(
      //       new Date(this.orgtForm.getRawValue()['join_dat']) instanceof Date &&
      //       !isNaN(new Date(this.orgtForm.getRawValue()['join_dat']).valueOf())
      //     )
      //   ) {
      //     this.openSnackBar(`Введена не существующая дата вступления`, 'Ok');
      //     return false;
      //   }

      // } else {
      //   this.orgtForm.patchValue({
      //     join_dat: null,
      //   });
      // }
      // if (
      //   !isNullOrUndefined(this.orgtForm.getRawValue()['dismis_dat']) &&
      //   this.orgtForm.getRawValue()['dismis_dat'] !== ''
      // ) {
      //   if (
      //     !(
      //       new Date(this.orgtForm.getRawValue()['dismis_dat']) instanceof
      //         Date &&
      //       !isNaN(
      //         new Date(this.orgtForm.getRawValue()['dismis_dat']).valueOf()
      //       )
      //     )
      //   ) {
      //     this.openSnackBar(`Введена не существующая дате выхода`, 'Ok');
      //     return false;
      //   }

      // } else {
      //   this.orgtForm.patchValue({
      //     dismis_dat: null,
      //   });
      // }
      switch (this.intType) {
        case 'editdoc': // редактирование документа
          /**Удаляем дочерние записи, удаленные из формы*/
          await Promise.all(
            this.coorgidTodel.map((id) => {
              return this.coorgtService.deleteOneById(id);
            })
          );
          /**Сохранение дочерних сущностей */
          for (let j = 0; j < this.coorgtForms.length; j++) {
            if (
              !isNullOrUndefined(this.myControl_human_code[j].value) &&
              !isNullOrUndefined(this.myControl_human_code[j].value.code)
            ) {
              if (!isNullOrUndefined(this.myControl_human_code[j].value)) {
                this.coorgtForms[j].patchValue({
                  human_code: this.myControl_human_code[j].value.code,
                });
              }
              if (
                this.coorgtForms[j].value['sign_mark'] === true ||
                this.coorgtForms[j].value['sign_mark'] === '*'
              ) {
                this.coorgtForms[j].patchValue({
                  sign_mark: '*',
                });
              } else {
                this.coorgtForms[j].patchValue({
                  sign_mark: null,
                });
              }
              this.coorgtForms[j].patchValue({
                org_id: this.id,
              });
              if (isNullOrUndefined(this.coorgtForms[j].value.id)) {
                this.coorgtForms[j].removeControl('id');
                await this.coorgtService.postOne(this.coorgtForms[j].value);
              } else {
                if (
                  this.coorgtForms[j].touched ||
                  this.myControl_human_code[j].touched
                ) {
                  await this.coorgtService.putOneById(
                    this.coorgtForms[j].value.id,
                    this.coorgtForms[j].value
                  );
                }
              }
            } else {
              this.openSnackBar(
                `Не верно указано лицо, упоминаемое в связи в c учебой или работой #${
                  j + 1
                }`,
                'Ok'
              );
              return false;
            }
          }
          await this.orgtService.putOneById(
            this.id,
            this.orgtForm.getRawValue()
          );
          await this.changePubStatus();
          break;
        case 'newdoc': // создание документа
          this.orgtForm.patchValue({
            personal_code: this.selectedPersonalcode,
          });
          this.orgtForm.removeControl('id');
          let postForm = await this.orgtService.postOne(
            this.orgtForm.getRawValue()
          );
          this.id = +postForm.id;
          this.intType = 'editdoc';
          /**Сохранение дочерних сущностей */
          for (let j = 0; j < this.coorgtForms.length; j++) {
            if (
              !isNullOrUndefined(this.myControl_human_code[j].value) &&
              !isNullOrUndefined(this.myControl_human_code[j].value.code)
            ) {
              if (!isNullOrUndefined(this.myControl_human_code[j].value)) {
                this.coorgtForms[j].patchValue({
                  human_code: this.myControl_human_code[j].value.code,
                });
              }
              if (
                this.coorgtForms[j].value['sign_mark'] === true ||
                this.coorgtForms[j].value['sign_mark'] === '*'
              ) {
                this.coorgtForms[j].patchValue({
                  sign_mark: '*',
                });
              } else {
                this.coorgtForms[j].patchValue({
                  sign_mark: null,
                });
              }
              this.coorgtForms[j].patchValue({
                org_id: this.id,
              });
              this.coorgtForms[j].removeControl('id');
              await this.coorgtService.postOne(this.coorgtForms[j].value);
            } else {
              this.openSnackBar(
                `Не верно указано лицо, упоминаемое в связи в c учебой или работой #${
                  j + 1
                }`,
                'Ok'
              );
              return false;
            }
          }
          this.router.navigate([
            `sys/person/orgtedit/edit_${this.selectedPersonalcode}_${this.id}`,
          ]);
          break;
        default:
      }
      if (addnew === true) {
        this.router.navigate([
          `sys/person/orgtedit/new_${this.selectedPersonalcode}`,
        ]);
        this.intType = 'newdoc';
      }
      await this.getData();
      if (!silent) {
        this.openSnackBar(`Сохранено.`, 'Ok');
      }
      res = true;
    } catch (err) {
      if (err.status === 401) {
        console.error(`Ошибка авторизации`);
        this.authService.logout();
      } else if (err.status === 409) {
        this.orgtForm.controls['org_no'].setErrors({ incorrect: true });
        this.openSnackBar(`${err.error['error']}`, 'Ok');
      } else {
        console.error(`Ошибка ${err}`);
        this.openSnackBar(`Ошибка ${err.status}`, 'Ok');
      }
    } finally {
      this.spinnerService.hide();
      return res;
    }
  }

  async goBack() {
    let str = '/sys/person/personaltedit/';
    str += this.viewOnly ? 'v_' : '';
    str += `${this.selectedPersonalcode}`;
    let saved = true;
    if (this.hasChanges() && this.valid) {
      let dialogRef = this.dialog.open(DialogSaveNoComponent, {
        data: {
          title: 'Сохранить изменения?',
          question: ``,
        },
      });
      dialogRef.afterClosed().subscribe(async (result) => {
        if (result === true) {
          saved = await this.onSaveForm(true);
          if (saved === true) {
            this.router.navigate([str]);
            // this.router.navigate([`/sys/person/personaltedit/${this.selectedPersonalcode}`]);
          }
        } else if (result === false) {
          this.router.navigate([
            `/sys/person/personaltedit/${this.selectedPersonalcode}`,
          ]);
        } else {
          return;
        }
      });
    } else if (this.hasChanges() && !this.valid) {
      let dialogRef = this.dialog.open(DialogYesNoComponent, {
        data: {
          title: 'Изменения не будут сохранены!',
          question: `Не заполнены обязательные поля. Уйти со страницы?`,
        },
      });
      dialogRef.afterClosed().subscribe(async (result) => {
        if (result === true) {
          this.router.navigate([str]);
          // this.router.navigate([`/sys/person/personaltedit/${this.selectedPersonalcode}`]);
        } else {
          return;
        }
      });
    } else {
      this.router.navigate([str]);
      // this.router.navigate([`/sys/person/personaltedit/${this.selectedPersonalcode}`]);
    }
  }

  isValid() {
    let res = this.orgtForm.valid;
    this.myControl_human_code.map((control) => {
      if (
        !(
          typeof control.value === 'object' && !isNullOrUndefined(control.value)
        )
      ) {
        res = false;
      }
    });
    this.valid = res;
  }

  inputLink_human_code(j) {
    clearTimeout(this.timerId);
    this.timerId = setTimeout(async () => {
      this.personalts = await this.personaltService.getWithShortfilterLimit(
        100,
        this.myControl_human_code[j].value
      );
      if (!isNullOrUndefined(this.personalts)) {
        try {
          this.filteredPersonalts = (<FormControl>(
            this.myControl_human_code[j]
          )).valueChanges.pipe(
            startWith<string | PersonalT>(''),
            map((value) => (typeof value === 'string' ? value : value.surname)),
            map((name) =>
              name ? this._filterPersonalt(name) : [...this.personalts]
            )
          );
        } catch {}
      }
    }, 500);
  }

  private _filterPersonalt(name: string): PersonalT[] {
    const filterValue = name.toLowerCase();
    return isNullOrUndefined(this.personalts)
      ? null
      : this.personalts
          .filter(
            (personalt) =>
              !isNullOrUndefined(personalt.surname) && personalt.surname !== ''
          )
          .filter(
            (personalt) =>
              personalt.surname.toLowerCase().indexOf(filterValue) !== -1
          );
    // this.personalts.filter(personalt => personalt.surname.toLowerCase().indexOf(filterValue) !== -1);
  }

  displayFn_human_code(personal?: PersonalT): string | undefined {
    // if (!personal) {
    //   return undefined;
    // }
    // let str = '';
    // str += personal.code + '. ';
    // str += (!isNullOrUndefined(personal.surname)) ? personal.surname + ' ' : '';
    // str += (!isNullOrUndefined(personal.fname)) ? personal.fname + ' ' : '';
    // str += (!isNullOrUndefined(personal.lname)) ? personal.lname : '';
    // return personal ? str : undefined;
    if (!personal) {
      return undefined;
    }
    let str = '';
    str += personal.code + '. ';
    str += !isNullOrUndefined(personal.surname) ? personal.surname + ' ' : '';
    str += !isNullOrUndefined(personal.fname) ? personal.fname + ' ' : '';
    str += !isNullOrUndefined(personal.lname) ? personal.lname + ' ' : '';
    // if (!isNullOrUndefined(personal.birth)) {
    //   str += (!isNullOrUndefined(personal.birth)) ? new Date(personal.birth).toLocaleDateString() : '';
    // } else {
    //   str += (!isNullOrUndefined(personal.birth_rmk)) ? personal.birth_rmk : '';
    // }
    return personal ? str : undefined;
  }

  private _filter(value: string) {
    // const filterValue = value.toLowerCase();
    return this.org_names;
    // return this.org_names.filter(org_name => org_name.name_ent.toLowerCase().indexOf(filterValue) === 0);
  }

  // resetDismis_dat() {
  //   this.orgtForm.patchValue({
  //     dismis_dat: null,
  //   });
  // }

  // resetJoin_dat() {
  //   this.orgtForm.patchValue({
  //     join_dat: null,
  //   });
  // }

  onAddPersonalT(idx) {
    this.filteredPersonalts = null;
    let dialogRef = this.dialog.open(DialogPersonalEditComponent, {
      panelClass: 'app-dialog-extend',
    });
    dialogRef.afterClosed().subscribe(async (result) => {
      if (result !== false) {
        let res = await this.personaltService.postOne(result);
        this.myControl_human_code[idx].setValue(res);
      }
    });
  }

  onLinkPersonalT(id) {
    let str = this.viewOnly
      ? `/sys/person/personaltedit/v_${id}`
      : `/sys/person/personaltedit/${id}`;
    this.router.navigate([str]);
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
    });
  }
}
