import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { archivefolder_width } from 'src/app/shared/models/archivefolder.model';

@Component({
  selector: 'app-dialog-marchive-edit',
  templateUrl: './dialog-marchive-edit.component.html',
  styleUrls: ['./dialog-marchive-edit.component.css']
})
export class DialogMarchiveEditComponent implements OnInit {
  archivefolder_width = archivefolder_width;

  title = 'Добавление дела';
  marchiveForm: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<DialogMarchiveEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {
    this.marchiveForm = new FormGroup({
      name: new FormControl(this.data.form.name, []),
      page_cnt: new FormControl(0, [Validators.required]),
      opis_id: new FormControl(this.data.form.opis_id, [Validators.required]),
      delo: new FormControl(this.data.form.delo, [Validators.required]),
      spec_page_cnt: new FormControl(0, [Validators.required]),
      doc_cnt: new FormControl(0, [Validators.required]),
      foto_cnt: new FormControl(0, [Validators.required]),
      have_scanned: new FormControl(false, [Validators.required]),
      have_memories: new FormControl(false, [Validators.required]),
      have_arts: new FormControl(false, [Validators.required]),
      have_act: new FormControl(false, [Validators.required]),
      status: new FormControl(0, [Validators.required])
    });
  }

  onSave() {
    this.dialogRef.close(this.marchiveForm.getRawValue());
  }
}
