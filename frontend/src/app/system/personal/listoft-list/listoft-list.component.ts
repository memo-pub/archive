import { Component, OnInit, HostListener, ViewChild } from '@angular/core';
import { ListOfT } from 'src/app/shared/models/listoft.model';
import { Muser } from 'src/app/shared/models/muser.model';
import { PageEvent, MatPaginator } from '@angular/material/paginator';
import { ListoftService } from '../../shared/services/listoft.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { AuthService } from 'src/app/shared/services/auth.service';
import { MuserService } from '../../shared/services/muser.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { isNullOrUndefined } from 'util';
import { DeathreasontService } from '../../shared/services/deathreasont.service';
import { MatDialog } from '@angular/material/dialog';
import { DialogTwoSelectComponent } from '../../shared/components/dialog-two-select/dialog-two-select.component';
import { SeeknamelistoftService } from '../../shared/services/seeknamelistoft.service';

@Component({
  selector: 'app-listoft-list',
  templateUrl: './listoft-list.component.html',
  styleUrls: ['./listoft-list.component.css'],
})
export class ListoftListComponent implements OnInit {
  loggedInUser: Muser;
  searchStr = '';
  listofts: ListOfT[];
  name_f: any[];

  // MatPaginator Inputs
  length = 100;
  pageSize = 100;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  pageEvent: PageEvent;
  start = 0;
  end = 0;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  selectedIndex = 0;

  aditStr = 'Редактировать';

  /**Для пагинатора */
  page = 1;
  limit = 100;

  postfixId = '<i class="fa fa-sort-amount-asc" aria-hidden="true"></i>';
  postfixName = '';
  postfixFullname = '';

  searchOrder = 'byid';

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.aditStr =
      event.target.innerWidth < 700
        ? '<i class="fa fa-pencil" aria-hidden="true"></i>'
        : 'Редактировать';
  }

  constructor(
    public authService: AuthService,
    private muserService: MuserService,
    private globalService: GlobalService,
    private listoftService: ListoftService,
    private seeknamelistoftService: SeeknamelistoftService,
    public dialog: MatDialog,
    private deathreasontService: DeathreasontService,
    private router: Router,
    private spinnerService: NgxSpinnerService
  ) {}

  async ngOnInit() {
    this.selectedIndex = isNullOrUndefined(this.globalService.listoftIndex)
      ? 0
      : this.globalService.listoftIndex;
    this.name_f = this.globalService.name_f;
    this.aditStr =
      window.screen.width < 700
        ? '<i class="fa fa-pencil" aria-hidden="true"></i>'
        : 'Редактировать';
    try {
      let loggedInUser = this.muserService.getOneById(
        this.authService.getCurUser().id
      );
      let listofts;
      if (this.selectedIndex === 18) {
        listofts = this.deathreasontService.getAll();
      } else {
        listofts = this.listoftService.getWithFilter(
          this.name_f[this.selectedIndex].key
        );
      }
      this.listofts = isNullOrUndefined(await listofts) ? [] : await listofts;
      this.length = this.listofts.length;
      this.loggedInUser = await loggedInUser;
      if (this.length > 100) {
        this.pageSizeOptions.push(this.length);
      }
      this.end = this.pageSize;
    } catch (err) {
      this.globalService.exceptionHandling(err);
    } finally {
      this.spinnerService.hide();
      this.selectOrder('set');
    }
  }

  updateData() {
    if (this.searchStr.length > 0) {
      this.start = 0;
      // this.end = this.paginator.pageSize;
      this.end = this.limit;
      // this.paginator.firstPage();
      this.page = 1;
    } else {
      // this.start = this.paginator.pageSize * this.paginator.pageIndex;
      // this.end = this.start + this.paginator.pageSize;
      this.start = this.limit * (this.page - 1);
      this.end = this.start + this.limit;
    }
  }

  setPageSizeOptions(event: PageEvent) {
    this.start = event.pageSize * event.pageIndex;
    this.end = this.start + event.pageSize;
    return event;
  }

  onAddListoft() {
    this.router.navigate([
      '/sys/person/listoftedit/newdoc-' + this.selectedIndex,
    ]);
  }

  onSeekListoft() {
    let dialogRef = this.dialog.open(DialogTwoSelectComponent, {
      data: {
        title: 'Объединить позиции?',
        question: ``,
        list1: this.listofts,
        list2: this.listofts,
      },
    });
    dialogRef.afterClosed().subscribe(async (result) => {
      if (!isNullOrUndefined(result)) {
        this.spinnerService.show();
        try {
          await this.seeknamelistoftService.seeknamelistoft(
            result.stay_id,
            result.del_id
          );
        } catch (err) {
          this.globalService.exceptionHandling(err);
        } finally {
          await this.selectTab(null);
          this.spinnerService.hide();
        }
      }
    });
  }

  onEditListoft(id) {
    this.router.navigate([
      `/sys/person/listoftedit/${id}-${this.selectedIndex}`,
    ]);
  }

  async selectTab(event) {
    if (this.selectedIndex === 18) {
      this.globalService.listoftIndex = this.selectedIndex;
      let listofts = this.deathreasontService.getAll();
      this.listofts = isNullOrUndefined(await listofts) ? [] : await listofts;
      this.length = this.listofts.length;
    } else {
      this.globalService.listoftIndex = this.selectedIndex;
      let listofts = this.listoftService.getWithFilter(
        this.name_f[this.selectedIndex].key
      );
      this.listofts = isNullOrUndefined(await listofts) ? [] : await listofts;
      this.length = this.listofts.length;
    }
    this.selectOrder('set');
  }

  goToPage(n: number): void {
    this.page = n;
    this.updatePage();
  }

  onNext(): void {
    this.page++;
    this.updatePage();
  }

  onPrev(): void {
    this.page = 1;
    this.updatePage();
  }

  onNext10(n = 1): void {
    this.page += 10 * n;
    this.updatePage();
  }

  onPrev10(n = 1): void {
    this.page -= 10 * n;
    this.updatePage();
  }

  async updatePage() {
    this.start = this.limit * (this.page - 1);
    this.end = this.start + this.limit;
  }

  async selectOrder(type: string) {
    switch (type) {
      case 'byid':
        this.searchOrder = this.searchOrder === 'byid' ? 'byiddesc' : 'byid';
        break;
      case 'byname_ent':
        this.searchOrder =
          this.searchOrder === 'byname_ent' ? 'byname_entdesc' : 'byname_ent';
        break;
      case 'byname_rmk':
        this.searchOrder =
          this.searchOrder === 'byname_rmk' ? 'byname_rmkdesc' : 'byname_rmk';
        break;
    }
    switch (this.searchOrder) {
      case 'byid':
        this.listofts.sort((a, b) => a.id - b.id);
        break;
      case 'byiddesc':
        this.listofts.sort((a, b) => b.id - a.id);
        break;
      case 'byname_ent':
        if (this.selectedIndex === 18) {
          this.listofts.sort((a, b) => (a.death_type > b.death_type ? 1 : -1));
        } else {
          this.listofts.sort((a, b) => (a.name_ent > b.name_ent ? 1 : -1));
        }
        break;
      case 'byname_entdesc':
        if (this.selectedIndex === 18) {
          this.listofts.sort((a, b) => (a.death_type < b.death_type ? 1 : -1));
        } else {
          this.listofts.sort((a, b) => (a.name_ent < b.name_ent ? 1 : -1));
        }
        break;
      case 'byname_rmk':
        if (this.selectedIndex === 18) {
          this.listofts.sort((a, b) => {
            if (a.death_rmk === b.death_rmk) {
              return 0;
            } else {
              return a.death_rmk > b.death_rmk ? 1 : -1;
            }
          });
        } else {
          // console.log(this.listofts);
          this.listofts.sort((a, b) => {
            if (a.name_rmk === b.name_rmk) {
              return 0;
            } else {
              return a.name_rmk > b.name_rmk ? 1 : -1;
            }
          });
        }
        break;
      case 'byname_rmkdesc':
        if (this.selectedIndex === 18) {
          this.listofts.sort((a, b) => {
            if (a.death_rmk === b.death_rmk) {
              return 0;
            } else {
              return a.death_rmk < b.death_rmk ? 1 : -1;
            }
          });
        } else {
          this.listofts.sort((a, b) => {
            if (a.name_rmk === b.name_rmk) {
              return 0;
            } else {
              return a.name_rmk < b.name_rmk ? 1 : -1;
            }
          });
        }
        break;
    }
    this.setOrgerStr();
  }

  setOrgerStr() {
    switch (this.searchOrder) {
      case 'byid':
        this.postfixId =
          '<i class="fa fa-sort-amount-asc" aria-hidden="true"></i>';
        this.postfixName = '';
        this.postfixFullname = '';
        break;
      case 'byiddesc':
        this.postfixId =
          '<i class="fa fa-sort-amount-desc" aria-hidden="true"></i>';
        this.postfixName = '';
        this.postfixFullname = '';
        break;
      case 'byname_ent':
        this.postfixId = '';
        this.postfixName =
          '<i class="fa fa-sort-amount-asc" aria-hidden="true"></i>';
        this.postfixFullname = '';
        break;
      case 'byname_entdesc':
        this.postfixId = '';
        this.postfixName =
          '<i class="fa fa-sort-amount-desc" aria-hidden="true"></i>';
        this.postfixFullname = '';
        break;
      case 'byname_rmk':
        this.postfixId = '';
        this.postfixName = '';
        this.postfixFullname =
          '<i class="fa fa-sort-amount-asc" aria-hidden="true"></i>';
        break;
      case 'byname_rmkdesc':
        this.postfixId = '';
        this.postfixName = '';
        this.postfixFullname =
          '<i class="fa fa-sort-amount-desc" aria-hidden="true"></i>';
        break;
    }
  }
}
