import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import {HttpClientModule} from '@angular/common/http';
import { MatDialogModule} from '@angular/material/dialog';
import { FieldjoinFilterPipe } from '../../shared/pipes/fieldjoin-filter.pipe';

import { StatEditComponent } from './stat-edit.component';

describe('StatEditComponent', () => {
  let component: StatEditComponent;
  let fixture: ComponentFixture<StatEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatEditComponent, FieldjoinFilterPipe ],
            imports: [RouterTestingModule, MatSnackBarModule, HttpClientTestingModule, MatDialogModule],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
