import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { AuthService } from 'src/app/shared/services/auth.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { DialogSetstringComponent } from '../../shared/components/dialog-setstring/dialog-setstring.component';
import { DialogYesNoComponent } from '../../shared/components/dialog-yes-no/dialog-yes-no.component';
import { FieldjoinService } from '../../shared/services/fieldjoin.service';

@Component({
  selector: 'app-stat-edit',
  templateUrl: './stat-edit.component.html',
  styleUrls: ['./stat-edit.component.css'],
})
export class StatEditComponent implements OnInit {
  id: number;
  fieldjoins: any[];
  start = 0;
  end = 0;
  page = 0;
  limit = 100;
  searchStr = '';

  constructor(
    private authService: AuthService,
    private fieldjoinService: FieldjoinService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private globalService: GlobalService,
    private spinnerService: NgxSpinnerService,
    public dialog: MatDialog,
    public snackBar: MatSnackBar
  ) {
    this.activatedRoute.params.subscribe((param) => {
      this.id = +param.id;
    });
  }

  ngOnInit() {
    this.getData();
    this.end = this.limit;
  }

  async getData() {
    this.spinnerService.show();
    try {
      let fieldjoins = this.fieldjoinService.getOneById(this.id);
      this.fieldjoins = !(await fieldjoins) ? [] : await fieldjoins;
    } catch (err) {
      if (err.status === 401) {
        console.error(`Ошибка авторизации`);
        this.authService.logout();
      } else {
        console.error(`Ошибка ${err}`);
      }
    } finally {
      this.spinnerService.hide();
    }
  }

  goToPage(n: number): void {
    this.page = n;
    this.updatePage();
  }

  onNext(): void {
    this.page++;
    this.updatePage();
  }

  onPrev(): void {
    this.page = 1;
    this.updatePage();
  }

  onNext10(n = 1): void {
    this.page += 10 * n;
    this.updatePage();
  }

  onPrev10(n = 1): void {
    this.page -= 10 * n;
    this.updatePage();
  }

  updatePage() {
    this.start = this.limit * (this.page - 1);
    this.end = this.start + this.limit;
  }

  onEdit(field) {
    let dialogRef = this.dialog.open(DialogSetstringComponent, {
      data: {
        str: '',
        savestr: 'Заменить',
      },
    });
    dialogRef.afterClosed().subscribe(async (result) => {
      if (typeof result === 'string') {
        let dialogRef1 = this.dialog.open(DialogYesNoComponent, {
          data: {
            title: `Уверенны ли вы, что хотите заменить все значения ${field.value} на значение ${result}?`,
          },
        });
        dialogRef1.afterClosed().subscribe(async (result1) => {
          if (result1 === true) {
            this.spinnerService.show();
            try {
              await this.fieldjoinService.putOneById(this.id, {
                exist_field: field.value,
                repl_field: result,
              });
            } catch (e) {
              this.globalService.exceptionHandling(e);
            } finally {
              this.getData();
            }
          }
        });
      }
    });
  }

  updateData() {
    if (this.searchStr.length > 0) {
      this.start = 0;
      this.end = this.limit;
      this.page = 1;
    } else {
      this.start = this.limit * (this.page - 1);
      this.end = this.start + this.limit;
    }
  }
}
