import { Component, OnInit } from '@angular/core';
import { Muser } from 'src/app/shared/models/muser.model';
import { PlaceT } from 'src/app/shared/models/placet.model';
import { AuthService } from 'src/app/shared/services/auth.service';
import { MuserService } from '../../shared/services/muser.service';
import { PlacetService } from '../../shared/services/placet.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { isNullOrUndefined } from 'util';
import { FiltersService } from '../../shared/services/filters.service';
import { GlobalService } from 'src/app/shared/services/global.service';

@Component({
  selector: 'app-placet-list',
  templateUrl: './placet-list.component.html',
  styleUrls: ['./placet-list.component.css'],
})
export class PlacetListComponent implements OnInit {
  loggedInUser: Muser;
  // searchStr = '';
  places: PlaceT[];

  // MatPaginator Inputs
  // length = 100;
  // pageSize = 100;
  // pageSizeOptions: number[] = [5, 10, 25, 100];
  // pageEvent: PageEvent;
  // @ViewChild(MatPaginator) paginator: MatPaginator;
  start = 0;
  end = 0;
  timerId;

  // aditStr = 'Редактировать';

  /**Для пагинатора */
  total = 0;
  // page = 1;
  limit = 100;

  // @HostListener('window:resize', ['$event'])
  // onResize(event) {
  //   this.aditStr =
  //     event.target.innerWidth < 700
  //       ? '<i class="fa fa-pencil" aria-hidden="true"></i>'
  //       : 'Редактировать';
  // }

  constructor(
    public filtersService: FiltersService,
    private authService: AuthService,
    private globalService: GlobalService,
    private muserService: MuserService,
    private placetService: PlacetService,
    private router: Router,
    private spinnerService: NgxSpinnerService
  ) {}

  async ngOnInit() {
    // this.aditStr =
    //   window.screen.width < 700
    //     ? '<i class="fa fa-pencil" aria-hidden="true"></i>'
    //     : 'Редактировать';
    try {
      let loggedInUser = this.muserService.getOneById(
        this.authService.getCurUser().id
      );
      this.loggedInUser = await loggedInUser;
      // let resCount = await this.placetService.getAll_count();
      // this.length = +resCount.rowcount;
      // this.total = +resCount.rowcount;
      this.end = this.limit;
      // let places = this.placetService.getWithOffsetLimit(0, this.end);
      // this.places = isNullOrUndefined(await places) ? [] : await places;
      this.getPlaces();
    } catch (err) {
      this.globalService.exceptionHandling(err);
    } finally {
      this.spinnerService.hide();
    }
  }

  updateData() {
    this.filtersService.placeTListFilter.page = 1;
    if (
      this.filtersService.placeTListFilter.searchStr.length > 0 &&
      this.filtersService.placeTListFilter.searchStr !== ''
    ) {
      this.start = 0;
      // this.end = this.paginator.pageSize;
      this.end = this.limit;
    } else {
      this.start = this.limit * (this.filtersService.placeTListFilter.page - 1);
      this.end = this.start + this.limit;
      // this.start = this.paginator.pageSize * this.paginator.pageIndex;
      // this.end = this.start + this.paginator.pageSize;
    }
    this.getPlaces();
  }

  async getPlaces() {
    // if (!isNullOrUndefined(this.pageEvent)) {
    //   this.pageEvent.pageIndex = 0;
    // }
    // this.page = 1;
    this.start = this.limit * (this.filtersService.placeTListFilter.page - 1);
    this.end = this.start + this.limit;
    try {
      this.spinnerService.show();
      this.places = [];
      let places;
      if (this.filtersService.placeTListFilter.searchStr !== '') {
        clearTimeout(this.timerId);
        this.timerId = setTimeout(async () => {
          places = this.placetService.getWithFilterOffsetLimit(
            this.start,
            this.end - this.start,
            this.filtersService.placeTListFilter.searchStr
          );
          this.total = (
            await this.placetService.getWithFilter_count(
              this.filtersService.placeTListFilter.searchStr
            )
          ).rowcount;
          this.places = isNullOrUndefined(await places) ? [] : await places;
        }, 500);
      } else {
        this.total = (await this.placetService.getAll_count()).rowcount;
        places = this.placetService.getWithOffsetLimit(
          this.start,
          this.end - this.start
        );
        this.places = isNullOrUndefined(await places) ? [] : await places;
      }
    } catch (err) {
      this.globalService.exceptionHandling(err);
    } finally {
      this.spinnerService.hide();
    }
  }

  // setPageSizeOptions(event: PageEvent) {
  //   this.start = event.pageSize * event.pageIndex;
  //   this.end = this.start + event.pageSize;
  //   this.getPlaces();
  //   return event;
  // }

  onAddPlace() {
    this.router.navigate(['/sys/person/placetedit/newdoc']);
  }

  onEditPlace(id) {
    this.router.navigate([`/sys/person/placetedit/${id}`]);
  }

  goToPage(n: number): void {
    this.filtersService.placeTListFilter.page = n;
    this.updatePage();
  }

  onNext(): void {
    this.filtersService.placeTListFilter.page++;
    this.updatePage();
  }

  onPrev(): void {
    this.filtersService.placeTListFilter.page = 1;
    this.updatePage();
  }

  onNext10(n = 1): void {
    this.filtersService.placeTListFilter.page += 10 * n;
    this.updatePage();
  }

  onPrev10(n = 1): void {
    this.filtersService.placeTListFilter.page -= 10 * n;
    this.updatePage();
  }

  async updatePage() {
    this.start = this.limit * (this.filtersService.placeTListFilter.page - 1);
    this.end = this.start + this.limit;
    this.getPlaces();
  }
}
