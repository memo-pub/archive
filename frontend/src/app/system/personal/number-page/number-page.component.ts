import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/shared/services/auth.service';
import { PersonaltService } from '../../shared/services/personalt.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { MatSnackBar } from '@angular/material/snack-bar';
import { GlobalService } from 'src/app/shared/services/global.service';

@Component({
  selector: 'app-number-page',
  templateUrl: './number-page.component.html',
  styleUrls: ['./number-page.component.css'],
})
export class NumberPageComponent implements OnInit {
  fond: number;
  opis: number;

  constructor(
    public authService: AuthService,
    private personaltService: PersonaltService,
    private globalService: GlobalService,
    private spinnerService: NgxSpinnerService,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit() {}

  async onNumbering() {
    this.spinnerService.show();
    try {
      let res = await this.personaltService.postNumbering(
        +this.fond,
        +this.opis
      );
      // console.log(res);
    } catch (err) {
      this.globalService.exceptionHandling(err);
    } finally {
      this.spinnerService.hide();
    }
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 10000,
    });
  }
}
