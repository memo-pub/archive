import {
  Component,
  OnInit,
  ElementRef,
  ViewChild,
  HostListener,
  OnDestroy,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { saveAs as importedSaveAs } from 'file-saver';
import { Observable, Subscription } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

import { ArchivefolderfileService } from './../../shared/services/archivefolderfile.service';
import {
  Archivefolder,
  archivefolder_width,
} from './../../../shared/models/archivefolder.model';
import { AuthService } from '../../../shared/services/auth.service';
import { ArchivefolderService } from '../../shared/services/archivefolder.service';
import { GlobalService } from '../../../shared/services/global.service';
import { Muser } from '../../../shared/models/muser.model';
import { DialogYesNoComponent } from '../../shared/components/dialog-yes-no/dialog-yes-no.component';
import { ArchivefolderlinkService } from '../../shared/services/archivefolderlink.service';
import { OpisService } from '../../shared/services/opis.service';
import { Opis } from '../../../shared/models/opis.model';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { Location } from '@angular/common';
import { DialogSetstringComponent } from '../../shared/components/dialog-setstring/dialog-setstring.component';
import { MuserService } from '../../shared/services/muser.service';
import { FiltersService } from '../../shared/services/filters.service';
import {
  LightboxImage,
  LightboxComponent,
} from '../../shared/components/lightbox/lightbox.component';
import { DialogNameslistComponent } from '../../shared/components/dialog-nameslist/dialog-nameslist.component';
import { UsagetService } from '../../shared/services/usaget.service';
import { UsageT } from 'src/app/shared/models/usaget.model';
import { ListOfT } from 'src/app/shared/models/listoft.model';
import { ListoftService } from '../../shared/services/listoft.service';
import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem,
} from '@angular/cdk/drag-drop';
import { MoveordernomService } from '../../shared/services/moveordernom.service';
import { SourtFilter } from 'src/app/shared/models/sourt.model';
import { DateparseService } from 'src/app/shared/services/dateparse.service';
import { MarchfilecopyService } from '../../shared/services/marchfilecopy.service';
import { DialogFileListComponent } from '../../shared/components/dialog-file-list/dialog-file-list.component';
import { DialogOkComponent } from '../../shared/components/dialog-ok/dialog-ok.component';
import { DialogPdfViewerComponent } from '../../shared/components/dialog-pdf-viewer/dialog-pdf-viewer.component';
import { DownloadarchlogService } from '../../shared/services/downloadarchlog.service';
import { OVERLAY_KEYBOARD_DISPATCHER_PROVIDER_FACTORY } from '@angular/cdk/overlay/keyboard/overlay-keyboard-dispatcher';
import { PublicSiteActionsService } from '../../shared/services/publicsiteactions.service';
import { PUBLICATION_MESSAGES } from '../../shared/constants/publication-strings';
import { ALLOWED_EXTENSIONS } from '../../shared/constants/allowed-extensions';

@Component({
  selector: 'app-marchive-edit',
  templateUrl: './marchive-edit.component.html',
  styleUrls: ['./marchive-edit.component.css'],
})
export class MarchiveEditComponent implements OnInit, OnDestroy {
  constructor(
    public authService: AuthService,
    public filtersService: FiltersService,
    public dateparseService: DateparseService,
    private muserService: MuserService,
    private archivefolderService: ArchivefolderService,
    private archivefolderlinkService: ArchivefolderlinkService,
    private archivefolderfileService: ArchivefolderfileService,
    private listoftService: ListoftService,
    private publicSiteActions: PublicSiteActionsService,
    private usagetService: UsagetService,
    private opisService: OpisService,
    private moveordernomService: MoveordernomService,
    private marchfilecopyService: MarchfilecopyService,
    public globalService: GlobalService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private spinnerService: NgxSpinnerService,
    public dialog: MatDialog,
    private location: Location,
    private downloadarchlogService: DownloadarchlogService,
    public snackBar: MatSnackBar
  ) {}
  archivefolder_width = archivefolder_width;

  id: number;
  intType: string | any = ''; // тип интерфейса
  loggedInUser: Muser;
  archivefolderForm: FormGroup;
  files = [];
  links = [];
  postfixTypes: any[];
  statusTypes: any[];

  selectedArchivefolder: Archivefolder;
  selectedArchivefolderOldData: Archivefolder;
  archivefolders: Archivefolder[];
  selectedArchivefolderfiles = [];
  selectedArchivefolderfilesimg = [];
  archivefolderlinks = [];
  isShowDeleteAllFilesButton: boolean;

  opisItems: Opis[];

  enableArch = false; // доступность интерфейса архивиста
  enableScan = false; // доступность интерфейса сканировщика

  inProgress = false;
  inProgressExt = false;
  inProgressText = false;
  inProgressVal = 0;
  inProgressValExt = 0;
  loaded = 0; //
  // inProgressVal_ = 0;
  // bufferValue = 0;

  myControl = new FormControl();
  timerId;
  filteredArchivefolders: Observable<Archivefolder[]>;

  searchOpisId = [];
  opises: Opis[];

  first = false;
  last = false;
  viewOnly = true;

  link_type = 1;
  link_types: any[] = [];
  descrip_type: string;
  descrip_types: ListOfT[] = [];
  actionLogTypes: any[];

  @ViewChild('filesInput') filesInput: ElementRef;

  /**Галерея */
  imgBufsize = 3; // /2 буфер изображений в галерее
  lightboxImages: LightboxImage[] = [];
  // bufflightboxImages: LightboxImage[];
  @ViewChild('lightbox', { static: true }) lightbox: LightboxComponent;

  countJpg = 0;
  countTiff = 0;
  countPdf = 0;
  setArchivefolderId: number; // id дела в галерее

  archiveUser: string;
  scanUser: string;
  musers: Muser[];
  musersScan: Muser[];
  musersArch: Muser[];
  allowedExtensions = ALLOWED_EXTENSIONS.archivefolder;

  nextId: number;
  previousId: number;
  activeRouterObserver: Subscription;

  selectedUsagets: UsageT[] = [];
  us_types: ListOfT[];

  galleryWait = false;
  isPublicationUnblocked: boolean;
  opisInfo: Opis;
  curDate: string;
  ngOnDestroy(): void {
    // this.activeRouterObserver.unsubscribe();
  }

  @HostListener('window:keydown', ['$event'])
  onKeyPress(event: KeyboardEvent) {
    if ((event.ctrlKey || event.metaKey) && event.keyCode == 83) {
      event.preventDefault();
      if (this.isSaveValid() && !this.viewOnly) {
        this.onSaveArchivefolderForm();
      }
    }
  }

  async downloadCard() {
    this.spinnerService.show();
    try {
      let blob = await this.downloadarchlogService.getAf_history(this.id);

      blob.subscribe(
        (event) => {
          if (event.type === HttpEventType.DownloadProgress) {
            const percentDone = Math.round((100 * event.loaded) / event.total);
          } else if (event instanceof HttpResponse) {
            if (event.body.type.includes('application')) {
              importedSaveAs(event.body, `af_history_${this.id}.xlsx`);
            } else {
              importedSaveAs(event.body, `af_history_${this.id}.zip`);
            }
            this.spinnerService.hide();
          } else if (event.status === 409) {
            this.spinnerService.hide();
          }
        },
        (err) => {
          this.spinnerService.hide();
        }
      );
    } catch (err) {
      this.globalService.exceptionHandling(err);
      this.spinnerService.hide();
    }
  }

  async ngOnInit() {
    this.inProgress = false;
    this.inProgressVal = 0;
    this.isShowDeleteAllFilesButton = this.isShowDeleteAllFilesButton;
    this.postfixTypes = this.globalService.postfixTypes;
    this.statusTypes = this.globalService.statusTypes;
    this.link_types = this.globalService.link_types;
    this.curDate = new Date().toISOString().split('T')[0];
    this.archivefolderForm = new FormGroup({
      postfix: new FormControl(null),
      name: new FormControl(null, []),
      description: new FormControl(null),
      page_cnt: new FormControl(0, [Validators.required]),
      spec_page_cnt: new FormControl(0, []),
      doc_cnt: new FormControl(0, []),
      foto_cnt: new FormControl(0, []),
      have_scanned: new FormControl(false, []),
      have_memories: new FormControl(false, []),
      have_arts: new FormControl(false, []),
      have_act: new FormControl(false, []),
      digital: new FormControl(false, []),
      descrip_type: new FormControl({ value: null, disabled: false }, []),
      descrip_type_rmk: new FormControl(null),
      comment: new FormControl(null),
      date: new FormControl(null),
      url: new FormControl(null),
      not_scanned: new FormControl(false),
      not_scanned_rmk: new FormControl(null),
      status: new FormControl(0, []),
      opis_id: new FormControl(null, [Validators.required]),
      delo: new FormControl(null, [Validators.required]),
      delo_extra: new FormControl(null),
      scan_comment: new FormControl(null),
      createduser_id: new FormControl(null),
      scanuser_id: new FormControl(null),
      doc_begin: new FormControl(null, [
        Validators.maxLength(4),
        Validators.max(9999),
      ]),
      doc_end: new FormControl(null, [
        Validators.maxLength(4),
        Validators.max(9999),
      ]),
      pub_date: new FormControl(null),
      page_num_cnt: new FormControl(null),
      published: new FormControl({ value: true, disabled: this.viewOnly }, []),
      mustPublish: new FormControl(null),
      mustUnpublish: new FormControl(null),
      pub_change_date: new FormControl(null),
    });
    try {
      let opisItems = this.opisService.getAll();
      let us_types = this.listoftService.getWithFilter('US_TYPE');
      this.loggedInUser = this.authService.getCurUser();
      let opises = this.opisService.getAll();
      let musers = this.muserService.getAll();
      this.actionLogTypes = this.globalService.actionLogTypes;
      this.us_types = !(await us_types) ? [] : await us_types;
      this.musers = !(await musers) ? [] : await musers;
      this.musers.sort((a, b) => (a.id > b.id ? 1 : -1));
      this.musersScan = this.musers.filter(
        (muser) => muser.usertype === 2 || muser.isadmin === true
      );
      this.isShowDeleteAllFilesButton = this.selectedArchivefolderfiles?.some(
        (file) => file.checkToDel === false
      );
      this.musersArch = this.musers.filter(
        (muser) => muser.usertype === 1 || muser.isadmin === true
      );
      this.opises = !(await opises) ? [] : await opises;
      this.opisItems = !(await opisItems) ? [] : await opisItems;
      this.opisItems.sort((a, b) => a.fond - b.fond);
      if (this.enableArch === true) {
        this.archivefolders =
          await this.archivefolderService.getWithScanuserOffsetLimit(0, 100);
        if (this.archivefolders) {
          this.archivefolders.map((archivefolder) => {
            let _opis = this.opises.find(
              (opis) => opis.id === archivefolder.opis_id
            );
            archivefolder.opis = !_opis ? null : _opis.opis_name;
            archivefolder.fond = !_opis ? null : _opis.fond;
          });
        }
        this.archivefolders = this.archivefolders.filter(
          (archivefolders) => archivefolders.id !== this.id
        );
        this.filteredArchivefolders = this.myControl.valueChanges.pipe(
          startWith<string | Archivefolder>(''),
          map((value) =>
            typeof value === 'string' ? value : !value ? null : value.name
          ),
          map((name) => (name ? this._filter(name) : [...this.archivefolders]))
        );
      }
      this.getSearchOpisId();
    } catch (err) {
      if (err.status === 401) {
        console.error(`Ошибка авторизации`);
        this.authService.logout();
      } else {
        console.error(`Ошибка ${err}`);
        this.openSnackBar('Ошибка при получении данных дела', 'Ok');
      }
    }
    this.activeRouterObserver = this.activatedRoute.params.subscribe(
      (param) => {
        this.id = param.id;
        if (param.id === 'newdoc') {
          this.intType = 'newdoc';
          this.filtersService.newDoc = true;
          this.viewOnly = false;
        } else {
          let str = param.id.split('_');
          if (str[0] === 'v') {
            this.id = +str[1];
            this.intType = 'editdoc';
            this.viewOnly = true;
          } else {
            this.id = +param.id;
            this.intType = 'editdoc';
            this.viewOnly = false;
          }
        }
        this.getPage();
        this.getData();
      }
    );
    this.lightboxImages = [];
  }

  async setInt() {
    /**Установка доступа к элементам интерфейса */
    if (this.loggedInUser.isadmin) {
      this.enableArch = true;
      this.enableScan = true;
    } else if (
      !this.loggedInUser.isadmin &&
      (this.loggedInUser.usertype === 1 || this.loggedInUser.usertype === 3)
    ) {
      this.enableArch = true;
      this.enableScan = true;
    } else if (!this.loggedInUser.isadmin && this.loggedInUser.usertype === 2) {
      this.enableArch = false;
      this.enableScan = true;
      if (this.archivefolderForm.value.status === 3) {
        this.enableScan = false;
        this.viewOnly = true;
      }
    } else {
      this.enableArch = false;
      this.enableScan = false;
      this.viewOnly = true;
    }

    // if (
    //   this.loggedInUser.isadmin !== true &&
    //   this.loggedInUser.usertype === 2
    // ) {
    //   if (!isNullOrUndefined(this.selectedArchivefolder.scanuser_id)) {
    //     if (this.selectedArchivefolder.scanuser_id !== this.loggedInUser.id) {
    //       this.enableScan = false;
    //       this.viewOnly = true;
    //     }
    //   }
    // }
    if (
      this.viewOnly === true ||
      (!this.authService.getCurUser().isadmin &&
        this.authService.getCurUser().usertype === 2)
    ) {
      this.archivefolderForm.controls['spec_page_cnt'].disable();
      this.archivefolderForm.controls['page_cnt'].disable();
      this.archivefolderForm.controls['scan_comment'].enable();
    } else {
      this.archivefolderForm.controls['spec_page_cnt'].enable();
      this.archivefolderForm.controls['page_cnt'].enable();
    }
    if (this.viewOnly === true) {
      this.myControl.disable();
      this.archivefolderForm.controls['foto_cnt'].disable();
      this.archivefolderForm.controls['doc_cnt'].disable();
    } else {
      this.myControl.enable();
      this.archivefolderForm.controls['foto_cnt'].enable();
      this.archivefolderForm.controls['doc_cnt'].enable();
    }
    if (
      !this.authService.getCurUser().isadmin &&
      this.authService.getCurUser().usertype === 4
    ) {
      this.archivefolderForm.controls['doc_begin'].disable();
      this.archivefolderForm.controls['doc_end'].disable();
      this.archivefolderForm.controls['scan_comment'].disable();
      this.archivefolderForm.controls['pub_date'].disable();
    } else {
      this.archivefolderForm.controls['doc_begin'].enable();
      this.archivefolderForm.controls['doc_end'].enable();
      this.archivefolderForm.controls['scan_comment'].enable();
      this.archivefolderForm.controls['pub_date'].enable();
    }
    if (this.viewOnly === true || this.enableArch === false) {
      // this.archivefolderForm.controls['scan_comment'].disable();
      this.archivefolderForm.controls['status'].disable();
      this.archivefolderForm.controls['have_act'].disable();
      this.archivefolderForm.controls['have_arts'].disable();
      this.archivefolderForm.controls['have_memories'].disable();
      this.archivefolderForm.controls['have_scanned'].disable();
      this.archivefolderForm.controls['comment'].disable();
      this.archivefolderForm.controls['date'].disable();
      this.archivefolderForm.controls['description'].disable();
      this.archivefolderForm.controls['not_scanned_rmk'].disable();
      this.archivefolderForm.controls['not_scanned'].disable();
      this.archivefolderForm.controls['url'].disable();
      this.archivefolderForm.controls['descrip_type'].disable();
      this.archivefolderForm.controls['descrip_type_rmk'].disable();
      this.archivefolderForm.controls['digital'].disable();
      this.archivefolderForm.controls['postfix'].disable();
      this.archivefolderForm.controls['name'].disable();
      this.archivefolderForm.controls['delo_extra'].disable();
      this.archivefolderForm.controls['delo'].disable();
      this.archivefolderForm.controls['opis_id'].disable();
      this.archivefolderForm.controls['page_num_cnt'].disable();
      this.archivefolderForm.controls['published'].disable();
    } else {
      this.archivefolderForm.controls['page_num_cnt'].enable();
      this.archivefolderForm.controls['scan_comment'].enable();
      this.archivefolderForm.controls['status'].enable();
      this.archivefolderForm.controls['have_act'].enable();
      this.archivefolderForm.controls['have_arts'].enable();
      this.archivefolderForm.controls['have_memories'].enable();
      this.archivefolderForm.controls['have_scanned'].enable();
      this.archivefolderForm.controls['comment'].enable();
      this.archivefolderForm.controls['date'].enable();
      this.archivefolderForm.controls['description'].enable();
      this.archivefolderForm.controls['not_scanned_rmk'].enable();
      this.archivefolderForm.controls['not_scanned'].enable();
      this.archivefolderForm.controls['url'].enable();
      this.archivefolderForm.controls['descrip_type'].enable();
      this.archivefolderForm.controls['descrip_type_rmk'].enable();
      this.archivefolderForm.controls['digital'].enable();
      this.archivefolderForm.controls['postfix'].enable();
      this.archivefolderForm.controls['name'].enable();
      this.archivefolderForm.controls['delo_extra'].enable();
      this.archivefolderForm.controls['delo'].enable();
      this.archivefolderForm.controls['opis_id'].enable();
      this.archivefolderForm.controls['published'].enable();
    }
  }

  async getData() {
    this.spinnerService.show();
    this.lightboxImages = [];
    this.onDelFiles();
    try {
      switch (this.intType) {
        case 'editdoc': // редактирование документа
          let idn = this.archivefolderService.getnextkey(
            this.id,
            this.filtersService.archivefolderListFilter.searchStr,
            this.filtersService.archivefolderListFilter.searchOpis,
            this.filtersService.archivefolderListFilter.searchOrder,
            this.filtersService.archivefolderListFilter.searchStatus,
            this.filtersService.archivefolderListFilter.searchUnPhoto,
            this.filtersService.archivefolderListFilter.searchUnDoc,
            this.filtersService.archivefolderListFilter.searchAct,
            this.filtersService.archivefolderListFilter.searchMem,
            this.filtersService.archivefolderListFilter.searchArt,
            this.filtersService.archivefolderListFilter.searchDig,
            this.filtersService.archivefolderListFilter.searchtxt,
            this.filtersService.archivefolderListFilter.searchflds,
            this.filtersService.archivefolderListFilter.descrip_type,
            this.filtersService.archivefolderListFilter.filterExact
          );
          let idp = this.archivefolderService.getpreviouskey(
            this.id,
            this.filtersService.archivefolderListFilter.searchStr,
            this.filtersService.archivefolderListFilter.searchOpis,
            this.filtersService.archivefolderListFilter.searchOrder,
            this.filtersService.archivefolderListFilter.searchStatus,
            this.filtersService.archivefolderListFilter.searchUnPhoto,
            this.filtersService.archivefolderListFilter.searchUnDoc,
            this.filtersService.archivefolderListFilter.searchAct,
            this.filtersService.archivefolderListFilter.searchMem,
            this.filtersService.archivefolderListFilter.searchArt,
            this.filtersService.archivefolderListFilter.searchDig,
            this.filtersService.archivefolderListFilter.searchtxt,
            this.filtersService.archivefolderListFilter.searchflds,
            this.filtersService.archivefolderListFilter.descrip_type,
            this.filtersService.archivefolderListFilter.filterExact
          );
          let selectedArchivefolder = this.archivefolderService.getOneById(
            this.id
          );
          let archivefolderlinks =
            this.archivefolderlinkService.getAllWithNamesById(this.id);
          let selectedArchivefolderfiles =
            this.archivefolderfileService.getAllByParentId(this.id, true);
          let selectedUsagets = this.usagetService.getByArchivefolderId(
            this.id
          );
          this.selectedArchivefolder = await selectedArchivefolder;
          this.selectedArchivefolderOldData = this.selectedArchivefolder;
          this.selectedUsagets = (await selectedUsagets)
            ? await selectedUsagets
            : [];
          this.selectedUsagets.sort((a, b) => (a.us_no > b.us_no ? 1 : -1));
          let createduser;
          if (this.selectedArchivefolder.createduser_id) {
            createduser = this.muserService.getOneById(
              this.selectedArchivefolder.createduser_id
            );
          }
          let descrip_types = this.listoftService.getWithFilter('DESCRIP_TYPE');
          this.descrip_types = !(await descrip_types)
            ? []
            : await descrip_types;
          this.descrip_types.sort((a, b) => {
            if (a.name_ent < b.name_ent) {
              return -1;
            }
            if (a.name_ent > b.name_ent) {
              return 1;
            }
            return 0;
          });
          let scanuser;
          if (this.selectedArchivefolder.scanuser_id) {
            scanuser = this.muserService.getOneById(
              this.selectedArchivefolder.scanuser_id
            );
          }
          this.archivefolderlinks = !(await archivefolderlinks)
            ? []
            : await archivefolderlinks;
          this.archivefolderlinks.map((archivefolderlink) => {
            archivefolderlink.checkToDel = false;
            if (this.enableArch === true) {
              if (this.archivefolders) {
                this.archivefolders = this.archivefolders.filter(
                  (archivefolders) =>
                    !(
                      archivefolders.id ===
                        archivefolderlink.archivefolder_id1 ||
                      archivefolders.id === archivefolderlink.archivefolder_id2
                    )
                );
              }
            }
          });
          this.myControl.patchValue({});
          this.selectedArchivefolderfiles = !(await selectedArchivefolderfiles)
            ? []
            : await selectedArchivefolderfiles;

          this.countJpg = 0;
          this.countTiff = 0;
          this.countPdf = 0;
          this.selectedArchivefolderfiles.map((selectedArchivefolderfile) => {
            selectedArchivefolderfile.checkToDel = false;
            let ext = selectedArchivefolderfile.archivefile.filename
              .split('.')
              .reverse()[0];
            if (ext.toLowerCase() === 'jpg' || ext.toLowerCase() === 'jpeg') {
              this.countJpg++;
            } else if (
              ext.toLowerCase() === 'tiff' ||
              ext.toLowerCase() === 'tif'
            ) {
              this.countTiff++;
            } else if (ext.toLowerCase() === 'pdf') {
              this.countPdf++;
            }
          });
          this.selectedArchivefolderfiles.sort((a, b) =>
            a.archivefile.filename > b.archivefile.filename ? 1 : -1
          );
          this.selectedArchivefolderfilesimg =
            this.selectedArchivefolderfiles.filter(
              (selectedArchivefolderfile) =>
                this.globalService.isImgtype(
                  selectedArchivefolderfile.archivefile.filename
                ) ||
                this.globalService.isVideotype(
                  selectedArchivefolderfile.archivefile.filename
                ) ||
                this.globalService.isAudiotype(
                  selectedArchivefolderfile.archivefile.filename
                )
            );
          this.selectedArchivefolderfilesimg.sort((a, b) =>
            a.archivefile.filename > b.archivefile.filename ? 1 : -1
          );
          // let doc_begin = 9999;
          // let doc_end = 0;
          // if (Array.isArray(this.selectedArchivefolderfiles)) {
          //   this.selectedArchivefolderfiles.forEach((element) => {

          //   });
          // }
          this.opisInfo = this.opises.find(
            (opis: Opis) => opis.id === this.selectedArchivefolder.opis_id
          );
          this.isPublicationUnblocked = this.opisInfo.published === '*';
          this.archivefolderForm.patchValue({
            postfix: !this.selectedArchivefolder.postfix
              ? ''
              : this.selectedArchivefolder.postfix,
            name: this.selectedArchivefolder.name,
            description: this.selectedArchivefolder.description,
            page_cnt: this.selectedArchivefolder.page_cnt,
            spec_page_cnt: this.selectedArchivefolder.spec_page_cnt,
            doc_cnt: 0,
            foto_cnt: 0,
            have_scanned: this.selectedArchivefolder.have_scanned,
            have_memories: this.selectedArchivefolder.have_memories,
            have_arts: this.selectedArchivefolder.have_arts,
            descrip_type: this.selectedArchivefolder.descrip_type
              ? +this.selectedArchivefolder.descrip_type
              : null,
            descrip_type_rmk: this.selectedArchivefolder.descrip_type_rmk,
            have_act: this.selectedArchivefolder.have_act,
            digital: this.selectedArchivefolder.digital,
            comment: this.selectedArchivefolder.comment,
            date: this.selectedArchivefolder.date,
            status: this.selectedArchivefolder.status,
            opis_id: this.selectedArchivefolder.opis_id,
            delo: this.selectedArchivefolder.delo,
            delo_extra: this.selectedArchivefolder.delo_extra,
            url: this.selectedArchivefolder.url,
            not_scanned: this.selectedArchivefolder.not_scanned,
            not_scanned_rmk: this.selectedArchivefolder.not_scanned_rmk,
            scan_comment: this.selectedArchivefolder.scan_comment,
            createduser_id: this.selectedArchivefolder.createduser_id,
            scanuser_id: this.selectedArchivefolder.scanuser_id,
            doc_begin: this.selectedArchivefolder.doc_begin,
            doc_end: this.selectedArchivefolder.doc_end,
            pub_date: this.globalService.dateFromDateString(
              this.selectedArchivefolder.pub_date
            ),
            page_num_cnt: this.selectedArchivefolder.page_num_cnt,
            published:
              this.selectedArchivefolder.published === '*' ? true : false,
            mustPublish: this.selectedArchivefolder.mustPublish,
            mustUnpublish: this.selectedArchivefolder.mustUnpublish,
            pub_change_date: this.selectedArchivefolder.pub_change_date,
          });
          if ((await idn) && (await idn).nextkey) {
            this.last = false;
          } else {
            this.last = true;
          }
          if ((await idn) && (await idp).previouskey) {
            this.first = false;
          } else {
            this.first = true;
          }
          if (this.selectedArchivefolder.createduser_id) {
            let date = new Date(this.selectedArchivefolder['date_created']);
            this.archiveUser = `${(await createduser).surname} ${
              (await createduser).name // '0' + MyDate.getDate()).slice(-2)
            } (${('0' + date.getDate()).slice(-2)}.${(
              '0' +
              (+date.getMonth() + +1)
            ).slice(
              -2
            )}.${date.getFullYear()} ${date.getHours()}:${date.getMinutes()})`;
          } else {
            this.archiveUser = null;
          }

          if (this.selectedArchivefolder.scanuser_id) {
            let date = new Date(this.selectedArchivefolder['date_scan']);
            this.scanUser = `${(await scanuser).surname} ${
              (await scanuser).name
            } (${('0' + date.getDate()).slice(-2)}.${(
              '0' +
              (+date.getMonth() + +1)
            ).slice(
              -2
            )}.${date.getFullYear()} ${date.getHours()}:${date.getMinutes()})`;
          } else {
            this.scanUser = null;
          }

          this.setInt();
          break;
        case 'newdoc': // создание документа
          this.archivefolderForm.reset();
          this.archivefolderForm.patchValue({
            page_cnt: 0,
            spec_page_cnt: 0,
            doc_cnt: 0,
            foto_cnt: 0,
            status: 0,
            have_scanned: false,
            have_memories: false,
            have_arts: false,
            have_act: false,
            published: '*',
            mustPublish: '*',
            mustUnpublish: null,
            pub_change_date: this.curDate,
          });
          this.setInt();
          if (this.filtersService.tempOpisid) {
            this.archivefolderForm.patchValue({
              opis_id: this.filtersService.tempOpisid,
            });
          }
          break;
        default:
      }
    } catch (err) {
      if (err.status === 401) {
        console.error(`Ошибка авторизации`);
        this.authService.logout();
      } else {
        console.error(`Ошибка ${err}`);
        this.openSnackBar('Ошибка при получении данных дела для формы', 'Ok');
      }
    } finally {
      this.spinnerService.hide();
    }
  }

  async onPubToggleChange() {
    const publishedFormValue = this.archivefolderForm.value.published;
    const pubDateFormValue = this.archivefolderForm.value.pub_date;
    if (!this.isPublicationUnblocked) {
      this.dialog.open(DialogOkComponent, {
        data: {
          title: PUBLICATION_MESSAGES.CHANGE_PARENT_STATUS,
        },
      });
      this.archivefolderForm.patchValue({
        published: this.selectedArchivefolder.published === '*' ? true : false,
      });
    }
    if (!publishedFormValue && !pubDateFormValue) {
      this.dialog.open(DialogOkComponent, {
        data: {
          title: PUBLICATION_MESSAGES.SET_PUB_CHANGE,
        },
      });
    }
  }

  async checkFormChanges(oldData: Archivefolder, newData: Archivefolder) {
    const isChanged = Object.keys(oldData).some(
      (key) => oldData[key] !== newData[key]
    );

    const isPublished = newData.published === '*';

    if (isChanged && isPublished) {
      this.archivefolderForm.patchValue({
        mustPublish: '*',
        mustUnpublish: null,
        pub_change_date: this.curDate,
      });
    }
  }

  openPdf(selectedArchivefolderfile) {
    this.dialog.open(DialogPdfViewerComponent, {
      panelClass: 'app-dialog-extend',
      data: {
        file: selectedArchivefolderfile,
      },
    });
  }

  async onShowImg(id, selectedArchivefolderfile) {
    if (
      this.globalService.isPdftype(
        selectedArchivefolderfile.archivefile.filename
      )
    ) {
      this.openPdf(selectedArchivefolderfile);
      return;
    }
    let archivefolderfiles: any[];
    let parchivefolderfiles: any[];
    let narchivefolderfiles: any[];
    this.lightboxImages.length = 0;
    let idx = this.selectedArchivefolderfilesimg.findIndex(
      (item) => item.id === id
    );
    parchivefolderfiles = this.selectedArchivefolderfilesimg.slice(
      idx,
      this.imgBufsize + idx > this.selectedArchivefolderfilesimg.length
        ? this.selectedArchivefolderfilesimg.length
        : this.imgBufsize + idx
    );
    narchivefolderfiles = this.selectedArchivefolderfilesimg.slice(
      idx - this.imgBufsize > 0 ? idx - this.imgBufsize : 0,
      idx
    );
    archivefolderfiles = [...narchivefolderfiles, ...parchivefolderfiles];
    archivefolderfiles.sort((a, b) =>
      a.archivefile.filename > b.archivefile.filename ? 1 : -1
    );
    let fname: string;
    this.selectedArchivefolderfilesimg.map((item) => {
      if (item.id === id) {
        fname = item.archivefile.filename;
      }
    });
    this.spinnerService.show();
    this.inProgress = true;
    this.inProgressVal += 100 / archivefolderfiles.length;
    await Promise.all(
      archivefolderfiles.map((item) => {
        return new Promise(async (resolve) => {
          let resfile;
          if (
            this.globalService.isImgtype(item.archivefile.filename) === true ||
            this.globalService.isVideotype(item.archivefile.filename) ===
              true ||
            this.globalService.isAudiotype(item.archivefile.filename) === true
          ) {
            try {
              let file;
              let type;
              let ext;
              if (
                this.globalService.isImgtype(item.archivefile.filename) === true
              ) {
                type = 'image';
                file = await this.archivefolderfileService.getFilePreviewById(
                  item.id
                );
                resfile = new Blob([file], { type: 'image/jpg' });
                let reader = new FileReader();
                reader.readAsDataURL(resfile);
                reader.onloadend = () => {
                  let base64data = String(reader.result);
                  this.lightboxImages.push({
                    src: base64data,
                    caption: item.archivefile.filename,
                    id: item.id,
                    type,
                    ext,
                    orientation: item.orientation,
                  });
                  resolve(null);
                };
              } else if (
                this.globalService.isVideotype(item.archivefile.filename) ===
                true
              ) {
                type = 'video';
                ext = item.archivefile.filename
                  .toLowerCase()
                  .split('.')
                  .reverse()[0];
                this.lightboxImages.push({
                  src: this.archivefolderfileService.generateLink(
                    item.id,
                    item.jwt,
                    item.archivefile.storage
                  ),
                  caption: item.archivefile.filename,
                  id: item.id,
                  type,
                  ext,
                  orientation: item.orientation,
                });
                resolve(null);
              } else {
                type = 'audio';
                ext = item.archivefile.filename
                  .toLowerCase()
                  .split('.')
                  .reverse()[0];
                this.lightboxImages.push({
                  src: this.archivefolderfileService.generateLink(
                    item.id,
                    item.jwt,
                    item.archivefile.storage
                  ),
                  caption: item.archivefile.filename,
                  id: item.id,
                  type,
                  ext,
                  orientation: item.orientation,
                });
                resolve(null);
              }
              this.inProgressVal += 100 / archivefolderfiles.length;
            } catch (err) {
              resolve(null);
            }
          } else {
            resolve(null);
          }
        });
      })
    );
    this.spinnerService.hide();
    this.lightboxImages.sort((a, b) => (a.caption > b.caption ? 1 : -1));
    this.inProgress = false;
    setTimeout(() => {
      let idx = this.lightboxImages.findIndex((item) => item.caption === fname);
      this.lightbox.open(idx);
    }, 300);
  }

  async addPrevImg() {
    let firstId = this.lightboxImages[0].id;
    let idx = this.selectedArchivefolderfilesimg.findIndex(
      (item) => item.id === +firstId
    );
    if (idx === 0 || idx === -1) {
      return false;
    } else {
      this.spinnerService.show();
      this.galleryWait = true;
      let file;
      let type;
      let resfile;
      try {
        if (
          this.globalService.isImgtype(
            this.selectedArchivefolderfilesimg[idx - 1].archivefile.filename
          ) === true
        ) {
          type = 'image';
          file = await this.archivefolderfileService.getFilePreviewById(
            this.selectedArchivefolderfilesimg[idx - 1].id
          );
          resfile = new Blob([file], { type: 'image/jpg' });
          let reader = new FileReader();
          reader.readAsDataURL(resfile);
          reader.onloadend = () => {
            let base64data = String(reader.result);
            this.lightbox.unshiftImg({
              src: base64data,
              caption:
                this.selectedArchivefolderfilesimg[idx - 1].archivefile
                  .filename,
              id: this.selectedArchivefolderfilesimg[idx - 1].id,
              type,
              orientation:
                this.selectedArchivefolderfilesimg[idx - 1].orientation,
            });
          };
        } else if (
          this.globalService.isVideotype(
            this.selectedArchivefolderfilesimg[idx - 1].archivefile.filename
          ) === true
        ) {
          type = 'video';
          let ext = this.selectedArchivefolderfilesimg[
            idx - 1
          ].archivefile.filename
            .toLowerCase()
            .split('.')
            .reverse()[0];
          this.lightbox.unshiftImg({
            src: this.archivefolderfileService.generateLink(
              this.selectedArchivefolderfilesimg[idx - 1].id,
              this.selectedArchivefolderfilesimg[idx - 1].jwt,
              this.selectedArchivefolderfilesimg[idx - 1].archivefile.storage
            ),
            caption:
              this.selectedArchivefolderfilesimg[idx - 1].archivefile.filename,
            id: this.selectedArchivefolderfilesimg[idx - 1].id,
            type,
            ext,
            orientation:
              this.selectedArchivefolderfilesimg[idx - 1].orientation,
          });
        } else {
          type = 'audio';
          let ext = this.selectedArchivefolderfilesimg[
            idx - 1
          ].archivefile.filename
            .toLowerCase()
            .split('.')
            .reverse()[0];
          this.lightbox.unshiftImg({
            src: this.archivefolderfileService.generateLink(
              this.selectedArchivefolderfilesimg[idx - 1].id,
              this.selectedArchivefolderfilesimg[idx - 1].jwt,
              this.selectedArchivefolderfilesimg[idx - 1].archivefile.storage
            ),
            caption:
              this.selectedArchivefolderfilesimg[idx - 1].archivefile.filename,
            id: this.selectedArchivefolderfilesimg[idx - 1].id,
            type,
            ext,
            orientation:
              this.selectedArchivefolderfilesimg[idx - 1].orientation,
          });
        }
      } catch {
        this.openSnackBar(
          `Ошибка при обработке изображения ${
            this.selectedArchivefolderfilesimg[idx - 1].archivefile.filename
          }`,
          'Ok'
        );
      } finally {
        this.spinnerService.hide();
        this.galleryWait = false;
      }
      return true;
    }
  }
  getActionValueByKey(key): string {
    return !this.actionLogTypes.find((item) => item.key === key)
      ? ''
      : this.actionLogTypes.find((item) => item.key === key).value;
  }
  getOldData(obj) {
    if (obj !== null) {
      if (obj.old_data != null) {
        return obj.old_data;
      } else {
        return '';
      }
    }
  }

  getNewData(obj) {
    if (obj !== null) {
      if (obj.new_data != null) {
        return obj.new_data;
      } else {
        return '';
      }
    }
  }

  async addNextImg() {
    let lastId = +this.lightboxImages[this.lightboxImages.length - 1].id;
    let idx = this.selectedArchivefolderfilesimg.findIndex(
      (item) => item.id === +lastId
    );
    if (idx + 1 > this.selectedArchivefolderfilesimg.length - 1 || idx === -1) {
      return false;
    } else {
      this.spinnerService.show();
      this.galleryWait = true;
      let file;
      let type;
      let resfile;
      try {
        if (
          this.globalService.isImgtype(
            this.selectedArchivefolderfilesimg[idx + 1].archivefile.filename
          ) === true
        ) {
          type = 'image';
          file = await this.archivefolderfileService.getFilePreviewById(
            this.selectedArchivefolderfilesimg[idx + 1].id
          );
          resfile = new Blob([file], { type: 'image/jpg' });
          let reader = new FileReader();
          reader.readAsDataURL(resfile);
          reader.onloadend = () => {
            let base64data = String(reader.result);
            this.lightbox.pushImg({
              src: base64data,
              caption:
                this.selectedArchivefolderfilesimg[idx + 1].archivefile
                  .filename,
              id: this.selectedArchivefolderfilesimg[idx + 1].id,
              type,
              orientation:
                this.selectedArchivefolderfilesimg[idx + 1].orientation,
            });
          };
        } else if (
          this.globalService.isVideotype(
            this.selectedArchivefolderfilesimg[idx + 1].archivefile.filename
          ) === true
        ) {
          type = 'video';
          let ext = this.selectedArchivefolderfilesimg[
            idx + 1
          ].archivefile.filename
            .toLowerCase()
            .split('.')
            .reverse()[0];
          this.lightbox.pushImg({
            src: this.archivefolderfileService.generateLink(
              this.selectedArchivefolderfilesimg[idx + 1].id,
              this.selectedArchivefolderfilesimg[idx + 1].jwt,
              this.selectedArchivefolderfilesimg[idx + 1].archivefile.storage
            ),
            caption:
              this.selectedArchivefolderfilesimg[idx + 1].archivefile.filename,
            id: this.selectedArchivefolderfilesimg[idx + 1].id,
            type,
            ext,
            orientation:
              this.selectedArchivefolderfilesimg[idx + 1].orientation,
          });
        } else {
          type = 'audio';
          let ext = this.selectedArchivefolderfilesimg[
            idx + 1
          ].archivefile.filename
            .toLowerCase()
            .split('.')
            .reverse()[0];
          this.lightbox.pushImg({
            src: this.archivefolderfileService.generateLink(
              this.selectedArchivefolderfilesimg[idx + 1].id,
              this.selectedArchivefolderfilesimg[idx + 1].jwt,
              this.selectedArchivefolderfilesimg[idx + 1].archivefile.storage
            ),
            caption:
              this.selectedArchivefolderfilesimg[idx + 1].archivefile.filename,
            id: this.selectedArchivefolderfilesimg[idx + 1].id,
            type,
            ext,
            orientation:
              this.selectedArchivefolderfilesimg[idx + 1].orientation,
          });
        }
      } catch (err) {
        console.error(err);
        this.openSnackBar(
          `Ошибка при обработке изображения ${
            this.selectedArchivefolderfilesimg[idx + 1].archivefile.filename
          }`,
          'Ok'
        );
      } finally {
        this.spinnerService.hide();
        this.galleryWait = false;
      }
      return true;
    }
  }

  async previewChange(e) {
    if (e.forward === false) {
      // if (!isNullOrUndefined(this.bufflightboxImages)) {
      //   this.lightboxImages = [...this.bufflightboxImages];
      // }
      await this.addPrevImg();
    } else {
      await this.addNextImg();
    }
  }

  async getPublicSiteActionType(delPers?) {
    const oldData = this.selectedArchivefolderOldData;
    const newData = this.archivefolderForm;
    if (!delPers && oldData && oldData?.published !== newData.value.published) {
      const isActionDelete =
        this.archivefolderForm.value['published'] === null ? '*' : null;
      const isActionUpdate =
        this.archivefolderForm.value['published'] === '*' ? '*' : null;
      await this.publicSiteActions.postOne({
        entity_id: this.id,
        entity_type: 2,
        action_delete: isActionDelete,
        action_update: isActionUpdate,
        legacyId: `ID-arch-${this.opisInfo.fond}-${
          this.opisInfo.opis_name
        }-${this.selectedArchivefolder.delo}${
          this.selectedArchivefolder.delo_extra
            ? `/${this.selectedArchivefolder.delo_extra}`
            : ''
        }`,
      });
    }
    if (!oldData && !delPers) {
      await this.publicSiteActions.postOne({
        entity_id: this.id,
        entity_type: 2,
        action_delete: null,
        action_update: '*',
        legacyId: `ID-arch-${this.opisInfo.fond}-${
          this.opisInfo.opis_name
        }-${this.selectedArchivefolder.delo}${
          this.selectedArchivefolder.delo_extra
            ? `/${this.selectedArchivefolder.delo_extra}`
            : ''
        }`,
      });
    }
    if (delPers) {
      await this.publicSiteActions.postOne({
        entity_id: this.id,
        entity_type: 2,
        action_delete: '*',
        action_update: null,
        legacyId: `ID-arch-${this.opisInfo.fond}-${
          this.opisInfo.opis_name
        }-${this.selectedArchivefolder.delo}${
          this.selectedArchivefolder.delo_extra
            ? `/${this.selectedArchivefolder.delo_extra}`
            : ''
        }`,
      });
    }
  }

  async checkingPublicationLogic() {
    if (this.isPublicationUnblocked) {
      const archivefolderPublishedField =
        this.archivefolderForm.value['published'];
      if (
        archivefolderPublishedField === '*' ||
        archivefolderPublishedField === true
      ) {
        this.archivefolderForm.patchValue({
          published: '*',
        });
      } else {
        this.archivefolderForm.patchValue({
          published: null,
        });
      }
      const archivefolderPubDateField =
        this.archivefolderForm.value['pub_date'];

      if (archivefolderPubDateField && archivefolderPubDateField !== '') {
        const isCorrectDateFormat =
          new Date(this.archivefolderForm.value['pub_date']) instanceof Date;
        const isPubDateValueExist = !!new Date(
          this.archivefolderForm.value['pub_date']
        ).valueOf();
        const isPubDateCorrect = isCorrectDateFormat && isPubDateValueExist;
        const pubDate = new Date(this.archivefolderForm.value['pub_date']);
        const curDate = new Date();

        if (!isPubDateCorrect) {
          this.openSnackBar(PUBLICATION_MESSAGES.NO_PUB_DATE, 'Ok');
          return false;
        }
        const dayMatch = pubDate.getDate() === curDate.getDate();
        const monthMatch = pubDate.getMonth() === curDate.getMonth();
        const yearMatch = pubDate.getFullYear() === curDate.getFullYear();
        if (+curDate < +pubDate) {
          this.archivefolderForm.patchValue({
            published: null,
          });
        }
        if (dayMatch && monthMatch && yearMatch) {
          this.archivefolderForm.patchValue({
            published: '*',
          });
        }
      } else {
        this.archivefolderForm.patchValue({
          pub_date: null,
        });
      }
    } else {
      if (
        this.archivefolderForm.value['published'] === true ||
        this.archivefolderForm.value['published'] === '*'
      ) {
        this.archivefolderForm.patchValue({
          published: '*',
        });
      } else {
        this.archivefolderForm.patchValue({
          published: null,
        });
      }
    }
  }

  async onSaveArchivefolderForm(create = false, next = false, silent = false) {
    return new Promise(async (resolve, reject) => {
      // this.spinnerService.show();
      if (this.archivefolderForm.value.postfix === '') {
        this.archivefolderForm.patchValue({
          postfix: null,
        });
      }

      if (this.archivefolderForm.value.descrip_type) {
        this.archivefolderForm.patchValue({
          descrip_type: this.archivefolderForm.value.descrip_type.toString(),
        });
      }

      if (this.archivefolderForm.value.descrip_type === null) {
        this.archivefolderForm.patchValue({
          descrip_type: null,
        });
      }

      if (this.selectedArchivefolder) {
        await this.checkingPublicationLogic();

        const oldPublicationStatus = this.selectedArchivefolder.published;
        const newPublicationStatus = this.archivefolderForm.value.published;
        if (oldPublicationStatus !== newPublicationStatus) {
          if (
            this.archivefolderForm.value.published === true ||
            this.archivefolderForm.value.published === '*'
          ) {
            this.archivefolderForm.patchValue({
              mustPublish: '*',
              mustUnpublish: null,
              pub_change_date: this.curDate,
            });
          } else {
            this.archivefolderForm.patchValue({
              mustPublish: null,
              mustUnpublish: '*',
              pub_change_date: this.curDate,
            });
          }
        }
      }

      /**Проверка на имена файлов */
      let tmpNames = [];
      tmpNames = this.files.filter((file) => {
        return tmpNames[file.name] || !(tmpNames[file.name] = !0);
      });
      this.files.forEach((file) => {
        this.selectedArchivefolderfiles.forEach((selectedArchivefolderfile) => {
          if (file.name === selectedArchivefolderfile.archivefile.filename) {
            tmpNames.push(file);
          }
        });
      });
      let used = {};

      tmpNames = tmpNames.filter((obj) => {
        return obj.name in used ? 0 : (used[obj.name] = 1);
      });

      if (tmpNames.length !== 0) {
        this.dialog.open(DialogNameslistComponent, {
          data: {
            title: 'Найдены одинаковые имена файлов',
            files: tmpNames,
          },
        });
        return false;
      }

      this.inProgress = true;
      this.inProgressExt = true;
      this.spinnerService.show();

      try {
        switch (this.intType) {
          case 'editdoc': // редактирование документа
            if (this.enableArch || this.enableScan) {
              this.checkFormChanges(
                this.selectedArchivefolder,
                this.archivefolderForm.value
              );
              let putArchivefolderForm = this.archivefolderService.putOneById(
                this.id,
                this.archivefolderForm.getRawValue()
              );
              await putArchivefolderForm;
              await this.getPublicSiteActionType();
            }

            if (this.enableArch) {
              // let putArchivefolderForm = this.archivefolderService.putOneById(
              //   this.id,
              //   this.archivefolderForm.getRawValue()
              // );
              // await putArchivefolderForm;
              let deleteArchivefolderlinks = Promise.all(
                this.archivefolderlinks.map(async (archivefolderlink) => {
                  if (archivefolderlink.checkToDel) {
                    if (archivefolderlink.link_type === 1) {
                      return this.archivefolderlinkService.deleteOneById(
                        archivefolderlink.id,
                        {
                          id_fordelete:
                            +archivefolderlink.archivefolder_id1 === +this.id
                              ? archivefolderlink.archivefolder_id2
                              : archivefolderlink.archivefolder_id1,
                        }
                      );
                    } else {
                      return this.archivefolderlinkService.deleteOneById(
                        archivefolderlink.id
                      );
                    }
                  }
                })
              );
              let addArchivefolderlinks = Promise.all(
                this.links.map(async (link) => {
                  await this.archivefolderlinkService.postOne({
                    archivefolder_id1: this.id,
                    archivefolder_id2: link.val.id,
                    link_type: link.link_type,
                  });
                })
              );
              await deleteArchivefolderlinks;
              await addArchivefolderlinks;
            }
            if (this.enableScan) {
              // let putArchivefolderForm = this.archivefolderService.putOneById(
              //   this.id,
              //   this.archivefolderForm.getRawValue()
              // );
              let deleteSelectedArchivefolderfiles = Promise.all(
                this.selectedArchivefolderfiles.map(
                  async (selectedArchivefolderfile) => {
                    if (selectedArchivefolderfile.checkToDel) {
                      let deleteSelectedArchivefolderfile =
                        await this.archivefolderfileService.deleteOneById(
                          selectedArchivefolderfile.id
                        );
                    }
                  }
                )
              );
              let size = 0;
              for (let i = 0; i < this.files.length; i++) {
                size += this.files[i].size;
              }
              let addArchivefolderfiles = Promise.all(
                this.files.map(async (file) => {
                  return new Promise((resolve, reject) => {
                    let fileForm: FormData = new FormData();
                    fileForm.append('archivefolder_id', this.id.toString());
                    fileForm.append('archivefile', file);
                    this.inProgressValExt = 0;
                    this.archivefolderfileService
                      .uploadOne(fileForm)
                      .subscribe((event) => {
                        if (event.type === HttpEventType.UploadProgress) {
                          const percentDone = Math.round(
                            (100 * event.loaded) / event.total
                          );
                          this.inProgressValExt = percentDone;
                        } else if (event instanceof HttpResponse) {
                          this.inProgressVal += Math.round(
                            (100 * file.size) / size
                          );
                          resolve(true);
                        }
                      });
                  });
                })
              );
              await deleteSelectedArchivefolderfiles;
              await addArchivefolderfiles;
              // await putArchivefolderForm;
            }
            if (!this.last && next === true) {
              this.goToPage('next');
            }
            this.getData();
            break;
          case 'newdoc': // создание документа
            let newArchivefolder;
            if (this.enableArch) {
              if (
                this.archivefolderForm.value['published'] === true ||
                this.archivefolderForm.value['published'] === '*'
              ) {
                const opisInfo = this.opises.find(
                  (opis) => opis.id === this.archivefolderForm?.value?.opis_id
                );
                const isOpisPublished = opisInfo?.published !== null;

                this.archivefolderForm.patchValue({
                  published: isOpisPublished ? '*' : null,
                  mustPublish: isOpisPublished ? '*' : null,
                  mustUnpublish: isOpisPublished ? null : null,
                  pub_change_date: isOpisPublished ? this.curDate : null,
                });

              } else {
                this.archivefolderForm.patchValue({
                  published: null,
                  mustPublish: null,
                  mustUnpublish: null,
                  pub_change_date: null,
                });
              }
              let addArchivefolderForm = this.archivefolderService.postOne(
                this.archivefolderForm.getRawValue()
              );
              newArchivefolder = await addArchivefolderForm;
              this.selectedArchivefolder = await addArchivefolderForm;
              let addArchivefolderlinks = Promise.all(
                this.links.map(async (link) => {
                  let addArchivefolderlink =
                    await this.archivefolderlinkService.postOne({
                      archivefolder_id1: newArchivefolder.id,
                      archivefolder_id2: link.val.id,
                      link_type: link.link_type,
                    });
                })
              );
              await addArchivefolderlinks;
            }
            if (this.enableScan) {
              let addArchivefolderfiles = Promise.all(
                this.files.map(async (file) => {
                  let fileForm: FormData = new FormData();
                  fileForm.append(
                    'archivefolder_id',
                    newArchivefolder.id.toString()
                  );
                  fileForm.append('archivefile', file);
                  let addArchivefolderfile =
                    await this.archivefolderfileService.postOne(fileForm);
                  this.inProgressVal += 100 / this.files.length;
                })
              );
              await addArchivefolderfiles;
            }
            this.filtersService.tempOpisid =
              this.archivefolderForm.value.opis_id;
            if (create === true) {
              let tempOpisid = this.archivefolderForm.value.opis_id;
              this.getData();
              this.archivefolderForm.patchValue({
                opis_id: tempOpisid,
              });
              let posY =
                document.documentElement.scrollTop || document.body.scrollTop;
              const interval = setInterval(() => {
                posY -= 20;
                document.documentElement.scrollTo(0, posY);
                if (posY <= 0) {
                  clearInterval(interval);
                }
              });
            } else {
              this.router.navigate([`/sys/arch/edit/${newArchivefolder.id}`]);
              this.id = newArchivefolder.id;
            }
            break;
          default:
        }
        if (!silent) {
          this.openSnackBar(`Сохранено.`, 'Ok');
        }
      } catch (err) {
        resolve(false);
        if (err.status === 401) {
          console.error(`Ошибка авторизации`);
          this.authService.logout();
        } else if (err.status === 409) {
          this.archivefolderForm.controls['delo'].setErrors({
            incorrect: true,
          });
          this.openSnackBar(`${err.error['error']}`, 'Ok');
        } else {
          console.error(`Ошибка ${err}`);
          this.openSnackBar(`Ошибка ${err.status}`, 'Ok');
        }
      } finally {
        resolve(true);
        this.onDelFiles();
        this.links.length = 0;
        this.inProgressVal = 0;
        this.inProgressValExt = 0;
        this.inProgress = false;
        this.inProgressExt = false;
        this.spinnerService.hide();
      }
    });
  }

  onFilesChange(file: FileList) {
    this.files = this.files.concat(file);
  }

  onFilesChangeInput(event) {
    const allowedExtensions = this.allowedExtensions;
    for (let i = 0; i < event.srcElement.files.length; i++) {
      if (
        allowedExtensions.includes(
          event.srcElement.files[i].name.split('.').pop().toLowerCase()
        )
      ) {
        this.files.push(event.srcElement.files[i]);
      }
    }
  }

  onDelFileById(id) {
    this.files.splice(id, 1);
  }

  onDelLinkById(id) {
    this.archivefolders = this.archivefolders.concat(this.links.splice(id, 1));
    this.myControl.patchValue({});
  }

  onDelFiles() {
    this.files.length = 0;
  }

  onOpenFilesInput() {
    this.filesInput.nativeElement.value = '';
    this.filesInput.nativeElement.click();
  }

  onDelArchivefolder() {
    let dialogRef = this.dialog.open(DialogYesNoComponent, {
      data: {
        title: 'Удалить документ?',
        question: ``,
      },
    });
    dialogRef.afterClosed().subscribe(async (result) => {
      if (result === true) {
        this.spinnerService.show();
        try {
          let deleteArchivefolder = this.archivefolderService.deleteOneById(
            this.id
          );
          await deleteArchivefolder;
          await this.getPublicSiteActionType(true);
          this.router.navigate(['/sys/arch/list']);
          this.openSnackBar(`Удалено.`, 'Ok');
        } catch (err) {
          console.error(`Ошибка при удалении документа ${err}`);
          this.openSnackBar(`Ошибка ${err.status}`, 'Ok');
        } finally {
          this.spinnerService.hide();
        }
      }
    });
  }

  toggleCheckToDel(id) {
    this.selectedArchivefolderfiles[id].checkToDel =
      !this.selectedArchivefolderfiles[id].checkToDel;
  }

  toggleCheckToAllDel() {
    try {
      this.selectedArchivefolderfiles.forEach(
        (f) => (f.checkToDel = !f.checkToDel)
      );
      this.isShowDeleteAllFilesButton = !this.isShowDeleteAllFilesButton;
    } catch (err) {
      console.error(`Ошибка при удалении файлов ${err}`);
    }
  }

  toggleCheckToDelLink(id, archivefolderlink) {
    if (
      archivefolderlink.link_type === 1 &&
      archivefolderlink.checkToDel !== true
    ) {
      let dialogRef = this.dialog.open(DialogYesNoComponent, {
        data: {
          title:
            'Разрывая данную связь вы разрываете все связи типа "дополнение" у целевого дела. Продолжить?',
          question: ``,
        },
      });
      dialogRef.afterClosed().subscribe(async (result) => {
        if (result === true) {
          this.archivefolderlinks[id].checkToDel =
            !this.archivefolderlinks[id].checkToDel;
        }
      });
    } else {
      this.archivefolderlinks[id].checkToDel =
        !this.archivefolderlinks[id].checkToDel;
    }
  }

  async onDownloadArchivefolderfile(id) {
    this.spinnerService.show();
    let fileName = '';
    for (let i = 0; i < this.selectedArchivefolderfiles.length; i++) {
      if (this.selectedArchivefolderfiles[i]) {
        if (id === this.selectedArchivefolderfiles[i].id) {
          fileName = this.selectedArchivefolderfiles[i].archivefile.filename;
        }
      }
    }
    try {
      let blob = this.archivefolderfileService.downloadOneById(id);
      this.inProgress = true;
      this.inProgressVal = 0;
      blob.subscribe((event) => {
        if (event.type === HttpEventType.DownloadProgress) {
          const percentDone = Math.round((100 * event.loaded) / event.total);
          this.inProgressVal = percentDone;
        } else if (event instanceof HttpResponse) {
          this.inProgress = false;
          this.inProgressVal = 0;
          this.spinnerService.hide();
          importedSaveAs(event.body, fileName);
        }
      });
    } catch (err) {
      this.globalService.exceptionHandling(err);
      this.spinnerService.hide();
      this.inProgress = false;
      this.inProgressVal = 0;
      this.globalService.hasError.next(false);
    }
  }

  async onSetStatus(val) {
    let dialogRef = this.dialog.open(DialogYesNoComponent, {
      data: {
        title: 'Установить для документа статус',
        question: `"${
          this.statusTypes.find((item) => item.key === val).value
        }"?`,
      },
    });
    dialogRef.afterClosed().subscribe(async (result) => {
      if (result === true) {
        this.archivefolderForm.patchValue({
          status: val,
        });
        if (this.intType === 'newdoc' || this.filtersService.newDoc === true) {
          let timer;
          try {
            let res = await this.onSaveArchivefolderForm(true);
            if (res === true) {
              timer = setTimeout(() => {
                this.router.navigate([`/sys/arch/edit/newdoc`]);
              }, 500);
            }
          } catch (err) {
            return;
          }
        } else {
          if (val === 1) {
            try {
              let res = await this.onSaveArchivefolderForm(false, true);
            } catch (err) {
              return;
            }
          } else if (val === 2) {
            try {
              let res = await this.onSaveArchivefolderForm(false, false);
            } catch (err) {
              return;
            }
          } else if (val === 3) {
            this.archivefolderForm.patchValue({
              page_cnt: this.selectedArchivefolderfiles.length,
              spec_page_cnt: this.countTiff,
            });
            try {
              let res = await this.onSaveArchivefolderForm(false, true);
            } catch (err) {
              return;
            }
          }
        }
      }
    });
  }

  isSaveValid(): boolean {
    if (!this.loggedInUser) {
      return false;
    } else if (this.loggedInUser.isadmin) {
      return this.archivefolderForm.valid ? true : false;
    }
    if (this.enableScan === false && this.enableArch === false) {
      return false;
    }
    let res = false;
    switch (this.loggedInUser.usertype) {
      case 3:
      case 1: // архивист
        res = this.archivefolderForm.valid;
        break;
      case 2: // сканировщик
        res =
          this.files.length > 0 ||
          this.selectedArchivefolderfiles.filter(
            (selectedArchivefolderfile) => selectedArchivefolderfile.checkToDel
          ).length > 0 ||
          this.archivefolderForm.dirty
            ? true
            : false;
        break;
      default:
    }
    return res;
  }

  isSaveValidClean(): boolean {
    if (!this.loggedInUser) {
      return false;
    } else if (this.loggedInUser.isadmin) {
      return this.archivefolderForm.valid ? true : false;
    }
    if (this.enableScan === false && this.enableArch === false) {
      return false;
    }
    let res = false;
    switch (this.loggedInUser.usertype) {
      case 1: // архивист
        res = this.archivefolderForm.valid;
        break;
      case 2: // сканировщик
        res = true;
        break;
      default:
    }
    return res;
  }

  async onAddArchivefolderlink() {
    let id = this.myControl.value.id;
    let l = this.archivefolderlinks.find(
      (link) => link.archivefolder_id2 === id || link.archivefolder_id1 === id
    );
    if (!l) {
      this.links.push({ val: this.myControl.value, link_type: this.link_type }); // link_type
      this.archivefolders = this.archivefolders.filter(
        (item) => item.id !== this.myControl.value.id
      );
    } else {
      this.openSnackBar(`Связь с делом уже установлена`, 'Ok');
    }
    this.myControl.patchValue({});
  }

  displayFn(user?: Archivefolder): string | undefined {
    return user ? user.name : undefined;
  }

  private _filter(name: string): Archivefolder[] {
    const filterValue = name.toLowerCase();
    return !this.archivefolders
      ? null
      : this.archivefolders.filter(
          (archivefolder) =>
            archivefolder.name.toLowerCase().indexOf(filterValue) !== -1
        );
  }

  async onMove(str) {
    this.goToPage(str);
  }

  async goToPage(str) {
    if (str === 'next') {
      if (this.nextId && this.nextId) {
        this.router.navigate([`/sys/arch/edit/${this.nextId}`]);
        this.id = this.nextId;
      }
    } else if (str === 'previous') {
      if (this.previousId) {
        this.router.navigate([`/sys/arch/edit/${this.previousId}`]);
        this.id = this.previousId;
      }
    }
  }

  async getPage() {
    if (this.intType === 'newdoc') {
      return;
    }
    try {
      let idNext = this.archivefolderService.getnextkey(
        this.id,
        this.filtersService.archivefolderListFilter.searchStr,
        this.filtersService.archivefolderListFilter.searchOpis,
        this.filtersService.archivefolderListFilter.searchOrder,
        this.filtersService.archivefolderListFilter.searchStatus,
        this.filtersService.archivefolderListFilter.searchUnPhoto,
        this.filtersService.archivefolderListFilter.searchUnDoc,
        this.filtersService.archivefolderListFilter.searchAct,
        this.filtersService.archivefolderListFilter.searchMem,
        this.filtersService.archivefolderListFilter.searchArt,
        this.filtersService.archivefolderListFilter.searchDig,
        this.filtersService.archivefolderListFilter.searchtxt,
        this.filtersService.archivefolderListFilter.searchflds,
        this.filtersService.archivefolderListFilter.descrip_type,
        this.filtersService.archivefolderListFilter.filterExact
      );
      let idPrevious = this.archivefolderService.getpreviouskey(
        this.id,
        this.filtersService.archivefolderListFilter.searchStr,
        this.filtersService.archivefolderListFilter.searchOpis,
        this.filtersService.archivefolderListFilter.searchOrder,
        this.filtersService.archivefolderListFilter.searchStatus,
        this.filtersService.archivefolderListFilter.searchUnPhoto,
        this.filtersService.archivefolderListFilter.searchUnDoc,
        this.filtersService.archivefolderListFilter.searchAct,
        this.filtersService.archivefolderListFilter.searchMem,
        this.filtersService.archivefolderListFilter.searchArt,
        this.filtersService.archivefolderListFilter.searchDig,
        this.filtersService.archivefolderListFilter.searchtxt,
        this.filtersService.archivefolderListFilter.searchflds,
        this.filtersService.archivefolderListFilter.descrip_type,
        this.filtersService.archivefolderListFilter.filterExact
      );

      this.nextId = +(await idNext).nextkey;
      this.previousId = +(await idPrevious).previouskey;
      this.onDelFiles();
      this.links.length = 0;
    } catch (err) {
      this.globalService.exceptionHandling(err);
    }
  }

  inputLink() {
    clearTimeout(this.timerId);
    this.timerId = setTimeout(async () => {
      this.archivefolders = await this.archivefolderService.getWithFilter(
        100,
        this.myControl.value
      );
      if (this.archivefolders) {
        this.archivefolders.map((archivefolder) => {
          let _opis = this.opises.find(
            (opis) => opis.id === archivefolder.opis_id
          );
          archivefolder.opis = !_opis ? null : _opis.opis_name;
          archivefolder.fond = !_opis ? null : _opis.fond;
        });
      }
      if (this.archivefolders) {
        try {
          this.archivefolders = this.archivefolders.filter(
            (archivefolders) => archivefolders.id !== this.id
          );
          this.links.map((link) => {
            this.archivefolders = this.archivefolders.filter(
              (archivefolders) => archivefolders.id !== link.val.id
            );
          });
          this.filteredArchivefolders = this.myControl.valueChanges.pipe(
            startWith<string | Archivefolder>(''),
            map((value) =>
              typeof value === 'string' ? value : !value ? null : value.name
            ),
            map((name) =>
              name ? this._filter(name) : [...this.archivefolders]
            )
          );
        } catch {}
      }
    }, 500);
  }

  getSearchOpisId() {
    this.searchOpisId.length = 0;
    let _opis = [];
    if (this.filtersService.archivefolderListFilter.searchOpis.length === 0) {
      _opis = this.opises;
    } else {
      this.filtersService.archivefolderListFilter.searchOpis.map((item) => {
        _opis = _opis.concat(
          this.opises.filter((_item) => _item.opis_name === item)
        );
      });
    }
    let _fond = [];
    if (this.filtersService.archivefolderListFilter.searchFond.length === 0) {
      _fond = _opis;
    } else {
      this.filtersService.archivefolderListFilter.searchFond.map((item) => {
        _fond = _fond.concat(_opis.filter((_item) => _item.fond === item));
      });
    }
    _fond.map((item) => {
      this.searchOpisId.push(item.id);
    });
  }

  onBack() {
    this.location.back();
  }

  async onDownloadZip() {
    let opis = this.opisItems.find(
      (item) => item.id === this.archivefolderForm.getRawValue().opis_id
    );
    let zipName = 'name.zip';
    if (opis) {
      zipName = `${opis.fond}-${opis.opis_name}-${
        this.archivefolderForm.getRawValue().delo
      }.zip`;
    }
    try {
      let blob = this.archivefolderfileService.getZip(this.id);
      this.loaded = 0;
      this.globalService.loadedStr.next('');
      this.openSnackBar('Архив загружается', 'Ок');
      blob.subscribe((event) => {
        if (event.type === HttpEventType.DownloadProgress) {
          this.loaded = Math.round(event.loaded / 1000000);
          this.globalService.loadedStr.next(`Загружено ${this.loaded} мб.`);
        } else if (event instanceof HttpResponse) {
          this.loaded = 0;
          this.globalService.loadedStr.next('');
          importedSaveAs(event.body, zipName);
          this.openSnackBar('Загрузка архива завершена', 'Ок');
        }
      });
    } catch (err) {
      this.globalService.exceptionHandling(err);
      this.inProgressText = false;
      this.loaded = 0;
    }
  }

  async onDownloadPreviewZip() {
    let opis = this.opisItems.find(
      (item) => item.id === this.archivefolderForm.getRawValue().opis_id
    );
    let zipName = 'name.zip';
    if (opis) {
      zipName = `${opis.fond}-${opis.opis_name}-${
        this.archivefolderForm.getRawValue().delo
      }.zip`;
    }
    const archiveNumber = Math.ceil(
      this.selectedArchivefolderfiles?.length / 100
    );

    const downloadPortionOfFiles = async (i?: number) => {
      try {
        const blob = this.archivefolderfileService.getPreviewZip(this.id, i);
        this.loaded = 0;
        this.globalService.loadedStr.next('');
        this.openSnackBar('Архив загружается', 'Ок');
        blob.subscribe(async (event) => {
          if (event.type === HttpEventType.DownloadProgress) {
            this.loaded = Math.round(event.loaded / 1000000);
            this.globalService.loadedStr.next(`Загружено ${this.loaded} мб.`);
          } else if (event instanceof HttpResponse) {
            this.loaded = 0;
            importedSaveAs(event.body, zipName);
            this.globalService.loadedStr.next('');
            this.openSnackBar('Загрузка архива завершена', 'Ок');
            if (i !== archiveNumber) {
              downloadPortionOfFiles(i + 1);
            }
          }
        });
      } catch (err) {
        this.globalService.exceptionHandling(err);
        this.inProgressText = false;
        this.loaded = 0;
      }
    };
    downloadPortionOfFiles(1);
  }

  getLink_typeByKey(key: number) {
    let link_type = this.link_types.find((item) => item.key === key);
    return link_type ? link_type : {};
  }

  onLinkMarchive(id) {
    this.spinnerService.show();
    this.router.navigate([`/sys/arch/edit/${id}`]);
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 10000,
    });
  }

  onRenameFile(id: number, fileName: string) {
    let dialogRef = this.dialog.open(DialogSetstringComponent, {
      data: {
        title: 'Введите название файла',
        question: ``,
        str: fileName,
      },
    });
    dialogRef.afterClosed().subscribe(async (result) => {
      if (typeof result === 'string') {
        try {
          await this.archivefolderfileService.putFilename(id, result);
        } catch (err) {
          this.globalService.exceptionHandling(err);
        } finally {
          this.getData();
        }
      }
    });
  }

  linkUrl() {
    let str = '';
    if (this.archivefolderForm.getRawValue().url.indexOf('//') === -1) {
      str = '//';
    }
    str += this.archivefolderForm.getRawValue().url;
    window.open(str, '_blank');
  }

  resetPub_date() {
    this.archivefolderForm.patchValue({
      pub_date: '',
    });
  }

  async onOrientation(obj) {
    this.spinnerService.show();
    let idx = this.selectedArchivefolderfiles.findIndex(
      (item) => item.id === obj.id
    );
    this.selectedArchivefolderfiles[idx].orientation = obj.val;
    try {
      await this.archivefolderfileService.putOneById(obj.id, {
        orientation: obj.val,
      });
    } catch (err) {
      this.globalService.exceptionHandling(err);
    } finally {
      this.spinnerService.hide();
    }
  }

  async addUsaget() {
    await this.onSaveArchivefolderForm(false, false, true);
    this.router.navigate([`/sys/arch/usagetedit/${this.id}`]);
  }

  async editUsaget(id) {
    await this.onSaveArchivefolderForm(false, false, true);
    this.router.navigate([`/sys/arch/usagetedit/${this.id}/${id}`]);
  }

  stop(e) {
    e.stopPropagation();
  }

  getUstypeById(id: number) {
    return this.us_types.find((item) => item.id === id);
  }

  async drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    }
    try {
      let newArr;
      newArr = await this.moveordernomService.move(
        'usage_t',
        this.selectedUsagets[event.currentIndex].id,
        event.currentIndex + 1
      );
      this.selectedUsagets = [...newArr];
    } catch (err) {
      this.globalService.exceptionHandling(err);
    } finally {
    }
  }

  onLinkDocuments() {
    this.filtersService.selectionSourtFilter = new SourtFilter();
    let opis = this.opisItems.find(
      (item) => item.id == this.archivefolderForm.getRawValue().opis_id
    );
    if (opis) {
      this.filtersService.selectionSourtFilter.fund = `${opis.fond}`;
      this.filtersService.selectionSourtFilter.list_no = `${opis.opis_name}`;
    }
    this.filtersService.selectionSourtFilter.file_no =
      this.archivefolderForm.getRawValue().delo;

    this.filtersService.selectionSourtFilter.file_no_ext =
      this.archivefolderForm.getRawValue().delo_extra;
    if (
      !this.archivefolderForm.getRawValue().delo_extra ||
      this.archivefolderForm.getRawValue().delo_extra === ''
    ) {
      this.filtersService.selectionSourtFilter.file_no_ext_noneed = true;
    }
    this.router.navigate(['sys', 'person', 'doclist']);
  }

  async onLinkFromServer() {
    let files = [];
    try {
      this.spinnerService.show();
      files = await this.marchfilecopyService.getAll();
      if (Array.isArray(files)) {
        files.forEach((file) => {
          let f = this.selectedArchivefolderfiles.find(
            (item) =>
              item.archivefile.filename.toLowerCase() ===
              file.name.toLowerCase()
          );
          if (f) {
            file.disabled = true;
          }
          file.selected = false;
        });
      }
    } catch (err) {
      this.globalService.exceptionHandling(err);
    } finally {
      this.spinnerService.hide();
    }
    let dialogRef = this.dialog.open(DialogFileListComponent, {
      panelClass: 'app-dialog-extend',
      data: {
        files,
      },
    });
    dialogRef.afterClosed().subscribe(async (result) => {
      if (result) {
        try {
          this.spinnerService.show();
          let res = await this.marchfilecopyService.postOne({
            archivefolder_id: this.id,
            files_name: result.map((item) => item.name),
          });
          let dialogRef = this.dialog.open(DialogOkComponent, {
            data: {
              title:
                'Копирование началось, по его окончанию файлы будут прикреплены к делу.',
            },
          });
        } catch (err) {
          let dialogRef = this.dialog.open(DialogOkComponent, {
            data: {
              title:
                'Произошла ошибка при подготовке к копированию, проверьте правильность введеных данных.',
            },
          });
        } finally {
          this.spinnerService.hide();
        }
      }
    });
  }

  async generatedelorange() {
    this.spinnerService.show();
    try {
      let dates = await this.archivefolderService.generatedelorange(
        this.selectedArchivefolder.fond,
        this.selectedArchivefolder.opis_name,
        this.selectedArchivefolder.delo,
        this.selectedArchivefolder.delo_extra
      );
      if (dates.yearmin) {
        this.archivefolderForm.patchValue({ doc_begin: dates.yearmin });
      }
      if (dates.yearmax) {
        this.archivefolderForm.patchValue({ doc_end: dates.yearmax });
      }
    } catch (err) {
    } finally {
      this.spinnerService.hide();
    }
  }

  copyFilename(file) {
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = file.archivefile.file;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
  }
}
