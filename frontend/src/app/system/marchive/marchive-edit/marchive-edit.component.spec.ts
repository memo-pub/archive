import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MatDialogModule} from '@angular/material/dialog';
import {DragDropModule} from '@angular/cdk/drag-drop';

import { MarchiveEditComponent } from './marchive-edit.component';

describe('MarchiveEditComponent', () => {
    let component: MarchiveEditComponent;
    let fixture: ComponentFixture<MarchiveEditComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [MarchiveEditComponent],
            imports: [RouterTestingModule, MatSnackBarModule, HttpClientTestingModule, MatDialogModule, DragDropModule],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(MarchiveEditComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
