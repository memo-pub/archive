import {
  Component,
  OnInit,
  HostListener,
  OnDestroy,
  ViewChild,
} from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { saveAs as importedSaveAs } from 'file-saver';
import { Muser } from '../../../shared/models/muser.model';
import { Opis } from '../../../shared/models/opis.model';
import { AuthService } from '../../../shared/services/auth.service';
import { MuserService } from '../../shared/services/muser.service';
import { OpisService } from '../../shared/services/opis.service';
import { DialogYesNoComponent } from '../../shared/components/dialog-yes-no/dialog-yes-no.component';
import { Fond, fond_width } from 'src/app/shared/models/fond.model';
import { FondService } from '../../shared/services/fond.service';
import { isNullOrUndefined } from 'util';
import { DialogSaveNoComponent } from '../../shared/components/dialog-save-no/dialog-save-no.component';
import { Subscription } from 'rxjs';
import { GlobalService } from 'src/app/shared/services/global.service';
import { ElementRef } from '@angular/core';
import { FondfileService } from '../../shared/services/fondfile.service';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { DialogSetstringComponent } from '../../shared/components/dialog-setstring/dialog-setstring.component';
import { DialogPdfViewerComponent } from '../../shared/components/dialog-pdf-viewer/dialog-pdf-viewer.component';
import { PublicSiteActionsService } from '../../shared/services/publicsiteactions.service';
import { ALLOWED_EXTENSIONS } from '../../shared/constants/allowed-extensions';

@Component({
  selector: 'app-fond-edit',
  templateUrl: './fond-edit.component.html',
  styleUrls: ['./fond-edit.component.css'],
})
export class FondEditComponent implements OnInit, OnDestroy {
  constructor(
    private authService: AuthService,
    private muserService: MuserService,
    private fondService: FondService,
    private opisService: OpisService,
    private fondfileService: FondfileService,
    private publicSiteActions: PublicSiteActionsService,
    private globalService: GlobalService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private spinnerService: NgxSpinnerService,
    public dialog: MatDialog,
    public snackBar: MatSnackBar
  ) {
    this.activeRouterObserver = this.activatedRoute.params.subscribe(
      (param) => {
        this.id = param.id;
        if (param.id === 'newdoc') {
          this.intType = 'newdoc';
        } else {
          this.id = +param.id;
          this.intType = 'editdoc';
        }
      }
    );
  }
  fond_width = fond_width;

  id: number;
  intType: string | any = ''; // тип интерфейса
  loggedInUser: Muser;
  fondForm: FormGroup;
  files = [];
  postfixTypes: any[];
  statusTypes: any[];

  selectedFond: Fond;
  selectedFondOldData: Fond;
  opisItems: Opis[];
  currDate: string;
  timer;
  viewOnly: boolean;
  activeRouterObserver: Subscription;
  @ViewChild('filesInput', { static: true }) filesInput: ElementRef;

  inProgressVal = 0;
  inProgressValExt = 0;
  selectedArchivefolderfiles = [];
  inProgress = false;
  allowedExtensions = ALLOWED_EXTENSIONS.fond;
  ngOnDestroy(): void {
    // this.activeRouterObserver.unsubscribe();
  }

  @HostListener('window:keydown', ['$event'])
  onKeyPress(event: KeyboardEvent) {
    if ((event.ctrlKey || event.metaKey) && event.keyCode == 83) {
      event.preventDefault();
      if (this.intType !== 'myprofile' && !this.fondForm.invalid) {
        this.onSaveFondForm();
      }
    }
  }

  async ngOnInit() {
    const userType = this.authService.getCurUser().usertype;
    userType === 2 || userType === 4
      ? (this.viewOnly = true)
      : (this.viewOnly = false);
    this.currDate = new Date().toISOString().split('T')[0];
    this.fondForm = new FormGroup({
      fond: new FormControl({ value: null, disabled: this.viewOnly }, [
        Validators.required,
      ]),
      description: new FormControl({ value: null, disabled: this.viewOnly }),
      description_long: new FormControl({
        value: null,
        disabled: this.viewOnly,
      }),
      doc_begin: new FormControl({ value: null, disabled: this.viewOnly }, [
        Validators.maxLength(4),
        Validators.max(9999),
      ]),
      doc_end: new FormControl({ value: null, disabled: this.viewOnly }, [
        Validators.maxLength(4),
        Validators.max(9999),
      ]),
      form_date: new FormControl({ value: null, disabled: this.viewOnly }),
      access_lim: new FormControl({ value: null, disabled: this.viewOnly }),
      help: new FormControl({ value: null, disabled: this.viewOnly }),
      storage_capacity: new FormControl({
        value: null,
        disabled: this.viewOnly,
      }),
      creators_name: new FormControl({ value: null, disabled: this.viewOnly }),
      admin_biog_history: new FormControl({
        value: null,
        disabled: this.viewOnly,
      }),
      arch_history: new FormControl({ value: null, disabled: this.viewOnly }),
      donation_source: new FormControl({
        value: null,
        disabled: this.viewOnly,
      }),
      mat_organ_system: new FormControl({
        value: null,
        disabled: this.viewOnly,
      }),
      archivist_rmk: new FormControl({ value: null, disabled: this.viewOnly }),
      descrip_date: new FormControl({ value: null, disabled: this.viewOnly }),
      published: new FormControl({ value: null, disabled: this.viewOnly }),
      mustPublish: new FormControl(null),
      mustUnpublish: new FormControl(null),
      pub_change_date: new FormControl(null),
    });
    try {
      let loggedInUser = this.muserService.getOneById(
        this.authService.getCurUser().id
      );
      this.loggedInUser = await loggedInUser;
    } catch (err) {
      this.globalService.exceptionHandling(err);
    }
    await this.getData();
  }

  async getData() {
    this.spinnerService.show();
    try {
      switch (this.intType) {
        case 'editdoc': // редактирование описи
          let selectedFond = this.fondService.getOneById(this.id);
          let selectedArchivefolderfiles =
            this.fondfileService.getAllByParentId(this.id, true);
          this.selectedArchivefolderfiles = isNullOrUndefined(
            await selectedArchivefolderfiles
          )
            ? []
            : await selectedArchivefolderfiles;

          this.selectedArchivefolderfiles.forEach(
            (selectedArchivefolderfile) => {
              selectedArchivefolderfile['checkToDel'] = false;
            }
          );
          this.selectedArchivefolderfiles.sort((a, b) =>
            a.fondfile.filename > b.fondfile.filename ? 1 : -1
          );
          this.selectedFond = await selectedFond;
          this.selectedFondOldData = await selectedFond;
          this.fondForm.patchValue({
            fond: this.selectedFond.fond,
            description: this.selectedFond.description,
            description_long: this.selectedFond.description_long,
            doc_begin: this.selectedFond.doc_begin,
            doc_end: this.selectedFond.doc_end,
            form_date: this.selectedFond.form_date,
            access_lim: this.selectedFond.access_lim,
            help: this.selectedFond.help,
            storage_capacity: this.selectedFond.storage_capacity,
            creators_name: this.selectedFond.creators_name,
            admin_biog_history: this.selectedFond.admin_biog_history,
            arch_history: this.selectedFond.arch_history,
            donation_source: this.selectedFond.donation_source,
            mat_organ_system: this.selectedFond.mat_organ_system,
            archivist_rmk: this.selectedFond.archivist_rmk,
            descrip_date: this.selectedFond.descrip_date,
            published: this.selectedFond.published === '*' ? true : false,
            mustPublish: this.selectedFond.mustPublish,
            mustUnpublish: this.selectedFond.mustUnpublish,
            pub_change_date: this.selectedFond.pub_change_date,
          });
          this.getOpisItems();
          break;
        case 'newdoc': // создание фонда
          this.fondForm.reset();
          this.fondForm.patchValue({
            published: '*',
            mustPublish: '*',
            mustUnpublish: null,
            pub_change_date: this.currDate,
          });
          break;
        default:
      }
    } catch (err) {
      console.log(err);
      this.globalService.exceptionHandling(err);
    } finally {
      this.spinnerService.hide();
    }
  }

  async getOpisItems() {
    if (!isNullOrUndefined(this.fondForm.getRawValue().fond)) {
      try {
        let items = this.opisService.getByFond(
          this.fondForm.getRawValue().fond
        );
        this.opisItems = isNullOrUndefined(await items) ? [] : await items;
        this.opisItems.sort((a, b) => +a.opis_name - +b.opis_name);
      } catch (err) {
        this.globalService.exceptionHandling(err);
      }
    }
  }

  updateOpisItems() {
    clearTimeout(this.timer);
    if (!isNullOrUndefined(this.fondForm.getRawValue().fond)) {
      this.timer = setTimeout(() => {
        this.getOpisItems();
      }, 300);
    }
  }

  async getPublicSiteActionType(fondForm?) {
    const oldData = this.selectedFondOldData;
    const newData = this.fondForm;
    if (oldData && newData) {
      if (oldData.published !== newData.value.published) {
        const isActionDelete =
          this.fondForm.value['published'] === null ? '*' : null;
        const isActionUpdate =
          this.fondForm.value['published'] === '*' ? '*' : null;
        if (isActionDelete !== isActionUpdate) {
          await this.publicSiteActions.postOne({
            entity_id: this.id,
            entity_type: 3,
            action_delete: isActionDelete,
            action_update: isActionUpdate,
            legacyId: `ID-arch-${this.selectedFond.fond}`,
          });
        } else {
          // fond deleted
          await this.publicSiteActions.postOne({
            entity_id: this.id,
            entity_type: 3,
            action_delete: '*',
            action_update: null,
            legacyId: `ID-arch-${this.selectedFond.fond}`,
          });
        }
      }
    }

    if (!oldData) {
      // added new fond
      await this.publicSiteActions.postOne({
        entity_id: fondForm?.id,
        entity_type: 3,
        action_delete: null,
        action_update: '*',
        legacyId: `ID-arch-${this.selectedFond.fond}`,
      });
    }
  }

  async checkFormChanges(oldData: Fond, newData: Fond) {
    const isChanged = Object.keys(oldData).some(
      (key) => oldData[key] !== newData[key]
    );

    const isPublished = newData.published === '*';

    if (isChanged && isPublished) {
      this.fondForm.patchValue({
        mustPublish: '*',
        mustUnpublish: null,
        pub_change_date: this.currDate,
      });
    }
  }

  async onSaveFondForm(silent = false) {
    this.spinnerService.show();
    try {
      switch (this.intType) {
        case 'editdoc': // редактирование документа
          if (
            this.fondForm.value['published'] === true ||
            this.fondForm.value['published'] === '*'
          ) {
            this.fondForm.patchValue({
              published: '*',
              mustPublish: '*',
              mustUnpublish: null,
              pub_change_date: this.currDate,
            });
          } else {
            this.fondForm.patchValue({
              published: null,
              mustPublish: null,
              mustUnpublish: '*',
              pub_change_date: this.currDate,
            });
          }

          this.checkFormChanges(this.selectedFond, this.fondForm.value);

          let putFondForm = this.fondService.putOneById(
            this.id,
            this.fondForm.value
          );

          let size = 0;
          for (let i = 0; i < this.files.length; i++) {
            size += this.files[i].size;
          }
          let addfiles = Promise.all(
            this.files.map(async (file) => {
              return new Promise((resolve, reject) => {
                let fileForm: FormData = new FormData();
                fileForm.append('fond_id', this.id.toString());
                fileForm.append('fondfile', file);
                this.inProgressValExt = 0;
                this.fondfileService.uploadOne(fileForm).subscribe((event) => {
                  if (event.type === HttpEventType.UploadProgress) {
                    const percentDone = Math.round(
                      (100 * event.loaded) / event.total
                    );
                    this.inProgressValExt = percentDone;
                  } else if (event instanceof HttpResponse) {
                    this.inProgressVal += Math.round((100 * file.size) / size);
                    resolve(true);
                  }
                });
              });
            })
          );
          let deleteSelectedArchivefolderfiles = Promise.all(
            this.selectedArchivefolderfiles.map(
              async (selectedArchivefolderfile) => {
                if (selectedArchivefolderfile.checkToDel) {
                  let deleteSelectedArchivefolderfile =
                    await this.fondfileService.deleteOneById(
                      selectedArchivefolderfile.id
                    );
                }
              }
            )
          );
          await putFondForm;
          await addfiles;
          await deleteSelectedArchivefolderfiles;
          await this.getPublicSiteActionType();
          break;
        case 'newdoc': // создание документа
          if (
            this.fondForm.value['published'] === true ||
            this.fondForm.value['published'] === '*'
          ) {
            this.fondForm.patchValue({
              published: '*',
              mustPublish: '*',
              mustUnpublish: null,
              pub_change_date: this.currDate,
            });
          } else {
            this.fondForm.patchValue({
              published: null,
              mustPublish: null,
              mustUnpublish: null,
              pub_change_date: this.currDate,
            });
          }
          let addFondForm = this.fondService.postOne(this.fondForm.value);
          let _size = 0;
          for (let i = 0; i < this.files.length; i++) {
            _size += this.files[i].size;
          }
          let _addfiles = Promise.all(
            this.files.map(async (file) => {
              return new Promise(async (resolve, reject) => {
                let fileForm: FormData = new FormData();
                fileForm.append('fond_id', (await addFondForm).id.toString());
                fileForm.append('fondfile', file);
                this.inProgressValExt = 0;
                this.fondfileService.uploadOne(fileForm).subscribe((event) => {
                  if (event.type === HttpEventType.UploadProgress) {
                    const percentDone = Math.round(
                      (100 * event.loaded) / event.total
                    );
                    this.inProgressValExt = percentDone;
                  } else if (event instanceof HttpResponse) {
                    this.inProgressVal += Math.round((100 * file.size) / _size);
                    resolve(true);
                  }
                });
              });
            })
          );
          await _addfiles;
          await addFondForm;
          await this.getPublicSiteActionType(await addFondForm);
          break;
        default:
      }
      if (silent === false) {
        this.openSnackBar(`Сохранено.`, 'Ok');
      }
      return true;
    } catch (err) {
      this.globalService.exceptionHandling(err);
      return false;
    } finally {
      this.files.length = 0;
      await this.getData();
      this.fondForm.markAsUntouched();
      this.spinnerService.hide();
    }
  }

  async onDownloadArchivefolderfile(id) {
    this.spinnerService.show();
    let fileName = '';
    for (let i = 0; i < this.selectedArchivefolderfiles.length; i++) {
      if (!isNullOrUndefined(this.selectedArchivefolderfiles[i])) {
        if (id === this.selectedArchivefolderfiles[i].id) {
          fileName = this.selectedArchivefolderfiles[i].fondfile.filename;
        }
      }
    }
    try {
      let blob = this.fondfileService.downloadOneById(id);
      this.inProgress = true;
      this.inProgressVal = 0;
      blob.subscribe((event) => {
        if (event.type === HttpEventType.DownloadProgress) {
          const percentDone = Math.round((100 * event.loaded) / event.total);
          this.inProgressVal = percentDone;
        } else if (event instanceof HttpResponse) {
          this.inProgress = false;
          this.inProgressVal = 0;
          this.spinnerService.hide();
          importedSaveAs(event.body, fileName);
        }
      });
    } catch (err) {
      console.error(`Ошибка ${err}`);
      this.spinnerService.hide();
    }
  }

  toggleCheckToDel(id) {
    this.selectedArchivefolderfiles[id].checkToDel =
      !this.selectedArchivefolderfiles[id].checkToDel;
  }

  async onDelFond() {
    let dialogRef = this.dialog.open(DialogYesNoComponent, {
      data: {
        title: 'Удалить фонд?',
        question: ``,
      },
    });
    dialogRef.afterClosed().subscribe(async (result) => {
      if (result === true) {
        this.spinnerService.show();
        try {
          let resDelFond = await this.fondService.deleteOneById(this.id);
          await this.getPublicSiteActionType();
          this.router.navigate(['/sys/arch/f-list']);
          this.openSnackBar(`Удалено.`, 'Ok');
        } catch (err) {
          console.error(`Ошибка при удалении фонда ${err}`);
          this.globalService.exceptionHandling(err);
        } finally {
          this.spinnerService.hide();
        }
      }
    });
  }

  async canDeactivate() {
    if (this.fondForm.touched && this.fondForm.dirty) {
      let dialogRef = this.dialog.open(DialogSaveNoComponent, {
        data: {
          title: 'Сохранить изменения?',
          question: ``,
        },
      });
      let res = await new Promise((resolve, reject) => {
        dialogRef.afterClosed().subscribe(async (result) => {
          if (result === true) {
            let _res = await this.onSaveFondForm(true);
            if (_res === true) {
              resolve(true);
            } else {
              resolve(false);
            }
          } else if (result === false) {
            resolve(true);
          } else {
            resolve(false);
          }
        });
      });
      return res;
    }
    return true;
  }

  onLinkOpis(opis: number) {
    // this.router.navigate([`/sys/arch/list`], {
    //   queryParams: { opis, fond: this.fondForm.value.fond },
    // });
    this.router.navigate(['sys/arch/o-edit', opis, this.fondForm.value.fond]);
  }

  onAddOpis() {
    this.router.navigate(['/sys/arch/o-edit/newdoc', this.fondForm.value.fond]);
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 10000,
    });
  }

  onOpenFilesInput() {
    this.filesInput.nativeElement.click();
  }

  onFilesChange(file: FileList) {
    this.files = this.files.concat(file);
  }

  onFilesChangeInput(event) {
    const allowedExtensions = this.allowedExtensions;
    for (let i = 0; i < event.srcElement.files.length; i++) {
      if (
        allowedExtensions.includes(
          event.srcElement.files[i].name.split('.').pop().toLowerCase()
        )
      ) {
        this.files.push(event.srcElement.files[i]);
      }
    }
  }

  onDelFileById(id) {
    this.files.splice(id, 1);
  }

  onDelFiles() {
    this.files.length = 0;
  }

  onRenameFile(id: number, fileName: string) {
    let dialogRef = this.dialog.open(DialogSetstringComponent, {
      data: {
        title: 'Введите название файла',
        question: ``,
        str: fileName,
      },
    });
    dialogRef.afterClosed().subscribe(async (result) => {
      if (typeof result === 'string') {
        try {
          await this.fondfileService.putFilename(id, result);
        } catch (err) {
          this.globalService.exceptionHandling(err);
        } finally {
          this.getData();
        }
      }
    });
  }

  async onShowImg(id, selectedArchivefolderfile) {
    this.openPdf(selectedArchivefolderfile);
  }

  openPdf(selectedArchivefolderfile) {
    this.dialog.open(DialogPdfViewerComponent, {
      panelClass: 'app-dialog-extend',
      data: {
        file: selectedArchivefolderfile,
      },
    });
  }
  copyFilename(file) {
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = file.fondfile.file;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
  }
}
