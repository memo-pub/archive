import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MarchiveComponent } from './marchive.component';
import { MarchiveEditComponent } from './marchive-edit/marchive-edit.component';
import { MarchiveListComponent } from './marchive-list/marchive-list.component';
import { OpisEditComponent } from './opis-edit/opis-edit.component';
import { OpisListComponent } from './opis-list/opis-list.component';
import { FilesEditComponent } from './files/files-edit/files-edit.component';
import { FilesListComponent } from './files/files-list/files-list.component';
import { FilesComponent } from './files/files.component';
import { FondListComponent } from './fond-list/fond-list.component';
import { FondEditComponent } from './fond-edit/fond-edit.component';
import { CanDeactivateGuard } from 'src/app/shared/services/can-deactivate.guard';
import { MainGuard } from 'src/app/shared/services/main.guard';
import { UsagetEditComponent } from './usaget-edit/usaget-edit.component';

const routes: Routes = [
  {
    path: '',
    component: MarchiveComponent,
    children: [
      { path: 'edit/:id', component: MarchiveEditComponent },
      {
        path: 'list',
        component: MarchiveListComponent,
        canActivate: [MainGuard],
      },
      {
        path: 'o-edit/:id/:fondId',
        component: OpisEditComponent,
        canActivate: [MainGuard],
      },
      {
        path: 'f-edit/:id',
        component: FondEditComponent,
        canDeactivate: [CanDeactivateGuard],
        canActivate: [MainGuard],
      },
      // {
      //   path: 'o-list',
      //   component: OpisListComponent,
      //   canActivate: [MainGuard],
      // },
      {
        path: 'f-list',
        component: FondListComponent,
        canActivate: [MainGuard],
      },
      {
        path: 'files',
        component: FilesComponent,
        canActivate: [MainGuard],
        children: [
          { path: 'list', component: FilesListComponent },
          { path: 'edit/:id', component: FilesEditComponent },
        ],
      },
      {
        path: 'usagetedit/:archivefolder_id',
        component: UsagetEditComponent,
      },
      {
        path: 'usagetedit/:archivefolder_id/:id',
        component: UsagetEditComponent,
      },
      // { path: 'files-list', component: FilesListComponent },
      // { path: 'files-edit/:id', component: FilesEditComponent },

      { path: '**', component: MarchiveListComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MarchiveRoutingModule {}
