import { Component, OnInit, HostListener, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

import { Muser } from '../../../shared/models/muser.model';
import { AuthService } from '../../../shared/services/auth.service';
import { MuserService } from '../../shared/services/muser.service';
import { Opis } from '../../../shared/models/opis.model';
import { isNullOrUndefined } from 'util';
import { PageEvent, MatPaginator } from '@angular/material/paginator';
import { OpisService } from '../../shared/services/opis.service';
import { Fond } from 'src/app/shared/models/fond.model';
import { FondService } from '../../shared/services/fond.service';
import { GlobalService } from 'src/app/shared/services/global.service';

@Component({
  selector: 'app-fond-list',
  templateUrl: './fond-list.component.html',
  styleUrls: ['./fond-list.component.css'],
})
export class FondListComponent implements OnInit {
  loggedInUser: Muser;
  searchStr = '';
  fondItems: Fond[];
  fondItemsView: Fond[];

  searchOrder = 'byfond';
  postfixFond = '';

  length = 100;
  pageSize = 100;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  pageEvent: PageEvent;
  start = 0;
  end = 0;

  /**Для пагинатора */
  page = 1;
  limit = 100;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private authService: AuthService,
    private muserService: MuserService,
    private fondService: FondService,
    private globalService: GlobalService,
    private router: Router,
    private spinnerService: NgxSpinnerService
  ) {}

  async ngOnInit() {
    this.spinnerService.show();
    try {
      let loggedInUser = this.muserService.getOneById(
        this.authService.getCurUser().id
      );
      this.loggedInUser = await loggedInUser;
      let fondItems = this.fondService.getAll();
      this.fondItems = isNullOrUndefined(await fondItems)
        ? []
        : await fondItems;
      this.fondItems.sort((a, b) => (a.id > b.id ? 1 : -1));
      this.length = this.fondItems.length;
      if (this.length > 100) {
        this.pageSizeOptions.push(this.length);
      }
      this.end = this.pageSize;
      this.setOrgerStr();
    } catch (err) {
      this.globalService.exceptionHandling(err);
    } finally {
      this.spinnerService.hide();
    }
  }

  onAddFonditem() {
    this.router.navigate(['/sys/arch/f-edit/newdoc']);
  }

  onEditFonditem(id) {
    // console.log(`/sys/arch/f-edit/${id}`);
    this.router.navigate([`/sys/arch/f-edit/${id}`]);
  }

  updateData() {
    if (this.searchStr.length > 0) {
      this.start = 0;
      // this.end = this.paginator.pageSize;
      this.end = this.limit;
      // this.paginator.firstPage();
      this.page = 1;
    } else {
      // this.start = this.paginator.pageSize * this.paginator.pageIndex;
      // this.end = this.start + this.paginator.pageSize;
      this.start = this.limit * (this.page - 1);
      this.end = this.start + this.limit;
    }
  }

  setPageSizeOptions(event: PageEvent) {
    this.start = event.pageSize * event.pageIndex;
    this.end = this.start + event.pageSize;
    return event;
  }

  goToPage(n: number): void {
    this.page = n;
    this.updatePage();
  }

  onNext(): void {
    this.page++;
    this.updatePage();
  }

  onPrev(): void {
    this.page = 1;
    this.updatePage();
  }

  onNext10(n = 1): void {
    this.page += 10 * n;
    this.updatePage();
  }

  onPrev10(n = 1): void {
    this.page -= 10 * n;
    this.updatePage();
  }

  selectOrder(type: string) {
    switch (type) {
      case 'byfond':
        this.searchOrder =
          this.searchOrder === 'byfond' ? 'byfonddesc' : 'byfond';
        break;
    }
    this.setOrgerStr();
  }

  setOrgerStr() {
    switch (this.searchOrder) {
      case 'byfond':
        this.postfixFond =
          '<i class="fa fa-sort-amount-asc" aria-hidden="true"></i>';
        this.fondItems.sort((a, b) => a.fond - b.fond);
        break;
      case 'byfonddesc':
        this.postfixFond =
          '<i class="fa fa-sort-amount-desc" aria-hidden="true"></i>';
        this.fondItems.sort((a, b) => b.fond - a.fond);
        break;
    }
  }

  async updatePage() {
    this.start = this.limit * (this.page - 1);
    this.end = this.start + this.limit;
  }
}
