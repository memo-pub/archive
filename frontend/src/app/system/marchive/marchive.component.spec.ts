import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MarchiveComponent } from './marchive.component';

describe('MarchiveComponent', () => {
  let component: MarchiveComponent;
  let fixture: ComponentFixture<MarchiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarchiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarchiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
