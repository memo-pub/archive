import { Component, OnInit, HostListener } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { isNullOrUndefined } from 'util';
import { NgModel } from '@angular/forms';

import { ArchivefolderService } from './../../shared/services/archivefolder.service';
import { AuthService } from '../../../shared/services/auth.service';
import { Muser } from '../../../shared/models/muser.model';
import { Archivefolder } from '../../../shared/models/archivefolder.model';
import { GlobalService } from '../../../shared/services/global.service';
import { Opis } from '../../../shared/models/opis.model';
import { OpisService } from '../../shared/services/opis.service';
import { FiltersService } from '../../shared/services/filters.service';
import { DiskinfoService } from '../../shared/services/diskinfo.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ListOfT } from 'src/app/shared/models/listoft.model';
import { ListoftService } from '../../shared/services/listoft.service';
import { MatDialog } from '@angular/material/dialog';
import { DialogFileLoaderComponent } from '../../shared/components/dialog-file-loader/dialog-file-loader.component';

@Component({
  selector: 'app-marchive-list',
  templateUrl: './marchive-list.component.html',
  styleUrls: ['./marchive-list.component.css'],
})
export class MarchiveListComponent implements OnInit {
  loggedInUser: Muser;
  searchOpisId = [];
  archivefolders: Archivefolder[];
  opises: Opis[];
  opisesFond = [];
  opisesName = [];

  statusTypes: any[];
  descrip_types: ListOfT[] = [];
  postfixTypes: any[];

  selectedFond: any[];
  selectedOpis: any[];

  postfixId = '';
  postfixName = '';
  postfixShifr = '';

  /**Для пагинатора */
  total = 0;
  limit = 100;

  searchflds: any[];

  timer;

  constructor(
    public filtersService: FiltersService,
    private authService: AuthService,
    private archivefolderService: ArchivefolderService,
    private listoftService: ListoftService,
    private opisService: OpisService,
    private diskinfoService: DiskinfoService,
    private globalService: GlobalService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private spinnerService: NgxSpinnerService,
    private dialog: MatDialog,
    public snackBar: MatSnackBar
  ) {}

  deselectAll(select: NgModel, select1?: NgModel) {
    select.update.emit([]);
    if (!isNullOrUndefined(select1)) {
      select1.update.emit([]);
    }
  }

  async ngOnInit() {
    this.spinnerService.show();
    // this.page = this.globalService.marchivelistPage;
    this.statusTypes = this.globalService.statusTypes;
    this.descrip_types = await this.listoftService.getWithFilter('DESCRIP_TYPE');
    this.descrip_types.sort((a, b) => {
      if (a.name_ent < b.name_ent) {
        return -1;
      }
      if (a.name_ent > b.name_ent) {
        return 1;
      }
      return 0;
    });
    this.postfixTypes = this.globalService.postfixTypes;
    this.searchflds = this.globalService.searchflds;
    this.filtersService.newDoc = false;
    this.filtersService.tempOpisid = null;
    // this.searchStr = '';
    // this.searchFond = isNullOrUndefined(this.globalService.marchivelistFilter)
    //   ? []
    //   : this.globalService.marchivelistFilter.searchFond;
    // this.searchOpis = isNullOrUndefined(this.globalService.marchivelistFilter)
    //   ? []
    //   : this.globalService.marchivelistFilter.searchOpis;

    // this.searchStatus = isNullOrUndefined(this.globalService.marchivelistFilter)
    //   ? null
    //   : this.globalService.marchivelistFilter.searchStatus;
    // this.searchUnPhoto = isNullOrUndefined(
    //   this.globalService.marchivelistFilter
    // )
    //   ? null
    //   : this.globalService.marchivelistFilter.searchUnPhoto;
    // this.searchUnDoc = isNullOrUndefined(this.globalService.marchivelistFilter)
    //   ? null
    //   : this.globalService.marchivelistFilter.searchUnDoc;
    // this.searchAct = isNullOrUndefined(this.globalService.marchivelistFilter)
    //   ? null
    //   : this.globalService.marchivelistFilter.searchAct;
    // this.searchMem = isNullOrUndefined(this.globalService.marchivelistFilter)
    //   ? null
    //   : this.globalService.marchivelistFilter.searchMem;
    // this.searchArt = isNullOrUndefined(this.globalService.marchivelistFilter)
    //   ? null
    //   : this.globalService.marchivelistFilter.searchArt;
    // this.searchDig = isNullOrUndefined(this.globalService.marchivelistFilter)
    //   ? null
    //   : this.globalService.marchivelistFilter.searchDig;
    // this.searchOrder = isNullOrUndefined(this.globalService.marchivelistFilter)
    //   ? []
    //   : this.globalService.marchivelistFilter.searchOrder;
    /**Получение фонда и описи из словаря фонда */
    if (!isNullOrUndefined(this.activatedRoute.snapshot.queryParams['opis'])) {
      this.filtersService.archivefolderListFilter.searchOpis = [
        +this.activatedRoute.snapshot.queryParams['opis'],
      ];
    }
    if (!isNullOrUndefined(this.activatedRoute.snapshot.queryParams['fond'])) {
      this.filtersService.archivefolderListFilter.searchFond = [
        +this.activatedRoute.snapshot.queryParams['fond'],
      ];
    }
    this.setOrgerStr();
    try {
      this.loggedInUser = this.authService.getCurUser();
      let opises = this.opisService.getAll();
      this.opises = isNullOrUndefined(await opises) ? [] : await opises;
      this.getSearchOpisId();
      await this.getArchivefolders();
      await this.selectFilter(false);
      this.getFiltersData();
      if (this.authService.getCurUser().isadmin) {
        let sizes = await this.diskinfoService.getAll();
        if (Array.isArray(sizes)) {
          for (let i = 0; i < sizes.length; i++) {
            if (!sizes[i].error) {
              if (!window.localStorage.getItem(sizes[i].mount)) {
                if (+sizes[i].available.split('MB')[0] < 20000) {
                  this.openSnackBar(
                    `Осталось менее 20гб в папке ${sizes[i].mount}`,
                    'Ok'
                  );
                  window.localStorage.setItem(sizes[i].mount, '20');
                  continue;
                }
                if (+sizes[i].available.split('MB')[0] < 10000) {
                  this.openSnackBar(
                    `Осталось менее 10гб в папке ${sizes[i].mount}`,
                    'Ok'
                  );
                  window.localStorage.setItem(sizes[i].mount, '10');
                  continue;
                }
                if (+sizes[i].available.split('MB')[0] < 5000) {
                  this.openSnackBar(
                    `Осталось менее 5гб в папке ${sizes[i].mount}`,
                    'Ok'
                  );
                  window.localStorage.setItem(sizes[i].mount, '5');
                  continue;
                }
              } else {
                if (
                  +window.localStorage.getItem(sizes[i].mount) <= 10 &&
                  +sizes[i].available.split('MB')[0] > 10000
                ) {
                  window.localStorage.clearItem(sizes[i].mount);
                  continue;
                }
                if (
                  +window.localStorage.getItem(sizes[i].mount) <= 5 &&
                  +sizes[i].available.split('MB')[0] > 5000
                ) {
                  window.localStorage.clearItem(sizes[i].mount);
                  continue;
                }
                if (
                  +window.localStorage.getItem(sizes[i].mount) > 10 &&
                  +sizes[i].available.split('MB')[0] < 10000
                ) {
                  this.openSnackBar(
                    `Осталось менее 10гб в папке ${sizes[i].mount}`,
                    'Ok'
                  );
                  window.localStorage.setItem(sizes[i].mount, '10');
                  continue;
                }
                if (
                  +window.localStorage.getItem(sizes[i].mount) > 5 &&
                  +sizes[i].available.split('MB')[0] < 5000
                ) {
                  this.openSnackBar(
                    `Осталось менее 5гб в папке ${sizes[i].mount}`,
                    'Ok'
                  );
                  window.localStorage.setItem(sizes[i].mount, '5');
                  continue;
                }
              }
            }
          }
        }
      }
    } catch (err) {
      this.globalService.exceptionHandling(err);
    } finally {
      this.spinnerService.hide();
    }
  }

  async getArchivefolders() {
    // if (!isNullOrUndefined(this.pageEvent)) {
    //   this.pageEvent.pageIndex = 0;
    // }
    // this.page = 1;
    let start =
      this.limit * (this.filtersService.archivefolderListFilter.page - 1);
    try {
      // this.spinnerService.show();
      let archivefolders;
      let length;
      if (
        this.filtersService.archivefolderListFilter.searchOrder !== '' ||
        this.filtersService.archivefolderListFilter.searchStr !== '' ||
        this.filtersService.archivefolderListFilter.searchStatus !== null ||
        this.filtersService.archivefolderListFilter.descrip_type !== null ||
        this.filtersService.archivefolderListFilter.searchUnPhoto !== null ||
        this.filtersService.archivefolderListFilter.searchUnDoc !== null ||
        this.filtersService.archivefolderListFilter.searchAct !== null ||
        this.filtersService.archivefolderListFilter.searchMem !== null ||
        this.filtersService.archivefolderListFilter.searchArt !== null ||
        this.filtersService.archivefolderListFilter.searchDig !== null ||
        this.filtersService.archivefolderListFilter.searchOpis.length > 0 ||
        (this.filtersService.archivefolderListFilter.searchtxt !== '' &&
          this.filtersService.archivefolderListFilter.searchflds.length > 0)
      ) {
        length = this.archivefolderService.getFilter_count(
          this.filtersService.archivefolderListFilter.searchStr,
          // this.searchOpisId,
          this.filtersService.archivefolderListFilter.searchOpis,
          this.filtersService.archivefolderListFilter.searchStatus,
          this.filtersService.archivefolderListFilter.searchUnPhoto,
          this.filtersService.archivefolderListFilter.searchUnDoc,
          this.filtersService.archivefolderListFilter.searchAct,
          this.filtersService.archivefolderListFilter.searchMem,
          this.filtersService.archivefolderListFilter.searchArt,
          this.filtersService.archivefolderListFilter.searchDig,
          this.filtersService.archivefolderListFilter.searchtxt,
          this.filtersService.archivefolderListFilter.searchflds,
          this.filtersService.archivefolderListFilter.descrip_type,
          this.filtersService.archivefolderListFilter.filterExact
        );
        archivefolders =
          this.archivefolderService.getWithScanuserOffsetLimitFilterOrder(
            start,
            this.limit,
            this.filtersService.archivefolderListFilter.searchStr,
            // this.searchOpisId,
            this.filtersService.archivefolderListFilter.searchOpis,
            this.filtersService.archivefolderListFilter.searchOrder,
            this.filtersService.archivefolderListFilter.searchStatus,
            this.filtersService.archivefolderListFilter.searchUnPhoto,
            this.filtersService.archivefolderListFilter.searchUnDoc,
            this.filtersService.archivefolderListFilter.searchAct,
            this.filtersService.archivefolderListFilter.searchMem,
            this.filtersService.archivefolderListFilter.searchArt,
            this.filtersService.archivefolderListFilter.searchDig,
            this.filtersService.archivefolderListFilter.searchtxt,
            this.filtersService.archivefolderListFilter.searchflds,
            this.filtersService.archivefolderListFilter.descrip_type,
            this.filtersService.archivefolderListFilter.filterExact
          );
      } else {
        length = this.archivefolderService.getAll_count();
        archivefolders = this.archivefolderService.getWithScanuserOffsetLimit(
          start,
          this.limit
        );
      }
      // this.length = (await length).rowcount;
      this.total = (await length).rowcount;
      this.archivefolders = isNullOrUndefined(await archivefolders)
        ? []
        : await archivefolders;
      this.archivefolders.map((archivefolder) => {
        let _opis = this.opises.find(
          (opis) => opis.id === archivefolder.opis_id
        );
        archivefolder.opis = isNullOrUndefined(_opis) ? null : _opis.opis_name;
        archivefolder.fond = isNullOrUndefined(_opis) ? null : _opis.fond;
      });
    } catch (err) {
      this.globalService.exceptionHandling(err);
    } finally {
      // console.log(this.archivefolders);
      // this.spinnerService.hide();
    }
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 100000,
    });
  }

  getFiltersData() {
    let opisesFond = [];
    let opisesName = [];
    this.archivefolders.map((archivefolder) => {
      let _opis = this.opises.find((opis) => opis.id === archivefolder.opis_id);
      archivefolder.opis = isNullOrUndefined(_opis) ? null : _opis.opis_name;
      archivefolder.fond = isNullOrUndefined(_opis) ? null : _opis.fond;
    });
    this.opises.map((opis) => {
      opisesFond.push({
        name: opis.fond,
        id: opis.id,
        description: opis.fond_description,
      });
      // opisesName.push({ fond: opis.fond, opis_name: opis.opis_name, id: opis.id });
      // console.log(this.searchFond);
      if (
        this.filtersService.archivefolderListFilter.searchFond.find(
          (item) => item === opis.fond
        )
      ) {
        opisesName.push({
          fond: opis.fond,
          opis_name: opis.opis_name,
          id: opis.id,
          description: opis.description,
        });
      }
    });
    let used = {};
    this.opisesFond = opisesFond.filter((obj) => {
      return obj.name in used ? 0 : (used[obj.name] = 1);
    });
    used = {};
    this.opisesName = opisesName.filter((obj) => {
      // return obj.name in used ? 0 : (used[obj.name] = 1);
      return true;
    });
    this.opisesFond.sort((a, b) =>
      a.name === b.name ? (a.id > b.id ? 1 : -1) : a.name > b.name ? 1 : -1
    );
    this.opisesName.sort((a, b) => (a.id > b.id ? 1 : -1));
    this.opisesName.sort((a, b) => (a.opis_name < b.opis_name ? 1 : -1));
    this.opisesName.sort((a, b) => (a.fond > b.fond ? 1 : -1));
  }

  onAddDocument() {
    this.router.navigate(['/sys/arch/edit/newdoc']);
  }

  onUploadFiles() {
    let dialogRef = this.dialog.open(DialogFileLoaderComponent, {
      panelClass: 'app-dialog-extend',
    });
  }

  onEditDocument(id) {
    this.router.navigate([`/sys/arch/edit/${id}`]);
  }

  // async setPageSizeOptions(event: PageEvent) {
  //   let start = event.pageSize * event.pageIndex;
  //   try {
  //     let archivefolders;
  //     if (
  //       this.searchOrder !== '' ||
  //       this.searchStr !== '' ||
  //       this.searchOpisId.length > 0
  //     ) {
  //       archivefolders = this.archivefolderService.getWithScanuserOffsetLimitFilterOrder(
  //         start,
  //         event.pageSize,
  //         this.searchStr,
  //         this.searchOpisId,
  //         this.searchOrder
  //       );
  //     } else {
  //       archivefolders = this.archivefolderService.getWithScanuserOffsetLimit(
  //         start,
  //         event.pageSize
  //       );
  //     }
  //     this.archivefolders = isNullOrUndefined(await archivefolders)
  //       ? []
  //       : await archivefolders;
  //     // this.archivefolders.sort((a, b) => (a.id > b.id) ? 1 : -1);
  //     this.getFiltersData();
  //   } catch (err) {
  //     console.error(`Ошибка ${err}`);
  //   }
  //   this.pageSize = event.pageSize;
  //   this.pageEvent = event;
  // }

  getValueByKey(key): string {
    return isNullOrUndefined(this.statusTypes.find((item) => item.key === key))
      ? ''
      : this.statusTypes.find((item) => item.key === key).value;
  }

  selectFond() {
    this.filtersService.archivefolderListFilter.searchOpis.length = 0;
    let searchOpis = [];
    this.filtersService.archivefolderListFilter.searchFond.map((item) => {
      // searchOpis = searchOpis.concat(this.opisesName.filter(_item => _item.id === item));
      searchOpis = searchOpis.concat(
        this.opises.filter((_item) => _item.fond === item)
      );
    });
    // let opisesName = [];
    // console.log(searchOpis);
    this.opisesName = searchOpis;
    searchOpis.map((item) => {
      this.filtersService.archivefolderListFilter.searchOpis.push(item.id);
      // opisesName.push(this.opisesName.find(_item => _item.id === item.id));
    });
    let used = {};
    this.filtersService.archivefolderListFilter.searchOpis =
      this.filtersService.archivefolderListFilter.searchOpis.filter((obj) => {
        return obj in used ? 0 : (used[obj] = 1);
      });
    // this.opisesName.sort((a, b) =>
    //   a.fond === b.fond
    //     ? a.opis_name === b.opis_name
    //       ? a.opis_name > b.opis_name
    //         ? 1
    //         : -1
    //       : a.id > b.id
    //       ? 1
    //       : -1
    //     : a.fond > b.fond
    //     ? 1
    //     : -1
    // ); //opis_name;
    this.opisesName.sort((a, b) => (a.id > b.id ? 1 : -1));
    this.opisesName.sort((a, b) => (a.opis_name < b.opis_name ? 1 : -1));
    this.opisesName.sort((a, b) => (a.fond > b.fond ? 1 : -1));
  }

  getSearchOpisId() {
    this.searchOpisId.length = 0;
    let _opis = [];
    if (this.filtersService.archivefolderListFilter.searchOpis.length === 0) {
      _opis = [];
      // _opis = this.opises;
    } else {
      this.filtersService.archivefolderListFilter.searchOpis.map((item) => {
        _opis = _opis.concat(this.opises.filter((_item) => _item.id === item));
      });
    }
    let _fond = [];
    if (this.filtersService.archivefolderListFilter.searchFond.length === 0) {
      _fond = _opis;
    } else {
      this.filtersService.archivefolderListFilter.searchFond.map((item) => {
        _fond = _fond.concat(_opis.filter((_item) => _item.fond === item));
      });
    }
    _fond.map((item) => {
      this.searchOpisId.push(item.id);
    });
  }

  selectOrder(type: string) {
    switch (type) {
      case 'byid':
        this.filtersService.archivefolderListFilter.searchOrder =
          this.filtersService.archivefolderListFilter.searchOrder === 'byid'
            ? 'byiddesc'
            : 'byid';
        break;
      case 'byname':
        this.filtersService.archivefolderListFilter.searchOrder =
          this.filtersService.archivefolderListFilter.searchOrder === 'byname'
            ? 'bynamedesc'
            : 'byname';
        break;
      case 'byshifr':
        this.filtersService.archivefolderListFilter.searchOrder =
          this.filtersService.archivefolderListFilter.searchOrder === 'byshifr'
            ? 'byshifrdesc'
            : 'byshifr';
        break;
    }
    this.selectFilter(true);
  }

  setOrgerStr() {
    switch (this.filtersService.archivefolderListFilter.searchOrder) {
      case 'byid':
        this.postfixId =
          '<i class="fa fa-sort-amount-asc" aria-hidden="true"></i>';
        this.postfixName = '';
        this.postfixShifr = '';
        break;
      case 'byiddesc':
        this.postfixId =
          '<i class="fa fa-sort-amount-desc" aria-hidden="true"></i>';
        this.postfixName = '';
        this.postfixShifr = '';
        break;
      case 'byname':
        this.postfixId = '';
        this.postfixName =
          '<i class="fa fa-sort-amount-asc" aria-hidden="true"></i>';
        this.postfixShifr = '';
        break;
      case 'bynamedesc':
        this.postfixId = '';
        this.postfixName =
          '<i class="fa fa-sort-amount-desc" aria-hidden="true"></i>';
        this.postfixShifr = '';
        break;
      case 'byshifr':
        this.postfixId = '';
        this.postfixName = '';
        this.postfixShifr =
          '<i class="fa fa-sort-amount-asc" aria-hidden="true"></i>';
        break;
      case 'byshifrdesc':
        this.postfixId = '';
        this.postfixName = '';
        this.postfixShifr =
          '<i class="fa fa-sort-amount-desc" aria-hidden="true"></i>';
        break;
    }
  }

  selectFilter(event) {
    clearTimeout(this.timer);
    this.timer = setTimeout(() => {
      this.getSearchOpisId();
      if (
        (this.filtersService.archivefolderListFilter.searchOpis.length > 0 ||
          this.filtersService.archivefolderListFilter.searchFond.length > 0) &&
        this.filtersService.archivefolderListFilter.searchOpis.length === 0
      ) {
        this.archivefolders.length = 0;
        this.total = 0;
      } else {
        if (event !== false) {
          this.filtersService.archivefolderListFilter.page = 1;
          this.getArchivefolders();
        }
      }
      this.setOrgerStr();
    }, 300);
  }

  goToPage(n: number): void {
    this.filtersService.archivefolderListFilter.page = n;
    this.updatePage();
  }

  onNext(): void {
    this.filtersService.archivefolderListFilter.page++;
    this.updatePage();
  }

  onPrev(): void {
    this.filtersService.archivefolderListFilter.page = 1;
    this.updatePage();
  }

  onNext10(n = 1): void {
    this.filtersService.archivefolderListFilter.page += 10 * n;
    this.updatePage();
  }

  onPrev10(n = 1): void {
    this.filtersService.archivefolderListFilter.page -= 10 * n;
    this.updatePage();
  }

  async updatePage() {
    // this.globalService.marchivelistPage = this.page;
    let start =
      this.limit * (this.filtersService.archivefolderListFilter.page - 1);
    try {
      let archivefolders;
      if (
        this.filtersService.archivefolderListFilter.searchOrder !== '' ||
        this.filtersService.archivefolderListFilter.searchStr !== '' ||
        this.filtersService.archivefolderListFilter.searchStatus != null ||
        this.filtersService.archivefolderListFilter.descrip_type != null ||
        this.filtersService.archivefolderListFilter.searchUnPhoto != null ||
        this.filtersService.archivefolderListFilter.searchUnDoc != null ||
        this.filtersService.archivefolderListFilter.searchAct != null ||
        this.filtersService.archivefolderListFilter.searchMem != null ||
        this.filtersService.archivefolderListFilter.searchArt != null ||
        this.filtersService.archivefolderListFilter.searchDig != null ||
        // this.searchOpisId.length > 0
        this.filtersService.archivefolderListFilter.searchOpis.length > 0 ||
        (this.filtersService.archivefolderListFilter.searchtxt !== '' &&
          this.filtersService.archivefolderListFilter.searchflds.length > 0)
      ) {
        archivefolders =
          this.archivefolderService.getWithScanuserOffsetLimitFilterOrder(
            start,
            this.limit,
            this.filtersService.archivefolderListFilter.searchStr,
            // this.searchOpisId,
            this.filtersService.archivefolderListFilter.searchOpis,
            this.filtersService.archivefolderListFilter.searchOrder,
            this.filtersService.archivefolderListFilter.searchStatus,
            this.filtersService.archivefolderListFilter.searchUnPhoto,
            this.filtersService.archivefolderListFilter.searchUnDoc,
            this.filtersService.archivefolderListFilter.searchAct,
            this.filtersService.archivefolderListFilter.searchMem,
            this.filtersService.archivefolderListFilter.searchArt,
            this.filtersService.archivefolderListFilter.searchDig,
            this.filtersService.archivefolderListFilter.searchtxt,
            this.filtersService.archivefolderListFilter.searchflds,
            this.filtersService.archivefolderListFilter.descrip_type,
            this.filtersService.archivefolderListFilter.filterExact
          );
      } else {
        archivefolders = this.archivefolderService.getWithScanuserOffsetLimit(
          start,
          this.limit
        );
      }
      this.archivefolders = isNullOrUndefined(await archivefolders)
        ? []
        : await archivefolders;
      this.getFiltersData();
    } catch (err) {
      console.error(`Ошибка ${err}`);
    }
  }
}
