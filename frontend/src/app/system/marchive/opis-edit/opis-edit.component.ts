import {
  Component,
  OnInit,
  HostListener,
  ViewChild,
  ElementRef,
  OnDestroy,
} from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { saveAs as importedSaveAs } from 'file-saver';

import { Muser } from '../../../shared/models/muser.model';
import { Opis, opis_width } from '../../../shared/models/opis.model';
import { AuthService } from '../../../shared/services/auth.service';
import { MuserService } from '../../shared/services/muser.service';
import { OpisService } from '../../shared/services/opis.service';
import { DialogYesNoComponent } from '../../shared/components/dialog-yes-no/dialog-yes-no.component';
import { OpisfileService } from '../../shared/services/opisfile.service';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { isNullOrUndefined } from 'util';
import { DialogSetstringComponent } from '../../shared/components/dialog-setstring/dialog-setstring.component';
import { Subscription } from 'rxjs';
import { GlobalService } from 'src/app/shared/services/global.service';
import { DialogPdfViewerComponent } from '../../shared/components/dialog-pdf-viewer/dialog-pdf-viewer.component';
import { PublicSiteActionsService } from '../../shared/services/publicsiteactions.service';
import { Fond } from 'src/app/shared/models/fond.model';
import { FondService } from '../../shared/services/fond.service';
import { DialogOkComponent } from '../../shared/components/dialog-ok/dialog-ok.component';
import { PUBLICATION_MESSAGES } from '../../shared/constants/publication-strings';
import { ALLOWED_EXTENSIONS } from '../../shared/constants/allowed-extensions';

@Component({
  selector: 'app-opis-edit',
  templateUrl: './opis-edit.component.html',
  styleUrls: ['./opis-edit.component.css'],
})
export class OpisEditComponent implements OnInit, OnDestroy {
  constructor(
    private authService: AuthService,
    private muserService: MuserService,
    private opisService: OpisService,
    private fondService: FondService,
    private globalService: GlobalService,
    private opisfileService: OpisfileService,
    private activatedRoute: ActivatedRoute,
    private publicSiteActions: PublicSiteActionsService,
    private router: Router,
    private spinnerService: NgxSpinnerService,
    public dialog: MatDialog,
    public snackBar: MatSnackBar
  ) {
    this.activeRouterObserver = this.activatedRoute.params.subscribe(
      (param) => {
        this.id = param.id;
        this.fondId = param.fondId;
        if (param.id === 'newdoc') {
          this.intType = 'newdoc';
        } else {
          this.id = +param.id;
          this.intType = 'editdoc';
        }
        if (this.inited) {
          this.getData();
        }
      }
    );
  }
  opis_width = opis_width;

  id: number;
  fondId: number;
  intType: string | any = ''; // тип интерфейса
  loggedInUser: Muser;
  opisForm: FormGroup;
  files = [];
  postfixTypes: any[];
  statusTypes: any[];

  selectedOpis: Opis;
  selectedOpisOldData: Opis;
  fondInfo: Fond;
  isPublicationUnblocked: boolean;

  inited = false;
  allowedExtensions = ALLOWED_EXTENSIONS.opis;

  @ViewChild('filesInput', { static: true }) filesInput: ElementRef;
  inProgressVal = 0;
  inProgressValExt = 0;
  selectedArchivefolderfiles = [];
  inProgress = false;
  activeRouterObserver: Subscription;
  viewOnly: boolean;
  currDate: string;
  ngOnDestroy(): void {
    // this.activeRouterObserver.unsubscribe();
  }
  @HostListener('window:keydown', ['$event'])
  onKeyPress(event: KeyboardEvent) {
    if ((event.ctrlKey || event.metaKey) && event.keyCode == 83) {
      event.preventDefault();
      if (this.intType !== 'myprofile' && !this.opisForm.invalid) {
        this.onSaveOpisForm();
      }
    }
  }

  async ngOnInit() {
    const userType = this.authService.getCurUser().usertype;
    userType === 2 || userType === 4
      ? (this.viewOnly = true)
      : (this.viewOnly = false);
    this.currDate = new Date().toISOString().split('T')[0];
    this.opisForm = new FormGroup({
      fond: new FormControl({ value: null, disabled: this.viewOnly }, [
        Validators.required,
      ]),
      opis_name: new FormControl({ value: null, disabled: this.viewOnly }, [
        Validators.required,
      ]),
      trudindex: new FormControl({ value: null, disabled: this.viewOnly }, [
        Validators.required,
      ]),
      trudindex_scann: new FormControl({
        value: null,
        disabled: this.viewOnly,
      }),
      description: new FormControl({ value: null, disabled: this.viewOnly }),
      doc_begin: new FormControl({ value: null, disabled: this.viewOnly }, [
        Validators.maxLength(4),
        Validators.max(9999),
      ]),
      doc_end: new FormControl({ value: null, disabled: this.viewOnly }, [
        Validators.maxLength(4),
        Validators.max(9999),
      ]),
      form_date: new FormControl({ value: null, disabled: this.viewOnly }),
      storage_org: new FormControl({ value: null, disabled: this.viewOnly }),
      access_lim: new FormControl({ value: null, disabled: this.viewOnly }),
      help: new FormControl({ value: null, disabled: this.viewOnly }),
      storage_capacity: new FormControl({
        value: null,
        disabled: this.viewOnly,
      }),
      creators_name: new FormControl({ value: null, disabled: this.viewOnly }),
      admin_biog_history: new FormControl({
        value: null,
        disabled: this.viewOnly,
      }),
      arch_history: new FormControl({ value: null, disabled: this.viewOnly }),
      donation_source: new FormControl({
        value: null,
        disabled: this.viewOnly,
      }),
      mat_organ_system: new FormControl({
        value: null,
        disabled: this.viewOnly,
      }),
      archivist_rmk: new FormControl({ value: null, disabled: this.viewOnly }),
      descrip_date: new FormControl({ value: null, disabled: this.viewOnly }),
      name: new FormControl({ value: null, disabled: this.viewOnly }),
      published: new FormControl({ value: null, disabled: this.viewOnly }),
      pub_date: new FormControl({ value: null, disabled: this.viewOnly }),
      mustPublish: new FormControl(null),
      mustUnpublish: new FormControl(null),
      pub_change_date: new FormControl(null),
    });
    try {
      let loggedInUser = this.muserService.getOneById(
        this.authService.getCurUser().id
      );
      this.loggedInUser = await loggedInUser;
    } catch (err) {
      this.globalService.exceptionHandling(err);
    }
    this.inited = true;
    this.getData();
  }

  async getData() {
    this.spinnerService.show();
    try {
      switch (this.intType) {
        case 'editdoc': // редактирование описи
          let selectedOpis = this.opisService.getOneById(this.id);
          let selectedArchivefolderfiles =
            this.opisfileService.getAllByParentId(this.id, true);
          this.selectedArchivefolderfiles = isNullOrUndefined(
            await selectedArchivefolderfiles
          )
            ? []
            : await selectedArchivefolderfiles;
          this.selectedArchivefolderfiles.map((selectedArchivefolderfile) => {
            selectedArchivefolderfile.checkToDel = false;
          });
          this.selectedArchivefolderfiles.sort((a, b) =>
            a.opisfile.filename > b.opisfile.filename ? 1 : -1
          );
          this.selectedOpis = await selectedOpis;
          this.selectedOpisOldData = await selectedOpis;

          const allFonds = await this.fondService.getAll();
          this.fondInfo = allFonds.find(
            (fond: Fond) => fond.fond === this.selectedOpis.fond
          );
          this.isPublicationUnblocked = this.fondInfo.published === '*';

          this.opisForm.patchValue({
            fond: this.selectedOpis.fond,
            opis_name: this.selectedOpis.opis_name,
            trudindex: this.selectedOpis.trudindex,
            trudindex_scann: this.selectedOpis.trudindex_scann,
            description: this.selectedOpis.description,
            doc_begin: this.selectedOpis.doc_begin,
            doc_end: this.selectedOpis.doc_end,
            form_date: this.selectedOpis.form_date,
            storage_org: this.selectedOpis.storage_org,
            access_lim: this.selectedOpis.access_lim,
            help: this.selectedOpis.help,
            storage_capacity: this.selectedOpis.storage_capacity,
            creators_name: this.selectedOpis.creators_name,
            admin_biog_history: this.selectedOpis.admin_biog_history,
            arch_history: this.selectedOpis.arch_history,
            donation_source: this.selectedOpis.donation_source,
            mat_organ_system: this.selectedOpis.mat_organ_system,
            archivist_rmk: this.selectedOpis.archivist_rmk,
            descrip_date: this.selectedOpis.descrip_date,
            name: this.selectedOpis.name,
            published: this.selectedOpis.published === '*' ? true : false,
            pub_date: this.globalService.dateFromDateString(
              this.selectedOpis.pub_date
            ),
            mustPublish: this.selectedOpis.mustPublish,
            mustUnpublish: this.selectedOpis.mustUnpublish,
            pub_change_date: this.selectedOpis.pub_change_date,
          });
          break;
        case 'newdoc': // создание описи
          this.opisForm.reset();
          this.opisForm.patchValue({ fond: this.fondId });
          this.opisForm.patchValue({
            published: '*',
            mustPublish: '*',
            mustUnpublish: null,
            pub_change_date: this.currDate,
          });
          break;
        default:
      }
    } catch (err) {
      this.globalService.exceptionHandling(err);
    } finally {
      this.spinnerService.hide();
    }
  }

  async getPublicSiteActionType(opisForm?) {
    const oldData = this.selectedOpisOldData;
    const newData = this.opisForm;
    if (oldData && newData) {
      if (oldData.published !== newData.value.published) {
        const isActionDelete =
          this.opisForm.value['published'] === null ? '*' : null;
        const isActionUpdate =
          this.opisForm.value['published'] === '*' ? '*' : null;
        if (isActionDelete !== isActionUpdate) {
          await this.publicSiteActions.postOne({
            entity_id: this.id,
            entity_type: 4,
            action_delete: isActionDelete,
            action_update: isActionUpdate,
            legacyId: `ID-arch-${this.selectedOpis.fond}-${this.selectedOpis.opis_name}`,
          });
        } else {
          // opis deleted
          await this.publicSiteActions.postOne({
            entity_id: this.id,
            entity_type: 4,
            action_delete: '*',
            action_update: null,
            legacyId: `ID-arch-${this.selectedOpis.fond}-${this.selectedOpis.opis_name}`,
          });
        }
      }
    }
    if (!oldData) {
      // added new opis
      await this.publicSiteActions.postOne({
        entity_id: opisForm?.id,
        entity_type: 4,
        action_delete: null,
        action_update: '*',
        legacyId: `ID-arch-${this.opisForm.value.fond}-${this.opisForm.value.opis_name}`,
      });
    }
  }

  async onPubToggleChange() {
    const publishedFormValue = this.opisForm.value.published;
    const pubDateFormValue = this.opisForm.value.pub_date;
    if (!this.isPublicationUnblocked) {
      this.dialog.open(DialogOkComponent, {
        data: {
          title: PUBLICATION_MESSAGES.CHANGE_PARENT_STATUS,
        },
      });
      this.opisForm.patchValue({
        published: this.selectedOpis.published === '*' ? true : false,
      });
    }
    if (!publishedFormValue && !pubDateFormValue) {
      this.dialog.open(DialogOkComponent, {
        data: {
          title: PUBLICATION_MESSAGES.SET_PUB_CHANGE,
        },
      });
    }
  }

  async checkingPublicationLogic() {
    if (this.isPublicationUnblocked) {
      const opisPublishedField = this.opisForm.value['published'];
      if (opisPublishedField === '*' || opisPublishedField === true) {
        this.opisForm.patchValue({
          published: '*',
        });
      } else {
        this.opisForm.patchValue({
          published: null,
        });
      }
      const opisPubDateField = this.opisForm.value['pub_date'];

      if (opisPubDateField && opisPubDateField !== '') {
        const isCorrectDateFormat =
          new Date(this.opisForm.value['pub_date']) instanceof Date;
        const isPubDateValueExist = !!new Date(
          this.opisForm.value['pub_date']
        ).valueOf();
        const isPubDateCorrect = isCorrectDateFormat && isPubDateValueExist;
        const pubDate = new Date(this.opisForm.value['pub_date']);
        const curDate = new Date();

        if (!isPubDateCorrect) {
          this.openSnackBar(PUBLICATION_MESSAGES.NO_PUB_DATE, 'Ok');
          return false;
        }
        const dayMatch = pubDate.getDate() === curDate.getDate();
        const monthMatch = pubDate.getMonth() === curDate.getMonth();
        const yearMatch = pubDate.getFullYear() === curDate.getFullYear();
        if (+curDate < +pubDate) {
          this.opisForm.patchValue({
            published: null,
          });
        }
        if (dayMatch && monthMatch && yearMatch) {
          this.opisForm.patchValue({
            published: '*',
          });
        }
      } else {
        this.opisForm.patchValue({
          pub_date: null,
        });
      }
    } else {
      if (
        this.opisForm.value['published'] === true ||
        this.opisForm.value['published'] === '*'
      ) {
        this.opisForm.patchValue({
          published: '*',
        });
      } else {
        this.opisForm.patchValue({
          published: null,
        });
      }
    }
  }

  async checkFormChanges(oldData: Opis, newData: Opis) {
    const isChanged = Object.keys(oldData).some(
      (key) => oldData[key] !== newData[key]
    );

    const isPublished = newData.published === '*';

    if (isChanged && isPublished) {
      this.opisForm.patchValue({
        mustPublish: '*',
        mustUnpublish: null,
        pub_change_date: this.currDate,
      });
    }
  }
  async onSaveOpisForm(addNew = false) {
    this.spinnerService.show();
    try {
      switch (this.intType) {
        case 'editdoc': // редактирование документ
          await this.checkingPublicationLogic();
          const oldPublicationStatus = this.selectedOpis.published;
          const newPublicationStatus = this.opisForm.value.published;
          if (oldPublicationStatus !== newPublicationStatus) {
            if (
              this.opisForm.value['published'] === true ||
              this.opisForm.value['published'] === '*'
            ) {
              this.opisForm.patchValue({
                mustPublish: '*',
                mustUnpublish: null,
                pub_change_date: this.currDate,
              });
            } else {
              this.opisForm.patchValue({
                mustPublish: null,
                mustUnpublish: '*',
                pub_change_date: this.currDate,
              });
            }
          }

          this.checkFormChanges(this.selectedOpis, this.opisForm.value);

          let putOpisForm = this.opisService.putOneById(
            this.id,
            this.opisForm.value
          );
          let size = 0;
          for (let i = 0; i < this.files.length; i++) {
            size += this.files[i].size;
          }
          let addfiles = Promise.all(
            this.files.map(async (file) => {
              return new Promise((resolve, reject) => {
                let fileForm: FormData = new FormData();
                fileForm.append('opis_id', this.id.toString());
                fileForm.append('opisfile', file);
                this.inProgressValExt = 0;
                this.opisfileService.uploadOne(fileForm).subscribe((event) => {
                  if (event.type === HttpEventType.UploadProgress) {
                    const percentDone = Math.round(
                      (100 * event.loaded) / event.total
                    );
                    this.inProgressValExt = percentDone;
                  } else if (event instanceof HttpResponse) {
                    this.inProgressVal += Math.round((100 * file.size) / size);
                    resolve(true);
                  }
                });
              });
            })
          );
          let deleteSelectedArchivefolderfiles = Promise.all(
            this.selectedArchivefolderfiles.map(
              async (selectedArchivefolderfile) => {
                if (selectedArchivefolderfile.checkToDel) {
                  let deleteSelectedArchivefolderfile =
                    await this.opisfileService.deleteOneById(
                      selectedArchivefolderfile.id
                    );
                }
              }
            )
          );
          await putOpisForm;
          await addfiles;
          await deleteSelectedArchivefolderfiles;
          await this.getPublicSiteActionType();
          if (addNew) {
            this.router.navigate(['/sys/arch/o-edit/newdoc/', this.fondId]);
          } else {
            this.getData();
            this.spinnerService.hide();
          }
          break;
        case 'newdoc': // создание документа
          if (
            this.opisForm.value['published'] === true ||
            this.opisForm.value['published'] === '*'
          ) {
            const allFonds = await this.fondService.getAll();
            const fondInfo = allFonds?.find(
              (fond: Fond) => fond.fond === +this.opisForm.value.fond
            );

            const isFondPublished = fondInfo?.published !== null;

            this.opisForm.patchValue({
              published: isFondPublished ? '*' : null,
              mustPublish: isFondPublished ? '*' : null,
              mustUnpublish: isFondPublished ? null : null,
              pub_change_date: isFondPublished ? this.currDate : null,
            });
          } else {
            this.opisForm.patchValue({
              published: null,
              mustPublish: null,
              mustUnpublish: null,
              pub_change_date: null,
            });
          }
          let addOpisForm = this.opisService.postOne(this.opisForm.value);
          await addOpisForm;
          let _size = 0;
          for (let i = 0; i < this.files.length; i++) {
            _size += this.files[i].size;
          }
          let _addfiles = Promise.all(
            this.files.map(async (file) => {
              return new Promise(async (resolve, reject) => {
                let fileForm: FormData = new FormData();
                fileForm.append('opis_id', (await addOpisForm).id.toString());
                fileForm.append('opisfile', file);
                this.inProgressValExt = 0;
                this.opisfileService.uploadOne(fileForm).subscribe((event) => {
                  if (event.type === HttpEventType.UploadProgress) {
                    const percentDone = Math.round(
                      (100 * event.loaded) / event.total
                    );
                    this.inProgressValExt = percentDone;
                  } else if (event instanceof HttpResponse) {
                    this.inProgressVal += Math.round((100 * file.size) / _size);
                    resolve(true);
                  }
                });
              });
            })
          );

          await _addfiles;
          await this.getPublicSiteActionType(await addOpisForm);
          if (addNew) {
            this.router.navigate(['/sys/arch/o-edit/newdoc/', this.fondId]);

            this.getData();
          } else {
            this.router.navigate([
              '/sys/arch/o-edit/',
              (await addOpisForm).id.toString(),
              this.fondId,
            ]);
          }
          break;
        default:
      }
      this.openSnackBar(`Сохранено.`, 'Ok');
    } catch (err) {
      this.globalService.exceptionHandling(err);
    } finally {
      this.files.length = 0;
    }
  }

  async onDelOpis() {
    let dialogRef = this.dialog.open(DialogYesNoComponent, {
      data: {
        title: 'Удалить опись?',
        question: ``,
      },
    });
    dialogRef.afterClosed().subscribe(async (result) => {
      if (result === true) {
        this.spinnerService.show();
        try {
          let resDelOpis = await this.opisService.deleteOneById(this.id);
          await this.getPublicSiteActionType();
          this.router.navigate(['/sys/arch/f-edit/', this.fondId]);
          // this.router.navigate(['/sys/arch/o-list']);
          this.openSnackBar(`Удалено.`, 'Ok');
        } catch (err) {
          console.error(`Ошибка при удалении описи ${err}`);
          this.openSnackBar(`Ошибка ${err.status}`, 'Ok');
        } finally {
          this.spinnerService.hide();
        }
      }
    });
  }

  resetPub_date() {
    this.opisForm.patchValue({
      pub_date: '',
    });
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 10000,
    });
  }

  onFilesChangeInput(event) {
    const allowedExtensions = this.allowedExtensions;
    for (let i = 0; i < event.srcElement.files.length; i++) {
      if (
        allowedExtensions.includes(
          event.srcElement.files[i].name.split('.').pop().toLowerCase()
        )
      ) {
        this.files.push(event.srcElement.files[i]);
      }
    }
  }

  onFilesChange(file: FileList) {
    this.files = this.files.concat(file);
  }

  onDelFileById(id) {
    this.files.splice(id, 1);
  }

  onDelFiles() {
    this.files.length = 0;
  }

  onOpenFilesInput() {
    this.filesInput.nativeElement.click();
  }

  async onDownloadArchivefolderfile(id) {
    this.spinnerService.show();
    let fileName = '';
    for (let i = 0; i < this.selectedArchivefolderfiles.length; i++) {
      if (!isNullOrUndefined(this.selectedArchivefolderfiles[i])) {
        if (id === this.selectedArchivefolderfiles[i].id) {
          fileName = this.selectedArchivefolderfiles[i].opisfile.filename;
        }
      }
    }
    try {
      let blob = this.opisfileService.downloadOneById(id);
      this.inProgress = true;
      this.inProgressVal = 0;
      blob.subscribe((event) => {
        if (event.type === HttpEventType.DownloadProgress) {
          const percentDone = Math.round((100 * event.loaded) / event.total);
          this.inProgressVal = percentDone;
        } else if (event instanceof HttpResponse) {
          this.inProgress = false;
          this.inProgressVal = 0;
          this.spinnerService.hide();
          importedSaveAs(event.body, fileName);
        }
      });
    } catch (err) {
      console.error(`Ошибка ${err}`);
      this.spinnerService.hide();
    }
  }

  onRenameFile(id: number, fileName: string) {
    let dialogRef = this.dialog.open(DialogSetstringComponent, {
      data: {
        title: 'Введите название файла',
        question: ``,
        str: fileName,
      },
    });
    dialogRef.afterClosed().subscribe(async (result) => {
      if (typeof result === 'string') {
        try {
          await this.opisfileService.putFilename(id, result);
        } catch (err) {
          console.error(err);
        } finally {
          this.getData();
        }
      }
    });
  }

  toggleCheckToDel(id) {
    this.selectedArchivefolderfiles[id].checkToDel =
      !this.selectedArchivefolderfiles[id].checkToDel;
  }

  copyFilename(file) {
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = file.opisfile.file;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
  }

  onLinkOpis() {
    this.router.navigate([`/sys/arch/list`], {
      queryParams: { opis: this.id, fond: this.opisForm.value.fond },
    });
  }

  async onShowImg(id, selectedArchivefolderfile) {
    this.openPdf(selectedArchivefolderfile);
  }

  openPdf(selectedArchivefolderfile) {
    this.dialog.open(DialogPdfViewerComponent, {
      panelClass: 'app-dialog-extend',
      data: {
        file: selectedArchivefolderfile,
      },
    });
  }
}
