import {
  Component,
  OnInit,
  HostListener,
  OnDestroy,
  ViewEncapsulation,
} from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/shared/services/auth.service';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DialogYesNoComponent } from 'src/app/system/shared/components/dialog-yes-no/dialog-yes-no.component';
import { DialogSaveNoComponent } from 'src/app/system/shared/components/dialog-save-no/dialog-save-no.component';
import { UsagetService } from 'src/app/system/shared/services/usaget.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { PersonalT } from 'src/app/shared/models/personalt.model';
import { PersonaltService } from 'src/app/system/shared/services/personalt.service';
import { usageT_width } from 'src/app/shared/models/usaget.model';
import { Subscription } from 'rxjs';
import { ListOfT } from 'src/app/shared/models/listoft.model';
import { ListoftService } from 'src/app/system/shared/services/listoft.service';
import { DateparseService } from 'src/app/shared/services/dateparse.service';

@Component({
  selector: 'app-usaget-edit',
  templateUrl: './usaget-edit.component.html',
  styleUrls: ['./usaget-edit.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class UsagetEditComponent implements OnInit, OnDestroy {
  usageT_width = usageT_width;

  timerId;
  id: number;
  archivefolder_id: number;

  viewOnly = false;
  us_types: ListOfT[];

  usagetForm: FormGroup;
  activeRouterObserver: Subscription;

  @HostListener('window:keydown', ['$event'])
  onKeyPress(event: KeyboardEvent) {
    if ((event.ctrlKey || event.metaKey) && event.keyCode == 83) {
      event.preventDefault();
      if (!this.viewOnly && !this.usagetForm.invalid) {
        this.onSaveForm();
      }
    }
  }

  constructor(
    public dateparseService: DateparseService,
    private authService: AuthService,
    private globalService: GlobalService,
    private personaltService: PersonaltService,
    private usagetService: UsagetService,
    private listoftService: ListoftService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private spinnerService: NgxSpinnerService,
    public dialog: MatDialog,
    public snackBar: MatSnackBar
  ) {
    this.activeRouterObserver = this.activatedRoute.params.subscribe(
      (param) => {
        this.id = param.id ? +param.id : null;
        this.archivefolder_id = param.archivefolder_id
          ? +param.archivefolder_id
          : null;
        this.viewOnly =
          this.viewOnly === false
            ? this.authService.getCurUser().usertype === 4
            : true;
      }
    );
  }

  ngOnDestroy(): void {
    this.activeRouterObserver.unsubscribe();
  }

  async ngOnInit() {
    this.usagetForm = new FormGroup({
      id: new FormControl({ value: null, disabled: this.viewOnly }, []),
      archivefolder_id: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      us_no: new FormControl({ value: null, disabled: true }, [
        Validators.required,
      ]),
      us_type_id: new FormControl({ value: null, disabled: this.viewOnly }, []),
      // us_date: new FormControl({ value: null, disabled: true }, []),
      us_date_begin: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      us_date_end: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      us_date_begin_end: new FormControl(
        { value: null, disabled: this.viewOnly },
        []
      ),
      sour_code: new FormControl({ value: null, disabled: this.viewOnly }, []),
      sour_txt: new FormControl({ value: null, disabled: this.viewOnly }, []),
      state_type: new FormControl({ value: null, disabled: this.viewOnly }, []),
    });
    try {
      await this.getData();
    } catch (err) {
      if (err.status === 401) {
        console.error(`Ошибка авторизации`);
        this.authService.logout();
      } else {
        console.error(`Ошибка ${err}`);
      }
    } finally {
    }
  }

  async getData() {
    try {
      this.spinnerService.show();
      // selectedPersonal = this.personaltService.getBycode(
      //   this.selectedPersonalcode
      // );
      let us_types = this.listoftService.getWithFilter('US_TYPE');
      this.us_types = !(await us_types) ? [] : await us_types;
      this.us_types.sort((a, b) => (a.name_ent < b.name_ent ? -1 : 1));
    } catch (err) {
      if (err.status === 401) {
        console.error(`Ошибка авторизации`);
        this.authService.logout();
      } else {
        console.error(`Ошибка ${err}`);
      }
    }
    if (this.id) {
      try {
        let selectedUsaget = await this.usagetService.getOneById(this.id);
        this.usagetForm.reset();
        this.usagetForm.patchValue({
          id: selectedUsaget.id,
          archivefolder_id: selectedUsaget.archivefolder_id,
          us_no: selectedUsaget.us_no,
          us_type_id: selectedUsaget.us_type_id,
          // us_date: selectedUsaget.us_date,
          us_date_begin: selectedUsaget.us_date_begin,
          us_date_end: selectedUsaget.us_date_end,
          sour_code: selectedUsaget.sour_code,
          sour_txt: selectedUsaget.sour_txt,
          state_type: selectedUsaget.state_type,
        });
        this.dateparseService.setSourDate(
          this.usagetForm,
          'us_date_begin_end',
          selectedUsaget.us_date_begin,
          selectedUsaget.us_date_end
        );
      } catch (err) {
        if (err.status === 401) {
          console.error(`Ошибка авторизации`);
          this.authService.logout();
        } else {
          console.error(`Ошибка ${err}`);
        }
      } finally {
        this.spinnerService.hide();
      }
    } else {
      try {
        let _usages = this.usagetService.getByArchivefolderId(
          this.archivefolder_id
        );
        let usages = !(await _usages) ? [] : await _usages;
        let num: number;
        if (usages.length === 0) {
          num = 1;
        } else {
          usages.sort((a, b) => b.us_no - a.us_no);
          num = usages[0].us_no + 1;
        }
        this.usagetForm.reset();
        this.usagetForm.patchValue({
          us_no: num,
        });
      } catch (err) {
        if (err.status === 401) {
          console.error(`Ошибка авторизации`);
          this.authService.logout();
        } else {
          console.error(`Ошибка ${err}`);
        }
      } finally {
        this.spinnerService.hide();
      }
    }
    this.spinnerService.hide();
  }

  async onDel() {
    let dialogRef = this.dialog.open(DialogYesNoComponent, {
      data: {
        title: 'Удалить сведения об использовании?',
        question: ``,
      },
    });
    dialogRef.afterClosed().subscribe(async (result) => {
      if (result === true) {
        this.spinnerService.show();
        try {
          await this.usagetService.deleteOneById(this.id);
          this.router.navigate([`/sys/arch/edit/${this.archivefolder_id}`]);
          this.openSnackBar(`Удалено.`, 'Ok');
        } catch (err) {
          console.error(`Ошибка при удалении ${err}`);
          this.openSnackBar(`Ошибка ${err.status}`, 'Ok');
        } finally {
          this.spinnerService.hide();
        }
      }
    });
  }

  /**
   * Функция возвращает true, если бли внесены изменения
   */
  hasChanges(): boolean {
    let res = false;
    if (this.usagetForm.dirty) {
      res = true;
    }
    return res;
  }

  /**
   * Функция сохраняет форму. Silent для показа сообщения. Возвращает true в случаеуспешного сохранения
   */
  async onSaveForm(silent = false) {
    let res = false;
    this.spinnerService.show();
    try {
      // if (
      //   this.usagetForm.getRawValue()['us_date'] &&
      //   this.usagetForm.getRawValue()['us_date'] !== ''
      // ) {
      //   if (
      //     !(
      //       new Date(this.usagetForm.getRawValue()['us_date']) instanceof
      //         Date &&
      //       !isNaN(new Date(this.usagetForm.getRawValue()['us_date']).valueOf())
      //     )
      //   ) {
      //     this.openSnackBar(`Введена не существующая дата использования`, 'Ok');
      //     return false;
      //   }
      // } else {
      //   this.usagetForm.patchValue({
      //     us_date: null,
      //   });
      // }
      if (this.id) {
        await this.usagetService.putOneById(
          this.id,
          this.usagetForm.getRawValue()
        );
      } else {
        this.usagetForm.patchValue({
          archivefolder_id: this.archivefolder_id,
        });
        this.usagetForm.removeControl('id');
        let postForm = await this.usagetService.postOne(
          this.usagetForm.getRawValue()
        );
        this.router.navigate([
          `/sys/arch/usagetedit/${this.archivefolder_id}/${postForm.id}`,
        ]);
      }
      await this.getData();
      if (!silent) {
        this.openSnackBar(`Сохранено.`, 'Ok');
      }
      res = true;
    } catch (err) {
      if (err.status === 401) {
        console.error(`Ошибка авторизации`);
        this.authService.logout();
      } else if (err.status === 409) {
        this.usagetForm.controls['us_no'].setErrors({ incorrect: true });
        this.openSnackBar(`${err.error['error']}`, 'Ok');
      } else {
        console.error(`Ошибка ${err}`);
        this.openSnackBar(`Ошибка ${err.status}`, 'Ok');
      }
    } finally {
      this.spinnerService.hide();
      return res;
    }
  }

  async goBack() {
    let str = '/sys/arch/edit/';
    str += `${this.archivefolder_id}`;
    let saved = true;
    if (this.hasChanges() && this.usagetForm.valid) {
      let dialogRef = this.dialog.open(DialogSaveNoComponent, {
        data: {
          title: 'Сохранить изменения?',
          question: ``,
        },
      });
      dialogRef.afterClosed().subscribe(async (result) => {
        if (result === true) {
          saved = await this.onSaveForm(true);
          if (saved === true) {
            this.router.navigate([str]);
          }
        } else if (result === false) {
          this.router.navigate([str]);
        } else {
          return;
        }
      });
    } else if (this.hasChanges() && this.usagetForm.valid) {
      let dialogRef = this.dialog.open(DialogYesNoComponent, {
        data: {
          title: 'Изменения не будут сохранены!',
          question: `Не заполнены обязательные поля. Уйти со страницы?`,
        },
      });
      dialogRef.afterClosed().subscribe(async (result) => {
        if (result === true) {
          this.router.navigate([str]);
        } else {
          return;
        }
      });
    } else {
      this.router.navigate([str]);
    }
  }

  // resetUs_date() {
  //   this.usagetForm.patchValue({
  //     us_date: null,
  //   });
  // }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
    });
  }
}
