import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PageEvent, MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthService } from 'src/app/shared/services/auth.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { isNullOrUndefined } from 'util';
import { ArchivefolderfileService } from 'src/app/system/shared/services/archivefolderfile.service';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { saveAs as importedSaveAs } from 'file-saver';
import { DialogYesNoComponent } from 'src/app/system/shared/components/dialog-yes-no/dialog-yes-no.component';
import {
  LightboxImage,
  LightboxComponent,
  LightboxEvent,
} from 'src/app/system/shared/components/lightbox/lightbox.component';

@Component({
  selector: 'app-files-list',
  templateUrl: './files-list.component.html',
  styleUrls: ['./files-list.component.css'],
})
export class FilesListComponent implements OnInit {
  archivefolderfiles: any[];
  _archivefolderfiles: any[];

  searchOrder = 'byid';
  postfixId = '';
  postfixCrtime = '';
  postfixShifr = '';
  postfixFilename = '';

  filter_fond: string;
  filter_opis_nam: string;
  filter_delo: string;
  filter_filename: string;

  // MatPaginator Inputs
  length = 100;
  pageSize = 100;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  pageEvent: PageEvent;
  start = 0;
  end = 0;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  inProgress = false;
  inProgressVal = 0;

  /**Галерея */
  lightboxImages: LightboxImage[] = [];
  // bufflightboxImages: LightboxImage[];
  lightboxEvents: LightboxEvent[] = [
    {
      name: 'edit',
      icon: '<i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i>',
      caption: 'Редактировать',
    },
    {
      name: 'marchive',
      icon: '<i class="fa fa-file-text-o fa-2x" aria-hidden="true"></i>',
      caption: 'Перейти к делу',
    },
  ];
  @ViewChild('lightbox', { static: true }) lightbox: LightboxComponent;
  setImgIdx: number; // индекс картинки,открытой в галерее
  setImgId: number; // id открытая в галерее
  setArchivefolderId: number; // id дела в галерее
  openImgIdx = -1; // idx изображения с которого галерея открыта
  openImgIdxCount = 0; // инкримент к idx изображения с которого галерея открыта

  imgBufsize = 10; // /2 буфер изображений в галерее
  subimgBufsize = 10000;
  openedE; // файл, открыто в галерее

  /**Для пагинатора */
  total = 0;
  page = 1;
  limit = 100;

  galleryWait = false;

  constructor(
    private authService: AuthService,
    private archivefolderfileService: ArchivefolderfileService,
    public globalService: GlobalService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private spinnerService: NgxSpinnerService,
    public dialog: MatDialog,
    public snackBar: MatSnackBar
  ) {}

  async ngOnInit() {
    this.searchOrder = this.globalService.fileslistFilter.searchOrder;
    this.filter_fond = this.globalService.fileslistFilter.filter_fond;
    this.filter_opis_nam = this.globalService.fileslistFilter.filter_opis_nam;
    this.filter_delo = this.globalService.fileslistFilter.filter_delo;
    this.filter_filename = this.globalService.fileslistFilter.filter_filename;
    try {
      if (!isNullOrUndefined(this.pageEvent)) {
        this.pageEvent.pageIndex = 0;
      }
      this.page = 1;
      await this.setOrgerStr();
      await this.updateData();
    } catch (err) {
      this.globalService.exceptionHandling(err);
    } finally {
      if (+this.globalService.fileslistsetImgIdx >= 0) {
        this.onShowImgBack(
          +this.globalService.fileslistsetImgIdx +
            +this.globalService.fileslistsetImgIdxCount
        );
      }
    }
  }

  // setOptions(
  //   arrowPrevIcon = 'fa fa-arrow-circle-o-left',
  //   arrowNextIcon = 'fa fa-arrow-circle-o-right'
  // ) {
  //   this.galleryOptions = [
  //     {
  //       imageAnimation: NgxGalleryAnimation.Fade,
  //       arrowPrevIcon: arrowPrevIcon,
  //       arrowNextIcon: arrowNextIcon,
  //       previewZoom: true,
  //       previewRotate: false,
  //       thumbnails: false,
  //       image: false,
  //       height: '1px',
  //       actions: [
  //         {
  //           icon: 'fa fa-pencil-square-o',
  //           onClick: () => {
  //             if (!isNullOrUndefined(this._archivefolderfiles)) {
  //               let checkfile = this._archivefolderfiles.find(
  //                 item => item.id === this.setImgId
  //               );
  //               if (!isNullOrUndefined(checkfile)) {
  //                 if (
  //                   checkfile.archivefile.filename
  //                     .toLowerCase()
  //                     .split('.')
  //                     .reverse()[0] === 'tiff' ||
  //                   checkfile.archivefile.filename
  //                     .toLowerCase()
  //                     .split('.')
  //                     .reverse()[0] === 'tif'
  //                 ) {
  //                   let res = confirm(
  //                     'Редактирование *.tiff файлов высокого разрешения может вызвать ошибку. Продолжить?'
  //                   );
  //                   if (res !== true) {
  //                     return;
  //                   }
  //                 }
  //               }
  //             }
  //             this.globalService.fileslistsetImgId = this.setImgId;
  //             this.globalService.fileslistsetImgIdx = this.openImgIdx;
  //             this.globalService.fileslistsetImgIdxCount = this.openImgIdxCount;
  //             this.router.navigate([`/sys/arch/files/edit/${this.setImgId}`]);
  //           },
  //           titleText: 'Редактировать'
  //         },
  //         {
  //           icon: 'fa fa-file-text-o',
  //           onClick: () => {
  //             this.globalService.fileslistsetImgId = this.setImgId;
  //             this.globalService.fileslistsetImgIdx = this.openImgIdx;
  //             this.globalService.fileslistsetImgIdxCount = this.openImgIdxCount;
  //             this.router.navigate([
  //               `/sys/arch/edit/${this.setArchivefolderId}`
  //             ]);
  //           },
  //           titleText: 'Перейти к делу'
  //         }
  //       ]
  //     },
  //     { previewCloseOnEsc: true }
  //   ];
  // }

  onLightboxEvent(e) {
    switch (e.name) {
      case 'marchive':
        this.globalService.fileslistsetImgId = this.setImgId;
        this.globalService.fileslistsetImgIdx = this.openImgIdx;
        this.globalService.fileslistsetImgIdxCount = this.openImgIdxCount;
        this.router.navigate([`/sys/arch/edit/${this.setArchivefolderId}`]);
        break;
      case 'edit':
        if (!isNullOrUndefined(this._archivefolderfiles)) {
          let checkfile = this._archivefolderfiles.find(
            (item) => item.id === this.setImgId
          );
          if (!isNullOrUndefined(checkfile)) {
            if (
              checkfile.archivefile.filename
                .toLowerCase()
                .split('.')
                .reverse()[0] === 'tiff' ||
              checkfile.archivefile.filename
                .toLowerCase()
                .split('.')
                .reverse()[0] === 'tif'
            ) {
              let res = confirm(
                'Редактирование *.tiff файлов высокого разрешения может вызвать ошибку. Продолжить?'
              );
              if (res !== true) {
                return;
              }
            }
          }
        }
        this.globalService.fileslistsetImgId = this.setImgId;
        this.globalService.fileslistsetImgIdx = this.openImgIdx;
        this.globalService.fileslistsetImgIdxCount = this.openImgIdxCount;
        this.router.navigate([`/sys/arch/files/edit/${this.setImgId}`]);
        break;
      default:
        break;
    }
  }

  selectOrder(type: string) {
    switch (type) {
      case 'byid':
        this.searchOrder = this.searchOrder === 'byid' ? 'byiddesc' : 'byid';
        break;
      case 'bycrtime':
        this.searchOrder =
          this.searchOrder === 'bycrtime' ? 'bycrtimedesc' : 'bycrtime';
        break;
      case 'byshifr':
        this.searchOrder =
          this.searchOrder === 'byshifr' ? 'byshifrdesc' : 'byshifr';
        break;
      case 'byfilename':
        this.searchOrder =
          this.searchOrder === 'byfilename' ? 'byfilenamedesc' : 'byfilename';
        break;
    }
    this.selectFilter(true);
  }

  selectFilter(event) {
    this.globalService.fileslistFilter = {
      searchOrder: this.searchOrder,
      filter_fond: this.filter_fond,
      filter_opis_nam: this.filter_opis_nam,
      filter_delo: this.filter_delo,
      filter_filename: this.filter_filename,
    };
    this.setOrgerStr();
    this.updateData();
  }

  async updateData() {
    this.globalService.fileslistFilter = {
      searchOrder: this.searchOrder,
      filter_fond: this.filter_fond,
      filter_opis_nam: this.filter_opis_nam,
      filter_delo: this.filter_delo,
      filter_filename: this.filter_filename,
    };
    try {
      let archivefolderfiles;
      let length;
      archivefolderfiles = this.archivefolderfileService.getWithFiltersOffsetLimitOrder(
        this.filter_fond,
        this.filter_opis_nam,
        this.filter_delo,
        this.filter_filename,
        0,
        this.pageSize,
        this.searchOrder
      );
      length = this.archivefolderfileService.getWithFilters_count(
        this.filter_fond,
        this.filter_opis_nam,
        this.filter_delo,
        this.filter_filename
      );
      this.length = (await length).rowcount;
      this.total = (await length).rowcount;
      // this.paginator.firstPage();
      this.page = 1;
      this.archivefolderfiles = isNullOrUndefined(await archivefolderfiles)
        ? []
        : await archivefolderfiles;
    } catch (err) {
      this.globalService.exceptionHandling(err);
    }
  }

  setOrgerStr() {
    switch (this.searchOrder) {
      case 'byid':
        this.postfixId =
          '<i class="fa fa-sort-amount-asc" aria-hidden="true"></i>';
        this.postfixCrtime = '';
        this.postfixShifr = '';
        this.postfixFilename = '';
        break;
      case 'byiddesc':
        this.postfixId =
          '<i class="fa fa-sort-amount-desc" aria-hidden="true"></i>';
        this.postfixCrtime = '';
        this.postfixShifr = '';
        this.postfixFilename = '';
        break;
      case 'bycrtime':
        this.postfixId = '';
        this.postfixCrtime =
          '<i class="fa fa-sort-amount-asc" aria-hidden="true"></i>';
        this.postfixShifr = '';
        this.postfixFilename = '';
        break;
      case 'bycrtimedesc':
        this.postfixId = '';
        this.postfixCrtime =
          '<i class="fa fa-sort-amount-desc" aria-hidden="true"></i>';
        this.postfixShifr = '';
        this.postfixFilename = '';
        break;
      case 'byshifr':
        this.postfixId = '';
        this.postfixCrtime = '';
        this.postfixShifr =
          '<i class="fa fa-sort-amount-asc" aria-hidden="true"></i>';
        this.postfixFilename = '';
        break;
      case 'byshifrdesc':
        this.postfixId = '';
        this.postfixCrtime = '';
        this.postfixShifr =
          '<i class="fa fa-sort-amount-desc" aria-hidden="true"></i>';
        this.postfixFilename = '';
        break;
      case 'byfilename':
        this.postfixId = '';
        this.postfixCrtime = '';
        this.postfixShifr = '';
        this.postfixFilename =
          '<i class="fa fa-sort-amount-asc" aria-hidden="true"></i>';
        break;
      case 'byfilenamedesc':
        this.postfixId = '';
        this.postfixCrtime = '';
        this.postfixShifr = '';
        this.postfixFilename =
          '<i class="fa fa-sort-amount-desc" aria-hidden="true"></i>';
        break;
    }
  }

  async setPageSizeOptions(event: PageEvent) {
    this.start = event.pageSize * event.pageIndex;
    try {
      let archivefolderfiles = this.archivefolderfileService.getWithFiltersOffsetLimitOrder(
        this.filter_fond,
        this.filter_opis_nam,
        this.filter_delo,
        this.filter_filename,
        this.start,
        event.pageSize,
        this.searchOrder
      );
      this.archivefolderfiles = isNullOrUndefined(await archivefolderfiles)
        ? []
        : await archivefolderfiles;
    } catch (err) {
      this.globalService.exceptionHandling(err);
    }
    this.pageSize = event.pageSize;
    this.pageEvent = event;
  }

  // isImgtype(filename: string): boolean {
  //   let res = false;
  //   switch (
  //     filename
  //       .toLowerCase()
  //       .split('.')
  //       .reverse()[0]
  //   ) {
  //     case 'png':
  //     case 'jpg':
  //     case 'jpeg':
  //     case 'bmp':
  //     case 'tif':
  //     case 'tiff':
  //       res = true;
  //       break;
  //     default:
  //       res = false;
  //       break;
  //   }
  //   return res;
  // }

  async onShowImgBack(idx) {
    this.openImgIdx = idx;
    this.setImgId = +this.globalService.fileslistsetImgId;
    let parchivefolderfiles: any[];
    let narchivefolderfiles: any[];
    let archivefolderfiles: any[];
    try {
      let _parchivefolderfiles = this.archivefolderfileService.getWithFiltersOffsetLimitOrder(
        this.filter_fond,
        this.filter_opis_nam,
        this.filter_delo,
        this.filter_filename,
        idx,
        this.subimgBufsize + 1,
        this.searchOrder,
        true
      );
      let _narchivefolderfiles = this.archivefolderfileService.getWithFiltersOffsetLimitOrder(
        this.filter_fond,
        this.filter_opis_nam,
        this.filter_delo,
        this.filter_filename,
        idx - this.subimgBufsize > 0 ? idx - this.subimgBufsize : 0,
        this.subimgBufsize,
        this.searchOrder,
        true
      );
      parchivefolderfiles = isNullOrUndefined(await _parchivefolderfiles)
        ? []
        : await _parchivefolderfiles;
      narchivefolderfiles = isNullOrUndefined(await _narchivefolderfiles)
        ? []
        : await _narchivefolderfiles;
      this._archivefolderfiles = [
        ...narchivefolderfiles,
        ...parchivefolderfiles,
      ];
      let used = {};
      this._archivefolderfiles = this._archivefolderfiles.filter((obj) => {
        return obj.id in used ? 0 : (used[obj.id] = 1);
      });
      let _idx = this._archivefolderfiles.findIndex(
        (item) => item.id === this.setImgId
      );
      archivefolderfiles = this._archivefolderfiles.slice(
        _idx - this.imgBufsize > 0 ? _idx - this.imgBufsize : 0,
        _idx + this.imgBufsize < this._archivefolderfiles.length
          ? _idx + this.imgBufsize
          : this._archivefolderfiles.length
      );
    } catch (err) {
      this.globalService.exceptionHandling(err);
    }
    this.lightboxImages.length = 0;
    this.inProgress = true;
    this.inProgressVal = 0;
    this.inProgressVal += 100 / archivefolderfiles.length;
    await Promise.all(
      archivefolderfiles.map((item) => {
        return new Promise(async (resolve) => {
          let resfile;
          if (this.globalService.isImgtype(item.archivefile.filename)) {
            let file = await this.archivefolderfileService.getFilePreviewById(
              item.id
            );
            this.inProgressVal += 100 / archivefolderfiles.length;
            resfile = new Blob([file], { type: 'image/jpg' });
            let reader = new FileReader();
            reader.readAsDataURL(resfile);
            reader.onloadend = () => {
              let base64data = String(reader.result);
              this.lightboxImages.push({
                src: base64data,
                caption: item.archivefile.filename,
                id: `${item.id}_!!%_${item.fond}_!!%_${item.creation_datetime}_!!%_${item.archivefolder_id}`,
                orientation: item.orientation,
              });
              switch (this.searchOrder) {
                case 'byid':
                  this.lightboxImages.sort(
                    (a, b) =>
                      +`${a.id}`.split('_!!%_')[0] -
                      +`${b.id}`.split('_!!%_')[0]
                  );
                  break;
                case 'byiddesc':
                  this.lightboxImages.sort(
                    (a, b) =>
                      +`${b.id}`.split('_!!%_')[0] -
                      +`${a.id}`.split('_!!%_')[0]
                  );
                  break;
                case 'bycrtime':
                  this.lightboxImages.sort((a, b) => {
                    if (
                      new Date(`${a.id}`.split('_!!%_')[2]) >
                      new Date(`${b.id}`.split('_!!%_')[2])
                    ) {
                      return 1;
                    } else {
                      return -1;
                    }
                  });
                  break;
                case 'bycrtimedesc':
                  this.lightboxImages.sort((a, b) => {
                    if (
                      new Date(`${a.id}`.split('_!!%_')[2]) <
                      new Date(`${b.id}`.split('_!!%_')[2])
                    ) {
                      return 1;
                    } else {
                      return -1;
                    }
                  });
                  break;
                case 'byshifr':
                  this.lightboxImages.sort(
                    (a, b) =>
                      +`${a.id}`.split('_!!%_')[1] -
                      +`${b.id}`.split('_!!%_')[1]
                  );
                  break;
                case 'byshifrdesc':
                  this.lightboxImages.sort(
                    (a, b) =>
                      +`${b.id}`.split('_!!%_')[1] -
                      +`${a.id}`.split('_!!%_')[1]
                  );
                  break;
                case 'byfilename':
                  this.lightboxImages.sort((a, b) =>
                    a.caption > b.caption ? 1 : -1
                  );
                  break;
                case 'byfilenamedesc':
                  this.lightboxImages.sort((a, b) =>
                    a.caption < b.caption ? 1 : -1
                  );
                  break;
              }
              resolve(null);
            };
          }
        });
      })
    );
    this.inProgress = false;
    setTimeout(() => {
      this.setImgIdx = this.lightboxImages.findIndex(
        (item) => +`${item.id}`.split('_!!%_')[0] === this.setImgId
      );
      this.setArchivefolderId = +`${
        this.lightboxImages[this.setImgIdx].id
      }`.split('_!!%_')[3];
      this.lightbox.open(this.setImgIdx);
    }, 300);
  }

  async onShowImg(idx) {
    this.openImgIdx = this.start + idx;
    let id = this.archivefolderfiles[idx].id;
    this.setImgId = id;
    let parchivefolderfiles: any[];
    let narchivefolderfiles: any[];
    let archivefolderfiles: any[];
    try {
      let _parchivefolderfiles = this.archivefolderfileService.getWithFiltersOffsetLimitOrder(
        this.filter_fond,
        this.filter_opis_nam,
        this.filter_delo,
        this.filter_filename,
        this.start + idx,
        this.subimgBufsize + 1,
        this.searchOrder,
        true
      );
      let _narchivefolderfiles = this.archivefolderfileService.getWithFiltersOffsetLimitOrder(
        this.filter_fond,
        this.filter_opis_nam,
        this.filter_delo,
        this.filter_filename,
        this.start + idx - this.subimgBufsize > 0
          ? this.start + idx - this.subimgBufsize
          : 0,
        this.subimgBufsize,
        this.searchOrder,
        true
      );
      parchivefolderfiles = isNullOrUndefined(await _parchivefolderfiles)
        ? []
        : await _parchivefolderfiles;
      narchivefolderfiles = isNullOrUndefined(await _narchivefolderfiles)
        ? []
        : await _narchivefolderfiles;
      this._archivefolderfiles = [
        ...narchivefolderfiles,
        ...parchivefolderfiles,
      ];
      let used = {};
      this._archivefolderfiles = this._archivefolderfiles.filter((obj) => {
        return obj.id in used ? 0 : (used[obj.id] = 1);
      });
      let _idx = this._archivefolderfiles.findIndex((item) => item.id === id);
      archivefolderfiles = this._archivefolderfiles.slice(
        _idx - this.imgBufsize > 0 ? _idx - this.imgBufsize : 0,
        _idx + this.imgBufsize < this._archivefolderfiles.length
          ? _idx + this.imgBufsize
          : this._archivefolderfiles.length
      );
    } catch (err) {
      this.globalService.exceptionHandling(err);
    }
    this.lightboxImages.length = 0;
    this.inProgress = true;
    this.inProgressVal = 0;
    this.inProgressVal += 100 / archivefolderfiles.length;
    await Promise.all(
      archivefolderfiles.map((item) => {
        return new Promise(async (resolve) => {
          let resfile;
          if (this.globalService.isImgtype(item.archivefile.filename)) {
            try {
              let file = await this.archivefolderfileService.getFilePreviewById(
                item.id
              );
              this.inProgressVal += 100 / archivefolderfiles.length;
              resfile = new Blob([file], { type: 'image/jpg' });
              let reader = new FileReader();
              reader.readAsDataURL(resfile);
              reader.onloadend = () => {
                let base64data = String(reader.result);
                this.lightboxImages.push({
                  src: base64data,
                  caption: item.archivefile.filename,
                  // id: `${item.id}_!!%_${item.fond}_!!%_${item.creation_datetime}_!!%_${item.archivefolder_id}`
                  id: `${item.id}_!!%_${this.archivefolderfiles.findIndex(
                    (_item) => _item.id === item.id
                  )}_!!%_${item.creation_datetime}_!!%_${
                    item.archivefolder_id
                  }`,
                  orientation: item.orientation,
                });
                switch (this.searchOrder) {
                  case 'byid':
                    this.lightboxImages.sort(
                      (a, b) =>
                        +`${a.id}`.split('_!!%_')[0] -
                        +`${b.id}`.split('_!!%_')[0]
                    );
                    break;
                  case 'byiddesc':
                    this.lightboxImages.sort(
                      (a, b) =>
                        +`${b.id}`.split('_!!%_')[0] -
                        +`${a.id}`.split('_!!%_')[0]
                    );
                    break;
                  case 'bycrtime':
                    this.lightboxImages.sort((a, b) => {
                      if (
                        new Date(`${a.id}`.split('_!!%_')[2]) >
                        new Date(`${b.id}`.split('_!!%_')[2])
                      ) {
                        return 1;
                      } else {
                        return -1;
                      }
                    });
                    break;
                  case 'bycrtimedesc':
                    this.lightboxImages.sort((a, b) => {
                      if (
                        new Date(`${a.id}`.split('_!!%_')[2]) <
                        new Date(`${b.id}`.split('_!!%_')[2])
                      ) {
                        return 1;
                      } else {
                        return -1;
                      }
                    });
                    break;
                  case 'byshifr':
                    this.lightboxImages.sort(
                      (a, b) =>
                        +`${a.id}`.split('_!!%_')[1] -
                        +`${b.id}`.split('_!!%_')[1]
                    );
                    break;
                  case 'byshifrdesc':
                    this.lightboxImages.sort(
                      (a, b) =>
                        +`${b.id}`.split('_!!%_')[1] -
                        +`${a.id}`.split('_!!%_')[1]
                    );
                    break;
                  case 'byfilename':
                    this.lightboxImages.sort((a, b) =>
                      a.caption > b.caption ? 1 : -1
                    );
                    break;
                  case 'byfilenamedesc':
                    this.lightboxImages.sort((a, b) =>
                      a.caption < b.caption ? 1 : -1
                    );
                    break;
                }
                resolve(null);
              };
            } catch (err) {
              resolve(null);
            }
          }
        });
      })
    );
    this.inProgress = false;
    setTimeout(() => {
      this.setImgIdx = this.lightboxImages.findIndex(
        (item) => +`${item.id}`.split('_!!%_')[0] === id
      );
      this.setArchivefolderId = +`${
        this.lightboxImages[this.setImgIdx].id
      }`.split('_!!%_')[3];
      this.lightbox.open(this.setImgIdx);
    }, 300);
  }

  async addPrevImg() {
    // console.log(this._archivefolderfiles);
    let firstId = `${this.lightboxImages[0].id}`.split('_!!%_')[0];
    let idx = this._archivefolderfiles.findIndex(
      (item) => item.id === +firstId
    );
    if (idx === 0 || idx === -1) {
      return false;
    } else {
      // this.bufflightboxImages = [...this.lightboxImages];
      // console.log(this.bufflightboxImages);
      this.spinnerService.show();
      this.galleryWait = true;
      let file;
      try {
        file = await this.archivefolderfileService.getFilePreviewById(
          this._archivefolderfiles[idx - 1].id
        );
      } catch (err) {
        console.error(err);
        this.openSnackBar(
          `Ошибка при обработке изображения ${
            this._archivefolderfiles[idx - 1].archivefile.filename
          }`,
          'Ok'
        );
      } finally {
        this.spinnerService.hide();
        this.galleryWait = false;
      }
      let resfile = new Blob([file], { type: 'image/jpg' });
      let reader = new FileReader();
      reader.readAsDataURL(resfile);
      reader.onloadend = () => {
        let base64data = String(reader.result);
        // this.galleryImages.unshift({
        //   small: base64data,
        //   medium: base64data,
        //   big: base64data,
        //   description: this._archivefolderfiles[idx - 1].archivefile.filename,
        //   label: `${this._archivefolderfiles[idx - 1].id}_!!%_${
        //     this._archivefolderfiles[idx - 1].fond
        //   }_!!%_${this._archivefolderfiles[idx - 1].creation_datetime}_!!%_${
        //     this._archivefolderfiles[idx - 1].archivefolder_id
        //   }`
        // });
        // this.bufflightboxImages.unshift({
        //   src: base64data,
        //   caption: this._archivefolderfiles[idx - 1].archivefile.filename,
        //   id: `${this._archivefolderfiles[idx - 1].id}_!!%_${
        //     this._archivefolderfiles[idx - 1].fond
        //   }_!!%_${this._archivefolderfiles[idx - 1].creation_datetime}_!!%_${
        //     this._archivefolderfiles[idx - 1].archivefolder_id
        //   }`
        // });
        this.lightbox.unshiftImg({
          src: base64data,
          caption: this._archivefolderfiles[idx - 1].archivefile.filename,
          id: `${this._archivefolderfiles[idx - 1].id}_!!%_${
            this._archivefolderfiles[idx - 1].fond
          }_!!%_${this._archivefolderfiles[idx - 1].creation_datetime}_!!%_${
            this._archivefolderfiles[idx - 1].archivefolder_id
          }`,
          orientation: this._archivefolderfiles[idx - 1].orientation,
        });
      };
      return true;
    }
  }

  async addNextImg() {
    let lastId = `${
      this.lightboxImages[this.lightboxImages.length - 1].id
    }`.split('_!!%_')[0];
    let idx = this._archivefolderfiles.findIndex((item) => item.id === +lastId);
    if (idx > this._archivefolderfiles.length - 2 || idx === -1) {
      return false;
    } else {
      this.spinnerService.show();
      this.galleryWait = true;
      let file;
      try {
        file = await this.archivefolderfileService.getFilePreviewById(
          this._archivefolderfiles[idx + 1].id
        );
      } catch (err) {
        console.error(err);
        this.openSnackBar(
          `Ошибка при обработке изображения ${
            this._archivefolderfiles[idx + 1].archivefile.filename
          }`,
          'Ok'
        );
      } finally {
        this.spinnerService.hide();
        this.galleryWait = false;
      }
      let resfile = new Blob([file], { type: 'image/jpg' });
      let reader = new FileReader();
      reader.readAsDataURL(resfile);
      reader.onloadend = () => {
        let base64data = String(reader.result);
        // this.lightboxImages.push({
        //   src: base64data,
        //   caption: this._archivefolderfiles[idx + 1].archivefile.filename,
        //   id: `${this._archivefolderfiles[idx + 1].id}_!!%_${
        //     this._archivefolderfiles[idx + 1].fond
        //   }_!!%_${this._archivefolderfiles[idx + 1].creation_datetime}_!!%_${
        //     this._archivefolderfiles[idx + 1].archivefolder_id
        //   }`
        // });
        this.lightbox.pushImg({
          src: base64data,
          caption: this._archivefolderfiles[idx + 1].archivefile.filename,
          id: `${this._archivefolderfiles[idx + 1].id}_!!%_${
            this._archivefolderfiles[idx + 1].fond
          }_!!%_${this._archivefolderfiles[idx + 1].creation_datetime}_!!%_${
            this._archivefolderfiles[idx + 1].archivefolder_id
          }`,
          orientation: this._archivefolderfiles[idx + 1].orientation,
        });
      };
      return true;
    }
  }

  async previewChange(e) {
    if (e.forward === false) {
      await this.addPrevImg();
    } else {
      await this.addNextImg();
    }
    this.setImgId = +e.image.id.split('_!!%_')[0]; // id текущей картинки
    this.setArchivefolderId = +e.image.id.split('_!!%_')[3]; // id документа текущей картинки
  }

  onDownloadfile(id) {
    this.spinnerService.show();
    let fileName = '';
    for (let i = 0; i < this.archivefolderfiles.length; i++) {
      if (!isNullOrUndefined(this.archivefolderfiles[i])) {
        if (id === this.archivefolderfiles[i].id) {
          fileName = this.archivefolderfiles[i].archivefile.filename;
        }
      }
    }
    try {
      let blob = this.archivefolderfileService.downloadOneById(id);
      this.inProgress = true;
      this.inProgressVal = 0;
      blob.subscribe((event) => {
        if (event.type === HttpEventType.DownloadProgress) {
          const percentDone = Math.round((100 * event.loaded) / event.total);
          this.inProgressVal = percentDone;
        } else if (event instanceof HttpResponse) {
          this.inProgress = false;
          this.inProgressVal = 0;
          this.spinnerService.hide();
          importedSaveAs(event.body, fileName);
        }
      });
    } catch (err) {
      this.globalService.exceptionHandling(err);
      this.spinnerService.hide();
    }
  }

  goToPage(n: number): void {
    this.page = n;
    this.updatePage();
  }

  onNext(): void {
    this.page++;
    this.updatePage();
  }

  onPrev(): void {
    this.page = 1;
    this.updatePage();
  }

  onNext10(n = 1): void {
    this.page += 10 * n;
    this.updatePage();
  }

  onPrev10(n = 1): void {
    this.page -= 10 * n;
    this.updatePage();
  }

  async updatePage() {
    let start = this.limit * (this.page - 1);
    try {
      let archivefolderfiles = this.archivefolderfileService.getWithFiltersOffsetLimitOrder(
        this.filter_fond,
        this.filter_opis_nam,
        this.filter_delo,
        this.filter_filename,
        start,
        this.limit,
        this.searchOrder
      );
      this.archivefolderfiles = isNullOrUndefined(await archivefolderfiles)
        ? []
        : await archivefolderfiles;
    } catch (err) {
      this.globalService.exceptionHandling(err);
    }
  }

  async onOrientation(obj) {
    this.spinnerService.show();
    let id = +obj.id.split('_!!%_')[0];
    let idx = this._archivefolderfiles.findIndex((item) => item.id === id);
    this._archivefolderfiles[idx].orientation = obj.val;
    try {
      await this.archivefolderfileService.putOneById(id, {
        orientation: obj.val,
      });
    } catch (err) {
      this.globalService.exceptionHandling(err);
    } finally {
      this.spinnerService.hide();
    }
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 10000,
    });
  }
}
