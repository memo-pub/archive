import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { ImageCroppedEvent, ImageCropperComponent } from 'ngx-image-cropper';
import { ArchivefolderfileService } from 'src/app/system/shared/services/archivefolderfile.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { GlobalService } from 'src/app/shared/services/global.service';

@Component({
  selector: 'app-files-edit',
  templateUrl: './files-edit.component.html',
  styleUrls: ['./files-edit.component.css'],
})
export class FilesEditComponent implements OnInit {
  id: number;
  filename = '';

  show = false;
  croppedImage: any;
  imgfile: any;
  maxHeight = 500;
  // aspectRatio = 4/3;

  activeRouterObserver;

  @ViewChild(ImageCropperComponent) imageCropper: ImageCropperComponent;
  @HostListener('window:keydown', ['$event'])
  onKeyPress(event: KeyboardEvent) {
    if ((event.ctrlKey || event.metaKey) && event.keyCode == 83) {
      event.preventDefault();
      this.onSave();
    }
  }
  constructor(
    private archivefolderfileService: ArchivefolderfileService,
    private globalService: GlobalService,
    private spinnerService: NgxSpinnerService,
    private location: Location,
    private activatedRoute: ActivatedRoute
  ) {
    this.activeRouterObserver = this.activatedRoute.params.subscribe(
      (param) => {
        this.id = param.id;
      }
    );
  }

  async ngOnInit() {
    try {
      this.maxHeight = window.innerHeight - 280;
      this.spinnerService.show();
      let archivefolderfile = this.archivefolderfileService.getOneById(this.id);
      let file = await this.archivefolderfileService.getFileById(this.id);
      let resfile = new Blob([file], { type: 'image/jpg' });
      let reader = new FileReader();
      reader.readAsDataURL(resfile);
      reader.onloadend = () => {
        let base64data = String(reader.result);
        this.imgfile = base64data;
        this.show = true;
      };
      this.filename = (await archivefolderfile).archivefile.filename;
    } catch (err) {
      this.globalService.exceptionHandling(err);
    } finally {
      this.spinnerService.hide();
    }
  }

  fileChangeEvent(event: any): void {
    // this.imageChangedEvent = event;
  }
  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.file;
  }
  imageLoaded() {
    // show cropper
  }
  cropperReady() {
    // cropper ready
  }
  loadImageFailed() {
    // show message
  }
  rotateLeft() {
    this.imageCropper.rotateLeft();
  }
  rotateRight() {
    this.imageCropper.rotateRight();
  }
  flipHorizontal() {
    this.imageCropper.flipHorizontal();
  }
  flipVertical() {
    this.imageCropper.flipVertical();
  }

  async onSave() {
    this.spinnerService.show();
    try {
      this.croppedImage['lastModifiedDate'] = new Date();
      let formData: FormData = new FormData();
      formData.append('archivefile', this.croppedImage, this.filename);
      let res = await this.archivefolderfileService.putOneById(
        this.id,
        formData
      );
      this.location.back();
    } catch (err) {
      this.globalService.exceptionHandling(err);
    } finally {
      this.spinnerService.hide();
    }
  }

  onGoback() {
    this.location.back();
  }
}
