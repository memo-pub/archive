import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { GlobalService } from 'src/app/shared/services/global.service';

@Component({
  selector: 'app-files',
  templateUrl: './files.component.html',
  styleUrls: ['./files.component.css']
})
export class FilesComponent implements OnInit, OnDestroy {
  constructor(private globalService: GlobalService, private router: Router) {
    this.globalService.routerObserver = router.events.subscribe(val => {
      if (val instanceof NavigationEnd) {
        if (val.url === '/sys/arch/files') {
          this.globalService.fileslistsetImgIdx = -1;
          this.globalService.fileslistsetImgIdxCount = 0;
          this.router.navigate(['/sys/arch/files/list']);
        }
      }
    });
  }

  ngOnInit() {}

  ngOnDestroy(): void {
    this.globalService.routerObserver.unsubscribe();
  }
}
