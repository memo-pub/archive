import { NgModule } from '@angular/core';

import { MarchiveRoutingModule } from './marchive-routing.module';
import { MarchiveComponent } from './marchive.component';
import { MarchiveListComponent } from './marchive-list/marchive-list.component';
import { MarchiveEditComponent } from './marchive-edit/marchive-edit.component';
import { SharedModule } from '../../shared/shared.module';
import { OpisListComponent } from './opis-list/opis-list.component';
import { OpisEditComponent } from './opis-edit/opis-edit.component';
import { OpisitemsFilterPipe } from '../shared/pipes/opisitems-filter.pipe';
import { ArchivefolderFilterPipe } from '../shared/pipes/archivefolder-filter.pipe';
import { FilesComponent } from './files/files.component';
import { FilesEditComponent } from './files/files-edit/files-edit.component';
import { FilesListComponent } from './files/files-list/files-list.component';
import { FondListComponent } from './fond-list/fond-list.component';
import { FonditemsFilterPipe } from '../shared/pipes/fonditems-filter.pipe';
import { FondEditComponent } from './fond-edit/fond-edit.component';
import { UsagetEditComponent } from './usaget-edit/usaget-edit.component';

@NgModule({
  imports: [SharedModule, MarchiveRoutingModule],
  declarations: [
    MarchiveComponent,
    MarchiveListComponent,
    MarchiveEditComponent,
    OpisListComponent,
    FondListComponent,
    OpisEditComponent,
    FondEditComponent,
    OpisitemsFilterPipe,
    FonditemsFilterPipe,
    ArchivefolderFilterPipe,
    FilesListComponent,
    FilesEditComponent,
    FilesComponent,
    UsagetEditComponent,
  ],
})
export class MarchiveModule {}
