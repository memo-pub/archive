import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import {HttpClientModule} from '@angular/common/http';
import { MatDialogModule} from '@angular/material/dialog';
import { OpisitemsFilterPipe } from '../../shared/pipes/opisitems-filter.pipe';

import { OpisListComponent } from './opis-list.component';

describe('OpisListComponent', () => {
  let component: OpisListComponent;
  let fixture: ComponentFixture<OpisListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpisListComponent, OpisitemsFilterPipe ],
      imports: [RouterTestingModule, MatSnackBarModule, HttpClientTestingModule, MatDialogModule],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpisListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
