import { Component, OnInit, HostListener, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

import { Muser } from '../../../shared/models/muser.model';
import { AuthService } from '../../../shared/services/auth.service';
import { MuserService } from '../../shared/services/muser.service';
import { Opis } from '../../../shared/models/opis.model';
import { isNullOrUndefined } from 'util';
import { PageEvent, MatPaginator } from '@angular/material/paginator';
import { OpisService } from '../../shared/services/opis.service';
import { FiltersService } from '../../shared/services/filters.service';

@Component({
  selector: 'app-opis-list',
  templateUrl: './opis-list.component.html',
  styleUrls: ['./opis-list.component.css']
})
export class OpisListComponent implements OnInit {
  loggedInUser: Muser;
  // searchStr = '';
  opisItems: Opis[];
  opisItemsView: Opis[];

  // length = 100;
  // pageSize = 100;
  // pageSizeOptions: number[] = [5, 10, 25, 100];
  // pageEvent: PageEvent;
  start = 0;
  end = 0;

  /**Для пагинатора */
  // page = 1;
  limit = 100;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    public filterService: FiltersService,
    private authService: AuthService,
    private muserService: MuserService,
    private opisService: OpisService,
    private router: Router,
    private spinnerService: NgxSpinnerService
  ) {}

  async ngOnInit() {
    this.spinnerService.show();
    try {
      let loggedInUser = this.muserService.getOneById(
        this.authService.getCurUser().id
      );
      this.loggedInUser = await loggedInUser;
      let opisItems = this.opisService.getAll();
      this.opisItems = isNullOrUndefined(await opisItems)
        ? []
        : await opisItems;
      // this.opisItems.sort((a, b) => (a.id > b.id ? 1 : -1));
      // this.length = this.opisItems.length;
      // if (this.length > 100) {
      //   this.pageSizeOptions.push(this.length);
      // }
      this.end = this.limit;
    } catch (err) {
      if (err.status === 401) {
        console.error(`Ошибка авторизации`);
        this.authService.logout();
      } else {
        console.error(`Ошибка ${err}`);
      }
    } finally {
      this.spinnerService.hide();
    }
  }

  onAddOpisitem() {
    this.router.navigate(['/sys/arch/o-edit/newdoc']);
  }

  onEditOpisitem(id) {
    this.router.navigate([`/sys/arch/o-edit/${id}`]);
  }

  updateData() {
    if (this.filterService.opisListFilter.searchStr.length > 0) {
      this.filterService.opisListFilter.page = 1;
      this.start = 0;
      // this.end = this.paginator.pageSize;
      this.end = this.limit;
      // this.paginator.firstPage();
      this.filterService.opisListFilter.page = 1;
    } else {
      // this.start = this.paginator.pageSize * this.paginator.pageIndex;
      // this.end = this.start + this.paginator.pageSize;
      this.start = this.limit * (this.filterService.opisListFilter.page - 1);
      this.end = this.start + this.limit;
    }
  }

  // setPageSizeOptions(event: PageEvent) {
  //   this.start = event.pageSize * event.pageIndex;
  //   this.end = this.start + event.pageSize;
  //   return event;
  // }

  goToPage(n: number): void {
    this.filterService.opisListFilter.page = n;
    this.updatePage();
  }

  onNext(): void {
    this.filterService.opisListFilter.page++;
    this.updatePage();
  }

  onPrev(): void {
    this.filterService.opisListFilter.page = 1;
    this.updatePage();
  }

  onNext10(n = 1): void {
    this.filterService.opisListFilter.page += 10 * n;
    this.updatePage();
  }

  onPrev10(n = 1): void {
    this.filterService.opisListFilter.page -= 10 * n;
    this.updatePage();
  }

  async updatePage() {
    this.start = this.limit * (this.filterService.opisListFilter.page - 1);
    this.end = this.start + this.limit;
  }
}
