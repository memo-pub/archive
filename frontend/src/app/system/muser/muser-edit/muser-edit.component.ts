import { Component, OnInit, HostListener, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { isNullOrUndefined, isUndefined } from 'util';

import { Muser, muser_width } from '../../../shared/models/muser.model';
import { AuthService } from '../../../shared/services/auth.service';
import { MuserService } from '../../shared/services/muser.service';
import { GlobalService } from '../../../shared/services/global.service';
import { DialogYesNoComponent } from '../../shared/components/dialog-yes-no/dialog-yes-no.component';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-muser-edit',
  templateUrl: './muser-edit.component.html',
  styleUrls: ['./muser-edit.component.css'],
})
export class MuserEditComponent implements OnInit, OnDestroy {
  constructor(
    private authService: AuthService,
    private muserService: MuserService,
    private globalService: GlobalService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private spinnerService: NgxSpinnerService,
    public dialog: MatDialog,
    public snackBar: MatSnackBar
  ) {
    this.activeRouterObserver = this.activatedRoute.params.subscribe(
      (param) => {
        this.id = param.id;
        if (param.id === 'myprofile') {
          this.intType = 'myprofile';
          this.ngOnInit();
        } else if (param.id === 'newprofile') {
          this.intType = 'newprofile';
        } else {
          this.id = +param.id;
          this.intType = 'editprofile';
        }
      }
    );
  }
  muser_width = muser_width;

  id: number;
  intType: string | any = ''; // тип интерфейса
  loggedInUser: Muser;
  selectedUser: Muser;
  musers: Muser[];
  muserForm: FormGroup;
  accessLogin = true;
  muserTypes: any[];
  isAdmin = this.authService.getCurUser()?.isadmin;
  activeRouterObserver: Subscription;

  readonly = true;
  ngOnDestroy(): void {
    // this.activeRouterObserver.unsubscribe();
  }
  @HostListener('window:keydown', ['$event'])
  onKeyPress(event: KeyboardEvent) {
    if ((event.ctrlKey || event.metaKey) && event.keyCode == 83) {
      event.preventDefault();
      if (!this.muserForm.invalid && this.intType !== 'myprofile') {
        this.onSaveMuserForm();
      }
    }
  }

  async ngOnInit() {
    this.spinnerService.show();
    this.muserTypes = this.globalService.muserTypes;
    this.muserForm = new FormGroup({
      isadmin: new FormControl({
        value: false,
        disabled: !this.isAdmin,
      }),
      login: new FormControl(
        { value: null, disabled: (!this.isAdmin || this.intType === 'myprofile') },
        [Validators.required]
      ),
      name: new FormControl(
        { value: null, disabled: (!this.isAdmin || this.intType === 'myprofile') },
        [Validators.required]
      ),
      surname: new FormControl(
        { value: null, disabled: (!this.isAdmin || this.intType === 'myprofile') },
        [Validators.required]
      ),
      usertype: new FormControl(
        { value: null, disabled: (!this.isAdmin || this.intType === 'myprofile') },
        [Validators.required]
      ),
      password:
        this.intType === 'newprofile'
          ? new FormControl({ value: null, disabled: !this.isAdmin }, [Validators.required])
          : new FormControl({ value: null, disabled: !this.isAdmin }),
    });
    try {
      let loggedInUser = this.muserService.getOneById(
        this.authService.getCurUser().id
      );
      let musers = this.muserService.getAll();
      this.loggedInUser = await loggedInUser;
      this.musers = isNullOrUndefined(await musers) ? [] : await musers;
      await this.getData();
    } catch (err) {
      if (err.status === 401) {
        console.error(`Ошибка авторизации`);
        this.authService.logout();
      } else {
        console.error(`Ошибка ${err}`);
      }
    } finally {
      this.spinnerService.hide();
    }
  }

  async getData() {
    switch (this.intType) {
      case 'myprofile':
        this.muserForm.patchValue({
          name: this.loggedInUser.name,
          surname: this.loggedInUser.surname,
          login: this.loggedInUser.login,
          isadmin: this.loggedInUser.isadmin,
          usertype: this.loggedInUser.usertype,
        });
        break;
      case 'newprofile':
        this.muserForm.patchValue({
          name: '',
          surname: '',
          login: '',
          password: null,
          isadmin: false,
          usertype: -1,
        });
        break;
      case 'editprofile':
        this.selectedUser = this.musers.find((muser) => muser.id === this.id);
        this.muserForm.patchValue({
          name: this.selectedUser.name,
          surname: this.selectedUser.surname,
          login: this.selectedUser.login,
          isadmin: this.selectedUser.isadmin,
          usertype: this.selectedUser.usertype,
          password: null,
        });
        break;
      default:
    }
  }

  async onSaveMuserForm() {
    if (
      this.musers.filter((muser) => muser.login === this.muserForm.value.login)
        .length > 0 &&
      this.muserForm.value['login'] !== this.selectedUser.login
    ) {
      this.openSnackBar(`Этот логин уже занят.`, 'Ok');
      return;
    }
    switch (this.intType) {
      case 'newprofile':
        this.spinnerService.show();
        try {
          let resPostMuser = await this.muserService.postOne(
            this.muserForm.value
          );
          this.musers.push(resPostMuser);
          this.getData();
          this.readonly = true;
          this.openSnackBar(`Пользователь добавлен.`, 'Ok');
        } catch (err) {
          console.error(`Ошибка ${err}`);
          this.openSnackBar(`Ошибка ${err.status}`, 'Ok');
        } finally {
          this.spinnerService.hide();
        }
        break;
      case 'editprofile':
        this.spinnerService.show();
        if (
          this.muserForm.value['password'] == null ||
          this.muserForm.value['password'] === ''
        ) {
          this.muserForm.removeControl('password');
        }
        try {
          let resPutMuser = await this.muserService.putOneById(
            this.id,
            this.muserForm.value
          );
          if (!isNullOrUndefined(resPutMuser)) {
            this.musers.splice(
              this.musers.findIndex((muser) => muser.id === resPutMuser.id),
              1
            );
            this.musers.push(resPutMuser);
          }
          this.getData();
          this.openSnackBar(`Сохранено.`, 'Ok');
        } catch (err) {
          console.error(`Ошибка ${err}`);
          this.openSnackBar(`Ошибка ${err.status}`, 'Ok');
        } finally {
          if (isUndefined(this.muserForm.value['password'])) {
            this.muserForm.addControl('password', new FormControl(null));
          }
          this.spinnerService.hide();
        }
        break;
      default:
        break;
    }
  }

  async onDelMuser() {
    let dialogRef = this.dialog.open(DialogYesNoComponent, {
      data: {
        title: 'Удалить пользователя',
        question: `${this.selectedUser.name} ${this.selectedUser.surname}?`,
      },
    });
    dialogRef.afterClosed().subscribe(async (result) => {
      if (result === true) {
        this.spinnerService.show();
        try {
          let resDelMuser = await this.muserService.deleteOneById(this.id);
          this.router.navigate(['/sys/users/list']);
          this.openSnackBar(`Удалено.`, 'Ok');
        } catch (err) {
          console.error(`Ошибка при удалении пользователя ${err}`);
          this.openSnackBar(`Ошибка ${err.status}`, 'Ok');
        } finally {
          this.spinnerService.hide();
        }
      }
    });
  }

  onIsadminChange() {
    if (this.muserForm.value.isadmin) {
      this.muserForm.patchValue({
        usertype: -1,
      });
    }
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 10000,
    });
  }
}
