import { Component, OnInit, HostListener } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MuserService } from '../../shared/services/muser.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { MatSnackBar } from '@angular/material/snack-bar';
const now = new Date();

@Component({
  selector: 'app-muser-report',
  templateUrl: './muser-report.component.html',
  styleUrls: ['./muser-report.component.css']
})
export class MuserReportComponent implements OnInit {
  datestart: NgbDateStruct;
  dateend: NgbDateStruct;
  _datestart: NgbDateStruct;
  _dateend: NgbDateStruct;
  resType1 = [];
  resType2 = [];
  resType3 = [];
  kolvoStr = '';
  kolvoZ = '';
  kolvoS = '';
  kolvoP = '';
  kolvoV = '';
  strStr = '';
  strspecStr = '';
  selectedType = -1;
  selectedTypeIdx = 0;

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.kolvoStr = event.target.innerWidth < 700 ? 'Кол.' : 'Количество';
    this.kolvoZ =
      event.target.innerWidth < 700 ? 'Кол.зап.' : 'Кол. заполненных';
    this.kolvoS =
      event.target.innerWidth < 700 ? 'Кол.созд.' : 'Кол. созданных';
    this.kolvoP =
      event.target.innerWidth < 700 ? 'Кол.перс.' : 'Кол. персоналий';
    this.kolvoV = event.target.innerWidth < 700 ? 'Кол.влож.' : 'Кол. вложений';
    this.strStr = event.target.innerWidth < 700 ? 'С.' : 'Страниц';
    this.strspecStr =
      event.target.innerWidth < 700 ? 'С.cп.' : 'Cтр. спецобработки';
  }

  constructor(
    private muserService: MuserService,
    public snackBar: MatSnackBar,
    private spinnerService: NgxSpinnerService
  ) {}

  async ngOnInit() {
    this.kolvoStr = window.screen.width < 675 ? 'Кол.' : 'Количество';
    this.kolvoZ = window.screen.width < 700 ? 'Кол.зап.' : 'Кол. заполненных';
    this.kolvoS = window.screen.width < 700 ? 'Кол.созд.' : 'Кол. созданных';
    this.strStr = window.screen.width < 675 ? 'С.' : 'Страниц';
    this.kolvoP = window.screen.width < 700 ? 'Кол.перс.' : 'Кол. персоналий';
    this.kolvoV = window.screen.width < 700 ? 'Кол.влож.' : 'Кол. вложений';
    this.strspecStr =
      window.screen.width < 675 ? 'С.cп.' : 'Cтр. спецобработки';
    this.datestart = {
      year: now.getFullYear(),
      month: now.getMonth() + 1,
      day: now.getDate()
    };
    this.dateend = {
      year: now.getFullYear(),
      month: now.getMonth() + 1,
      day: now.getDate()
    };
    this._datestart = {
      year: now.getFullYear(),
      month: now.getMonth() + 1,
      day: now.getDate()
    };
    this._dateend = {
      year: now.getFullYear(),
      month: now.getMonth() + 1,
      day: now.getDate()
    };
    this.getData();
  }

  async getData() {
    this.spinnerService.show();
    try {
      if (
        JSON.stringify(this.datestart) === JSON.stringify(this._datestart) &&
        JSON.stringify(this.dateend) === JSON.stringify(this._dateend)
      ) {
        return;
      } else {
        this._datestart = this.datestart;
        this._dateend = this.dateend;
      }
      let start = `${this.datestart.year}.${this.datestart.month}.${this.datestart.day}`;
      let end = `${this.dateend.year}.${this.dateend.month}.${this.dateend.day}`;
      if (
        !(
          new Date(
            this.datestart.year,
            this.datestart.month,
            this.datestart.day,
            0,
            0
          ) instanceof Date &&
          !isNaN(
            new Date(
              this.datestart.year,
              this.datestart.month,
              this.datestart.day,
              0,
              0
            ).valueOf()
          )
        )
      ) {
        this.openSnackBar(`Введена не существующая дата начала`, 'Ok');
        return;
      }
      if (
        !(
          new Date(
            this.dateend.year,
            this.dateend.month,
            this.dateend.day,
            0,
            0
          ) instanceof Date &&
          !isNaN(
            new Date(
              this.dateend.year,
              this.dateend.month,
              this.dateend.day,
              0,
              0
            ).valueOf()
          )
        )
      ) {
        this.openSnackBar(`Введена не существующая дата конца`, 'Ok');
        return;
      }
      let resType1 = this.muserService.getArchstatistic(start, end);
      let resType2 = this.muserService.getScanstatistic(start, end);
      let resType3 = this.muserService.getPersonal(start, end);
      this.resType1 = await resType1;
      this.resType2 = await resType2;
      this.resType3 = await resType3;
      // console.log(this.resType1);
      // console.log(this.resType2);
      // console.log(this.resType3);
    } catch (err) {
      console.error(`Ошибка ${err}`);
    } finally {
      this.spinnerService.hide();
    }
  }

  selectType(event) {
    // this.selectedType = event.tab.origin;
    // console.log(event.tab);
  }

  toLength(val, l: number) {
    let res = `${val}`.padEnd(l);
    // console.log(res);
    return res;
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 5000
    });
  }
}
