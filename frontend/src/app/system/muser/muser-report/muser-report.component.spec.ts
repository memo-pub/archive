import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import {HttpClientModule} from '@angular/common/http';
import { MatDialogModule} from '@angular/material/dialog';
import { NgbDatepicker } from '@ng-bootstrap/ng-bootstrap';
import { NgbDate, NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { MuserReportComponent } from './muser-report.component';

describe('MuserReportComponent', () => {
  let component: MuserReportComponent;
  let fixture: ComponentFixture<MuserReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MuserReportComponent, NgbDatepicker ],
            imports: [RouterTestingModule, MatSnackBarModule, HttpClientTestingModule, MatDialogModule, NgbModule],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MuserReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
