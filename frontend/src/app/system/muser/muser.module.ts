import { SharedModule } from './../../shared/shared.module';
import { NgModule } from '@angular/core';

import { MuserRoutingModule } from './muser-routing.module';
import { MuserComponent } from './muser.component';
import { MuserEditComponent } from './muser-edit/muser-edit.component';
import { MuserListComponent } from './muser-list/muser-list.component';
import { MusersFilterPipe } from '../shared/pipes/musers-filter.pipe';
import { MuserConfigComponent } from './muser-config/muser-config.component';
import { MuserReportComponent } from './muser-report/muser-report.component';

@NgModule({
  imports: [
    SharedModule,
    MuserRoutingModule
  ],
  declarations: [
    MuserComponent,
    MuserEditComponent,
    MuserListComponent,
    MusersFilterPipe,
    MuserConfigComponent,
    MuserReportComponent
  ]
})
export class MuserModule { }
