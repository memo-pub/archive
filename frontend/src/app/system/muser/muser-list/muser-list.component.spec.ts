import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import {HttpClientModule} from '@angular/common/http';
import { MatDialogModule} from '@angular/material/dialog';
import { MusersFilterPipe } from '../../shared/pipes/musers-filter.pipe';

import { MuserListComponent } from './muser-list.component';

describe('MuserListComponent', () => {
    let component: MuserListComponent;
    let fixture: ComponentFixture<MuserListComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [MuserListComponent, MusersFilterPipe],
            imports: [RouterTestingModule, MatSnackBarModule, HttpClientTestingModule, MatDialogModule],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(MuserListComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
