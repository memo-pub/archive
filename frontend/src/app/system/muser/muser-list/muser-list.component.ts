import { Component, OnInit, HostListener, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { PageEvent, MatPaginator } from '@angular/material/paginator';
import { isNullOrUndefined } from 'util';

import { Muser } from '../../../shared/models/muser.model';
import { AuthService } from '../../../shared/services/auth.service';
import { MuserService } from '../../shared/services/muser.service';
import { GlobalService } from '../../../shared/services/global.service';
import { FiltersService } from '../../shared/services/filters.service';

@Component({
  selector: 'app-muser-list',
  templateUrl: './muser-list.component.html',
  styleUrls: ['./muser-list.component.css']
})
export class MuserListComponent implements OnInit {
  loggedInUser: Muser;
  // searchStr = '';
  musers: Muser[];
  muserTypes: any[];

  // MatPaginator Inputs
  // length = 100;
  // pageSize = 100;
  // pageSizeOptions: number[] = [5, 10, 25, 100];
  // pageEvent: PageEvent;
  start = 0;
  end = 0;
  // @ViewChild(MatPaginator) paginator: MatPaginator;

  // aditStr = 'Редактировать';

  /**Для пагинатора */
  // page = 1;
  limit = 100;

  // @HostListener('window:resize', ['$event'])
  // onResize(event) {
  //   this.aditStr =
  //     event.target.innerWidth < 700
  //       ? '<i class="fa fa-pencil" aria-hidden="true"></i>'
  //       : 'Редактировать';
  // }

  constructor(
    public filtersService: FiltersService,
    private globalService: GlobalService,
    private authService: AuthService,
    private muserService: MuserService,
    private router: Router,
    private spinnerService: NgxSpinnerService
  ) {}

  async ngOnInit() {
    this.spinnerService.show();
    this.muserTypes = this.globalService.muserTypes;
    // this.aditStr =
    //   window.screen.width < 675
    //     ? '<i class="fa fa-pencil" aria-hidden="true"></i>'
    //     : 'Редактировать';
    try {
      let loggedInUser = this.muserService.getOneById(
        this.authService.getCurUser().id
      );
      let musers = this.muserService.getAll();
      this.loggedInUser = await loggedInUser;
      this.musers = isNullOrUndefined(await musers) ? [] : await musers;
      this.musers.sort((a, b) => (a.id > b.id ? 1 : -1));
      // this.length = this.musers.length;
      // if (this.length > 100) {
      //   this.pageSizeOptions.push(this.length);
      // }
      this.end = this.limit;
    } catch (err) {
      if (err.status === 401) {
        console.error(`Ошибка авторизации`);
        this.authService.logout();
      } else {
        console.error(`Ошибка ${err}`);
      }
    } finally {
      this.spinnerService.hide();
    }
  }

  updateData() {
    if (this.filtersService.muserListFilter.searchStr.length > 0) {
      this.start = 0;
      // this.end = this.paginator.pageSize;
      this.end = this.limit;
      // this.paginator.firstPage();
      this.filtersService.muserListFilter.page = 1;
    } else {
      // this.start = this.paginator.pageSize * this.paginator.pageIndex;
      // this.end = this.start + this.paginator.pageSize;
      this.start = this.limit * (this.filtersService.muserListFilter.page - 1);
      this.end = this.start + this.limit;
    }
  }

  setPageSizeOptions(event: PageEvent) {
    this.start = event.pageSize * event.pageIndex;
    this.end = this.start + event.pageSize;
    return event;
  }

  onAddUser() {
    this.router.navigate(['/sys/users/edit/newprofile']);
  }

  onEditUser(id) {
    this.router.navigate([`/sys/users/edit/${id}`]);
  }

  trim(content) {
    let end: number;
    let str: string;
    if (document.documentElement.offsetWidth > 650) {
      end = Math.floor((document.documentElement.offsetWidth - 300) / 60);
    } else {
      end = Math.floor(document.documentElement.offsetWidth / 60);
    }
    if (content.length > end + 2) {
      str = content.substring(0, end);
    } else {
      str = content;
    }
    return { str, end };
  }

  getValueByKey(key): string {
    let _item = this.muserTypes.find(item => item.key === key);
    return isNullOrUndefined(_item) ? '' : _item.value;
  }

  goToPage(n: number): void {
    this.filtersService.muserListFilter.page = n;
    this.updatePage();
  }

  onNext(): void {
    this.filtersService.muserListFilter.page++;
    this.updatePage();
  }

  onPrev(): void {
    this.filtersService.muserListFilter.page = 1;
    this.updatePage();
  }

  onNext10(n = 1): void {
    this.filtersService.muserListFilter.page += 10 * n;
    this.updatePage();
  }

  onPrev10(n = 1): void {
    this.filtersService.muserListFilter.page -= 10 * n;
    this.updatePage();
  }

  async updatePage() {
    this.start = this.limit * (this.filtersService.muserListFilter.page - 1);
    this.end = this.start + this.limit;
  }
}
