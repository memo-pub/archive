import { Component, OnInit, HostListener } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormGroup, FormControl } from '@angular/forms';

import { AuthService } from '../../../shared/services/auth.service';
import { MuserService } from '../../shared/services/muser.service';
import { MconfigService } from './../../shared/services/mconfig.service';
import { Muser } from '../../../shared/models/muser.model';

@Component({
  selector: 'app-muser-config',
  templateUrl: './muser-config.component.html',
  styleUrls: ['./muser-config.component.css'],
})
export class MuserConfigComponent implements OnInit {
  mconfigForm: FormGroup;
  loggedInUser: Muser;
  mconfig = [];
  width: number;

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.width = event.target.innerWidth;
  }

  @HostListener('window:keydown', ['$event'])
  onKeyPress(event: KeyboardEvent) {
    if ((event.ctrlKey || event.metaKey) && event.keyCode == 83) {
      event.preventDefault();
      if (!this.mconfigForm.invalid) {
        this.onSaveMconfigForm();
      }
    }
  }
  constructor(
    private authService: AuthService,
    private muserService: MuserService,
    private mconfigService: MconfigService,
    private spinnerService: NgxSpinnerService,
    public snackBar: MatSnackBar
  ) {}

  async ngOnInit() {
    this.spinnerService.show();
    this.width = window.screen.width;
    this.mconfigForm = new FormGroup({
      standart_page_price: new FormControl(null),
      special_page_price: new FormControl(null),
      file_scan_price: new FormControl(null),
      file_archive_price: new FormControl(null),
      personal_price: new FormControl(null),
      in_personal_price: new FormControl(null),
      video_ext: new FormControl(null),
    });
    try {
      let loggedInUser = this.muserService.getOneById(
        this.authService.getCurUser().id
      );
      let mconfig = this.mconfigService.getAll();
      this.mconfig = await mconfig;
      this.loggedInUser = await loggedInUser;
      this.getData();
    } catch (err) {
      if (err.status === 401) {
        console.error(`Ошибка авторизации`);
        this.authService.logout();
      } else {
        console.error(`Ошибка ${err}`);
      }
    } finally {
      this.spinnerService.hide();
    }
  }

  async getData() {
    this.mconfigForm.patchValue({
      standart_page_price: this.mconfig.find(
        (mconfig) => mconfig.key === 'standart_page_price'
      ).value.value,
      special_page_price: this.mconfig.find(
        (mconfig) => mconfig.key === 'special_page_price'
      ).value.value,
      file_scan_price: this.mconfig.find(
        (mconfig) => mconfig.key === 'file_scan_price'
      ).value.value,
      file_archive_price: this.mconfig.find(
        (mconfig) => mconfig.key === 'file_archive_price'
      ).value.value,
      personal_price: this.mconfig.find(
        (mconfig) => mconfig.key === 'personal_price'
      ).value.value,
      in_personal_price: this.mconfig.find(
        (mconfig) => mconfig.key === 'in_personal_price'
      ).value.value,
      video_ext: this.mconfig.find((mconfig) => mconfig.key === 'video_ext')
        .value.value,
    });

  }

  async onSaveMconfigForm() {
    this.spinnerService.show();
    try {
      let putMconfigs = Promise.all(
        this.mconfig.map(async (mconfig) => {
          if (this.mconfigForm.value.hasOwnProperty(mconfig.key)) {
            if (mconfig.key === 'video_ext') {
              mconfig.value.value = this.mconfigForm.value[mconfig.key];
              let obj = { value: this.mconfigForm.value[mconfig.key] };
              let putMconfig = await this.mconfigService.putOneById(
                mconfig.key,
                {
                  value: JSON.stringify(obj),
                }
              );
            } else {
              mconfig.value.value = this.mconfigForm.value[mconfig.key];
              let obj = { value: this.mconfigForm.value[mconfig.key] };
              let putMconfig = await this.mconfigService.putOneById(
                mconfig.key,
                {
                  value: JSON.stringify(obj),
                }
              );
            }
          }
        })
      );
      await putMconfigs;
      this.openSnackBar(`Сохранено`, 'Ok');
    } catch (err) {
      if (err.status === 401) {
        console.error(`Ошибка авторизации`);
        this.authService.logout();
      } else {
        console.error(`Ошибка ${err}`);
        this.openSnackBar(`Ошибка ${err.status}`, 'Ok');
      }
    } finally {
      this.spinnerService.hide();
    }
  }

  trimText() {}

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 10000,
    });
  }
}
