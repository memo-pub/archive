import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MuserComponent } from './muser.component';
import { MuserConfigComponent } from './muser-config/muser-config.component';
import { MuserListComponent } from './muser-list/muser-list.component';
import { MuserEditComponent } from './muser-edit/muser-edit.component';
import { MuserReportComponent } from './muser-report/muser-report.component';
import { AdminGuard } from 'src/app/shared/services/admin.guard';
import { AuthGuard } from 'src/app/shared/services/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: MuserComponent,
    children: [
      {
        path: 'edit/:id',
        canActivate: [AuthGuard],
        component: MuserEditComponent
      },
      {
        path: 'list',
        canActivate: [AdminGuard],
        component: MuserListComponent
      },
      {
        path: 'config',
        canActivate: [AdminGuard],
        component: MuserConfigComponent
      },
      {
        path: 'report',
        canActivate: [AdminGuard],
        component: MuserReportComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MuserRoutingModule {}
